/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: funcio							*/
/*                                                         		*/
/* Module Description: DMT shared memory support for smartio		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 13Dec00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: funcio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FUNCIO_H
#define _LIGO_FUNCIO_H

#include "iosupport.hh"
#include <string>


namespace fantom {


/** @name Function callback support
    This header defines support methods reading and writing frames
    through function callbacks.
   
    @memo Function callback support
    @author Written November 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Function callback IO class. This is a support class for the smart 
    function callback input and smart function callback output device 
    classes. 

    The callback function for the output has the following prototype:
    \begin{verbatim}
    bool callback (const char* frame, int len, void* priv);
    \end{verbatim}
    The address of the callback function constitues the name in ascii.
    (use the %p option in printf).    
   
    \begin{verbatim}
    func://'func addr' [-d 'priv.']
    The following options are supported:
    -d 'priv.' : Pointer to private data block (used in callback)
    \end{verbatim}
    
    @memo DMT IO support class.
 ************************************************************************/
   class func_support : public iosupport {
      friend class func_frame_storage;
      friend class func_frameout;
   
   public:
      /// iIput callback prototype
      typedef bool (*in_callback) (char*& frame, int& len, 
                        void* priv);
      /// Output callback prototype
      typedef bool (*out_callback) (const char* frame, int len, 
                        void* priv);
   
       /** Create support for DMT shared memory partitions.
           @param prod DMT support acts as a producer
           @param pname Partition name
           @param conf Option argument
         */
      explicit func_support (bool isout = true, const char* faddr = 0, 
                        const char* conf = 0) 
      : fOut (isout), fFunc (0), fData (0), fEoC (false) {
         setFaddr (faddr, conf); }
      /// Destructor
      virtual ~func_support() {
      }
   
      /// Set partition name
      bool setFaddr (const char* addr = 0, const char* conf = 0);
      /// Get function address
      out_callback getFuncOut() const {
         return (out_callback)fFunc; }
      /// Get function address
      in_callback getFuncIn() const {
         return (in_callback)fFunc; }
      /// Get private data address
      void* getPriv () const {
         return fData; }
   
      /// Read next frame into buffer
      virtual framefast::basic_frame_storage* readFrame ();
      /// get callback writer
      virtual framefast::basic_frameout* getWriter (const char* fname);
      /// End of file?
      virtual bool eof() const {
         return fEoC; }
      /// Set end of callbacks
      virtual void setEoC() {
         fEoC = true; }
   
   protected:
      /// Is producer? (frame output)
      bool		fOut;
      /// Partition name (as specified)
      void*		fFunc;
      /// Buffer length
      void*		fData;
      /// End of callback?
      bool 		fEoC;
   };


//@}

}

#endif // _LIGO_FUNCIO_H
