#include <time.h>
#include <stdio.h>
#include "goption.hh"
#include "funcio.hh"
#include "framefast/frametype.hh"
#include "framefast/frameio.hh"
#include <iostream>
#include <cstring>

namespace fantom {
   using namespace std;
   using namespace gdsbase;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// func frame out                                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class func_frameout : public framefast::basic_frameout {
   public:
      /// Constructor
      func_frameout (func_support& dsup);
      /// Destructor
      virtual ~func_frameout ();
      /// Open
      virtual bool open (int flen = 0);
      /// Close
      virtual void close();
      /// Write
      virtual bool write (const char* p, int len);
      /// Scatter-gather write
      virtual bool write (const src_dest_t* s, int slen);
   
   protected:
      /// Pointer to function callback support 
      func_support*	fFunc;
      /// Length of data
      int		fLen;
      /// buffer pointer
      char*		fBuf;
   };

//______________________________________________________________________________
   func_frameout::func_frameout (func_support& dsup)
   : fFunc (&dsup), fLen (0), fBuf (0)
   {
   }

//______________________________________________________________________________
   bool func_frameout::open (int flen)
   {
      if (fBuf) close();
      // allocate buffer
      fLen = flen;
      fBuf = new char [fLen];
      return (fBuf != 0);
   }

//______________________________________________________________________________
   void func_frameout::close()
   {
      // free buffer
      if (fBuf) {
         // do callback
         if (!fFunc->getFuncOut() (fBuf, fLen, fFunc->getPriv())) {
            fFunc->setEoC();
         }
         delete [] fBuf;
      }
      fBuf = 0;
      fLen = 0;
   }

//______________________________________________________________________________
   bool func_frameout::write (const char* p, int size)
   {
      //cerr << "Write " << size << "B" << endl;
   
      // check buffer
      if (!fBuf || !p) {
         return false;
      }
      if (size <= 0) {
         return true;
      }
       // check length
      if (fSofar + size > fLen) {
         size = fLen - fSofar;
      }
      // copy data
      memcpy (fBuf + fSofar, p, size);
      fSofar += size;
      return true;
   }

//______________________________________________________________________________
   bool func_frameout::write (const src_dest_t* s, int slen)
   {
      return basic_frameout::write (s, slen); 
   }

//______________________________________________________________________________
   func_frameout::~func_frameout ()
   {
      close();
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// func_support		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool func_support::setFaddr (const char* addr, const char* conf) 
   {
      // clean up first
      fFunc = 0;
      fData = 0;
   
      // check partition name; remove leading /
      if (!addr || !*addr) {
         return false;
      }
      sscanf (addr, "%p", &fFunc);
      // get option arguments
      option_string opts (addr, conf, "d:");
      //cerr << "FUNC CONF IS " << (conf ? conf : "") << endl;
      string s;
      if (opts.getOpt ('d', s)) {
         sscanf (s.c_str(), "%p", &fData);
      }
      return (fFunc != 0);
   }

//______________________________________________________________________________
   framefast::basic_frame_storage* func_support::readFrame ()
   {
      if (fOut || !fFunc) {
         return 0;
      }
      char* frame = 0;
      int len = 0;
      // do callback
      if (!getFuncIn() (frame, len, fData)) {
         setEoC();
         return 0;
      }
      else {
         return new framefast::memory_frame_storage (frame, len, true);
      }
   }

//______________________________________________________________________________
   framefast::basic_frameout* func_support::getWriter (const char* fname)
   {
      if (!fOut || !fFunc) {
         return 0;
      }
      return new (nothrow) func_frameout (*this);
   }


}
