#ifndef _LIGO_SENDSIO_H
#define _LIGO_SENDSIO_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: ndsio							*/
/*                                                         		*/
/* Module Description: NDS support for smartio				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 3Mar01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: ndsio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "iosupport.hh"
#include "fchannel.hh"
#include "fname.hh"
#include <string>

namespace sends {
  class NDS2Socket;
}

namespace fantom {


/** @name NDS2 support
    This header defines support methods for reading from a network
    data server (NDS2).
   
    @memo NDS2 support
    @author Written July 2009 by John Zweizig
    @version 1.0
 ************************************************************************/

//@{


/** NDS2 IO class. This is a support class for the smart NDS input
    class.
    
    @memo NDS2 IO support class.
 ************************************************************************/
   class sends_support : public iosupport {
   public:
      /// Default NDS port
      static const int kNDSPORT;
   
      /// Create NDS support
      explicit sends_support (const char* servername = 0, 
                        const char* conf = 0);
      /// Destructor
      virtual ~sends_support();
   
      /// Set device/file name
      void setServer (const char* servername);
      /// Set configuration
      void setConf (const char* conf);
   
      /// Get address
      const char* getAddr() const {
         return fAddr.c_str(); }
      /// Get port number
      int getPort () const {
         return fPort; }
      /// Get frame type
      frametype getType() const {
         return fType; }
   
      /// Set channel selection
      bool selectChannels (const channelquerylist* chns);
      /// Read next frame into buffer (caller owns return object!)
      virtual framefast::basic_frame_storage* readFrame ();
      /// Get frame writer (output not supported!)
      virtual framefast::basic_frameout* getWriter (const char* fname) {
         return 0; }
      /// End of file
      virtual bool eof() const;
   
      /// Get list of channels
      static bool getChannels (const char* server, int port,
                        channellist& chns, frametype utype,
			unsigned long start = 0, unsigned long stop = 0);
      /// Get list of channels
      bool getChannels (channellist& chns) const;
   
      /// Get list of times
      static bool getTimes (const char* server, int port,
                        Time& start, Time& stop, frametype utype);
      /// Get list of times
      bool getTimes (Time& start, Time& stop) const;
   
   protected:
      /// Internet address
      std::string	fAddr;
      /// Internet port number
      int		fPort;
      /// Frame type (FF, MTF or STF)
      frametype		fType;
      /// Epoch times.
      unsigned long	fEpochStart ;
      unsigned long     fEpochStop ;
   
      /// Open connection
      bool open ();
      /// Close connection
      void close();
      /// request data
      bool request ();
      /// get data
      bool getdata (char*& data, int& len);
   
   private:
      /// socket
     sends::NDS2Socket*	fNDS;
   };


//@}

}

#endif // _LIGO_NDSIO_H
