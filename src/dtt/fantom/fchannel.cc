/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <time.h>
#include <fnmatch.h>
#include <regex.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <fstream>
#include <functional>
#include "fchannel.hh"

namespace fantom {
   using namespace std;

   static int my_debug = 0 ;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// channelentry                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   channelentry::channelentry (const char* name, float rate, int dcuid)
   : fActive (name != 0), fName (name ? name : ""), fRate (rate),
     fUDN (""), fIsDuplicate (false), fHasDuplicate (false),
     fWildcard (false), fDCUID(dcuid)
   {
      if ((fName.find ('*') != string::npos) || 
         (fName.find ('?') != string::npos) || 
         (fName.find ('[') != string::npos)) {
         fWildcard = true;
      }
   }

//______________________________________________________________________________
   bool channelentry::operator== (const channelentry& chn) const
   {
#if 0
      return ((strcasecmp (fName.c_str(), chn.fName.c_str()) == 0) &&
	     (fRate == chn.fRate) &&
             (strcasecmp (fUDN.c_str(), chn.fUDN.c_str()) == 0));
#else
      // Compare both the name and the data rate to determine if the channel
      // entries are equal.
      bool rc = strcasecmp (fName.c_str(), chn.fName.c_str()) == 0 ;
      if (rc) {
	 rc = chn.fRate == 0 || fRate == 0 || fRate == chn.fRate ;
	 if (!rc) {
	    if (my_debug) cerr << "channelentry::operator==, " << chn.fName << " has different rates, " << chn.fRate << " and " << fRate << endl ;
	 }
	 else
	    rc = strcasecmp (fUDN.c_str(), chn.fUDN.c_str()) == 0 ;
      }
      return rc ;
#endif
   }

//______________________________________________________________________________
   bool channelentry::operator< (const channelentry& chn) const
   {
      int rv = strcasecmp (fName.c_str(), chn.fName.c_str());
      return rv < 0 || (!rv && strcasecmp(fUDN.c_str(), chn.fUDN.c_str()) < 0);
   }

//______________________________________________________________________________
   bool channelentry::checkDuplicateNameRate (channelentry& chn)
   {
      if (strcasecmp (fName.c_str(), chn.fName.c_str()) == 0) {
	 if ( fRate == 0 || chn.fRate == 0 || fRate == chn.fRate ) 
	 {
	    chn.fIsDuplicate = true;
	    fHasDuplicate = true;
	    chn.fHasDuplicate = true;
	    return true;
	 }
	 else {
	    if (my_debug) cerr << "channelentry::checkDuplicateNameRate() - " << chn.fName << " has multiple rates, " << chn.fRate << " and " << fRate << endl ;
	    return false;
	 }
      }
      else
	 return false ;
   }

//______________________________________________________________________________
   bool channelentry::checkDuplicateName (channelentry& chn)
   {
      if (strcasecmp (fName.c_str(), chn.fName.c_str()) == 0) {
	 chn.fIsDuplicate = true;
	 fHasDuplicate = true;
	 chn.fHasDuplicate = true;
	 return true;
      }
      else
	 return false ;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Channel routines                                                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool Channels2String (const channellist& list, std::string& chns,
                     bool suppressUDN)
   {
      chns = "";
      chns.reserve (40 * list.size());
      for (const_chniter i = list.begin(); i != list.end(); ++i) {
         if (!i->Active()) {
            continue;
         }
         if (!chns.empty()) chns += " ";
         chns += i->Name();
         if (!suppressUDN && (strlen (i->UDN()) > 0)) {
            chns += string (" ") + i->UDN();
         }
         if (i->Rate() > 0) {
            char buf[256];
            sprintf (buf, "%g", i->Rate());
            chns += string (" ") + buf;
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool String2Channels (channellist& list, const char* chns)
   {
      list.clear();
      // copy input string and parse it
      char* p = new (nothrow) char [strlen (chns) + 10];
      strcpy (p, chns);
      char* last;
      char* tok = strtok_r (p, " \t\n\f\r\v", &last);
      while (tok) {
         string name = tok;
         string udn;
         float rate = 0;
         tok = strtok_r (0, " \t\n\f\r\v", &last);
         // check if following token represents a UDN
         if (tok && (*tok == '@')) {
            udn = tok + 1;
            tok = strtok_r (0, " \t\n\f\r\v", &last);
         }
         // check if following token represents sampling rate?
         bool num = true;
         for (char* c = tok; c && *c && num; c++) {
            num = isdigit (*c) || (*c == '.');
         }
         if (tok && *tok && num) {
            rate = atof (tok);
            tok = strtok_r (0, " \t\n\f\r\v", &last);
         }
         channelentry c (name.c_str(), rate);
         c.SetUDN (udn.c_str());
         list.push_back (c);
      }
      delete [] p;
      return true;
   }

//______________________________________________________________________________
   bool SortChannels (channellist& list, bool check_rate)
   {
      if (my_debug) cerr << "SortChannels() - size of list = " << list.size() << endl ;
      sort (list.begin(), list.end());
      if (!list.empty()) {
         chniter i = list.begin();
         chniter j = list.begin();
	 if (check_rate) {
	    for (++j; j != list.end(); ++i, ++j) {
	       i->checkDuplicateNameRate (*j);
	    }
	 } else {
	    for (++j; j != list.end(); ++i, ++j) {
	       i->checkDuplicateName (*j);
	    }
	 }
      }
      if (my_debug) cerr << "SortChannels() - return. Size of list = " << list.size() << endl ;
      return true;
   }

//______________________________________________________________________________
   chniter FindChannel (channellist& list, const char* name, const float rate)
   {
      channelentry chn (name, rate);
      chniter i = lower_bound (list.begin(), list.end(), chn);
      if ((i != list.end()) && (*i == chn)) {
         return i;
      }
      else {
         return list.end();
      }
   }

//______________________________________________________________________________
   const_chniter FindChannelConst (const channellist& list, const char* name, const float rate)
   {
      channelentry chn (name, rate);
      const_chniter i = lower_bound (list.begin(), list.end(), chn);
      if ((i != list.end()) && (*i == chn)) {
         return i;
      }
      else {
         return list.end();
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// channelquery                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   channelquery::channelquery (const char* pattern, float rate)
   : fIsWildcard (false), fPattern (pattern), fRate (fabs (rate))
   {
      for (string::iterator i = fPattern.begin(); 
          i != fPattern.end(); ++i) {
         *i = toupper (*i);
         if ((*i == '*') || (*i == '?') || (*i == '[')) {
            fIsWildcard = true;
         }
      }
   }

//______________________________________________________________________________
   channelquery::channelquery (const channelentry& chn)
   : fIsWildcard (chn.IsWildcard()), fPattern (chn.Name()), 
   fRate (chn.Rate())
   {
      for (string::iterator i = fPattern.begin(); 
          i != fPattern.end(); ++i) {
         *i = toupper (*i);
      }
   }

//______________________________________________________________________________
   bool channelquery::operator== (const char* s) const
   {
      return strcmp (s, fPattern.c_str()) == 0;
   }

//______________________________________________________________________________
   bool channelquery::match (const char* s) const
   {
      return fnmatch (fPattern.c_str(), s, 0) == 0;
   }

//______________________________________________________________________________
   std::string channelquery::str (bool withrate) const
   {
      char buf[1024];
      if (withrate) {
         sprintf (buf, "%s %g", fPattern.c_str(), fRate);
      }
      else {
         sprintf (buf, "%s", fPattern.c_str());
      }
      return buf;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// channel query list                                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   channelquerylist::channelquerylist (const channellist& list)
   {
      *this = list;
   }

//______________________________________________________________________________
   channelquerylist& channelquerylist::operator= (const channellist& list)
   {
      clear();
      for (const_chniter i = list.begin(); i != list.end(); ++i) {
         if (i->IsWildcard()) {
            fWList.push_back (channelquery(*i));
         }
         else {
            add (channelquery (*i));
         }
      }
      return *this;
   }

//______________________________________________________________________________
   std::string channelquerylist::str (bool withrate) const
   {
      string s;
      for (exactlist::const_iterator c = fEList.begin();
          c != fEList.end(); ++c) {
         s += c->second.str (withrate) + " ";
      }
      for (wildcardlist::const_iterator c = fWList.begin();
          c != fWList.end(); ++c) {
         s += c->str (withrate) + " ";
      }
      if (!s.empty()) {
         s.erase (s.size() - 1);
      }
      return s;
   }

//______________________________________________________________________________
   const channelquery* channelquerylist::findMatch (
                     const string& name) const
   {
      // transform to upper case
      string n (name);
      for (string::iterator i = n.begin(); i != n.end(); ++i) {
         *i = toupper (*i);
      }
      // check exact list first
      exactlist::const_iterator j = fEList.find (name);
      if (j != fEList.end()) {
         return &(j->second);
      }
      // check wildcards next
      bool (channelquery::*test) (const char*) const = 
         &channelquery::match;
      wildcardlist::const_iterator i =
	find_if (fWList.begin(), fWList.end(), 
#if __cplusplus >= 201100L
		 std::bind( std::mem_fn(test), std::placeholders::_1, n.c_str() )
#else
		 bind2nd( mem_fun_ref(test), n.c_str() )
#endif
		  );
      if (i != fWList.end()) {
         return &(*i);
      }
      else {
         return 0;
      }
   }

//______________________________________________________________________________
   void channelquerylist::add (const channelquery& q)
   {
      if (q.IsWildcard()) {
         fWList.push_back (q);
      }
      else {
         fEList.insert (exactlist::value_type (q.GetPattern(), q));
      }
   }

//______________________________________________________________________________
   void channelquerylist::add (const channelquerylist& list)
   {
      copy (list.fWList.begin(), list.fWList.end(), back_inserter (fWList));
      for (exactlist::const_iterator i = list.fEList.begin();
          i != list.fEList.end(); ++i) {
         fEList[i->first] = i->second;
      }
   }

//______________________________________________________________________________
   channelquerylist* newChannelList (const char* config, char* error)
   {
      channelquerylist* l = new (nothrow) channelquerylist;
      if (l == 0) {
         return 0;
      }
      const char* p = config;
      //cerr << "Channel list = " << p << endl;
      while (*p) {
         while (isspace (*p)) p++;
         if (!*p) 
            break;
         string pattern;
         int count = 0;
         // check if channel starts with "
         if (*p == '"') {
            // look for matching "
            while (p[count]) {
               count++;
               if ((p[count] == '"') && (p[count-1] != '\\')) {
                  break;
               }
            }
            if (!p[count]) {
               if (error) 
                  sprintf (error, "Error: Channel names must use """);
               delete l;
               return 0;
            }
            // found channel pattern
            pattern = string (p+1, count-1);
            p += count + 1;
         }
         // channel does not start with "
         else {
            // look for next space
            while (p[count] && !isspace (p[count])) {
               count++;
            }
            // found channel pattern
            pattern = string (p, count);
            p += count;
         }
         // check for sampling rate specification
         float rate = 0;
         while (isspace (*p)) p++;
         if (isdigit (*p)) {
            char* stop = 0;
            rate = fabs (strtod (p, &stop));
            p = stop;
         }
         l->add (channelquery (pattern.c_str(), rate));
      }
      if (l->empty()) {
         delete l;
         l = 0;
      }
      return l;
   }

//______________________________________________________________________________
   channelquerylist* newChannelListFromFile (const char* filename, 
                     char* error)
   {
      ifstream		inp (filename);
      if (!inp) {
         if (error) sprintf (error, "Illegal filename\n");
         return 0;
      }
   
      channelquerylist* l = new (nothrow) channelquerylist;
      if (l == 0) {
         if (error) sprintf (error, "Memory allocation failed\n");
         return 0;
      }
   
      // read in file and add channel
      string		line;
      string		rawline;
      getline (inp, rawline, '\n');
      while (inp) {
         line = trim (rawline.c_str());
         getline (inp, rawline, '\n');
         if (line.empty() || (line[0] == '#')) {
            continue;
         }
         // look for space
         const char* pp = line.c_str();
         const char* p = pp;
         while (*p && !isspace (*p)) p++;
         // found channel pattern
         string pattern (pp, (int)(p - pp));
         // check for sampling rate specification
         float rate = 0;
         while (isspace (*p)) p++;
         if (*p) {
            char* stop = 0;
            rate = fabs (strtod (p, &stop));
            p = stop;
         }
         l->add (channelquery (pattern.c_str(), rate));
      }
      // check if empty?
      if (l->empty()) {
         delete l;
         l = 0;
      }
      return l;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// channel filtering                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool QueryChannel (const channelentry& channel, 
                     const channelquerylist* filter)
   {
      return (filter == 0) || (filter->findMatch (channel) != 0);
   }

//______________________________________________________________________________
   struct TestChannel {
      // reference to channel query list
      const channelquerylist& fFilter;
      TestChannel (const channelquerylist& filter) :
      fFilter (filter) {
      }
      bool operator() (const channelentry& channel) {
         return fFilter.findMatch (channel) == 0; }
   };

//______________________________________________________________________________
   bool FilterChannels (channellist& list, 
                     const channelquerylist* filter)
   {
      if (filter == 0) {
         return true;
      }
      chniter pos = remove_if (list.begin(), list.end(),
                           TestChannel (*filter));
      list.erase (pos, list.end());
      return true;
   }

//______________________________________________________________________________
   bool FilterChannels (const channellist& list1, 
                     channellist& list2,
                     const channelquerylist* filter)
   {
      if (my_debug) {
	 cerr << "FilterChannels()" << endl ;
	 cerr << "FilterChannels - size of list1 is " << list1.size() << endl ;
	 cerr << "FilterChannels - size of list2 is " << list2.size() << endl ;
	 if (filter)
	    cerr << "FilterChannels - filter is " << filter->str() << endl ;
      }
      if (filter == 0) {
	 if (my_debug) cerr << "FilterChannels - filter is null" << endl ;
         copy (list1.begin(), list1.end(), back_inserter (list2));
      }
      else {
         for (const_chniter i = list1.begin(); i != list1.end(); ++i) {
	    if (my_debug) cerr << "  i->Name() = " << i->Name() << endl ;
            if (QueryChannel (*i, filter)) {
               list2.push_back (*i);
            }
         }
      }
      if (my_debug) {
	 cerr << "FilterChannels at end" << endl ;
	 cerr << "FilterChannels - size of list1 is " << list1.size() << endl ;
	 cerr << "FilterChannels - size of list2 is " << list2.size() << endl ;
      }
      return true;
   }

}
