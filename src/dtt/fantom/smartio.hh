/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: smartio							*/
/*                                                         		*/
/* Module Description: smart IO module for fantom			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 4Oct00   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: framefast.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_SMARTIO_H
#define _LIGO_SMARTIO_H

#include "PConfig.h"
#include "Time.hh"
#include "Interval.hh"
#include "gmutex.hh"
#include "fmsgq.hh"
#include "framefast/frametype.hh"
#include "fname.hh"
#include <string>
#include <vector>
#include <map>
#include <deque>
#include <queue>

namespace framefast {
   class framereader;
   class framewriter;
}

namespace fantom {


   class channelquerylist;


/** @name Smart IO
    This header defines the smart IO classes used by fantom.
   
    @memo Smart Input/Output
    @author Written October 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Smart channel IO base class. This is the base class for both the
    smart_input and the smart_output classes. It implements the mutex
    and a thread capability to support asynchronous IO.
    
    @memo Smart IO base class.
 ************************************************************************/
   class smartio_basic {
      friend void* iothread_start (void* p);
   
   public:
      /// io state
      enum iostate {
      /// inactive
      io_inactive = 0,
      /// active
      io_active = 1,
      /// failed
      io_failed = 2
      };
   
      typedef fantom::namerecord namerecord;
      typedef fantom::namelist namelist;
      typedef fantom::fmsgqueue::fmsg fmsg;
   
      /// Generic IO channel
      explicit smartio_basic (bool out = false);
      /// Destructor
      virtual ~smartio_basic ();
   
      /// Returns the file extension
      static std::string extension (frametype ftype);
   
      /// Setup start the IO thread
      virtual bool setup ();
      /// Terminates the IO thread
      virtual void terminate ();
   
      /// Returns true on error
      bool operator! () {
         return fError; }
      /// Last message
      const char* Message () const {
         return fMsg.c_str(); }
      /// Is output channel?
      bool isOut () const { 
         return fOutDev; }
      /// get state
      virtual iostate getState () const { 
         return fState; }
      /// set state
      virtual bool setState (iostate state);
      /// set log message
      virtual bool log (const fmsg& msg) {
         return fLog.push (msg); }
      /// set log message
      virtual bool log (const char* msg) {
         return fLog.push (msg); }
      /// pop a log message from queue (true if popped)
      virtual bool poplog (fmsg& msg) {
         return fLog.pop (msg); }
      /// Set time limits
      virtual void setTimeLimits (const Time& t0, const Interval& dt) {
         fT0 = t0; fDt = dt; }
      /// Get time limits
      virtual void getTimeLimits (Time& t0, Interval& dt) const {
         t0 = fT0; dt = fDt; }
      /// set new channel list
      virtual bool setChannelList (const channelquerylist* q);
      /// set new channel list
      virtual bool setChannelList (const char* config);
      /// get channel list
      virtual const channelquerylist* getChannelList () const {
         return fQuery; }
   
      /// add a new name by parsing the command line arguments
      virtual bool parseName (devicetype dtype, const char* arg);
      /// add a new name by parsing the configuration line
      virtual bool parseName (const char* conf);
      /// add a new name (record gets adopted)
      virtual bool addName (namerecord* name, bool back = true);
      /// add a new name
      virtual bool addName (const char* name, bool back = true);
      /// add a new name
      virtual bool addName (const char* name, const char* conf,
                        bool back = true);
      /// add a new name representing a file
      virtual bool addFileName (const char* name, unsigned int cont = 0, 
                        bool back = true);
      /// add a new name representing a file or a file list (wildcard)
      virtual int addFiles (const char* name, bool back = true);
      /// add a list of names to the beginnin of the list
      virtual bool addNameList (const char* mem, int len, bool back = true);
      /// Remove a name from the list and return it (caller owns!)
      virtual namerecord* removeName ();
      /// load a new name into location
      virtual bool loadName (namelist::iterator loc);
      /// get user data
      void* getUser() const {
         return fUser; }
      /// set user data
      void setUser (void* user) {
         fUser = user; }
   
      /// Clear IO caches
      static void ClearCache();
   
   protected:
      /// mutex
      mutable thread::recursivemutex	fMux;
      /// Error?
      bool		fError;
      /// Error message
      std::string	fMsg;
      /// input or output?
      bool		fOutDev;
      /// state
      iostate		fState;
      /// Work log
      fmsgqueue		fLog;
      /// Name list
      namelist		fNames;
      /// user type
      void*		fUser;
      /// nominal queue length (access req. mutex)
      int		fQueueLen;
      /// memory size limit for queue (in kB) (access req. mutex)
      int		fQueueLimit;
      /// time interval limit (0 = start at beginning)
      Time		fT0;
      /// time duration limit (<=0 = infinite)
      Interval		fDt;
      /// channel query
      channelquerylist*	fQuery;
   
      /// IO thread ID
      pthread_t		fTID;
      /// IO thread
      virtual void iothread() {
      }
      /// Tests if thread is busy
      virtual bool busy() const {
         return false; }
   
   private:
      smartio_basic (const smartio_basic&);
      smartio_basic& operator= (const smartio_basic&);
   };


/** Smart input base class. This class implements the basic features
    of an input device for frame files.

    Support for multiple frame request anwsers:
    In most cases a smart_input will handle exaclty one frame 
    input stream. But in some cases when accessing the archive server
    LARS, the returned data is split into several frame streams.
    To scope with this situation, the getFrameIn function requires
    an argument specifying the frame stream. The number of frame
    streams can be queried with getFrameNum. Typically, one would
    implement a for loop to run through all frame streams. The
    number of frame streams can change from request to request. The
    following functions may alter this number: next (overall), eof,
    wait and prefetch. The following functions are guaranteed not
    to alter the number of stream: getFrameNum, getFrameIn, nexttime 
    and next (with stream position argument).
    
    @memo Smart input class.
 ************************************************************************/
   class smart_input : public smartio_basic {
   public:
      /// Default constructor
      smart_input () 
      : smartio_basic (false), fEOF (false), fIn (0) {
      }
      /// Constructor
      explicit smart_input (const char* conf);
      /// Destructor
      virtual ~smart_input ();
   
      /// discard frame at position number n
      virtual bool next (int n);
      /// end of file?
      bool eof ();
      /// Prefetch input frames
      virtual bool prefetch (int count = 1);
      /// Wait for at least one frame at each frame input stream
      virtual bool wait (const bool* ctrlC = 0);
      /// Wait for frames as old or older than t0
      virtual bool wait (const Time& t0, const bool* ctrlC = 0);
      /// time of next frame
      virtual Time nexttime (int n) const;
   
      /// get the number of frames waiting
      virtual int getFrameNum () const;
      /// get the current frame reader for the frame with number n
      virtual framefast::framereader* getFrameIn (int n) const;
   
   public:
      /// Child list type
      typedef std::vector<smart_input*> childlist;
      /// Child list iterator
      typedef childlist::iterator childiterator;
      /// Const child list iterator
      typedef childlist::const_iterator const_childiterator;
      /// input queue element
      class inputqueue_el {
      public:
         /// Pointer to frame
         framefast::framereader*	fFrame;
         /// List of children
         childlist			fChildren;
         /// Constructor
         inputqueue_el() : fFrame (0) {
         }
         /// Clear
         void clear();
      };
      /// input queue
      typedef std::deque<inputqueue_el> inputqueue;
   
   protected:
      /// EOF file? (access req. mutex)
      bool		fEOF;
      /// Time of next frame
      Time		fNext;
      /// current frame input
      framefast::framereader*	fIn;
      /// prefetch queue (access req. mutex)
      inputqueue	fInQueue;
      /// Child list
      childlist		fChildren;
      /// Input task busy?
      bool		fInputBusy;
   
      /// Load a frame from a specified location
      bool loadFrameFrom (inputqueue_el& el, 
                        namelist::iterator name, bool& keepname);
      /// Return the queue size in kB (use req. mutex)
      int queueTotal() const;
      /// IO thread
      virtual void iothread();
      /// Tests if thread is busy
      virtual bool busy() const;
   };


/** Smart output base class. This class implements the basic features
    of an output device for frame files.
    
    @memo Smart output class.
 ************************************************************************/
   class smart_output : public smartio_basic {
   public:
      /// output queue
      typedef std::deque<framefast::framewriter*> outputqueue;
   
      /// Default constructor
      smart_output () 
      : smartio_basic (true), fFrameType (framefast::FF), fFrameLen (1), 
      fFrameN (1), fFrameCompress (true), fRunNum (0), fFrameNum (0),
      fOut (0) {
      }
      explicit smart_output (const char* conf);
      /// Destructor
      virtual ~smart_output ();
   
      /// move to the next frame
      virtual bool next (const bool* ctrlC = 0);
   
      /// set channel type
      virtual bool setType (const char* type);
      /// get frame type
      frametype getFrameType() const {
         return fFrameType; }
      /// get frame length
      int getFrameLength () const {
         return fFrameLen; }
      /// get number of frames per file
      int getFrameN () const {
         return fFrameN; }
      /// get compression type
      int getFrameCompress () const {
         return fFrameCompress; }
      /// get frame version
      int getFrameVersion () const {
         return fFrameVersion; }
      /// get the current frame writer
      framefast::framewriter* getFrameOut () const {
         return fOut; }
      /// create a new output frame
      framefast::framewriter* createFrame();
      /// set detector info
      void setDetectorInfo (const framefast::detector_t& det) {
         fDetector = det; }
      /** Set the frame number.
          @memo Set the frame number.
          @param num frame number
       ******************************************************************/
      void setFrameNum (int num) {
         fFrameNum = num; }
      /** Get the frame number.
          @memo Get frame number.
          @return frame number
       ******************************************************************/
      int getFrameNum () const {
         return fFrameNum; }
      /** Set the run number.
          @memo Set the run number.
          @param num run number
       ******************************************************************/
      void setRunNum (int num) {
         fRunNum = num; }
      /** Get the run number.
          @memo Get run number.
          @return run number
       ******************************************************************/
      int getRunNum () const {
         return fRunNum; }
   
   protected:
      /// Frame type
      frametype		fFrameType;
      /// Frame length
      int		fFrameLen;
      /// Number of frames per file
      int		fFrameN;
      /// Frame compressed?
      int		fFrameCompress;
      /// Frame specification version
      int		fFrameVersion;
      /// Run number
      int		fRunNum;
      /// Frame number
      int		fFrameNum;
      /// current frame output
      framefast::framewriter*	fOut;
      /// Detector information
      framefast::detector_t	fDetector;
      /// write back queue
      outputqueue	fOutQueue;
      /// Output task busy?
      bool		fOutputBusy;
   
      /// Save a frame to a specified location
      bool saveFrameTo (framefast::framewriter* fr, 
                       namelist::iterator name, bool& keepname);
      /// Return the queue size in kB (use req. mutex)
      int queueTotal() const;
      /// IO thread
      virtual void iothread();
      /// Tests if thread is busy
      virtual bool busy() const;
   };



/** Template class for managing a list of input or output devices.
    Each device is accessed through an id number.
    
    @memo IO device manager template.
 ************************************************************************/
template <class T>
   class smart_io {
   public:
      /// smart io channel
      typedef T smartio_channel;
      /// list of io channels (type)
      typedef std::map<int, smartio_channel*> smartio_list;
      /// iterator
      typedef typename smartio_list::iterator iterator;
      /// const_iterator
      typedef typename smartio_list::const_iterator const_iterator;
   
      /// Create a list of smart IO channels
      smart_io () {
      }
      /// Destruct list of smart IO channels
      ~smart_io();
   
      /// Add a smartio channel
      bool Add (int num, const char* conf) {
         return false; }
      /// Add a smartio channel
      bool Add (int num, smartio_channel* sin);
      /// Delete a smartio channel
      bool Delete (int num);
      /// Get smartio channel
      smartio_channel* Get (int num);
      /// Clear all smartio channels
      void Clear();
   
      /// begin
      iterator begin() {
         return fIO.begin(); }
      const_iterator begin() const {
         return fIO.begin(); }
      /// end
      iterator end() {
         return fIO.end(); }
      const_iterator end() const {
         return fIO.end(); }
     /// size
      int size() {
         return fIO.size(); }
   
      /// Last message
      const char* Message () const {
         return fMsg.c_str(); }
   
   protected:
      /// list of io channels
      smartio_list	fIO;
      /// message
      std::string	fMsg;
   };

   /// Smart input device list
   typedef smart_io<smart_input> smart_ilist;
   /// Smart output device list
   typedef smart_io<smart_output> smart_olist;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// smart_io (template code)						//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
template <class T>
   smart_io<T>::~smart_io<T>()
   {
      for (iterator i = fIO.begin(); i != fIO.end(); i++) {
         delete i->second;
      }
   }

//______________________________________________________________________________
template <class T>
   void smart_io<T>::Clear()
   {
      for (iterator i = fIO.begin(); i != fIO.end(); i++) {
         delete i->second;
         i->second = 0;
      }
      fIO.clear();
   }

//______________________________________________________________________________
template <class T>
   bool smart_io<T>::Add (int num, typename smart_io<T>::smartio_channel* sin)
   {
      iterator i = fIO.find (num);
      if (i != fIO.end()) {
         fMsg = "Error: Cannot add channel";
         return false;
      }
      else {
         if (!(*sin)) {
            fMsg = sin->Message();
            delete sin;
            return false;
         }
         sin->setup();
         typename smartio_list::value_type val (num, sin);
         fIO.insert (val);
         return true;
      }
   }

//______________________________________________________________________________
template <class T>
   bool smart_io<T>::Delete (int num)
   {
      iterator i = fIO.find (num);
      if (i == fIO.end()) {
         fMsg = "Error: Cannot delete channel";
         return false;
      }
      else {
         delete i->second; 
         i->second = 0;
         fIO.erase (num);
         return true;
      }
   }

//______________________________________________________________________________
template <class T>
   typename smart_io<T>::smartio_channel* smart_io<T>::Get (int num)
   {
      iterator i = fIO.find (num);
      if (i == fIO.end()) {
         return 0;
      }
      else {
         return i->second;
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// smart_io (specialization)						//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
template <>
   bool smart_io<smart_input>::Add (int num, const char* conf);
template <>
   bool smart_io<smart_output>::Add (int num, const char* conf);

//@}

}

#endif // _LIGO_SMARTIO_H
