
// large file support for Solaris/Linux
#define _LARGEFILE_SOURCE 1
#define _FILE_OFFSET_BITS 64
#include "PConfig.h"
#include <time.h>
#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <cstring>
#include <cerrno>
#ifdef P__SOLARIS
#include <archives.h>
#elif defined (P__LINUX)
#include <tar.h>
#elif defined (P__DARWIN)
#include <tar.h>
#elif defined (P__WIN32)
#define TMAGLEN 6
#define TVERSLEN 2
#define TMAGIC "ustar"
#endif
#include <pwd.h>
#include <grp.h>
#include <fnmatch.h>
#include <cstdlib>


// large file support for Solaris (work around defines for open!)
// MUST BE BEFORE STL LIBRARIES ON Solaris 8
   inline int fileopen (const char* path, int oflag, mode_t mode)
   {
      return ::open (path, oflag, mode);
   }
#ifdef open
#undef open
#endif

#include <iostream>
#include "tapeio.hh"
#include "gmutex.hh"
#include "goption.hh"
#include "framefast/frametype.hh"
#include "framefast/frameio.hh"
#include "robotctrl.hh"

// TAR support for Linux
#if defined(P__LINUX) || defined (P__WIN32) || defined (P__DARWIN)
// tar header block constants 
#define TBLOCK  512     /* length of tar header and data blocks */
#define TNAMLEN 100     /* maximum length for tar file names */
#define TMODLEN 8       /* length of mode field */
#define TUIDLEN 8       /* length of uid field */
#define TGIDLEN 8       /* length of gid field */
#define TSIZLEN 12      /* length of size field */
#define TTIMLEN 12      /* length of modification time field */
#define TCRCLEN 8       /* length of header checksum field */

// tar header definition
union tblock {
        char dummy[TBLOCK];
        struct tar_hdr {
                char    t_name[TNAMLEN],        /* name of file */
                        t_mode[TMODLEN],        /* mode of file */
                        t_uid[TUIDLEN],         /* uid of file */
                        t_gid[TGIDLEN],         /* gid of file */
                        t_size[TSIZLEN],        /* size of file in bytes */
                        t_mtime[TTIMLEN],       /* modification time of file */
                        t_cksum[TCRCLEN],       /* checksum of header */
                        t_typeflag,
                        t_linkname[TNAMLEN],    /* file this file linked with */
                        t_magic[TMAGLEN],
                        t_version[TVERSLEN],
                        t_uname[32],
                        t_gname[32],
                        t_devmajor[8],
                        t_devminor[8],
                        t_prefix[155];
        } tbuf;
};
#endif

namespace fantom {
   using namespace std;
   using namespace gdsbase;
   using namespace thread;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }

//______________________________________________________________________________
   static string getstr (const char* p, int max)  
   {
      string s;
      for (int i = 0; i < max; i++) {
         if (p[i] == 0) {
            break;
         }
         s += p[i];
      }
      return s;
   }

//______________________________________________________________________________

#define UNAME_FIELD_SIZE   32
#define GNAME_FIELD_SIZE   32

   static thread::mutex cached_names_mux;
   static char cached_uname[UNAME_FIELD_SIZE] = "";
   static char cached_gname[GNAME_FIELD_SIZE] = "";

   static uid_t cached_uid;	/* valid only if cached_uname is not empty */
   static gid_t cached_gid;	/* valid only if cached_gname is not empty */

/* These variables are valid only if nonempty.  */
   //static char cached_no_such_uname[UNAME_FIELD_SIZE] = "";
   //static char cached_no_such_gname[GNAME_FIELD_SIZE] = "";

/* These variables are valid only if nonzero.  It's not worth optimizing
   the case for weird systems where 0 is not a valid uid or gid.  */
   static uid_t cached_no_such_uid = 0;
   static gid_t cached_no_such_gid = 0;

//______________________________________________________________________________
   static std::string uid_to_uname (uid_t uid) // MT-safe!
   {
      semlock lockit (cached_names_mux);
      char uname[UNAME_FIELD_SIZE];
      struct passwd *passwd;
   
      if (uid != 0 && uid == cached_no_such_uid) 
      {
         *uname = '\0';
         return uname;
      }
   
      if (!cached_uname[0] || uid != cached_uid)
      {
         struct passwd pwd;
         char nbuf[1024];
         if ((getpwuid_r (uid, &pwd, nbuf, sizeof (nbuf), &passwd) == 0) && 
            passwd) 
         {
            cached_uid = uid;
            strncpy (cached_uname, passwd->pw_name, UNAME_FIELD_SIZE);
            cached_uname[UNAME_FIELD_SIZE-1]=0;
         }
         else
         {
            cached_no_such_uid = uid;
            *uname = '\0';
            return uname;
         }
      }
      strncpy (uname, cached_uname, UNAME_FIELD_SIZE);
      uname[UNAME_FIELD_SIZE-1] = 0;
      return uname;
   }

//______________________________________________________________________________
   static std::string gid_to_gname (gid_t gid) // MT-safe!
   {
      semlock lockit (cached_names_mux);
      char gname[GNAME_FIELD_SIZE];
      struct group *group;
   
      if (gid != 0 && gid == cached_no_such_gid)
      {
         *gname = '\0';
         return gname;
      }
   
      if (!cached_gname[0] || gid != cached_gid)
      {
         setgrent ();		/* FIXME: why?! */
#ifndef P__WIN32
         struct group grp;
         char gbuf[1024];
#endif
         group = getgrgid (gid);
         if (
#ifndef P__WIN32
	     (getgrgid_r (gid, &grp, gbuf, sizeof (gbuf), &group) == 0) &&
#endif
	      group)
         {
            cached_gid = gid;
            strncpy (cached_gname, group->gr_name, GNAME_FIELD_SIZE);
            cached_gname[GNAME_FIELD_SIZE-1] = 0;
         }
         else
         {
            cached_no_such_gid = gid;
            *gname = '\0';
            return gname;
         }
      }
      strncpy (gname, cached_gname, GNAME_FIELD_SIZE);
      gname[GNAME_FIELD_SIZE-1] = 0;
      return gname;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// tape frame out                                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class tape_frameout : public framefast::basic_frameout {
   public:
      /// Constructor
      tape_frameout (tape_support& tsup, const char* fname);
      /// Destructor
      virtual ~tape_frameout ();
      /// Open
      virtual bool open (int flen = 0);
      /// Close
      virtual void close();
      /// Write
      virtual bool write (const char* p, int len);
      /// Scatter-gather write
      virtual bool write (const src_dest_t* s, int slen);
   
   protected:
      /// Pointer to tape support
      tape_support*	fTape;
      /// tape header
      tape_header	fHeader;
      /// Data block
      char		fBlock[TBLOCK];
      /// Length of data
      int		fLen;
   };

//______________________________________________________________________________
   tape_frameout::tape_frameout (tape_support& tsup, 
                     const char* fname)
   : fTape (&tsup), fLen (0) {
      fHeader.defaults();
      fHeader.fFilename = fname ? fname : "INVALID";
   }

//______________________________________________________________________________

   tape_frameout::~tape_frameout ()
   {
   }

//______________________________________________________________________________
   bool tape_frameout::open (int flen)
   {
      //cerr << "open frame " << fHeader.fFilename << " size = " << flen << endl;
      // set header
      fHeader.fSize = flen;
      fTape->setHeader (fHeader);
   
      // open device if necessary
      fLen = 0;
      if (!fTape->open (O_WRONLY | O_CREAT | O_TRUNC)) {
         return false;
      }
   
      // write header
      if (!fHeader.write (fTape->fData + fTape->fDatapos * TBLOCK)) {
         return false;
      }
      fTape->fDatapos++;
   
      return true;
   }

//______________________________________________________________________________
   void tape_frameout::close()
   {
      // flush last block
      if (fLen) {
         //cerr << "flush last tape block" << endl;
         if (fLen < TBLOCK) {
            memset (fBlock + fLen, 0, TBLOCK - fLen);
         }
         if (fTape->checkblk()) {
            memcpy (fTape->fData + fTape->fDatapos * TBLOCK, 
                   fBlock, TBLOCK);
            fTape->fDatapos++;
         }
      }
   
      // increase file position
      fTape->next();
   }

//______________________________________________________________________________
   bool tape_frameout::write (const char* p, int size)
   {
      //cerr << "Write " << size << "B" << endl;
   
      // write unfinishe block first
      if (fLen) {
         //cerr << "unfinished = " << fLen << endl;
         // make sure we have at least on block
         if (!fTape->checkblk()) {
            cerr << "no space left" << endl;
            return false;
         }
         memcpy (fTape->fData + fTape->fDatapos * TBLOCK, 
                fBlock, fLen);
      }
      // write data
      int len = 0;
      int left = 0; // left over at the end which doesn't fill a block
      while (len < size) {
         // make sure we have at least on block
         if (!fTape->checkblk()) {
            cerr << "no space left 2" << endl;
            return false;
         }
         // copy data
         int blen = (fTape->fSize - fTape->fDatapos) * TBLOCK - fLen;
         if (blen > size - len) {
            blen = size - len;
            left = (blen + fLen) % TBLOCK;
         }
         //cerr << "write " << blen << "B" << endl;
         memcpy (fTape->fData + fTape->fDatapos * TBLOCK + fLen, 
                p + len, blen);
         len += blen;
         fTape->fDatapos += (blen + fLen) / TBLOCK;
         fLen = 0; // was only needed to account for unfinished block
      }
   
      // left over?
      if (left) {
         //cerr << "left over = " << left << endl;
         memcpy (fBlock, fTape->fData + fTape->fDatapos * TBLOCK, left);
         fLen = left;
      }
      //cerr << "Write done" << endl;
      fSofar += size;
      return true;
   }

//______________________________________________________________________________
   bool tape_frameout::write (const src_dest_t* s, int slen)
   {
      return basic_frameout::write (s, slen); 
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// tape header                                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   void tape_header::defaults()
   {
      fFilename = "";
      fMode = 0100666;
      fUid = ::getuid();
      fUName = uid_to_uname (fUid);
      fGid = ::getgid();
      fGName = gid_to_gname (fGid);
      fSize = 0;
      fModTime = time(0);
      fType = 0;
      fLink = "";
      fMagic = TMAGIC;
      fVersion = 0;
      fDevMajor = 0;
      fDevMinor = 0;
   }

//______________________________________________________________________________
   bool tape_header::read (const char* p, bool debug)
   {
      // get file header
      tblock header;
      memcpy (&header, p, sizeof (header));
      // verify checksum
      int cksum = strtol (header.tbuf.t_cksum, 0, 8);
      strncpy (header.tbuf.t_cksum, "        ", 8);
      int cksum1 = 0;
      int cksum2 = 0;
      for (int i = 0; i < TBLOCK; i++) {
         cksum1 += ((unsigned char*) header.dummy)[i];
         cksum2 += ((char*) header.dummy)[i];
      }
      if ((cksum1 != cksum) && (cksum2 != cksum)) {
         if (debug) {
            cerr << "check sum faild: " << cksum << 
               " comapared against " << cksum1 << " and " << cksum2;
         }
         return false;
      }
      // get rest of header
      fMagic = getstr (header.tbuf.t_magic, TMAGLEN);
      fMode = strtol (getstr (header.tbuf.t_mode, TMODLEN).c_str(), 0, 8);
      fUid = strtol (getstr (header.tbuf.t_uid, TUIDLEN).c_str(), 0, 8);
      fGid = strtol (getstr (header.tbuf.t_gid, TGIDLEN).c_str(), 0, 8);
      fSize = strtol (getstr (header.tbuf.t_size, TSIZLEN).c_str(), 0, 8);
      fModTime = strtol (getstr (header.tbuf.t_mtime, TTIMLEN).c_str(), 0, 8);
      if (header.tbuf.t_typeflag == 0) {
         fType = 0;
      }
      else {
         fType = isdigit (header.tbuf.t_typeflag) ? 
            header.tbuf.t_typeflag - '0' : -1;
      }
      fLink = getstr (header.tbuf.t_linkname, TNAMLEN);
      // ustar format
      if (strncmp (fMagic.c_str(), TMAGIC, TMAGLEN) == 0) {
         fFilename = getstr (header.tbuf.t_prefix, 155) 
            + getstr (header.tbuf.t_name, TNAMLEN);
         fVersion = strtol (getstr (header.tbuf.t_version, TVERSLEN).c_str(), 0, 8);
         fUName = getstr (header.tbuf.t_uname, 32);
         fGName = getstr (header.tbuf.t_gname, 32);
         fDevMajor = strtol (getstr (header.tbuf.t_devmajor, 8).c_str(), 0, 8);
         fDevMinor = strtol (getstr (header.tbuf.t_devminor, 8).c_str(), 0, 8);
      }
      // old tar format
      else {
         fFilename = getstr (header.tbuf.t_name, TNAMLEN);
      }
      if (debug) {
         time_t tmod = fModTime;
         cerr << "TAR/USTAR HEADER" << endl;
         cerr << "name    = " << fFilename << endl;
         cerr << "mode    = " << oct << fMode << dec << endl;
         cerr << "uid     = " << fUid << endl;
         cerr << "gid     = " << fGid << endl;
         cerr << "size    = " << fSize << endl;
         cerr << "mtime   = " << ctime (&tmod);
         cerr << "chksum  = " << cksum << endl;
         cerr << "type    = " << fType << endl;
         cerr << "link    = " << fLink << endl;
         cerr << "magic   = " << fMagic << endl;
         if (fMagic == TMAGIC) {
            cerr << "version = " << fVersion << endl;
            cerr << "uname   = " << fUName << endl;
            cerr << "gname   = " << fGName << endl;
            cerr << "devmaj  = " << fDevMajor << endl;
            cerr << "devmin  = " << fDevMinor << endl;
         }
      }
      return (fType >= 0);
   }

//______________________________________________________________________________
   bool tape_header::write (char* p, bool debug)
   {
      static int fInvalid = 0;
   
      // set file header
      tblock header;
      memset (&header, 0, sizeof (tblock));
      strncpy (header.tbuf.t_cksum, "        ", 8);
      strncpy (header.tbuf.t_magic, fMagic.c_str(), TMAGLEN);
      sprintf (header.tbuf.t_mode, "%07o", fMode);
      sprintf (header.tbuf.t_uid, "%07o", fUid);
      sprintf (header.tbuf.t_gid, "%07o", fGid);
      sprintf (header.tbuf.t_size, "%011o", fSize);
      sprintf (header.tbuf.t_mtime, "%011lo", fModTime);
      header.tbuf.t_typeflag = fType + '0';
      strncpy (header.tbuf.t_linkname, fLink.c_str(), TNAMLEN);
      // ustar extensions
      if (fMagic == TMAGIC) {
         if (fFilename.size() <= TNAMLEN) {
            strncpy (header.tbuf.t_name, fFilename.c_str(), TNAMLEN);
         }
         else {
            // break at a '/' if possible
            const char* p = fFilename.c_str();
            const char* pos = p;
            do {
               pos = strchr (pos, '/');
            } while (pos && (strlen (pos) <= TNAMLEN));
            int ofs = fFilename.size() - TNAMLEN;
            if (pos && (strlen (pos) <= TNAMLEN) &&
               ((int) (pos - p) <= 155)) {
               ofs = (int) (pos - p);
            }
            if (ofs <= 155) {
               strncpy (header.tbuf.t_name, p + ofs, TNAMLEN);
               strncpy (header.tbuf.t_prefix, p, ofs);
            }
            else {
               sprintf (header.tbuf.t_name, "INVALID%d", fInvalid++);
            }
         }
         char buf[4];
         sprintf (buf, "%02d", fVersion);
         strncpy (header.tbuf.t_version, buf, TVERSLEN);
         strncpy (header.tbuf.t_uname, fUName.c_str(), 32);
         strncpy (header.tbuf.t_gname, fGName.c_str(), 32);
         if (fDevMajor) {
            sprintf (header.tbuf.t_devmajor, "%07o", fDevMajor);
            sprintf (header.tbuf.t_devminor, "%07o", fDevMinor);
         }
      }
      // old tar format
      else {
         if (fFilename.size() <= TNAMLEN) {
            strncpy (header.tbuf.t_name, fFilename.c_str(), TNAMLEN);
         }
         else {
            sprintf (header.tbuf.t_name, "INVALID%d", fInvalid++);
         }
      }
   
      // calculate checksum
      int cksum1 = 0;
      int cksum2 = 0;
      for (int i = 0; i < TBLOCK; i++) {
         cksum1 += ((unsigned char*) header.dummy)[i];
         cksum2 += ((char*) header.dummy)[i];
      }
      sprintf (header.tbuf.t_cksum, "%07o", cksum1);
   
      // write header
      memcpy (p, &header, sizeof (header));
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// tape_support		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   tape_support::tape_support (const char* devicename, const char* conf) 
   : fDebug (0), fBlock (256), fOpen (false), fMode (0), fd (-1), 
   fData(0), fRobotDriver (0)
   {
      init();
      setDevicename (devicename); 
      setConf (conf);
   }

//______________________________________________________________________________
   tape_support::~tape_support()
   {
      close();
      if (fData) delete [] fData;
      if (fRobotDriver) delete fRobotDriver;
   }

//______________________________________________________________________________
   void tape_support::init ()
   {
      close();
      if (fData) {
         delete [] fData;
         fData = 0;
      }
      if (fRobotDriver) {
         delete fRobotDriver;
         fRobotDriver = 0;
      }
      fEof = false;
      fEofTar = false;
      fEofTape = false;
      fFilePos = -1;
      fArchPos = 0;
      fTapePos = 0;
      fFileStart = 0;
      fArchNum = -1;
      fFileNum = -1;
      fDir = dir_support ("");
      fDir.setCINFlag (false);
      fFileMatch = "";
      fRobot = "";
   }

//______________________________________________________________________________
   void tape_support::setDevicename (const char* devicename) 
   {
      if (devicename == 0) {
         fDevicename = "";
         fIsMT = false;
      }
      else {
         fDevicename = trim (devicename);
         fIsMT = strncmp (fDevicename.c_str(), "/dev/rmt", 8) == 0;
      }
   }

//______________________________________________________________________________
   void tape_support::setConf (const char* conf) 
   {
      if (!conf) {
         return;
      }
      option_string opts ("tape", conf, "p:a:n:d:f:r:");
      string s;
      // filepos
      if (opts.getOpt ('p', s)) {
         fFileStart = atoi (s.c_str());
      }
      else {
         fFileStart = 0;
      }
      // archives per tape
      if (opts.getOpt ('a', s)) {
         fArchNum = atoi (s.c_str());
      }
      else {
         fArchNum = -1;
      }
      // filenum
      if (opts.getOpt ('n', s)) {
         fFileNum = atoi (s.c_str());
      }
      else {
         fFileNum = -1;
      }
      // dir name
      if (opts.getOpt ('d', s)) {
         fDir.setDirname (s.c_str());
      }
      else {
         fDir.setDirname ("");
      }
      // file(s)
      if (opts.getOpt ('f', s)) {
         fFileMatch = s.c_str();
      }
      else {
         fFileMatch = "";
      }
      // robot
      if (opts.getOpt ('r', s)) {
         fRobot = s.c_str();
      }
      else {
         fRobot = "";
      }
   }

//______________________________________________________________________________
   void tape_support::setBlocking (int blk)
   {
      if (fData) {
         delete [] fData;
         fData = 0;
         fDatapos = 0;
      }
      fBlock = blk <= 0 ? 256 : blk;
   }

//______________________________________________________________________________
   int tape_support::readFile (char*& data, int max)
   {
      // open device if necessary
      if (!open (O_RDONLY)) {
         return -1;
      }
   
      // get header
      if (!fHeader.read (fData + fDatapos * TBLOCK, fDebug)) {
         return -1;
      }
      int size = fHeader.fSize;
      fDatapos++;
      // allocate data array for file if necessary
      if (data == 0) {
         data = new (nothrow) char [size+1];
         if (!data) {
            return -1;
         }
         max = size;
      }
      // read file
      int len = 0;
      while (len < size) {
         // read blocks
         if (!checkblk()) {
            return -1;
         }
         // copy them into data array
         int bdiff = fSize - fDatapos;
         int bsize = bdiff * TBLOCK;
         if (bsize + len > size) {
            bsize = size - len;
            bdiff = (bsize + TBLOCK - 1) / TBLOCK;
         }
         int csize = bsize;
         if (len + csize > max) {
            csize = max - len;
         }
         if (csize > 0) {
            memcpy (data + len, fData + fDatapos * TBLOCK, csize);
         }
         len += bsize;
         fDatapos += bdiff;
      }
      // check eof
      checkeoftar();
   
      return len;
   }

//______________________________________________________________________________
   int tape_support::nextRegularFile (char*& data, int max)
   {
      bool owndata = (data == 0);
      int size = 0;
      bool match;
      do {
         if (owndata && data) {
            delete [] data;
            data = 0;
         }
         size = readFile (data, max);
         // test: file type
         match = true;
         if ((fHeader.fType != 0) || (fHeader.fSize == 0)) {
            match = false;
         }
         // test: file match
         if (match) {
            match = fFileMatch.empty() || 
               fnmatch (fFileMatch.c_str(), fHeader.fFilename.c_str(), 0);
         }
         // test: file start position
         if (match) {
            match = next();
         }
      } while (!eof() && !match);
      return match ? size : -1;
   }

//______________________________________________________________________________
   framefast::basic_frame_storage* tape_support::readFrame () 
   {
      char* data = 0;
      int size = nextRegularFile (data, 0);
      if (!data) {
         return 0;
      }
      else {
         framefast::basic_frame_storage* fr = new (nothrow) 
            framefast::memory_frame_storage (data, size, true);
         if (!fr) {
            delete [] data;
         }
         else {
            fr->setname (fHeader.fFilename.c_str());
         }
         return fr;
      }
   }

//______________________________________________________________________________
   bool tape_support::writeFile (const tape_header& header,
                     const char* data, int size)
   {
      // set header
      setHeader (header);
      setDeviceext (header.fFilename.c_str());
   
      // open device if necessary
      if (!open (O_WRONLY | O_CREAT | O_TRUNC)) {
         return false;
      }
   
      // write header
      if (!fHeader.write (fData + fDatapos, fDebug)) {
         return false;
      }
      fDatapos++;
   
      // write data
      int len = 0;
      while (len < size) {
         // make sure we have at least on block
         if (!checkblk()) {
            return false;
         }
         int blen = (fSize - fDatapos) * TBLOCK;
         int pad = 0;
         if (blen < size - len) {
            blen = size - len;
            pad = (blen % TBLOCK) ? TBLOCK - blen % TBLOCK : 0;
         }
         memcpy (fData + fDatapos * TBLOCK, data + len, blen);
         if (pad) memset (fData + fDatapos * TBLOCK + blen, 0, pad);
         len += blen;
         fDatapos += (blen + pad) / TBLOCK;
      }
      // move to next file
      next();
   
      return true;
   }

//______________________________________________________________________________
   framefast::basic_frameout* tape_support::getWriter (const char* fname)
   {
      if (fname) {
         string fullname;
         if (!fDir.setNextFilename (fullname, fname)) {
            fullname = fname ? fname : "";
         }
         setDeviceext (fname);
         return new (nothrow) tape_frameout (*this, fullname.c_str());
      }
      else {
         return 0;
      }
   }

//______________________________________________________________________________
   std::string tape_support::getDevicename() const
   {
      string s (fDevicename);
      if (fIsMT || (fFileNum <= 0) || fDeviceext.empty() || 
         fDevicename.empty() ||
         (fDevicename[fDevicename.size()-1] != '/')) {
      }
      else {
         char buf[64];
         sprintf (buf, ".n%d", fFileNum);
         s += fDeviceext + buf;
      }
      return s;
   }

//______________________________________________________________________________
   void tape_support::setDeviceext (const char* devext)
   {
      if (!devext || !*devext) {
         fDeviceext = "";
      }
      // get rid of dirs
      const char* p = devext;
      const char* pp;
      while ((pp = strchr (p, '/'))) {
         p = pp;
      }
      fDeviceext = p;
   }

//______________________________________________________________________________
   bool tape_support::open (int mode)
   {
      if (mode & O_RDWR) {
         if (fDebug) cerr << "read/write not supported for " << 
               fDevicename << endl;
         return false;
      }
      // open device if necessary
      if (!fOpen) {
         // load tape first time around
         if (fIsMT && !fRobot.empty() && (fArchPos == 0)) {
            if (!nexttape()) {
               return false;
            }   
         }
         // before writing: check if we have to move to a new tape
         fArchPos++;
         if (fMode & O_WRONLY) {
            // end of tape (write)
            if (fEofTape) {
               if (!nexttape()) {
                  return false;
               }
            }
         }
         // open device
         fd = fileopen (getDevicename().c_str(), mode, 0666);
         if (fd < 0) {
            cerr << "open for " << fDevicename << " failed with error " << errno << endl;
            return false;
         }
         fOpen = true;
         fMode = mode;
         fDatapos = 0;
         fSize = (mode == O_RDONLY) ? 0 : fBlock;
         fEofTar = false;
      }
      // make sure we have at least one block
      if (checkblk()) {
         return true;
      }
      // an empty file marks the end of tape; close it here
      close();
      // No more data: error if write
      if (fMode & O_WRONLY) {
         return false;
      }
      // move to next tape if read
      if (nexttape()) {
         // try it again
         return open (mode);
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   void tape_support::close()
   {
      if (fOpen) {
         // flush
         flush();
         // close
         ::close (fd);
         fOpen = false;
         fMode = 0;
         fd = -1;
         cerr << "end of tar file" << endl;
         fEofTar = true;
         if (!fIsMT) {
            fEof = true;
         }
      }
   }

//______________________________________________________________________________
   void tape_support::flush()
   {
      if (fOpen) {
         if ((fMode & O_WRONLY) && fDatapos) {
            if (fDatapos < fSize) {
               memset (fData + fDatapos * TBLOCK, 0, 
                      (fSize - fDatapos) * TBLOCK);
            }
            fDatapos = fSize;
            checkblk();
         }
      }
   }

//______________________________________________________________________________
   bool tape_support::next()
   {
      // if (!fOpen) {
         // return false;
      // }
      // when writing
      if (fMode & O_WRONLY) {
         fFilePos++;
         // end of archive/tar file
         if ((fFileNum > 0) && ((fFilePos+1) % fFileNum == 0)) {
            cerr << "end of tar" << endl;
            close();
            if ((fArchNum > 0) && (fArchPos % fArchNum == 0)) {
               fEofTape = true;
            }
         }
      }
      // when reading
      else {
         fFilePos++;
         // test: file number > start
         if (fFilePos < fFileStart) {
            return false;
         }
         // test: file number <= last
         if ((fFileNum >= 0) && (fFilePos >= fFileStart + fFileNum)) {
            close();
            cerr << "end of all" << endl;
            fEofTape = true;
            fEof = true;
            return false;
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool tape_support::nexttape()
   {
      // check if robot is set or done
      if (fEof || !fIsMT || fRobot.empty()) {
         return false;
      }
      // make new robot driver if necessary
      if (fRobotDriver == 0) {
         fRobotDriver = new (nothrow) robot_ctrl 
            (getDevicename().c_str(), fRobot.c_str());
         // check for error
         if (!fRobotDriver || (fRobotDriver->eof())) {
            fEofTape = true;
            fEof = true;
            return false;
         }
      }
      // load new tape
      fTapePos++;
      cerr << "load new tape..." << endl;
      bool success = fRobotDriver->next();
      if (success) {
         cerr << "load new tape done" << endl;
         fEofTape = false;
         return true;
      }
      else {
         fEofTape = true;
         fEof = true;
         cerr << "end of all" << endl;
         return false;
      }
   }

//______________________________________________________________________________
   bool tape_support::checkblk ()
   {
      // allocate data block
      if (!fData) {
         fData = new (nothrow) char [fBlock * TBLOCK];
         if (!fData) {
            return false;
         }
         fDatapos = 0;
      }
      // check avail.
      if (fDatapos >= fSize) {
         // write next
         if (fMode & O_WRONLY) {
            if (writeblk () <= 0) {
               return false;
            }
         }
         // read next
         else {
            if (readblk () <= 0) {
               return false;
            }
         }
         fDatapos = 0;
      }
      return true;
   }

//______________________________________________________________________________
   void tape_support::checkeoftar ()
   {
      // Two empty blocks mark eof
      if (!checkblk()) {
         return;
      }
      bool eof = true;
      char* start = fData + fDatapos * TBLOCK;
      for (int i = 0; i < TBLOCK; i++) {
         if (start[i] != 0) eof = false;
      }
      if (eof) {
         fDatapos++;
         if (!checkblk()) {
            return;
         }
         char* start = fData + fDatapos * TBLOCK;
         for (int i = 0; i < TBLOCK; i++) {
            if (start[i] != 0) eof = false;
         }
         if (eof) {
            fDatapos++;
            close();
         }
      }
   }

//______________________________________________________________________________
   int tape_support::readblk ()
   {
      if (!fOpen || !fData) {
         return 0;
      }
      ssize_t size = ::read (fd, fData, fBlock * TBLOCK);
      fSize = (size < 0) ? 0 : size / TBLOCK;
      return fSize;
   }

//______________________________________________________________________________
   int tape_support::writeblk ()
   {
      if (!fOpen || !fData) {
         return 0;
      }
      ssize_t size = ::write (fd, fData, fBlock * TBLOCK);
      if (fDebug) cerr << "write a block of size " << size << endl;
      return size;
   }


}
