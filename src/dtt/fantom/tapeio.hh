/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: tapeio							*/
/*                                                         		*/
/* Module Description: tape support for smartio				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 22Nov00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: tapeio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_TAPEIO_H
#define _LIGO_TAPEIO_H

#include <string>
#include "dirio.hh"

namespace fantom {


   class robot_ctrl;

/** @name Tape IO
    This header defines support methods for tape IO. It uses the POSIX
    USTAR format and should be comaptible wih most tar commands.
   
    @memo Tape Input/Output
    @author Written November 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** Tape/tar header. This class implements a USTAR header.
    
    @memo Tape/tar header.
 ************************************************************************/
   class tape_header {
   public:
      /// Filename
      std::string 	fFilename;
      /// Mode
      int		fMode;
      /// user id
      int		fUid;
      /// user name
      std::string	fUName;
      /// group id
      int		fGid;
      /// group name
      std::string	fGName;
      /// File size
      int		fSize;
      /// Modification time
      unsigned long	fModTime;
      /// Type
      int		fType;
      /// Link name
      std::string	fLink;
      /// Magic id
      std::string	fMagic;
      /// version
      int		fVersion;
      /// Device major number
      int		fDevMajor;
      /// Device major number
      int		fDevMinor;
   
      /// initialize header
      tape_header ()
      : fMode (0100666), fUid (0), fUName ("root"), fGid (0), 
      fGName ("root"), fSize (0), fModTime (0), fType (0),
      fLink (""), fMagic ("ustar"), fVersion (0), 
      fDevMajor (0), fDevMinor (0) {
      }
      /// set default values
      void defaults();
   
      /// Read header from block
      bool read (const char* p, bool debug = false);
      /// Write header to block
      bool write (char* p, bool debug = false);
   };


/** Tape IO class. This is a support class for the smart tape input
    and smart tape output device classes. If the device is a magnetic 
    tape it should be of the format "/dev/rmt/0n". If the device is 
    a tar archive on disk, the name represents a file name.
    \begin{verbatim}
    tape://'medium' [-p 'pos] [-n 'num'] [-f 'files'] [-d 'dir']
    The following options are supported:
    -p 'filepos': file position to start (read only).
    -n 'filenum': number of files to read (read only).
    -f 'files': file name or wildcard (read only).
    -d 'directory': directory name to use; can contain [#filenum] 
                    (write only).
    \end{verbatim}
    @memo Tape IO support class.
 ************************************************************************/
   class tape_support : public iosupport {
      /// friend class
      friend class tape_frameout;
   
   public:
      /// create tape support
      explicit tape_support (const char* devicename = 0, 
                        const char* conf = 0);
      /// Destructor
      virtual ~tape_support();
      /// Initialization
      void init();
   
      /// Set device/file name
      void setDevicename (const char* devicename = 0);
      /// Set configuration
      void setConf (const char* conf);
   
      /// Set the blocking factor
      void setBlocking (int blk = 256);
      /// Get the blocking factor
      int getBlocking () const {
         return fBlock; }
      /// get the current file position
      int getFilepos () const {
         return fFilePos; }
      /// get the start file position
      int getFileStart () const {
         return fFileStart; }
      /// get the number of archives per tape
      int getArchiveNum () const {
         return fArchNum; }
      /// get the number of files
      int getFileNum () const {
         return fFileNum; }
      /// get file specifictaion
      const char* getFileSpec () const {
         return fFileMatch.c_str(); }
      /// get dir specifictaion
      const char* getDirSpec () const {
         return fDir.getDirname(); }
      /// get robot specifictaion
      const char* getRobotSpec () const {
         return fRobot.c_str(); }
   
      /** Read a file from tape; caller owns returned data if 
          data array is set to zero before call! The maximum
          is ignored in this case. This function may return
          directories and symbolic links, etc.
          @param data pointer to data
          @param max maximum length of data array
          @return length of data array
        */
      int readFile (char*& data, int max);
      /// Read the next regular file from tape.
      int nextRegularFile (char*& data, int max);
      /// Read next frame into buffer
      virtual framefast::basic_frame_storage* readFrame ();
      /// eof file?
      virtual bool eof() const {
         return fEof; }
      /// Write a file to tape
      bool writeFile (const tape_header& header, const char* data, 
                     int size);
      /// get tape writer
      virtual framefast::basic_frameout* getWriter (const char* fname);
   
      /// Get the header
      const tape_header* getHeader () const {
         return &fHeader; }
      /// Set the header
      void setHeader (const tape_header& header) {
         fHeader = header; }
      /// Set debug level header
      void setDebug (int debug = 0) {
         fDebug = debug; }
      /// Get device name
      std::string getDevicename() const;
      /// Set device extension
      void setDeviceext (const char* devext);  
   
   protected:
      /// debug level
      int		fDebug;
      /// Device name
      std::string	fDevicename;
      /// Device extension
      std::string	fDeviceext;
      /// Is magnetic tape drive?
      bool		fIsMT;
      /// blocking factor
      int		fBlock;
      /// device open?
      bool		fOpen;
      /// device mode (O_RDONLY or O_WRONLY)
      int		fMode;
      /// device decriptor
      int		fd;
      /// end of file?
      bool		fEof;
      /// end of tar file?
      bool		fEofTar;
      /// end of tape?
      bool		fEofTape;
      /// Header
      tape_header	fHeader;
      /// file position
      int		fFilePos;
      /// Archive/tar file position
      int		fArchPos;
      /// Tape position
      int		fTapePos;
      /// File position to start
      int		fFileStart;
      /// Number of files to read
      int		fFileNum;
      /// Number of archives per tape
      int		fArchNum;
      /// Directory support
      dir_support	fDir;
      /// File matching pattern
      std::string	fFileMatch;
      /// Robot specification
      std::string	fRobot;
   
      /// open device
      bool open (int mode);
      /// Close device/file(s)
      void close();
      /// flush block buffer
      void flush();
      /// Move to next file
      bool next();
      /// Move to next tape
      bool nexttape();
      /// check that we have at least one block available
      bool checkblk ();
      /// check if we are at eof tar file?
      void checkeoftar ();
      /// Read a set of blocks
      int readblk ();
      /// Write a set of blocks
      int writeblk ();
   
   private:
      /// Data blocks
      char*		fData;
      /// Data size (in number of blocks)
      int		fSize;
      /// data position (in number of blocks)
      int		fDatapos;
      /// Robot support driver
      robot_ctrl*	fRobotDriver;
   };


//@}

}

#endif // _LIGO_TAPEIO_H
