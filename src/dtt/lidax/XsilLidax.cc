/* version $Id: XsilLidax.cc 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
#include <time.h>
#include <string>
#include <strings.h>
#include <fstream>
#include "XsilLidax.hh"
#include "XsilStd.hh"
#include "fchannel.hh"
#include "TLGEntry.hh"
#include <TGClient.h>
#include <TGMsgBox.h>
#include <TVirtualX.h>


namespace lidax {
   using namespace std;
   using namespace ligogui;
   using namespace xml;
   using namespace dfm;


   static const char* const gSaveAsTypes[] = { 
   "LIGO light weight", "*.ldx",
   "LIGO light weight", "*.xml",
   "All files", "*",
   0, 0 };


   static const char* const gOpenTypes[] = { 
   "LIGO light weight", "*.ldx",
   "LIGO light weight", "*.xml",
   "All files", "*",
   0, 0 };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// output operator                                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void write (std::ostream& os, const selserverentry& e, int count,
              bool clnt = false)
   {
      // server/client name
      char name[256];
      sprintf (name, clnt ? "Client[%i]" : "Server[%i]", count);
      os << xsilParameter<const char*> (name, 
                           (const char*)e.getName()) << endl;
      // UDN
      sprintf (name, "UDN[%i]", count);
      if (e.getUDN().begin() == e.getUDN().end()) {
         os << xsilParameter<const char*> (name, "") << endl;
         return;
      }
      else {
         os << xsilParameter<const char*> (name, (const char*)
                              e.getUDN().begin()->first) << endl;
      }
      // channels
      int cnum = 0;
      for (fantom::const_chniter c = e.channels().begin(); 
          c != e.channels().end(); ++c, ++cnum) {
         sprintf (name, "Channel[%i][%i]", count, cnum);
         os << xsilParameter<const char*> (name, c->Name()) << endl;
         if (c->Rate()) {
            sprintf (name, "Rate[%i][%i]", count, cnum);
            os << xsilParameter<double> (name, c->Rate()) << endl;
         }
      }
      // format
      if (clnt) {
         sprintf (name, "Format[%i]", count);
         os << xsilParameter<const char*> (name, 
                              e.format().c_str()) << endl;
      }
   }

//______________________________________________________________________________
   std::ostream& operator<< (std::ostream& os, const LidaxParam& ldx)
   {
      const selservers& srvr = ldx.fDacc->sel();
      const selservers& clnt = ldx.fDacc->dest();
      // Times
      os << xsilDataBegin ("Lidax", "Fantom");
      os << xsilTime ("Start", srvr.selectedTime().getS(),
                     srvr.selectedTime().getN()) << endl;
      os << xsilParameter<double> ("Duration", 
                           srvr.selectedDuration()) << endl;
      // Source
      int count = 0;
      if (srvr.isMultiple()) {
         for (const_selserveriter i = srvr.begin(); i != srvr.end(); ++i) {
            write (os, *i, count++);
         }
      }
      else {
         write (os, srvr.selectedS(), count++);
      }
      // Monitors
      if (ldx.fDMTEnable) {
         os << xsilParameter<bool> ("MonitorKill", ldx.fDMTKill) << endl;
         int count = 0;
         for (const_monitor_iter i = ldx.fMonitors.begin(); 
             i != ldx.fMonitors.end(); ++i, ++count) {
            char name[256];
            sprintf (name, "MonitorName[%i]", count);
            os << xsilParameter<const char*> (name, i->fName.c_str()) << endl;
            sprintf (name, "MonitorArg[%i]", count);
            os << xsilParameter<const char*> (name, i->fArgs.c_str()) << endl;
            sprintf (name, "MonitorData[%i]", count);
            os << xsilParameter<const char*> (name, i->fUDN.c_str()) << endl;
         }
      }
      // Destination & log
      if (ldx.fOutEnable) {
         if (clnt.isMultiple()) {
            for (const_selserveriter i = clnt.begin(); i != clnt.end(); ++i) {
               write (os, *i, count++, true);
            }
         }
         else {
            write (os, clnt.selectedS(), count++, true);
         }
         os << xsilParameter<bool> ("Log", ldx.fLogOn) << endl;
         os << xsilParameter<const char*> ("Logfile", ldx.fLog.c_str()) << endl;
         os << xsilParameter<bool> ("Web", ldx.fWebOn) << endl;
         os << xsilParameter<const char*> ("Webfile", ldx.fWeb.c_str()) << endl;
         os << xsilParameter<bool> ("Email", ldx.fEmailOn) << endl;
         os << xsilParameter<const char*> ("EmailAddress", ldx.fEmail.c_str()) << endl;
         os << xsilParameter<bool> ("Progress", ldx.fDlgProgress) << endl;
      }
      os << xsilDataEnd<float> ();
      return os;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerLidax::LidaxList		                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class LidaxEntry {
   public:
      // channel entry type
      typedef map <int, fantom::channelentry> chnentry;
   
      // is valid
      bool		fValid;
      // is client?
      bool		fClnt;
      // server/lient name
      string		fName;
      // UDN name
      string		fUDN;
      // Format
      string		fFormat;
      // channel name & rate
      chnentry		fChn;
      // Constructor
      LidaxEntry () : fValid (false) {
      }
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerLidax::LidaxList		                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class xsilHandlerLidax::LidaxList : public map <int, LidaxEntry> {
   public:
      LidaxList() {
      }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerLidax                                                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandlerLidax::xsilHandlerLidax (LidaxParam& ldx)
   : fLdx (ldx)
   {
      fList = new (nothrow) LidaxList;
      fLdx.fDacc->sel().clear();
      fLdx.fDacc->dest().clear();
   }

//______________________________________________________________________________
   xsilHandlerLidax::~xsilHandlerLidax()
   {
      if (!fList) {
         return;
      }
      // add servers / clients form list
      for (LidaxList::iterator i = fList->begin(); i != fList->end(); ++i) {
         // check if valid entry
         if ((i->first < 0) || !i->second.fValid ||
            i->second.fName.empty() || i->second.fUDN.empty()) {
            continue;
         }
         // make channel list
         fantom::channellist chns;
         for (LidaxEntry::chnentry::iterator c = i->second.fChn.begin(); 
             c != i->second.fChn.end(); ++c) {
            if (c->first >= 0) {
               chns.push_back (c->second);
            }
         }
         fLdx.fDacc->addEntry (i->second.fClnt, i->second.fName,
                              i->second.fUDN, chns, i->second.fFormat);
      
         // dataserver* ds = fLdx.fDacc->get (i->second.fName);
         // dataservername dname (i->second.fName.c_str());
         // if (dname.getType() == st_NDS) {
            // dataserver dsrv (st_NDS, dname.getAddr());
            // fLdx.fDacc->insert (dname.get(), dsrv);
            // ds = fLdx.fDacc->get (i->second.fName);
            // if (ds) ds->lookupUDNs();
         // }
         // if (!ds) {
            // continue;
         // }
      //    // add server/udn
         // UDNList ul;
         // UDN u (i->second.fUDN.c_str());
         // ul[u] = UDNInfo();
         // selserverentry e (dataservername (i->second.fName.c_str()), ul);
      //    // set channels
         // e.setChannels (chns);
      //    // set format
         // if (i->second.fClnt) {
            // e.selectFormat (i->second.fFormat.c_str());
         // }
         // selservers& sel = 
            // (i->second.fClnt) ? fLdx.fDacc->dest() : fLdx.fDacc->sel();
      //    // add udn to data server if necessary
         // switch (ds->getType()) {
            // case st_File:
            // case st_Tape:
            // case st_SM:
               // {
                  // ds->insert (u);
                  // if (!i->second.fClnt) ds->lookupUDN (u);
                  // break;
               // }
            // default:
               // {
                  // break;
               // }
         // }
      //    // add selected server/udn
         // sel.add (e);
      }
      delete fList;
      // check monitor list for empty entries
      for (monitor_iter i = fLdx.fMonitors.begin(); 
          i != fLdx.fMonitors.end(); ) {
         if (i->fName.empty()) {
            fLdx.fMonitors.erase (i);
         }
         else {
            ++i; 
         }
      }
   }

//______________________________________________________________________________
   bool xsilHandlerLidax::HandleParameter (const std::string& name,
                     const attrlist& attr,
                     const bool& p, int N)
   {
      if (strcasecmp (name.c_str(), "Log") == 0) {
         fLdx.fLogOn = p;
         return true; 
      }
      else if (strcasecmp (name.c_str(), "Web") == 0) {
         fLdx.fWebOn = p;
         return true; 
      }
      else if (strcasecmp (name.c_str(), "Email") == 0) {
         fLdx.fEmailOn = p;
         return true; 
      }
      else if (strcasecmp (name.c_str(), "Progress") == 0) {
         fLdx.fDlgProgress = p;
         return true; 
      }
      else if (strcasecmp (name.c_str(), "MonitorKill") == 0) {
         fLdx.fDMTKill = p;
         return true;
      }
      return false;
   }

//______________________________________________________________________________
   bool xsilHandlerLidax::HandleParameter (const std::string& name,
                     const attrlist& attr,
                     const int& p, int N)
   {
      return false;
   }

//______________________________________________________________________________
   bool xsilHandlerLidax::HandleParameter (const std::string& name,
                     const attrlist& attr,
                     const double& p, int N)
   {
      int i1;
      int i2;
      string n;
      if (strcasecmp (name.c_str(), "Duration") == 0) {
         fLdx.fDacc->sel().selectTime 
            (fLdx.fDacc->sel().selectedTime(), Interval (p));
         return true; 
      }
      else if (!xsilStd::analyzeName (name, n, i1, i2)) {
         return true;
      }
      else if (strcasecmp (n.c_str(), "Rate") == 0) {
         if (fList) (*fList)[i1].fChn[i2].SetRate (p);
         return true; 
      }
      return false;
   }

//______________________________________________________________________________
   bool xsilHandlerLidax::HandleParameter (const std::string& name,
                     const attrlist& attr, const std::string& p)
   {
      int i1;
      int i2;
      string n;
      if (strcasecmp (name.c_str(), "Logfile") == 0) {
         fLdx.fLog = p;
         return true;
      }
      else if (strcasecmp (name.c_str(), "Webfile") == 0) {
         fLdx.fWeb = p;
         return true;
      }
      else if (strcasecmp (name.c_str(), "EmailAddress") == 0) {
         fLdx.fEmail = p;
         return true;
      }
      else if (!xsilStd::analyzeName (name, n, i1, i2)) {
         return true;
      }
      else if (strcasecmp (n.c_str(), "Server") == 0) {
         if (fList) (*fList)[i1].fValid = true;
         if (fList) (*fList)[i1].fClnt = false;
         if (fList) (*fList)[i1].fName = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), "Client") == 0) {
         if (fList) (*fList)[i1].fValid = true;
         if (fList) (*fList)[i1].fClnt = true;
         if (fList) (*fList)[i1].fName = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), "UDN") == 0) {
         if (fList) (*fList)[i1].fUDN = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), "Format") == 0) {
         if (fList) (*fList)[i1].fFormat = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), "Channel") == 0) {
         if (fList) {
            double rate = (*fList)[i1].fChn[i2].Rate();
            (*fList)[i1].fChn[i2] = 
               fantom::channelentry (p.c_str(), rate);
         }
         return true;
      }
      else if ((strcasecmp (n.c_str(), "MonitorName") == 0) &&
              (i1 >= 0) && (i1 < 1000)) {
         if ((int)fLdx.fMonitors.size() <= i1) {
            fLdx.fMonitors.resize (i1 + 1);
         }
         fLdx.fMonitors[i1].fName = p;
      }
      else if ((strcasecmp (n.c_str(), "MonitorArg") == 0) &&
              (i1 >= 0) && (i1 < 1000)) {
         if ((int)fLdx.fMonitors.size() <= i1) {
            fLdx.fMonitors.resize (i1 + 1);
         }
         fLdx.fMonitors[i1].fArgs = p;
      }
      else if ((strcasecmp (n.c_str(), "MonitorData") == 0) &&
              (i1 >= 0) && (i1 < 1000)) {
         if ((int)fLdx.fMonitors.size() <= i1) {
            fLdx.fMonitors.resize (i1 + 1);
         }
         fLdx.fMonitors[i1].fUDN = p;
      }
      return false;
   }

//______________________________________________________________________________
   bool xsilHandlerLidax::HandleTime (const std::string& name,
                     const attrlist& attr,
                     unsigned long sec, unsigned long nsec)
   {
      if (strcasecmp (name.c_str(), "Start") == 0) {
         fLdx.fDacc->sel().selectTime 
            (Time (sec, nsec), fLdx.fDacc->sel().selectedDuration());
         return true;
      }
      return false;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerQueryLidax                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandler* xsilHandlerQueryLidax::GetHandler (const attrlist& attr)
   {
      attrlist::const_iterator ni = attr.find (xmlName);
      if ((ni != attr.end()) &&
         (strncasecmp (ni->second.c_str(), "Lidax", 5) == 0)) {
         return new xsilHandlerLidax (fLdx);
      }
      else {
         return 0;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilStoreLidax	                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool xsilStoreLidax (const TGWindow *p, const LidaxParam& ldx)
   {
      // file save as dialog
      TGFileInfo info;
      info.fFilename = 0;
      info.fIniDir = 0;
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gSaveAsTypes);
   #else
      info.fFileTypes = const_cast<char**>(gSaveAsTypes);
   #endif
   #if 1
      info.fFileTypeIdx = 0 ; // Point at .ldx
      {
	 // On return, info.fFilename will be filled in
	 // unless the user cancels the operation.
	 new TLGFileDialog(p, &info, kFDSave) ;
      }
      if (!info.fFilename)
   #else
      if (!TLGFileDialog (p, info, kFDSave, ".ldx"))
   #endif
      {
      #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
         delete [] info.fFilename;
      #endif
         return false;
      }
      string error;
      bool ret = xsilStoreLidaxToFile (info.fFilename, ldx, error);
      if (!ret) {
         new TGMsgBox (gClient->GetRoot(), p, "Error", 
                      error.c_str(), kMBIconStop, kMBOk);
      }
   #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
      delete [] info.fFilename;
   #endif
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilStoreLidax	                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool xsilStoreLidaxToFile (const char* filename, 
                     const LidaxParam& ldx, string& err)
   {
      ofstream out (filename);
      if (!out) {
         err = "Unable to open file ";
         err += filename;
         return false;
      }
      out << xsilHeader() << endl;
      out << ldx << endl;
      out << xsilTrailer() << endl;
      if (!out) {
         err = "Unable to write to file ";
         err += filename;
         return false;
      }
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilRestoreLidax	                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool xsilRestoreLidax (const TGWindow *p, LidaxParam& ldx)
   {
      static Cursor_t fWaitCursor = (Cursor_t) -1;
      if (fWaitCursor == (Cursor_t)-1) {
         fWaitCursor = gVirtualX->CreateCursor (kWatch);
      }
   
      TGFileInfo	info;
      info.fFilename = 0;
      info.fIniDir = 0;
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gOpenTypes);
   #else
      info.fFileTypes = const_cast<char**>(gOpenTypes);
   #endif
   #if 1
      info.fFileTypeIdx = 0 ; // Point at .ldx
      {
	 // On return, info.fFilename will be filled in
	 // unless the user cancels the operation.
	 new TLGFileDialog(p, &info, kFDOpen) ;
      }
      if (!info.fFilename)
   #else
      if (!TLGFileDialog (p, info, kFDOpen, 0))
   #endif
      {
      #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
         delete [] info.fFilename;
      #endif
         return false;
      }
      gVirtualX->SetCursor (p->GetId(), fWaitCursor);
      gVirtualX->Update();
      string error;
      bool ret = xsilRestoreLidaxFromFile (info.fFilename, ldx, error);
      if (!ret) {
         new TGMsgBox (gClient->GetRoot(), p, "Error", 
                      error.c_str(), kMBIconStop, kMBOk);
      }
   #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
      delete [] info.fFilename;
   #endif
      gVirtualX->SetCursor (p->GetId(), kNone);
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilRestoreLidaxFromFile                                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool xsilRestoreLidaxFromFile (const char* filename, 
                     LidaxParam& ldx, string& err)
   {
      xsilParser inp;
      xsilHandlerQueryLidax ldxq (ldx);
      inp.AddHandler (ldxq);
      if (!inp.ParseFile (filename)) {
         err = "Unable to read file ";
         err += filename;
         return false;
      }
      return true;
   }


}
