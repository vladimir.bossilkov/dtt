/* version $Id: diagtest.cc 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */

#include <complex>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <expat.h>
#include <stdio.h>
#include "gdsstring.h"
#include "gdsdatum.hh"
#include "gdsstring.h"

   using namespace std;
   using namespace diag;

   int main () 
   {
      gdsStorage	dat ("Daniel Sigg", "November 10, 1998");
      dat.parameters.push_back 
         (gdsStorage::gdsParameterPtr (
                           new (std::nothrow) gdsParameter ("class", "swept sine")));
      dat.parameters.push_back 
         (gdsStorage::gdsParameterPtr (
                           new (std::nothrow) gdsParameter ("pi", 3.14159)));
      dat.parameters.push_back 
         (gdsStorage::gdsParameterPtr (
                           new (std::nothrow) gdsParameter ("length", 8751987)));
      dat.parameters.push_back 
         (gdsStorage::gdsParameterPtr (
                           new (std::nothrow) gdsParameter ("e", (float) 2.089809, 
                                             "Hz", "This is a small number")));     
      dat.parameters.push_back 
         (gdsStorage::gdsParameterPtr (
                           new (std::nothrow) gdsParameter (
                                             "c", complex<float> (4e3, 7.9) )));
      dat.fsave ("dtest.xml");
   
   /*
      istringstream	is ("6487.432 88.783");
      double	re, im;
      is >> re >> im;
   
      complex <double>	x (re, im);
      cout << "complex = " << x << endl; */
   
      return 0;
   }
