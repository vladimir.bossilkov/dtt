/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdscmd							*/
/*                                                         		*/
/* Module Description: implements functions for executing gds commands	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef DEBUG
#define DEBUG
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Includes: 								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <cstdio>
#include <ctype.h>
#include <cstdlib>
#include <cstring>
#include <time.h>
#include <string>
#include <fstream>
#include <iostream>
#include <memory>
#include "gdsstringcc.hh"
#include "gmutex.hh"
#include "goption.hh"
#include "gdstask.h"
#include "gdscmd.hh"
#include "gdsdatum.hh"
#include "diagdatum.hh"
#include "diagorg.hh"
#include "testpoint.h"
#include "testpointmgr.hh"
#include "databroker.hh"
#include "rtddinput.hh"
#include "nds2input.hh"
#include "lidaxinput.hh"
#include "awgapi.h"
#include "FilterDesign.hh"
#include "iirutil.hh"
#include "gdserr.h" // JCB

static int my_debug = 0 ;

namespace diag {
   using namespace std;
   using namespace thread;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: _help_text	help text				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   const char* const 	_help_text =
   "  run [-wqt 'timeout']: run test\n"
   "       w: wait for test to finish\n"
   "       q: quit program on error\n"
   "       t 'timeout': timeout for test wait\n"
   "  abort: abort test\n"
   "  pause/resume: pause and resume test\n"
   "  save 'filename': save test data\n"
   "  restore 'filename': restore test data\n"
   "  set 'paramname' = 'value': set test parameter\n"
   "  get 'paramname': get test parameter (wildcard * supported)\n"
   "  awg 'cmd': control the arbitrary waveform generator\n"
   "  tp 'cmd': control test points\n"
   "Use 'awg help' or 'tp help' for further information\n";

   const int _TEST_PRIORITY = 30;
   const char _TEST_TASK_NAME[] = "tTest";
   const struct timespec _TICK = {0, 1000000};
   const double _TP_LAZY = 15 * 60; 	// 15 min
   const double _RTDD_LAZY = 10 * 60; 	// 10 min


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: 				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: init		intialization flag			*/
/*          capability		capability flag				*/
/*          ntfy_func		notification function			*/
/*          gds			storage object				*/
/*          tpmgr		test point manager	   		*/
/*          rtddmgr		real-time data distribution manager	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int			init = 0;
   static thread::mutex		cmdmux;
   static int			capability = 0;
   static gdsCmdNotification	ntfy_func = 0;
   static diagStorage*		gds = 0;
   static testpointMgr*		tpmgr = 0;
   static dataBroker*		databroker = 0;
   static string		brokername = "";
   static string		brokerid = "";
   static string 		ndsserver = "";
   static int 			ndsport = 0;
   static bool			brief = true;
   static bool			testAbort = false;
   static bool			testPause = false;
   static bool			testRunning = false;
   static string		testErrmsg;
   static taskID_t		TID = 0;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Forward declarations: 						*/
/*	initGdsMsg		init of gds message interface		*/
/*	finiGdsMsg		cleanup of gds message interface	*/
/*      tpMakeHandle		make a rpc client handle		*/
/*      tpGetIndexDirect	gets the specified test point index	*/
/*      readTestpoints		reads the test point indexes from RM	*/
/*      								*/
/*----------------------------------------------------------------------*/


   int diagNotify (const char* msg, const char* prm, int pLen, 
                  char** res, int* rLen)
   {
      if (ntfy_func != 0) {
         return ntfy_func (msg, prm, pLen, res, rLen);
      }
      else {
         return 0;
      }
   }

   int dummyval = 12345;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: runTest					*/
/*                                                         		*/
/* Procedure Description: runs a diagnostics test			*/
/*                                                         		*/
/* Procedure Arguments: error/startup flag				*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
extern "C"
   void* runTest (int& err)
   {
      if (my_debug) cerr << "runTest()" << endl;
      /* test storage object */
      if ((gds == 0) || (databroker == 0) || (tpmgr == 0)) {
         testErrmsg = "Invalid storage object";
         err = -1;
         return 0;
      }
   
      if (my_debug) {
	 if (gds->datatype == gds_string)
	    cerr << "  gds name = " << gds->value << endl ;
	 else
	    cerr << "  gds is not a gds_string type." << endl ;
      }
      /* look if requested supervisory is among registered ones */
      /* See supervisory.hh for supervisory class. */
      if (my_debug) cerr << "runTest() - getSupervisory()" << endl ;
      const supervisory*	s = getSupervisory (*gds);
      if (s == 0) {
         testErrmsg = "Unknown supervisory task";
         err = -2;
         return 0;
      }
   
      /* initialize supervisory */
      unique_ptr<supervisory> 	super (s->self());
      if (super.get() == 0) {
         testErrmsg = "Unable to create supervisory task";
         err = -3;
         return 0;
      }
      cmdnotify		notify (diagNotify);
      if (my_debug) cerr << "runTest() - super->init()" << endl ;
      if (!super->init (*gds, notify, *databroker, *tpmgr, 
                       &testAbort, &testPause)) {
         testErrmsg = super->errmsg.str();
         err = -4;
         return 0;
      }
      // Pass the abort variable to the data broker so collecting
      // data can be aborted by the impatient user.
      databroker->setPauseAbortFlag(&testPause, &testAbort) ;
   
      /* run test */
      err = 1;	/* make sure gdsCmd returns */
      if (my_debug) cerr << "runTest() - super->run()" << endl ;
      if (!super->run()) {
         //cerr << super->errmsg.str() << endl;
         notify.sendError (super->errmsg.str());
         testErrmsg = super->errmsg.str();
      }
   
      /* cleanup */
      semlock		lockit (cmdmux);
      TID = 0;
      testRunning = false;
      if (my_debug) cerr << "runTest() return 0" << endl ;
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: resolveFilter				*/
/*                                                         		*/
/* Procedure Description: resolve filter string 		 	*/
/*                        (must be done here since it is C++)		*/
/*                                                         		*/
/* Procedure Arguments: awg command					*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void resolveFilter (stringcase& cmd)
   {
      // check for filter string
      if (gds_strncasecmp (cmd.c_str(), "filter", 6) != 0) {
         return;
      }
      const char* p = cmd.c_str() + 6;
      // scan slot number
      int sl, n;
      if (sscanf (p, "%i%n", &sl, &n) != 1) {
         return;
      }
      p += n;
      while (*p && isspace (*p)) ++p;
      // check if this is a filter command string (rather than numbers)
      if (!isalpha (*p)) {
         return;
      }
      // get sampling rate from pagesize
      char buf[256];
      sprintf (buf, "show %i", sl);
      char* show = awgCommand (buf);
      char* s = strstr (show, "pagesize:");
      if (!s) {
         free (show);
         return;
      }
      int psz = atoi (s + 9);
      free (show);
      if (psz <= 0) {
         return;
      }
      // transform into 2nd order coefficient list
      FilterDesign ds (16.*(double)psz);
      if (!ds.filter (p) || !isiir (ds())) {
         return;
      }
      int nba = 4 * iirsoscount (ds()) + 1;
      double* ba = new double[nba];
      if (iir2z (ds(), nba, ba)) {
         sprintf (buf, "filter %i %22.14g", sl, ba[0]);
         cmd = buf;
         for (int i = 1; i < nba; ++i) {
            sprintf (buf, " %20.14g", ba[i]);
            cmd += buf;
         }
         //printf ("NEW FILTER CMD: %s\n", cmd.c_str());
      }
      delete [] ba;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsCmdInit					*/
/*                                                         		*/
/* Procedure Description: initializes the gds command interface		*/
/*                                                         		*/
/* Procedure Arguments: flag						*/
/*                                                         		*/
/* Procedure Returns: capability if successful, <0 when failed		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
extern "C" 
   int gdsCmdInit (int flag, const char* conf)
   {
      semlock		lockit (cmdmux);
      // test for multiple opens
      if (init != 0) {
         return -1;
      }
   
      // determine configuration parameters
      ndsserver = "";
      ndsport = 0;
      if (conf != 0) {
         string cfg = conf;
         // nds server name
         string::size_type pos = cfg.find ("-n");
         if (pos != string::npos) {
            for (string::size_type i = pos + 3; i < cfg.size(); i++) {
               if ((cfg[i] == ' ') || (cfg[i] == '\t')) {
                  if (!ndsserver.empty()) {
                     break;
                  }
               }
               else {
                  ndsserver += cfg[i];
               }
            }
         }
         // nds port number
         pos = cfg.find ("-m");
         if (pos != string::npos) {
            bool first = false;
            for (string::size_type i = pos + 3; i < cfg.size(); i++) {
               if ((cfg[i] >= '0') && (cfg[i] <= '9')) {
                  ndsport = 10 * ndsport + (cfg[i] - '0');
                  first = true;
               }
               else if (first || ((cfg[i] != ' ') && (cfg[i] != '\t'))) {
                  break;
               }
            }
         }
      }
   
      // initialize channel information
      if (!ndsserver.empty()) {
         //cerr << "set channel server to " << ndsserver << " @ " 
	 //     << ndsport << endl;
         gdsChannelSetHostAddress (ndsserver.c_str(), ndsport);
      }
   
      // initialize test point manager
      capability = 0;
   #ifndef _NO_TESTPOINTS
      if ((flag & CMD_TESTPOINT) != 0) {
         if (testpoint_client () > 0) {
            capability |= CMD_TESTPOINT;
         }
      }
      tpmgr = new (nothrow) testpointMgr (_TP_LAZY);
      if (tpmgr == 0) {
         return -2;
      }
   #endif
   
      // initialize AWG
      if ((flag & CMD_AWG) != 0) {
         if (awg_client () > 0) {
            capability |= CMD_AWG;
         }
      }

      // initialize test facility
      if ((flag & CMD_TEST) != 0) {
         gds = new (nothrow) diagStorage ();
         if (gds == 0) {
            delete tpmgr; tpmgr = 0;
            return -3;
         }

         //for now we're just going to enable CMD_TEST until
         //diaggui is smart enough not to connect when not online.
         //if(! ndsserver.empty())
         {
             capability |= CMD_TEST;
         }
      }
      //cerr << "flag = " << flag << endl;
      //cerr << "capabilities = " << capability << endl;
   
      init = 1;
      return capability;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsCmdFini					*/
/*                                                         		*/
/* Procedure Description: cleans up the gds command interface		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/    
extern "C" 
   int gdsCmdFini ()
   {
      if (init == 0) {
         return 0;
      }
   
      printf ("EXIT KERNEL\n");
      semlock		lockit (cmdmux);
   
      if ((testRunning) && (TID != 0)) {
         taskCancel (&TID);
         testRunning = false;
      }
   
      // terminate AWG
      if ((capability & CMD_AWG) != 0) {
         awg_cleanup ();
      }
   
      // terminates test points
   #ifndef _NO_TESTPOINTS
      delete tpmgr; 
      tpmgr = 0;
   #endif
   
      // terminate test facility
      capability= 0;
      ntfy_func = 0;
      delete gds;
      gds = 0;
      delete databroker; 
      databroker = 0;
      brokername = "";
   
      init = 0;
      // const struct timespec wait = {0, 80000000};
      // nanosleep (&wait, 0);
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: cmdreply					*/
/*                                                         		*/
/* Procedure Description: command reply					*/
/*                                                         		*/
/* Procedure Arguments: string						*/
/*                                                         		*/
/* Procedure Returns: newly allocated char*				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/   
   char* cmdreply (const char* m)
   {
      if (m == 0) {
         return 0;
      }
      else {
         char* p = (char*) malloc (strlen (m) + 1);
         if (p != 0) {
            strcpy (p, m);
         }
	 else /* JCB */
	 {
	    gdsDebug("cmdreply malloc(strlen(m) + 1) failed.") ;
	 }
         return p;
      }
   }


   char* cmdreply (const string& m)
   {
      return cmdreply (m.c_str());
   }


   static gdsStorage::ioflags fileFlags (const string& arg)
   {
      stringcase	s (arg.c_str());
      if (s.find ("-std") != string::npos) {
         return gdsStorage::ioStandard;
      }
      else if (s.find ("-ext") != string::npos) {
         return gdsStorage::ioExtended;
      }
      else if (s.find ("-all") != string::npos) {
         return gdsStorage::ioEverything;
      }
      else if (s.find ("-prm") != string::npos) {
         return gdsStorage::ioParamOnly;
      }
      else {
         return gdsStorage::ioStandard;
      }
   }


   static string fileName (const string& arg)
   {
      string buf (arg);
   
      while (!buf.empty() && isspace (buf[0])) buf.erase (0, 1);
      while (!buf.empty() && isspace (buf[buf.size()-1])) {
         buf.erase (buf.size()-1, 1);
      }
      if (!buf.empty() && (buf[0] == '-')) {
         while (!buf.empty() && !isspace (buf[0])) buf.erase (0, 1);
         while (!buf.empty() && isspace (buf[0])) buf.erase (0, 1);
      }
      return buf;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsCmd					*/
/*                                                         		*/
/* Procedure Description: calls a gds command 				*/
/*                                                         		*/
/* Procedure Arguments: message, parameter, reply pointer		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
extern "C" 
   int gdsCmd (const char* msg, const char* prm, int pLen,
              char** res, int* rLen)
   {
      *res = 0;
      *rLen = 0;
   
      /* test if init */
      if (init == 0) {
         return -1;
      }
   
      stringcase 	message (msg);
   
      // help command
      if (message.substr (0, 4) == "help") {
         *res = cmdreply (_help_text);
      }
      
      // testpoint commands
      else if (message.substr (0, 2) == "tp") {
      #ifdef _NO_TESTPOINTS
         *res = cmdreply ("error: test points not available");
      #else
         if ((capability & CMD_TESTPOINT) == 0) {
            *res = cmdreply ("error: test points not available");
         }
         else {
            stringcase		cmd (msg + 2);
            /* remove blanks */
            while (!cmd.empty() && (cmd[0] == ' ')) {
               cmd.erase (0, 1);
            }
            *res = tpCommand (cmd.c_str());
         }
      #endif
      }
      
      // AWG commands
      else if (message.substr (0, 3) == "awg") {
         if ((capability & CMD_AWG) == 0) {
            *res = cmdreply ("error: AWGs are not available");
         }
         else {
            stringcase		cmd (msg + 3);
            // remove blanks
            while (!cmd.empty() && isspace(cmd[0])) {
               cmd.erase (0, 1);
            }
            // resolve filter command string if necessary
            resolveFilter (cmd);
            // call awg
            *res = awgCommand (cmd.c_str());
         }
      }
      
      // set command
      else if (message.substr (0, 3) == "set") 
      {
         string		var;
         string		val;
         string::size_type pos;
         var = message.substr (3, message.size()).c_str();
	 // cerr << "gdsCmd() - set " << var << endl ;
	 // Get rid of leading spaces.
         while ((var.size() > 0) && (var[0] == ' ')) {
            var.erase (0, 1);
         }
	 // Find the separator between the variable name and value.
         pos = var.find ('=');
         if (pos == string::npos) {
	    // If it's not there, complain.
            *res = cmdreply ("error: illegal argument");
         }
         else 
	 {
	    // Split the string into variable and value, then
	    // get rid of spaces at the beginning and end of each.
            val = var.substr (pos + 1, var.size() - pos - 1);
            var = var.substr (0, pos);
            while ((var.size() > 0) && (var[0] == ' ')) {
               var.erase (0, 1);
            }
            while ((var.size() > 0) && (var[var.size()-1] == ' ')) {
               var.erase (var.size()-1, 1);
            }
            while ((val.size() > 0) && (val[0] == ' ')) {
               val.erase (0, 1);
            }
            while ((val.size() > 0) && (val[val.size()-1] == ' ')) {
               val.erase (val.size()-1, 1);
            }
	    // Set the value.
            if (gds->set (var, val)) {
               *res = cmdreply ("");
            }
            else {
               *res = cmdreply ("error: illegal argument");
            }
         }
      }
      
      // get command
      else if (message.substr (0, 3) == "get") 
      {
         string		var;
         string		val;
         var = message.substr (3, message.size()).c_str();
         while ((var.size() > 0) && (var[0] == ' ')) {
            var.erase (0, 1);
         }
         if (gds_strncasecmp (var.c_str(), "channels", 8) == 0) {
            // get all channel names
            *res = gdsChannelNames (-1, 0, 1);
         }
         else if (gds_strncasecmp (var.c_str(), "times", 5) == 0) {
            // get time of 
            rtddManager* rtdd = dynamic_cast<rtddManager*>(databroker);
            if (rtdd) {
               taisec_t start;
               taisec_t duration;
               rtdd->getTimes (start, duration);
               char buf[1024];
               sprintf (buf, "times = %li %li", start, duration);
               *res = cmdreply (buf);
            }
            else {
               *res = cmdreply ("times = 0 0");
            }
         }
         else if (gds_strncasecmp (var.c_str(), "rawdatanames", 12) == 0) {
            // get all raw data (channel objects) names
            vector<string>	names;
            if (gds->getChannelNames (names)) {
               ostringstream	os;
               os << "rawDataNames = ";
               for (vector<string>::iterator iter = names.begin();
                   iter != names.end(); iter++) {
                  os << *iter << endl;
               }
               *res = cmdreply (os.str().c_str());
            }
            else {
               *res = cmdreply ("error: no raw data objects available");
            }
         }
         else if (gds_strncasecmp (var.c_str(), "referencenames", 14) == 0) {
            // get the names of the reference traces
            vector<string>	names;
            if (gds->getReferenceTraceNames (names)) {
               ostringstream	os;
               os << "referenceNames = ";
               for (vector<string>::iterator iter = names.begin();
                   iter != names.end(); iter++) {
                  os << *iter << endl;
               }
               *res = cmdreply (os.str().c_str());
            }
            else {
               *res = cmdreply ("error: no reference traces available");
            }
         }
         else if (gds_strncasecmp (var.c_str(), "auxdatanames", 12) == 0) {
            // get the names of the auxiliary data objects
            vector<string>	names;
            if (gds->getAuxiliaryResultNames (names)) {
               ostringstream	os;
               os << "auxdataNames = ";
               for (vector<string>::iterator iter = names.begin();
                   iter != names.end(); iter++) {
                  os << *iter << endl;
               }
               *res = cmdreply (os.str().c_str());
            }
            else {
               *res = cmdreply ("error: no auxiliary data objects available");
            }
         }
         else {
            // normal get
            if (gds->getMultiple (var, val, brief) && 
               (val.size() > 0)) {
               *res = cmdreply (val.c_str());
            }
            else {
               *res = cmdreply ("error: illegal argument");
            }
         }
      }
      
      // delete command 
      else if (message.substr (0, 3) == "del") {
         string		var;
         string		norm;
         var = message.substr (3, message.size()).c_str();
         while ((var.size() > 0) && (var[0] == ' ')) {
            var.erase (0, 1);
         }
         // erase rawdata
         if (gds_strncasecmp (var.c_str(), "rawdata", 7) == 0) {
            gds->purgeChannelData (0);
            *res = cmdreply ("rawdate erased");
         }
         // erase plot settings
         else if (gds_strncasecmp (var.c_str(), "plotsettings", 12) == 0) {
            gds->erasePlotSettings();
            *res = cmdreply ("plot settings erased");
         }
         // erase calibration records
         else if (gds_strncasecmp (var.c_str(), "calibration", 11) == 0) {
            gds->eraseCalibration();
            *res = cmdreply ("calibration records erased");
         }
         // erase result data
         else if (gds_strncasecmp (var.c_str(), "results", 7) == 0) {
            gds->eraseResults();
            *res = cmdreply ("results erased");
         }
         // erase reference traces
         else if (gds_strncasecmp (var.c_str(), "references", 10) == 0) {
            gds->eraseReferenceTraces();
            *res = cmdreply ("reference traces erased");
         }
         // erase/clear IO and UDN caches
         else if (gds_strncasecmp (var.c_str(), "cache", 5) == 0) {
            lidaxManager::clearCache();
            *res = cmdreply ("cache cleared");
         }
         // normal erase
         else {
            if (gds->erase (var, &norm)) {
               *res = cmdreply (norm + " erased");
            }
            else {
               *res = cmdreply ("error: illegal argument");
            }
         }
      }
      
      // defined command
      else if (message.substr (0, 7) == "defined") {
         string		var;
         string		val;
         var = message.substr (7, message.size()).c_str();
         while ((var.size() > 0) && (var[0] == ' ')) {
            var.erase (0, 1);
         }
         if (gds->getMultiple (var, val, false, true) && 
            (val.size() > 0)) {
            *res = cmdreply (val.c_str());
         }
         else {
            *res = cmdreply ("");
         }
      }
      
      // brief command
      else if (message.substr (0, 5) == "brief") {
         brief = !brief;
         if (brief) {
            *res = cmdreply ("brief is on");
         }
         else {
            *res = cmdreply ("brief is off");
         }
      }
      
      // run command
      else if (message.substr (0, 3) == "run") {
         gdsbase::option_string opt ("run", 
                              message.substr (4, message.size()).c_str(), "wqt:");
         bool wait = opt.opt ('w');
         bool quitonerr = opt.opt ('q');
         int timeout = -1;
         if (!opt.getOpt ('t', timeout)) timeout = -1;
         semlock		lockit (cmdmux);
         if (testRunning) {
            *res = cmdreply ("error: test already running");
         }
         else {
	    if (my_debug) cerr << "RUN TEST" << endl;
            // string s = "failed";
            // gds->getMultiple ("*", s);
            // cerr << s << endl;
         
            // setup data broker
            string var = stInputSource;
            string newbrokername = stInputSourceOnline;
	    // As a reminder, gds is a diagStorage object created in gdsCmdInit().
            gds->get (var, newbrokername, 0);
	    if (my_debug) cerr << "  INPUT Source = " << newbrokername << endl;
            stringcase isource (newbrokername.c_str());
            // reconnect?
            var = string (stDef) + "." + stDefReconnect;
            bool reconnect = false;
            string val;
            if (gds->get (var, val, 0) && !val.empty()) {
               if (isdigit(val[0])) {
                  reconnect = (atoi (val.c_str()) != 0);
               }
               else {
                  reconnect = ((val[0] == 't') || (val[0] == 'T'));
               }
            }
            if (newbrokername != brokername) {
               reconnect = true;
            }
            // get broker id for lidax
            string newbrokerid;
            if (isource == stInputSourceLidax) {
               newbrokerid = lidaxManager::id (*gds);
               // reconnect lidax if id changed
               if ((brokername == stInputSourceLidax) &&
                  (brokerid != newbrokerid)) {
                  reconnect = true;
               }
            }
            if (my_debug) cerr << "old source = " << brokername << 
               " new source = " << newbrokername << 
               " reconnect = " << reconnect << endl;
            if (reconnect || (databroker == 0)) {
               if (my_debug) cerr << "New data broker" << endl;
               if (databroker) delete databroker;
               databroker = 0;
               // online
               if (isource == stInputSourceOnline) {
                  if (my_debug) cerr << "New online data broker" << endl;
                  rtddManager* rtddmgr = new (nothrow) rtddManager (
                                       gds, tpmgr, _RTDD_LAZY);
                  if (rtddmgr == 0) {
                     *res = cmdreply ("error: Unable to connect to "
                                     "online server");
                  }
                  // establish connection to rtdd
                  else if (!rtddmgr->connect 
                          (ndsserver.empty() ? 0 : ndsserver.c_str(), 
                          ndsport)) {
                     delete rtddmgr;
                     *res = cmdreply ("error: Unable to connect to "
                                     "online server");
                  }
                  else {
                     databroker = rtddmgr;
                     brokername = stInputSourceOnline;
                  }
               }
               // user NDS2
               else if ((isource.substr (0, 5) == stInputSourceSENDS)) {
                  if (my_debug) cerr << "  New SENDS data broker" << endl ;
                          // get NDS name and port number
                          string user_ndsserver;
                          int user_ndsport = 0;
                          string s = newbrokername;
                          s.erase (0, 5);
                          while (!s.empty() && isspace (s[0])) s.erase (0, 1);
                          string::size_type pos = s.find_first_of (" \t\n\f\r\v:");
                          if (pos == string::npos) {
                             user_ndsserver = s;
                             user_ndsport = 0;
                          }
                          else {
                             user_ndsserver = s.substr (0, pos);
                             user_ndsport = 0;
                             pos = s.find (':');
                             if (pos != string::npos) {
                                user_ndsport = atoi (s.c_str() + pos + 1);
                             }
                          }

                  // Extract the epoch start time and stop time if they are
                  // present in the newbrokername.
                  string es("epoch_start=") ;
                  string ee("epoch_end=") ;
                  string::size_type es_pos = s.find(es) ;
                  string::size_type ee_pos = s.find(ee) ;
                  unsigned long epoch_start = 0 ;
                  unsigned long epoch_stop = 0 ;
                  if (es_pos != string::npos && ee_pos != string::npos)
                  {
                     epoch_start = atoi(s.c_str() + es_pos + es.length()) ;
                     epoch_stop = atoi(s.c_str() + ee_pos + ee.length()) ;
                  }

                  // create new data broker
                  // cerr << "New user NDS2 data broker" << endl;
                  // Defined in storage/nds2input.hh,cc.
                  nds2Manager* nds2mgr = new (nothrow) nds2Manager (
                                       gds, tpmgr, _RTDD_LAZY);
                  if (nds2mgr == 0) {
                     *res = cmdreply ("error: Unable to connect to NDS2");
                  }
                  // establish connection to nds2
                  else if (user_ndsserver.empty() ||
                          !nds2mgr->connect (user_ndsserver.c_str(), 
                                            user_ndsport, true, 
					    epoch_start, epoch_stop)) {
                     delete nds2mgr;
                     *res = cmdreply ("Error: Unable to connect to NDS2");
                  }
                  else {
                     databroker = nds2mgr;
                     brokername = newbrokername;
                  }
               }
               // user NDS
               else if ((isource.substr (0, 3) == stInputSourceNDS)) {
                  // get NDS name and port number
		              if (my_debug) cerr << "  New user NDS data broker" << endl ;
		              string user_ndsserver;
                      int user_ndsport = 0;
                      string s = newbrokername;
                      s.erase (0, 3);
                      while (!s.empty() && isspace (s[0])) s.erase (0, 1);
                      string::size_type pos = s.find_first_of (" \t\n\f\r\v:");
                      if (pos == string::npos) {
                         user_ndsserver = s;
                         user_ndsport = 0;
                      }
                      else {
                         user_ndsserver = s.substr (0, pos);
                         user_ndsport = 0;
                         pos = s.find (':');
                         if (pos != string::npos) {
                            user_ndsport = atoi (s.c_str() + pos + 1);
                         }
                      }
                      // create new data broker
                      //cerr << "New user NDS data broker" << endl;
                      rtddManager* rtddmgr = new (nothrow) rtddManager (
                                           gds, tpmgr, _RTDD_LAZY);
                      if (rtddmgr == 0) {
                         *res = cmdreply ("error: Unable to connect to NDS");
                      }
                      // establish connection to rtdd
                      else if (user_ndsserver.empty() ||
                              !rtddmgr->connect (user_ndsserver.c_str(),
                                                user_ndsport, true)) {
                         delete rtddmgr;
                         *res = cmdreply ("error: Unable to connect to NDS");
                      }
                      else {
                         databroker = rtddmgr;
                         brokername = newbrokername;
                      }
               }
               // lidax
               else if (isource == stInputSourceLidax)
               {
                   if (my_debug) cerr << "  New offline data broker (lidax)" << endl;
                   lidaxManager *lidaxmgr = new(nothrow) lidaxManager(
                           gds, _RTDD_LAZY);
                   if (lidaxmgr == 0)
                   {
                       *res = cmdreply("error: Unable to connect to "
                                       "offline server");
                   }
                   else if (!lidaxmgr->connect())
                   {
                       delete lidaxmgr;
                       *res = cmdreply("error: Unable to connect to "
                                       "offline server");
                   }
                   else
                   {
                       databroker = lidaxmgr;
                       brokername = stInputSourceLidax;
                       brokerid = newbrokerid;
                   }
               }
               // unrecognized server
               else {
                   if (my_debug) cerr << "  Unrecognized data broker..." << endl;
                   string err = "error: Unrecognized data server ";
                   err += newbrokername;
                   *res = cmdreply(err);
               }
            }
            else
            {
              if(!databroker->reconnect())
              {
                delete databroker;
                databroker = nullptr;
                *res = cmdreply ("Error: Unable to reconnect to data source");
              }
            }
         
            // run test
            if (!*res) {
                dummyval=99999;
               testAbort = false;
               testPause = false;
               testErrmsg = "";
               int		ret = 0;
               int		attr = 0;
            
            #ifndef OS_VXWORKS
               attr = PTHREAD_CREATE_DETACHED | PTHREAD_SCOPE_SYSTEM;
            #endif
               if (taskCreate (0, _TEST_PRIORITY, &TID, _TEST_TASK_NAME,
                              (taskfunc_t) runTest, (taskarg_t) &ret) == 0) {
                  while (ret == 0) {
                     nanosleep (&_TICK, 0);
                  }
               }
               if (ret > 0) {
                  testRunning = true;
                  if (!wait) {
                     *res = cmdreply ("test is running");
                  }
                  else {
                     tainsec_t start = TAInow();
                     cmdmux.unlock();
                     while (testRunning && 
                           ((timeout < 0) || (start + timeout * _ONESEC > TAInow()))) {
                        nanosleep (&_TICK, 0);
                     }
                     cmdmux.lock();
                     if (!testRunning) {
                        if (testErrmsg.empty()) {
                           *res = cmdreply ("test has completed");
                        }
                        else {
                           *res = cmdreply ("error: test has aborted");
                        }
                     }
                     else {
                        *res = cmdreply ("error: test is still running after timeout");
                     }
                  }
               }
               else {
                  *res = cmdreply ("error: " + testErrmsg);
               }
            }
         }
      	 // check for error
         if (quitonerr && *res && (strncmp (*res, "error:", 6) == 0)) {
            cerr << "diagnostics test aborted" << endl;
            exit (1);
         }
      }
      
      // abort command
      else if (message.substr (0, 5) == "abort") {
         semlock		lockit (cmdmux);
         if (testRunning) {
            testAbort = true;
            *res = cmdreply ("test aborted");
         }
         else {
            *res = cmdreply ("error: test not running");
         }
      }
      
      // pause command
      else if (message.substr (0, 5) == "pause") {
         semlock		lockit (cmdmux);
         if (testRunning) {
            testPause = true;
            *res = cmdreply ("test paused");
         }
         else {
            *res = cmdreply ("error: test not running");
         }
      }
      
      // resume command
      else if (message.substr (0, 6) == "resume") {
         semlock		lockit (cmdmux);
         if (testRunning) {
            testPause = false;
            *res = cmdreply ("test resumed");
         }
         else {
            *res = cmdreply ("error: test not running");
         }
      }
      
      // save command
      else if (message.substr (0, 4) == "save") {
         string		arg;
         arg = message.substr (4, message.size()).c_str();
         gdsStorage::ioflags	flags = fileFlags (arg);
         string		filename = fileName (arg);
         bool		filetemp = false;
      
         if (filename.size() == 0) {
            filename = tempFilename();
            filetemp = true;
         }
         if (!gds->fsave (filename, flags)) {
            *res = cmdreply ("error: " + gds->errmsg());
         }
         else if (!filetemp) {
            *res = cmdreply (filename + " saved");
         }
         else {
            ifstream		inp (filename.c_str());
            inp.seekg (0, ios::end);
            *rLen = inp.tellg();
            inp.seekg (0);
            *res = (char*) malloc (*rLen);
	    if (!*res) /* JCB */
	    {
	       gdsDebug("gdsCmd malloc(*rLen) failed.") ;
	       return false ;
	    }
            if ((*res == 0) || (!inp.read (*res, *rLen))) {
               free (*res);
               *res = cmdreply ("error: insufficient memory");
            }
            else {
               return true;
            }
         }
      }
      
      // restore command
      else if (message.substr (0, 7) == "restore") {
         string		arg;
         arg = message.substr (7, message.size()).c_str();
         gdsStorage::ioflags	flags = fileFlags (arg);
         string		filename = fileName (arg);
         bool		filetemp = false;
         if ((filename.size() == 0) && (pLen > 0) && (prm != 0)) {
            // write parameter data to temporary file
            filename = tempFilename();
            ofstream		out (filename.c_str());
            if (!out) {
               *res = cmdreply ("error: illegal temp file");
            }
            else {
               gds->registerTempFile (filename);
               filetemp = true;
               out.write (prm, pLen);
               if (!out) {
                  *res = cmdreply ("error: insufficient memory");
               }
            }
            out.close();
         }
         // restore from disk
         if (*res == 0) {
            if (gds->frestore (filename, flags)) {
//               *res = cmdreply (filename + " restored");
            }
            else {
               *res = cmdreply ("error: " + gds->errmsg());
            }
         }
         if (filetemp) {
            gds->unregisterTempFile (filename);
         }
      }
      
      // unrecognized command
      else {
         *res = cmdreply ("error: unrecognized command");
      }
      if (*res != 0) {
         *rLen = (int) strlen (*res);
      }
      return 0;   
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsCmdData					*/
/*                                                         		*/
/* Procedure Description: transfers data 				*/
/*                                                         		*/
/* Procedure Arguments: data object name, transfer direction, data type,*/
/*                      length, offset, data array, array length	*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
extern "C" 
   int gdsCmdData (const char* name, int toKernel, int datatype,
                  int len, int ofs, float** data, int* datalength)
   {
      // check 
      if ((name == 0) || (data == 0) || (datalength == 0)) {
         return -1;
      }
      // object name
      string		var (name);
      while ((var.size() > 0) && (var[0] == ' ')) {
         var.erase (0, 1);
      }
   
      // store data
      if (toKernel) {
         int newindex;
         if (gds->putData (var, datatype, len, ofs, 
                          *data, *datalength, &newindex)) {
            return newindex;
         }
         else {
            return -1;
         }
      }
      // retrieve data
      else {
         *data = 0;
         *datalength = 0;
         if (gds->getData (var, datatype, len, ofs, 
                          *data, *datalength)) {
            return 0;
         }
         else {
            return -1;
         }
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsCmdNotifyHandler				*/
/*                                                         		*/
/* Procedure Description: installs a command notification handler	*/
/*                                                         		*/
/* Procedure Arguments: callback routine				*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
extern "C" 
   int gdsCmdNotifyHandler (gdsCmdNotification callback)
   {
      if (init == 0) {
         return -1;
      }
      else {
         ntfy_func = callback;
         return 0;
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: cmdNotification				*/
/*                                                         		*/
/* Procedure Description: sends a command notification message		*/
/*                                                         		*/
/* Procedure Arguments: message, parameter, reply			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
extern "C" 
   int cmdNotification (const char* msg, const char* prm, 
                     int pLen, char** res, int* rLen)
   {
      if ((init != 0) && (ntfy_func != 0)) {
         return (*ntfy_func) (msg, prm, pLen, res, rLen);
      }
      else {
         *res = 0;
         *rLen = 0;
         return -1;
      }
   }


#ifndef P__DARWIN
   static void initCMD (void);

#ifndef __GNUC__
#pragma init(initCMD)
#endif

   static void initCMD (void)
   {
      //cerr << "I shouldn't be here" << endl;
   }
#endif


}
