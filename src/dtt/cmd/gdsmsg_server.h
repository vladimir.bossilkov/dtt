/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsmsg_server						*/
/*                                                         		*/
/* Module Description: API for handling testpoints			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 25June98 D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: gdsmsg_server.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_GDSMSG_SERVER_H
#define _GDS_GDSMSG_SERVER_H

#ifdef __cplusplus
extern "C" {
#endif


/** @name Command Message Server
    Remote procedure service for diagnostics messages.

    @memo rpc server for diagnostics messages
    @author Written January 1999 by Daniel Sigg
    @see Test Point API
    @version 0.1
************************************************************************/

/*@{*/

/** Starts the rpc service for the diagnostics messages interface.
    Ideally, this service is called by the inet daemon at a well-known
    address. The inet daemon will start a new command message server
    with every request. The command message server will load a local
    diagnostics kernel and establish a bidirectional connection to
    the client on a private channel. (The original rpc number is only 
    used to open the conection.) The value of well-known rpc number is
    0x31001004, version 1. 

    Assuming this routine is implement as the main program of the 
    diagnistics kernel whose full pathname is '/home/gds/bin/gdsd'
    The following line should be added to the inetd.conf file:
    \begin{verbatim}
    822087684/1  stream  rpc/tcp  nowait  gds  /home/gds/bin/gdsd  gdsd
    \end{verbatim}
    where 'pathname' is the full path and filename of gdsmsg_server
    executable.

    @param sock socket for startup connection
    @return 0 if successful, <0 error number otherwise
    @see Message API
    @author DS, June 98
************************************************************************/
   int gdsmsg_server (int sock);


/*@}*/


#ifdef __cplusplus
}
#endif

#endif /*_GDS_GDSMSG_SERVER_H */
