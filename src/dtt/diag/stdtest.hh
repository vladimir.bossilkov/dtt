/* version $Id: stdtest.hh 7690 2016-08-17 00:07:17Z james.batch@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: stdtest.h						*/
/*                                                         		*/
/* Module Description: Standard diagnostics test			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 28Nov98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: stdtest.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_STDTEST_H
#define _GDS_STDTEST_H

/* Header File List: */
#include <string>
#include <vector>
#include <deque>
#include <set>
#include <utility>
#include "gdsstring.h"
#include "diagtest.hh"
//#include "testsync.hh" included in diagtest.hh
#include "rtddinput.hh"

namespace diag {

   class diagTest;

/** Standard test template
    This object implements a abstract standard test. For a straight
    forward test it should be sufficient to overwrite calcTime,
    calcMeasurement, readParam and the sync functions.
   
    @memo Object for implementing a standard test
    @author Written November 1998 by Daniel Sigg
    @see Diagnostics test manual for used algorithms
    @version 0.1
 ************************************************************************/
   class stdtest : public diagtest {  
   public:
   
      /** Constructs a standard test object.
          @memo Default constructor.
          @return void
       ******************************************************************/
      explicit stdtest (const std::string& name);

      /** Destroy a standard test object.
          @memo Default destructor.
       ******************************************************************/
      virtual ~stdtest(void);
   
      /** Begin of test. This function reads parameters and subscribes
          channels to the excitation and rtdd managers, respectively.
          (Neither excitations nor rtdd channels are turned on,
   	  however.)
          @memo Startup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool begin (std::ostringstream& errmsg);
   
      /** End of test. This function cleans up a test measurement by 
          unsubscribing both excitation and rtdd channels.
          @memo Cleanup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool end (std::ostringstream& errmsg);
   
      /** Sets up the test. This function will activate the excitation
   	  and start the data flow from the rtdd interface - after 
          calculating measurement times and setting up the 
          excitations signals and the measurement partitions.
          This function also returns the first synchronization point.
          @memo Setup method.
          @param errmsg error message stream
          @param starttime start time of test
          @param sync synchronization point
          @return true if successful
       ******************************************************************/
      virtual bool setup (std::ostringstream& errmsg, 
                        tainsec_t starttime,
                        syncpointer& sync);
   
   protected:
      /// friend
      friend class chnrec;
   
      /// simulus object type
      class stimulus {
      public:
         /// list of awg signals
         typedef std::vector<AWG_Component> awglist;
         /// list of awg points
         typedef std::vector<float> pointlist;
      
         /// channel name of excitation
         std::string		name;
         /// is read back?
         bool		isReadback;
         /// channel name of readback
         std::string		readback;
         /// excitation waveform type
         AWG_WaveType	waveform;
         /// frequency
         double		freq;
         /// amplitude	
         double		ampl;
         /// offset
         double 	offs;
         /// phase
         double		phas;
         /// square wave ratio
         double		ratio;
         /// frequency range
         double		frange;
         /// amplitude range
         double		arange;
         /// filter command
         std::string		filtercmd;
         /// list of points for arbitrary waveform
         pointlist	points;
         /// channel info for exciation channel
         gdsChnInfo_t	excInfo;
         /// channel info for readback
         gdsChnInfo_t	readbackInfo;
         /// excitation signal list
         awglist	signals;
         /// true if readback channel is a duplicate
         bool		duplicate; 
         /// partition list
         dataChannel::partitionlist partitions;
      	 /// use active time for preprocessing
         bool		useActiveTime;
      
         /** Constructs a stimulus object.
             @memo Default constructor.
             @return void
          ***************************************************************/
         stimulus ();
      
         /** Generates the signal list which is described by the stimulus
             object.
             @memo Generates signal list.
             @param t0 start time of excitation
             @param duration duration of excitation
	     @param rampUp duration of excitation ramp up
	     @param rampDown duration of excitation ramp down
             @return true if successful
          ***************************************************************/
         bool calcSignal (tainsec_t t0, tainsec_t duration, tainsec_t rampUp=0, tainsec_t rampDown=0);

         /**
          * Populate signal arrary with components for a burst noise excitation
          * @param t0 start time of first component
          * @param pitch_ns time delta from one component to the next
          * @param rampUp_ns time to ramp up for excitation
          * @param rampDown_ns time to ramp down the excitation
          * @param excitation_ns excitation time between ramp up and ramp down.
          * @param num_points number of components to create
          * @return true if successful
          */
         bool calcBurstSignal(std::ostringstream& errmsg, tainsec_t t0, tainsec_t pitch_ns, tainsec_t rampUp_ns, tainsec_t rampDown_ns, tainsec_t excitation_ns, int num_points);
      
         /** Generates a sine wave signal which described by the stimulus
             object and which ramps up smoothly. This function will 
             generate an amplitude ramp up for the first sine signal
             generated. Subsequent sine waves are set, so that the 
             ramp smooyhly (both frequnecy and amplitude) from the 
             the previous one.  
             @memo Generates sine signal.
             @param t0 start time of excitation
             @param duration duration of excitation
             @param rampUp duration of excitation ramp up
	     @param rampDown duration of excitation ramp down
             @return true if successful
          ***************************************************************/
         bool calcSineSignal (tainsec_t t0, tainsec_t duration, 
                           tainsec_t rampUp=0, tainsec_t rampDown=0);
      };
      /// stimuli type
      typedef std::vector<stimulus> stimuluslist;
      /// allowed stimulus waveform type
      typedef std::set<AWG_WaveType> waveformset;
      /// sine wave only
      const static waveformset sinewaveOnly;
      /// periodic waveforms only
      const static waveformset periodicOnly;
      /// noise waveforms only
      const static waveformset noiseOnly;
      /// all waveforms
      const static waveformset allWaveforms;
   
      /// type for selecting stimulus channels
      enum stimulustype {
      /// ignore stimulus channels
      stimNone = 0,
      /// include stimulus channels which have a valid readback
      stimReadback = 1,
      /// include all stimulus channels
      stimAll = 2};
   
      /// measurement channel object type
      class measurementchannel {
      public:
         /// channel name for measurement
         std::string		name;
         /// channel info for measurement channel
         gdsChnInfo_t	info;
         /// true if channel is a duplicate
         bool		duplicate; 
         /// partition list
         dataChannel::partitionlist partitions;
      	 /// use active time for preprocessing
         bool		useActiveTime;
      
         /** Constructs a measurement object.
             @memo Default constructor.
             @return void
          ***************************************************************/
         measurementchannel ();
      };
      /// measurement channels type
      typedef std::vector<measurementchannel> measlist;
      typedef measlist::iterator meas_iter;

      /// interval class
      class interval {
      public:
         /// time interval
         typedef std::pair <tainsec_t, tainsec_t> timeinterval;
         /// time interval
         timeinterval		ival;
         /// if false, this interval was not rejected by the analysis
         bool			good;
      
         /** Constructs an interval object.
             @memo Constructor.
             @return void
          ***************************************************************/
         interval (tainsec_t t, tainsec_t dt);
      
         /** Returns the interval start time.
             @memo Interval start.
             @return start time
          ***************************************************************/
         tainsec_t t0 () const;
      
         /** Returns the interval duration.
             @memo Interval duration.
             @return duration
          ***************************************************************/
         tainsec_t dt () const;
      };
      /// list of intervals
      typedef std::vector <interval> intervallist;
   
      /// argument for synchronization callbacks
      struct callbackarg {
         /// time interval
         typedef std::pair <tainsec_t, tainsec_t> timeinterval;
         /// time interval
         timeinterval		ival;
         /// measurement point
         int		measPoint;
         /// measurement period
         int		measPeriod;
      };
      typedef struct callbackarg callbackarg;
   
      /** Member function prototype for channel analysis callback.
          @param resultnum number of result record
          @param measnum measurement number
          @param chnname channel name (including array indices)
          @param stim true if stimulus readback channel
          @return true if successful
       ******************************************************************/
      typedef bool (stdtest::*channelAnalysis) 
      (int resultnum, int measnum, std::string chnname, bool stim, 
      const callbackarg& id);
   
      // friend
      friend class chnThreadArg;
   
      /// synchronization object for standard test
      typedef testsync <stdtest, callbackarg> stdtestsync;
   
      /// friend 
      friend class testsync <stdtest, callbackarg>;
   
      /// test access class for storage object
      const diagTest*	test;
      /// test type
      std::string	testType;
      /// true is analysis should be done; set false during pause
      bool		doAnalysis;
      /// time ahead (safe to read ahead)
      tainsec_t		timeAhead;
   
      /// start time: t = 0
      tainsec_t		T0;
      /// average type
      int		averageType;
      /// number of averages
      int		averages;
      /// size of average buffer, i.e. number of measurement points
      int		avrgsize;
      /// number of analyzed measurement periods
      int		avrgnum;
      /// time grid of test; used for determining the start time
      double		timeGrid;
      /// exciatations
      stimuluslist	stimuli;
      /// measurement channels
      measlist		meas;
      /// measurement intervals
      intervallist	intervals;
      /// queue of synchronization points
      std::deque<syncpointer>	syncqueue;
      /// number of data errors
      int		dataerrors;
   
   
      /** Read parameters. This function checks the storage, rttd and
          excitation manager pointer and reads the parameters (test type,
          averages, averageType) from the storage object. This function
          should be called by descendents. It also sets the test access
          class. Descendents should overwrite this function to read 
          their own parameters.
          @memo Read parameter method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool readParam (std::ostringstream& errmsg);
   
      /** Read measurement channel parameters. This function reads the
          parameters describing the measurement channel(s). This function
          is not automatically called, but rather has to be invoked by
          descendents. If the maximum index is larger than 1, array 
          indices are used to read in the measurement channels. At least
          one measurement channel must be specified for the routine to
          return successful.
          @memo Read measurement parameter method.
          @param errmsg error message stream
          @param maxIndex maximum array index of parameters
          @return true if successful
       ******************************************************************/
      virtual bool readMeasParam (std::ostringstream& errmsg, 
                        int maxIndex = 100);
   
      /** Read stimuli channel parameters. This function reads the
          parameters describing the excitation channel(s). This function
          is not automatically called, but rather has to be invoked by
          descendents. If the maximum index is larger than 1, array 
          indices are used to read in the measurement channels. The
          routine may return succesful without reading any stimulus
          channel.
          @memo Read stimuli parameter method.
          @param errmsg error message stream
          @param needReadback if true a readback channel must be set
          @param maxIndex maximum array index of parameters
          @return true if successful
       ******************************************************************/
      virtual bool readStimuliParam (std::ostringstream& errmsg, 
                        bool needReadback = false,
                        waveformset allowedWaveforms = allWaveforms,
                        int maxIndex = 100);
   
      /** Subscribes channels. This function subscribes excitation
   	  channels to the excitation manager and measurement channels
          to the rtdd manager. Usually, there is no need to overwrite
          this function.
          @memo Subscribe channels method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool subscribeChannels (std::ostringstream& errmsg);
   
   
      /** Returns the lowest and highest sampling frequencies. Given
          the highest frequency of interest calculates the required
          (highest) sampling frequency. It then checks measurement and
          stimulus readback channels and determines the lowest sampling
          frequency available (the lowest sampling frequency is always
          smaller than the highest one.) The sampling rates are always
          a power of two.
          @memo Sampling frequency range.
          @param fMax highest frequency of interest
          @param fSampleMin lowest sampling frequency (return)
          @param fSampleMax highest sampling frequency (return)
          @return rounded GPS time or <0 on error
       ******************************************************************/
      void samplingFrequencies (double fMax, double& fSampleMin, 
                        double& fSampleMax);
   
      /** Returns the heterodyne frequency. This function returns true,
          if all channels have the same heterodyne frequency. If none
          of the channels are heterodyned, the function returns also 
          true, but sets fZoom to zero.
          @memo Heterodyne frequency.
          @param fZoom heterodyne frequency (return)
          @return true if channels are consistent
       ******************************************************************/
      bool heterodyneFrequency (double &fZoom);
   							
      /** Calcluate the time grid. This function calculates the maximum
          sampling period of all stimulus readbacks and measurement
          channels. Additionally, the function will adjust the time
          grid, so that it is not much smaller than the specified maximum
          frequency. The returned time grid is always a power of 2.
          Optionally, this function can fine adjust the start time.
          @memo Calculate time grid method.
          @param fMax maximum frequency of interest
          @param t0 pointer to start time (ignored if 0)
          @return time grid
       ******************************************************************/
      virtual double calcTimeGrid (double fMax, tainsec_t* t0 = 0);
   
      /** Calcluates the measurement time. This function calculates start 
   	  time and duration of each measuremenmt period. This function
          must be overwritten by descendents. It can fine adjust
          the start time t0 if necessary.
          @memo Calculate measurement time method.
          @param errmsg error message stream
          @param t0 start time
          @return true if successful
       ******************************************************************/
      virtual bool calcTimes (std::ostringstream& errmsg,
                        tainsec_t& t0) = 0;
   
      /** Adds new measurement partitions. This function adds
          measurement partitions to the both stimuli readback and 
          measurement channels. Decimation factors are derived from
          the requested sampling frequency and the channel data rate.
          If fZoomSample is zero, it is ignored. Measurement partitions
          are added to the partition list of the channel structures.
          @memo Add measurement partitions method. 
          @param ival measurement interval
          @param pIndex partition index
          @param fSample requested sampling rate (before optional zoom)
          @param pt precursor time
          @param fZoom heterodyne frequency
          @param fZoomSample requested sampling frequency after zoom
          @param tZoom start time (t = 0) for modulation signal
          @return true if successful
       ******************************************************************/
      virtual bool addMeasPartitions (const interval& ival, int pIndex,
                        double fSample, tainsec_t pt = 0, double fZoom = 0, 
                        double fZoomSample = 0, tainsec_t tZoom = 0);
   
      /** Adds a new synchronization point. The synchronization
          point is automatically added to the synchronization queue.
          @memo Add synchronization point method. 
          @param ival measurement interval
          @param measPeriod index of measurement period
          @param measPoint index of measurement point
          @return true if successful
       ******************************************************************/
      virtual bool addSyncPoint (const interval& ival, 
                        int measPeriod, int measPoint = 0);
   
    /** Generates a new measurement point. By default just returns
          false. If overwritten by descendents, this function must
          add exaclty one new interval, one synchronization point and 
          one new measurement partition for every stimulus and 
          measurement channel. 
          @memo new measurement point method. 
          @param i measurement point index
          @param measPoint measurement index
          @return true if successful
       ******************************************************************/
      virtual bool newMeasPoint (int i, int measPoint = 0);
   
      /** Calculates measurement parameters. This function determines 
          excitation signals, partitions for the rtdd and synchronization 
          points. This function must be overwritten by descendents.
          @memo Add measurements method.
          @param errmsg error message stream
          @param t0 start time
          @param measPoint measurement point number
          @return true if successful
       ******************************************************************/
      virtual bool calcMeasurements (std::ostringstream& errmsg,
                        tainsec_t t0 = 0, int measPoint = 0) = 0;   
   
      /** Deletes measurements. This function deletes excitation signals,
   	  data partitions and synchronization points. Usually, there is 
          no need to overwrite this function.
          @memo Delete measurements method. 
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool delMeasurements (std::ostringstream& errmsg);
   
      /** Adds measurements. This function calls calcTimes and 
          calcMeasurements. Usually, there is no need to overwrite
          this function.
          @memo Add measurements method.
          @param errmsg error message stream
          @param t0 start time
          @param measPoint measurement point number
          @return true if successful
       ******************************************************************/
      virtual bool addMeasurements (std::ostringstream& errmsg,
                        tainsec_t t0 = 0, int measPoint = 0);
   
      /** Starts the measurement. This function switches the excitation
          signals on and initiates the data flow from the rtdd interface.
          Usually, there is no need to overwrite this function.
          @memo Start measurements method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool startMeasurements (std::ostringstream& errmsg);
   
      /** Stops the measurements. This function resets rtdd channels
          and optionally deletes data in the storage object beginning
          with firstIndex. If firstIndex is negative, no data objects
          are deleted. Usually, there is no need to 
          overwrite this function.
          @memo Stop measurements method. 
          @param firstIndex first data index to delete
          @return true if successful
       ******************************************************************/
      virtual bool stopMeasurements (int firstIndex = -1);
   
      /** Prepares the nex measurement point. This function will delete
          the outdated measurement points and prepare a new measurement
          point by calling newMeasPoint. New partitions are added from
          the end of the partition lists in stimuli and meas to the rtdd, 
          if newMeasPoint returns successful.
          @memo Prepare method. 
          @param id callback argument of current sync point
          @return true if successful
       ******************************************************************/
      bool prepareNextPoint (const callbackarg& id);
   
      /** Gets the next synchronization point. If no synchronization 
          points are left returns a 0 pointer. Usually, there is no need 
          to overwrite this function.
          @memo next sync method. 
          @param id callback argument of current sync point
          @param sync pointer to next synchronization point (return)
          @return true if successful
       ******************************************************************/
      virtual bool getNextSyncPoint (const callbackarg& id,
                        syncpointer& sync); 
   
      /** Callback of synchronization point. This function is called
   	  after each measurement step. It calls the analysis method and
          returns the next synchronization point. This function can 
          be overwritten by descendents.
          @memo Sync action method.
          @param id callback argument describing the sync event
          @param sync next synchronization point, or 0 if finished
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool syncAction (const callbackarg& id, syncpointer& sync,
                        bool& notify);
   
      /** Callback for aborting the test. This function is called
   	  when the user aborts the test. It stops the measurement.
          This function can be overwritten by descendents.
          @memo Sync abort method.
          @param id callback argument describing the sync event
          @return true if successful
       ******************************************************************/
      virtual bool syncAbort (const callbackarg& id);
   
      /** Callback for pausing the test. This function is called
   	  when the user pauses the test. It disables the analysis
          and sets the avera type to running. This function can be 
          overwritten by descendents.
          @memo Sync pause method.
          @param id callback argument describing the sync event
          @param sync next synchronization point, or 0 if continue
          @return true if successful
       ******************************************************************/
      virtual bool syncPause (const callbackarg& id, syncpointer& sync);
   
      /** Callback for resuming the test. This function is called
   	  when the user resumes the test. It disables the analysis
          and sets the avera type to running. This function can be 
          overwritten by descendents.
          @memo Sync abort method.
          @param id callback argument describing the sync event
          @param sync next synchronization point, or 0 if continue
          @return true if successful
       ******************************************************************/
      virtual bool syncResume (const callbackarg& id, syncpointer& sync);
   
      /** Callback for getting the maximum time data can be read ahead. 
          This function can be overwritten by descendents.
          @memo Sync time ahead method.
          @param ahead time ahead
          @return true if successful
       ******************************************************************/
      virtual bool syncTimeAhead (tainsec_t& ahead) const;
   
      /** Calls the specified method for every measuerement channel,
          and if needed for all or some stimulus channels. Returns
          if true if all channel analysis calls were successful. This
          function can be called from the analyze method to 
          automatically loop over all channels. If mt is true, the
          channel analysis methods are called by multiple threads.
          However, the function only returns when all analysis threads
          have finished.
          @memo Channel analyis method.
          @param id callback argument describing the sync event
          @param measnum measurement number
          @param method channel analysis function
          @param firstres number of first result record to use
          @param inclStim describes which stimulus are included
          @param async it true the channel methods are multi-threaded
          @param skipDuplicates skip duplicated channel channel names
          @return true if successful
       ******************************************************************/
      virtual bool callChannelAnalysis (const callbackarg& id, 
                        int measnum, channelAnalysis method, 
                        int firstres = 0, 
                        stimulustype inclStim = stimReadback, 
                        bool async = true, bool skipDuplicates = true); 
   
      /** Analysis routine which performs the analysis. This function 
          must be overwritten by descendents.
          @memo Analysis method.
          @param id callback argument describing the sync event
          @param measnum measurement number
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool analyze (const callbackarg& id, int measnum,
                        bool& notify) = 0;

      // JCB - Keep track of instance count
      private:
	 static int instance_count ;
	 int	myinstance ;
   };

}
#endif // _GDS_STDTEST_H
