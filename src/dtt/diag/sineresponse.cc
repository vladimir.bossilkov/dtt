/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: sinereponse						*/
/*                                                         		*/
/* Module Description: sine response test				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */

#include <time.h>
#include <math.h>
#include <complex>
#include <iostream>
#include "sineresponse.hh"
#include "diagnames.h"
#include "awgfunc.h"
#include "diagdatum.hh"
#include "gdsalgorithm.h"
#include "lud.hh"


   static const int my_debug=0 ;

   // maximum number of sampling points per (excitation) period
   const double _MAX_OVERSAMPLING = 4.0;
   // Time ahead buffers are allocated
   const double _TIME_AHEAD = 3.0; 
   // maximum number of harmonic order
   const int maxHarmonicOrder = 200;
   // maximum ramp in/out time of excitation
   const double _MAX_RAMPTIME = 10.0;


namespace diag {
   using namespace std;
   using namespace thread;


   sineresponse::tmpresult::tmpresult (tainsec_t T0, 
                     int SizeA, int SizeB, int SizeF) 
   : sizeA (0), sizeB (0), sizeF (0), t0 (T0), f (0), coeff (0), 
   cross (0), pwr (0)
   {
      allocate (SizeA, SizeB, SizeF);
   }


   sineresponse::tmpresult::~tmpresult () 
   
   {
      allocate ();
   }


   bool sineresponse::tmpresult::allocate (int SizeA, int SizeB, 
                     int SizeF)
   {
      // delete old arrays
      if (f != 0) {
         delete [] f;
         f = 0;
      }
      if (coeff != 0) {
         delete [] coeff;
         coeff = 0;
      }
      if (cross != 0) {
         delete [] cross;
         cross = 0;
      }
      if (pwr != 0) {
         delete [] pwr;
         pwr = 0;
      }
      // alocate new ones
      sizeA = SizeA;
      sizeB = SizeB;
      sizeF = SizeF;
      if ((sizeA + sizeB <= 0) || (sizeF <= 0)) {
         return true;
      }
      else {
         f = new (nothrow) double[sizeF];
         coeff = new (nothrow) complex<float>[(sizeA + sizeB) * sizeF];
         cross = new (nothrow) complex<float>[(sizeA + sizeB) * sizeF];
         pwr = new (nothrow) float[(sizeA + sizeB) * sizeF];
         return valid();
      }
   }


   bool sineresponse::tmpresult::valid () const
   {
      return ((f != 0) && (coeff != 0) && (cross != 0) && (pwr != 0));
   }


   sineresponse::tmpresult::tmpresult (const tmpresult& tmp) 
   {
      *this = tmp;
   }


   sineresponse::tmpresult& sineresponse::tmpresult::operator= (
                     const tmpresult& tmp)
   {
      if (this != &tmp) {
         t0 = tmp.t0;
         names = tmp.names;
         if (!allocate (tmp.sizeA, tmp.sizeB, tmp.sizeF)) {
            return *this;
         }
         memcpy (f, tmp.f, sizeF * sizeof (double));
         memcpy (coeff, tmp.coeff, 
                (sizeA + sizeB) * sizeF * sizeof (complex<float>));
         memcpy (cross, tmp.cross, 
                (sizeA + sizeB) * sizeF * sizeof (complex<float>));
         memcpy (pwr, tmp.pwr, 
                (sizeA + sizeB) * sizeF * sizeof (float));
      }
      return *this;
   }


   sineresponse::sineresponse () 
   : stdtest (sineresponsename),
   settlingTime (0), harmonicOrder (1), window (0), fftResult (false),
   fMin (0), fMax (0), fMaxMeas (0), fMaxSample (0), 
   mTime (0), mTimeAdd (0), pTime (0), sTime (0), rTime (0)
   {
      measTime[0] = -1;
      measTime[1] = -1;
   }


   diagtest* sineresponse::self () const 
   {
      return new (nothrow) sineresponse ();
   }


   bool sineresponse::end (std::ostringstream& errmsg)
   {
      semlock		lockit (mux);
      // delete temporary storage
      tmp.allocate();
   
      return stdtest::end (errmsg);
   }


   bool sineresponse::readParam (ostringstream& errmsg)
   {
      // call parent method
      if (!stdtest::readParam (errmsg)) {
         return false;
      }
   
      semlock		lockit (mux);
      bool		err = false;
   
      // read measurement time
      if (!test->getParam (*storage->Test, srMeasurementTime, *measTime, 2)) {
         errmsg << "Unable to load values from Test." <<
            srMeasurementTime << endl;
         err = true;
      }
      // read settling time
      if (!test->getParam (*storage->Test, srSettlingTime, settlingTime)) {
         errmsg << "Unable to load value from Test." <<
            srSettlingTime << endl;
         err = true;
      }
      // Read ramp down time
      if (!test->getParam (*storage->Test, srRampDown, rampDown)) {
         errmsg << "Unable to load value from Test." << srRampDown << endl ;
         err = true ;
      }
      if (my_debug) cerr << "  rampDown parameter = " << rampDown << endl ;
      // Read ramp up time
      if (!test->getParam (*storage->Test, srRampUp, rampUp)) {
         errmsg << "Unable to load value from Test." << srRampUp << endl ;
         err = true ;
      }
      if (my_debug) cerr << "  rampUp parameter = " << rampUp << endl ;
      // read window
      if (!test->getParam (*storage->Test, srWindow, window)) {
         errmsg << "Unable to load value from Test." <<
            srWindow << endl;
         err = true;
      }
   
      // read stimuli channel
      if (!readStimuliParam (errmsg, false, sinewaveOnly)) {
         return false;
      }
      if (stimuli.empty()) {
         errmsg << "No stimulus channel defined" << endl;
         err = true;
      }
   
      // read measurement channels
      if (!readMeasParam (errmsg)) {
         return false;
      }
   
      // check heterodyne frequency
      double fZoom = 0.0;
      if (!heterodyneFrequency (fZoom) || (fZoom != 0)) {
         errmsg << "Heterodyned channels not supported." << endl;
         err = true;
      }
   
      // read harmonic order
      if (!test->getParam (*storage->Test, srHarmonicOrder, harmonicOrder)) {
         errmsg << "Unable to load value from Test." <<
            srHarmonicOrder << endl;
         err = true;
      }
      if (harmonicOrder <= 0) {
         harmonicOrder = 1000;
      }
   
      // read fft result
      if (!test->getParam (*storage->Test, srFFTResult, fftResult)) {
         errmsg << "Unable to load value from Test." <<
            srFFTResult << endl;
         err = true;
      }
   
      return !err;
   }


   bool sineresponse::calcTimes (std::ostringstream& errmsg,
                     tainsec_t& t0)
   {
      semlock		lockit (mux);
      bool		err = false;
   
      if (my_debug) cerr << "sineresponse::calcTimes(..., t0 = " << t0 << ")" << endl ;
      // check averages
      if (averages < 1) {
         errmsg << "Number of averages must be at least one" << endl;
         err = true;
      }
      // check harmonic order
      if (harmonicOrder < 1) {
         errmsg << "Order of harmonics must be at least one" << endl;
         err = true;
      }
      if (err) {
         return false;
      }
      // number of result records
      rnumber = 2 + (((stimuli.size() == 1) || (stimuli.size() == 2)) ? 1 : 0);
      int sizeA = 0;
      for (stimuluslist::iterator iter = stimuli.begin(); iter != stimuli.end(); iter++) 
      {
         if (!iter->duplicate) {
            sizeA++;
         }
      }
      if ((sizeA == (int)stimuli.size()) && (sizeA > 1)) {
         rnumber++;
      }
   
      // determine smallest and largest excitation frequency
      fMin = 1E99;
      fMax = 0;
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         fMin = min (fMin, iter->freq);
         fMax = max (fMax, iter->freq);
      }
      if ((fMin <= 0) || (fMin > 1E98) || (fMax <= 0)) {
         errmsg << "Frequencies must be positive" << endl;
         return false;
      }
      // highest frequency of interest
      if (harmonicOrder > _MAX_OVERSAMPLING) {
         fMaxMeas = harmonicOrder * fMax;
      }
      else {
         fMaxMeas = _MAX_OVERSAMPLING * fMax;
      }
      if (my_debug) 
      {
         cerr << "timeseries::calcTimes() calling samplingFrequencies()" << endl ;
	 cerr << "   fMaxMeas   = " << fMaxMeas << endl ;
	 cerr << "   fMinSample = " << fMinSample << endl ;
	 cerr << "   fMaxSample = " << fMaxSample << endl ;
      }
      // determine sampling rates
      samplingFrequencies (fMaxMeas, fMinSample, fMaxSample);
      //if (my_debug)
      {
         cerr << "   LINE " << __LINE__ << endl ;
	 cerr << "   fMaxMeas   = " << fMaxMeas << endl ;
	 cerr << "   fMinSample = " << fMinSample << endl ;
	 cerr << "   fMaxSample = " << fMaxSample << endl ;
      }

      // check that the nyquist frequency, which is slowest
      // measurement sampling rate/2 is at least
      // harmonic order * highest excitation frequency
      // or 4x max excitation frequency.
      if(fMinSample < 2 * harmonicOrder * fMax)
      {
        errmsg << "Nyquist freq. < harmonic order * highest excitation freq." << endl;
        return false;
      }
   
      // calculate time grid
      timeGrid = calcTimeGrid (fMaxSample / 2.0, &t0);
      // precursor time
      // N_LEAD is defined as 24 in SignalProcessing/dttalgo/sineanalyze.h
      pTime = N_LEAD * timeGrid;
      if (my_debug) {
	 cerr << "  fMaxMeas    = " << fMaxMeas << endl ;
	 cerr << "  fMinSample  = " << fMinSample << endl ;
	 cerr << "  fMaxSample  = " << fMaxSample << endl ;
	 cerr << "  timeGrid    = " << timeGrid << endl ;
	 cerr << "  pTime       = " << pTime << endl ;
      }
   
      // calculate measurement time (mTime)
      if (my_debug) cerr << "  calling measurementTime(" << measTime[0] << ", " << measTime[1] << ", " << fMin << ", true)" << endl ;
      mTime = measurementTime (measTime[0], measTime[1], fMin, true);
      if ((mTime <= 0) || (timeGrid <= 0)) {
         errmsg << "Measurement time must be positive" << endl;
         return false;
      }
      if (my_debug) cerr << "  mTime = " << mTime << ", timeGrid = " << timeGrid << endl ;
      mTime += timeGrid;
      mTimeAdd = adjustForSampling (mTime, timeGrid) - mTime;
      if (my_debug) cerr << "  mTimeAdd = " << mTimeAdd << endl ;

      // calculate settling time (sTime)
      sTime = settlingTime * mTime;
      if (sTime < 0) {
         sTime = 0;
      }
      if (my_debug) cerr << "  sTime = " << sTime << endl ;

      // calculate ramp time (rTime); make sure dwell time lies on grid
      if (rampUp > sTime) {
	 rTime = rampUp ;
      }
      else
	 rTime = min (sTime, _MAX_RAMPTIME); // _MAX_RAMPTIME = 10.

      if (my_debug) cerr << "  ramp time = " << rTime << endl ;

      sTime = adjustForSampling (sTime + rTime, timeGrid) - rTime;
      if (my_debug) cerr << "  sTime adjusted to " << sTime << endl ;
      testExc->setRampDown ((tainsec_t)(rampDown * 1E9 + 0.5)) ;
      testExc->setRampUp((tainsec_t)(rampUp * 1E9 + 0.5)) ;
      if (my_debug) cerr << "  rampUp time is " << rampUp * 1E9 << endl ;
   
      // handle averaging
      avrgsize = averages;
      if (avrgsize * (mTime + mTimeAdd) < _TIME_AHEAD) {
         // need more buffers
         avrgsize = (int) (_TIME_AHEAD / (mTime + mTimeAdd));
      }
      // make sure we account for filter delay
      int onum = (int) (ceil ((mTime + mTimeAdd + 21./fMinSample) / 
                             (mTime + mTimeAdd + 1E-12)) + 0.1);
      // if (avrgsize < onum) {
         // avrgsize = onum;
      // }
      if (onum > 1) avrgsize += onum;
      avrgsize += 1;
      cout << "AVRGSIZE IS _________________" << avrgsize << endl;
   
      if (my_debug) cerr << "sineresponse::calcTimes() returns true" << endl ;
      return true;
   }


   bool sineresponse::newMeasPoint (int i, int measPoint)
   {
      semlock		lockit (mux);
   
      if (my_debug)
	 cerr << "sineresponse::newMeasPoint (i = " << i << ", measPoint = " << measPoint << ")" << endl ;
      // calulate start time
      tainsec_t	start = T0 + (tainsec_t) ((rTime + sTime + (double) (i + skipMeas) * (mTime + mTimeAdd)) * 1E9 + 0.5);
   
      if (my_debug)
      {
	 cerr << "  start = " << start << endl ;
	 cerr << "    T0       = "<< T0 << endl ;
	 cerr << "    rTime    = " << rTime << endl ;
	 cerr << "    skipMeas = " << skipMeas << endl ;
	 cerr << "    i        = " << i << endl ;
	 cerr << "    measTime = " << measTime[0] << ", " << measTime[1] << endl ;
	 cerr << "    mTimeAdd = " << mTimeAdd << endl ;
      }
      // check if too far behind
      if (RTmode) {
	 if (my_debug) cerr << "  RTmode = " << RTmode << endl ;
         tainsec_t now = TAInow();
         if (start < now + _EPOCH) {
	    if (my_debug)
	       cerr << "    now = " << now << ", _EPOCH = " << _EPOCH << endl ;
            skipMeas = (int) (((double) (now + _EPOCH - T0) / 1E9 - rTime - sTime) /
                     (mTime + mTimeAdd) + 0.99) - i;
	    if (my_debug)
	       cerr << "    skipMeas = " << skipMeas << endl ;
            if (skipMeas < 0) {
               skipMeas = 0;
            }
            start = T0 + (tainsec_t)
               ((rTime + sTime + (double) (i + skipMeas) * 
                (mTime + mTimeAdd)) * 1E9 + 0.5);
	    if (my_debug)
	       cerr << "    start recalculate, = " << start << endl ;
         }
      }
      // cerr << "start " << start << " " << rTime + sTime << " " <<
         // (mTime + mTimeAdd) << endl;
   
      // fine adjust to time grid & calc. duration
      start = fineAdjustForSampling (start, timeGrid);
      tainsec_t	duration = (tainsec_t) ((mTime + mTimeAdd + pTime) * 1E9 + 0.5);
      tainsec_t	tp = fineAdjustForSampling (pTime * 1E9 + 0.5, timeGrid);
   
      if (my_debug)
      {
	 cerr << "  start, duration after fineAdjustForSampling()" << endl ;
	 cerr << "    start = " << start << ", duration = " << duration << ", tp = " << tp << endl ;
      }

      // add interval
      intervals.push_back (interval (start, duration));
   
      // add new partitions
      if (!addMeasPartitions (intervals.back(), measPoint * averages + i, fMaxSample, tp)) {
         return false;
      }
   
      // add synchronization point
      if (!addSyncPoint (intervals.back(), i, measPoint)) {
         return false;
      }
   
      if (my_debug) cerr << "sineresponse::newMeasPoint() return true" << endl ;
      return true;
   }


   bool sineresponse::calcMeasurements (std::ostringstream& errmsg,
                     tainsec_t t0, int measPoint)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "sineresponse::calcMeasurements(..., t0 = " << t0 << ", measPoint = " << measPoint << ")" << endl ;
      // determine excitation signals
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (!iter->calcSineSignal (t0, -1, rTime * 1E9 + 0.5)) {
            errmsg << "Unable to calculate excitation signal" << endl;
            return false;
         }
      }
   
      // create initial measurement points
      skipMeas = 0;
      for (int i = 0; i < avrgsize; i++) {
         if (!newMeasPoint (i)) {
            errmsg << "Unable to create measurement points" << endl;
            return false;
         }
      }
      if (my_debug) cerr << "sineresponse::calcMeasurements() return true" << endl ;
      return true;
   }


   bool sineresponse::analyze (const callbackarg& id, int measnum, 
                     bool& note)
   {
      semlock		lockit (mux);
   
      // get result offset and access class
      const diagResult* aRes = 
         diagResult::self (stObjectTypeCoefficients);
      if (aRes == 0) {
         return false;
      }
      // determine number of excitations
      // sizeExc: number of excitation siganls
      // sizeF: number of frequencies to analyse
      // sizeA: number of different excitation (readback) channels
      // sizeB: number of different measurement channels
      int sizeExc = stimuli.size();
   
      /////////////////////////////////////////////////////////
      // Init analysis first time around		     //
      /////////////////////////////////////////////////////////
   
      if (measnum == 0) {
      
      /////////////////////////// Create temporary storage
         int sizeF = stimuli.size();
         if (sizeF == 1) {
            sizeF = (stimuli.front().freq < 1E-4) ? 1 :
               (int)((fMaxSample / 2.0) / stimuli.front().freq);
            if (sizeF > maxHarmonicOrder) {
               sizeF = maxHarmonicOrder;
            }
         }
         else if (sizeF == 2) {
            sizeF += 2;
         }
         int sizeA = 0;
         for (stimuluslist::iterator iter = stimuli.begin();
             iter != stimuli.end(); iter++) {
            if (!iter->duplicate) {
               sizeA++;
            }
         }	
         int sizeB = 0;
         for (measlist::iterator iter = meas.begin();
             iter != meas.end(); iter++) {
            if (!iter->duplicate) {
               sizeB++;
            }
         }
         tmp = tmpresult (T0 + (tainsec_t) (1E9 * rTime + 0.5),  
                         sizeA, sizeB, sizeF);
         if (!tmp.valid()) {
            return false;
         }
         int b = 0;
         for (stimuluslist::iterator iter = stimuli.begin();
             iter != stimuli.end(); iter++, b++) {
            tmp.f[b] = fabs (iter->freq);
            if (iter->duplicate) {
               continue;
            }
            if (iter->isReadback) {
               tmp.names.push_back (iter->readbackInfo.chName);
            }
            else {
               tmp.names.push_back (iter->excInfo.chName);
            }
         }
         for (measlist::iterator iter = meas.begin();
             iter != meas.end(); iter++) {
            if (iter->duplicate) {
               continue;
            }
            tmp.names.push_back (iter->info.chName);
         }
         // frequencies for harmonic analysis
         if (sizeExc == 1) {
            for (int a = 1; a < sizeF; a++) {
               tmp.f[a] = (a + 1.0) * tmp.f[0];
            }
         }
         // frequencies for two-tone intermodulation
         else if (sizeExc == 2) {
            tmp.f[2] = fabs (tmp.f[1] - tmp.f[0]);
            tmp.f[3] = fabs (tmp.f[1] + tmp.f[0]);
         }
      
      /////////////////////////// Create result: transfer coefficients
         string		resname = 
            diagObjectName::makeName (stResult, rindex);
         gdsDataObject* 	res = storage->findData (resname);
         if (res != 0) {
            storage->erase (resname);
         }
         res = aRes->newObject (0, sizeExc, tmp.sizeA + tmp.sizeB + 1, 
                              rindex, -1, gds_complex32);
         if (res == 0) {
            return false;
         }
         // set parameters of transfer coefficients
         aRes->setParam (*res, stCoefficientsSubtype, 4);
         aRes->setParam (*res, stCoefficientst0, tmp.t0);
         aRes->setParam (*res, stCoefficientsBW, 1 / (mTime + mTimeAdd));
         aRes->setParam (*res, stCoefficientsWindow, window);
         aRes->setParam (*res, stCoefficientsAverageType, averageType);
         aRes->setParam (*res, stCoefficientsAverages, averages);
         for (int b = 0; b < tmp.sizeA; b++) {
            string cn = diagObjectName::makeName (
                                 stCoefficientsChannelA, b);
            aRes->setParam (*res, cn, tmp.names[b]);
         }
         for (int b = 0; b < tmp.sizeB; b++) {
            string cn = diagObjectName::makeName (
                                 stCoefficientsChannelB, tmp.sizeA + b);
            aRes->setParam (*res, cn, tmp.names[tmp.sizeA + b]);
         }
         aRes->setParam (*res, stCoefficientsM, sizeExc);
         aRes->setParam (*res, stCoefficientsN, tmp.sizeA + tmp.sizeB);
         for (int a = 0; a < sizeExc; a++) {
            ((complex<float>*)res->value)[a * (tmp.sizeA + tmp.sizeB + 1)] = 
               tmp.f[a];
         }
         storage->addData (*res, false);
      
      /////////////////////////// Create results: coherence
         resname = diagObjectName::makeName (stResult, rindex + 1);
         res = storage->findData (resname);
         if (res != 0) {
            storage->erase (resname);
         }
         res = aRes->newObject (0, sizeExc, tmp.sizeA + tmp.sizeB + 1, 
                              rindex + 1, -1, gds_float32);
         if (res == 0) {
            return false;
         }
         // set parameters of coherence coefficients
         aRes->setParam (*res, stCoefficientsSubtype, 7);
         aRes->setParam (*res, stCoefficientst0, tmp.t0);
         aRes->setParam (*res, stCoefficientsBW, 1 / (mTime + mTimeAdd));
         aRes->setParam (*res, stCoefficientsWindow, window);
         aRes->setParam (*res, stCoefficientsAverageType, averageType);
         aRes->setParam (*res, stCoefficientsAverages, averages);
         for (int b = 0; b < tmp.sizeA; b++) {
            string cn = diagObjectName::makeName (
                                 stCoefficientsChannelA, b);
            aRes->setParam (*res, cn, tmp.names[b]);
         }
         for (int b = 0; b < tmp.sizeB; b++) {
            string cn = diagObjectName::makeName (
                                 stCoefficientsChannelB, tmp.sizeA + b);
            aRes->setParam (*res, cn, tmp.names[tmp.sizeA + b]);
         }
         aRes->setParam (*res, stCoefficientsM, sizeExc);
         aRes->setParam (*res, stCoefficientsN, tmp.sizeA + tmp.sizeB);
         for (int a = 0; a < sizeExc; a++) {
            ((float*)res->value)[a * (tmp.sizeA + tmp.sizeB + 1)] = tmp.f[a];
         }
         storage->addData (*res, false);
      
      /////////////////////////// Create result: transfer matrix
         resname = diagObjectName::makeName (stResult, rindex + 2);
         res = storage->findData (resname);
         if (res != 0) {
            storage->erase (resname);
         }
         if ((tmp.sizeA == sizeExc) && (sizeExc > 1)) {
            res = aRes->newObject (0, tmp.sizeA, tmp.sizeB, 
                                 rindex + 2, -1, gds_complex32);
            if (res == 0) {
               return false;
            }
            // set parameters of transfer coefficients
            aRes->setParam (*res, stCoefficientsSubtype, 8);
            aRes->setParam (*res, stCoefficientst0, tmp.t0);
            aRes->setParam (*res, stCoefficientsBW, 1 / (mTime + mTimeAdd));
            aRes->setParam (*res, stCoefficientsWindow, window);
            aRes->setParam (*res, stCoefficientsAverageType, averageType);
            aRes->setParam (*res, stCoefficientsAverages, averages);
            for (int b = 0; b < tmp.sizeA; b++) {
               string cn = diagObjectName::makeName (
                                    stCoefficientsChannelA, b);
               aRes->setParam (*res, cn, tmp.names[b]);
            }
            for (int b = 0; b < tmp.sizeB; b++) {
               string cn = diagObjectName::makeName (
                                    stCoefficientsChannelB, tmp.sizeA + b);
               aRes->setParam (*res, cn, tmp.names[tmp.sizeA + b]);
            }
            aRes->setParam (*res, stCoefficientsM, tmp.sizeA);
            aRes->setParam (*res, stCoefficientsN, tmp.sizeB);
            storage->addData (*res, false);
         }
      
      /////////////////////////// Create results: harmonic & 2-tone
         int rnum = rindex + 2;
         if ((tmp.sizeA == sizeExc) && (sizeExc > 1)) {
            rnum++;
         }
         resname = diagObjectName::makeName (stResult, rnum);
         res = storage->findData (resname);
         if (res != 0) {
            storage->erase (resname);
         }
         if ((sizeExc == 1) || (sizeExc == 2)) {
            int rows = (sizeExc == 1) ? tmp.sizeF + 1 : tmp.sizeF;
            res = aRes->newObject (0, rows, tmp.sizeA + tmp.sizeB + 1, 
                                 rnum, -1, gds_complex32);
            if (res == 0) {
               return false;
            }
            // set parameters of harmonic/2-tone intermod. coeff.
            int subtype = (stimuli.size() == 1) ? 5 : 6;
            aRes->setParam (*res, stCoefficientsSubtype, subtype);
            aRes->setParam (*res, stCoefficientst0, tmp.t0);
            aRes->setParam (*res, stCoefficientsBW, 1 / (mTime + mTimeAdd));
            aRes->setParam (*res, stCoefficientsWindow, window);
            aRes->setParam (*res, stCoefficientsAverageType, averageType);
            aRes->setParam (*res, stCoefficientsAverages, averages);
            for (int b = 0; b < tmp.sizeA; b++) {
               string cn = diagObjectName::makeName (
                                    stCoefficientsChannelA, b);
               aRes->setParam (*res, cn, tmp.names[b]);
            }
            for (int b = 0; b < tmp.sizeB; b++) {
               string cn = diagObjectName::makeName (
                                    stCoefficientsChannelB, tmp.sizeA + b);
               aRes->setParam (*res, cn, tmp.names[tmp.sizeA + b]);
            }
            aRes->setParam (*res, stCoefficientsM, rows);
            aRes->setParam (*res, stCoefficientsN, tmp.sizeA + tmp.sizeB);
            complex<float>* p = (complex<float>*)res->value;
            if (sizeExc == 1) {
               *p = 0.0;
               p += tmp.sizeA + tmp.sizeB + 1;
            }
            for (int a = 0; a < tmp.sizeF; a++) {
               *p = tmp.f[a];
               p += tmp.sizeA + tmp.sizeB + 1;
            }
            storage->addData (*res, false);
         }
      
      /////////////////////////// Create index
         const diagIndex& 	indx = diagIndex::self();
         // find index / create new index if necessary
         gdsDataObject* 	iobj = storage->findData (stIndex);
         if (iobj == 0) {
            iobj = indx.newObject (0);
            if (iobj == 0) {
               return false;
            }
            storage->addData (*iobj, false);
         }
      
         // write index 
         ostringstream	entry1;
         // transfer coefficients & coherence entries
         for (int b = 0; b < tmp.sizeA + tmp.sizeB; b++) {
            diagIndex::channelEntry (entry1, b, tmp.names[b], 
                                 (b < tmp.sizeA) ? 'A' : 'B');
         }
         string channelentries (entry1.str());
         int	N = sizeExc * (tmp.sizeA + tmp.sizeB);
         diagIndex::resultEntry (entry1, rindex, 0, N, -1);
         indx.setEntry (*iobj, icTransferCoeff, step, entry1.str());
      
         ostringstream	entry2;
         entry2 << *channelentries.c_str() << 
            (channelentries.c_str() + 1);  // compiler nonsens
         diagIndex::resultEntry (entry2, rindex + 1, 0, N, -1);
         indx.setEntry (*iobj, icCoherenceCoeff, step, entry2.str());
         // transfer matrix
         rnum = 2;
         if ((tmp.sizeA == sizeExc) && (sizeExc > 1)) {
            ostringstream	entry3;
            entry3 << *channelentries.c_str() << 
               (channelentries.c_str() + 1); // compiler nonsense
            N = tmp.sizeA * tmp.sizeB;
            diagIndex::resultEntry (entry3, rindex + 2, 0, N, -1);
            indx.setEntry (*iobj, icTransferMatrix, step, entry3.str());
            rnum++;
         }
         // harmonic or two-tone intermod. entry
         if ((sizeExc == 1) || (sizeExc == 2)) {
            ostringstream	entry3;
            entry3 << *channelentries.c_str() << 
               (channelentries.c_str() + 1); // compiler nonsense
            int rows = (sizeExc == 1) ? tmp.sizeF + 1 : tmp.sizeF;
            N = rows * (tmp.sizeA + tmp.sizeB);
            diagIndex::resultEntry (entry3, rnum, 0, N, -1);
            string ic = (stimuli.size() == 1) ? 
               icHarmonicCoeff : icIntermodulationCoeff;
            indx.setEntry (*iobj, ic, step, entry3.str());
            // cerr << "entry3 = " << entry3.str() << endl;
         }
         // cerr << "entry1 = " << entry1.str() << endl;
         // cerr << "entry2 = " << entry2.str() << endl;
      }
   
      /////////////////////////////////////////////////////////
      // Calculate sine amplitude/phase of all channels	     //
      /////////////////////////////////////////////////////////
   
      // analyze all measurement channels
      if (!callChannelAnalysis (id, measnum, 
                           (channelAnalysis) &sineresponse::sinedet, 
                           0, stimAll)) {
         return false;
      }
   
      /////////////////////////////////////////////////////////
      // Calculate cross & power coefficients		     //
      /////////////////////////////////////////////////////////
   
      tmpresult		x (0, tmp.sizeA, tmp.sizeB, tmp.sizeF);
      // tmp.f: frequency points
      // tmp.coeff: current amplitude/phase coefficients
      // tmp.pwr: averaged power coefficients
      // tmp.cross: averaged cross-correlation coefficients
      // x.coeff: current transfer coeffiecients
      // x.pwr: current power coefficients
      // x.cross: current cross-correlation coefficients
      for (int a = 0; a < tmp.sizeF; a++) {
         for (int b = 0; b < tmp.sizeA + tmp.sizeB; b++) {
            x.pwr[a * (tmp.sizeA + tmp.sizeB) + b] = 
               norm (tmp.coeff[a * (tmp.sizeA + tmp.sizeB) + b]);
            x.cross[a * (tmp.sizeA + tmp.sizeB) + b] = 
               tmp.coeff[a * (tmp.sizeA + tmp.sizeB) + b] * 
               conj (tmp.coeff[a * (tmp.sizeA + tmp.sizeB) + a]);
         }
      }
      // average
      avg_specs		avgprm;
      avgprm.avg_type = (averageType != 1) ? 
         AVG_LINEAR_VECTOR : AVG_EXPON_VECTOR;
      avgprm.dataset_length = tmp.sizeF * (tmp.sizeA + tmp.sizeB);
      avgprm.data_type = DATA_REAL;
      avgprm.number_of_averages = averages;
      int		num_so_far = measnum;
      if (avg (&avgprm, 1, x.pwr, &num_so_far, tmp.pwr) < 0) {
         return false;
      }
      avgprm.data_type = DATA_COMPLEX;
      num_so_far = measnum;
      if (avg (&avgprm, 1, (float*) x.cross, &num_so_far, 
              (float*) tmp.cross) < 0) {
         return false;
      }
   
      /////////////////////////////////////////////////////////
      // Calculate transfer coefficients		     //
      /////////////////////////////////////////////////////////
   
      // get transfer function result object
      string		resname = 
         diagObjectName::makeName (stResult, rindex);
      gdsDataObject* 	res = storage->findData (resname);
      if (res == 0) {
         return false;
      }
      // calculate transfer function coefficients
      // (sum over sizeA frequency components only)
      for (int a = 0; a < sizeExc; a++) {
         for (int b = 0; b < tmp.sizeA + tmp.sizeB; b++) {
            if (a != b) {
               x.coeff[a * (tmp.sizeA + tmp.sizeB) + b] =  
                  tmp.coeff[a * (tmp.sizeA + tmp.sizeB) + b] /
                  tmp.coeff[a * (tmp.sizeA + tmp.sizeB) + a];
            }
            else {
               x.coeff[a * (tmp.sizeA + tmp.sizeB) + a] =  
                  tmp.coeff[a * (tmp.sizeA + tmp.sizeB) + a];
            }
         }
      }
      // average
      avgprm.avg_type = (averageType != 1) ? 
         AVG_LINEAR_VECTOR : AVG_EXPON_VECTOR;
      avgprm.dataset_length = (tmp.sizeA + tmp.sizeB);
      avgprm.data_type = DATA_COMPLEX;
      avgprm.number_of_averages = averages;
      for (int a = 0; a < sizeExc; a++) {
         num_so_far = measnum;
         if (avg (&avgprm, 1, (float*) (x.coeff + a * (tmp.sizeA + tmp.sizeB)), 
                 &num_so_far, (float*) res->value + 
                 2 * (a * (tmp.sizeA + tmp.sizeB + 1) + 1)) < 0) {
            return false;
         }
      }
      aRes->setParam (*res, stCoefficientsAverages, 
                     (averageType != 1) ? measnum + 1 : 
                     min (measnum + 1, averages));
   
      /////////////////////////////////////////////////////////
      // Calculate transfer matrix.			     //
      /////////////////////////////////////////////////////////
      int rnum = rindex + 2;
      if ((tmp.sizeA == sizeExc) && (sizeExc > 1)) {
         // get result record
         resname = diagObjectName::makeName (stResult, rnum);
         gdsDataObject* res2 = storage->findData (resname);
         if (res2 == 0) {
            return false;
         }
      
         // declare matrix A, B and M
         complex<double>*	aMatrix = 
            new (nothrow) complex<double>[tmp.sizeA * tmp.sizeA];
         complex<double>*	bMatrix = 
            new (nothrow) complex<double>[tmp.sizeA * tmp.sizeB];
         complex<double>*	mMatrix = 
            new (nothrow) complex<double>[tmp.sizeA * tmp.sizeB];
         complex<double>**	aMat = 
            nrMatrix (aMatrix, tmp.sizeA, tmp.sizeA);
         complex<double>**	bMat = 
            nrMatrix (bMatrix, tmp.sizeA, tmp.sizeB);
         complex<double>**	mMat = 
            nrMatrix (mMatrix, tmp.sizeA, tmp.sizeB);
      
         if ((aMatrix == 0) || (bMatrix == 0) || (mMatrix == 0) ||
            (aMat == 0) || (bMat == 0) || (mMat == 0)) {
            delete [] aMatrix;
            delete [] bMatrix;
            delete [] mMatrix;
            nrMatrixFree (aMat);
            nrMatrixFree (bMat);
            nrMatrixFree (mMat);
            return false;
         }
         // get matrix A and B
         for (int a = 0; a < tmp.sizeA; a++) {
            for (int b = 0; b < tmp.sizeA; b++) {
               if (a != b) {
                  aMat[a+1][b+1] = ((complex<float>*) res->value)
                     [a * (tmp.sizeA + tmp.sizeB + 1) + b + 1];
               }
               else {
                  aMat[a+1][b+1] = 1.0;
               }
            }
            for (int b = 0; b < tmp.sizeB; b++) {
              bMat[a+1][b+1] = ((complex<float>*) res->value)
                  [a * (tmp.sizeA + tmp.sizeB + 1) + tmp.sizeA + b + 1];
            }
         }
         // multiply A^(-1) with B: M = A^(-1) * B
         if (imul (mMat, aMat, bMat, tmp.sizeA, tmp.sizeB) < 0) {
            // on error set result to zero
            cerr << "error in matrix inversion" << endl;
            for (int a = 0; a < tmp.sizeA; a++) {
               for (int b = 0; b < tmp.sizeB; b++) {
                  mMat[a+1][b+1] = 0.0;
               }
            }
         }
      	 // store result and free temporary storage
         for (int a = 0; a < tmp.sizeA; a++) {
            for (int b = 0; b < tmp.sizeB; b++) {
               ((complex<float>*)res2->value)[a * tmp.sizeB + b] =
                  complex<float> (mMat[a+1][b+1]);
            }
         }
         delete [] aMatrix;
         delete [] bMatrix;
         delete [] mMatrix;
         nrMatrixFree (aMat);
         nrMatrixFree (bMat);
         nrMatrixFree (mMat);
         rnum++;
         aRes->setParam (*res2, stCoefficientsAverages, 
                        (averageType != 1) ? measnum + 1 : 
                        min (measnum + 1, averages));
      }
   
      /////////////////////////////////////////////////////////
      // Calculate coherence				     //
      /////////////////////////////////////////////////////////
   
      // get coherence result object
      resname = diagObjectName::makeName (stResult, rindex + 1);
      res = storage->findData (resname);
      if (res == 0) {
         return false;
      }
      // calculate coherence
      // (sum over sizeA frequency components only)
      for (int a = 0; a < sizeExc; a++) {
         for (int b = 0; b < tmp.sizeA + tmp.sizeB; b++) {
            float denom = 
               tmp.pwr [a * (tmp.sizeA + tmp.sizeB) + b] * 
               tmp.pwr [a * (tmp.sizeA + tmp.sizeB) + a];
            ((float*) res->value)[a * (tmp.sizeA + tmp.sizeB + 1) + b + 1] =
               (denom > 0) ?
               norm (tmp.cross [a * (tmp.sizeA + tmp.sizeB) + b]) / denom : 0;
         }
      }
      aRes->setParam (*res, stCoefficientsAverages, 
                     (averageType != 1) ? measnum + 1 : 
                     min (measnum + 1, averages));
   
      /////////////////////////////////////////////////////////
      // Calculate harmonic distortion & two-tone intermod.  //
      /////////////////////////////////////////////////////////
   
      if ((sizeExc == 1) || (sizeExc == 2)) {
         // get coherence result object
         resname = diagObjectName::makeName (stResult, rnum);
         res = storage->findData (resname);
         if (res == 0) {
            return false;
         }
         // average
         avgprm.avg_type = (averageType != 1) ? 
            AVG_LINEAR_VECTOR : AVG_EXPON_VECTOR;
         avgprm.dataset_length = tmp.sizeA + tmp.sizeB;
         avgprm.data_type = DATA_COMPLEX;
         avgprm.number_of_averages = averages;
         int  ndx = 1;
      	 // make room for THD
         if (sizeExc ==  1) {
            ndx += tmp.sizeA + tmp.sizeB + 1;
         }
         for (int a = 0; a < tmp.sizeF; a++) {
            num_so_far = measnum;
            if (avg (&avgprm, 1, (float*) tmp.coeff + 
                    2 * a * (tmp.sizeA + tmp.sizeB), &num_so_far, 
                    (float*) res->value + 2 * ndx) < 0) {
               return false;
            }
            ndx += tmp.sizeA + tmp.sizeB + 1;
         }
      	 // calculate THD
         if (sizeExc ==  1) {
            complex<float>*	p = (complex<float>*) res->value + 1;
            for (int b = 0; b < tmp.sizeA + tmp.sizeB; b++, p++) {
               double thd = 0.0;
               for (int a = 1; a < tmp.sizeF; a++) {
                  thd += norm (p[(a+1) * (tmp.sizeA + tmp.sizeB + 1)]);
               }
               double denom = abs (p[tmp.sizeA + tmp.sizeB + 1]);
               if (denom > 0) {
                  p[0] = sqrt (thd) / denom;
               }
               else {
                  p[0] = 1E99;
               }
            }
         }	
         aRes->setParam (*res, stCoefficientsAverages, 
                        (averageType != 1) ? measnum + 1 : 
                        min (measnum + 1, averages));
      }
   
      // return
      note = true;
      return true;
   }


   bool sineresponse::sinedet (int resultnum, int measnum, 
                     string chnname, bool stim, const callbackarg& id)
   {
      if (my_debug) cerr << "analyze " << chnname << " from " << measnum << " into " << resultnum << endl;
   
      // stimulus channels without a readback
      if (stim && chnname.empty()) {
         complex<double> 	x;
         double			t;
         t = (double) (id.ival.first - tmp.t0) / 1E9 + pTime;
         for (int b = 0; b < tmp.sizeF; b++) {
            if (b != resultnum) {
               x = 0;
            }
            else {
               x = stimuli[b].ampl * 
                  exp (complex<double>(0,TWO_PI) * tmp.f[b] * t);
            }
            tmp.coeff[b * (tmp.sizeA + tmp.sizeB) + resultnum] = 
               complex<float> (x);
         }
      }
      
      // measurement channels & stimulus readback channels
      else {
         // get time series access class
         const diagResult& aChn = diagChn::self();
         // get time series
         gdsDataObject* chndat = storage->findData (chnname);
         if (chndat == 0) {
            return false;
         }
         // get time series parameters
         int N;
         if (!aChn.getParam (*chndat, stTimeSeriesN, N)) {
            return false;
         }
         tainsec_t t0;
         if (!aChn.getParam (*chndat, stTimeSeriest0, t0)) {
            return false;
         }
         double dt;
         if (!aChn.getParam (*chndat, stTimeSeriesdt, dt) || (dt <= 0)) {
            return false;
         }
         double tp;
         if (!aChn.getParam (*chndat, stTimeSeriestp, tp) || (tp < 0)) {
            return false;
         }
         int N0 = (int) (tp / dt + 0.5) - N_LEAD;
         if (N0 < 0) {
            return false;
         }
         double tstart = (double) (t0 - tmp.t0) / 1E9 + N0 * dt;
      
         // sine detection
         dCmplx x;
         for (int a = 0; a < tmp.sizeF; a++) {
            if (sineAnalyze (1, window, (float*) chndat->value + N0, 
                            N - N0, 1 / dt, tmp.f[a], 1, tstart, 
                            &x, &x) < 0) {
               return false;
            }
            // cerr << "sdet a = " << a << "(" << N0 << ") at " << tmp.f[a] << 
               // " coeff = " << (complex<double>&) x << endl;
            tmp.coeff[a * (tmp.sizeA + tmp.sizeB) + resultnum] = 
	      complex<float> (x.re, x.im);
         }
      }
   
      return true;
   }


}
