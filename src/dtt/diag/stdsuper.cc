/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: stdsuper						*/
/*                                                         		*/
/* Module Description:standard diagnostics supervisory task		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef DEBUG
#define DEBUG
#endif

/* Header File List: */
#include "gdserr.h"
#include "stdsuper.hh"
#include "diagorg.hh"
#include "rtddinput.hh"
#include "diagdatum.hh"
#include <iostream>
#include <cmath>
#include <string>
#include <cstdio>

#ifdef gdsDebug
#undef gdsDebug
#endif
#define gdsDebug(msg) \
   gdsDebugMessageEx (msg, __FILE__, __LINE__); \
   fprintf (stderr, "%s in %s at line %i\n", msg, __FILE__, __LINE__)

static const int my_debug = 0 ;

namespace diag {
   using namespace std;
   using namespace thread;


   const double __ONESEC = (double) _ONESEC;

//    // timeout for receiving data (real-time interface)
   // const tainsec_t timeoutWaitRT = 5 * _ONESEC;
//    // timeout for receiving data (off-line interface)
   // const tainsec_t timeoutWaitOL = 20 * _ONESEC;


   standardsupervisory::~standardsupervisory ()
   {
      setEnvironmentExcitationManager (0);
   }


   bool standardsupervisory::setup () 
   {
      semlock		lockit (mux);
   
      /* return on error */
      if (!basic_supervisory::setup()) {
         return false;
      }
   
      /* check storage */
      if (storage == 0) {
         errmsg << "No diagnostics parameters" << endl;
         return false;
      }
      /* erase old result */
      storage->eraseResults();
   
      /* check test point manager */
      if (tpMgr == 0) {
         errmsg << "No test point management functions" << endl;
         return false;
      }
   
      /* check test iterator name */
      const testiterator* ti = getTestIterator (*storage);
      if (ti == 0) {
         errmsg << "Unknown test iterator" << endl;
         return false;
      }
   
      /* check test name */
      const diagtest*	tst = getTest (*storage);
      if (tst == 0) {
         errmsg << "Unknown test" << endl;
         return false;
      }
   
      /* setup test */
      test.reset (tst->self());
      if (test.get() == 0) {
         errmsg << "Unable to create test object" << endl;
         return false;
      }
      if (!test->init (*storage, notify, *dataMgr, testEnvExc, testExc,
                      keepTraces, RTmode)) {
         errmsg << "Unable to initialize test object" << endl;
         return false;
      }
   
      /* setup test iterator */
      testiter.reset (ti->self());
      if (testiter.get() == 0) {
         errmsg << "Unable to create test iterator object" << endl;
         return false;
      }
      if (!testiter->init (*storage, iterExc, *test, notify)) {
         errmsg << "Unable to initialize test iterator object" << endl;
         return false;
      }
   
      /* initialze excitation managers */
      if (!envExc.init (*tpMgr, noStimulus || !RTmode) ||
         !envExc.setSiteIfo (siteDefault, siteForce, 
                           ifoDefault, ifoForce) ||
         !iterExc.init (*tpMgr, noStimulus || !RTmode) ||
         !iterExc.setSiteIfo (siteDefault, siteForce, 
                           ifoDefault, ifoForce) ||
         !testEnvExc.init (*tpMgr, noStimulus || !RTmode) ||
         !testEnvExc.setSiteIfo (siteDefault, siteForce, 
                           ifoDefault, ifoForce) ||
         !testExc.init (*tpMgr, noStimulus || !RTmode) ||
         !testExc.setSiteIfo (siteDefault, siteForce, 
                           ifoDefault, ifoForce)) {
         errmsg << "Unable to initialize excitation managers" << endl;
         return false;
      }
      setEnvironmentExcitationManager (&envExc);
   
      /* check validity of sync channel names */
   
      return true;
   }


   bool standardsupervisory::run ()
   {
      if (RTmode) {
         return runRT();
      }
      else {
         return runOL();
      }
   }


   bool standardsupervisory::runRT ()
   {
      semlock		lockit (mux);
      tainsec_t		timeEnvOn;	// time when env. was switched on
      tainsec_t		timeTPactive;	// time when TPs become active
      tainsec_t		timeIteratorOn;	// time when iterator is ready
      tainsec_t		timeTestOn;	// time when test starts
      tainsec_t		envDwellTime;	// dwell time of env.
      tainsec_t		iterDwellTime;	// iterator dwell time
      tainsec_t		testExcDwellTime;// test exc. dwell time
      double		timeAhead;	// 
      int		iteration;	// iteration count
      bool		continueIteration; // 
      syncpointer	sync;		// synchronization point
      bool		testabort = false;// test abort flag
      int		rindex;		// result index
   
      gdsDebug ("standard supervisory: Initialization");
   
      /* check storage */
      if (storage == 0) {
         errmsg << "No diagnostics parameters" << endl;
         return false;
      }
   
      /* check rtdd manager */
      if (dataMgr == 0) {
         errmsg << "No data distribution management functions" << endl;
         return false;
      }
   
      /* check test point manager */
      if (tpMgr == 0) {
         errmsg << "No test point management functions" << endl;
         return false;
      }
   
      /* check test and test iterator */
      if ((test.get() == 0) || (testiter.get() == 0)) {
         errmsg << "No test and/or test iterator" << endl;
         return false;
      }
   
      /////////////////////////////////////////////////////////
      // Synchronization 1				     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Synchronization 1");
   
      /* check start time */
      if (start > 0) {
         timeAhead = (double) (start - TAInow()) / __ONESEC + wait;
      }
      else {
         timeAhead = wait;
      }
      if (timeAhead > 10) {
         if (!timeWait (timeAhead - 10)) {
            errmsg << "Test aborted" << endl;
            return false;
         }
      }
   
      /* wait for trigger */
      if (!waitForStart.empty()) {
         if (!triggerWait (waitForStart)) {
            errmsg << "Test aborted" << endl;
            return false;
         }
      }
   
      /* send notification */
      notify.send (cmdnotify::begin);
      rindex = 0;
   
      /////////////////////////////////////////////////////////
      // Global Environment Setup			     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Global Environment Setup");
   
      /* read environment parameters */
      if (!readEnvironment ()) {
         errmsg << "Unable to read global environment variables" << endl;
         return false;
      }
      /* subscribe environment */
      if (!subscribeEnvironment (errmsg)) {
         errmsg << "Unable to setup global environment" << endl;
         return false;
      }
   
      /////////////////////////////////////////////////////////
      // Private Environment Setup			     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Private Environment Setup");
   
      /* read environment parameters */
      if (!test->readEnvironment ()) {
         errmsg << "Unable to read private environment variables" << endl;
         return false;
      }
      /* subscribe environment */
      if (!test->subscribeEnvironment (errmsg)) {
         errmsg << "Unable to setup private environment" << endl;
         return false;
      }
   
      /////////////////////////////////////////////////////////
      // Switch Environments On				     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Switch Environments On");
   
      /* set test points of environments */
      if (!tpMgr->set (&timeEnvOn)) {
         errmsg << "Unable to select test points for environment" << endl;
         return false;
      }
      /* wait for test points */
      while ((timeEnvOn > 0) && (timeEnvOn > TAInow())) {
         timeWait (0.01);
      }
   
      /* switch on global environment */
      if (!startEnvironment ()) {
         errmsg << "Unable to switch global environment on" << endl;
         envExc.del();
         return false;
      }
      /* switch on private environment */
      if (!test->startEnvironment ()) {
         errmsg << "Unable to switch private environment on" << endl;
         envExc.del();
         testEnvExc.del();
         return false;
      }
      timeEnvOn = TAInow();
      envDwellTime = (tainsec_t) 
         (max (envExc.dwellTime(), testEnvExc.dwellTime())*(double) _ONESEC);
   
      /////////////////////////////////////////////////////////
      // Iterator Initialization			     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Iterator Initialization");
   
      /* subscribe iterator channels */
      if (!testiter->begin (errmsg)) {
         errmsg << "Unable to setup iterator channels" << endl;
         testabort = true;
      }
      iterDwellTime = (tainsec_t)
         (iterExc.dwellTime() * (double) _ONESEC);
   
      /////////////////////////////////////////////////////////
      // Test Initialization				     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Test Initialization");
   
      /* subscribe measurement & excitation channels */
      if (!test->begin (errmsg)) {
         errmsg << "Unable to setup test channels" << endl;
         testabort = true;
      }
      testExcDwellTime = (tainsec_t) 
         (testExc.dwellTime() * (double) _ONESEC);
   
      /* set test points of iterator & test */
      if (!testabort && !tpMgr->set (&timeTPactive)) {
         errmsg << "Unable to select test points" << endl;
         testabort = true;
      }
   
      /* set channels of rtdd */
      // cerr << "tp time = " << timeTPactive << endl;
      if (!testabort && !dataMgr->set (timeTPactive, &timeTPactive)) {
         errmsg << "Unable to obtain measurement data" << endl;
         testabort = true;
      }
      // cerr << "nds time = " << timeTPactive << endl;
      // cerr << "time = " << TAInow() << endl;
   
      /////////////////////////////////////////////////////////
      // Iterator Setup					     //
      /////////////////////////////////////////////////////////
      iteration = -1;
      do {
         if (testabort) {
            break;
         }
         iteration++;
      
         gdsDebug ("standard supervisory: Iterator Setup");
      
         /* switch iterator channels on if required */
         timeIteratorOn = 
            max (max (timeTPactive, timeEnvOn + envDwellTime), TAInow());
         if (!testiter->setup (errmsg, timeIteratorOn)) {
            errmsg << "Unable to start test iterator" << endl;
            testabort = true;
            break;
         }
      
      /////////////////////////////////////////////////////////
      // Synchronization 2				     //
      /////////////////////////////////////////////////////////
      
         gdsDebug ("standard supervisory: Synchronization 2");
      
         /* wait for trigger */
         if (!waitAtEachStep.empty()) {
            if (!triggerWait (waitAtEachStep)) {
               errmsg << "Test aborted" << endl;
               testabort = true;
               break;
            }
         }
      
         /* calculate test start time */
         timeTestOn =
            max (timeIteratorOn + iterDwellTime,
                TAInow() + testExcDwellTime + _EPOCH);
         if (iteration == 0) {
            setMeasurementTime (timeTestOn);
         }
      
         /* send notification */
         notify.send (cmdnotify::testBegin);
      
      /////////////////////////////////////////////////////////
      // Test Setup					     //
      /////////////////////////////////////////////////////////
      
         gdsDebug ("standard supervisory: Test Setup");
      
         /* start excitation and measurement */
         sync.reset();
         test->setStep (iteration, rindex);
         if (!test->setup (errmsg, timeTestOn, sync)) {
            errmsg << "Unable to start test" << endl;
            testabort = true;
            break;
         }
      
      /////////////////////////////////////////////////////////
      // Synchronization 3				     //
      // Test Intermediate Analysis			     //
      // Synchronization 4				     //
      // Test Final Analysis				     //
      /////////////////////////////////////////////////////////
      
         bool 		testPaused = false;
         while ((sync.get() != 0) && (!!*sync)) {
            gdsDebug ("standard supervisory: Synchronization 3/4");
         
            switch (syncWait (*sync, testPaused)) {
               // pause test
               case paused: 
                  {
                     syncpointer	next;
                     if (testPaused || sync->pause (next)) {
                        testPaused = true;
                        if (next.get() != 0) {
                           sync = next;
                        }
                        continue;
                     }
                     else {
                        errmsg << "Unable to pause test" << endl;
                        testabort = true;
                        sync.reset();
                     }
                     break;
                  }
               // resume test
               case resumed:
                  {
                     syncpointer	next;
                     if (!testPaused || sync->resume (next)) {
                        testPaused = false;
                        if (next.get() != 0) {
                           sync = next;
                        }
                        continue;
                     }
                     else {
                        errmsg << "Unable to resume test" << endl;
                        testabort = true;
                        sync.reset();
                     }
                     break;
                  }
               // call analysis algorithms
               case normal:
                  {
                     syncpointer	next;
                     bool		note;
                     if (!sync->action (next, note)) {
                        errmsg << "Synchronization error" << endl;
                        testabort = true;
                        break;
                     }
                     sync = next;
                     // send notification
                     if (note) {
                        notify.send (cmdnotify::testAnalysis);
                     }
                     break;
                  }
               // test aborted
               case aborted:
                  {
                     sync->abort ();
                     errmsg << "Test aborted" << endl;
                     testabort = true;
                     sync.reset();
                     break;
                  }
               // test timed out
               case disconnected:
                  {
                      sync->abort();
                      errmsg << "Disconnected from data source" << endl;
                      testabort = true;
                      sync.reset();
                      break;
                  }
               case no_data:
                  {
                     sync->abort ();
                     errmsg << "Gap in data or no data from data source" << endl;
                     if (my_debug) cerr << "runRT() - no data, line " << __LINE__ << endl ;
                     testabort = true;
                     sync.reset();
                     break;
                  }
               case data_on_tape:
                  {
                    sync->abort ();
                    errmsg << "data found on tape, but data source not configured to read tape" << endl;
                    if (my_debug) cerr << "runRT() - data on tape, line " << __LINE__ << endl ;
                    testabort = true;
                    sync.reset();
                    break;
                  }
               case timeout:
               default:
                  {
                     sync->abort ();
                     errmsg << "Test timed-out" << endl;
		     if (my_debug) cerr << "runRT() - timeout, line " << __LINE__ << endl ;
                     testabort = true;
                     sync.reset();
                     break;
                  }

            }
            if (testabort) {
               break;
            }
         }
         if (testabort) {
            break;
         }
      
         // send notification
         if (!signalEndOfStep.empty()) {
            signalSend (signalEndOfStep);
         }
         notify.send (cmdnotify::testEnd);
      
         // increase result index
         int		testrindex = rindex;
         rindex += test->getResultNumber();
      
      /////////////////////////////////////////////////////////
      // Iterator Evaluation				     //
      /////////////////////////////////////////////////////////
      
         gdsDebug ("standard supervisory: Iterator Evaluation");
      
         // evaluate iterator
         bool		note;
         if (!testiter->evaluate (errmsg, continueIteration, 
                              testrindex, rindex, note)) {
            errmsg << "Unable to evaluate test iterator" << endl;
            testabort = true;
            break;
         }
         /* send notfication */
         if (note) {
            notify.send (cmdnotify::iterEvaluation);
         }
      
         /* check pause */
         if (continueIteration && (pause != 0) && (*pause)) {
            resumeWait ();
         }
         /* check abort */
         if (allowCancel && (abort != 0) && (*abort)) {
            errmsg << "Test aborted" << endl;
            testabort = true;
            break;
         }
      
      } while (continueIteration);
   
      /////////////////////////////////////////////////////////
      // Cleanup					     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Cleanup");
   
      /* release test points */
      if (!tpMgr->clear (true)) {
         errmsg << "Unable to clean up test points" << endl;
         testabort = true;
      }
      cerr << "clean 1" << endl;
      /* release rtdd channels */
      if (!dataMgr->clear (false)) {
         errmsg << "Unable to stop data distribution" << endl;
         testabort = true;
      }
      cerr << "clean 2" << endl;
      /* unsubscribe test channels */
      if (!test->end (errmsg)) {
         errmsg << "Unable to clean up test channels" << endl;
         testabort = true;
      }
      cerr << "clean 3" << endl;
      /* unsubscribe iterator channels */
      if (!testiter->end (errmsg)) {
         errmsg << "Unable to clean up iterator channels" << endl;
         testabort = true;
      }
      cerr << "clean 4" << endl;
      /* make sure excitation signals are turned off */
      testExc.del();
      iterExc.del();
      testEnvExc.del();
      envExc.del();
      cerr << "err = " << errmsg.str() << endl;
   
      /* test for abort */
      if (testabort) {
         gdsDebug("standard supervisory: Test Aborted");
         return false;
      }
   
      /* send notification */
      if (!signalEnd.empty()) {
         signalSend (signalEnd);
      }
      notify.send (cmdnotify::end);
   
      gdsDebug ("standard supervisory: Finished");
      return true;
   }


   bool standardsupervisory::runOL ()
   {
      semlock		lockit (mux);
      tainsec_t		now;		// time when measurement started
      tainsec_t		mtime;		// measurement start time
      taisec_t		lasttime;	// measurement stop time of last read
      taisec_t		lastlasttime;	// measurement stop time of read before last
      double		timeAhead;	// 
      int		iteration;	// iteration count
      bool		continueIteration; // 
      syncpointer	sync;		// synchronization point
      bool		testabort = false;// test abort flag
      int		rindex;		// result index
   
      gdsDebug ("standard supervisory: Initialization");
   
      /* check storage */
      if (storage == 0) {
         errmsg << "No diagnostics parameters" << endl;
         return false;
      }
   
      /* check rtdd manager */
      if (dataMgr == 0) {
         errmsg << "No data distribution management functions" << endl;
         return false;
      }
   
      /* check test point manager */
      if (tpMgr == 0) {
         errmsg << "No test point management functions" << endl;
         return false;
      }
   
      /* check test and test iterator */
      if ((test.get() == 0) || (testiter.get() == 0)) {
         errmsg << "No test and/or test iterator" << endl;
         return false;
      }
   
      /////////////////////////////////////////////////////////
      // Synchronization 1				     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Synchronization 1");
   
      /* check start time */
      now = _ONESEC * ((TAInow()+_ONESEC-1) / _ONESEC);
      mtime = ((start == 0) ? now : start) + 
         (tainsec_t) (wait * __ONESEC);
      lasttime = mtime / _ONESEC;
      lastlasttime = 0;
      cerr << "start = " << start / _ONESEC << endl;
      cerr << "mtime = " << mtime / _ONESEC << "/" << mtime % _ONESEC << endl;
   
      timeAhead = (double) (mtime - now) / __ONESEC;
      if (timeAhead > 10) {
         if (!timeWait (timeAhead - 10)) {
            errmsg << "Test aborted" << endl;
            return false;
         }
      }
   
      /* send notification */
      notify.send (cmdnotify::begin);
      rindex = 0;
   
      /////////////////////////////////////////////////////////
      // Iterator Initialization			     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Iterator Initialization");
   
      /* subscribe iterator channels */
      if (!testiter->begin (errmsg)) {
         errmsg << "Unable to setup iterator channels" << endl;
         testabort = true;
      }
   
      /////////////////////////////////////////////////////////
      // Test Initialization				     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Test Initialization");
   
      /* subscribe measurement & excitation channels */
      if (!test->begin (errmsg)) {
         errmsg << "Unable to setup test channels" << endl;
         testabort = true;
      }
   
      /////////////////////////////////////////////////////////
      // Iterator Setup					     //
      /////////////////////////////////////////////////////////
      iteration = -1;
      do {
         if (testabort) {
	    if (my_debug) cerr << "runOL() - testabort true, line " << __LINE__ << endl ;
            break;
         }
         iteration++;
      
         gdsDebug ("standard supervisory: Iterator Setup");
      
         /* switch iterator channels on if required */
         if (!testiter->setup (errmsg, mtime)) {
            errmsg << "Unable to start test iterator" << endl;
            testabort = true;
            break;
         }
      
      /////////////////////////////////////////////////////////
      // Synchronization 2				     //
      /////////////////////////////////////////////////////////
      
         gdsDebug ("standard supervisory: Synchronization 2");
      
      	 /* set start time */
         if (iteration == 0) {
            setMeasurementTime (mtime);
         }
         /* send notification */
         notify.send (cmdnotify::testBegin);
      
      
      /////////////////////////////////////////////////////////
      // Test Setup					     //
      /////////////////////////////////////////////////////////
      
         gdsDebug ("standard supervisory: Test Setup");
      
         /* start excitation and measurement */
         sync.reset();
         test->setStep (iteration, rindex);
         if (!test->setup (errmsg, mtime, sync)) {
            errmsg << "Unable to start test" << endl;
            testabort = true;
            break;
         }
      
      /////////////////////////////////////////////////////////
      // Synchronization 3				     //
      // Test Intermediate Analysis			     //
      // Synchronization 4				     //
      // Test Final Analysis				     //
      /////////////////////////////////////////////////////////
      
         bool 		testPaused = false;
         while ((sync.get() != 0) && (!!*sync)) {
            gdsDebug ("standard supervisory: Synchronization 3/4");
         
            switch (syncRead (*sync, lasttime, lastlasttime, testPaused)) {
               // pause test
               case paused: 
                  {
                     testPaused = true;
                     break;
                  }
               // resume test
               case resumed:
                  {
                     testPaused = false;
                     break;
                  }
               // call analysis algorithms
               case normal:
                  {
                     syncpointer	next;
                     bool		note;
                     if (!sync->action (next, note)) {
                        errmsg << "Synchronization error" << endl;
                        testabort = true;
                        break;
                     }
                     mtime = sync->time;
                     sync = next;
                     // send notification
                     if (note) {
                        notify.send (cmdnotify::testAnalysis);
                     }
                     // slow down?
                     if (slowDown > 0) {
                        if (!timeWait (slowDown)) {
                           sync->abort ();
			   if (my_debug) cerr << "runOL() test aborted, line " << __LINE__ << endl ;
                           errmsg << "Test aborted" << endl;
                           testabort = true;
                           sync.reset();
                        }
                     }
                     break;
                  }
               // test aborted
               case aborted:
                  {
                     sync->abort ();
		     if (my_debug) cerr << "runOL() test aborted, line " << __LINE__ << endl ;
                     errmsg << "Test aborted" << endl;
                     testabort = true;
                     sync.reset();
                     break;
                  }
               case no_data:
                  {
                    sync->abort ();
                    errmsg << "Gap in data or no data found" << endl;
                    if (my_debug) cerr << "runOL() - no data, line " << __LINE__ << endl ;
                    testabort = true;
                    sync.reset();
                    break;
                  }
               case data_on_tape:
                  {
                    sync->abort ();
                    errmsg << "Data found on tape, but data source not configured to read tape" << endl;
                    if (my_debug) cerr << "runOL() - data on tape, line " << __LINE__ << endl ;
                    testabort = true;
                    sync.reset();
                    break;
                  }
               // test timed out
               case timeout:
               default:
                  {
                     sync->abort ();
                     errmsg << "Test timed-out" << endl;
                     if (my_debug) cerr << "runOL() - timeout, line " << __LINE__ << endl ;
		     testabort = true;
                     sync.reset();
                     break;
                  }
            }
            if (testabort) {
	       if (my_debug) cerr << "runOL() test aborted, line " << __LINE__ << endl ;
               break;
            }
         }
         if (testabort) {
	    if (my_debug) cerr << "runOL() test aborted, line " << __LINE__ << endl ;
            break;
         }
      
         // send notification
         if (!signalEndOfStep.empty()) {
            signalSend (signalEndOfStep);
         }
         notify.send (cmdnotify::testEnd);
      
         // increase result index
         int		testrindex = rindex;
         rindex += test->getResultNumber();
      
      /////////////////////////////////////////////////////////
      // Iterator Evaluation				     //
      /////////////////////////////////////////////////////////
      
         gdsDebug ("standard supervisory: Iterator Evaluation");
      
         // evaluate iterator
         bool		note;
         if (!testiter->evaluate (errmsg, continueIteration, 
                              testrindex, rindex, note)) {
	    if (my_debug) cerr << "runOL() test aborted, line " << __LINE__ << endl ;
            errmsg << "Unable to evaluate test iterator" << endl;
            testabort = true;
            break;
         }
         /* send notfication */
         if (note) {
            notify.send (cmdnotify::iterEvaluation);
         }
      
         /* check pause */
         if (continueIteration && (pause != 0) && (*pause)) {
            resumeWait ();
         }
         /* check abort */
         if (allowCancel && (abort != 0) && (*abort)) {
	    if (my_debug) cerr << "runOL() test aborted, line " << __LINE__ << endl ;
            errmsg << "Test aborted" << endl;
            testabort = true;
            break;
         }
      
      } while (continueIteration);
   
      /////////////////////////////////////////////////////////
      // Cleanup					     //
      /////////////////////////////////////////////////////////
   
      gdsDebug ("standard supervisory: Cleanup");
   
      /* release test points */
      if (!tpMgr->clear (true)) {
         errmsg << "Unable to clean up test points" << endl;
         testabort = true;
      }
      cerr << "clean 1" << endl;
      /* release rtdd channels */
      if (!dataMgr->clear (false)) {
         errmsg << "Unable to stop data distribution" << endl;
         testabort = true;
      }
      cerr << "clean 2" << endl;
      /* unsubscribe test channels */
      if (!test->end (errmsg)) {
         errmsg << "Unable to clean up test channels" << endl;
         testabort = true;
      }
      cerr << "clean 3" << endl;
      /* unsubscribe iterator channels */
      if (!testiter->end (errmsg)) {
         errmsg << "Unable to clean up iterator channels" << endl;
         testabort = true;
      }
      cerr << "clean 4" << endl;
      /* make sure excitation signals are turned off */
      testExc.del();
      iterExc.del();
      testEnvExc.del();
      envExc.del();
      cerr << "err = " << errmsg.str() << endl;
   
      /* test for abort */
      if (testabort) {
	 if (my_debug) cerr << "runOL() test aborted, line " << __LINE__ << endl ;
         return false;
      }
   
      /* send notification */
      if (!signalEnd.empty()) {
         signalSend (signalEnd);
      }
      notify.send (cmdnotify::end);
   
      gdsDebug ("standard supervisory: Finished");
      return true;
   }


   supervisory* standardsupervisory::self () const
   {
      return new (nothrow) standardsupervisory();
   }


   bool standardsupervisory::timeWait (double duration) const
   {
      const struct timespec 	tick = {0, 3000000}; // 3ms
      tainsec_t			waittime = 
         (tainsec_t) (duration * (double) _ONESEC);
      tainsec_t			starttime = TAInow();
   
      while (TAInow() - starttime < waittime) {
         nanosleep (&tick, 0);
         if (allowCancel && (abort != 0) && (*abort)) {
            return false;
         }
      }
      return true;
   }


   bool standardsupervisory::triggerWait (const string& channel) const
   {
      /* not yet implemented */
      return true;
   }


   standardsupervisory::syncevent 
   standardsupervisory::syncWait (const syncpoint& sync,
                     bool testPaused) const
   {
      // cerr << "wait for " << sync.time << endl;
   
      // get time delay of rtdd
      tainsec_t rtddDelay = (dataMgr == 0) ? 0 : dataMgr->maxDelay();
   
      // event loop
      do {
         // check sync condition
         switch (sync.sync) {
            // check absolute time
            case syncpoint::absoluteTime:
               {
                  if (TAInow() >= sync.time) {
                     return normal;
                  }
                  break;
               }
            // is data ready?
            case syncpoint::dataReady:
               {
                  if (dataMgr == 0) {
		     if (my_debug) cerr << "syncWait() - dataMgr = 0, timeout, line " << __LINE__ << endl ;
                     return timeout;
                  }
                  else {
                     static tainsec_t oldtstamp = -1;
                     tainsec_t	timestamp = dataMgr->timeStamp();
                     if (timestamp != oldtstamp) {
                        oldtstamp = timestamp;
                        cerr << "data time stamp = " << 
                           (double) (timestamp % (1000 * _ONESEC)) / 1E9 << 
                           " at GPS = " << 
                           (double) (TAInow() % (1000 * _ONESEC)) / 1E9 << endl;
                     }
                     if ((timestamp > 0) && (timestamp >= sync.time)) {
                        return normal;
                     }
                  }
                  break;
               }
            // is excitation needed
            case syncpoint::excitationNeeded:
               {
                  if (TAInow() - 4 * _EPOCH >= sync.time) {
                     return normal;
                  }
                  break;
               }
            // error: abort
            default: 
               {
		  if (my_debug) cerr << "syncWait() - timeout, sync.sync error, line " << __LINE__ << endl ;
                  return timeout;
               }
         }
         // check timeout
         tainsec_t timeoutval = dataMgr->timeoutValue (true);
         if ((timeoutval > 0) && 
            (TAInow() - rtddDelay > sync.time + timeoutval)) {
	    if (my_debug) cerr << "syncWait() - timeout, line " << __LINE__ << endl ;  
	    if (my_debug) cerr << "  timeoutval = " << timeoutval << endl ;
	    if (my_debug) cerr << "  TAInow() - rtddDelay = " << TAInow() - rtddDelay << ", sync.time + timeoutval = " << sync.time + timeoutval << endl ;
            return timeout;
         }
         //check disconnected
         if(!dataMgr->busy())
         {
            return disconnected;
         }
         // check pause
         if ((pause != 0) && (*pause != testPaused) && 
            (sync.pauseable)) {
            return testPaused ? resumed : paused;
         }
         // wait a little while
         if (!timeWait (0.01) && sync.abortable) {
            return aborted;
         } 
      } while (true);
   }


   standardsupervisory::syncevent 
   standardsupervisory::syncRead (const syncpoint& sync,
                     taisec_t& last, taisec_t& lastlast, bool testPaused) const
   {
      cerr << "wait for " << sync.time << endl;
   
      // get time delay of rtdd and calculate how long to read
      tainsec_t rtddDelay = (dataMgr == 0) ? 0 : dataMgr->maxDelay();
      if (lastlast == 0) {
         last = last - (rtddDelay + _ONESEC - 1) / _ONESEC;
         cerr << "filter delay = " << rtddDelay / 1E9 << endl;
      }
      // Do we need to read anything?
      taisec_t next = (sync.time + rtddDelay + _ONESEC - 1)/_ONESEC;
      taisec_t duration = (next > last) ? next - last : 0;
      // If yes, look ahead as far as possible
      if (duration > 0) {
         taisec_t ahead = sync.time_ahead()/_ONESEC;
         if (ahead > next) duration = (ahead > last) ? ahead - last : 0;
      }
      // event loop
      do {
         // check abort
         if (allowCancel && (abort != 0) && (*abort) && sync.abortable) {
            return aborted;
         }
         // check sync condition
         if (!testPaused && (sync.time + _ONESEC < TAInow())) 
	 {
            switch (sync.sync) 
	    {
               // not supported
               case syncpoint::absoluteTime:
               case syncpoint::excitationNeeded:
               default:
                  {
		     if (my_debug) cerr << "syncRead() - timeout, line " << __LINE__ << endl ;
                     return timeout;
                  }
               // is data ready?
               case syncpoint::dataReady:
                  {
                     if (dataMgr == 0) 
		     {
			if (my_debug) cerr << "syncRead() - dataMgr = 0, line " << __LINE__ << endl ;
                        return timeout;
                     }
                     // check if to read from data manager
                     if ((duration > 0) && !dataMgr->busy()) 
		     {
                        // check if data ready
                        taisec_t askfor = duration;
                        tainsec_t delta = TAInow() - _ONESEC * (last + 1);
                        if (delta < 0) {
                           askfor = 0;
                        }
                        else if (delta < (unsigned) (duration * _ONESEC)) {
                           askfor = delta / _ONESEC;
                        }
                     	// ask for new data
                        if (askfor > 0) 
			{
                          try {
                            if (!dataMgr->set(last, askfor)) {
                              cerr << "*********************************************************"
                                   << endl;
                              cerr << "duration = " << askfor
                                   << " last = " << last / 1e9 << endl;
                              if (my_debug)
                                cerr << "syncRead() - timeout, line "
                                     << __LINE__ << endl;
                              return no_data;
                            }
                            lastlast = last;
                            last += askfor;
                            duration -= askfor;
                          }
                          catch(NoDataError ex)
                          {
                            cerr << "NoDataError exception: " << ex.what() << endl;
                            return no_data;
                          }
                          catch(DataOnTapeError ex)
                          {
                            cerr << "DataOnTapeError exception: " << ex.what() << endl;
                            return data_on_tape;
                          }
                          catch(QueryTimedOutError ex)
                          {
                            cerr << "QueryTimedOutError exception: " << ex.what() << endl;
                            return timeout;
                          }
                        }

                     }
                     // check time stamp
                     static tainsec_t oldtstamp = -1;
                     tainsec_t	timestamp = dataMgr->timeStamp();
                     if (timestamp != oldtstamp) 
		     {
                        oldtstamp = timestamp;
                        cerr << "data time stamp = " << 
                           (double) (timestamp % (1000 * _ONESEC)) / 1E9 << 
                           " at GPS = " << 
                           (double) (TAInow() % (1000 * _ONESEC)) / 1E9 << endl;
                     }
                     if ((timestamp > 0) && (timestamp >= sync.time)) 
		     {
                        cerr << "NORMAL_______________ data time stamp = " << 
                           (double) (timestamp % (1000 * _ONESEC)) / 1E9 << endl;                        
                        return normal;
                     }
                     break;
                  }
            }
            // check timeout
            tainsec_t timeoutval = dataMgr->timeoutValue (false);
            if ((timeoutval > 0) && (TAInow() > dataMgr->receivedTime() + timeoutval)) 
	    {
               cerr << "*********************************************************" << endl;
               cerr << "timeout = " << timeoutval/1E9 << " dataMgr->receivedTime() = " <<
                  dataMgr->receivedTime()/1e9 << " Now = " << TAInow()/1e9 << 
                  " diff = " << (dataMgr->receivedTime() - TAInow())/1E9 << endl;
	       if (my_debug) cerr << "syncRead() - timeout line " << __LINE__ << endl ;
               return timeout;
            }
         }
         // check pause
         if ((pause != 0) && (*pause != testPaused) && 
            (sync.pauseable)) {
            return testPaused ? resumed : paused;
         }
         // wait a little while
         if (!timeWait (0.01) && sync.abortable) {
            return aborted;
         } 
      } while (true);
   }


   bool standardsupervisory::resumeWait () const
   {
      if (pause == 0) {
         return false;
      }
      while (*pause) {
         if (!timeWait (0.01)) {
            return false;
         }
      }
      return true;
   }


   bool standardsupervisory::signalSend (const string& channel) const
   {
      /* not yet implemented */
      return true;
   }

}
