/* -*- mode: c++; c-basic-offset: 3; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: stdtest							*/
/*                                                         		*/
/* Module Description: standard test					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */
#include <cmath>
#include <chrono>
#include <semaphore.h>
#include <iostream> // JCB - Need to output to stderr for debugging.
#include <cstdio>  // JCB - Need for sprintf.
#include <cstdlib> // JCB - Need for abort().
#include <errno.h>  // JCB - Need for debugging and error messages.
#include <string.h> // JCB - Need for strerror().
#include "gdsmain.h"
#include "gdstask.h"
#include "map.h"
#include "diagnames.h"
#include "gdsconst.h"
#include "awgfunc.h"
#include "diagdatum.hh"
#include "testpointmgr.hh"
#include "excitation.hh"
#include "stdtest.hh"
#include "PConfig.h"  // JCB - need to know if P__DARWIN is defined.
#ifdef P__DARWIN
#include <time.h>
#endif

#define test_RATE 1

namespace diag {
   using namespace std;
   using namespace thread;

   static const int my_debug = 0 ;

   // Initialize the static variable in the class to 0.
   int stdtest::instance_count = 0 ; // JCB

   const int maxDataErrors = 100;


#define __ONESEC	1E9
#define fMod(x,y)	((x) - (y) * floor ((x) / (y)))


   stdtest::stimulus::stimulus () 
   : isReadback (false), waveform (awgNone), freq (0), ampl (0), offs (0),
   phas (0), ratio (0.5), frange (0), arange (0), 
   duplicate (false), useActiveTime (false)
   {
   }


   bool stdtest::stimulus::calcSignal (tainsec_t t0, tainsec_t duration,
				       tainsec_t rampup, tainsec_t rampdown)
   {
      AWG_Component	comp[2];
      int 		cnum = 1;
      if (my_debug) cerr << "stdtest::stimulus::calcSignal(t0 = "
			 << (t0 / _ONESEC) << '.' << (t0 % _ONESEC)
			 << ", duration = " << duration << ", rampup = "
			 << rampup << ")" << endl ;
   
      switch (waveform) {
         case awgSine:
         case awgRamp:
         case awgTriangle:
            {
	       if (my_debug) cerr << "  waveform type is " << waveform
				  << ", (Sine, Ramp, Triangle)" << endl ;
               if (awgPeriodicComponent (waveform, freq, ampl, phas,
                                    offs, comp) < 0) {
                  return false;
               }
               comp[0].par[2] = phas;
               comp[0].start = t0 ;
               comp[0].duration = duration ;
	       comp[0].ramptime[0]=rampup ;
	       comp[0].ramptime[1]=rampdown ;
               break;
            }
         case awgSquare:
            {
               if (fabs (ratio - 0.5) < 1E-9) {
                  if (awgPeriodicComponent (waveform, freq, ampl, phas,
                                       offs, comp) < 0) {
                     return false;
                  }
                  comp[0].par[2] = phas;
                  comp[0].start = t0 ;
                  comp[0].duration = duration ;
		  comp[0].ramptime[0]=rampup ;
		  comp[0].ramptime[1]=rampdown ;
               }
               else {
                  if (awgSquareWaveComponent (freq, ampl, phas,
                                       offs, ratio, comp) < 0) {
                     return false;
                  }
                  comp[0].start = t0  + (tainsec_t) 
                     (__ONESEC * (fMod (phas / TWO_PI, 1) - 1)/ freq + 0.5);
                  comp[0].duration = duration ;
		  comp[0].ramptime[0]=rampup ;
		  comp[0].ramptime[1]=rampdown ;
                  comp[1].start = t0 ;
                  comp[1].duration = duration ;
                  cnum = 2;
               }
               break;
            }
         case awgImpulse:
            {
               if (awgPeriodicComponent (waveform, freq, ampl, phas,
                                    offs, comp) < 0) {
                  return false;
               }
               comp[0].start = t0 ;
               comp[0].duration = duration ;
	       comp[0].ramptime[0]=rampup ;
	       comp[0].ramptime[1]=rampdown ;
               break;
            }
         case awgConst:
            {
               if (awgConstantComponent (ampl, comp) < 0) {
                  return false;
               }
               comp[0].start = t0 ;
               comp[0].duration = duration ;
	       comp[0].ramptime[0]=rampup ;
	       comp[0].ramptime[1]=rampdown ;
               break;
            }
         case awgNoiseN:
         case awgNoiseU:
            {
               if (awgNoiseComponent (waveform, freq - frange, freq + 
                                    frange, ampl, offs, comp) < 0) {
                  return false;
               }
               comp[0].start = t0 ;
               comp[0].duration = duration ;
	       comp[0].ramptime[0]=rampup ;
	       comp[0].ramptime[1]=rampdown ;
               break;
            }
         case awgArb:
            {
               if (awgPeriodicComponent (waveform, freq, ampl, phas,
                                    offs, comp) < 0) {
                  return false;
               }
               comp[0].start = t0 ;
               comp[0].duration = duration ;
	       comp[0].ramptime[0]=rampup ;
	       comp[0].ramptime[1]=rampdown ;
               break;
            }
         // sweep
         case (AWG_WaveType) 10:
         case (AWG_WaveType) 11:
            {
               long	flag = (waveform == (AWG_WaveType) 10) ? 
                  0 : AWG_SWEEP_LOG;
               if (awgSweepComponents (t0 , duration , freq - frange, freq + 
				       frange, ampl - arange, ampl + arange,
				       flag, comp, &cnum) < 0) {
                  return false;
               }
	       comp[0].ramptime[0]=rampup ;
	       comp[0].ramptime[1]=rampdown ;
               break;
            }
         case awgNone:
            {
               cnum = 0;
               return true;
            }
         default:
            {
               return false;
            }
      }
      // check components
      if (!awgIsValidComponent (comp) ||
         ((cnum > 1) && !awgIsValidComponent (comp + 1))) {
         return false;
      }
      // fill in ramp parameters (amplitude phase-in only)
      if (rampup > 0) {
#if 1
	 comp[0].ramptype = RAMP_TYPE (AWG_PHASING_QUADRATIC, 
			      AWG_PHASING_QUADRATIC, 
			      AWG_PHASING_LINEAR);
#else
	 comp[0].ramptype = 1 ;
#endif
	 if (my_debug) cerr << "  calSignal() - Setting ramptype to " << comp[0].ramptype << endl ;
      }

      // add waveforms
      for (int i = 0; i < cnum ; i++) {
         signals.push_back (comp[i]);
      }
      if (my_debug) cerr << "  calcSignal() return true" << endl ;
      return true;
   }

   bool stdtest::stimulus::calcBurstSignal(std::ostringstream& errmsg, tainsec_t t0, tainsec_t pitch_ns,
           tainsec_t rampUp_ns, tainsec_t rampDown_ns,
           tainsec_t excitation_ns, int num_points)
   {
       signals.resize(num_points);
       tainsec_t duration_ns = excitation_ns + rampDown_ns + rampUp_ns;

       for(int i=0; i < num_points; ++i)
       {
           AWG_Component *comp;
           comp = &signals[i];
           tainsec_t start_ns = t0 + i * pitch_ns;
           switch (waveform) {
               case awgSine:
               case awgRamp:
               case awgTriangle:
               {
                   if (my_debug) cerr << "  waveform type is " << waveform
                                      << ", (Sine, Ramp, Triangle)" << endl ;
                   if (awgPeriodicComponent (waveform, freq, ampl, phas,
                                             offs, comp) < 0) {
                       errmsg << "Bad component values when creating excitation." << endl;
                       return false;
                   }
                   comp[0].par[2] = phas;
                   comp[0].start = start_ns;
                   comp[0].duration = duration_ns ;
                   comp[0].ramptime[0]=rampUp_ns ;
                   comp[0].ramptime[1]=rampDown_ns ;
                   break;
               }
               case awgImpulse:
               {
                   if (awgPeriodicComponent (waveform, freq, ampl, phas,
                                             offs, comp) < 0) {
                       errmsg << "Bad component values when creating excitation." << endl;
                       return false;
                   }
                   comp[0].start = start_ns ;
                   comp[0].duration = duration_ns ;
                   comp[0].ramptime[0]=rampUp_ns ;
                   comp[0].ramptime[1]=rampDown_ns ;
                   break;
               }
               case awgConst:
               {
                   if (awgConstantComponent (ampl, comp) < 0) {
                       errmsg << "Bad component values when creating excitation." << endl;
                       return false;
                   }
                   comp[0].start = start_ns ;
                   comp[0].duration = duration_ns ;
                   comp[0].ramptime[0]=rampUp_ns ;
                   comp[0].ramptime[1]=rampDown_ns ;
                   break;
               }
               case awgNoiseN:
               case awgNoiseU:
               {
                   if (awgNoiseComponent (waveform, freq - frange, freq +
                                                                   frange, ampl, offs, comp) < 0) {
                       errmsg << "Bad component values when creating excitation." << endl;
                       return false;
                   }
                   comp[0].start = start_ns ;
                   comp[0].duration = duration_ns ;
                   comp[0].ramptime[0]= rampUp_ns ;
                   comp[0].ramptime[1]= rampDown_ns ;
                   break;
               }
               case awgArb:
               {
                   if (awgPeriodicComponent (waveform, freq, ampl, phas,
                                             offs, comp) < 0) {
                       errmsg << "Bad component values when creating excitation." << endl;
                       return false;
                   }
                   comp[0].start = start_ns ;
                   comp[0].duration = duration_ns ;
                   comp[0].ramptime[0]=rampUp_ns ;
                   comp[0].ramptime[1]=rampDown_ns ;
                   break;
               }
               case awgNone:
               {
                   signals.clear();
                   return true;
               }
               case awgSquare:  //not doing awg square.  Too much garbage in implementation.
               errmsg << "Square wave not supported with Burst Noise Quiet Time > 0." << endl;
               return false;

               case (AWG_WaveType) 10:
               case (AWG_WaveType) 11:
               default:
               {
                   errmsg << "Unsupported wave type " << waveform << " when Burst Noise Quiet Time > 0" << endl;
                   return false;
               }
           }
           comp[0].ramptype = RAMP_TYPE (AWG_PHASING_QUADRATIC,
                                         AWG_PHASING_QUADRATIC,
                                         AWG_PHASING_LINEAR);
       }
       return true;
   }


   bool stdtest::stimulus::calcSineSignal (tainsec_t t0, 
                     tainsec_t duration, tainsec_t rampup, tainsec_t rampdown)
   {
      AWG_Component		awg;
   
      if (my_debug) cerr << "stdtest::stimulus::calcSineSignal(t0="<<t0<<", duration="<<duration<<", rampup="<< rampup << ")"<<endl;
      // check waveform 
      if (waveform != awgSine) {
         return false;
      }
      // calculate basic waveform
      awgPeriodicComponent (waveform, freq, ampl, phas, offs, &awg);
   
      // start time and duration
      if (signals.empty()) {
         // first signal includes an amplitide ramp up
         awg.start = t0;
         awg.duration = duration;
      }
      else {
         // all other signals ramp from previous signal
         awg.start = t0 + rampup;
         awg.duration = (duration < 0) ? duration : duration - rampup;
      }
   
      // fill in ramp parameters (amplitude phase-in only)
      awg.ramptype = RAMP_TYPE (AWG_PHASING_QUADRATIC, 
                           AWG_PHASING_QUADRATIC, 
                           AWG_PHASING_LINEAR);
      awg.ramptime[0] = 0;
      awg.ramptime[1] = 0;
      if (signals.empty()) {
         // first signal includes amplitide ramp up
         awg.ramptime[0] = rampup;
	 if (my_debug) cerr << "  signals.empty(), setting awg.ramptime[0] = "<<rampup<<endl ;
         // set phase to 0 at start
         if (duration >= 0) awg.par[2] = 0; 
         phas = fmod (TWO_PI * freq * (double) rampup / __ONESEC + awg.par[2], TWO_PI);
      }
      else {
	 // Subsequent signals don't have ramp up.
	 awg.ramptime[0] = 0 ;
	 if (my_debug) cerr << "  Add ramp to previous signal" << endl ;
         // add ramp to previous signal!
         // patch duration of previous signal
         signals.back().duration = awg.start - signals.back().start;
      	 // set signal phase (as smooth a transition as possible)
         awg.par[2] = fmod (signals.back().par[2] - 
                           TWO_PI * (double) signals.back().duration /
                           __ONESEC * signals.back().par[1] -
                           PI * (double) rampup / __ONESEC * 
                           (awg.par[1] - signals.back().par[1]), TWO_PI);
         phas = - awg.par[2];
         // patch phase-out of previous signal (both A and f)
         signals.back().ramptime[1] = rampup;
         signals.back().ramppar[0] = awg.par[0];
         signals.back().ramppar[1] = awg.par[1];
         signals.back().ramppar[2] = awg.par[2];
         signals.back().ramppar[3] = awg.par[3];
      }
   
      signals.push_back (awg);
      if (my_debug) cerr << "    awg.ramptime[0] = "<<awg.ramptime[0]<<", ramptime[1] = "<<awg.ramptime[1] << endl ;
      if (my_debug) cerr << "stdtest::stimulus::calcSineSignal() return true" << endl ;
      return true;
   }


   static const AWG_WaveType waveformlist[] = {
   awgNone,
   awgSine,
   awgSquare,
   awgRamp,
   awgTriangle,
   awgImpulse,
   awgConst,
   awgNoiseN,
   awgNoiseU,
   awgArb};

   const stdtest::waveformset stdtest::sinewaveOnly =
   stdtest::waveformset (waveformlist + 1, waveformlist + 2);

   const stdtest::waveformset stdtest::periodicOnly =
   stdtest::waveformset (waveformlist + 1, waveformlist + 6);

   const stdtest::waveformset stdtest::noiseOnly =
   stdtest::waveformset (waveformlist + 7, waveformlist + 9);

   const stdtest::waveformset stdtest::allWaveforms =
   stdtest::waveformset (waveformlist + 1, waveformlist + 10);


   stdtest::measurementchannel::measurementchannel ()
   : duplicate (false), useActiveTime (false)
   {
   }


   stdtest::interval::interval (tainsec_t t, tainsec_t dt) 
   : ival (t, dt), good (true) 
   {
   }


   tainsec_t stdtest::interval::t0 () const 
   {
      return ival.first;
   }


   tainsec_t stdtest::interval::dt () const 
   {
      return ival.second;
   }



   stdtest::stdtest (const string& name) 
   : diagtest (name), test (0), doAnalysis (true), timeAhead (0), T0 (0), 
   averageType (0), averages (1), avrgsize (0), avrgnum (0),
   timeGrid (1.0), dataerrors (0)
   {
      // 
      myinstance = instance_count ; // JCB
      instance_count++ ; // JCB 
//      if (my_debug) cerr << "stdtest " << myinstance << " created" << endl ; // JCB
   }

   stdtest::~stdtest(void)
   {
//      if (my_debug) cerr << "stdtest " << myinstance << " destroyed" << endl ; // JCB
   }

   bool stdtest::readParam (ostringstream& errmsg)
   {
      semlock		lockit (mux);
      bool		err = false;
   
      if (my_debug) cerr << "stdtest::readParam()" << endl ;
      // check storage
      if (storage == 0) {
         errmsg << "No diagnostics parameters" << endl;
         return false;
      }
      // check storage
      if (dataMgr == 0) {
         errmsg << "No real-time data distribution manager" << endl;
         return false;
      }
      // check storage
      if (testExc == 0) {
         errmsg << "No excitation manager for test" << endl;
         return false;
      }
      // check test storage objects
      if (storage->Test == 0) {
         errmsg << "Unable to load value from Test" << endl;
         return false;
      }
      // check test access class
      test = diagTest::self(myname);
      if (test == 0) {
         errmsg << "Unable to access Test" << endl;
         return false;
      }
   
      // read test settings
      if (!test->getParam (*storage->Test,
                          stTestParameterSubtype, testType)) {
         errmsg << "Unable to load value from Test." << 
            stTestParameterSubtype << endl;
         err = true;
      }
      if (compareTestNames (testType, myname) != 0) {
         errmsg << "Not " << myname << " test (" << 
            testType << ")" << endl;
         err = true;
      }
      // read average type
      if (!test->getParam (*storage->Test,
                          stAverageType, averageType)) {
         averageType = 0;
      }
      // read # of averages
      if (!test->getParam (*storage->Test,
                          stAverages, averages)) {
         averages = 1;
      }
      if (my_debug) cerr << "stdtest::readParam() return " << (err ? "false" : "true") << endl ;
      return !err;
   }


   bool stdtest::readMeasParam (std::ostringstream& errmsg, 
                     int maxIndex)
   {
      semlock		lockit (mux);
      bool		err = false;
   
      if (my_debug) cerr << "stdtest::readMeasParam()" << endl ;
      // make sure max index is at least one
      int	max = (maxIndex > 1) ? maxIndex : 1;
   
      // read measurement channels
      for (int i = 0; i < max; i++) {
         measurementchannel	measchn;
         string			chnname;
         string			chnactive;
         chnname = (max == 1) ? string (stMeasurementChannel) :
            diagObjectName::makeName (stMeasurementChannel, i);
         chnactive = (max == 1) ? string (stMeasurementChannelActive) :
            diagObjectName::makeName (stMeasurementChannelActive, i);
#ifdef TEST_RATE
	 string			chnrate ;
	 chnrate = (max == 1) ? string (stMeasurementChannelRate) :
	    diagObjectName::makeName (stMeasurementChannelRate, i) ;
#endif
         // read channel name
         if (!test->getParam (*storage->Test, chnname, measchn.name) ||
            (measchn.name.empty())) {
            continue;
         }
         // read active
         bool active = true;
         if (test->getParam (*storage->Test, chnactive, active)) {
            if (!active) {
               continue;
            }
         }
#ifdef TEST_RATE
	 int rate = 0;
	 if (test->getParam (*storage->Test, chnrate, rate)) {
	    if (!rate) {
	       continue ;
	    }
	    if (my_debug) cerr << "  rate for " << measchn.name << " is " << rate << endl ;
	 }
#endif
         // now get channel info
#ifdef TEST_RATE
         if (!dataMgr->channelInfo (measchn.name, measchn.info, rate)  ||
#else
         if (!dataMgr->channelInfo (measchn.name, measchn.info)  ||
#endif
            (measchn.info.dataRate <= 0)) {
            errmsg << "Invalid channel name (" << measchn.name << 
               ")" << endl;
            err = true;
            continue;
         }
         meas.push_back (measchn);
      }
      if (meas.empty()) {
         errmsg << "No measurement channel defined" << endl;
         err = true;
      }
      if (my_debug) cerr << "stdtest::readMeasParam() return " << (err ? "false" : "true") << endl ;
      return !err;
   }


   bool stdtest::readStimuliParam (std::ostringstream& errmsg, 
                     bool needReadback, waveformset allowedWaveforms,
                     int maxIndex)
   {
      semlock		lockit (mux);
      bool		err = false;
   
      // make sure max index is at least one
      int	max = (maxIndex > 1) ? maxIndex : 1;
   
      if (my_debug) cerr << "stdtest::readStimuliParam()" << endl ;
      // read stimuli channel
      for (int i = 0; i < max; i++) {
         stimulus		stim;
         int			indx = (max > 1) ? i : -1;
         // read channel name
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusChannel, indx), stim.name) ||
            stim.name.empty()) {
            continue;
         }
         // read active flag
         bool active = true;
         if (test->getParam (*storage->Test, diagObjectName::makeName 
                            (stStimulusActive, indx), active) &&
            !active) {
            continue;
         }
      
         // read readback name
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusReadback, indx), stim.readback) ||
            stim.readback.empty()) {
            char	buf[256];
            if (tpReadbackName (stim.name.c_str(), buf) == 0) {
               stim.readback = buf;
            }
            else {
               stim.readback = "";
            }
         }
         stim.isReadback =
            !stim.readback.empty() && (stim.readback != "!");
      
         if (needReadback && !stim.isReadback) {
            errmsg << "Readback channel unavailable (" <<
               stim.readback << ")" << endl;
            err = true;
            continue;
         }
      
         // read stimulus type
         stim.waveform = awgNone;
         int 		waveform;
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusType, indx), waveform)) {
            stim.waveform = awgSine;
         }
         else {
            stim.waveform = (AWG_WaveType) waveform;
         }
         // test waveform
         if (stim.waveform == awgNone) {
            continue;
         }
         if (allowedWaveforms.count (stim.waveform) == 0) {
            errmsg << "Illegal waveform (" <<
               stim.waveform << ")" << endl;
            err = true;
            continue;
         }
      
         // read frequency
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusFrequency, indx), stim.freq)) {
            stim.freq = 0;
         }
         // read amplitude
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusAmplitude, indx), stim.ampl)) {
            stim.ampl = 0;
         }
         // read offset
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusOffset, indx), stim.offs)) {
            stim.offs = 0;
         }
         // read phase
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusPhase, indx), stim.phas)) {
            stim.phas = 0;
         }
         // read ratio
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusRatio, indx), stim.ratio)) {
            stim.ratio = 0.5;
         }
         // read frequency range
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusFrequencyRange, indx), 
                             stim.frange)) {
            stim.frange = 0;
         }
         // read amplitude range
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusAmplitudeRange, indx), 
                             stim.arange)) {
            stim.arange = 0;
         }
         // read filter
         if (!test->getParam (*storage->Test, diagObjectName::makeName 
                             (stStimulusFilterCmd, indx), 
                             stim.filtercmd)) {
            stim.filtercmd = "";
         }
         // read points
         gdsDatum		pt;
         if (test->getParam (*storage->Test, diagObjectName::makeName 
                            (stStimulusPoints, indx), pt)) {
            if (((pt.datatype == gds_float32) || 
                (pt.datatype == gds_float64)) &&
               (pt.dimension.size() == 1) &&
               (pt.value != 0)) {
               float 		ptval;	// point value
               for (int i = 0; i < pt.elNumber(); i++) {
                  ptval = (pt.datatype == gds_float32) ?
                     *((float*) pt.value + i) :
                     *((double*) pt.value + i);
                  stim.points.push_back (ptval);
               }
            } 
            else {
               errmsg << "Unable to load values from Test." <<
                  stStimulusPoints << endl;
               err = true;
            }
         }
         // now get channel info
         if (!testExc->channelInfo (stim.name, stim.excInfo) ||
            (stim.excInfo.dataRate <= 0)) {
            errmsg << "Invalid channel name (" << stim.name << 
               ")" << endl;
            err = true;
            continue;
         }
         if (stim.isReadback) {
            if (!testExc->channelInfo (stim.readback, 
                                 stim.readbackInfo) ||
               (stim.readbackInfo.dataRate <= 0)) {
               errmsg << "Invalid channel name (" << stim.readback << 
                  ")" << endl;
               err = true;
               continue;
            }
         }
         stimuli.push_back (stim);
      }
      if (my_debug) cerr << "stdtest::readStimuliParam() return " << (err ? "false" : "true") << endl ;
      return !err;
   }


   bool stdtest::subscribeChannels (std::ostringstream& errmsg)
   {
      semlock		lockit (mux);
      bool		err = false;
      int		count; 		// in use count
   
      if (my_debug) cerr << "stdtest::subscribeChannels()" << endl ;
      // subscribe excitation channels
      if (my_debug) cerr << "  subscribeChannels() - subscribe excitation channels" << endl ;
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
	 if (my_debug) cerr << "  subscribeChannels() - calling dataMgr->add(" << iter->name << ",...)" << endl ;
         if (!testExc->add (iter->name)) {
            errmsg << "Invalid excitation channel (" << iter->name <<
               ")" << endl;
            err = true;
         }
      }
   
      // subscribe measurement channels; read back channels first
      if (my_debug) cerr << "  subscribeChannels() - subscribe read back channels" << endl ;
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (iter->isReadback) {
	    if (my_debug) cerr << "  subscribeChannels() - calling dataMgr->add(" << iter->readback << ",...)" << endl ;
            if (!dataMgr->add (iter->readback, &count)) {
               errmsg << "Invalid measurement channel (" << 
                  iter->readback << ")" << endl;
               err = true;
            }
            iter->duplicate = (count > 1);
         }
      }
      if (my_debug) cerr << "  subscribeChannels() - subscribe measurement channels" << endl ;
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
	 if (my_debug) cerr << "  subscribeChannels() - calling dataMgr->add(" << iter->name << ",..., dataRate = " << iter->info.dataRate << ")" << endl ;
         if (!dataMgr->add (iter->name, &count, iter->info.dataRate)) {
            errmsg << "Invalid measurement channel (" << 
               iter->name << ")" << endl;
            err = true;
         }
         iter->duplicate = (count > 1);
      }
      if (my_debug) cerr << "stdtest::subscribeChannels() return " << (err ? "false" : "true") << endl ;
   
      return !err;
   }


   bool stdtest::delMeasurements (std::ostringstream& errmsg)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::delMeasurements()" << endl ;
      // clears excitation signals & measurement partitions
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         iter->signals.clear();
         iter->partitions.clear();
      }
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         iter->partitions.clear();
      }
   
      // clear interval list
      intervals.clear();
      // clears synchronization points
      syncqueue.clear();
   
      if (my_debug) cerr << "stdtest::delMeasurements() return true" << endl ;
      return true;
   }


   void stdtest::samplingFrequencies (double fMax, double& fSampleMin, 
                     double& fSampleMax)
   {
      // highest sampling rate
      int	exp;
      if (my_debug) cerr << "stdtest::samplingFrequencies( fMax = " << fMax << ", ...)" << endl ;
      frexp (2.0 * fMax - 1E-8, &exp);
      fSampleMax = ldexp (1.0, exp);
   
      // lowest sampling rate
      fSampleMin = fSampleMax;
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if ((iter->isReadback) && 
            (iter->readbackInfo.dataRate < fSampleMin)) {
            fSampleMin = iter->readbackInfo.dataRate;
         }
      }
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         if (iter->info.dataRate < fSampleMin) {
            fSampleMin = iter->info.dataRate;
         }
      }
      if (my_debug) cerr << "stdtest::samplingFrequencies(..., fSampleMin = " << fSampleMin << ", fSampleMax = " << fSampleMax << ") return" << endl ;
   }


   bool stdtest::heterodyneFrequency (double &fZoom)
   {
      double fz = 0;
      bool fzinit = false;
   
      if (my_debug) cerr << "stdtest::heterodyneFrequency()" << endl ;
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (iter->isReadback) {
            if (iter->readbackInfo.dataType == DAQ_DATATYPE_COMPLEX) {
               if (!fzinit) {
                  fz = iter->readbackInfo.gain;
                  fzinit = true;
               }
               else if (fabs (fz - iter->readbackInfo.gain) > 1E-8) {
                  return false;
               }
            }
            else {
               if (!fzinit) {
                  fz = 0;
                  fzinit = true;
               }
               else if (fz != 0) {
                  return false;
               }
            }
         }
      }
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         if (iter->info.dataType == DAQ_DATATYPE_COMPLEX) {
            if (!fzinit) {
               fz = iter->info.gain;
               fzinit = true;
            }
            else if (fabs (fz - iter->info.gain) > 1E-6) {
               return false;
            }
         }
         else {
            if (!fzinit) {
               fz = 0;
               fzinit = true;
            }
            else if (fz != 0) {
               return false;
            }
         }
      }
      fZoom = fz;
      if (my_debug) cerr << "stdtest::heterodyneFrequency() return true" << endl ;
      return true;
   }


   double stdtest::calcTimeGrid (double fMax, tainsec_t* t0)
   {
      semlock		lockit (mux);
   
      if (my_debug) {
	 cerr << "stdtest::calcTimeGrid( fMax = " << fMax ;
	 if (t0 != 0)
	    cerr << ", t0 = " << (*t0 / _ONESEC) << '.' << (*t0 % _ONESEC) << ")" << endl ;
	 else
	    cerr << ", NULL)" << endl ;
      }
      // start with 1/16384 sec.
      //double tGrid = 1.0 / sin (2);
      double tGrid = ldexp (1.0, -14); // 0.00006103525625
      if (my_debug) cerr << "  tGrid = " << tGrid << endl ;
   
      // determine maximum sampling period of stimulus readback & 
      // measurement channels
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (iter->isReadback) {
            if (iter->readbackInfo.dataRate > 0) {
	       if (my_debug) {
		  cerr << "  sample rate of " << iter->readbackInfo.chName << " = "
	            << iter->readbackInfo.dataRate << ", 1/dataRate = " <<
		    1.0/(double)iter->readbackInfo.dataRate << endl ;
	       }
               tGrid = max (tGrid, 1.0/(double)iter->readbackInfo.dataRate);
            }
         }
      }
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         if (iter->info.dataRate > 0) {
	    if (my_debug) {
	       cerr << "  sample rate of " << iter->info.chName << " = "
		 << iter->info.dataRate << ", 1/dataRate = " <<
		 1.0/(double)iter->info.dataRate << endl ;
	    }
            tGrid = max (tGrid, 1.0 / (double) iter->info.dataRate);
         }
      }
      if (my_debug) cerr << "  tGrid = " << tGrid << endl ;
      // adjust time grid if sampling freq much larger 
      // than meas/exc. freq.
      while (1.0 / tGrid > 2 * fMax + 1E-12) {
         tGrid *= 2.0;
	 if (my_debug) cerr << "  Adjust time grid, tGrid = " << tGrid << endl ;
      }
      // adjust t0 if necessary
      if (t0 != 0) {
         *t0 = fineAdjustForSampling (*t0 + (tainsec_t ) 
                              (tGrid/2 * 1E9 - 10000), tGrid);
      }
      if (my_debug) {
	 cerr << "stdtest::calcTimeGrid() return" ;
	 if (t0)
	    cerr << ", t0 modified to " << (*t0 / _ONESEC) << '.' << (*t0 % _ONESEC) ;
	 cerr << ", tGrid = " << tGrid << endl ;
      }
      return tGrid;
   }


   bool stdtest::addMeasurements (std::ostringstream& errmsg,
                     tainsec_t t0, int measPoint)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::addMeasurements(t0 = " << (t0 / _ONESEC) << '.' << (t0 % _ONESEC) << ", measPoint = " << measPoint << ")" << endl ;
      // calculate measurement times
      if (!calcTimes (errmsg, t0)) {
         return false;
      }
   
      // set start time
      T0 = t0;
      // handle averaging
      avrgnum = 0;
      if (avrgsize == 0) {
         avrgsize = averages;
         if (avrgsize < 1) {
            avrgsize = 1;
         }
      }
   
      // calculates measurement parameters
      if (!calcMeasurements (errmsg, t0, measPoint)) {
         return false;
      }
   
      if (my_debug) cerr << "stdtest::addMeasurements() return true" << endl ;
      return true;
   }


   bool stdtest::startMeasurements (std::ostringstream& errmsg)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::startMeasurements()" << endl ;
      // start excitation signals
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (!testExc->add (iter->name, iter->signals)) {
            errmsg << "Unable to turn on excitations" << endl;
            return false;
         }
         if (!testExc->addFilter (iter->name, iter->filtercmd)) {
            errmsg << "Unable to turn on excitations" << endl;
            return false;
         }
      }
      if (!testExc->start (-1)) {
         errmsg << "Unable to turn on excitations" << endl;
         return false;
      }
   
      // start receiveing data
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (!iter->isReadback || (iter->duplicate)) {
            continue;
         }
         if (!dataMgr->add (iter->readback, iter->partitions, 
                           iter->useActiveTime)) {
            errmsg << "Unable to start real-time data distribution" << 
               endl;
            return false;
         }
      }
      for (meas_iter iter = meas.begin(); iter != meas.end(); iter++) {
         if (iter->duplicate) {
            continue;
         }
         if (!dataMgr->add (iter->name, iter->partitions, 
                           iter->useActiveTime)) {
            errmsg << "Unable to start real-time data distribution" << 
               endl;
            return false;
         }
      }
   
      if (my_debug) cerr << "stdtest::startMeasurements() return true" << endl ;
      return true;
   }


   bool stdtest::stopMeasurements (int firstIndex)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::stopMeasurements( firstIndex = " << firstIndex << ")" << endl ;
      int num = (avrgsize < 0) ? intervals.size() - firstIndex : avrgsize;
   
      // stop rtdd: stimulus readbacks first
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (!iter->isReadback || (iter->duplicate)) {
            continue;
         }
         dataMgr->reset (iter->readbackInfo.chName);
         // make sure all unnecessary data objects are deleted
         if (firstIndex >= 0) {
            for (int i = firstIndex; i < firstIndex + num; i++) {
               storage->eraseData (diagObjectName::makeName 
                                  (iter->readbackInfo.chName, step, i));
            }
         }
      }
      // stop rtdd : measurement channels next
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         if (iter->duplicate) {
            continue;
         }
         dataMgr->reset (iter->info.chName);
         // make sure all unnecessary data objects are deleted
         if (firstIndex >= 0) {
            for (int i = firstIndex; i < firstIndex + num; i++) {
               storage->eraseData (diagObjectName::makeName 
                                  (iter->info.chName, step, i));
            }
         }
      }
   
      // clear intervals & sync points
      intervals.clear();
      syncqueue.clear();
   
      if (my_debug) cerr << "stdtest::stopMeasurements() return true" << endl ;
      return true;
   }


   bool stdtest::begin (std::ostringstream& errmsg)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::begin()" << endl ;
      // make sure object is cleaned up
      syncqueue.clear();
      meas.clear();
      stimuli.clear();
      intervals.clear();
      dataerrors = 0;
   
      // read parameters
      if (!readParam (errmsg)) {
         return false;
      }
   
      // subscribe channels
      if (!subscribeChannels (errmsg)) {
         return false;
      }
   
      if (my_debug) cerr << "stdtest::begin() return" << endl ;
      return true;
   }


   bool stdtest::end (ostringstream& errmsg)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::end()" << endl ;
      // unsubscribe excitation signals & rtdd channels
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         testExc->del (iter->name);
         if (!iter->isReadback) {
            dataMgr->del (iter->readback);
         }
      }
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         dataMgr->del (iter->name);
      }
   
      // cleanup local parameters
      syncqueue.clear();
      meas.clear();
      stimuli.clear();
      intervals.clear();
      if (my_debug) cerr << "stdtest::end() return true" << endl ;
      return true;
   }


   bool stdtest::setup (std::ostringstream& errmsg, 
                     tainsec_t starttime, syncpointer& sync)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::setup( starttime = " << (starttime / _ONESEC) << '.' << (starttime % _ONESEC) << ")" << endl ;
      // make sure partition and excitation lists are empty
      if (!delMeasurements (errmsg)) {
         return false;
      }
   
      // add measurement descriptors
      if (!addMeasurements (errmsg, starttime)) {
         return false;
      }
   
      // start measurement
      if (!startMeasurements (errmsg)) {
         return false;
      }
   
      // get first synchronization point and return
      callbackarg		arg;
      arg.measPeriod = -1;
      bool rc = getNextSyncPoint (arg, sync);
      if (my_debug) cerr << "stdtest::setup() return " << (rc ? "true" : "false") << endl ;
      return rc ;
   }


   bool stdtest::addMeasPartitions (const interval& ival, int pIndex,
                     double fSample, tainsec_t pt, double fZoom, 
                     double fZoomSample, tainsec_t tZoom)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::addMeasPartitions()" << endl ;
      // calculate partitions for stimulus
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (iter->isReadback) {
            double rate = iter->readbackInfo.dataRate;
            int decimate1 = (rate > fSample) ? (int) (rate / fSample + 0.5) : 1;
            int decimate2 = (fZoomSample > 0) ? 
               (int) (rate / (double)decimate1 / fZoomSample + 0.5) : 1;
            if (decimate2 < 1) {
               decimate2 = 1;
            }
            if ((rate <= 0) || (decimate1 <= 0) || (decimate2 <= 0) ||
               (fZoom < 0) || (tZoom < 0)) {
               continue;
            }
            double dt = (rate > fSample / decimate2) ? 
               1.0 / (fSample / decimate2) : 1.0 / rate;
            string pname = diagObjectName::makeName (
                                 iter->readbackInfo.chName, step, pIndex);
            dataChannel::partition	part 
               (pname, ival.t0(), ival.dt(), dt, pt);
            part.setup (dt, decimate1, decimate2, tZoom, fZoom);
            iter->partitions.push_back (part);
         }
      }
      // calculate partitions for measurement channels
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         double rate = iter->info.dataRate;
         int decimate1 = (rate > fSample) ? (int) (rate / fSample + 0.5) : 1;
         int decimate2 = (fZoomSample > 0) ? 
            (int) (rate / (double)decimate1 / fZoomSample + 0.5) : 1;
         if (decimate2 < 1) {
            decimate2 = 1;
         }
         if ((rate <= 0) || (decimate1 <= 0) || (decimate2 <= 0) ||
            (fZoom < 0) || (tZoom < 0)) {
            continue;
         }
         double dt = (rate > fSample / decimate2) ? 
            1.0 / (fSample / decimate2) : 1.0 / rate;
         string pname = diagObjectName::makeName (
                              iter->info.chName, step, pIndex);
         dataChannel::partition	part 
            (pname, ival.t0(), ival.dt(), dt, pt);
         part.setup (dt, decimate1, decimate2, tZoom, fZoom);
         iter->partitions.push_back (part);
         //cerr << "add " << pname << " @ " << ival.t0() << endl;
      }
      if (my_debug) cerr << "stdtest::addMeasPartitions() return true" << endl ;
      return true;
   }


   bool stdtest::addSyncPoint (const interval& ival, 
                     int measPeriod, int measPoint)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::addSyncPoint( measPeriod = " << measPeriod << ", measPoint = " << measPoint << ")" << endl ;
      syncpointer	sync;
      callbackarg	arg;
      arg.measPoint = measPoint;
      arg.measPeriod = measPeriod;
      arg.ival = ival.ival;
      sync.reset (new (nothrow) stdtestsync 
                 (*this, arg, syncpoint::dataReady,
                 ival.t0() + ival.dt()));
      if (sync.get() == 0) {
         return false;
      }
      syncqueue.push_back (sync);
      timeAhead = ival.t0();
      if (my_debug) cerr << "stdtest::addSyncPoint() return true" << endl ;
      return true;
   }


   bool stdtest::newMeasPoint (int index, int measPoint)
   {
      return false;
   }


   bool stdtest::prepareNextPoint (const callbackarg& id)
   {
      if (my_debug) cerr << "stdtest::prepareNextPoint()" << endl ;
      if (avrgsize <= 0) {
         return true;
      }
      semlock		lockit (mux);
   
      // clear outdated measurement intervals
      if ((int)intervals.size() >= 2 * avrgsize) {
         // look for a bad one
         intervallist::iterator	iter;
         int			indx = 0;
         for (iter = intervals.begin(); iter != intervals.end();
             iter++, indx++) {
            if (!iter->good) {
               break;
            }
         }
         // if none found, use first one
         if (iter == intervals.end()) {
            iter = intervals.begin();
            indx = 0;
         }
         // delete interval
         intervals.erase (iter);
      
          // delete corersponding partition of stimuli
         for (stimuluslist::iterator iter = stimuli.begin();
             iter != stimuli.end(); iter++) {
            if ((int)iter->partitions.size() > indx) {
               dataChannel::partitionlist::iterator p =
                  iter->partitions.begin();
               advance (p, indx);
               iter->partitions.erase (p);
            }
         }
         // delete corresponding partition of measurement channels
         for (measlist::iterator iter = meas.begin();
             iter != meas.end(); iter++) {
            if ((int)iter->partitions.size() > indx) {
               dataChannel::partitionlist::iterator p =
                  iter->partitions.begin();
               advance (p, indx);
               iter->partitions.erase (p);
            }
         }
      }
   
      // add new point
      int i = id.measPeriod + syncqueue.size() + 1;
      if (!newMeasPoint (i, id.measPoint)) {
	 if (my_debug) cerr << "stdtest::prepareNextPoint() return true, line " << __LINE__ << endl ;
         return true;
      }
   
      // add stimuli partitions to data distribution
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (!iter->isReadback || (iter->duplicate)) {
            continue;
         }
         dataChannel::partitionlist 	p;
         p.push_back (iter->partitions.back());
         if (!dataMgr->add (iter->readback, p)) {
	    if (my_debug) cerr << "stdtest::prepareNextPoint() return false, line "<< __LINE__  << endl ;
            return false;
         }
      }
   
      // add measurement partitions to data distribution
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         if (iter->duplicate) {
            continue;
         }
         dataChannel::partitionlist 	p;
         p.push_back (iter->partitions.back());
         if (!dataMgr->add (iter->name, p)) {
	    if (my_debug) cerr << "stdtest::prepareNextPoint() return false, line " << __LINE__ << endl ;
            return false;
         }
      }
   
      if (my_debug) cerr << "stdtest::prepareNextPoint() return true" << endl ;
      return true;
   }


   bool stdtest::getNextSyncPoint (const callbackarg& id,
                     syncpointer& sync)
   {
      semlock		lockit (mux);
   
      // get next sync point
      if (!syncqueue.empty()) {
         sync = syncqueue.front();
         syncqueue.pop_front();
      }
      else {
         sync.reset ();
      }
      return true;
   }


   bool stdtest::syncAction (const callbackarg& id, 
                     syncpointer& sync, bool& note)
   {
      bool		dataerror = false;
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::syncAction()" << endl;
      // analysis disabled: delete data object
      int num = id.measPoint * averages + id.measPeriod;
      if (my_debug) cerr << "sync action (" << num << ") of " << myname << endl;
   
      // check if data was ok
      if (doAnalysis) 
      {
         gdsDataObject* 	dobj;
         for (stimuluslist::iterator iter = stimuli.begin(); iter != stimuli.end(); iter++) 
	 {
            if (iter->isReadback) 
	    {
               dobj = storage->findData (diagObjectName::makeName (iter->readbackInfo.chName, step, num));
               if ((dobj == 0) || (dobj->error)) 
	       {
                  dataerror = true;
		  if (my_debug) cerr << " syncAction() - dataerror, line " << __LINE__ << endl ;
               }
            }
         }
         // data object from measurement channels
         for (measlist::iterator iter = meas.begin(); iter != meas.end(); iter++) 
	 {
            dobj = storage->findData (diagObjectName::makeName (iter->info.chName, step, num));
            if ((dobj == 0) || (dobj->error)) 
	    {
               dataerror = true;
	       if (my_debug) cerr << " syncAction() - dataerror, line " << __LINE__ << endl ;
            }
         }
      } // if (doAnalysis)

      // call pause if data error
      if (dataerror) 
      {
         dataerrors++; // dataerrors is a member of class stdtest.
         if (dataerrors >= maxDataErrors)  // maxDataErrors is 100.
	 {
            syncAbort (id);
	    if (my_debug) cerr << "syncAction() - dataerrors exceeds maxDataErrors, line " << __LINE__ << endl ;
            return false;
         }
         syncpointer next;
         if (!syncPause (id, next)) 
	 {
	    if (my_debug) cerr << "syncAction() - syncPause() failed, line " << __LINE__ << endl ;
            return false;
         }
         notify.send (cmdnotify::dataError);
      } // if (dataerror)
   
      // if no analysis just delete measured data
      if (!doAnalysis) 
      {
         // data object from stimuli channels
         for (stimuluslist::iterator iter = stimuli.begin(); iter != stimuli.end(); iter++) 
	 {
            if (iter->isReadback) 
	    {
               storage->eraseData (diagObjectName::makeName (iter->readbackInfo.chName, step, num));
            }
         }
         // data object from measurement channels
         for (measlist::iterator iter = meas.begin(); iter != meas.end(); iter++) 
	 {
            storage->eraseData (diagObjectName::makeName (iter->info.chName, step, num));
         }
         // mark corresponding interval as bad
         for (intervallist::iterator iter = intervals.begin(); iter != intervals.end(); iter++) 
	 {
            if (iter->ival == id.ival) 
	    {
               iter->good = false;
               break;
            }
         }
         note = false;
      }
      // else do analysis
      else 
      {
         // call analysis routine
         if (!analyze (id, avrgnum, note)) 
	 {
	    if (my_debug) cerr << "syncAction() - analyze() returned false, line " << __LINE__ << endl ;
            return false;
         }
         // increase average num
         avrgnum++;
      }
   
      // stop if end of measurement
      if ((avrgsize > 0) && (averageType == 0) && (avrgnum >= averages)) {
         stopMeasurements (num + 1);
         syncqueue.clear();
      }
      // else prepare next measurement point
      else 
      {
         if (!prepareNextPoint (id)) 
	 {
            return false;
         }
      }
   
      // resume on data error 
      if (dataerror) 
      {
         syncpointer next;
	 if (my_debug) cerr << "syncAction() - resume on data error, line " << __LINE__ << endl ;
         if (!syncResume (id, next)) {
	    if (my_debug) cerr << "syncAction() - syncResume() failed, line " << __LINE__ << endl ;
            return false;
         }
         if (next.get() != 0) {
            sync = next;
            return true;
         }
      }   
   
      // check if traces should be kept
      if (keepTraces >= 0) {
	 if (my_debug) cerr << "stdtest::syncAction() - purgeChannelData()" << endl ;
         storage->purgeChannelData (keepTraces, step, num + 1);
      }   
   
      // return next sync point
      bool ret = getNextSyncPoint (id, sync);
   
      if (my_debug) cerr << "stdtest::syncAction() return " << (ret ? "true" : "false")  << endl;
      return ret;
   }


   bool stdtest::syncPause (const callbackarg& id, syncpointer& sync)
   { 
      if (my_debug) cerr << "stdtest::syncPause()" << endl ;
      // continue running; disable analysis
      doAnalysis = false;
      sync.reset();
      if (my_debug) cerr << "stdtest::syncPause() return" << endl ;
      return true;
   }


   bool stdtest::syncResume (const callbackarg& id, syncpointer& sync)
   {
      // resume analysis
      if (my_debug) cerr << "stdtest::syncResume() " << endl ;
      doAnalysis = true;
      sync.reset();
      if (my_debug) cerr << "stdtest::syncResume() return" << endl ;
      return true;
   }


   bool stdtest::syncAbort (const callbackarg& id)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::syncAbort()" << endl ;
      // stop measurements
      int num = id. measPoint * averages + id.measPeriod;
      stopMeasurements (num);
      if (my_debug) cerr << "stdtest::syncAbort() return" << endl ;
   
      return true;
   }


   bool stdtest::syncTimeAhead (tainsec_t& ahead) const
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "stdtest::syncTimeAhead()" << endl ;
      // get next sync point
      if (timeAhead && !syncqueue.empty()) {
         ahead = timeAhead;
	 if (my_debug) cerr << "stdtest::syncTimeAhead() return true" << endl;
         return true;
      }
      else {
	 if (my_debug) cerr << "stdtest::syncTimeAhead() return false" << endl;
         return false;
      }
   }


   // maximum number of analysis threads
   const int maxAnalysisThreads = 4;
   // analysis thread priority
   const int taskAnalysisPriority = 15;

   // channel record
   struct chnrec {
      // channel name 
      string 		name;
      // result number
      int		rnum;
      // is stimulus?
      bool		stim;
      // callback argument pointer
      const stdtest::callbackarg* id;
   }; 
   // list of channel records
   typedef vector<chnrec> chnlist;

  // argument for channel analysis threads
   struct chnThreadArg {
      // measurment number
      int		measnum;
      // channel list
      chnlist		chns;
      // test object 
      stdtest*		test;
      // analysis method
      stdtest::channelAnalysis	method;
      // return argument
      bool		ret;
#ifdef P__DARWIN
      int		*count ;
#else
      // counting semaphore for thread count?
      sem_t*		count;
#endif
   };
   typedef struct chnThreadArg chnThreadArg;

  // channel analysis thread
extern "C" 
   void chnAnalysisThread (chnThreadArg& arg)
   {
      arg.ret = true;
      if (my_debug)
      { // JCB
	 cerr << "chnAnalysisThread: arg.chns size = " << (int) arg.chns.size() << endl ;
      }
      for (chnlist::iterator iter = arg.chns.begin(); iter != arg.chns.end(); iter++) 
      {
	 if (my_debug)
	 {
	    cerr << "chnAnalysisThread: iter.rnum = " << iter->rnum << endl ; // JCB
	    cerr << "chnAnalysisThread: iter.stim = " << iter->stim << endl ; // JCB
	    cerr << "chnAnalysisThread: iter.name = " << iter->name << endl ; // JCB
	 }
         arg.ret &= (arg.test->*arg.method) (iter->rnum, arg.measnum, 
                              iter->name, iter->stim, *(iter->id));
      }
#ifdef P__DARWIN
      --(*arg.count) ;
      if (my_debug)
      {
	 cerr << "chnAnalysisThread: arg.count decremented to " << (*arg.count) << endl ;
      }
#else
      {
	 int retval ;
	 if ((retval = sem_post (arg.count)) < 0)
	 {
	    if (my_debug)
	    {
	       cerr << "chnAnalysisThread: sem_post returned " << retval << endl ;
	    }
	 }
      }
#endif
   }

   bool stdtest::callChannelAnalysis (const callbackarg& id,
                     int measnum, channelAnalysis method, 
                     int firstres, stimulustype inclStim, bool mt,
                     bool skipDuplicates)
   {
      // build channel list first
      int 	num = id.measPoint * averages + id.measPeriod;
      int 	rnum = firstres;// result number 
      chnrec	rec;		// record 
      chnlist	chns;		// channel list
   
      if (my_debug) cerr << "stdtest::callChannelAnalysis()" << endl ;
      mux.lock();
      // get names of stimuli channels
      if (inclStim != stimNone) {
         for (stimuluslist::iterator iter = stimuli.begin();
             iter != stimuli.end(); iter++) {
            if (skipDuplicates && iter->duplicate) {
               continue;
            }
            if ((inclStim == stimAll) ||
               ((inclStim == stimReadback) && (iter->isReadback))) {
               if (iter->isReadback) {
                  rec.name = diagObjectName::makeName 
                     (iter->readbackInfo.chName, step, num);
               } 
               else {
                  rec.name = "";
               }
               rec.rnum = rnum;
               rec.stim = true;
               rec.id = &id;
               chns.push_back (rec);
               rnum++;
            }
         }
      }
      // get names of measurement channels
      if (meas.empty())
      {
	 if (my_debug) cerr << "callChannelAnalysis: meas empty!" << endl ;
      } else
      {
	 if (my_debug)
	 { // JCB
	    cerr << "callChannelAnalysis: meas has " << (int) meas.size() << " items" << endl ;
	 }
	 for (measlist::iterator iter = meas.begin(); iter != meas.end(); iter++) 
	 {
	    if (skipDuplicates && iter->duplicate) {
	       continue;
	    }
	    if (my_debug)
	    { // JCB
	       cerr << "callChannelAnalysis: creating name for " << iter->info.chName << endl ;
	    }
	    rec.name = diagObjectName::makeName ( iter->info.chName, step, num);
	    if (my_debug)
	    { // JCB
	       cerr << "   name is " << rec.name << endl ;
	    }
	    rec.rnum = rnum;
	    rec.stim = false;
	    rec.id = &id;
	    chns.push_back (rec);
	    rnum++;
	 }
      }
      mux.unlock();   
   
      // do analysis: loop over channel list
      bool		ret = true;
      if (!mt || (chns.size() <= 1) || (maxAnalysisThreads <= 0)) 
      {
	if (my_debug) cerr << "callChannelAnalysis: Call method synchronously" << endl ;
        // just call method synchronously
         for (chnlist::iterator iter = chns.begin(); iter != chns.end();
             iter++) {
            ret &= (this->*method) (iter->rnum, measnum, 
                                 iter->name, iter->stim, *(iter->id));
         }
      }
      // multi-thread it
      else 
      {
         // channel list for threads
	 chnThreadArg	args[maxAnalysisThreads];

      	 // thread ID of threads
         taskID_t	TID[maxAnalysisThreads];

#ifdef P__DARWIN
	 // This is going to take the place of the unnamed semaphores which
	 // are not implemented in Darwin.  The semaphore (implemented with
	 // sem_init(), sem_post(), and sem_wait() are just used to keep 
	 // the callChannelAnalysis() function from returning before the
	 // threads implemented with chnAnalysisThread() return.  Since the
	 // functions called with chnAnalysisThread() use data created in
	 // callChannelAnalysis() to have that function return would cause
	 // the data to go out of scope.  So count keeps track of how many
	 // threads are still running, and callChannelAnalysis() won't return
	 // until the count goes to 0.
	 int		count ;  
#else
         sem_t		count;	// thread sync
#endif
      
	 if (my_debug)
	 { // JCB
	    cerr << "callChannelAnalysis: use threads." << endl ;
	    cerr << "callChannelAnalysis: " << (int) chns.size() << " chns in args list." << endl ;
	 }
         // split list
         int i = 0;
         for (chnlist::iterator iter = chns.begin(); iter != chns.end();
             iter++, i++) {
            args[i % maxAnalysisThreads].chns.push_back (*iter);
         }
	 if (my_debug)
	 { // JCB
	    cerr << "callChannelAnalysis: i = " << i << endl ;
	    cerr << "callChannelAnalysis: size of args[0].chns = " << (int) args[0].chns.size() << endl ;
	 }
         // create threads
#ifdef P__DARWIN
	 // Set count to the largest number of possible threads.  As each thread quits, count will be decremented.
	 count = maxAnalysisThreads ;
#else
	 {
	    // Don't ignore the return value of system calls.  
	    int retval ;
	    if ((retval = sem_init (&count, 0, 0)) < 0)
	    { // JCB
	       {
		  cerr << "callChannelAnalysis: sem_init() returned " << retval << " at " << __FILE__ << ", line " << __LINE__ << endl ;
	       }
	       abort() ; // Don't continue.
	    }
	 }
	 { // JCB
	    int	value ;
	    if (sem_getvalue(&count, &value))
	    {
	       cerr << "callChannelAnalysis: sem_getvalue() failed in " << __FILE__ << ", line " << __LINE__ << endl ;
	    }
	    else
	    {
	       if (my_debug)
	       {
		  cerr << "callChannelAnalysis: sem_init(&count, 0 0), count = " << value << endl ;
	       }
	    }
	 }
#endif
         for (i = 0; i < maxAnalysisThreads; i++) {
            // anything to do?
            if (args[i].chns.empty()) 
	    {
               args[i].ret = true;
#ifdef P__DARWIN
	       // No thread will be created, so decrement the count of threads.
	       --count ;
	       if (my_debug)
	       {
		  cerr << "callChannelAnalysis: count decremented to " << count << " because chns.empty() is true" << endl ;
	       }
#else
	       {
		  // don't ignore the return value of system calls.
		  int retval ;
		  if ((retval = sem_post (&count)) < 0)
		  {
		     cerr << "callChannelAnalysis: sem_post(&count) failed in " << __FILE__ << " at line " << __LINE__ << endl ;
		  }
		  else
		  {
		     int value ;
		     if (sem_getvalue(&count, &value))
		     {
			cerr << "callChannelAnalysis: sem_getvalue() failed in " << __FILE__ << " at line " << __LINE__ << endl ;
		     }
		     else
		     {
			if (my_debug)
			{
			   cerr << "callChannelAnalysis: sem_post(&count) returned " << retval << ", count = " << value << endl ;
			}
		     }
		  }
	       }
#endif
               continue;
            }
            // fill in thread arguments
            args[i].measnum = measnum;
            args[i].test = this;
            args[i].method = method;
            args[i].count = &count;
            int attr = PTHREAD_CREATE_DETACHED;
	    if (my_debug)
	    { // JCB
	       cerr << "callChannelAnalysis: Creating thread " << i << 
	               " args[" << i << "].measnum = " << args[i].measnum <<
		       " args[" << i << "].chns.size = " << (int) args[i].chns.size() << endl ;
	    }
	    if (my_debug)
	    {
	       for (unsigned int n = 0; n < args[i].chns.size(); n++) // JCB
	       { // JCB
		  cerr << "    args[" << i << "].chns[" << n << "].rnum = " << args[i].chns[n].rnum << endl ;
		  cerr << "    args[" << i << "].chns[" << n << "],name = " << args[i].chns[n].name << endl ;
	       } // JCB
	    }
            // create thread
            if (taskCreate (attr, taskAnalysisPriority, &TID[i], "tAnalysis", (taskfunc_t) chnAnalysisThread, (taskarg_t) &args[i]) != 0) 
	    {
               return false;
            }
         }
         // wait for them to finish
#ifdef P__DARWIN
	 if (my_debug)
	 {
	    cerr << "callChannelAnalysis: done creating threads, semaphore count value is " << count << endl ; 
	 }
	 // count will be positive while there are threads that have not returned.
	 while (count > 0)
	 {
	    struct timespec small_time ;
	    
	    small_time.tv_sec = 0 ;
	    small_time.tv_nsec = 1000 ; // 1uS

	    // To allow other threads to do something, sleep for a small time. We don't really care 
	    // if the call gets terminated early, just that this thread blocks temporarily.
	    nanosleep(&small_time, (struct timespec *) NULL) ;
	 }
#else
	 {
	    int value ;
	    if (sem_getvalue(&count, &value))
	    {
	       cerr << "callChannelAnalysis: sem_getvalue() failed in " << __FILE__ << " at line " << __LINE__ << endl ; 
	    }
	    else
	    {
	       if (my_debug)
	       {
		  cerr << "callChannelAnalysis: done creating threads, semaphore count value is " << value << endl ; 
	       }
	    }
	 }
         for (i = 0; i < maxAnalysisThreads; i++) {
            if (sem_wait (&count))
	    {
	       if (my_debug)
	       { // JCB
		  int  err = errno ;
		  cerr << "callChannelAnalysis: sem_wait() failed, " << strerror(err) << " in " <<  __FILE__ << " at line " << __LINE__ << endl ;
	       }
	    }
         }
#endif
         // get result
         for (i = 0; i < maxAnalysisThreads; i++) {
            ret &= args[i].ret;
         }
#ifndef P__DARWIN
         if (sem_destroy (&count))
	 {
	    if (my_debug)
	    {
	       int err = errno ;
	       cerr << "callChannelAnalysis: sem_destroy() failed, " << strerror(err) << " in " <<  __FILE__ << " at line " << __LINE__ << endl ;
	    }
	 }
#endif
      }
   
      if (my_debug) cerr << "stdtest::callChannelAnalysis() return" << endl ;
      return ret;
   }


}
