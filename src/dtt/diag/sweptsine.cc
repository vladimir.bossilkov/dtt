/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: sweptsine						*/
/*                                                         		*/
/* Module Description: swept sine response test				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */
#include <time.h>
#include <math.h>
#include <string.h>
#include <complex>
#include <vector>
#include <memory>
#include <utility>
#include <algorithm>
#include <iostream>
#include "gdsmain.h" 
#include "sweptsine.hh"
#include "diagnames.h"
#include "gdsalgorithm.h"
#include "diagdatum.hh"


static int my_debug = 0 ;

// maximum number of sampling points per (excitation) period
#define _MAX_OVERSAMPLING	4.0

// one second (in nsec)
#define __ONESEC		1E9

// fmod with positive return value
#define fMod(x,y)		((x) - (y) * floor ((x) / (y)))


//template class std::vector<diag::sweptsine::sweeppoint>;


namespace diag {
   using namespace std;
   using namespace thread;


   inline double power (double x, double y) 
   {
      return exp (log (x) * y);
   }

   double interpol (double f, const sweptsine::sweeppoints& envelope)
   {
      int l = 0;
      int r = envelope.size() - 1;
      // check special cases
      if (r < 0) {
         return 0;
      }
      else if (r == 0) {
         return envelope[0].ampl;
      }
      // check for out of range
      if (f <= envelope[l].freq) {
         return envelope[l].ampl;
      }
      if (f >= envelope[r].freq) {
         return envelope[r].ampl;
      }
      // binary search to find interval
      while (l < r - 1) {
         int m = (l + r) / 2;
         if (f < envelope[m].freq) {
            r = m;
         }
         else {
            l = m;
         }
      }
      // check if finite interval
      if (envelope[l].freq >= envelope[r].freq) {
         return envelope[l].ampl;
      }
      // interpolate
      return envelope[l].ampl + (envelope[r].ampl - envelope[l].ampl) /
         (envelope[r].freq - envelope[l].freq) * (f - envelope[l].freq);
      return 0;
   }


   sweptsine::sstmpresult::sstmpresult (const std::string& Name, 
                     int Averages) 
   : averages (0), name (Name), coeff (0) {
      allocate (Averages);
   }


   sweptsine::sstmpresult::~sstmpresult () 
   {
      allocate ();
   }


   bool sweptsine::sstmpresult::allocate (int Averages)
   {
      // delete old arrays
      if (coeff != 0) {
         delete [] coeff;
         coeff = 0;
      }
      // alocate new ones
      averages = Averages;
      if (averages <= 0) {
         return true;
      }
      else {
         coeff = new (nothrow) complex<double>[averages + 2];
         return valid();
      }
   }


   bool sweptsine::sstmpresult::valid () const
   {
      return (coeff != 0);
   }


   sweptsine::sstmpresult::sstmpresult (const sstmpresult& tmp) 
   : averages (0), name (""), coeff (0)  {
      *this = tmp;
   }


   sweptsine::sstmpresult& sweptsine::sstmpresult::operator= (
                     const sstmpresult& tmp)
   {
      if (this != &tmp) {
         name = tmp.name;
         if (!allocate (tmp.averages)) {
            return *this;
         }
         memcpy (coeff, tmp.coeff, 
                (averages + 2) * sizeof (complex<double>));
      }
      return *this;
   }


   sweptsine::sweptsine () 
   : stdtest (sweptsinename), 
   settlingTime (0), harmonicOrder (1), window (0), fftResult (false),
   sweepType (0), sweepDir (0), fStart (0), fStop (0), nSweep (0), 
   pAverages (1), AChannels (0), fMin (0), fMax (0), fMaxMeas (0), 
   fMaxSample (0), numA (0), mTime (0), mTimeAdd (0), pTime (0), 
   sTime (0), rTime (0), sstmps (0), sstmpsSize (0)
   {
      measTime[0] = -1;
      measTime[1] = -1;
   }


   sweptsine::~sweptsine () 
   {
      delete [] sstmps;
   }


   diagtest* sweptsine::self () const {    
      return new (std::nothrow) sweptsine ();
   }



   bool sweptsine::readParam (ostringstream& errmsg)
   {
      if (my_debug) cerr << "sweptsine::readParam()" << endl ;
      // call parent method
      if (!stdtest::readParam (errmsg)) {
         return false;
      }
   
      semlock		lockit (mux);
      bool		err = false;

      // read double precision
      if (!test->getParam (*storage->Test, ssDoublePrecFreq, doublePrecFreq)) {
        doublePrecFreq = 0;
      }
      // read measurement time
      if (!test->getParam (*storage->Test,
                          ssMeasurementTime, *measTime, 2)) {
         errmsg << "Unable to load values from Test." <<
            ssMeasurementTime << endl;
         err = true;
      }
      // read settling time
      if (!test->getParam (*storage->Test,
                          ssSettlingTime, settlingTime)) {
         errmsg << "Unable to load value from Test." <<
            ssSettlingTime << endl;
         err = true;
      }
      // Read ramp down time
      if (!test->getParam(*storage->Test, ssRampDown, rampDown)) {
	 errmsg << "Unable to lad value from Test." << ssRampDown << endl ;
	 err = true ;
      }
      // Read ramp up time
      if (!test->getParam(*storage->Test, ssRampUp, rampUp)) {
	 errmsg << "Unable to lad value from Test." << ssRampUp << endl ;
	 err = true ;
      }

      // read window
      if (!test->getParam (*storage->Test, ssWindow, window)) {
         errmsg << "Unable to load value from Test." <<
            ssWindow << endl;
         err = true;
      }
   
      // read sweep type
      if (!test->getParam (*storage->Test,
                          ssSweepType, sweepType)) {
         errmsg << "Unable to load value from Test." <<
            ssSweepType << endl;
         err = true;
      }
      // read sweep direction
      if (!test->getParam (*storage->Test,
                          ssSweepDirection, sweepDir)) {
         errmsg << "Unable to load value from Test." <<
            ssSweepDirection << endl;
         err = true;
      }
      // read start frequency
      if (!test->getParam (*storage->Test,
                          ssStartFrequency, fStart)) {
         errmsg << "Unable to load value from Test." <<
            ssStartFrequency << endl;
         err = true;
      }
      // read stop frequency
      if (!test->getParam (*storage->Test,
                          ssStopFrequency, fStop)) {
         errmsg << "Unable to load value from Test." <<
            ssStopFrequency << endl;
         err = true;
      }
      // read number of sweep points
      if (!test->getParam (*storage->Test,
                          ssNumberOfPoints, nSweep)) {
         errmsg << "Unable to load value from Test." <<
            ssNumberOfPoints << endl;
         err = true;
      }
      // read number of A channels
      if (!test->getParam (*storage->Test, ssAChannels, AChannels)) {
         errmsg << "Unable to load value from Test." <<
            ssAChannels << endl;
         err = true;
      }
      // handle averages
      pAverages = averages;
      averages = 1;
      averageType = 0;
   
      // read frequency points
      gdsDatum 		fpnts;
      fPointsFile.clear();
      if (test->getParam (*storage->Test, ssSweepPoints, fpnts)) {
         if (((fpnts.datatype == gds_float32) || 
             (fpnts.datatype == gds_float64)) &&
            (fpnts.dimension.size() == 1) &&
            (fpnts.value != 0)) {
            double 		pt;	// point value
            double		pt2=0;	// old point value
	    if (my_debug) cerr << "  reading frequency points, number of points is " << fpnts.elNumber() << ", sweepType is " << sweepType << endl ;
            for (int i = 0; i < fpnts.elNumber(); i++) {
               pt = (fpnts.datatype == gds_float32) ?
                  *((float*) fpnts.value + i) :
                  *((double*) fpnts.value + i);
               switch (sweepType) {
                  case 2: 
                     {
			if (my_debug) cerr << "  adding sweep point (freq = " << pt << ", ampl = 0, phase = 0)" << endl ; 
                        fPointsFile.push_back (sweeppoint (pt, 0));
                        break;
                     }
                  case 3:
                  case 4:
                  case 5:
                     {
                        if (i % 2 == 0) {
                           pt2 = pt;
                        } 
                        else {
			   if (my_debug) cerr << "  adding sweep point (freq = " << pt2 << ", ampl = " << pt << ", phase = 0)" << endl ; 
                           fPointsFile.push_back (sweeppoint (pt2, pt));
                        }
                        break;
                     }
                  default: 
                     {
                        break;
                     }
               }
            }
         } 
         else {
            errmsg << "Unable to load values from Test." <<
               ssSweepPoints << endl;
            err = true;
         }
      }
      sort( fPointsFile.begin(), fPointsFile.end() );
   
      // read stimuli channel
      if (!readStimuliParam (errmsg, true, sinewaveOnly, 1)) {
         return false;
      }
      if (stimuli.empty()) {
         errmsg << "No stimulus channel defined" << endl;
         err = true;
      }
      // set use active time
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         iter->useActiveTime = true;
      }
   
      // read measurement channels
      if (!readMeasParam (errmsg)) {
         return false;
      }
      // set use active time
      for (measlist::iterator iter = meas.begin();
          iter != meas.end(); iter++) {
         iter->useActiveTime = true;
      }
   
      // check heterodyne frequency
      double fZoom = 0.0;
      if (!heterodyneFrequency (fZoom) || (fZoom != 0)) {
         errmsg << "Heterodyned channels not supported." << endl;
         err = true;
      }
   
      // read harmonic order
      if (!test->getParam (*storage->Test,
                          ssHarmonicOrder, harmonicOrder)) {
         errmsg << "Unable to load value from Test." <<
            ssHarmonicOrder << endl;
         err = true;
      }
      if (harmonicOrder <= 0) {
         harmonicOrder = 1000;
      }
   
      // handle negative AChannels value
      if (AChannels < 0) {
         AChannels = meas.size();
      }
   
      // read fft result
      if (!test->getParam (*storage->Test, ssFFTResult, fftResult)) {
         errmsg << "Unable to load value from Test." <<
            ssFFTResult << endl;
         err = true;
      }
   
      if (my_debug) cerr << "sweptsine::readParam() returns " << (!err ? "TRUE" : "FALSE") << endl ;
      return !err;
   }


   bool sweptsine::calcTimes (std::ostringstream& errmsg,
                     tainsec_t& t0)
   {
      semlock		lockit (mux);
      bool		err = false;
   
      if (my_debug) cerr << "sweptsine::calcTimes(..., t0 = " << t0 << ")" << endl ;
      // check settling time
      if (settlingTime < 0) {
         errmsg << "Settling time must be positive or zero" << endl;
         return false;
      }
      // check harmonic order
      if (harmonicOrder < 1) {
         errmsg << "Order of harmonics must be at least one" << endl;
         err = true;
      }
      // check sweep type
      if ((sweepType < 0) || (sweepType > 5)) {
         errmsg << "Illegal sweep type (" << sweepType << ")" << endl;
         err = true;
      }
      // check sweep direction
      if ((sweepDir < 0) || (sweepDir > 1)) {
         errmsg << "Illegal sweep direction (" << sweepDir << ")" << endl;
         err = true;
      }
      // check start frequency
      if (((sweepType <= 1) || (sweepType >= 4)) && (fStart < 0)) {
         errmsg << "Illegal start frequency (" << fStart << ")" << endl;
         err = true;
      }
      // check stop frequency
      if (((sweepType <= 1) || (sweepType >= 4)) && (fStop <= fStart)) {
         errmsg << "Illegal stop frequency (" << fStop << ")" << endl;
         err = true;
      }
      // check sweep points
      if (((sweepType <= 1) || (sweepType >= 4)) && (nSweep <= 0)) {
         errmsg << "Illegal number of sweep points (" << 
            nSweep << ")" << endl;
         err = true;
      }
      // check averages
      if (pAverages < 1) {
         errmsg << "Number of averages must be at least one" << endl;
         err = true;
      }
      // check user supplied sweep points
      if ((sweepType >= 2) && fPointsFile.empty()) {
         errmsg << "Illegal number of sweep points (" << 
            fPointsFile.size() << ")" << endl;
         err = true;
      }
      // check number of A channels
      if ((AChannels < 0) || (AChannels > (int)meas.size())) {
         errmsg << "number of A channels is out of range" << endl;
         err = true;
      }
      if (err) {
         return false;
      }
      // total number of A channels
      numA = AChannels + stimuli.size();
      // number of result records
      rnumber = 2 * numA;
   
      // determine smallest and largest excitation frequency
      fMin = 1E99;
      fMax = 0;
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
	 if (my_debug) cerr << "  " << iter->name << " freq = " << iter->freq << ", ampl = " << iter->ampl << endl ;
         fMin = min (fMin, iter->freq);
         fMax = max (fMax, iter->freq);
      }
      if ((fMin <= 0) || (fMin > 1E98) || (fMax <= 0)) {
         errmsg << "Frequencies must be positive" << endl;
         return false;
      }
      // highest frequency of interest
      if (harmonicOrder > _MAX_OVERSAMPLING) {
         fMaxMeas = harmonicOrder * fMax;
      }
      else {
         fMaxMeas = _MAX_OVERSAMPLING * fMax;
      }
      // determine sampling rates
      samplingFrequencies (fMaxMeas, fMinSample, fMaxSample);
      if (fMinSample < 2 * fMax) {
         errmsg << "Frequency too high for at least one sample rate" << endl;
         return false;
      }
      if (my_debug) {
	 cerr << "sweptsine::calcTimes() - fMaxMeas = " << fMaxMeas <<  
		  ", fMinSample = " << fMinSample << ", fMaxSample = " << fMaxSample << endl ;
      }
   
      // calculate time grid
      timeGrid = calcTimeGrid (fMaxSample / 2.0, &t0);
      // precursor time
      pTime = N_LEAD * timeGrid;
      if (my_debug) cerr << "sweptsine::calcTimes() - timeGrid = " << timeGrid << ", pTime = " << pTime << endl ;
   
      // calculate measurement time
      mTime = measurementTime (measTime[0], measTime[1], fMin, true);
      if ((mTime <= 0) || (timeGrid <= 0)) {
         errmsg << "Measurement time must be positive" << endl;
         return false;
      }
      mTimeAdd = adjustForSampling (mTime, timeGrid) - mTime;
      if (my_debug) cerr << "  mTimeAdd = " << mTimeAdd << endl ;

      // calculate settling time
      sTime = settlingTime * mTime;
      if (sTime < 0) {
         sTime = 0;
      }
      if (my_debug) cerr << "  sTime = " << sTime << endl ;

      // calculate ramp time; make sure dwell time lies on grid
      if (rampUp > sTime) {
	 rTime = rampUp ;
      }
      else
	 // Take out 1 second limit on ramp based on settling time for mHz freqs.
	 //rTime = min (sTime, 1.0);
	 rTime = sTime ; 

      if (my_debug) cerr << "sweptsine::calcTimes() - sTime = " << sTime << ", rTime = " << rTime << endl ;

      sTime = adjustForSampling (sTime + rTime, timeGrid) - rTime;
      if (my_debug) cerr << "sweptsine::calcTimes() - sTime adjusted for sampling = " << sTime << endl ;
   
      // Use a value from a widget to set the ramp down time.
      testExc->setRampDown((tainsec_t)(rampDown * 1E9 + 0.5)) ;
      if (my_debug) cerr << "  rampDown time is " << rampDown * 1E9 << endl ;
      testExc->setRampUp((tainsec_t)(rampUp * 1E9 + 0.5)) ;
      if (my_debug) cerr << "  rampUp time is " << rampUp * 1E9 << endl ;
      // make sure we don't add new points automatically
      avrgsize = -1;
   
      if (my_debug) cerr << "sweptsine::calcTimes() return" << endl ;
      return true;
   }


   bool sweptsine::newMeasPoint (int i, int measPoint)
   {
      // only allow measurement points for sweep
      if (my_debug) cerr << "sweptsine::newMeasPoint( i=" << i << ", measPoint=" << measPoint<< ")" << endl ;
      if ((i != 0) || (measPoint >= nSweep)) {
	 if (my_debug) cerr << "  measPoint = " << measPoint << ", nSweep = " << nSweep << ", return false." << endl ;
         return false;
      }
   
      semlock		lockit (mux);
   
      // calulate start time
      tainsec_t	start = T0 + (tainsec_t) ((rTime + sTime) * __ONESEC + 0.5);

      if (my_debug)
      {
	 cerr << "  start = " << start << endl ;
	 cerr << "    T0       = "<< T0 << endl ;
	 cerr << "    rTime    = " << rTime << endl ;
	 cerr << "    sTime    = " << sTime << endl ;
      }
   
      // fine adjust to time grid & calc. duration
      start = fineAdjustForSampling (start, timeGrid);
      tainsec_t	duration = (tainsec_t) ((pAverages * (mTime + mTimeAdd) + pTime + timeGrid) * __ONESEC + 0.5);
      tainsec_t	tp = fineAdjustForSampling (pTime * __ONESEC + 0.5, timeGrid);
      cerr << "P: start " << (double)(start % (100*_ONESEC))/1E9 << " dwell=" << 
         rTime + sTime << " d=" << (double)duration/1E9 << 
         " tp = " << tp <<  endl;
   
      // add interval
      intervals.push_back (interval (start, duration));
   
      // add new partitions
      if (!addMeasPartitions (intervals.back(), measPoint * averages + i, fMaxSample, tp)) {
	 if (my_debug) cerr << "addMeasPartitions() failed, newMeasPoint() return false" << endl ;
         return false;
      }
   
      // add synchronization point
      if (!addSyncPoint (intervals.back(), i, measPoint)) {
	 if (my_debug) cerr << "addSyncPoint() failed, newMeasPoint() return false" << endl ;
         return false;
      }
      if (my_debug) cerr << "sweptsine::newMeasPoint() return true" << endl ;
      return true;
   }


   bool sweptsine::calcMeasurements (std::ostringstream& errmsg,
                     tainsec_t t0, int measPoint)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "sweptsine::calcMeasurements (..., t0=" << t0 << ", measPoint=" << measPoint << ")" << endl ;
      // determine excitation signals
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (!iter->calcSineSignal (t0, -1, rTime * __ONESEC + 0.5)) {
            errmsg << "Unable to calculate excitation signal" << endl;
            return false;
         }
      }
   
      // create initial measurement points
      tainsec_t	oldT0 = T0;
      T0 = t0;
      if (!newMeasPoint (0, measPoint)) {
         errmsg << "Unable to create measurement points" << endl;
         return false;
      }
      T0 = oldT0;
      cerr << "E: start " << (double)(stimuli.front().signals.back().start % (100*_ONESEC))/1E9 << 
         " ramp=" << rTime << " d=" << (double)stimuli.front().signals.back().duration/1E9 << endl;
   
      return true;
   }


   bool sweptsine::setup (ostringstream& errmsg, 
                     tainsec_t starttime, syncpointer& sync)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "sweptsine::setup(..., starttime = " << starttime << ", ...)" << endl ;
      // make sure partition and excitation lists are empty
      if (!delMeasurements (errmsg)) {
         return false;
      }
   
      // check stimulus
      if (my_debug) cerr << "  stimuli.size() = " << stimuli.size() << endl ;
      if (stimuli.size() != 1) {
         errmsg << "Illegal sweep stimulus" << endl;
         return false;
      }
      double 		a = stimuli.front().ampl;
      double 		f;
   
      if (my_debug) cerr << "  stimuli.front().ampl = " << stimuli.front().ampl << endl ;
      // caluclate sweep points

      if (my_debug) cerr << "  sweepType = " << sweepType << endl ;
      switch (sweepType) {
         // linear sweep
         case 0:
         // linear sweep: user supplied envelope
         case 4:
            {
	       if (my_debug) cerr << "  linear sweep, fStart = " << fStart << ", fStop = " << fStop << ", nSweep = " << nSweep << endl ;
               fPoints.clear();
               if ((fStart < 0) || (fStop <= fStart) || (nSweep < 1)) {
                  errmsg << "Illegal sweep parameters" << endl;
                  return false;
               }
               if (nSweep == 1) {
                  f = (fStart + fStop) / 2;
                  double ampl = (sweepType == 0) ? a : 
                     interpol (f, fPointsFile);
                  fPoints.push_back (sweeppoint (f, ampl));
               }
               else {
                  for (int i = 0; i < nSweep; i++) {
                     f = fStart + (double) i / (nSweep - 1.0) *
                        (fStop - fStart);
                     double ampl = (sweepType == 0) ? a : 
                        interpol (f, fPointsFile);
                     fPoints.push_back (sweeppoint (f, ampl));
                  }
               }
	       // reverse points if sweep downwards
	       if (sweepDir == 1) {
	          reverse (fPoints.begin(), fPoints.end());
	       }
               break;
            }
         // logarithmic sweep
         case 1:
         // logarithmic sweep: user supplied envelope
         case 5:
            {
               fPoints.clear();
               if ((fStart <= 0) || (fStop <= fStart) || (nSweep < 1)) {
                  errmsg << "Illegal sweep parameters" << endl;
                  return false;
               }
               if (nSweep == 1) {
                  f = (fStart * fStop) / (fStart + fStop);
                  double ampl = (sweepType == 1) ? a : 
                     interpol (f, fPointsFile);
                  fPoints.push_back (sweeppoint (f, ampl));
               }
               else {
                  for (int i = 0; i < nSweep; i++) {
                     f = fStart * power (fStop/fStart, 
                                        (double) i / (nSweep - 1.0));
                     double ampl = (sweepType == 1) ? a : 
                        interpol (f, fPointsFile);
                     fPoints.push_back (sweeppoint (f, ampl));
                  }
               }
	       // reverse points if sweep downwards
	       if (sweepDir == 1) {
	          reverse (fPoints.begin(), fPoints.end());
	       }
               break;
            }
	 // For user defined sweep, need to pay attention to the desired number of
	 // points.  If it's less than the number of points the user defined, we
	 // only want to use that many points.  But, if the sweep direction is up, 
	 // use the first n points the user defined.  If the sweep direction is down,
	 // use the last n points in reverse order.

         // user sweep: need to fill in amplitude
         case 2: 
            {
	       if (my_debug) cerr << "sweptsine::setup() - user sweep case 2, need to fill in amplitude" << endl ;
               fPoints.clear();
	       if (sweepDir == 1) {
		  // Sweep down, take the last points.
		  int n = fPointsFile.size() ;
		  int j = 0 ;
		  if (n > nSweep) n = nSweep ; // Limit the number of points to the number requested.
		  for (sweeppoints::reverse_iterator i = fPointsFile.rbegin(); j < n; j++, i++) {
		     fPoints.push_back (sweeppoint(i->freq, a)) ;
		  }
	       }
	       else {
		  // Sweep up, take the first points.
		  int n = fPointsFile.size() ;
		  int j = 0 ;
		  if (n > nSweep) n = nSweep ; // Limit the number of points to the number requested.
		  for (sweeppoints::iterator i = fPointsFile.begin(); j < n; j++, i++) {
		     fPoints.push_back (sweeppoint(i->freq, a)) ;
		  }
	       }
               break;
            }
         // user sweep: copy points
         case 3:
            {
	       if (my_debug) cerr << "sweptsine::setup() - user sweep case 3, copy points." << endl ;
               fPoints.clear();
	       if (sweepDir == 1) {
		  // Sweep down, take the last points.
		  int n = fPointsFile.size() ;
		  if (my_debug) cerr << "sweptsine::setup() - Sweep down, " << n << " points available" << endl ;
		  int j = 0 ;
		  if (n > nSweep) n = nSweep ; // Limit the number of points to the number requested.
		  if (my_debug) cerr << "sweptsine::setup() - use " << n << " points" << endl ;
		  for (sweeppoints::reverse_iterator i = fPointsFile.rbegin(); j < n; j++, i++) {
		     if (my_debug) cerr << "  freq = " << i->freq << ", ampl = " << i->ampl << endl ;
		     fPoints.push_back (*i) ;
		  }
	       }
	       else {
		  // Sweep up, take the first points.
		  int n = fPointsFile.size() ;
		  int j = 0 ;
		  if (n > nSweep) n = nSweep ; // Limit the number of points to the number requested.
		  for (sweeppoints::iterator i = fPointsFile.begin(); j < n; j++, i++) {
		     fPoints.push_back (*i) ;
		  }
	       }
               break;
            }
         default: 
            {
               errmsg << "Illegal sweep parameters" << endl;
               return false;
            }
      }
      // cerr << "SWEEP POINTS " << sweepType << endl;
      // for (sweeppoints::iterator i = fPoints.begin(); 
          // i != fPoints.end(); i++) {
         // cerr << "Sweep point " << i << " @ " << i->freq << " with " << i->ampl << endl;
      // }
   
      // add measurement descriptors
      int 		measPoint = 0;
      tainsec_t		t0 = starttime;
      int excwait = fPoints.size() / 200 + 1;
      if (excwait > 20) excwait = 20; /* but never more than 10s */
      t0 += excwait * _ONESEC;
      // loop over sweep points
      for (sweeppoints::iterator iter = fPoints.begin(); 
          iter != fPoints.end(); iter++, measPoint++) {
         stimuli.front().freq = iter->freq;
         stimuli.front().ampl = iter->ampl;
         stimuli.front().offs = 0;
         stimuli.front().phas = 0;
         // add next sweep step
         if (!addMeasurements (errmsg, t0, measPoint)) {
            return false;
         }
         // get phase at start of measurement
         cerr << "phi=" << 180/PI*stimuli.front().phas << 
            "  phiS=" << 360 * iter->freq * sTime <<
            "  phiP=" << 360 * iter->freq * pTime << endl;
         iter->phase = fmod (stimuli.front().phas + 
                            TWO_PI * iter->freq * (sTime + pTime), TWO_PI);
         if (!intervals.empty()) {
            // calculate start time of next sweep point
            t0 = intervals.back().t0() + intervals.back().dt();
         }
      }
   
      for (stimulus::awglist::iterator i = stimuli.front().signals.begin(); 
          i != stimuli.front().signals.end(); i++) {
         cerr << "E: start " << (double)(i->start % (100*_ONESEC))/1E9 << 
            " ramp=" << (double)i->ramptime[1]/1E9 << " d=" << (double)i->duration/1E9 << endl;
      }
   
      // start measurement
      if (!startMeasurements (errmsg)) {
         return false;
      }
   
      // get first synchronization point and return
      callbackarg		arg;
      arg.measPeriod = -1;
      return getNextSyncPoint (arg, sync);
   }


   bool sweptsine::end (ostringstream& errmsg)
   {
      semlock		lockit (mux);
      fPoints.clear();
      delete [] sstmps;
      sstmps = 0;
      return stdtest::end (errmsg);
   }


   bool sweptsine::analyze (const callbackarg& id, int measnum, 
                     bool& note)
   {
      semlock		lockit (mux);
   
      /////////////////////////////////////////////////////////
      // Init analysis first time around		     //
      /////////////////////////////////////////////////////////
   
      // cerr << "ANALYZE SSINE 1" << endl;
   
      if (id.measPoint == 0) {
      /////////////////////////// Create temporary storage
         if (sstmps != 0) {
            delete [] sstmps;
         }
         sstmpsSize = stimuli.size() + meas.size();
         sstmps = new (nothrow) sstmpresult [sstmpsSize];
         if (sstmps == 0) {
            return false;
         }
         int b = 0;
         for (stimuluslist::iterator iter = stimuli.begin();
             iter != stimuli.end(); iter++, b++) {
            if (iter->duplicate) {
               continue;
            }
            if (iter->isReadback) {
               sstmps[b] = 
                  sstmpresult (iter->readbackInfo.chName, pAverages);
            }
            else {
               sstmps[b] = sstmpresult (iter->excInfo.chName, pAverages);
            }
         }
         for (measlist::iterator iter = meas.begin();
             iter != meas.end(); iter++) {
            if (!iter->duplicate) {
               sstmps[b] = sstmpresult (iter->info.chName, pAverages);
               b++;
            }
         }
         sstmpsSize = b;
         cerr << "total analysis channels = " << b << endl;
      
      /////////////////////////// Create index
         // get index access class
         const diagIndex& 	indx = diagIndex::self();
         // find index / create new index if necessary
         gdsDataObject* 	iobj = storage->findData (stIndex);
         if (iobj == 0) {
            iobj = indx.newObject (0);
            if (iobj == 0) {
               return false;
            }
            storage->addData (*iobj, false);
         }
      
         // write index (common)
         int N = fPoints.size();
         ostringstream		entryCommon;
         for (int i = 0; i < numA; i++) {
            diagIndex::channelEntry (entryCommon, i, sstmps[i].name, 'A');
         }
         for (int i = 0; i < sstmpsSize; i++) {
            diagIndex::channelEntry (entryCommon, i, sstmps[i].name, 'B');
         }
      
         // transfer function entry
         ostringstream		entry1;
         entry1 << entryCommon.str();
         for (int i = 0; i < numA; i++) {
            for (int b = 0; b < sstmpsSize; b++) {
               int ofs = (b + 1) * N;
               diagIndex::resultEntry (entry1, rindex + i, ofs, N, i, b);
            }
         }
         indx.setEntry (*iobj, icTransferFunction, step, entry1.str());
      
         // coherence entry
         ostringstream		entry2;
         entry2 << entryCommon.str();
         for (int i = 0; i < numA; i++) {
            for (int b = 0; b < sstmpsSize; b++) {
               int ofs = (b + 1) * N;
               diagIndex::resultEntry (entry2, rindex + numA + i, 
                                    ofs, N, i, b);
            }
         }
         indx.setEntry (*iobj, icCoherenceFunction, step, entry2.str());
      
         // cerr << "ANALYZE SSINE 2" << endl;
      }
   
      /////////////////////////////////////////////////////////
      // Calculate sine coefficients of all channels	     //
      /////////////////////////////////////////////////////////
   
      // cerr << "ANALYZE SSINE 3" << endl;
      if (!callChannelAnalysis (id, measnum, 
                           (channelAnalysis) &sweptsine::sinedet, 
                           0, stimAll, false)) {
         return false;
      }
   
      /////////////////////////////////////////////////////////
      // Calculate transfer function between all channels    //
      /////////////////////////////////////////////////////////
   
      // cerr << "ANALYZE SSINE 4" << endl;
      if (!callChannelAnalysis (id, measnum, 
                           (channelAnalysis) &sweptsine::transfn, 
                           0, stimAll, false)) {
         return false;
      }
      cerr << "ANALYZE SSINE 5" << endl;
   
      // return
      note = true;
      return true;
   }


   bool sweptsine::sinedet (int resultnum, int measnum, 
                     string chnname, bool stim, const callbackarg& id)
   {
      cerr << "analyze " << chnname << " from " << measnum <<
         " into " << resultnum << endl;
   
      if ((id.measPoint < 0) || (id.measPoint >= (int)fPoints.size())) {
         return false;
      }
   
      // stimulus channel without a readback
      if (stim && chnname.empty()) {
         complex<double> 	x = fPoints[id.measPoint].ampl;
         sstmps[resultnum].coeff[0] = fPoints[id.measPoint].freq;
      
         for (int i = 0; i < pAverages + 1; i++) {
            sstmps[resultnum].coeff[i+1] = x;
         }
      }
      
      // measurement channels & stimulus readback channels
      else {
         // get time series access class
         const diagResult& aChn = diagChn::self();
         // get time series
         gdsDataObject* chndat = storage->findData (chnname);
         if (chndat == 0) {
            return false;
         }
         // get time series parameters
         int N;
         if (!aChn.getParam (*chndat, stTimeSeriesN, N)) {
            return false;
         }
         double dt;
         if (!aChn.getParam (*chndat, stTimeSeriesdt, dt) || (dt <= 0)) {
            return false;
         }
         double tp;
         if (!aChn.getParam (*chndat, stTimeSeriestp, tp) || (tp < 0)) {
            return false;
         }
         double delay;
         if (!aChn.getParam (*chndat, stTimeSeriesTimeDelay, delay)) {
            delay = 0.0;
         }
         int N0 = (int) (tp / dt + 0.5) - N_LEAD;
         if (N0 < 0) {
            return false;
         }
         double tstart = -delay - 0.00114;
         if (fPoints[id.measPoint].freq > 0) {
            tstart += fmod (fPoints[id.measPoint].phase / TWO_PI - 0.25 -
                           fPoints[id.measPoint].freq * N_LEAD * dt, 1) / 
               fPoints[id.measPoint].freq;
         }
      
         // sine detection
         cerr << "ssdet 8: f=" << fPoints[id.measPoint].freq << 
            " fs=" << 1 / dt << " N0=" << N0 << " N-N0=" << N-N0 <<
            " tS=" << tstart << " P=" <<
            180/PI*fPoints[id.measPoint].phase << " PtS=" <<
            360 * tstart * fPoints[id.measPoint].freq << "d" <<
            " delay=" << delay << endl;
      
         // if (sineAnalyze (1, (float*) chndat->value + N0, N - N0, 
                         // 1.0 / dt, fPoints[id.measPoint].freq, pAverages, 
                         // 0.0, (dCmplx*) sstmps[resultnum].coeff+1, 
                         // (dCmplx*) sstmps[resultnum].coeff+2) < 0) {
            // return false;
         // }
         // sstmps[resultnum].coeff[0] = fPoints[id.measPoint].freq;
         // cerr << "FREQ = " << sstmps[resultnum].coeff[0] << " coeff = " <<
            // sstmps[resultnum].coeff[1] << endl;
         cerr << "channel data # of elements = " << chndat->elNumber() << endl;	
         int err;
         if ((err=sineAnalyze (1, window, (float*) chndat->value + N0, N - N0, 
                              1.0 / dt, fPoints[id.measPoint].freq, pAverages, 
                              tstart, (dCmplx*) sstmps[resultnum].coeff+1, 
                              (dCmplx*) sstmps[resultnum].coeff+2)) < 0) {
            cerr << "SS ERROR = " << err << endl;
            return false;
         }
         sstmps[resultnum].coeff[0] = fPoints[id.measPoint].freq;
      
         cerr << "freq = " << sstmps[resultnum].coeff[0] << " coeff = " <<
            sstmps[resultnum].coeff[1] << endl;
         for (int k = 0; k < pAverages; k++) {
            cerr << sstmps[resultnum].coeff[k+2] << " ";
         }
         cerr << endl;
      }
   
      // cerr << "DO SSINE 2" << endl;
      return true;
   }


   bool sweptsine::transfn (int resultnum, int measnum, 
                     string chnname, bool stim, const callbackarg& id)
   {
      cerr << "trans " << chnname << " from " << measnum <<
         " into " << resultnum << endl;
   
      // only deal with A channels
      if (resultnum >= numA) {
         return true;
      }
   
      // get result access class
      // cerr << "DO SSINE 10" << endl;
      const diagResult* aRes = 
         diagResult::self (stObjectTypeTransferFunction);
      if (aRes == 0) {
         return false;
      }
   
      // get result objects for transfer function
      // cerr << "DO SSINE 11" << endl;
      string		resnameT = 
         diagObjectName::makeName (stResult, rindex + resultnum);
      gdsDataObject* 	resT = storage->findData (resnameT);
      // get result objects for coherence
      string		resnameC = 
         diagObjectName::makeName (stResult, rindex + numA + resultnum);
      gdsDataObject* 	resC = storage->findData (resnameC);
   
      int M = sstmpsSize;
      int N = fPoints.size();
      if (id.measPoint == 0) {
         // init analysis result first time around
         if (resT != 0) {
            storage->erase (resnameT);
         }
         if (resC != 0) {
            storage->erase (resnameC);
         }
         resT = aRes->newObject (0, N * (M + 1), 0, 
                              rindex + resultnum, -1, gds_complex32);
         resC = aRes->newObject (0, N * (M + 1), 0, 
                              rindex + numA + resultnum, -1,
                                doublePrecFreq ? gds_float64 : gds_float32);
         // cerr << "N = " << N << ", M = " << M << endl;
         if (resT != 0) {
            // set parameters of transfer function
            if(doublePrecFreq) {
              aRes->setParam(*resT, stTransferFunctionSubtype, 6);
            }
            else {
              aRes->setParam(*resT, stTransferFunctionSubtype, 3);
            }
            aRes->setParam (*resT, stTransferFunctionf0, (double) 0.0);
            aRes->setParam (*resT, stTransferFunctiondf, (double) 0.0);
            aRes->setParam (*resT, stTransferFunctiont0, id.ival.first);
            aRes->setParam (*resT, stTransferFunctionBW, 
                           1.0 / pAverages / (mTime + mTimeAdd));
            aRes->setParam (*resT, stTransferFunctionWindow, window);
            aRes->setParam (*resT, stTransferFunctionAverageType, 
                           averageType);
            aRes->setParam (*resT, stTransferFunctionAverages, pAverages);
            for (int b = 0; b < sstmpsSize; b++) {
               if (b == resultnum) {
                  aRes->setParam (*resT, stTransferFunctionChannelA, 
                                 sstmps[b].name);
               } 
               string bChn = diagObjectName::makeName 
                  (stSpectrumChannelB, b);
               aRes->setParam (*resT, bChn, sstmps[b].name);
            }
            aRes->setParam (*resT, stTransferFunctionN, N);
            aRes->setParam (*resT, stTransferFunctionM, M);
            storage->addData (*resT, false);
         }
         if (resC != 0) {
            // set parameters of coherence
            if(doublePrecFreq) {
              aRes->setParam(*resC, stTransferFunctionSubtype, 7);
            }
            else {
              aRes->setParam(*resC, stTransferFunctionSubtype, 5);
            }
            aRes->setParam (*resC, stTransferFunctionf0, (double) 0.0);
            aRes->setParam (*resC, stTransferFunctiondf, (double) 0.0);
            aRes->setParam (*resC, stTransferFunctiont0, id.ival.first);
            aRes->setParam (*resC, stTransferFunctionBW, 
                           1.0 / pAverages / (mTime + mTimeAdd));
            aRes->setParam (*resC, stTransferFunctionWindow, window);
            aRes->setParam (*resC, stTransferFunctionAverageType, 
                           averageType);
            aRes->setParam (*resC, stTransferFunctionAverages, pAverages);
            for (int b = 0; b < sstmpsSize; b++) {
               if (b == resultnum) {
                  aRes->setParam (*resC, stTransferFunctionChannelA,
                                 sstmps[b].name);
               } 
               string bChn = diagObjectName::makeName 
                  (stSpectrumChannelB, b);
               aRes->setParam (*resC, bChn, sstmps[b].name);
            }
            aRes->setParam (*resC, stTransferFunctionN, N);
            aRes->setParam (*resC, stTransferFunctionM, M);
            storage->addData (*resC, false);
         }
      }
      if ((resT == 0) || (resC == 0)) {
         return false;
      }
   
      // caluclate array index in result
      int ndx;
      if (sweepDir != 1) {
         ndx = id.measPoint;		// sweep upwards
      }
      else {
         ndx = N - 1 - id.measPoint;	// sweep downwards
      }
   
      // fill in transfer function coefficients
      cerr << "DO SSINE 12 " << M << endl;
      if(doublePrecFreq) {
        ((double *)resT->value)[ndx] = sstmps[resultnum].coeff[0].real();
      }
      else {
        ((complex<float>*) resT->value)[ndx] =
          complex<float>(sstmps[resultnum].coeff[0]);
      }
      for (int b = 0; b < M; b++) {
         complex<float>& x = 
            ((complex<float>*) resT->value)[(b + 1) * N + ndx];
         if (b != resultnum) {
            if (abs (sstmps[resultnum].coeff[1]) > 1E-99) {
               x = complex<float> 
                  (sstmps[b].coeff[1] / sstmps[resultnum].coeff[1]);
            }
            else {
               x = 1E99;
            }
            cerr << "freq = " << sstmps[resultnum].coeff[0] << " coeff = " <<
               sstmps[b].coeff[1] << "/" << sstmps[resultnum].coeff[1] <<
               " (" << ((complex<float>*) resT->value)[(b + 1) * N + ndx] << ")" << endl;
         }
         else {
            x = complex<float>(sstmps[resultnum].coeff[1]);
         }
      }
   
      // fill in coherence coefficients
      if(doublePrecFreq) {
        ((double *)resC->value)[ndx] = sstmps[resultnum].coeff[0].real();
        for (int b = 0; b < M; b++) {
          double& x =
              ((double*) resC->value)[(b + 1) * N + ndx];
          if (b != resultnum) {
            x = ssCoherence ((dCmplx*) sstmps[resultnum].coeff + 2,
                             (dCmplx*) sstmps[b].coeff + 2, pAverages);
          }
          else {
            x = 1.0;
          }
        }
      }
      else {
        ((float*) resC->value)[ndx] = sstmps[resultnum].coeff[0].real();
        for (int b = 0; b < M; b++) {
          float& x =
              ((float*) resC->value)[(b + 1) * N + ndx];
          if (b != resultnum) {
            x = ssCoherence ((dCmplx*) sstmps[resultnum].coeff + 2,
                             (dCmplx*) sstmps[b].coeff + 2, pAverages);
          }
          else {
            x = 1.0;
          }
        }
      }

   
      return true;
   }


   bool sweptsine::syncPause (const callbackarg& id, syncpointer& sync)
   {
      semlock		lockit (mux);
      bool 		retval ;

      if (my_debug)
	 cerr << "sweptsine::syncPause()" << endl ;
   
      // stop measurements
      int num = id. measPoint * averages + id.measPeriod;
      stopMeasurements (num);
      cerr << "sweptsine::syncPause() calling testExc->freeze()" << endl ;
      testExc->freeze();
   
      // add a synchronization point far in the future
      interval ival (id.ival.first + id.ival.second, 100 * _ONEDAY);
      if (!addSyncPoint (ival, id.measPeriod, id.measPoint)) {
         return false;
      }
   
      doAnalysis = false;
      retval = getNextSyncPoint (id, sync);
      if (my_debug) cerr << "sweptsine::syncPause() return " << (retval ? "TRUE" : "FALSE") << endl ;
      return retval ;
   }

   // This is a combination of syncPause followed by syncAbort. It was written
   // because the swept sine analysis didn't ramp down excitations when abort
   // was pressed like the other analysis do.  Experimentation found that if 
   // pause was pressed, the abort, the ramp down occurred so the stdtest::syncAbort()
   // was overwritten to do the functions of both.
   bool sweptsine::syncAbort (const callbackarg& id)
   {
      semlock           lockit (mux);

      if (my_debug) cerr << "sweptsine::syncAbort()" << endl ;

      // stop measurements
      int num = id. measPoint * averages + id.measPeriod;
      //stopMeasurements (num);
      cerr << "sweptsine::syncAbort() calling testExc->freeze() before stopMeasurements()" << endl ;
      testExc->freeze();
   
      // add a synchronization point far in the future
      interval ival (id.ival.first + id.ival.second, 100 * _ONEDAY);
      if (!addSyncPoint (ival, id.measPeriod, id.measPoint)) {
         return false;
      }
   
      doAnalysis = false;
      stopMeasurements(num) ;

      if (my_debug) cerr << "sweptsine::syncAbort() return TRUE" << endl ;
      return true ;
   }



   bool sweptsine::syncResume (const callbackarg& id, syncpointer& sync)
   {
      // stop excitation
      testExc->stop (-1, 2 * _EPOCH);
      // make sure there is no synchronization point left
      // make sure partition and excitation lists are empty
      ostringstream	errmsg;
      semlock		lockit (mux);
      if (!delMeasurements (errmsg)) {
         return false;
      }
   
      // determine start point & time
      int num = id.measPoint * averages + id.measPeriod;
      tainsec_t		t0 = TAInow() + 
         (tainsec_t) (testExc->dwellTime() * (double) _ONESEC) + 
         3 * _EPOCH;
   
      // add measurement descriptors
      sweeppoints::iterator iter = fPoints.begin();
      advance (iter, num);
      // loop over sweep points
      for (; iter != fPoints.end(); iter++, num++) {
         stimuli.front().freq = iter->freq;
         stimuli.front().ampl = iter->ampl;
         stimuli.front().offs = 0;
         stimuli.front().phas = 0;
         // add next sweep step
         if (!addMeasurements (errmsg, t0, num)) {
            return false;
         }
         // get phase at start of measurement
         cerr << "phi=" << 180/PI*stimuli.front().phas << 
            "  phiS=" << 360 * iter->freq * sTime <<
            "  phiP=" << 360 * iter->freq * pTime << endl;
         iter->phase = fmod (stimuli.front().phas + 
                            TWO_PI * iter->freq * (sTime + pTime), TWO_PI);
         if (!intervals.empty()) {
            // calculate start time of next sweep point
            t0 = intervals.back().t0() + intervals.back().dt();
         }
      }
   
      for (stimulus::awglist::iterator i = stimuli.front().signals.begin(); 
          i != stimuli.front().signals.end(); i++) {
         cerr << "E: start " << (double)(i->start % (100*_ONESEC))/1E9 << 
            " ramp=" << (double)i->ramptime[1]/1E9 << " d=" << (double)i->duration/1E9 << endl;
      }
   
      // start measurement
      if (!startMeasurements (errmsg)) {
         return false;
      }
   
      doAnalysis = true;
      return getNextSyncPoint (id, sync);
   }

}
