
/* This file provides two functions to help determine the correct address and port number of
 * an nds host.  
 *
 * getHostAddress() tries to resolve a hostname and port and returns 0 on success.
 * It reaturns the host ip in a string argument
 * 
 * getNDSHostPort() attempts to determine two hostname:port candidates for an nds host.
 * This is done by assuming that nds:8088 and nds1:8088 are valid hostname:port values for
 * the nds host, and modifying those names and port numbers based on the value of the 
 * NDSSERVER environment variable.  If present, the NDSSERVER environment variable value
 * would look like "hostname0:port0,hostname1:port1,...,hostname<n>:port<n>" where all
 * parts of the variable value are optional.  If hostname0:port0 is omitted, the ',' (comma)
 * must be present at the start of the value (",hostname1:port1").  If the hostname is
 * to be omitted but the port number isn't, then the ':' (colon) must be present 
 * (":port0,host1:port1").  If the port number is to be omitted, just skip the ":port" part
 * as in "hostname0,hostname1".  
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>  /* JCB - pick up declaration of inet_ntop() */
#include <unistd.h> /* JCB  - pick up declaration of close() */

static int	debug_level = 0 ;

/* get an ip address from a host name */
/* Return values: 0 on success
 * -1 No hostname provided
 *		  -2 invalid buffer
 *		  -3 not enough space in buffer
 *                -4 Can't resolve hostname to an IP.
 *                -5 Hostname resolved to an unsupported address type
 *                -6 Unknown inet_ntop() error, check errno
 */
int getHostAddress(char *hostname, char *host_ip, unsigned int host_len)
{
   struct sockaddr_in	server_addr ;
   int			optval ;	
   struct hostent	*host ;
   int			n, retval ;
   char			ipstr[INET_ADDRSTRLEN] ;
   int			sockfd ;

   if(!host_ip)
   {
       if(debug_level) fprintf(stderr, "getHostAddress(): invalid buffer\n");
       return -2;
   }

   /* If no hostname was passed, return -1 */
   if (!hostname)
   {
      if (debug_level)
	 fprintf(stderr, "getHostAddress(): No host name!\n") ;
      return (-1) ;
   }

   /* Get the host IP */
   printf("getting host by name: %s\n", hostname);
   host = gethostbyname(hostname) ;
   if (!host)
   {
       printf("did not find host\n");
      if (debug_level)
	 fprintf(stderr, "getHostAddress(): Can't find hostname %s\n", hostname) ;
      return (-4) ;
   }
   printf("found host\n");
   snprintf(host_ip, host_len, "%d.%d.%d.%d",
            (unsigned char)host->h_addr_list[0][0],
            (unsigned char)host->h_addr_list[0][1],
            (unsigned char)host->h_addr_list[0][2],
            (unsigned char)host->h_addr_list[0][3]);
   host_ip[host_len] = 0;

   if (debug_level)
   {
      fprintf(stderr, "Resolved %s at %s\n", hostname,  ipstr)  ;
      fflush(stderr) ;
   }
   return 0;

}

/* Parse through the NDSSERVER environment variable if it exists.
 * Ideally, the environment variable contains a comma-separated list
 * of hostname:port values on which nds services might be found. As
 * an example:
 * NDSSERVER=h2nds0:8088,h2nds1:8088,ldas-pcdev1.ligo.caltech.edu:31200
 * 
 * By default, this function will copy "nds" to hostname0, "nds1" to 
 * hostname1, and 8088 to port0, port1.  If the NDSSERVER environment
 * variable is present, it can specifiy alternate values for the nds
 * hostnames and ports.  Using the above example, "h2nds0" will be copied
 * to hostname0, "h2nds1" will be copied to hostname1, and the value 8088
 * will be written to port0 and port1.
 * 
 * For the NDSSERVER value, all parts (hostname0, port0, hostname1, port1)
 * are optional, if not present the default values will be used.
 * A value of "h2nds0,h2nds1" will overwrite the default hostname values
 * while leaving the port numbers unchanged.
 * A value of ":8087" would overwrite port0 with 8087 while leaving the
 * other values unchanged.
 * A value of "h2nds0,:8087" would overwrite hostname0 with h2nds0 and
 * port1 with 8087, leaving hostname1 and port0 as defaults.
 * A value of ",,ldas-pcdev1.caltech.edu:31200" would leave hostname0, port0
 * hostname1, and port1 as their default values.
 * 
 * A limit of 255 characters may be specified for hostnames, and 5 characters 
 * for port numbers (since a port number has a max value of 65535).
 * The return value in the range 0 to 4 is successful.  Anything else,
 * some error occurred.
 */

int parseNDSSERVERval(char **str, char *hostname0, int *port0, char *hostname1, int *port1) ;

int
getNDSHostPort(char *hostname0, int *port0, char *hostname1, int *port1)
{
   char	*env_val ;	/* Value of the NDSSERVER environment variable. */

   /* Pointers to buffers should be non-null. */
   if (!hostname0 || !port0 || !hostname1 || !port1)
      return -1 ;

   /* See if the NDSSERVER environment variable exists */
   env_val = getenv("NDSSERVER") ;
   if (debug_level)
      fprintf(stderr, "getenv() returned %s\n", (env_val ? env_val : "NULL")) ;
   if (env_val)
   {
      /* Parse the text returned. */
      return parseNDSSERVERval(&env_val, hostname0, port0, hostname1, port1) ;
   }
   else
   {
      strcpy(hostname0, "nds") ;
      strcpy(hostname1, "nds1") ;
      *port0 = *port1 = 8088 ;
   }
   return 0;
}

int
parseNDSSERVERval(char **str, char *hostname0, int *port0, char *hostname1, int *port1)
{
   char		*env_val ; 		/* Value of the NDSSERVER environment variable */
   char		*cptr1, *cptr2 ;
   int		state = 0 ;

   /* Pointers to buffers should be non-null. */
   if (!hostname0 || !port0 || !hostname1 || !port1)
      return -1 ;

   /* Default values for the nds hostname and port are:
    * nds:8088
    * nds0:8088
    */
   strcpy(hostname0, "nds") ;
   strcpy(hostname1, "nds1") ;
   *port0 = *port1 = 8088 ;

   if (str && (env_val = *str))
   {
      /* Parse the text returned.  We are looking for 
       * "hostname0:port0,hostname1:port1" where each part
       * or the whole thing is optional.  There may be other 
       * stuff in the string which we don't care about.
       */
      /* State 0 - looking for the hostname0 part
       * state 1 - looking for the port0 part
       * state 2 - looking for the hostname1 part
       * state 3 - looking for the port1 part
       * state 4 - done.
       * state 5 - error, ':' follows ':'.
       * state 6 - error, port number too large.
       */
      int state = 0 ;
      cptr1 = cptr2 = env_val ;
      while (*cptr2 && state < 4)
      {
	 switch (*cptr2) 
	 {
	    case ':' : switch (state)
			{
			case 0 :
			   /* cptr1 is the beginning, cptr2 is the
			    * end of the first hostname. If the pointer
			    * are different, there's a string that needs
			    * to overwrite the value in hostname0.
			    */
			   if (cptr1 != cptr2)
			   {
			      /* Limit the host name to something less than 255 chars. */
			      if (cptr2-cptr1 < 255)
			      {
				 strncpy(hostname0, cptr1, cptr2-cptr1) ;
				 hostname0[cptr2-cptr1] = '\0' ;
			      }
			   }
			   state = 1 ;
			   cptr1 = cptr2+1 ; /* At most, *cptr1 is '\0' */
			   break ;
			case 1 :
			   /* This is an error, a ':' can't follow
			    * another ':'.
			    */
			   state = 5 ;
			   break ;
			case 2 :
			   /* cptr1 is at the start of hostname1, cptr2
			    * is at the end of hostname1.  If the 
			    * pointers are different, there's a string
			    * that needs to overwrite the value in 
			    * hostname1.
			    */
			   if (cptr1 != cptr2)
			   {
			      /* Limit the host name to something less than 255 chars. */
			      if (cptr2-cptr1 < 255)
			      {
				 strncpy(hostname1, cptr1, cptr2-cptr1) ;
				 hostname1[cptr2-cptr1] = '\0' ;
			      }
			   }
			   state = 3 ;
			   cptr1 = cptr2+1 ;
			   break ;
			case 3:
			   /* This is an error, ':' can't follow ':'. */
			   state = 5 ;
			   break ;
			}
		     break ;
	    case ',' : switch (state)
			{
			   case 0 :
			      /* cptr1 is at the beginning, cptr2 is the
			       * end of the first hostname and there's no
			       * port0 number.  If the pointers are different
			       * there's a string that needs to overwrite
			       * the value in hostname0 .
			       */
			      if (cptr1 != cptr2)
			      {
				 /* Limit the host name to something less than 255 chars. */
				 if (cptr2-cptr1 < 255)
				 {
				    strncpy(hostname0, cptr1, cptr2-cptr1) ;
				    hostname0[cptr2-cptr1] = '\0' ;
				 }
			      }
			      state = 2 ; /* Next, look for hostname1. */
			      cptr1 = cptr2+1 ; /* At most, *cptr1 is '\0' */
			      break ;
			   case 1 :
			      /* cptr1 is at the beginning of the port0,
			       * cptr2 is at the end.  If the pointers are
			       * different, there should be a port number
			       * that needs to overwrite the value in port0.
			       */
			      if (cptr1 != cptr2)
			      {
				 char tmp[6] ; /* max port # is 65535 */
				 if (cptr2-cptr1 < 6)
				 {
				    strncpy(tmp, cptr1, cptr2-cptr1) ;
				    tmp[cptr2-cptr1] = '\0' ;
				    *port0 = atoi(tmp) ;
				    if (*port0 > 65535)
				       *port0 = 8088 ;
				 }
				 else
				 {
				    /* error, number is too large. */
				    state = 6 ;
				    break ;
				 }
			      }
			      state = 2 ;
			      cptr1 = cptr2+1 ;
			      break ;
			   case 2 :
			      /* cptr1 is at the beginning of the hostname1
			       * and cptr2 is at the end.  If the pointers are
			       * different, there's a string that needs to overwrite
			       * the value in hostname1.  There's no port1 following.
			       */
			      if (cptr1 != cptr2)
			      {
				 /* Limit the host name to something less than 255 chars. */
				 if (cptr2-cptr1 < 255)
				 {
				    strncpy(hostname1, cptr1, cptr2-cptr1) ;
				    hostname1[cptr2-cptr1] = '\0' ;
				 }
			      }
			      state = 4 ;
			      break ;
			   case 3 :
			      /* cptr1 is at the beginning of the port1
			       * and cptr2 is at the end.  If the pointers are different,
			       * there's a string that needs to overwrite the value
			       * of port1.
			       */
			      if (cptr1 != cptr2)
			      {
				 char tmp[6] ; /* max port # is 65535 */
				 if (cptr2-cptr1 < 6)
				 {
				    strncpy(tmp, cptr1, cptr2-cptr1) ;
				    tmp[cptr2-cptr1] = '\0' ;
				    *port1 = atoi(tmp) ;
				    if (*port1 > 65535)
				       *port1 = 8088 ;
				 }
				 else
				 {
				    /* Error, number is too large */
				    state = 6 ;
				    break ;
				 }
			      }
			      state = 4 ; /* done. */
			      break ;
			}
		     break ;
	    default :
		  /* It's some other character, do nothing and let the
		   * cptr2 get incremented. 
		   */
		  break ;

	 } /* switch (*cptr2) */
	 cptr2++ ;
      } /* while */

      /* Either we're out of characters in the environment variable value or
       * we found all the parts that were there.  If we ran out of characters,
       * finish off the last part.
       */
      switch (state)
      {
	 case 0 :
	    /* cptr1 is at the beginning of hostname0
	     * and cptr2 is at the end.  If the pointers are different,
	     * there's a string that needs to overwrite hostname0.
	     */
	    if (cptr1 != cptr2)
	    {
	       /* Limit the host name to something less than 255 chars. */
	       if (cptr2-cptr1 < 255)
	       {
		  strncpy(hostname0, cptr1, cptr2-cptr1) ;
		  hostname0[cptr2-cptr1] = '\0' ;
	       }
	    }
	    state = 4 ;
	    break ;
	 case 1 :
	    /* cptr1 is at the beginnint of port0 and cptr2 is
	     * at the end.  If the pointers are different,
	     * there's a string that needs to overwrite the value of
	     * port 0 ;
	     */
	    if (cptr1 != cptr2)
	    {
	       char tmp[6] ; /* max port # is 65535 */
	       if (cptr2-cptr1 < 6)
	       {
		  strncpy(tmp, cptr1, cptr2-cptr1) ;
		  tmp[cptr2-cptr1] = '\0' ;
		  *port0 = atoi(tmp) ;
		  if (*port0 > 65535)
		     *port0 = 8088 ;
	       }
	       else
	       {
		  /* Error, number is too large. */
		  state = 6 ;
		  break ;
	       }
	    }
	    state = 4 ;
	    break ;
	 case 2 :
	    /* cptr1 is at the beginning of hostname1
	     * and cptr2 is at the end.  If the pointers are 
	     * different, there's a string that needs to
	     * overwrite hostname1.
	     */
	    if (cptr1 != cptr2)
	    {
	       /* Limit the host name to something less than 255 chars. */
	       if (cptr2-cptr1 < 255)
	       {
		  strncpy(hostname1, cptr1, cptr2-cptr1) ;
		  hostname1[cptr2-cptr1] = '\0' ;
	       }
	    }
	    state = 4 ;
	    break ;
	 case 3 :
	    /* cptr1 is at the beginning of port1 and cptr2
	     * is at the end.  If the pointers are different
	     * there's a value that needs to overwrite port1.
	     */
	    if (cptr1 != cptr2)
	    {
	       char tmp[6] ; /* max port nuber is 6 */
	       if (cptr2-cptr1 < 6)
	       {
		  strncpy(tmp, cptr1, cptr2-cptr1) ;
		  tmp[cptr2-cptr1] = '\0' ;
		  *port1 = atoi(tmp) ;
		  if (*port1 > 65535)
		     *port1 = 8088 ;
	       }
	       else
	       {
		  /* Error, number too large. */
		  state = 6 ;
		  break ;
	       }
	    }
	    state = 4 ;
	    break ;
	 case 4 :
	    /* All the parts were found, cptr2 is at the character
	     * just after "host0:port0,host1:port1", which will either
	     * be a character or '\0'.  Point to the remainder of
	     * the string. 
	     */
	    *str = cptr2 ;
	    break ;
	 case 5 :
	 case 6 :
	    /* An error occurred, attempt to find a comma in the
	     * remainder of the string, which would indicate there
	     * might still be something useful in the value.
	     */
	    while (*cptr2 && *cptr2 != ',')
	       cptr2++ ;
	    break ;
      }
      *str = cptr2 ; /* Point to the remainder of the string. */
   }
   return (state) ;
}

