/* version $Id: FilterSection.cc 7005 2014-02-14 23:44:05Z james.batch@LIGO.ORG $ */
#include "FilterSection.hh"
#include "iirutil.hh"


   using namespace std;
   using namespace ligogui;

namespace filterwiz {


   /// Maximum line length for split command
   const int kLineLength = 100;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// FilterSection						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   FilterSection::FilterSection ()
   : fIndex (0), fFilter (1.0), fInpSwitch (kZeroHistory), 
   fOutSwitch (kImmediately), fRamp(0), fTolerance (0), fTimeout (0), fGainOnly(0)
   {
   }

//______________________________________________________________________________
   FilterSection::FilterSection (double fsample, int index)
   : fIndex (index), fFilter (fsample), fInpSwitch (kZeroHistory), 
   fOutSwitch (kImmediately), fRamp(0), fTolerance (0), fTimeout (0), fGainOnly(0)
   
   {
   }

//______________________________________________________________________________
   void FilterSection::setDesign (const char* p, bool splitcmd, int maxline) 
   {
      if (splitcmd) {
         fDesign = splitCmd (p, maxline); 
      }
      else {
         fDesign = p; 
      }
   }

//______________________________________________________________________________
   bool FilterSection::empty() const
   {
      return fFilter.isUnityGain();
   }

//______________________________________________________________________________
   bool FilterSection::designEmpty() const
   {
      // "Empty" means either there's nothing in the string, or there's
      // nothing but whitespace in the string.  Foton seems to add at least
      // one space to the design, even if nothing was ever added by the user.
      if (!fDesign.empty())
      {
	 return (fDesign.find_first_not_of(" \t\n") == string::npos) ;
      }
      else
      	return true ;
   }

//______________________________________________________________________________
   bool FilterSection::check() const
   {
      FilterDesign d (fFilter.getFSample());
      if (!d.filter (fDesign.c_str())) {
         return false;
      }
      return iircmp (d.get(), fFilter.get());
   }

//______________________________________________________________________________
   bool FilterSection::valid() const
   {
      FilterDesign d (fFilter.getFSample());
      return d.filter (fDesign.c_str());
   }

//______________________________________________________________________________
   bool FilterSection::update()
   {
      fFilter.reset();
      return fFilter.filter (fDesign.c_str());
   }

//______________________________________________________________________________
   bool FilterSection::add (const char* cmd)
   {
      if (!cmd || !fFilter.filter (cmd)) {
         return false;
      }
      fDesign += cmd;
      return true;
   }

//______________________________________________________________________________
   string FilterSection::splitCmd (const char* cmd, int maxlen)
   {
      if (maxlen <= 0) maxlen = kLineLength;
      string s;
      string w = cmd;
      while ((int)w.size() > maxlen) {
         bool split = false;
         // try splitting at \n
         for (int i = maxlen + 1; i >= 0; --i) {
            if (i >= (int)w.size()) {
               continue;
            }
            if (w[i] == '\n') {
               if ((i + 1 < (int)w.size()) && (w[i+1] == '*')) ++i;
               s += w.substr (0, i + 1);
               w.erase (0, i + 1);
               split = true;
               break;
            }
         }
         // try splitting at )
         if (!split) {
            for (int i = maxlen; i > 20; --i) {
               if (w[i] == ')') {
                  if ((i + 1 < (int)w.size()) && (w[i+1] == '*')) ++i;
                  s += w.substr (0, i + 1) + "\n";
                  w.erase (0, i + 1);
                  split = true;
                  break;
               }
            }
         }
         // try splitting at ,
         if (!split) {
            for (int i = maxlen; i > 40; --i) {
               if (w[i] == ',') {
                  s += w.substr (0, i + 1) + "\n";
                  w.erase (0, i + 1);
                  w.insert (0, "    ");
                  split = true;
                  break;
               }
            }
         }
         // try splitting at ;
         if (!split) {
            for (int i = maxlen; i > 60; --i) {
               if (w[i] == ';') {
                  s += w.substr (0, i + 1) + "\n";
                  w.erase (0, i + 1);
                  w.insert (0, "    ");
                  split = true;
                  break;
               }
            }
         }
         // just split it
         if (!split) {
            s += w.substr (0, maxlen) + "\n";
            w.erase (0, maxlen);
            w.insert (0, "    ");
         }
      }
      s += w;
      return s;
   }


}
