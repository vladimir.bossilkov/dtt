/* -*- mode: c++; c-basic-offset: 3; -*- */
/* version $Id: TLGFilterWizard.cc 8024 2018-04-18 18:04:26Z ed.maros@LIGO.ORG $ */
#include <algorithm>
#include <cstring>
#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>
#include <TGFSComboBox.h>
#include <TGListView.h>
#include <TGTextView.h>
#include <TGTextEdit.h>
#include <TGFSContainer.h>
#include <TGMsgBox.h>
#include <TSystem.h>
#include <cerrno>
#include "TLGFilterWizard.hh"
#include "TLGFilterDlg.hh"
#include "FilterFile.hh"
#include "PlotSet.hh"
#include "TLGEntry.hh"
#include "TLGTextEditor.hh"
#include "TLGComboEditBox.hh"
#include "TLGPad.hh"
#include "TLGErrorDlg.hh"
#include "iirutil.hh"
#include "constant.hh"
#include "BugReportDlg.h"

   using namespace std;
   using namespace ligogui;

   static const int my_debug = 0 ;

//______________________________________________________________________________
extern "C" {
   // These function is needed by the FilterDesign class
   // These function is needed by DTT to edit a filter string
   bool wizard___dynamic (const std::string name, std::string& filter);
}

namespace filterwiz {


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Constants							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   const char* const aboutmsg = 
   "LIGO))) Laser Interferometer Gravitational-wave Observatory\n"
   "Foton - Filter Online Tool\n\n"
   "by Daniel Sigg et al., 2002-2019\n"
   "version " CDS_VERSION "\n\n"
   "http://www.ligo.caltech.edu\n"
   "http://git.ligo.org/cds/dtt";

   static const char* const gSaveAsTypes[] = { 
   "Filter files", "*.txt",
   "All files", "*",
   0, 0 };

   const char *foton_changes[] = {
   "Change history of all cds-crtools releases can also be found at",
   "https://git.ligo.org/cds/dtt/wikis/ChangeHistory",
   "",
   "Version 3.1.1 April 2022",
   "- Fixed: when step response was selected, a time offset could be added to reference traces.  This has been fixed.",
   " ",
   "Version 3.0.0 July 2021",
   "- foton python bindings improved and modernized. These changes are only for Python3. An old version of the bindings will still be installed for python2, but will no longer be worked on. Please upgrade your scripts to python3!",
   "- filter file API now packaged as library 'libfilterfile'",
   "",
   "Version CDS 0.4.0 March 2021",
   "- Fixed: ROOT no longer erroneously consumes some command line arguments passed to foton.py.",
   "- Foton has been changed to accept any positive sample rate.",
   "- Runs using GDS 2.19.4 and ROOT 6.22/06",
   "",
   "Version CDS 0.3.0 August 2020",
   "- Project location changed to git.ligo.org/cds/dtt",
   "",
   "Version CDS 0.2.0 January 2020",
   "",
   "- FIXED: A bug that would sometimes create double second order sections",
   "when opening the \"SOS\" window with filter design components selected.",
   "Issue #2",
   "",
   "",
   "Version 2.17.9-3 (branch), trunk 2.17.14 March 2017",
   "",
   "* Change the format of the gain coeffient of filter files to write",
   "  in scientific notation with sufficient precision for very large",
   "  and very small numbers",
   "",
   "Version 2.17.9-2 (branch), February 2017",
   "",
   "* Bugzilla 1075, change the format of the gain coefficient of filter",
   "  files to include more decimal places of precision.  This should ",
   "  eliminate false reports of mismatches between design strings and ",
   "  coefficients.",
   "",
   "* Bugzilla 975",
   "* Bugzilla 855, add ability to create a new file.  A new option has",
   "  been added to foton (-x) to allow an \"experiment mode\" which will",
   "  allow a user to create a new filter file, define a sample rate, and ",
   "  add new modules to the file.  Filters may be added to the modules",
   "  with the ability to do bode plots, step response, and s-plane roots.",
   "",
   "Version 2.17.9-1 (branch), December 2016",
   "",
   "* Add a command line option to change the data rate of a filter",
   "  file.  This would alter all the filter coefficients based on the",
   "  new sample rate and design strings in the filter file.  This would",
   "  never be used unless a model's sample rate was being changed.",
   "  See T1600007 for details on usage.",
   "",
   "Version 2.17.9 (trunk), July 2016",
   "",
   "* Bugzilla 1017, remove extra whitespace and blank lines from",
   "  design strings for filters when the file is written.",
   "",
   "Version 2.17.2 June 2015",
   "",
   "* Bugzilla 827, correct allowed sample rates for foton filters",
   "",
   "* Bugzilla 826, allow any sample rate to be used by adding a '-a' option",
   "  to foton invocation.  The desired filter rate must be in the filter file",
   "  before it is read by foton",
   "",
   "* Fix Bugzilla 778, make filter module lists iterable in Python.",
   "",
   "* Bugzilla 761, graph line width control changed to integer values",
   "  greater than 0.",
   "",
   "* Bugzilla 760, Foton now warns the user if a filter design with too",
   "  many poles/zeros is pasted into the design window, either with a",
   "  warning popup, or a warning when the file is written.",
   "",
   "Version branch/2.16.17.1 January 2015",
   "",
   "* Replace changes file with embedded text to avoid issues of where",
   "  the changes text file should be placed.",
   "",
   "Version 2.16.16 November 2014",
   "",
   "* Bugzilla 754, graph legend background is red by default using",
   "  root version 5.34.21",
   "",
   "* Bugzilla 755, file filter for PNG files is incorrect in print",
   "  Save As dialog box.",
   "",
   "Version 2.16.15 October 2014",
   "",
   "* Fix Bugzilla 687, add copy/paste to Calibration dialog in graphics window.",
   "",
   "Version 2.16.12.2, July 2014",
   "",
   "* Implement patch listed in Bugzilla 659 to add foton scripting capability",
   "  using python.",
   "",
   "* Fix Bugzilla 685, add a '-o <outfile>' option to the foton command line",
   "  to be used with the -c option, allowing the file written by foton to have",
   "  a different name than the input file.",
   "",
   "* Fix Bugzilla 684, to get foton to use the correct path when the -c option",
   "  is used and the '-p <directory path>' option is used.",
   "",
   "* Remove leading spaces from the design string when a filter file is saved.",
   "  ",
   "Version 2.16.12.1, May 2014",
   "",
   "* Bugzilla 655 -Remove legacy write option and menu item, option is no ",
   "  longer needed.",
   "",
   "* Fix Bugzilla 656, foton sometimes crashes when invoked from diaggui.",
   "",
   "Version 2.16.9.1, February 2014",
   "",
   "* Change format of floating point numbers in design strings from %g ",
   "  to %.16g which prints more significant digits.  Affects cases where",
   "  foton creates a design string from a set of SOS coefficients.",
   "  Increasing the number of significant digits in the design string allows",
   "  foton to regenerate the SOS coefficients with more accuracy.",
   "",
   "Version 2.16.9, January 2014",
   "",
   "* Add a non-interactive correction mode to foton.  This is invoked with",
   "  a -c option, also specifying a filter file.  Foton will read the filter",
   "  file, then write it as if the user had saved the file in the interactive",
   "  mode.  The effect is to create design strings for filters that don't ",
   "  have them, and to strip out filters not associated with the module list",
   "  at the beginning of the file.  This option can be somewhat destructive ",
   "  if not used with care.",
   "",
   "Version 2.16.8, November 2013",
   "",
   "* Change warning message on file read when no design string is present for",
   "  filter module.  New message is 'Missing design string, a new string will",
   "  be generated.'  (bugzilla #511)",
   "",
   "* Add option to allow selection of filter module and section on invocation.",
   "  The sytax is 'foton -s <module>:<section> <filter file>', where module",
   "  is the name of a module in the filter file, and section is in the range",
   "  1 through 10 for the filter section.  The section may be omitted, in ",
   "  which case the module is selected. (bugzilla #497) ",
   "",
   "* Updates to compile with gcc version 4.7.x",
   "",
   "Version 2.16.5, October 2012",
   "",
   "* Change matlab merge batch mode functionality to skip displaying any ROOT ",
   "  graphics.  Foton will return 0 for successful batch merge operations, or ",
   "  -1 (255) for failure.  Errors reading the filter file and merge file will ",
   "  now be written to stdout if any are encountered.",
   "",
   "* Errors found on reading a filter file are reported to the user in a popup ",
   "  dialog box when the file is read.  Although foton still discards filters ",
   "  it doesn't understand at least you have fair warning.  The filter 'File:'",
   "  label has been changed to a button which turns red if the file has errors.",
   "  This button may be clicked to display file errors.",
   "",
   "* Added the ability to read release notes from the Help menu in foton.",
   "",
   "* If a filter has an output ramp set with 0 ramp time, foton will issue a ",
   "  warning and change the output switching to immediate.",
   "  ",
   "Version 2.16.3, October 2011",
   "",
   "* Filter files which are symbolic links will now preserve the symbolic link ",
   "  by following the links until the original file (if any) is found. This ",
   "  requires that the directory in which the actual file resides be writable by ",
   "  the user, as a temporary file is still generated when writing.",
   "",
   "Version 2.15.2, July 2011",
   "",
   "* Foton files can be saved even if no changes have been made.",
   "",
   "* Popup dialog windows now appear within the frame of foton instead of some ",
   "  random spot on the screen.",
   "",
   "* Ported to Mac OS X (10.6.5).  ",
   "",
   "Version 2.15.0, May 2011",
   "",
   "* Filter modules are now sorted before creating the module selection menu.",
   "",
   "* Add matlab merge file capability to foton, documented in the foton man page.",
   "",
   "* Fixed bugzilla #324, making sure that plot option channel A and B list boxes ",
   "  have a minimum size to make them useable.  ",
   "",
   (char *) NULL
   } ;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Widget ID numbers						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   const int kPathId = 100;
   const int kFileId = 101;
   const int kModuleId = 102;
   const int kDirUpId = 103;
   const int kSectMulId = 120;
   const int kSectSelAllId = 121;
   const int kSectSelNoneId = 122;
   const int kSectId = 123;     // 123-132
   const int kSectNameId = 133; // 133-142
   const int kSwitchInpId = 150;
   const int kSwitchOutId = 151;
   const int kSwitchParamId = 152; //152-154
   const int kPlotParamId = 160; // 160-163
   const int kPlotTypeId = 164;
   const int kSampleId = 170;
   const int kCommandId = 171;
   const int kAltId = 172;
   const int kSZFormatId = 173;
   const int kSetGainFId = 190;
   const int kSetGainValId = 191;
   const int kSetGainUnitId = 192;
   const int kSetGainSignId = 193;
   const int kSetGainId = 194;
   const int kSetGainCurrentId = 195;
   const int kSetGainCurrentUnitId = 196;
   const int kAddId = 200;
   const int kAddGainId = 200;
   const int kAddZpkId = 201;
   const int kAddPolyId = 202;
   const int kAddResGainId = 203;
   const int kAddNotchId = 204;
   const int kAddEllipId = 205;
   const int kAddButterId = 206;
   const int kAddCheby1Id = 207;
   const int kAddCheby2Id = 208;
   const int kAddCombId = 209;
   const int kAddSosId = 210;
   const int kAddZRootId = 211;
   const int kAddDirectId = 212;
   const int kSelCopyId = 900 ;         // Copy button - JCB
   const int kSelPasteId = 901 ;        // Paste button, kSelCopyId+1. - JCB
   const int kSelCutId = 902 ;          // Cut button, kSelCopyId+2. - JCB
   const int kSelRevertId = 903 ;       // Undo Paste button, kSelCopyId+3. - JCB
   const int kImportId = 904 ;          // Import button in design section - JCB
   const int kFileError = 905 ;         // File: button to show errors on reading - JCB


   const int kB_BODEPLOT = 15;
   const int kB_STEPPLOT = 16;
   const int kB_ROOTPLOT = 17;
   const int kB_WRITEFILTER = 18;
   const int kB_WRITELOAD = 19;
   const int kB_OK = 20;
   const int kB_CANCEL = 21;


   // The menu values are also defined in gui/dttgui/TLGMainMenu.hh, make sure
   // that any additions don't interfere with those.
   const int kM_FILE_SAVELOAD = 130;
   const int kM_FILE_READONLY = 131;
   const int kM_FILE_LEGACY = 905;	// Legacy menu item in File menu - JCB
   const int kM_FILE_MERGE = 906;       // Merge matlab design item in File menu - JCB
   const int kM_FILE_GAIN1 = 907; 	// Allow gain(1) filters item in File menu. - JCB
   const int kM_FILE_OK = 132;
   const int kM_FILE_CANCEL = 133;
   const int kM_PLOT_BODE = 410;
   const int kM_PLOT_STEP = 411;
   const int kM_PLOT_IMPULSE = 412;
   const int kM_PLOT_RAMP = 413;
   const int kM_PLOT_SROOTS = 414;
   const int kM_PLOT_ZROOTS = 415;
   const int kM_PLOT_CLOSEDLOOP = 416;
   const int kM_PLOT_NEGCONV = 417;
   const int kM_HELP_NOTES = 902;	// Display release notes from Help menu - JCB
   const int kM_HELP_BUG = 903;  // Show a bug report window



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Layout grids							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Int_t kModSelGrid[] = {
   440, 250,             // total width & height
   15, 25, 70, 24,       // Path label
//   15, 185, 70, 24,      // File label
   10, 185, 70, 24,      // File label
   15, 215, 70, 24,      // Module label
   85, 25, 300, 24,      // Path 
   395, 25, 24, 24,      // Directory up button
   85, 55, 335, 121,     // Directory list
   85, 185, 335, 24,     // File 
   85, 215, 335, 24,     // Module 
   0};

   Int_t kSecSelGrid[] = {
   270, 250,             // total width & height
   15, 25, 70, 24,       // select label
   85, 25, 100, 24,      // multiple section selection
   200, 25, 24, 24,      // select all button
   230, 25, 24, 24,      // select none button
   15, 65, 20, 24,       // 1st section
   35, 65, 90, 24,       // 1st section name
   15, 95, 20, 24,       // 2nd section
   35, 95, 90, 24,       // 2nd section name
   15, 125, 20, 24,      // 3rd section
   35, 125, 90, 24,      // 3rd section name
   15, 155, 20, 24,      // 4th section
   35, 155, 90, 24,      // 4th section name
   15, 185, 20, 24,      // 5th section
   35, 185, 90, 24,      // 5th section name
   145, 65, 20, 24,      // 6th section
   165, 65, 90, 24,      // 6th section name
   145, 95, 20, 24,      // 7th section
   165, 95, 90, 24,      // 7th section name
   145, 125, 20, 24,     // 8th section
   165, 125, 90, 24,     // 8th section name
   145, 155, 20, 24,     // 9th section
   165, 155, 90, 24,     // 9th section name
   145, 185, 20, 24,     // 10th section
   165, 185, 90, 24,     // 10th section name
   15,  215, 60, 24,     // Copy button  - JCB
   75, 215, 60, 24,      // Paste button - JCB
   135, 215, 60, 24,     // Cut button - JCB
   195, 215, 60, 24,     // Undo button - JCB
   0};

   Int_t kSwitchGrid[] = {
   220, 250,             // total width & height
   15,   25,  70, 24,    // input label
   15,   55,  70, 24,    // output label
   15,   95,  70, 24,    // ramp time label
   15,  125,  70, 24,    // tolerance label
   15,  155,  70, 24,    // timeout label
   180,  95,  25, 24,    // unit label
   180, 155,  25, 24,    // unit label
   85,   25, 120, 24,    // input switching
   85,   55, 120, 24,    // output switching
   85,   95,  90, 24,    // ramp time
   85,  125,  90, 24,    // tolerance
   85,  155,  90, 24,    // timeout   
   0};

   Int_t kDesignGrid[] = {
   946, 310,             // total width & height
   15,   25,  70, 24,    // fsample label
   180,  25,  30, 24,    // unit label
   15,   55,  70, 24,    // command label
   15,  145,  70, 24,    // alternate label
   85,  205,  70, 24,    // s-plane label
   540, 205,  70, 24,    // z-plane label
   15,  235,  70, 24,    // add label
   15,  275,  70, 24,    // set gain label
   180, 275,  30, 24,    // unit label
   220, 275,  50, 24,    // value label
   625, 275,  60, 24,    // curren value label
   85,   25,  90, 24,    // sample rate
   85,   55, 845, 84,    // command
   85,  145, 845, 54,    // alternate
   150, 205,  70, 24,    // s-plane format: s
   235, 205,  70, 24,    // s-plane format: f
   305, 205,  85, 24,    // s-plane format: n
   405, 205,  80, 24,    // s-plane format: p
   605, 205,  60, 24,    // z-plane format: SOS
   675, 205,  60, 24,    // z-plane format: roots
   745, 205,  60, 24,    // z-plane format: direct
   820, 205,  79, 24,    // z-plane format: online
   85,  275,  90, 24,    // set gain f
   265, 275,  90, 24,    // set gain value
   365, 275,  50, 24,    // set gain unit
   425, 275,  25, 24,    // set gain sign
   470, 275,  77, 24,    // set gain
   680, 275, 165, 24,    // set gain current value
   853, 275,  76, 24,    // set gain current value unit
   85,  235,  65, 24,    // gain
   150, 235,  65, 24,    // ZPK
   215, 235,  65, 24,    // poly
   280, 235,  65, 24,    // resgain
   345, 235,  65, 24,    // notch
   410, 235,  65, 24,    // elliptic
   475, 235,  65, 24,    // butter
   540, 235,  65, 24,    // cheby1
   605, 235,  65, 24,    // cheby2
   670, 235,  65, 24,    // comb
   735, 235,  65, 24,    // sos
   800, 235,  65, 24,    // zroots
   865, 235,  65, 24,    // direct
   (85+845)-77, 25, 77, 24,     // Import... Place above command, on the right side. - JCB
   930, 235,  60, 24,    // test   (JCB - This isn't present.)
   0};

   Int_t kPlotGrid[] = {
   946, 60,              // total width & height
   15,  25, 40, 24,      // start label
   175, 25, 40, 24,      // stop label
   335, 25, 100,24,      // points label
   565, 25, 40, 24,      // type label
   780, 25, 50, 24,      // duration label
   55,  25, 90, 24,      // start frequency
   215, 25, 90, 24,      // stop frequency
   445, 25, 90, 24,      // number of points
   605, 25, 110,24,      // plot type
   840, 25, 90, 24,      // time duration
   0};



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Utility function			                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   inline static double power (double x, double y) 
   {
      return exp (log (x) * y);
   }

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Determine if foton release notes are present so they can be          //
// displayed.                                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   int fotonChangesPresent(string filePath)
   {
      struct stat 	fileStat ;
      return (!filePath.empty() && stat(filePath.c_str(), &fileStat) == 0 && fileStat.st_size > 0) ;
   }

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGGridLayout						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGGridLayout : public TGLayoutManager {
   public:
      TLGGridLayout (TGCompositeFrame* p, const Int_t* coord) 
      : fMain (p), fCoord (coord) {
         fList = fMain->GetList(); }
      virtual void Layout ();
      virtual TGDimension GetDefaultSize() const {
         return TGDimension (fCoord[0], fCoord[1]); }
   
   protected:
      TGCompositeFrame*	fMain;
      TList*		fList;
      const Int_t*	fCoord;
   };

//______________________________________________________________________________
   void TLGGridLayout::Layout ()
   {
      const Int_t* i = fCoord + 2;
      TGFrameElement* ptr;
      TIter next (fList);
      while ((ptr = (TGFrameElement*) next())) {
         ptr->fFrame->MoveResize (i[0], i[1], i[2], i[3]);
         i += 4;
      }
   }


void
TLGFilterWizWindow::FileErrorIndication(Bool_t error)
{
   Pixel_t red ;
   Pixel_t black ;
   Pixel_t yellow ;					
   Pixel_t green ;					
   gClient->GetColorByName("red", red) ;
   gClient->GetColorByName("black", black) ;
   gClient->GetColorByName("yellow", yellow) ;	
   gClient->GetColorByName("green", green) ;		
   
   if (error)
   {
      fFileError->SetForegroundColor(black) ;
      fFileError->SetBackgroundColor(red) ;
      fFileError->SetToolTipText("Display errors reading file");
   }
   else
   {
      fFileError->SetForegroundColor(black) ;
      fFileError->SetBackgroundColor(fLabel[0]->GetBackground()) ;
      fFileError->SetToolTipText("No errors to display");
   }
   return ;
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGFilterWizWindow                                                   //
//                                                                      //
// Filter wizard frame                                                  //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGFilterWizWindow::TLGFilterWizWindow (const TGWindow* parent, 
                     std::string& fdesign, bool simple, 
                     const char* path, const char* file, Int_t id,
		     const char *module, int section, bool any_sample,
		     bool experiment_mode)
   : TGVerticalFrame (parent, 10, 10), TGWidget (id), fSimple (simple),
   fReadOnly (kTRUE), fClosedLoop (kFALSE), fClosedLoopNeg (kFALSE),
   fFilter (&fdesign), fCurModule (0), fCurSectionActive (kFALSE), 
   fCurSwitchActive (kFALSE), fCurDesignActive (kFALSE), fDirty (kFALSE), 
   fAnySample(any_sample), fExperimentMode(experiment_mode)
   {
      Pixel_t red ;					
      Pixel_t yellow ;				
      Pixel_t black ;			
      Pixel_t green ;					
      gClient->GetColorByName("red", red) ;	
      gClient->GetColorByName("yellow", yellow) ;	
      gClient->GetColorByName("black", black) ;	
      gClient->GetColorByName("green", green) ;

      for (int i = 0; i < 6; ++i) fF[i] = 0;
      for (int i = 0; i < 5; ++i) fG[i] = 0;
      for (int i = 0; i < 8; ++i) fL[i] = 0 ;
      for (int i = 0; i < 30; ++i) fLabel[i] = 0;
      fPath = 0;
      fCdup = 0;
      fPcdup = 0;
      fFv = 0;
      fFc = 0;
      fFile = 0;
      fModule = 0;
      fSectMul = 0;
      for (int i = 0; i < 2; ++i)
      {
	 fPSectSel[i] = 0;
	 fSectSel[i] = 0;
      }
      for (int i = 0; i < kMaxFilterSections; ++i) {
         fSect[i] = 0;
         fSectName[i] = 0;
      }
      for (int i = 0; i < 4; ++i) fSectCP[i] = 0 ;
      fSwitchInp = 0;
      fSwitchOut = 0;
      for (int i = 0; i < 3; ++i) fSwitchParam[i] = 0;
      for (int i = 0; i < 4; ++i) fPlotParam[i] = 0 ;
      fPlotType = 0 ;
      fSample = 0 ;
      fCommand = 0 ;
      fAlt = 0 ;
      for (int i = 0; i < 8; ++i) fSZFormat[i] = 0 ;
      for (int i = 0; i < 13; ++i) fAdd[i] = 0 ;
      fImportDesign = 0 ;
      fSetGainF = 0 ;
      fSetGainVal = 0 ;
      fSetGainUnit = 0 ;
      fSetGainCurrent = 0 ;
      fSetGainSign = 0 ;
      fSetGain = 0 ;
      fSetGainCurrentUnit = 0 ;
      fFileError = 0 ;

      // layouts
      // kLHintsLeft puts objects on the left side, 
      // kLHintsExpandX expands object to fill the frame in the horizontal direction.
      // kLHintsTop places objects at the top of the frame.
      // Final args are pixel padding on left, right, top, and bottom of frame.
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 0, 0);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 4, 4, 4, 4);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 4, 4, 4, 4);
      fL[3] = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 6, 4, 2, 2);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 2, unsigned(-6));
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, 
				 unsigned(-2), 2, 2);
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 2, 2);
      // groups
      if (!fSimple) {
         fF[0] = new TGHorizontalFrame (this, 10, 10);
         AddFrame (fF[0], fL[0]);
         fG[0] = new TGGroupFrame (fF[0], "Module Selection");
         fF[0]->AddFrame (fG[0], fL[1]);
         fG[1] = new TGGroupFrame (fF[0], "Sections");
         fF[0]->AddFrame (fG[1], fL[1]);
         fG[2] = new TGGroupFrame (fF[0], "Switching");
         fF[0]->AddFrame (fG[2], fL[1]);
      }
      fG[3] = new TGGroupFrame (this, "Design");
      AddFrame (fG[3], fL[2]);
      fG[4] = new TGGroupFrame (this, "Plotting");
      AddFrame (fG[4], fL[2]);
   
      // module selection
      if (!fSimple) {
         fG[0]->SetLayoutManager (new TLGGridLayout (fG[0], kModSelGrid));
         for (int i = 0; i < 3; ++i) {
            const char* const labels[] = 
               {"Path:", "", "Module:"};
            fLabel[i] = new TGLabel (fG[0], labels[i]);
            fLabel[i]->SetTextJustify (kTextLeft | kTextCenterY);
//            fG[0]->AddFrame (fLabel[i], 0);
         }
	 // Substitute a button for the file label. (fLabel[1])
	 // This button changes to a red color if there are errors.
	 // Pressing the red button shows the errors in a popup.
	 fFileError = new TGTextButton(fG[0], "File...  ", kFileError) ;
	 // Direct input events to this object's ProcessEvent method.
	 fFileError->Associate(this) ;

	 fG[0]->AddFrame(fLabel[0],0) ;
	 fG[0]->AddFrame(fFileError,0) ;
	 fG[0]->AddFrame(fLabel[2],0) ;

	 // Directory combo box
         fPath = new TGFSComboBox (fG[0], kPathId);
         fPath->Associate (this);
         fG[0]->AddFrame (fPath, 0);
         // Change directory up one level button.
         fPcdup = fClient->GetPicture("tb_uplevel.xpm");
         if (!fPcdup) {
            Error("TLGFilterWizWindow", "missing toolbar pixmap(s).n");
         }
         fCdup = new TGPictureButton (fG[0], fPcdup, kDirUpId);
         fCdup->SetToolTipText("Up One Level");
         fCdup->Associate(this);
         fG[0]->AddFrame(fCdup, 0);
	 // List box for showing directories
         fFv = new TGListView (fG[0], 400, 161);
         // File container, shows contents of the current directory.  Automatic
         // updating if the contents of the directory are changed.
         fFc = new TGFileContainer(fFv->GetViewPort(),
                              10, 10, kHorizontalFrame, fgWhitePixel);
         fFc->Associate (this);
         fFv->GetViewPort()->SetBackgroundColor (fgWhitePixel);
         fFv->SetContainer (fFc);
         fFv->SetViewMode (kLVList);
         fFc->SetFilter ("YOUneverHAVEaFILElikeTHIS!!!");
         fFc->Sort (kSortByType);
         if (!path) {
            path = ".";
         }
         fFc->ChangeDirectory (path);
         fPath->Update(fFc->GetDirectory());
         fG[0]->AddFrame(fFv, 0);

         // Combo box for file selection.
         fFile = new TGComboBox (fG[0], kFileId);
         fFile->Associate (this);
         fG[0]->AddFrame (fFile, 0);

         // Combo box for module selection.
         fModule = new TGComboBox (fG[0], kModuleId);
         fModule->Associate (this);
         fG[0]->AddFrame (fModule, 0);
      }
   
      // sections selection
      if (!fSimple) {
         fG[1]->SetLayoutManager (new TLGGridLayout (fG[1], kSecSelGrid));
         fLabel[3] = new TGLabel (fG[1], "Select:");
         fLabel[3]->SetTextJustify (kTextLeft | kTextCenterY);
         fG[1]->AddFrame (fLabel[3], 0);
         fSectMul = new TGComboBox (fG[1], kSectMulId);
         fSectMul->Associate (this);
         fSectMul->AddEntry ("Single", 0);
         fSectMul->AddEntry ("Multiple", 1);
         fSectMul->Select (0);
         fG[1]->AddFrame (fSectMul, 0);
         fPSectSel[0] = fClient->GetPicture ("arrow_up.xpm");
         fPSectSel[1] = fClient->GetPicture ("arrow_down.xpm");
         if (!fPSectSel[0] || !fPSectSel[1]) {
            Error("TLGFilterWizWindow", "missing toolbar pixmap(s).n");
         }
         fSectSel[0] = new TGPictureButton (fG[1], fPSectSel[0], 
                              kSectSelAllId);
         fSectSel[0]->SetToolTipText("Select All");
         fSectSel[0]->Associate(this);
         fG[1]->AddFrame (fSectSel[0], 0);
         fSectSel[1] = new TGPictureButton (fG[1], fPSectSel[1], 
                              kSectSelNoneId);
         fSectSel[1]->SetToolTipText("Deselect All");
         fSectSel[1]->Associate(this);
         fG[1]->AddFrame (fSectSel[1], 0);
         for (int i = 0; i < kMaxFilterSections; ++i) {
            char buf[64];
            sprintf (buf, "Filter Section %i", i);
            fSect[i] = new TGCheckButton (fG[1], "", kSectId + i);
            fSect[i]->SetToolTipText (buf);
            fSect[i]->Associate(this);
            fG[1]->AddFrame (fSect[i], 0);
            fSectName[i] = new TLGTextEntry (fG[1], "", kSectNameId + i);
            fSectName[i]->Associate(this);
            fG[1]->AddFrame (fSectName[i], 0);
         }
         fSect[0]->SetState (kButtonDown);
         fCurSections.insert (0);
         // Add a copy, paste, cut, and undo button to the bottom of the frame.
         const char* const cpLabel[] = {"Copy", "Paste", "Cut", "Undo"} ;
         const char* const cpTips[] = {"Copy selected sections into buffer",
                                        "Paste copied sections into module",
                                        "Remove selected sections from module",
                                        "Undo paste or cut operation"} ;
         for (int i=0; i<4; i++)
         {
             fSectCP[i] = new TGTextButton(fG[1], cpLabel[i], kSelCopyId+i) ;
             fSectCP[i]->SetToolTipText(cpTips[i]) ;
             fSectCP[i]->Associate(this) ;
             fG[1]->AddFrame(fSectCP[i],0) ;
             fSectCP[i]->SetState(kButtonDisabled) ;
         }
         // Make sure the copy buffer starts out empty.
         filterwiz::fSectCopyList.clear() ;
      }
   
      // Switching
      if (!fSimple) {
         fG[2]->SetLayoutManager (new TLGGridLayout (fG[2], kSwitchGrid));
         for (int i = 0; i < 7; ++i) {
            const char* const labels[] = 
               {"Input:", "Output:", "Ramp Time:", "Tolerance:", 
               "Timeout:", "sec", "sec"};
            fLabel[i+4] = new TGLabel (fG[2], labels[i]);
            fLabel[i+4]->SetTextJustify (kTextLeft | kTextCenterY);
            fG[2]->AddFrame (fLabel[i+4], 0);
         }
         fSwitchInp = new TLGComboBox (fG[2], kSwitchInpId);
         fSwitchInp->Associate (this);
         fG[2]->AddFrame (fSwitchInp, 0);
         fSwitchOut = new TLGComboBox (fG[2], kSwitchOutId);
         fSwitchOut->Associate (this);
         fG[2]->AddFrame (fSwitchOut, 0);

	 // Create the fields for the switching parameters
	 // Bounds on the user input should be applied here
	 // by specifying min and max for the TLGNumericControlBox constructor
         for (int i = 0; i < 3; ++i) 
	 {
            fSwitchParam[i] = new TLGNumericControlBox (fG[2], 
                                 0.0, 6, kSwitchParamId + i, kNESReal,
                                 kNEANonNegative);
            fSwitchParam[i]->Associate (this);
            fSwitchParam[i]->SetState (kFALSE);
            fG[2]->AddFrame (fSwitchParam[i], 0);
         }
      }
   
      // Plotting
      fG[4]->SetLayoutManager (new TLGGridLayout (fG[4], kPlotGrid));
      for (int i = 0; i < 5; ++i) {
         const char* const labels[] = 
            {"fStart:", "fStop:", "Number of Points:", "Type:",
            "Duration:"};
         fLabel[i+11] = new TGLabel (fG[4], labels[i]);
         fLabel[i+11]->SetTextJustify (kTextLeft | kTextCenterY);
         fG[4]->AddFrame (fLabel[i+11], 0);
      }
      for (int i = 0; i < 3; ++i) {
         const double numinit[] = {1, 1000, 1001};
         fPlotParam[i] = new TLGNumericControlBox (fG[4], 
                              numinit[i], 6, kPlotParamId + i,
                              i == 2 ? kNESInteger : kNESReal,
                              kNEAPositive);
         fPlotParam[i]->Associate (this);
         fG[4]->AddFrame (fPlotParam[i], 0);
      }
      fPlotType = new TGComboBox (fG[4], kPlotTypeId);
      fPlotType->Associate (this);
      fPlotType->AddEntry ("Logarithmic", 0);
      fPlotType->AddEntry ("Linear", 1);
      fPlotType->Select (0);
      fG[4]->AddFrame (fPlotType, 0);
      fPlotParam[3] = new TLGNumericControlBox (fG[4], 1.0, 6, 
                           kPlotParamId + 3, kNESReal, kNEAPositive);
      fPlotParam[3]->Associate (this);
      fG[4]->AddFrame (fPlotParam[3], 0);
   
      // Design
      fG[3]->SetLayoutManager (new TLGGridLayout (fG[3], kDesignGrid));
      //--------  Labels
      for (int i = 0; i < 11; ++i) {
         const char* const labels[] = 
            {"fSample:", "Hz", "Command:", "Alternate:", "s-Plane:", 
            "z-Plane:", "Add:", "Gain:", "Hz", "Value:",
            "Current:" };
         fLabel[i+16] = new TGLabel (fG[3], labels[i]);
         fLabel[i+16]->SetTextJustify (kTextLeft | kTextCenterY);
         fG[3]->AddFrame (fLabel[i+16], 0);
      }

      //--------  Sample rate selection
      fSample = new TLGComboBox(fG[3], kSampleId) ;
      fSample->AddEntry("2048", 2048) ;
      fSample->AddEntry("4096", 4096) ;
      fSample->AddEntry("8192", 8192) ;
      fSample->AddEntry("16384", 16384) ;
      fSample->AddEntry("32768", 32768) ;
      fSample->AddEntry("65536", 65536) ;
      if (fExperimentMode) {
	 // Add an entry for a custom sample rate
	 fSample->AddEntry("Other...",-1000) ;
      }
      fSample->Select(2048) ; // Set as default
      fSample->Associate (this);
      fG[3]->AddFrame (fSample, 0);

      //--------  Command text-box
      fCommand = new TDGTextEdit (fG[3], 10, 10, kCommandId);
      fCommand->Clear();
      fCommand->Associate (this);
      fG[3]->AddFrame (fCommand, 0);

      //--------  Alternate text-box
      fAlt = new TGTextView (fG[3], 10, 10, kAltId);
      fAlt->Associate (this);
      fG[3]->AddFrame (fAlt, 0);

      //--------  Mode radio buttonos
      for (int i = 0; i < 8; ++i) {
         const char* const labels[] = 
            {"s (rad/s)", "f (Hz)", "n (Hz/norm)", "Polynomial",
            "SOS", "Roots", "Direct", "Online"};
         const char* const tooltips[] = 
            {"Format with poles in left half plane and units of rad/s", 
            "Format with poles in left half plane and units of Hz", 
            "Format with poles in left half plane, units of Hz and "
            "normalized gains",
            "Rational polynomial in s", 
            "Second Order Section format", 
            "Roots of the z-plane",
            "Direct form",
            "Format used by online system"}; 
         fSZFormat[i] = new TGRadioButton (fG[3], labels[i], 
                              kSZFormatId + i);
         fSZFormat[i]->SetToolTipText (tooltips[i]);
         fSZFormat[i]->Associate (this);
         fG[3]->AddFrame (fSZFormat[i], 0);
      }
      fSZFormat[2]->SetState (kButtonDown);
      fSetGainF = new TLGNumericControlBox (fG[3], 
                           0, 6, kSetGainFId, kNESReal,
                           kNEAPositive);
      fSetGainF->Associate (this);
      fG[3]->AddFrame (fSetGainF, 0);
      fSetGainVal = new TLGNumericControlBox (fG[3], 
                           1, 6, kSetGainValId, kNESReal);
      fSetGainVal->Associate (this);
      fG[3]->AddFrame (fSetGainVal, 0);
      fSetGainUnit = new TLGComboBox (fG[3], kSetGainUnitId);
      fSetGainUnit->Associate (this);
      fSetGainUnit->AddEntry ("mag", 0);
      fSetGainUnit->AddEntry ("dB", 1);
      fSetGainUnit->Select (0);
      fG[3]->AddFrame (fSetGainUnit, 0);
      fSetGainSign = new TGTextButton (fG[3], "=", kSetGainSignId);
      fSetGainSign->SetToolTipText 
         ("Sign of real part of transfer coefficient");
      fSetGainSign->Associate (this);
      fG[3]->AddFrame (fSetGainSign, 0);
      fSetGain = new TGTextButton (fG[3], "Set", kSetGainId);
      fSetGain->SetToolTipText 
         ("Set the gain at frequency f to the specified value");
      fSetGain->Associate(this);
      fG[3]->AddFrame (fSetGain, 0);
      fSetGainCurrent = new TLGTextEntry (fG[3], "", kSetGainCurrentId);
      fSetGainCurrent->Associate (this);
      fSetGainCurrent->SetState (kFALSE);
      fG[3]->AddFrame (fSetGainCurrent, 0);
      fSetGainCurrentUnit = new TLGComboBox (fG[3], kSetGainCurrentUnitId);
      fSetGainCurrentUnit->Associate (this);
      fSetGainCurrentUnit->AddEntry ("dB/deg", 0);
      fSetGainCurrentUnit->AddEntry ("mag/deg", 1);
      fSetGainCurrentUnit->AddEntry ("Re/Im", 2);
      fSetGainCurrentUnit->AddEntry ("dB/rad", 3);
      fSetGainCurrentUnit->AddEntry ("mag/rad", 4);
      fSetGainCurrentUnit->Select (0);
      fG[3]->AddFrame (fSetGainCurrentUnit, 0);
      // buttons
      for (int i = 0; i < 13; ++i) {
         const char* const labels[] = 
            {"Gain", "ZPK", "RPoly", "ResGain", "Notch",
            "Ellip", "Butter", "Cheby1", "Cheby2", 
            "Comb", "SOS", "zRoots", "Direct"};
         const char* const tooltips[] = 
            {"Gain multiplier", "Zero-pole-gain specification", 
            "Rational polynomial", "Resonant gain filter", "Notch filter", 
            "Elliptic filter", "Butterworth filter", "Chebyshev Type 1 filter", 
            "Chebyshev Type 2 filter",
            "Comb filter (notch or resonant gain)",
            "List of second order sections", 
            "List of z-plane roots", "Direct form (avoid using)"}; 
         fAdd[i] = new TGTextButton (fG[3], labels[i], kAddId + i);
         fAdd[i]->SetToolTipText (tooltips[i]);
         fAdd[i]->Associate(this);
         fG[3]->AddFrame (fAdd[i], 0);
      }
      // Import button. - JCB
      if (!fSimple)
      {
	 fImportDesign = new TGTextButton(fG[3], "Import...", kImportId) ;
	 fImportDesign->SetToolTipText("Import design string from a text file") ;
	 fImportDesign->Associate(this) ;
	 fG[3]->AddFrame(fImportDesign, 0) ;
      }

      Setup (path, file, module);

      // Select a filter section if requested
      if (!fSimple)
      {
	 for (int i = 0; i < kMaxFilterSections; ++i)
	 {
	    if (i == section-1)
	    {
	       fSect[i]->SetState(kButtonDown) ;
	    }
	    else
	    {
	       fSect[i]->SetState(kButtonUp) ;
	    }
	 }
	 SelectSections() ;
	 SetSectButtonStates() ; // Set the filter section editing button states.  JCB
      }
   }

//______________________________________________________________________________
   TLGFilterWizWindow::~TLGFilterWizWindow ()
   {
      if (fSetGainCurrentUnit) delete fSetGainCurrentUnit;
      if (fSetGainCurrent) delete fSetGainCurrent;
      if (fSetGain) delete fSetGain;
      if (fSetGainSign) delete fSetGainSign;
      if (fSetGainUnit) delete fSetGainUnit;
      if (fSetGainVal) delete fSetGainVal;
      if (fSetGainF) delete fSetGainF;
      for (int i = 0; i < 13; ++i) if (fAdd[i]) delete fAdd[i];
      for (int i = 0; i < 8; ++i) if (fSZFormat[i]) delete fSZFormat[i];
      if (fAlt) delete fAlt;
      if (fCommand) delete fCommand;
      if (fSample) delete fSample;
      if (fPlotType) delete fPlotType;
      for (int i = 0; i < 4; ++i) if (fPlotParam[i]) delete fPlotParam[i];
      for (int i = 0; i < 3; ++i) if (fSwitchParam[i]) delete fSwitchParam[i];
      if (fSwitchOut) delete fSwitchOut;
      if (fSwitchInp) delete fSwitchInp;
      for (int i = 0; i < kMaxFilterSections; ++i) if (fSectName[i]) delete fSectName[i];
      for (int i = 0; i < kMaxFilterSections; ++i) if (fSect[i]) delete fSect[i];
      for (int i = 0; i < 2; ++i) 
         if (fPSectSel[i]) fClient->FreePicture (fPSectSel[i]);
      for (int i = 0; i < 2; ++i) if (fSectSel[i]) delete fSectSel[i];
      for (int i = 0; i < 4; ++i) if (fSectCP[i]) delete fSectCP[i] ;   // JCB
      if (!fSimple)
	 if (fImportDesign) delete fImportDesign ; // JCB
      if (fSectMul) delete fSectMul;
      if (fModule) delete fModule;
      if (fFile) delete fFile;
      if (fFc) delete fFc;
      if (fFv) delete fFv;
      if (fCdup) delete fCdup;
      if (fPcdup) fClient->FreePicture (fPcdup);
      if (fPath) delete fPath;
      for (int i = 0; i < 27; ++i) if (fLabel[i]) delete fLabel[i];
      for (int i = 0; i < 5; ++i) if (fG[i]) delete fG[i];
      for (int i = 0; i < 1; ++i) if (fF[i]) delete fF[i];
      for (int i = 0; i < 8; ++i) if (fL[i]) delete fL[i];
      if (fFileError) delete fFileError ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::AddFiles (const char* dir, const char* file)
   {
      typedef std::vector<std::string> stringlist;
   
      fFile->GetListBox()->IntegralHeight(kTRUE) ;
      // loop over files
      void *dirp;
      if ((dirp = gSystem->OpenDirectory (dir)) == 0) 
      {
         fFile->SetTopEntry (new TGTextLBEntry (fFile, new TGString (""), 0), 
            new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                              kLHintsExpandX));
         fFile->MapSubwindows();
         fFile->Layout();
         return kFALSE;
      }

      //-------------------------------- Scan file in directory. Compare
      //                                 first line to "magic string"
      stringlist nlist;
      int lMagic = strlen(kMagic);
      const char* name;
      while ((name = gSystem->GetDirEntry (dirp)) != 0) 
      {
         if (strcmp (name, ".") && strcmp(name, "..")) 
	 {
            printf("Checking file %s... ", name);
            // found a file: open it and check magic token
            string fullname = string (dir) + "/" + name;
            ifstream inp (fullname.c_str());
            char line[128];
            inp.read (line, lMagic);
            if (inp && (strncmp (line, kMagic, lMagic) == 0)) 
	    {
               printf("Added\n");
               nlist.push_back (name);
            }
            printf("Ignored\n");
         }
      }
      gSystem->FreeDirectory(dirp);
      sort (nlist.begin(), nlist.end());


      int id = 0;
      int cur = -1;
      for (stringlist::iterator i = nlist.begin(); i != nlist.end(); ++i) 
      {
         if (file && (*i == file)) cur = id;
         fFile->AddEntry (i->c_str(), id++);
      }
      if (cur < 0) cur = 0;
      // select file
      if (id > 0) 
      {
         fFile->MapSubwindows();
	 fFile->GetListBox()->Resize(100, 100) ; // JCB - Why is this needed???
         fFile->Select (cur); // This should call Layout().
      }
      else 
      {
         fFile->SetTopEntry (new TGTextLBEntry (fFile, new TGString (""), 0), 
            new TGLayoutHints (kLHintsLeft | kLHintsExpandY | kLHintsExpandX));
         fFile->MapSubwindows();
         fFile->Layout();
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::Setup (const char* dir, const char* file,
                     const char* module)
   {
      Bool_t retval ;
      if (fSimple) 
      {
         fFilterFile.add ("Default", 16384);
         fCurModule = fFilterFile.find ("Default");
         fCurSections.insert (0);
         if (fCurModule && fFilter) {
            (*fCurModule)[0].setDesign (fFilter->c_str());
         }
         return kTRUE;
      }
      // Setup only reads a new file if the directory has changed.
      if (dir && (fCurDir == TString (dir))) 
      {
	 FileErrorIndication(kFALSE) ;
         return kTRUE;
      }
      fFile->RemoveEntries (0, 10000);
      if (!dir) 
      {
         ReadFile (0);
	 FileErrorIndication(kFALSE) ;
         return kFALSE;
      }
      if (!AddFiles (dir, file)) 
      {
         ReadFile (0);
	 FileErrorIndication(kFALSE) ;
         return kTRUE;
      }
      fCurDir = dir;
      TGTextLBEntry* e = (TGTextLBEntry*)fFile->GetSelectedEntry();
      if (!e) 
      {
         ReadFile (0);
	 FileErrorIndication(kFALSE) ;
         return kTRUE;
      }
      retval = ReadFile (e->GetText()->GetString(), module);
      // Check to see if the file was read cleanly.  If not, warn the user.
      if (!fFilterFile.errorsEmpty())
      {
	 // Provide visual indication to user.
	 FileErrorIndication(kTRUE) ;
	 // List the errors for the user.
//	 new TLGErrorDialog(gClient->GetRoot(), fParent, fFilterFile.getErrors(), TGString(e->GetText()->GetString())) ;
      }
      else
      {
	 // Clear the indication that the file has an error.
	 FileErrorIndication(kFALSE) ;
      }
      return retval ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::UpdateFileSelection()
   {
      if (fSimple) {
         return kTRUE;
      }
      fFc->ChangeDirectory (fCurDir);
      gSystem->cd((const char *) fCurDir) ; // JCB
      fPath->Update (fFc->GetDirectory());
      fFile->RemoveEntries (0, 10000);
      if (!AddFiles (fCurDir, fCurFile)) 
      {
         ReadFile (0);
	 FileErrorIndication(kFALSE) ;
         return kFALSE;
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::SetExperimentMode(Bool_t mode) 
   {
      fExperimentMode = mode ;
      return( kTRUE) ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::ReadOnly() const
   {
      return fReadOnly;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::SetReadOnly (Bool_t readonly)
   {
      if (readonly != fReadOnly) {
         fReadOnly = readonly;
         Update (kTRUE, kTRUE);
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGFilterWizWindow::SetDirty (Bool_t on)
   {
      fDirty = on;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::CheckDirty()
   {
      if (!IsDirty() || ReadOnly()) {
         return kTRUE;
      }
      Int_t ret;
      new TGMsgBox (gClient->GetRoot(), fParent, "Save", 
                   "Do you want to save changes?",
                   kMBIconQuestion, kMBYes | kMBNo | kMBCancel, &ret);
      if (ret == kMBCancel) {
         return kFALSE;
      }
      else if (ret == kMBYes) {
         return SaveFile();
      }
      else {
         return kTRUE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::IsValid (Bool_t msgbox)
   {
      if (fCurFile == "") {
         return kTRUE;
      }
      string errmsg;
      if ((fCurFile == "") || fFilterFile.valid (errmsg)) {
         return kTRUE;
      }
      if (msgbox) {
         errmsg += " Invalid filter modules/sections:\n" + errmsg;
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      errmsg.c_str(), kMBIconExclamation, kMBOk, 0);
      }
      return kFALSE;
   }

//______________________________________________________________________________
// Create a new module
   Bool_t TLGFilterWizWindow::AddNewModule(const char *name)
   {
      int retval = 0 ;
      if (!name)
	 return kFALSE ;

      int cur = -1 ;

      // Get the id number of the module for the menu. It will be the last one in the list.
      cur = fFilterFile.modules().size() ;

      // Add the name to the menu with an id beyond the last current entry.
      fModule->AddEntry(name, cur) ;

      // Add a new module to the list of modules.
      fFilterFile.modules().push_back(FilterModule(name, (double) fSample->GetSelected())) ;

      // Select the new module.
      fModule->Select (cur);
      retval = SelectModule (((TGTextLBEntry*)fModule->GetSelectedEntry())->GetText()->GetString());

      return retval ;
   }

//______________________________________________________________________________
// Create a new file with no modules.
   Bool_t TLGFilterWizWindow::NewFile()
   {
      Bool_t retval = kTRUE ;

      fFile->SetTopEntry(new TGTextLBEntry (fFile, new TGString (""), 0),
			 new TGLayoutHints (kLHintsLeft | kLHintsExpandY |
					     kLHintsExpandX));
      fFile->Select(0) ;

      TGTextLBEntry* e = (TGTextLBEntry*)fFile->GetSelectedEntry();
      if (!e) {
	 // Foton was probably started in a directory with no filter files.
	 fFilterFile.SetAnySampleRate(fAnySample) ;
	 fFilterFile.setFilename("") ;
	 fCurModule = 0 ;
	 fCurFile = "" ;

	 // Make the sample rate selectable.
	 fSample->SetEnabled(kTRUE) ;

	 // Add a new entry to the modules list to allow new modules
	 
      } else if (CheckDirty()) {
	 // Clear out the old modules from the menu.
	 fModule->RemoveEntries (0, 1000) ;

	 //string fullname = string (fCurDir) + "/" ;
	 fFilterFile.SetAnySampleRate(fAnySample) ;
	 fFilterFile.setFilename("") ;
	 fCurModule = 0 ;
	 fCurFile = "" ;
	 // Clear out the selected filter file.

	 // Clear the modules
	 fFilterFile.clear() ;
	 SelectModule(0) ;

	 // Make the sample rate selectable.
	 fSample->SetEnabled(kTRUE) ;

	 // Add a new entry to the modules list to allow new modules


      }
      else
      {
	 UpdateFileSelection();
      }

      return (retval) ;
   }

//______________________________________________________________________________
// Read a filter file, and if module is not 0, select the module as current if
// it is in the filter file.
   Bool_t TLGFilterWizWindow::ReadFile (const char* file, 
                     const char* module)
   {
      Bool_t retval = kTRUE ;

      string oldname = fCurModule ? fCurModule->getName() : "";

      if (my_debug) cerr << "READ FILE " << (file ? file : "") << endl;
      fModule->RemoveEntries (0, 10000);
      string fullname = string(fCurDir.Data()) + "/" + (file ? file : "");
      fFilterFile.SetAnySampleRate(fAnySample) ;
      if (!file || !fFilterFile.read (fullname.c_str())) 
      {
         fCurModule = 0;
         fCurFile = "";
         SetDirty (kFALSE);
         return SelectModule (0);
      }
      fCurModule = 0;
      fCurFile = file;
      SetDirty (kFALSE);
      // Add modules to selection list
      int cur = -1;
      int id = 0;
      for (FilterModuleList::iterator i = fFilterFile.modules().begin();
          i != fFilterFile.modules().end(); ++i) 
      {
	 // If the module name is non-zero and i points to the module
	 // which matches the name, use it as the current module.
         if (module && strcmp (module, i->getName()) == 0) 
	    cur = id;
	 // If there was a previously selected module and it's name matches the
	 // module pointed to by i, select it as the current module.  This is
	 // an aid to the user if they want to edit the same module in different
	 // files, as each new file read will have the same module selected.
         if ((cur < 0) && !oldname.empty() && strcmp (oldname.c_str(), i->getName()) == 0) 
	    cur = id;
	 // Finally, add the module to the list of modules.
         fModule->AddEntry (i->getName(), id++);
      }
      fModule->MapSubwindows();
      fModule->Layout();

      // Set cur to 0 if no matching module was found, so first module in the list
      // will be selected as current.
      if (cur < 0) cur = 0;

      // Set module, if id is greater than 0 there were modules added to the list.
      // cur will be the index of the matching module name, or 0 as a default to
      // indicate the first module in the list.
      if (id > 0) 
      {
         fCurModule = 0;
         fModule->Select (cur);
	 // SelectModule by name.
         retval = SelectModule (((TGTextLBEntry*)fModule->GetSelectedEntry())->
                             GetText()->GetString());
      }
      else
      {
	 // Select no module, there weren't any.
	 retval = SelectModule (0);
      }
      return retval ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::SaveFile()
   {
      if (my_debug) cerr << "SAVE FILE" << endl;
      if (ReadOnly()) {   
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Unable to save filters in readonly mode.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
// Allow saving even if nothing has changed.  A new filter file created
// by the RCG doesn't have anything in it other than a list of modules and
// a sampling rate.  By allowing the user to save this unchanged file, foton
// will finish the file with comments for each filter module, and apparently
// this is needed by EZquack so it can have a place to put Matlab-generated
// filters.
#if 0
      if (!IsDirty()) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Nothing to save.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kTRUE;
      }
#endif
      // If the file name is empty, use SaveAs() instead.
      if (!strlen(fFilterFile.getFilename())) {
	 return SaveAs() ;
      }

      if (!IsValid()) {
         return kFALSE;
      }
      // If there were errors reading the file, warn the user that writing may
      // delete filters.
      if (!fFilterFile.errorsEmpty())
      {
	 Int_t ret ;
	 new TGMsgBox(gClient->GetRoot(), fParent, "Warning",
		      "There were errors reading the file!\n*** SAVING MAY DELETE FILTERS! ***\nContinue?",
		      kMBIconExclamation, kMBYes | kMBNo | kMBCancel, &ret) ;
	 if (ret == kMBCancel)
	 {
	    return kFALSE ;
	 }
	 if (ret == kMBNo)
	 {
	    return kTRUE ;
	 }
      }
      if (!fFilterFile.checkFileStat())
      {
	 new TGMsgBox(gClient->GetRoot(), fParent, "Error",
		     "Unable to save. Some other process has modified the file.",
		     kMBIconExclamation, kMBOk, 0) ;
	 return kFALSE ;
      }
      string filename = (const char*)fCurFile;
      if (filename.empty()) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Cannot save empty file.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      Update (kFALSE);
      // save
      // Make changes to the way files are saved here...  JCB
      //filename = string (fCurDir) + "/" + filename; // Removed, it causes issues with "foton dir1/file.txt"
      filename = fFilterFile.getRealFilename(filename, string(fCurDir.Data())) ;  // Follow the symbolic link? - JCB
      string tmpfilename = string (filename) + ".tmp" ;

      {
	 char errmsg[512] ;
	 memset(errmsg, 0, 512) ;
	 if (false == fFilterFile.write ((const char *) tmpfilename.c_str(), (char *) errmsg)) {
	    cerr << "write failed - " << errmsg << endl ;
	    new TGMsgBox (gClient->GetRoot(), fParent, "Error", errmsg,
			 kMBIconExclamation, kMBOk, 0);         
	    return kFALSE;
	 }
      }
      // The temp file has been written. 
      // If a backup is needed, link(filename.c_str(), <backup name>) at this
      // point.  rename() won't remove the link if this is done...
      if (rename(tmpfilename.c_str(), filename.c_str()))
      {
	 cerr << "rename() failed, " << strerror(errno) << endl ;
      }
      fFilterFile.updateFileStat(filename.c_str()) ;

      // Writing the file should have cleared the file error messages.  Check the 
      // file errors, if there are new ones, they are from writing the file.
      if (!fFilterFile.errorsEmpty())
      {
	 // Display the file errors.
	 new TLGErrorDialog(gClient->GetRoot(), fParent, fFilterFile.getErrors(), TGString(fFilterFile.getFilename())) ;
      }

      SetDirty (kFALSE);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::SaveAs()
   {
      // file save as dialog
      TGFileInfo info;
      info.fFilename = StrDup(fCurFile.Data()) ; // JCB - Start with the filename.
      info.fIniDir = StrDup(fCurDir.Data()) ;	 // JCB - Start with the current directory.
      info.fMultipleSelection = kFALSE ;	 // JCB - Allow only one file selection.
      info.fFileTypeIdx = 0 ;			 // JCB - Set the file filter to *.txt.

      if (!IsValid()) {
         return kFALSE;
      }
      
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gSaveAsTypes);
   #else
      info.fFileTypes = const_cast<char**>(gSaveAsTypes);
   #endif
      // Present the Save As dialog box to the user.
   #if 1
      {
	 // On return, info.fFilename will be filled in
	 // unless the user cancels the operation.
	 // Note that the default extension is in the
	 // TGFileInfo struct info.
	 new TLGFileDialog(fParent, &info, kFDSave) ;
      }
   #else
      if (!TLGFileDialog (this, info, kFDSave, ".txt")) {
         return kFALSE;
      }
   #endif
      if (!info.fFilename)
      {
	 // If the info.fFilename came back null, the user canceled.
	 return kFALSE ;
      }

      string filename = info.fFilename ;

   #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
      delete [] info.fFilename;
   #endif
      // save file
// Make changes to the way files are written here... JCB

      Update (kFALSE);
      // JCB
      // The filename returned by the Save As dialog box appears to be
      // a full path.  getRealFilename() expects a directory name of the
      // directory where the file exists, so split the string at the last
      // '/' to get the directory.  If this comes up null, use '/' since
      // the user is trying to write to the root directory (not a good idea).
      size_t pos = filename.find_last_of("/") ;
      string dirname =  filename.substr(0,pos) ;

      // Find the real name of the file, since filename could point to 
      // a symbolic link.  We don't want to remove the symbolic link.
      if (my_debug) cerr << "SaveAs(): User chose " << filename << " in directory " << dirname << '\n';
      filename = fFilterFile.getRealFilename(filename, dirname) ;
      if (my_debug) cerr << "SaveAs(): real filename = " <<  filename << '\n' ; 
      // JCB - end

      string tmpfilename = string(filename) + ".tmp" ;
      if (my_debug) cerr << "SAVE AS:" << tmpfilename << endl ;      
      {
	 char errmsg[512] ;
	 memset(errmsg, 0, 512) ;
	 if (false == fFilterFile.write ((const char *) tmpfilename.c_str(), (char *) errmsg)) {
	    cerr << "write failed - " << errmsg << endl ;
	    new TGMsgBox (gClient->GetRoot(), fParent, "Error", errmsg,
			 kMBIconExclamation, kMBOk, 0);         
	    return kFALSE;
	 }
      }
      // The temp file has been written.
      // If a backup is needed, link(filename.c_str(), <backup name>) at this
      // point.  rename() won't remove the link if this is done...
      if (rename(tmpfilename.c_str(), filename.c_str()))
      {
	 cerr << "rename() failed, " << strerror(errno) << endl ;
      }
      fFilterFile.updateFileStat(filename.c_str()) ;

      // update file selection box
      fCurDir = gSystem->DirName (filename.c_str());
      fCurFile = gSystem->BaseName (filename.c_str());

      UpdateFileSelection();
      SetDirty (kFALSE);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::LoadCoeff ()
   {
      if (ReadOnly()) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Unable to load filter coefficients in readonly "
                      "mode.", kMBIconExclamation, kMBOk, 0);         
         // return kFALSE; //JCB No need, it'll return kFALSE anyway.
      }
      return kFALSE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::SetGain (double f, double g, 
                     sign_type sign)
   {
      Bool_t retval = kTRUE ;
      if (!fCurModule || ReadOnly() || (fCurSections.size() != 1) ||
         !Update (kFALSE)) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Unable to set filter gain.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      FilterSection& sect = (*fCurModule)[*fCurSections.begin()];
   
      // make filter and get gain
      FilterDesign ds (fCurModule->getFSample());
      fComplex coeff;
      if (!ds.filter (sect.getDesign()) || !ds.Xfer (coeff, f)) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Unable to set filter gain.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      // adjust gain
      double gain_adj = 1;
      if (coeff.Mag() > 1E-12) gain_adj = fabs (g) / coeff.Mag();
      if (gain_adj < 1E-12) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Unable to set filter gain.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      // adjust sign
      int pos = true;
      switch (sign) {
         case kSignSame:
            {
               break;
            }
         case kSignMinus:
            pos = false;
            // no break!
         case kSignPlus:
            {
               bool coeff_pos = (coeff.Real() != 0) ? 
                  (coeff.Real() > 0) : (coeff.Imag() >= 0);
               if (coeff_pos != pos) {
                  gain_adj *= -1;
               }
               break;
            }
      }
   
      // add gain
      if (fabs (gain_adj - 1.0) < 1E-12) {
         return kTRUE;
      }
      string cmd = sect.getDesign();
      char buf[256];
      sprintf (buf, "gain(%g)", gain_adj);
      cmd += buf;
      sect.setDesign (cmd.c_str());
      // update and return
      retval = UpdateDesign (kTRUE, kTRUE);
      return retval ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::SelectModule (const char* module)
   {
      Bool_t retval = kTRUE ;

      if (fSimple) {
         return kTRUE;
      }
      Update (kFALSE);
      if (!module) {
         fCurModule = 0;
         fModule->SetTopEntry 
            (new TGTextLBEntry (fModule, new TGString (""), 0), 
            new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                              kLHintsExpandX));
         fModule->MapSubwindows();
         Update (kTRUE, kTRUE);
         retval = kTRUE;
      }
      else if (fCurModule && (strcmp (fCurModule->getName(), module) == 0)) 
      {
         retval = kTRUE;
      }
      else if(!(fCurModule = fFilterFile.find (module)))
      {
	 fModule->SetTopEntry 
	    (new TGTextLBEntry (fModule, new TGString (""), 0), 
	    new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
			      kLHintsExpandX));
	 fModule->MapSubwindows();
	 Update();
	 retval = kFALSE;
      }
      else
      {
	 retval = Update();
      }
      return retval ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::SelectSections()
   {
      Bool_t retval = kTRUE ;

      if (fSimple) {
         return kTRUE;
      }
      UpdateSwitching (kFALSE);
      UpdateDesign(kFALSE);
      fCurSections.clear();
      for (int i = 0; i < kMaxFilterSections; ++i) {
         if (fSect[i]->GetState() == kButtonDown) {
            fCurSections.insert (i);
         }
      }
      retval = UpdateSwitching() && UpdateDesign();
      return retval ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::UpdateSections (Bool_t toGUI, Bool_t force)
   {
      if (fSimple) {
         return kTRUE;
      }
      // do we have a module selected?
      if (!fCurModule) {
         if (fCurSectionActive || force) {
            for (int i = 0; i < kMaxFilterSections; ++i) {
	       // clear the filter section names, set them non-editable.
               fSectName[i]->SetText ("");
               fSectName[i]->SetState (kFALSE);
            }
            fCurSectionActive = kFALSE;
         }
         return kTRUE;
      }
      // make active if inactive
      if (toGUI && (!fCurSectionActive || force)) {
         // This really enables the editing of the text field if
         // the file is Read/Write.  For Read-only, editing is disabled.
         for (int i = 0; i < kMaxFilterSections; ++i) {
            fSectName[i]->SetState (ReadOnly() ? kFALSE : kTRUE);
         }
         fCurSectionActive = kTRUE;
      }
      // transfer data
      if (toGUI) {
         for (int i = 0; i < kMaxFilterSections; ++i) {
            // Take the name of the filter from the fName field
            // and copy it to the filter section widget fSectName[i]
            fSectName[i]->SetText ((*fCurModule)[i].getName()); 
         }
      }
      else 
      {
         // This updates the filter by potentially changing it's name.
         for (int i = 0; i < kMaxFilterSections; ++i) {
            (*fCurModule)[i].setName(fSectName[i]->GetText ());
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::UpdateSwitchParams()
   {
      if (fSimple) {
         return kTRUE;
      }
      int sel = fSwitchOut->GetSelected();
      switch (sel) {
         // Immediately
         case 1:
            {
               fSwitchParam[0]->SetState (kFALSE);
               fSwitchParam[1]->SetState (kFALSE);
               fSwitchParam[2]->SetState (kFALSE);
               break;
            }
         // Ramp
         case 2:
            {
               fSwitchParam[0]->SetState 
                  (ReadOnly() ? kFALSE : kTRUE);
               fSwitchParam[1]->SetState (kFALSE);
               fSwitchParam[2]->SetState (kFALSE);
               break;
            }
         // Input crossing
         case 3:
            {
               fSwitchParam[0]->SetState (kFALSE);
               fSwitchParam[1]->SetState 
                  (ReadOnly() ? kFALSE : kTRUE);
               fSwitchParam[2]->SetState 
                  (ReadOnly() ? kFALSE : kTRUE);
               break;
            }
         // Zero crossing
         case 4:
            {
               fSwitchParam[0]->SetState (kFALSE);
               fSwitchParam[1]->SetState 
                  (ReadOnly() ? kFALSE : kTRUE);
               fSwitchParam[2]->SetState 
                  (ReadOnly() ? kFALSE : kTRUE);
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::UpdateSwitching (Bool_t toGUI, Bool_t force)
   {
      if (fSimple) {
         return kTRUE;
      }
      // do we have multiple sections selected?
      if ((fCurSections.size() != 1) || !fCurModule) {
         if (fCurSwitchActive || force) {
            fSwitchInp->RemoveEntries (0, 10000);
            fSwitchOut->RemoveEntries (0, 10000);
            fSwitchInp->SetTopEntry 
               (new TGTextLBEntry (fSwitchInp, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fSwitchInp->MapSubwindows();
            fSwitchOut->SetTopEntry 
               (new TGTextLBEntry (fSwitchOut, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fSwitchOut->MapSubwindows();
            fSwitchInp->SetState (kFALSE);
            fSwitchOut->SetState (kFALSE);
            for (int i = 0; i < 3; ++i) {
               fSwitchParam[0]->SetState (kFALSE);
            }
            fCurSwitchActive = kFALSE;
         }
         return kTRUE;
      }
      // make active if inactive
      if (toGUI && !fCurSwitchActive) 
      {
         fSwitchInp->AddEntry ("Always On", kAlwaysOn);
         fSwitchInp->AddEntry ("Zero History", kZeroHistory);
         fSwitchOut->AddEntry ("Immediately", 1);
         fSwitchOut->AddEntry ("Ramp", 2);
         fSwitchOut->AddEntry ("Input Crossing", 3);
         fSwitchOut->AddEntry ("Zero Crossing", 4);
         fCurSwitchActive = kTRUE;
      }
      // transfer data
      FilterSection& sect = (*fCurModule)[*fCurSections.begin()];
      if (toGUI) 
      {
         // Transfer settings from the filter section to the GUI
         fSwitchInp->Select ((int)sect.getInputSwitch());
         fSwitchOut->Select ((int)sect.getOutputSwitch());
         fSwitchParam[0]->SetNumber (sect.getRamp());
         fSwitchParam[1]->SetNumber (sect.getTolerance());
         fSwitchParam[2]->SetNumber (sect.getTimeout());
         fSwitchInp->SetState (ReadOnly() ? kFALSE : kTRUE);
         fSwitchOut->SetState (ReadOnly() ? kFALSE : kTRUE);
         UpdateSwitchParams();
      }
      else 
      {
         // Transfer the switching data from the GUI to the
         // filter section.  This potentially modifies the filter.
         sect.setInputSwitch ((input_switching)
                             fSwitchInp->GetSelected());
         sect.setOutputSwitch ((output_switching)
                              fSwitchOut->GetSelected());
         sect.setRamp (fSwitchParam[0]->GetNumber());
         sect.setTolerance (fSwitchParam[1]->GetNumber());
         sect.setTimeout (fSwitchParam[2]->GetNumber());
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::UpdateDesign (Bool_t toGUI,
                     Bool_t force)
   {
      // The sample rate can only be changed if this is called from
      // diaggui.  If called from foton, the rate is constant read
      // from the file.
      fSample->SetEnabled(fSimple) ; // Can change if fSimple TRUE.

      // do we have a module selected?
      if (!fCurModule) {
         if (fCurDesignActive || force) {
            fCommand->LoadBuffer (" ");
            fCommand->SetState (kFALSE);
            fAlt->LoadBuffer (" ");
            fSetGainVal->SetState (kFALSE);
            fSetGainUnit->SetState (kFALSE);
            fSetGainSign->SetState (kButtonDisabled);
            fSetGain->SetState (kButtonDisabled);
            fCurDesignActive = kFALSE;
         }
         return kTRUE;
      }
      // There is a current module.
      // check active
      if ((fCurSections.size() == 1) && (!fCurDesignActive || force)) 
      {
         // Set the state of the GUI widgets for single selected section.
         fCommand->SetState (ReadOnly() ? kFALSE : kTRUE);
         fSetGainVal->SetState (ReadOnly() ? kFALSE : kTRUE);
         fSetGainUnit->SetState (ReadOnly() ? kFALSE : kTRUE);
         fSetGainSign->SetState (ReadOnly() ? kButtonDisabled : kButtonUp);
         fSetGain->SetState (ReadOnly() ? kButtonDisabled : kButtonUp);
         fCurDesignActive = kTRUE;
      }
      if ((fCurSections.size() != 1) && (fCurDesignActive || force)) 
      {
         fCommand->Clear();
         // Set the state of the GUI widgets for multiple selected sections.
         fCommand->LoadBuffer (" ");
         fCommand->SetState (kFALSE);
         fAlt->LoadBuffer (" ");
         fSetGainVal->SetState (kFALSE);
         fSetGainUnit->SetState (kFALSE);
         fSetGainSign->SetState (kButtonDisabled);
         fSetGain->SetState (kButtonDisabled);
         fCurDesignActive = kFALSE;
      }
      // transfer data
      if (toGUI) 
      {
         // This updates the Design GUI widget with the concatenated
         // designs of all the selected filter sections.  Does not
         // modify the filter file.
	 // First, set the sample rate combo box with the module's sample rate.
	 {
	    // If any sample rate is allowed, the fSample menu may not contain
	    // the entry for the sample rate.  Search for it, if it's not found
	    // add it to the menu before selection.
	    char s[16] ;
	    sprintf(s, "%d", (int) fCurModule->getFSample()) ;
	    if (!(fSample->FindEntry(s)))
	    {
	       fSample->AddEntry(s, (int) fCurModule->getFSample()) ;
	    }
	 }
	 fSample->Select( (int) fCurModule->getFSample()) ;
         string design;
         for (section_sel::iterator i = fCurSections.begin(); 
             i != fCurSections.end(); ++i) {
            const char* d = (*fCurModule)[*i].getDesign();
            if (d && *d && !design.empty()) design += "*";
            if (d && *d) design += d;
         }
         design = FilterSection::splitCmd (design.c_str());
         if (design.empty()) design = " ";
	 if (my_debug) cerr << "FilterWizWindow::UpdateDesign() design = " << design << endl ;
         fCommand->LoadBuffer (design.c_str());
         UpdateDesignZP();
      }
      else if (fCurSections.size() == 1) 
      {
         // Transfer the data from the Design GUI widget to the
         // fDesign string of the filter section if a single
         // section is selected. Potentially modifies the filter section.
	 if (fSimple)
	 {
	    // Only allow the module sample rate to be changed if called
	    // from diaggui.  Otherwise, it's a constant from the file.
	    fCurModule->setFSample( (double) fSample->GetSelected()) ;
	    if (my_debug) cerr << "fCurModule->setFSample(" << fSample->GetSelected() << ")\n" ;
	 }
         if (fCurSections.size() == 1) 
	 {
            string cmd = fCommand->GetString();
            (*fCurModule)[*fCurSections.begin()].setDesign (cmd.c_str());
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Int_t TLGFilterWizWindow::UpdateDesignZP()
   {
      double fsample ;
      
      // Get the sample rate from either the fSample ComboBox or 
      // from the filter file, depending on how the program was started.
      if (fSimple)
	 fsample = (double) fSample->GetSelected() ;
      else if (fCurModule)
	 fsample = fCurModule->getFSample() ;
      else
	 fsample = fFilterFile.getFSample() ; // Should be the same.
      if (my_debug) cerr << "UpdateDesignZP(): fsample = " << fsample << endl ;
      
      string cmd = fCommand->GetString();
      string plane = "s";
      string format = "s";
      string alt;
      bool splane = true;
      // s-Plane radio buttons.
      if (fSZFormat[0]->GetState() == kButtonDown) {
         plane = "s";	// s (rad/s)
      }
      else if (fSZFormat[1]->GetState() == kButtonDown) {
         plane = "f";	// f (Hz)
      }
      else if (fSZFormat[2]->GetState() == kButtonDown) {
         plane = "n";	// n (Hz/norm)
      }
      else if (fSZFormat[3]->GetState() == kButtonDown) {
         plane = "p";	// Polynomial
      }
      // z-Plane radio buttons.
      else if (fSZFormat[4]->GetState() == kButtonDown) {
         format = "s";	// SOS
         splane = false;
      }
      else if (fSZFormat[5]->GetState() == kButtonDown) {
         format = "r";	// Roots
         splane = false;
      }
      else if (fSZFormat[6]->GetState() == kButtonDown) {
         format = "d";	// Direct
         splane = false;
      }
      else if (fSZFormat[7]->GetState() == kButtonDown) {
         format = "o";	// Online
         splane = false;
      }
      else {
         fSetGainCurrent->SetText ("");
         return 0;
      }
      // make a filter
      FilterDesign ds (fsample);
      if (!ds.filter (cmd.c_str())) {
         fAlt->LoadBuffer ("Error");
         fSetGainCurrent->SetText ("");
         return 0;
      }

      // Return the count of sos as a convenience.
      int soscount = iirsoscount(ds.get()) ;
      if (my_debug) cerr << "UpdateDesignZP() - soscount is " << soscount << endl ;
      // set alternate form
      if (splane) {
         if (!iir2zpk (ds(), alt, plane.c_str())) {
            fAlt->LoadBuffer ("Error");
            fSetGainCurrent->SetText ("");
            return 0;
         }
         else {
            alt = FilterSection::splitCmd (alt.c_str());
            if (alt.empty()) alt = " ";
	    if (!fGain1Allowed) // JCB
	    {
	       // This eliminates gain(1) filters.
	       if ((alt == "zpk([],[],1)") || (alt == "zpk([],[],1,\"f\")") ||
		  (alt == "zpk([],[],1,\"n\")")) {
		  alt = " ";
	       }
	    }
            fAlt->LoadBuffer (alt.c_str());
         }
      }
      else {
         if (!iir2z (ds(), alt, format.c_str())) {
            fAlt->LoadBuffer ("Error");
            fSetGainCurrent->SetText ("");
            return 0;
         }
         else {
            alt = FilterSection::splitCmd (alt.c_str());
            if (alt.empty()) alt = " ";
	    if (!fGain1Allowed) // JCB
	    {
	       // This eliminates gain(1) filters.
	       if ((alt == "zroots([],[],1)") || (alt == "sos(1,[])") ||
		  (alt == "sos(1,[],\"o\")")) {
		  alt = " ";
	       }
	    }
            fAlt->LoadBuffer (alt.c_str());
         }
      }
      // set current gain value
      char buf[256];
      fComplex coeff;
      double f = fSetGainF->GetNumber();
      int unit = fSetGainCurrentUnit->GetSelected();
      if (ds.Xfer (coeff, f)) {
         double n1 = 0;
         double n2 = 0;
         switch (unit) {
            // dB/deg
            case 0:
               {
                  n1 = 20. * log10 (coeff.Mag());
                  n2 = 180. / pi * coeff.Arg();
                  break;
               }
            // mag/deg
            case 1:
               {
                  n1 = coeff.Mag();
                  n2 = 180. / pi * coeff.Arg();
                  break;
               }
            // Real/Imag
            case 2:
               {
                  n1 = coeff.Real();
                  n2 = coeff.Imag();
                  break;
               }
            // dB/rad
            case 3:
               {
                  n1 = 20. * log10 (coeff.Mag());
                  n2 = coeff.Arg();
                  break;
               }
            // mag/rad
            case 4:
               {
                  n1 = coeff.Mag();
                  n2 = coeff.Arg();
                  break;
               }
         }
         sprintf (buf, "%g  %g", n1, n2);
      }
      else {
         strcpy (buf, "");
      }
      fSetGainCurrent->SetText (buf);
   
      return soscount;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::Update (Bool_t toGUI, Bool_t force)
   {
      Bool_t retval = kTRUE ;
      retval = UpdateSections (toGUI, force) && 
         UpdateSwitching (toGUI, force) && 
         UpdateDesign (toGUI, force);
      return retval ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::UpdateFilter()
   {
      if (!fSimple || !fFilter) {
         return kFALSE;
      }
      Update (kFALSE);
      *fFilter = (*fCurModule)[0].getDesign();
      return kTRUE;
   }

//______________________________________________________________________________
   PlotDescriptor* TLGFilterWizWindow::PlotBode ()
   {
      PlotDescriptor* const retNull(0);

      if (!fCurModule || !Update (kFALSE)) {
	 if (!fCurModule)
	 if (!Update (kFALSE))
         return retNull;
      }
      double fsample ;
      if (fSimple)
	 fsample = (double) fSample->GetSelected() ;
      else if (fCurModule)
	 fsample = fCurModule->getFSample() ; 
      else
	 fsample = fFilterFile.getFSample() ;

      string cmd = fCommand->GetString();
      // get sweep parameters
      double start = fPlotParam[0]->GetNumber();
      double stop = fPlotParam[1]->GetNumber();
      int points = fPlotParam[2]->GetIntNumber();
      string sweeptype = "log";
      if (fPlotType->GetSelected() == 1) {
         sweeptype = "linear";
      }
      if (points < 1) points = 1;
      if (stop < start) swap (start, stop);
      if (stop <= 0) {
         return retNull;
      }
      if (start < 0) start = 0;
      if ((start == 0) && (sweeptype == "log")) {
         start = 1E-3;
      }
      // add filter
      FilterDesign ds (fsample);
      if (!ds.filter (cmd.c_str())) {
         return retNull;
      }
      if (fClosedLoop) {
         if (!ds.closeloop (fClosedLoopNeg ? -1 : +1)) {
            return retNull;
         }
      }
      // Compute Xfer
      DataDescriptor* data = new DataCopy (0, 0, points, true);
      if (!ds.Xfer (data->GetX(), (fComplex*)data->GetY(), 
                   start, stop, points, sweeptype.c_str())) {
         delete data;
         return retNull;
      }
      // Make plot descriptor
      string name = fCurModule->getName();
      if (fCurSections.size() == 1) {
         char buf[16];
         sprintf (buf, "%i", *fCurSections.begin());
         name += buf;
      }
      else if ((int)fCurSections.size() != kMaxFilterSections) {
         for (section_sel::iterator i = fCurSections.begin(); 
             i != fCurSections.end(); ++i) {
            char buf[16];
            sprintf (buf, "_%i", *i);
            name += buf;
         }   
      }
      string achn = name + "_in";
      string bchn = name + "_out";
      ParameterDescriptor param;
      Time now (Now().getS(), 0);
      param.SetStartTime (now.getS(), now.getN());
      param.SetThird (fCurFile);
      calibration::Descriptor caldesc (now, kPTTransferFunction, 
                           achn.c_str(), bchn.c_str());
      PlotDescriptor* pd = 
         new PlotDescriptor (data, kPTTransferFunction, achn.c_str(), 
                           bchn.c_str(), &param, &caldesc);
   
      return pd;
   }

//______________________________________________________________________________
   PlotDescriptor* TLGFilterWizWindow::PlotResponse (const char* resp)
   {
      PlotDescriptor* const retNull(0);
      double fsample ;

      if (!fCurModule || !Update (kFALSE)) {
         return retNull;
      }
      // get sample & cmd
      if (fSimple)
	 fsample = (double) fSample->GetSelected() ;
      else if (fCurModule)
	 fsample = fCurModule->getFSample() ; 
      else
	 fsample = fFilterFile.getFSample() ;

      string cmd = fCommand->GetString();

      // get time duration
      double duration = fPlotParam[3]->GetNumber();
      if (duration < 1. / fsample) duration = 1.0;
   
      // add filter
      FilterDesign ds (fsample);
      if (!ds.filter (cmd.c_str())) {
         return retNull;
      }
      if (fClosedLoop) {
         if (!ds.closeloop (fClosedLoopNeg ? -1 : +1)) {
            return retNull;
         }
      }
   
      // Compute response
      TSeries out (Time (0,0), Interval (0), 1);
      if (!ds.response (out, resp, Interval (duration))) {
         return retNull;
      }
      int points = out.getNSample();
      DataDescriptor* data = new DataCopy (0., out.getTStep(), 0, points);
      out.getData (points, data->GetY());
      // Make plot descriptor
      string name = fCurModule->getName();
      if (fCurSections.size() == 1) {
         char buf[16];
         sprintf (buf, "%i", *fCurSections.begin());
         name += buf;
      }
      else if ((int)fCurSections.size() != kMaxFilterSections) {
         for (section_sel::iterator i = fCurSections.begin(); 
             i != fCurSections.end(); ++i) {
            char buf[16];
            sprintf (buf, "_%i", *i);
            name += buf;
         }   
      }
      name += string ("_") + resp;
      ParameterDescriptor param;
      //Time now (Now().getS(), 0);
      Time now;
      param.SetStartTime (now.getS(), now.getN());
      param.SetThird (fCurFile);
      calibration::Descriptor caldesc (now, kPTTimeSeries, 
                           name.c_str());
      PlotDescriptor* pd = 
         new PlotDescriptor (data, kPTTimeSeries, name.c_str(), 
                           0, &param, &caldesc);
   
      return pd;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::PlotRoots (PlotDescriptor*& poles,
                     PlotDescriptor*& zeros, bool splane)
   {
      double fsample ;

      if (!fCurModule || !Update (kFALSE)) {
         return 0;
      }
      // get sample & cmd
      if (fSimple)
	 fsample = (double) fSample->GetSelected() ;
      else if (fCurModule)
	 fsample = fCurModule->getFSample() ; 
      else
	 fsample = fFilterFile.getFSample() ;

      string cmd = fCommand->GetString();

      // add filter
      FilterDesign ds (fsample);
      if (!ds.filter (cmd.c_str())) {
         return kFALSE;
      }
      if (fClosedLoop) {
         if (!ds.closeloop (fClosedLoopNeg ? -1 : +1)) {
            return kFALSE;
         }
      }
      // get poles and zeros
      int order = iirorder (ds.get());
      dComplex* p = new dComplex [order + 1];
      dComplex* z = new dComplex [order + 1];
      int npoles;
      int nzeros;
      double g;
      if ((splane && !iir2zpk (ds.get(), nzeros, z, npoles, p, g, "f")) ||
         (!splane && !iir2z (ds.get(), nzeros, z, npoles, p, g))) {
         delete [] p;
         delete [] z;
         return kFALSE;
      }
      // make data descriptors
      DataDescriptor* p_data = new DataCopy (0, 0, npoles);
      for (int i = 0; i < npoles; ++i) {
         p_data->GetX()[i] = p[i].Real();
         p_data->GetY()[i] = p[i].Imag();
      }
      DataDescriptor* z_data = new DataCopy (0, 0, nzeros);
      for (int i = 0; i < nzeros; ++i) {
         z_data->GetX()[i] = z[i].Real();
         z_data->GetY()[i] = z[i].Imag();
      }
      delete [] p;
      delete [] z;
      // create XY plot descriptor
      string name = fCurModule->getName();
      if (fCurSections.size() == 1) {
         char buf[16];
         sprintf (buf, "%i", *fCurSections.begin());
         name += buf;
      }
      else if ((int)fCurSections.size() != kMaxFilterSections) {
         for (section_sel::iterator i = fCurSections.begin(); 
             i != fCurSections.end(); ++i) {
            char buf[16];
            sprintf (buf, "_%i", *i);
            name += buf;
         }   
      }
      name += string ("_");
      ParameterDescriptor param;
      Time now (Now().getS(), 0);
      param.SetStartTime (now.getS(), now.getN());
      param.SetThird (fCurFile);
      string a = name + "poles_real";
      string b = name + "poles_imag";
      calibration::Descriptor p_caldesc (now, kPTXY, 
                           a.c_str(), b.c_str());
      if (splane) {
         p_caldesc.SetDomain (kCalUnitX, kCalDomainFrequency);
         p_caldesc.SetDomain (kCalUnitY, kCalDomainFrequency);
      }
      poles = new PlotDescriptor (p_data, kPTXY, a.c_str(), 
                           b.c_str(), &param, &p_caldesc);
      a = name + "zeros_real";
      b = name + "zeros_imag";
      calibration::Descriptor z_caldesc (now, kPTXY, 
                           a.c_str(), b.c_str());
      if (splane) {
         z_caldesc.SetDomain (kCalUnitX, kCalDomainFrequency);
         z_caldesc.SetDomain (kCalUnitY, kCalDomainFrequency);
      }
      zeros = new PlotDescriptor (z_data, kPTXY, a.c_str(), 
                           b.c_str(), &param, &z_caldesc);
      return kTRUE;
   }

//______________________________________________________________________________
   PlotDescriptor* TLGFilterWizWindow::UnitCircle (int segments)
   {
      // make data descriptors
      int n = segments + 1;
      DataDescriptor* data = new DataCopy (0, 0, n);
      for (int i = 0; i < segments + 1; ++i) {
         dComplex z;
         z.setMArg (1., 2. * pi * (double)i / (double)segments);
         data->GetX()[i] = z.Real();
         data->GetY()[i] = z.Imag();
      }
      // create XY plot descriptor
      string name = "unit_circle_";
      ParameterDescriptor param;
      Time now(0);
      param.SetStartTime (0, 0);
      param.SetThird ("Unit Circle");
      string a = name + "real";
      string b = name + "imag";
      calibration::Descriptor p_caldesc (now, kPTXY, 
                           a.c_str(), b.c_str());
      PlotDescriptor* pd = 
         new PlotDescriptor (data, kPTXY, a.c_str(), 
                           b.c_str(), &param, &p_caldesc);
      return pd;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::AddFilter (Long_t parm1)
   {
      double fsample ;
      if (fSimple)
	 fsample = (double) fSample->GetSelected() ;
      else if (fCurModule)
	 fsample = fCurModule->getFSample() ; 
      else
	 fsample = fFilterFile.getFSample() ;

      if (my_debug) cerr << "TLGFilterWizWindow::AddFilter() - fsample = " << fsample << endl ;
      // check for readonly
      if (ReadOnly()) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Cannot add filters in readonly mode.", 
                      kMBIconExclamation, kMBOk, 0);
         return kFALSE;
      }
      // check that a filter module is selected
      if (!fCurModule) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Need to have a module selected for "
                      "adding a filter.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      // Check only one section is selcted
      if (fCurSections.size() != 1) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Exactly one filter section has to be "
                      "selected for adding a filter.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      // check that we have a valid design
      if (!UpdateDesign (kFALSE) ||
         (!(*fCurModule)[*fCurSections.begin()].update())) {
         new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                      "Must start with a valid filter.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      // get marked string
      TString cmd;
      if (fCommand->IsMarked()) {
         fCommand->Copy();
         TGText* clip = fCommand->GetClipboard();
         for (int i = 0; i < clip->RowCount(); ++i) {
            char* p = clip->GetLine 
               (TGLongPosition (0, i), clip->ColCount());
            if (p) {
               if (cmd != "") cmd += "\n";
               cmd += p;
            }
            delete [] p;
         }
         // check marked string
         FilterDesign ds (fCurModule->getFSample());
         if (!ds.filter (cmd)) {
            new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                         "Selection must be a valid filter.", 
                         kMBIconExclamation, kMBOk, 0);         
            return kFALSE;
         }
      }
   
      // Bring up corresponding dialog box
      TLGNotchDialog::subtype_t sub1 = TLGNotchDialog::kNotch;
      TLGEllipDialog::subtype_t sub2 = TLGEllipDialog::kEllip;
      switch (parm1) {
         // Gain factor
         case kAddGainId:
            {
               new TLGGainDialog (gClient->GetRoot(), fParent, cmd);
               break;
            }
         // Poles and zeros
         case kAddZpkId:
         case kAddZRootId:
            {
               new TLGZpkDialog (gClient->GetRoot(), fParent, fsample,
                                cmd, parm1 == kAddZpkId);
               break;
            }
         // Polynomial forms
         case kAddPolyId:
         case kAddDirectId:
            {
               new TLGPolyDialog (gClient->GetRoot(), fParent, 
                                 cmd, fsample, parm1 == kAddPolyId);
               break;
            }
         // Notch, resonant gain and comb
         case kAddCombId:
            sub1 = TLGNotchDialog::kComb;
         // no break;
         case kAddResGainId:
            if (sub1 == TLGNotchDialog::kNotch)
               sub1 = TLGNotchDialog::kResGain;
         // no break;
         case kAddNotchId: 
            {
               new TLGNotchDialog (gClient->GetRoot(), fParent, 
                                  cmd, sub1);
               break;
            }
         // Elliptic, butterworth, chebyshev
         case kAddButterId:
            sub2 = TLGEllipDialog::kButter;
         // no break;
         case kAddCheby1Id:
            if (sub2 == TLGEllipDialog::kEllip)
               sub2 = TLGEllipDialog::kCheby1;
         // no break;
         case kAddCheby2Id:
            if (sub2 == TLGEllipDialog::kEllip)
               sub2 = TLGEllipDialog::kCheby2;
         // no break;
         case kAddEllipId:
            {
	       if (my_debug) cerr << "TLGFilterWizWindow::AddFilter() - Creating TLGEllipDialog, line " << __LINE__ << " File " << __FILE__ << endl ;
               new TLGEllipDialog (gClient->GetRoot(), fParent, 
                                  cmd, sub2);
	       if (my_debug) cerr << "TLGFilterWizWindow::AddFilter() - Returned from TLGEllipDialog, line " << __LINE__ << " File " << __FILE__ << endl ;
               break;
            }
         // SOS
         case kAddSosId:
            {
               new TLGSosDialog (gClient->GetRoot(), fParent, 
                                fsample, cmd);
               break;
            }
         // Import design string from file. - JCB
         case kImportId:
            {
	       if (!fSimple)
	       {
		  new TLGImportDialog(gClient->GetRoot(), fParent, cmd, fFc->GetDirectory()) ;
		  if (my_debug) cerr << "Import: " << cmd << endl ;
	       }
                break ;
            }
         // ???
         default:
            {
               new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                            "Unknown filter.", 
                            kMBIconExclamation, kMBOk, 0);         
               break;
            }
      }
      // cancel?
      if (cmd == "") {
         return kTRUE;
      }
      if (my_debug) cerr << "TLGFilterWizWindow::AddFilter() cmd = " << cmd << endl ;
      SetDirty();
      // cut out marked text and replace
      if (fCommand->IsMarked()) {
         TGText text (cmd);
         fCommand->Delete();
         fCommand->InsertText (&text, kTRUE);
         if (!UpdateDesign (kFALSE) || 
            !(*fCurModule)[*fCurSections.begin()].update()) {
            new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                         "Unable to add filter:\n" + cmd, 
                         kMBIconExclamation, kMBOk, 0); 
            return kFALSE;        
         }
         UpdateDesignZP();
      }
      // otherwise add at the end
      else 
      {
	 if (my_debug) cerr << "TLGFilterWizWindow::AddFilter() - Add the filter." << endl ;
         if (!(*fCurModule)[*fCurSections.begin()].add (cmd)) {
            new TGMsgBox (gClient->GetRoot(), fParent, "Error", 
                         "Unable to add filter:\n" + cmd, 
                         kMBIconExclamation, kMBOk, 0); 
            return kFALSE;        
         }
	 if (my_debug) cerr << "TLGFilterWizWindow::AddFilter() - Update Design." << endl ;
         UpdateDesign();
      }
      // Warn the user if there's more than 10 SOS at this point.
      int iircount = iirsoscount((*fCurModule)[*fCurSections.begin()].filter().get()) ;
      if (iircount > 10)
      {
	 new TGMsgBox(gClient->GetRoot(), fParent, "Warning",
		     "Filter has more than 10 SOS", kMBIconExclamation, kMBOk, 0) ;
      }
   
      return kTRUE;
   }


//______________________________________________________________________________
   Bool_t TLGFilterWizWindow::ProcessMessage (Long_t msg, Long_t parm1, Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) && (GET_SUBMSG (msg) == kCM_BUTTON)) 
      {
         switch (parm1) 
	 {
            // Directory up
            case kDirUpId:
               {
                  // Note that if the current file has been changed, a dialog box
                  // will be presented asking if you want to save the file.
                  // CheckDirty() will call SaveFile() if the user OK's the action
                  if (!fSimple && CheckDirty()) {
                     fFc->ChangeDirectory ("..");
                     fPath->Update (fFc->GetDirectory());
                     Setup (fFc->GetDirectory());
                  }
		  // Evaluate the section button states. - JCB
		  SetSectButtonStates() ;
                  break;     
               }
            // Select all sections
            case kSectSelAllId:
               {
                  if (!fSimple) {
                     fSectMul->Select (1);
                     for (int i = 0; i < kMaxFilterSections; ++i) {
                        fSect[i]->SetState (kButtonDown);
                     }
                     SelectSections();
                     // Evaluate the section button states. - JCB
                     SetSectButtonStates() ;
                  }
                  break;
               }
            // Deselect all sections
            case kSectSelNoneId:
               {
                  if (!fSimple) {
                     fSectMul->Select (0);
                     for (int i = 0; i < kMaxFilterSections; ++i) {
                        fSect[i]->SetState (kButtonUp);
                     }
                     SelectSections();
                     // Evaluate the section button states. - JCB
                     SetSectButtonStates() ;
                  }
                  break;
               }
           // Copy sections - JCB
            case kSelCopyId:
                {
                    if (!fSimple)
                    {
                        fSectCopyList.clear() ; // Get rid of the old copy set.
                        // The fCurSections set should have the currently selected filter section indicies in it.
                        for (section_sel::iterator i = fCurSections.begin() ;
                                i != fCurSections.end() ; i++)
                        {
                            fSectCopyList.push_back(SectCopy(&((*fCurModule)[*i]))) ;
                            // Check to see if only one should be selected.
                            if (fSectMul->GetSelected() == 0)
                            {
                                // Set the index of the SectCopy to -1 to indicate it can be pasted anywhere.
                                fSectCopyList.begin()->SetIndex(-1) ;
                                break ;  // Leave, we only want one so there's no need to continue looking.
                            }
                        }
                        // Evaluate the section button states.
                        SetSectButtonStates() ;
                    }
                    break ;
                }
            // Cut sections - JCB
            case kSelCutId:
                {
                    if (!fSimple)
                    {
                       fSectCopyList.clear() ; // Get rid of the old copy set.
                       // The fCurSections set should have the currently selected filter setiobn indicies in it.
                       // Enable the paste button and undo button unless the section_sel set is empty.
                       if (!fCurSections.empty())
                       {
                           // Save the current set of sections for undo.
                           (*fCurModule).SaveSections() ;

                           for (section_sel::iterator i = fCurSections.begin() ;
                                   i != fCurSections.end() ; i++)
                           {
                               fSectCopyList.push_back(SectCopy(&((*fCurModule)[*i]))) ;

                               // Now, clear the design of the filter section to "cut" it.
                               const char *empty_str = "" ;
                               (*fCurModule)[*i].setName(empty_str) ;
                               (*fCurModule)[*i].setDesign(empty_str) ;

                               // Check to see if only one should be selected.
                               if (fSectMul->GetSelected() == 0)
                               {
                                   // Set the index of the SectCopy to -1 to indicate it can be pasted anywhere.
                                   fSectCopyList.begin()->SetIndex(-1) ;
                                   break ; // Leave, there's only one section.
                               }
                           }
                           // Update the filter sections and display.
                           Update(kTRUE, kTRUE) ;

                           // Evaluate the section button states.
                           SetSectButtonStates() ;
                       }
                    }

                    break ;
                }
            // Revert sections (undo)- JCB
            case kSelRevertId:
                {
                    if (!fSimple)
                    {
                        if (!(*fCurModule).RestoreSections())
                        {
                            // Pop up a dialog to tell the user it didn't work.
                            new TGMsgBox (gClient->GetRoot(), fParent, "Error",
                                        "Restore Filter Sections failed",
                                        kMBIconExclamation, kMBOk, 0) ;
                        }
                        Update(kTRUE, kTRUE) ;

                        // Evaluate the section button states.
                        SetSectButtonStates() ;
                    }
                    break ;
                }
            // Paste sections - JCB
            case kSelPasteId:
                {
                    if (!fSimple)
                    {
                        // Don't do anything if the SectCopyList is empty, nothing to paste.
                        if (!fSectCopyList.empty())
                        {
                            // Save the current set of sections for undo.
                            (*fCurModule).SaveSections() ;

                            // See if the index of the first copied section is -1.  If it is, we want to paste
                            // the filter section in the selected section(s) of the module.  If not, we want to
                            // use the index to decide where to paste the sections.
                            vector<SectCopy>::iterator iter = fSectCopyList.begin() ;
                            if ((*iter).GetIndex() == -1)
                            {
                                if (fCurSections.empty())
                                {
                                    // Pop up some dialog box indicating that a section needs to be selected.
                                     new TGMsgBox (gClient->GetRoot(), fParent, "Error",
                                                  "Select destination for Paste",
                                                  kMBIconExclamation, kMBOk, 0);

                                }
                                else
                                {
                                    for (section_sel::iterator i = fCurSections.begin(); i != fCurSections.end() ; i++)
                                    {
                                        // Paste the filter section to this selected section, removing what was there.
                                        (*iter).PasteSection(&(*fCurModule)[*i]) ;

                                        // Update the filter.
                                        if (!(*fCurModule)[*i].update())
                                        {
                                            new TGMsgBox (gClient->GetRoot(), fParent, "Error",
                                                        "Design for filter is invalid",
                                                        kMBIconExclamation, kMBOk, 0) ;
                                        }
                                    }
                                    Update (kTRUE, kTRUE) ;
                                }
                            }
                            else // Multiple section paste
                            {
                                // For each section in SectCopy, paste it into the current module using the index.
                                for ( ; iter != fSectCopyList.end(); ++iter)
                                {
                                    int index = (*iter).GetIndex() ;

                                    // Paste the section
                                    (*iter).PasteSection(&(*fCurModule)[index]) ;

                                    // Update the filter.
                                    if (!(*fCurModule)[index].update())
                                    {
                                        new TGMsgBox (gClient->GetRoot(), fParent, "Error",
                                                    "Design for filter is invalid",
                                                    kMBIconExclamation, kMBOk, 0) ;
                                    }
                                }
                                Update(kTRUE, kTRUE) ;
                            }
                        }
                        // Evaluate the section button states.
                        SetSectButtonStates() ;
                    }
                    break ;
                }
            // Set gain sign
            case kSetGainSignId:
               {
                  if (fSetGainSign->GetString() == "=") {
                     fSetGainSign->SetText ("+");
                  }
                  else if (fSetGainSign->GetString() == "+") {
                     fSetGainSign->SetText ("-");
                  }
                  else {
                     fSetGainSign->SetText ("=");
                  }
                  break;
               }
            // Set gain
            case kSetGainId:
               {
                  double f = fSetGainF->GetNumber();
                  double g = fSetGainVal->GetNumber();
                  sign_type sign = kSignSame;
                  if (fSetGainSign->GetString() == "+") sign = kSignPlus;
                  if (fSetGainSign->GetString() == "-") sign = kSignMinus;
                  if (fSetGainUnit->GetSelected() == 1) {
                     g = power (10, g / 20.);
                  }
                  if (SetGain (f, fabs (g), sign)) {
                     SetDirty();
                  }
                  break;
               }
	    case kFileError:
	       {
		  if (!fFilterFile.errorsEmpty())
		     new TLGErrorDialog(gClient->GetRoot(), fParent, fFilterFile.getErrors(), TGString(fFilterFile.getFilename())) ;
	       }
	       break ;
         }

         // Add filter buttons
         if (((parm1 >= kAddId) && (parm1 < kAddId + 20)) || parm1 == kImportId) 
	 {

            AddFilter (parm1);
	    // Evaluate the section button states.
	    SetSectButtonStates() ;
         }
      }
      // Checkbox
      else if ((GET_MSG (msg) == kC_COMMAND) && (GET_SUBMSG (msg) == kCM_CHECKBUTTON)) 
      {
         // Sections
         if (!fSimple && (parm1 >= kSectId) && 
            (parm1 < kSectId + kMaxFilterSections)) {
            if (fSectMul->GetSelected() == 0) {
               for (int i = 0; i < kMaxFilterSections; ++i) {
                  fSect[i]->SetState (i == parm1 - kSectId ?
                                     kButtonDown : kButtonUp);
               }
            }
            SelectSections();
            // Evaluate the section button states. - JCB
            SetSectButtonStates() ;
         }
      }
      // Radio buttons
      else if ((GET_MSG (msg) == kC_COMMAND) && (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) 
      {
         // s/z-Plane format
         if ((parm1 >= kSZFormatId) && (parm1 < kSZFormatId + 8)) {
            for (int i = 0; i < 8; ++i) {
               fSZFormat[i]->SetState 
                  (i == parm1 - kSZFormatId ? kButtonDown: kButtonUp);
            }
            UpdateDesignZP();
         }
      }
      // Comboboxes
      else if ((GET_MSG (msg) == kC_COMMAND) && (GET_SUBMSG (msg) == kCM_COMBOBOX)) 
      {
         switch (parm1) 
	 {
            // Select path
            case kPathId:
               {
                  if (!fSimple) {
                     TGTreeLBEntry* e = (TGTreeLBEntry*) fPath->GetSelectedEntry();
                     // Note that if the current file has been changed, a dialog box
                     // will be presented asking if you want to save the file.
                     // CheckDirty() will call SaveFile() if the user OK's the action
                     if (e && CheckDirty()) {
                        fFc->ChangeDirectory (e->GetPath()->GetString());
                        fPath->Update (fFc->GetDirectory());
                        Setup (fFc->GetDirectory());
                     }
                     else {
                        UpdateFileSelection();
                     }
                     // Evaluate the section button states. - JCB
                     SetSectButtonStates() ;
                  }
                  break;
               }
            // Select file
            case kFileId:
               {
                  if (!fSimple) {
                     TGTextLBEntry* e = (TGTextLBEntry*)fFile->GetSelectedEntry();
                     // Note that if the current file has been changed, a dialog box
                     // will be presented asking if you want to save the file.
                     // CheckDirty() will call SaveFile() if the user OK's the action
                     if (e && CheckDirty()) {
                        ReadFile (e->GetText()->GetString());
			// Check to see if the file was read cleanly.  If not, warn the user.
			if (!fFilterFile.errorsEmpty())
			{
			   // Provide visual indication to user.
			   FileErrorIndication(kTRUE) ;
			   // List the errors for the user.
			   new TLGErrorDialog(gClient->GetRoot(), fParent, fFilterFile.getErrors(), TGString(e->GetText()->GetString())) ;
			}
			else
			{
			   // Clear the indication that the file has an error.
			   FileErrorIndication(kFALSE) ;
			}
                     }
                     else {
                        UpdateFileSelection();
                     }
                     // Evaluate the section button states. - JCB
                     SetSectButtonStates() ;
                  }
                  break;
               }
            // Select module
            case kModuleId:
               {
                  if (!fSimple) {
                     TGTextLBEntry* e = 
                        (TGTextLBEntry*)fModule->GetSelectedEntry();
                     if (e) {
                        SelectModule (e->GetText()->GetString());
                     }
                     // Evaluate the section button states.
                     SetSectButtonStates() ;
                  }
                  break;
               }
            // Multiple section select
            case kSectMulId:
               {
                  // if single, make sure only one is selected
                  if (!fSimple && fSectMul->GetSelected() == 0) {
                     bool firstup = false;
                     bool deselect = false;
                     for (int i = 0; i < kMaxFilterSections; ++i) {
                        if (firstup &&
                           (fSect[i]->GetState() == kButtonDown)) {
                           deselect = true;
                           fSect[i]->SetState (kButtonUp);
                        }
                        if (fSect[i]->GetState() == kButtonDown) {
                           firstup = true;
                        }
                     }
                     if (deselect) SelectSections();
                     // Evaluate the section button states.
                     SetSectButtonStates() ;
                  }
                  break;
               }
            // Input switching
            case kSwitchInpId:
               {
		  // JCB - Call UpdateSwitching to make sure changes
		  // made to the switching params get put in the 
		  // filter section. Otherwise, copy won't copy them.
		  UpdateSwitching(kFALSE) ;
                  SetDirty();
                  break;
               }
            // Output switching
            case kSwitchOutId:
               {
		  // JCB - Call UpdateSwitching to make sure changes
		  // made to the switching params get put in the 
		  // filter section. Otherwise, copy won't copy them.
		  UpdateSwitching(kFALSE) ;
                  UpdateSwitchParams();
                  SetDirty();
                  break;
               }
            // Set gain current unit
            case kSetGainCurrentUnitId:
               {
                  UpdateDesignZP();
                  break;
               }
	    // JCB
	    // Set sample rate.  This happens when fSimple is true.
	    // Also may happen if the file is "new" from the file->new menu pick.
	    case kSampleId:
	       {
		  if (fExperimentMode) 
		  {
		     int selected = fSample->GetSelected() ;
		     if (selected == -1000)
		     {
			char rate[256] ;
			int newRate = 0 ;
			// Open a dialog asking for a sample rate.
			new TLGInputDialog (gClient->GetRoot(), this,
					    "Enter Sample Rate", "Rate:  ",
					    rate) ;
			newRate = atoi(rate) ;
			if (newRate > 0 && newRate <= 65536)
			{
			   // Add the new rate to the menu if it isn't there already.
			   if (!(fSample->FindEntry(rate)))
			   {
			      fSample->AddEntry(rate, newRate) ;
			   }
			   fSample->Select(newRate) ;
			   fFilterFile.setFSample( (double) fSample->GetSelected()) ;
			   UpdateDesignZP();
			   SetDirty();
			}
		     }
		     else
		     {
			fFilterFile.setFSample( (double) fSample->GetSelected()) ;
			fFilterFile.setFSample( (double) fSample->GetSelected()) ;
			UpdateDesignZP();
			SetDirty();
		     }
		  }
		  else if (fSimple && fCurModule)
		  {
		     fCurModule->setFSample( (double) fSample->GetSelected()) ;
		     if (my_debug) cerr << "Sample rate changed to " << fCurModule->getFSample() << endl ;
		     UpdateDesignZP();
		     SetDirty();
		  }
		  else 
		  {
		     const char *name = fFilterFile.getFilename() ;
		     if (!name || strlen(name) == 0)
		     {
			fFilterFile.setFSample( (double) fSample->GetSelected()) ;
			if (my_debug) cerr << "Sample rate changed to " << fFilterFile.getFSample() << endl ;
			UpdateDesignZP();
			SetDirty();
		     }
		  }
		  break ;
	       }
         }
      }
      // File container double click
      else if ((GET_MSG (msg) == kC_CONTAINER) && (GET_SUBMSG (msg) == kCT_ITEMDBLCLICK)) 
      {
         // left mouse selects dir
         if (!fSimple && (parm1 == kButton1)) {
            if ((fFc->NumSelected() == 1) && CheckDirty()) {
               void* p = 0;
               TGFileItem* f = (TGFileItem *) fFc->GetNextSelected (&p);
               fFc->ChangeDirectory (f->GetItemName()->GetString());
               fPath->Update (fFc->GetDirectory());
               Setup (fFc->GetDirectory());
	       // Evaluate the section button states. - JCB
	       SetSectButtonStates() ;
            }
            else {
               UpdateFileSelection();
            }
         }
      }
      // numeric field updated
      else if ((GET_MSG (msg) == kC_TEXTENTRY) && (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) 
      {
         switch (parm1) 
	 {
            // sample
            case kSampleId:
               {
                  UpdateDesignZP();
                  SetDirty();
                  break;
               }
            // command text editor
            case kCommandId:
               {
		  // If there are more than 10 SOS and a single section is selected,
		  // warn the user.  
		  Int_t soscount = UpdateDesignZP() ;
		  if (soscount > 10 && fCurSections.size() == 1 && !fSimple)
		  {
		     new TGMsgBox(gClient->GetRoot(), fParent, "Warning",
			   "Filter has more than 10 SOS", kMBIconExclamation, kMBOk, 0) ;
		  }
                  SetDirty();
                  break;
               }
            // set gain frequency
            case kSetGainFId:
               {
                  UpdateDesignZP();
                  break;
               }
	    // JCB - Add event handler for switching parameters to make
	    // sure the parameters get put into the filter section.
	    // Otherwise, copy won't copy them.
	    case kSwitchParamId:
	    case kSwitchParamId+1:
	    case kSwitchParamId+2:
	       {
		  UpdateSwitching(kFALSE) ;
		  SetDirty() ;
		  break ;
	       }
         }
      }
      // text fields changed
      // JCB --- Unfortunately, this test ensures that every file read will be 
      // marked as dirty, because the SetText() methods generate this event. 
      // So the act of displaying the contents of a module (filling in the text
      // boxes) will generate the text changed event.  Later versions of ROOT
      // (later than 5.20) include an "emit" argument to SetText() that could
      // be used to suppress the event if it's not the user making the change.
      else if ((GET_MSG (msg) == kC_TEXTENTRY) &&
              (GET_SUBMSG (msg) == kTE_TEXTCHANGED)) 
      {
	 if (parm1 >= kSectNameId && parm1 < kSectNameId+10)
	 {
	    // Update the name of the filter section.
	    UpdateSections(kFALSE) ;
	 }
         SetDirty();
      }
      return kTRUE;
   }

//______________________________________________________________________________
// Evaluate the conditions to set the state of the copy/paste/cut/undo buttons
// in the Sections frame.
   void TLGFilterWizWindow::SetSectButtonStates()
   {
      // copy button.  If no current module, file, or no sections selected, the
      // copy button is disabled since there's nothing to select...
      // Cut rules are the same.  There has to be something to cut...
      if (fSectCP[0] && fSectCP[2])
      {
         // Start with the assumption the buttons should be disabled.
         fSectCP[0]->SetState(kButtonDisabled) ;
         fSectCP[2]->SetState(kButtonDisabled) ;

         if (fCurModule && !fCurSections.empty())
         {
            // Now check to see if the selected sections have anything in them.
            // If they are empty, there is nothing to cut or copy.

            for (section_sel::iterator i = fCurSections.begin(); i != fCurSections.end(); ++i)
            {
               const string s = (*fCurModule)[*i].refDesign() ;
               // If the section is not empty, there is something to cut or copy.
               if (!s.empty() && s.find_first_not_of(' ') != string::npos)
               {
                  // Enable the buttons
                  fSectCP[0]->SetState(kButtonUp) ;
                  fSectCP[2]->SetState(kButtonUp) ;
                  break ;
               }
            }
         }
      }
      // Paste button.  To be enabled, there needs to be something to paste, and
      // a place in which to paste it.
      if (fSectCP[1])
      {
         if (fCurModule && !fSectCopyList.empty())
         {
            // Look at the first entry in the copy list vector, if the index is -1,
            // it's the only one.
            if (fSectCopyList[0].GetIndex() == -1)
            {
               // Single section has been copied
               // See if a section has been checked as a target for pasting.
               if (fCurSections.empty())
               {
                  // No place to put the paste, disable the button.
                  fSectCP[1]->SetState(kButtonDisabled) ;
               }
               else
               {
                  fSectCP[1]->SetState(kButtonUp) ;
               }
            }
            else
            {
               // Multiple sections were copied, target for pasting is the index
               // number of the filter section so it's OK to paste.
               fSectCP[1]->SetState(kButtonUp) ;
            }
         }
         else
         {
            // Either there is no current module or nothing to paste.
            fSectCP[1]->SetState(kButtonDisabled) ;
         }
      }
      // Undo button.  To be enabled, there needs to be a current module and a
      // vector of sections in the fRevertSect.
      if (fSectCP[3])
      {
         if (fCurModule && !(fCurModule->RestoreSectionsEmpty()))
         {
            fSectCP[3]->SetState(kButtonUp) ;
         }
         else
         {
            fSectCP[3]->SetState(kButtonDisabled) ;
         }
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGFilterWizard                                                      //
//                                                                      //
// Filter wizard main window                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGFilterWizard::TLGFilterWizard (const TGWindow *p, bool& modal,
                     const char* title, std::string& fdesign,
                     const char* path, const char* file, 
		     const char *module, int section, bool any_sample,
		     bool experiment_mode)
   : TLGMainWindow (p, title), fModal (modal), fReturn (&modal),
   fFilter (&fdesign), fIniPath (path), fIniFile (file),
   fIniModule(module), fIniSection(section), fAnySample(any_sample),
   fExperimentMode(experiment_mode)
   {
   }

//______________________________________________________________________________
   TLGFilterWizard::~TLGFilterWizard ()
   {
      delete fFilterDesign;
      if (fMPad) delete fMPad;
      fMPad = 0;
      delete fMainTab;
      delete fFilterLayout;
   }

//______________________________________________________________________________
   void TLGFilterWizard::CloseWindow()
   {
      if (fModal) {
         DeleteWindow();
      }
      else {
         TLGMainWindow::CloseWindow();
      }
   }

//______________________________________________________________________________
   int TLGFilterWizard::AddButtons (TGCompositeFrame* p, TGButton* btns[], 
                     int max, TGLayoutHints* btnLayout,
                     GContext_t btnGC, FontStruct_t btnFont) 
   {
      int num = 0;
      TGButton* tmp_btn[max];

      if (fModal) {
         const char* const buttontext[] = 
            {"Bode Plot", "Step Response", "s-Plane Roots", "Ok", 
            "Cancel"};
         const char* const buttonhelp[] = 
            {"Plot the transfer function of the filter",
            "Plot the step response of the filter",
            "Plot the location of poles and zero in the s-plane",
            "Quit",
            "Exit without change"};
         const int buttonid[] = 
            {kB_BODEPLOT, kB_STEPPLOT, kB_ROOTPLOT, kB_OK, kB_CANCEL};

	 num = 5;
         for (int i = 0; i < num; i++) {
            tmp_btn[i] = new TGTextButton (p, buttontext[i], buttonid[i], 
                                 btnGC, btnFont);
            tmp_btn[i]->Associate (this);
            tmp_btn[i]->SetToolTipText (buttonhelp[i]);
            p->AddFrame (tmp_btn[i], btnLayout);
         }
      }
      else {
         const char* const buttontext[] = 
            {"Bode Plot", "Step Response", "s-Plane Roots", "Save", 
            "Load Coefficients"};
         const char* const buttonhelp[] = 
            {"Plot the transfer function of the filter",
            "Plot the step response of the filter",
            "Plot the location of poles and zero in the s-plane",
            "Save filters settings to file", 
            "Save filters to file and load coefficients into online system"};
         const int buttonid[] = 
            {kB_BODEPLOT, kB_STEPPLOT, kB_ROOTPLOT, kB_WRITEFILTER,
            kB_WRITELOAD};

	 num = 5;
         for (int i = 0; i < num; i++) {
            tmp_btn[i] = new TGTextButton (p, buttontext[i], buttonid[i], 
                                 btnGC, btnFont);
            tmp_btn[i]->Associate (this);
            tmp_btn[i]->SetToolTipText (buttonhelp[i]);
            if ((i == 3) || (i == 4)) {
               tmp_btn[i]->SetState (kButtonDisabled);
            }
            p->AddFrame (tmp_btn[i], btnLayout);
         }
      }

      //--------------------------------  Add the exit button.
      int nstd = AddStdButtons (ligogui::kButtonExit, 
				p, btns, max, btnLayout, btnGC, btnFont);

      for (int i = 0; i < num; i++) {
	btns[i] = tmp_btn[i];
      }

      //--------------------------------  The button layout was set here, but..
      //delete fButtonLayout;
      //fButtonLayout =
      //      new TGLayoutHints (kLHintsExpandX | kLHintsTop, 15, 15, 10, 6);
      return num + nstd;
   }

//______________________________________________________________________________
   TLGMultiPad* TLGFilterWizard::AddMainWindow (TGCompositeFrame* p,
                     TGLayoutHints* mainLayout,
                     PlotSet& plotset, int padnum)
   {
      fMainTab = new TGTab (p, 10, 10);
      fMainTab->Associate (this);
      p->AddFrame (fMainTab, mainLayout);
      // add filter design tab
      fFilterLayout = new TGLayoutHints (kLHintsCenterX | kLHintsCenterY, 
                           4, 4, 4, 4);
      fFilterTab = fMainTab->AddTab (" Design ");
      fFilterDesign = new TLGFilterWizWindow (fFilterTab, 
                           *fFilter, fModal, fIniPath, fIniFile, 
                           kMainFrameId + 1, fIniModule, fIniSection, fAnySample,
			   fExperimentMode);
      fFilterDesign->Associate (this);
      // Set the parent window for dialog and message boxes to keep them
      // centered in the main window.
      fFilterDesign->setParentWindow(this) ;
      fFilterTab->AddFrame (fFilterDesign, fFilterLayout);
      // add graphics tab
      fGraphicsTab = fMainTab->AddTab (" Graphics ");
      if (padnum < 1) padnum = 1;
      TLGMultiPad* pad = new TLGMultiPad (fGraphicsTab, "Plot", plotset, 
                           kMainFrameId, padnum);
      pad->Associate (this);
      if (padnum > 0) pad->GetPad(0)->HidePanel (kFALSE);
      if (padnum > 1) pad->GetPad(1)->HidePanel (kFALSE);
      fGraphicsTab->AddFrame (pad, mainLayout);
      return pad;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::ReadOnly() const
   {
      return fFilterDesign->ReadOnly();
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::SetExperimentMode (Bool_t mode)
   {
      fExperimentMode = mode ;
      return fFilterDesign->SetExperimentMode(mode) ;
   }
//______________________________________________________________________________
   Bool_t TLGFilterWizard::SetReadOnly (Bool_t readonly)
   {
// JCB - Always returns TRUE, why do it twice?      fFilterDesign->SetReadOnly (readonly);
      bool succ = fFilterDesign->SetReadOnly (readonly);
      if (succ && !fModal) {
         if (ReadOnly()) {
            fMenuFile->CheckEntry (kM_FILE_READONLY);
            fButtons[3]->SetState (kButtonDisabled);
            fButtons[4]->SetState (kButtonDisabled);
         }
         else {
            fMenuFile->UnCheckEntry (kM_FILE_READONLY);
            fButtons[3]->SetState (kButtonUp);
            fButtons[4]->SetState (kButtonDisabled); // for now
            //fButtons[4]->SetState (kButtonUp);
         }
      }
      return succ;
   }

// JCB
//______________________________________________________________________________
// 
// SetGain1Allowed() sets the menu item check indication and passes the value
// on to the TLGFilterWizWindow object
void TLGFilterWizard::SetGain1Allowed(Bool_t allowed)
{
   // Pass this on to the TLGFilterWizWindow object.
   fFilterDesign->SetGain1Allowed(allowed) ;

   // Adjust the file->Gain 1 allowed menu item display.
   if (allowed)
   {
      fMenuFile->CheckEntry(kM_FILE_GAIN1) ;
   }
   else
   {
      fMenuFile->UnCheckEntry(kM_FILE_GAIN1) ;
   }
}

//______________________________________________________________________________
// Legacy write is a menu item under the file menu that allows the user to write 
// the same file format as the SunOS versions of foton.  14 digit floating precision
// and no gain-only sections.
// Return the state of the legacy write variable.
   Bool_t TLGFilterWizard::LegacyWrite() 
   {
      // Note that fFilterDesign is a pointer to a TLGFilterWizWindow class, 
      // NOT the FilterDesign class...
      return fFilterDesign->LegacyWrite() ;
   }

//______________________________________________________________________________
// JCB
   Bool_t TLGFilterWizard::SetLegacyWrite(Bool_t legacyMode)
   {
      // The fFilterDesign is a pointer to the TLGFilterWizWindow class.
      fFilterDesign->SetLegacyWrite(legacyMode) ;

      // Now, make sure the file->legacy menu item shows the correct state.
      if (!fModal)
      {
	 if (legacyMode)
	 {
	    fMenuFile->CheckEntry(kM_FILE_LEGACY) ;
	 }
	 else
	 {
	    fMenuFile->UnCheckEntry(kM_FILE_LEGACY) ;
	 }
      }
      return(kTRUE) ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::FileSave ()
   {
      if (ReadOnly()) {
         new TGMsgBox (gClient->GetRoot(), this, "Error", 
                      "Unable to save filters in readonly mode.", 
                      kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      return fFilterDesign->SaveFile();
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::FileSaveAs ()
   {
      return fFilterDesign->SaveAs();
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::LoadCoeff ()
   {
      if (ReadOnly()) {
         new TGMsgBox (gClient->GetRoot(), this, "Error", 
                      "Unable to load filter coefficients in readonly "
                      "mode.", kMBIconExclamation, kMBOk, 0);         
         return kFALSE;
      }
      return fFilterDesign->LoadCoeff();
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::BodePlot ()
   {
      // The fFilterDesign points to the TLGFilterWizWindow.
      PlotDescriptor* pd = fFilterDesign->PlotBode();
      if (!pd) {
         Int_t ret;
         new TGMsgBox (gClient->GetRoot(), this, "Error", 
                      "Unable to generate Bode plot.", kMBIconExclamation, 
                      kMBOk, &ret);
         return kFALSE;
      }
      // fCalTable is from TLGMainWindow which the TLGFilterWizard is derived from.
      if (fCalTable)
      {
	 fCalTable->AddUnits (pd->Cal());
      }
      PlotDescriptor* cur = pd->Clone (pd->GetGraphType(),
                           "current_in", "current_out", fCalTable);
      // fPlot is a PlotSet* from TLGMainWindow which the TLGFilterWizard is derived from
      fPlot->Add (cur);
      ShowDefaultPlot (kFALSE, 0, 1);
      fPlot->Add (pd);
      // Updates all registered graphics pads (TLGPad)
      fPlot->Update();
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::ResponsePlot (const char* resp)
   {
      PlotDescriptor* pd = fFilterDesign->PlotResponse (resp);
      if (!pd) {
         Int_t ret;
         string msg = string ("Unable to generate ") + string (resp) +
            " response.";
         new TGMsgBox (gClient->GetRoot(), this, "Error", 
                      msg.c_str(), kMBIconExclamation, 
                      kMBOk, &ret);
         return kFALSE;
      }
      if (fCalTable) fCalTable->AddUnits (pd->Cal());
      string name = string ("current_") + resp;
      PlotDescriptor* cur = pd->Clone (pd->GetGraphType(),
                           name.c_str(), 0, fCalTable);
      fPlot->Add (cur);
      ShowDefaultPlot (kFALSE, 0, 3);
      fPlot->Add (pd);
      fPlot->Update();
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::RootPlot (bool splane)
   {
      PlotDescriptor* pole_pd = 0;
      PlotDescriptor* zero_pd = 0;
      if (!fFilterDesign->PlotRoots (pole_pd, zero_pd, splane)) {
         Int_t ret;
         string msg = string ("Unable to generate plot of roots.");
         new TGMsgBox (gClient->GetRoot(), this, "Error", 
                      msg.c_str(), kMBIconExclamation, 
                      kMBOk, &ret);
         return kFALSE;
      }
      if (fCalTable) fCalTable->AddUnits (pole_pd->Cal());
      if (fCalTable) fCalTable->AddUnits (zero_pd->Cal());
      string a = string ("current_poles_real");
      string b = string ("current_poles_imag");
      PlotDescriptor* pole_cur = pole_pd->Clone (pole_pd->GetGraphType(),
                           a.c_str(), b.c_str(), fCalTable);
      a = string ("current_zeros_real");
      b = string ("current_zeros_imag");
      PlotDescriptor* zero_cur = zero_pd->Clone (zero_pd->GetGraphType(),
                           a.c_str(), b.c_str(), fCalTable);
      fPlot->Add (pole_cur);
      fPlot->Add (zero_cur);
      // setup plot
      OptionAll_t* opt = fMPad->GetPlotOptions (0);
      if ((opt != 0) && 
         ((opt->fTraces.fGraphType.Length() == 0) ||
	  (opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
         if (opt->fConfig.fAutoConf) {
            SetDefaultGraphicsOptions (*opt);
         }
         // Defaults
         for (Int_t tr = 0; tr < kMaxTraces; tr++) {
            opt->fTraces.fPlotStyle[tr] = kPlotStyleMarker;
            // poles
            if (tr == 0) {
               opt->fTraces.fGraphType = pole_cur->GetGraphType();
               opt->fTraces.fAChannel[tr] = pole_cur->GetAChannel();
               if (pole_cur->GetBChannel() != 0) {
                  opt->fTraces.fBChannel[tr] = pole_cur->GetBChannel();
               }
               opt->fTraces.fActive[tr] = kTRUE;
            }
            // zeros
            else if (tr == 1) {
               opt->fTraces.fAChannel[tr] = zero_cur->GetAChannel();
               if (zero_cur->GetBChannel() != 0) {
                  opt->fTraces.fBChannel[tr] = zero_cur->GetBChannel();
               }
               opt->fTraces.fActive[tr] = kTRUE;
            }
            // add unit circle
            else if ((tr == kMaxTraces - 1) && !splane) {
               PlotDescriptor* unitc = TLGFilterWizWindow::UnitCircle();
               fPlot->Add (unitc);
               opt->fTraces.fGraphType = unitc->GetGraphType();
               opt->fTraces.fAChannel[tr] = unitc->GetAChannel();
               if (unitc->GetBChannel() != 0) {
                  opt->fTraces.fBChannel[tr] = unitc->GetBChannel();
               }
               opt->fTraces.fPlotStyle[tr] = kPlotStyleLine;
               opt->fTraces.fActive[tr] = kTRUE;
            }
            else {
               opt->fTraces.fActive[tr] = kFALSE;
            }
         }
         opt->fUnits.fYValues = kUnitMagnitude;
         opt->fRange.fAxisScale[0] = kAxisScaleLinear;
         opt->fRange.fAxisScale[1] = kAxisScaleLinear;
         opt->fRange.fRange[0] = kRangeAutomatic;
         opt->fRange.fRange[1] = kRangeAutomatic;
         opt->fRange.fBinLogSpacing = kFALSE;
         opt->fAxisX.fAxisTitle = splane ? "Real" : "Real";
         opt->fAxisY.fAxisTitle = splane ? "Imaginary" : "Imaginary";
         opt->fStyle.fTitle = splane ? "s-Plane" : "z-Plane";
         opt->fUnits.fXUnit = "Default";
         opt->fUnits.fYUnit = "Default";
         opt->fUnits.fXMag = 0;
         opt->fUnits.fYMag = 0;
         opt->fUnits.fXSlope = 1.;
         opt->fUnits.fXOffset = 0.;
         opt->fUnits.fYSlope = 1.;
         opt->fUnits.fYOffset = 0.;
      }
   
      fPlot->Add (pole_pd);
      fPlot->Add (zero_pd);
      fPlot->Update();
      return kFALSE;
   }

//______________________________________________________________________________
// This overrides the virtual function defined in the TLGMainMenu class.
   void TLGFilterWizard::AddMenuFile()
   {
      if (!fMenuFile) {
         fMenuFile = new TGPopupMenu (gClient->GetRoot());
         fMenuFile->Associate (TLGMainMenu::fParent);
      }
      fMenuFile->AddEntry ("&New", kM_FILE_NEW);
      if (!fModal) {
	 fMenuFile->AddEntry ("New Module...", kM_FILE_NEWMOD) ;
         fMenuFile->AddEntry ("Read &Only", kM_FILE_READONLY);
#ifdef LEGACY_WRITE
	 fMenuFile->AddEntry("&Legacy Mode", kM_FILE_LEGACY) ; // JCB
#endif
#ifdef GAIN1_ALLOWED
	 fMenuFile->AddEntry("Gain(1) Allowed", kM_FILE_GAIN1) ; // JCB
#endif
         fMenuFile->AddSeparator();
         fMenuFile->AddEntry ("Merge Matlab...", kM_FILE_MERGE) ; // JCB
         fMenuFile->AddEntry ("&Save", kM_FILE_SAVE);
         fMenuFile->AddEntry ("Save &As...", kM_FILE_SAVEAS);
         fMenuFile->AddEntry ("Save and &Load", kM_FILE_SAVELOAD);
      }
      if (fExperimentMode) {
	 fMenuFile->EnableEntry(kM_FILE_NEW) ;
	 fMenuFile->EnableEntry(kM_FILE_NEWMOD) ;
      } else {
	 fMenuFile->DisableEntry(kM_FILE_NEW) ;
	 fMenuFile->DisableEntry(kM_FILE_NEWMOD) ;
      }
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Import...", kM_FILE_IMPORT);
      fMenuFile->AddEntry ("&Export...", kM_FILE_EXPORT);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Print...", kM_FILE_PRINT);
      fMenuFile->AddEntry ("P&rint Setup...", kM_FILE_PRINTSETUP);
      fMenuFilePrintGraph = new TGPopupMenu (gClient->GetRoot());
      fMenuFilePrintGraph->AddEntry ("A", kM_FILE_PRINT_GRAPHA);
      fMenuFilePrintGraph->AddEntry ("B", kM_FILE_PRINT_GRAPHB);
      fMenuFile->AddPopup ("Print &Graph", fMenuFilePrintGraph);
      fMenuFile->AddSeparator();
      if (!fModal) {
         fMenuFile->AddEntry ("E&xit", kM_FILE_EXIT);
      }
      else {
         fMenuFile->AddEntry ("Ok", kM_FILE_OK);
         fMenuFile->AddEntry ("Cancel", kM_FILE_CANCEL);
      }
      fMenuBar->AddPopup ("&File", fMenuFile, fMenuBarItemLayout);
      fMenuFile->CheckEntry (kM_FILE_READONLY);
   }

//______________________________________________________________________________
   void TLGFilterWizard::AddMenuPlot()
   {
      if (!fMenuPlot) {
         fMenuPlot = new TGPopupMenu (gClient->GetRoot());
         fMenuPlot->Associate (TLGMainMenu::fParent);
      }
      fMenuPlot->AddEntry ("&Bode Plot", kM_PLOT_BODE);
      fMenuPlot->AddEntry ("S&tep Response", kM_PLOT_STEP);
      fMenuPlot->AddEntry ("&Impulse Response", kM_PLOT_IMPULSE);
      fMenuPlot->AddEntry ("R&amp Response", kM_PLOT_RAMP);
      fMenuPlot->AddSeparator();
      fMenuPlot->AddEntry ("&s-Plane Roots", kM_PLOT_SROOTS);
      fMenuPlot->AddEntry ("&z-Plane Roots", kM_PLOT_ZROOTS);
      fMenuPlot->AddSeparator();
      fMenuPlot->AddEntry ("&Closed Loop", kM_PLOT_CLOSEDLOOP);
      fMenuPlot->AddEntry ("&1/(1-G(f))", kM_PLOT_NEGCONV);
      fMenuPlot->AddSeparator();
      TLGMainMenu::AddMenuPlot();
   }

//______________________________________________________________________________
// Add an item to the About menu that lists the contents of the CHANGES file
// if it is present.
   void TLGFilterWizard::AddMenuHelp()
   {
      if (!fMenuHelp)
      {
	 fMenuHelp = new TGPopupMenu (gClient->GetRoot()) ;
	 fMenuHelp->Associate (TLGMainMenu::fParent) ;
      }

      fMenuHelp->AddEntry("Report a &Bug", kM_HELP_BUG);
      fMenuHelp->AddEntry("Release Notes", kM_HELP_NOTES) ;


      // Add the standard help menu items to the menu.
      TLGMainMenu::AddMenuHelp() ;
   }

//______________________________________________________________________________
   Bool_t TLGFilterWizard::ProcessButton (Long_t parm1, Long_t parm2) 
   {
      switch (parm1) 
      {
         case kB_BODEPLOT:
            {
               return BodePlot();
            }
         case kB_STEPPLOT:
            {
               return ResponsePlot ("step");
            }
         case kB_ROOTPLOT:
            {
               return RootPlot (true);
            }
         case kB_WRITEFILTER:
            {
               return FileSave();
            }
         case kB_WRITELOAD:
            {
               return FileSave() && LoadCoeff();
            }
         case kB_CANCEL:
            {
               if (fModal) {
                  if (fReturn) *fReturn = false;
                  DeleteWindow();
                  return kFALSE;
               }
               break;
            }
         case kB_OK:
            {
               if (fModal) {
                  bool succ = fFilterDesign->UpdateFilter();
                  if (fReturn) *fReturn = succ;
                  DeleteWindow();
                  return kTRUE;
               }
               break;
            }
         default:
            {
               return TLGMainWindow::ProcessButton (parm1, parm2);
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
// Handle File, Plot, and Window menu events.
   Bool_t TLGFilterWizard::ProcessMenu (Long_t parm1, Long_t parm2) 
   {
      switch (parm1) 
      {
	 case kM_FILE_NEW:
	    {
	       fFilterDesign->NewFile() ;

	       return kTRUE;
	    }
	 case kM_FILE_NEWMOD:
	    {
	       char modname[256] ;
	       new TLGInputDialog(gClient->GetRoot(), this, "New Module Name", "Name:  ", modname) ;
	       if (strlen(modname))
	       {
		  fFilterDesign->AddNewModule(modname) ;
	       }
	       return kTRUE ;
	    }
         case kM_FILE_READONLY:
            {
               return SetReadOnly (!ReadOnly());
            }
	 case kM_FILE_LEGACY:
	    {
	       SetLegacyWrite(!LegacyWrite()) ; // JCB
	       return kTRUE ;
	    }
	 case kM_FILE_GAIN1:
	    {
	       SetGain1Allowed(!Gain1Allowed()) ; // JCB
	       return kTRUE ;
	    }
         case kM_FILE_MERGE:
            {
               char file[512] ;
               new TLGImportMLDialog(gClient->GetRoot(), this, file, fFilterDesign->GetCurDir()) ;
               if (strlen(file))
               {
                  if (my_debug) cerr << "Calling merge(" << file << ")" << endl;
                  fFilterDesign->clearMergeErrors() ; // clear any previous errors.
                  if (fFilterDesign->MergeFile(file))
                     fFilterDesign->Update(kTRUE) ;
                  else
                  {
                     // Print the errors.  Display a popup with a TGTextView that shows
                     // the errors accumumated during the merge.
                     new TLGErrorDialog(gClient->GetRoot(), this, fFilterDesign->getMergeErrors(), TGString("Matlab Merge Errors")) ;
                  }
               }
               return kTRUE;

            }
         case kM_FILE_SAVELOAD:
            {
               return FileSave() && LoadCoeff();
            }
         case kM_FILE_OK:
            {
               return ProcessButton (kB_OK, 0);
            }
         case kM_FILE_CANCEL:
            {
               return ProcessButton (kB_CANCEL, 0);
            }
         case kM_PLOT_BODE:
            {
               return BodePlot();
            }
         case kM_PLOT_STEP:
            {
               return ResponsePlot ("step");
            }
         case kM_PLOT_IMPULSE:
            {
               return ResponsePlot ("impulse");
            }
         case kM_PLOT_RAMP:
            {
               return ResponsePlot ("ramp");
            }
         case kM_PLOT_SROOTS:
            {
               return RootPlot (true);
            }
         case kM_PLOT_ZROOTS:
            {
               return RootPlot (false);
            }
         case kM_PLOT_CLOSEDLOOP:
            {
               fFilterDesign->SetClosedLoop (
                                    !fFilterDesign->ClosedLoop());
               if (fFilterDesign->ClosedLoop()) {
                  fMenuPlot->CheckEntry (kM_PLOT_CLOSEDLOOP);
               }
               else {
                  fMenuPlot->UnCheckEntry (kM_PLOT_CLOSEDLOOP);
               }
               return kTRUE;
            }
         case kM_PLOT_NEGCONV:
            {
               fFilterDesign->SetClosedLoopNeg (
                                    !fFilterDesign->ClosedLoopNeg());
               if (fFilterDesign->ClosedLoopNeg()) {
                  fMenuPlot->CheckEntry (kM_PLOT_NEGCONV);
               }
               else {
                  fMenuPlot->UnCheckEntry (kM_PLOT_NEGCONV);
               }
               return kTRUE;
            }
         case kM_HELP_ABOUT:
            {
               new TGMsgBox (gClient->GetRoot(), TLGMainMenu::fParent, 
                            "About", aboutmsg, 0, kMBOk);
               break;
            }
	 case kM_HELP_NOTES:
	    {
	       new TLGErrorDialog(gClient->GetRoot(), TLGMainMenu::fParent,
			      foton_changes, "Release Notes") ;
	       break ;
	    }
          case kM_HELP_BUG:
          {
              const char bug_url[]="https://git.ligo.org/cds/dtt/issues/new";
              new ligogui::BugReportDlg(0, 0, bug_url, "contact+cds-dtt-5116-issue-@support.ligo.org");
              break ;
          }
         default:
            {
               return TLGMainWindow::ProcessMenu (parm1, parm2);
            }
      }
      return kTRUE;
   }

}


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Extern                                                               //
//                                                                      //
// These functions are needed by the FilterDesign class			//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C" {
   bool wizard___dynamic (const std::string name, std::string& filter)
   {
      if (!gClient) {
         return 0;
      }
      FilterDesign fdesign;
      string n = "Foton";
      if (!name.empty()) {
         n += ": ";
         n += name;
      }
   
      // create main window
      bool modal_ret = true;
      filterwiz::TLGFilterWizard* foton = new 
         filterwiz::TLGFilterWizard (gClient->GetRoot(), modal_ret, 
                           n.c_str(), filter, 0,  0);
      foton->Setup();
      foton->SetReadOnly (false);
      gClient->WaitFor (foton);
   
      return modal_ret;
   }

}
