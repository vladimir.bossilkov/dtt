/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <time.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <iostream>
#include <fstream>
#include "FilterFile.hh"
#include "iirutil.hh"
#include "mmap.hh"
#include <cstdlib>
#include <cstdarg>
#include <unistd.h>

   using namespace std;
   using namespace ligogui;

   static const int my_debug = 0 ;

namespace filterwiz {

static int linenum ;

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Parsing utilities						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   typedef vector<string> tokenlist;

//______________________________________________________________________________
// Given a memory map of characters pointed to by p and ending at end, find the
// first instance of a '\n' or '\r'.  When that character is located, it will be
// the start of the next line (and also the return value of this function).
// Calculate the length of the line by searching for the next instance of a '\n'
// or '\r' character (or the end of the huge array), counting the characters until
// it's found.
   const char* nextline (const char* p, const char* end, 
                     int& linelen, bool skipleadingspace = false)
   {
      bool newline = false;
      linenum++ ; // Increment the line count.
      for (; p < end; ++p) 
      {
         if ((*p == '\n') || (*p == '\r')) 
	 {
	    // To keep linenum correct, check to see if a newline has
	    // already been found.  If so, increment the line number
	    // because this indicates we've found a blank line.
	    if (newline == true)
	    {
	       linenum++ ;
	    }
            newline = true;
         }
         else if (newline) 
	 {
            // Once newline is true, p points to the first character
            // beyond the newline.  If no leading whitespace is wanted,
            // keep advancing the pointer p until a non-whitespace character
            // is found.
            if (skipleadingspace) 
	    {
               for (; (p < end) && isspace (*p); ++p) ;
            }
            // Find the next newline or end of file, counting the characters
            // so we know how long this line is.
            for (linelen = 0; (p + linelen < end) &&
                (p[linelen] != '\n') && (p[linelen] != '\r'); ++linelen) ;
            return p;
         }
      }
      return end;	// What is linelen a this point?
   }

//______________________________________________________________________________
// p is a character array that has been allocated in FilterFile::write(filename).
// The size of the array starts at 131072.  end points to the last character in the
// array, and line is the text that needs to be added to the array.  If the line to
// be added to p is longer than the space remaining (end-p), completely fill the
// array, but don't copy any more!  Successive calls to putline will not put any
// more characters in the array under this condition.
   char* putline (char* p, const char* end, const char* line)
   {
      int len = strlen (line);
      // Some lines have newlines embedded in them. Adjust linenum accordingly.
      for (int i = 0; i < len; ++i)
	 if (line[i] == '\n')
	    ++linenum ;

      if (len >= end - p) {
         memcpy (p, line, end - p);
         return p + (end - p);
      }
      else {
         memcpy (p, line, len);
         p[len] = '\n';
	 ++linenum ;
         return p + len + 1;
      }
   }

//______________________________________________________________________________
// Create a set of strings from a line of characters where each string is
// a set of characters separated by whitespace.  tokenlist is a vector<string>.
   void tokens (const char* p, int max, tokenlist& tok, bool add = false)
   {
      if (!add) tok.clear();
      const char* end = p + max;
      const char* first = 0;
      for (; p < end; ++p) {
         if (isspace (*p)) {
            if (first) {
               tok.push_back (string (first, p - first));
               first = 0;
            }
         }
         else if (!first) {
            first = p;
         }
      }
      if (first) {
         tok.push_back (string (first, p - first));
      }
   }

//______________________________________________________________________________
// bool isname(const char *p) takes a pointer to a conventional C string and
// evaluates it to see if it's a proper name.  Names start with an alpha and
// are followed by 0 or more alpha, numeral, or '_' characters.
   bool isname (const char* p) 
   {
      if (!isalpha (*p)) {
         return false;
      }
      for (; *p; ++p) {
         if (!isalnum (*p) && !(*p == '_')) {
            return false;
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool isname  (const string& p) 
   {
      return isname (p.c_str());
   }

//______________________________________________________________________________
// bool isintnum(const char *p) checks to see if the string pointed to by p
// represents an integer number.  An integer number is at least 1 digit
// '0' through '9', optionally preceded by a '+' or '-' character.
   bool isintnum  (const char* p) 
   {
      int where = 0; // 0 - start; 1 - after sign, 2 - after 1st digit
      for (; *p; ++p) {
         if (isdigit (*p)) {
            if (where <= 1) where = 2;
         }
         else if ((*p == '-') || (*p == '+')) {
            if (where == 0) where = 1;
            else 
               return false;
         }
         else {
            return false;
         }
      }
      return where == 2;
   }

//______________________________________________________________________________
   bool isintnum  (const string& p) 
   {
      return isintnum (p.c_str());
   }

//______________________________________________________________________________
   bool isnum  (const char* p) 
   {
      int where = 0; // 0 - start; 1 - after sign, 2 - after 1st digit
      // 3 - after point; 4 - after fraction didit
      // 5 - after expo, 6 - after expo sign, 7 - after expo digit
      for (; *p; ++p) {
         if (isdigit (*p)) {
            if (where <= 1) where = 2;
            else if (where == 3) where = 4;
            else if ((where == 5) || (where == 6)) where = 7;
         }
         else if ((*p == '-') || (*p == '+')) {
            if (where == 0) where = 1;
            else if (where == 5) where = 6;
            else 
               return false;
         }
         else if (*p == '.') {
            if (where == 2) where = 3;
            else 
               return false;
         }
         else if ((*p == 'e') || (*p == 'E')) {
            if ((where >= 2) && (where <= 4)) where = 5;
            else 
               return false;
         }
         else {
            return false;
         }
      }
      return (where == 2) || (where == 3) || (where == 4) || 
         (where == 7);
   }

//______________________________________________________________________________
   bool isnum  (const string& p) 
   {
      return isnum (p.c_str());
   }

//______________________________________________________________________________
// bool ismodname(const char *p) checks to see if the string pointed to by p
// is a proper module name.  This follows the same rule as above for a name,
// which is an alpha optionally followed by one or more alpha, numeral, or '_'
// characters.
   bool ismodname (const char* p) 
   {
      return isname (p);
   }

//______________________________________________________________________________
   bool ismodname  (const string& p) 
   {
      return ismodname (p.c_str());
   }

//______________________________________________________________________________
// Check for a valid sample rate.
   bool issamplerate(double samples_per_sec)
   {

     return samples_per_sec > 0;

   }
//______________________________________________________________________________
   bool issamplerate(const char *p)
   {
      if (p)
	 return issamplerate((int) strtol(p, (char **) NULL, 10)) ;
      else
	 return false ;
   }
//______________________________________________________________________________
   bool issamplerate(const string& p)
   {
      return issamplerate(p.c_str()) ;
   }
//______________________________________________________________________________
// bool compareModuleNames(FilterModule first, FilterModule second) checks the
// names of the modules using strcmp, and returns true if the first one is
// "less" than the second one, otherwise false.  This function is used by 
// the list::sort() method to sort the list of filter modules.
   bool compareModuleNames(FilterModule first, FilterModule second)
   {
      if (strcmp(first.getName(), second.getName()) <= 0)
      {
	 return true ;
      }
      return false ;
   }

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// FilterFile							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
// A FilterFile has a couple of protected members, fFilename which is a string,
// and fModules which is a FilterModuleList.
   FilterFile::FilterFile ()
   {
      if (my_debug) cerr << "FilterFile::FilterFile()\n" ;
      legacy_write = 0 ; 	// If true, write file compatible with iLigo.
      linenum = 1 ; 		// Line number in filter file, for error reporting.
      gain_only_allowed = 1 ; 	// Allow gain-only filters (not allowed in iLigo).
      fSample = "1" ; 		// Initialize to a known but illegal value.
      clearErrors() ;  		// Clear error messages accumulated reading a file.
   }

   FilterFile::~FilterFile()
   {
      if (my_debug) cerr << "FilterFile::~FilterFile()\n" ;
   }

//______________________________________________________________________________
   // Delete all the modules, clear the errors found reading the file.
   void FilterFile::clear()
   {
      fModules.clear();
      clearErrors() ;
   }

//______________________________________________________________________________
// Set the sample rate for the file given a character string
   void FilterFile::setFSample(const char *sample)
   {
      if (sample)
	 fSample = sample ;
      else
	 fSample = "" ;
   }

// Set the sample rate for the file given an unsigned int
   void FilterFile::setFSample(unsigned int sample)
   {
      char buf[16] ;

      sprintf(buf, "%u", sample) ;
      setFSample((const char *) buf) ;
   }

// Set the sample rate for the file given a double
   void FilterFile::setFSample(double sample)
   {
      setFSample((unsigned int) sample) ;
   }

//______________________________________________________________________________
// Either replace an existing FilterModule of the same name if it exists, or
// add a new FilterModule to the vector of FilterModules fModules.
   void FilterFile::add (const char* name, double fsample)
   {
      // Search the vector of FilterModule instances in fModules to see if the
      // name exists.  If it does, the FilterModule in the vector is replaced
      // with a new FilterModule of the same name and the specified fsample.
      for (FilterModuleList::iterator i = fModules.begin(); i != fModules.end(); ++i) 
      {
         if (strcmp (i->getName(), name) == 0) 
	 {
            *i = FilterModule (name, fsample);
            return;
         }
      }
      // The name wasn't found, add the new FilterModule to the end of the vector
      // of FilterModules, fModules.
      fModules.push_back (FilterModule (name, fsample));
   }

//______________________________________________________________________________
   void FilterFile::remove (const char* name)
   {
      for (FilterModuleList::iterator i = fModules.begin(); i != fModules.end(); ++i) 
      {
         if (strcmp (i->getName(), name) == 0) 
	 {
            fModules.erase (i);
            return;
         }
      }
   }

//______________________________________________________________________________
   FilterModule* FilterFile::find (const char* name)
   {
      for (FilterModuleList::iterator i = fModules.begin(); i != fModules.end(); ++i) 
      {
         if (strcmp (i->getName(), name) == 0) 
	 {
            return &*i;
         }
      }
      return 0;
   }

//______________________________________________________________________________
   const FilterModule* FilterFile::find (const char* name) const
   {
      for (FilterModuleList::const_iterator i = fModules.begin(); i != fModules.end(); ++i) 
      {
         if (strcmp (i->getName(), name) == 0) 
	 {
            return &*i;
         }
      }
      return 0;
   }

//______________________________________________________________________________
   bool FilterFile::valid (std::string& errmsg) const
   {
      errmsg = "";
      // loop over modules/sections
      for (FilterModuleList::const_iterator mod = fModules.begin(); mod != fModules.end(); ++mod) 
      {
         for (int i = 0; i < kMaxFilterSections; ++i) 
	 {
	    // *mod is a FilterModule, (*mod)[i] is a FilterSection
            if (!(*mod)[i].valid()) 
	    {
               if (!errmsg.empty()) errmsg += "\n";
               errmsg += mod->getName();
               char buf [256];
               sprintf (buf, "/%i", i);
               errmsg += buf; 
            }
         }
      }
      return errmsg.empty();
   }

//______________________________________________________________________________
// Update each filter section in each module in the file.
   bool FilterFile::update()
   {
      // loop over modules/sections
      for (FilterModuleList::iterator mod = fModules.begin(); mod != fModules.end(); ++mod) 
      {
         for (int i = 0; i < kMaxFilterSections; ++i) 
	 {
	    // *mod is a FilterModule, (*mod)[i] is a FilterSection
            if (!(*mod)[i].update()) 
	    {
	       const char *modulename = (*mod).getName() ;
	       const char *sectionname = (*mod)[i].getName() ;

	       cerr << "Error updating module " << modulename << ", section " << sectionname << endl ;
	       errorMessage("Error updating module %s, section %s", modulename, sectionname) ;
               return false; 
            }
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool FilterFile::read (const char* filename)
   {
      cerr << "read " << filename << endl ;
      // Note that the file name is the full path of the file.
      if (!filename) 
      {
         cerr << "Illegal file name" << endl;
	 errorMessage("Illegal file name") ;
         return false;
      }
      string oldfilename = fFilename;
      fFilename = filename;
      // Map the file to a block of memory.
      gdsbase::mmap mfile (filename, std::ios_base::in);
      if (!mfile) 
      {
         cerr << "Unable to open file " << filename << endl;
	 errorMessage("Unable to open file %s", filename) ;
         fFilename = oldfilename;
         return false;
      }

      // Save the file's status to determine if the file changes 
      // between reading and writing.  We want to alert the user
      // if the file is modified by another process while foton is open.
      if (stat(fFilename.c_str(), &fStat))
      {
	 // An error occurred getting stat.
	 cerr << "stat error, " << strerror(errno) << endl ;
	 errorMessage("stat error for file %s, %s", filename, strerror(errno)) ;
      }

      bool succ = read ((const char*)mfile.get(), mfile.size()) == (int)mfile.size();
      if (!succ) 
      {
         fFilename = oldfilename;
      }
      return succ;
   }

//______________________________________________________________________________
   int FilterFile::read (const char* beg, int maxlen)
   {
      const char* end = beg + maxlen;
      int linelen = 0;
      int lenModDef = strlen(kModuleDef);  		//kModuleDef is "# MODULES" from FilterModule.hh
      int lenSampDef = strlen(kModuleSamplingDef) ;	// kModleSamlingDef is "# SAMPLING"
      int lenDesignDef = strlen(kSectionDesignDef) ;	// kSectionDesignDef is "# DESIGN"
      int gain_only = 0 ;				// Set if the filter section is gain only.

      // clear old modules
      fModules.clear();
      // Clear merge errors
      errors.clear() ;
      // Clear file errors
      clearErrors() ;
      // Set line number to 0.
      linenum = 1 ;
      // check magic string
      if ((maxlen < (int)strlen (kMagic)) || (strncmp (beg, kMagic, strlen (kMagic)) != 0)) 
      {
         clear();
         cerr << "Not an online filter file" << endl;
	 errorMessage("Not an online filter file") ;
         return 0;
      }
      // first look for filter module names.  Scan the entire file for lines that 
      // start with "# MODULES".  When one is found, assume the remaining tokens
      // in the line are module names.  For each token beyond "# MODULES", check if the
      // name is valid, if it is create a new module with a bogus sample rate.  Otherwise
      // warn the user that the name is invalid.
      for (const char* p = beg; p < end; p = nextline (p, end, linelen)) 
      {
         if (linelen > lenModDef && strncmp (p, kModuleDef, lenModDef) == 0) 
	 {
            tokenlist tok;
            p += lenModDef;
            linelen -= lenModDef;
            tokens (p, linelen, tok);
            for (tokenlist::iterator i = tok.begin(); i != tok.end(); ++i) 
	    {
               if (ismodname (i->c_str())) 
	       {
		  // Add the module name to the list of modules for this file.
                  add (i->c_str(), 1E-12);
               }
               else 
	       {
                  cerr << linenum << ": " << "Illegal file module name " << *i << endl;
		  errorMessage("Illegal file module name %s", i->c_str()) ;
               }
            }
         }
      }

      /* JCB - Now, the modules have been read, they are unique.  Sort them by name.
       * The FilterModuleList pointed at by fModules has been changed from a 
       * <vector> to a <list> so we can use the list::sort() method.  A function has
       * been created for the sort to compare the names.
       */
      fModules.sort(compareModuleNames) ;
   
      /* JCB for aLIGO, specify one sample rate for the entire file.  Look for
       * any line that starts with kModuleSamplingDef and is followed either by a
       * number or some token followed by a number.  That should handle both legacy
       * files (where a sampling rate is specified for each module) and aLIGO
       * files where only one sample rate is specified near the beginning of the file.
       * (actually, it could be anywhere in the file, but by convention put it near the top
       * so we can find it.)
       */
      if (!legacy_write)
      {
	 double	sampleRate = 2048 ; // Default value

	 // Look anywhere in the file for a sampling rate 
	 linenum = 1 ; // Start with the first line again.
	 for (const char *p = beg; p < end; p = nextline (p, end, linelen))
	 {
	    // kModuleSamplingDef is "# SAMPLING" from FilterModule.hh
	    if ((linelen > (int) lenSampDef) && (strncmp (p, kModuleSamplingDef, lenSampDef) == 0))
	    {
	       tokenlist tok ;
	       tokens (p + lenSampDef, linelen - lenSampDef, tok) ;
	       if ((tok.size() == 1) && isnum (tok[0]) && (issamplerate(tok[0]) || any_sample_rate))
	       {
		  sampleRate = (double) strtol(tok[0].c_str(), (char **) NULL, 10) ;
		  break ;	// Found a line, quit looking.
	       }
	       else if ((tok.size() == 2) && isnum (tok[1]) && (issamplerate(tok[1]) || any_sample_rate))
	       {
		  // We really don't care what tok[0] was.
		  sampleRate = (double) strtol(tok[1].c_str(), (char **) NULL, 10) ;
		  break ;	// Found a line, quit looking.
	       }
	       else
	       {
		  const char *temp = string (p, linelen).c_str() ;

		  cerr << linenum << ": " << "Illegal sampling rate specification:" << endl 
		       << "  " << temp << endl ;
		  errorMessage("Illegal sampling rate specification: %s", temp) ; 
	       } 
	    }
	 }
	 // Assign the sample rate to the file.
	 setFSample(sampleRate) ;

	 // Assign the sample rate to all modules.  The rate is not changeable since this
	 // is read from a file, but filter editing functions expect the sample rate to 
	 // be associated with the module.  Note that a module may be created if foton is
	 // started from diaggui, and there's no file from which the sample rate can be read.
	 for (FilterModuleList::iterator i = modules().begin(); i != modules().end(); i++)
	 {
	    i->setFSample(sampleRate) ;
	 }
      }
      else
      {
	 // next look for filter module sampling rate
	 linenum = 1 ; // Start with the first line again.
	 for (const char* p = beg; p < end; p = nextline (p, end, linelen)) 
	 {
	    // kModuleSamplingDef is "# SAMPLING" from FilterModule.hh
	    if ((linelen > lenSampDef) && (strncmp (p, kModuleSamplingDef, lenSampDef) == 0)) 
	    {
	       tokenlist tok;
	       tokens (p + lenSampDef, linelen - lenSampDef, tok);
	       if ((tok.size() == 2) && ismodname (tok[0]) && isnum (tok[1])) 
	       {
		  FilterModule* mod = find (tok[0].c_str());
		  if (mod) 
		  {
		     mod->setFSample (atof (tok[1].c_str()));
		     // Also set the sample rate for the file.
		     setFSample(tok[1].c_str()) ;
		  }
		  else 
		  {
		     cerr << linenum << ": " << "Sampling rate for unknown filter module " 
			<< tok[0] << endl;
		     errorMessage("Sampling rate for unknown filter module %s", tok[0].c_str()) ;
		  }
	       }
	       else 
	       {
		  const char *temp = string(p, linelen).c_str() ;

		  cerr << linenum << ": " << "Illegal sampling rate specification:" << endl 
		     << "  " << temp << endl;
		  errorMessage("Illegal sampling rate specification: %s", temp) ;
	       }
	    }
	 }
      }
   
      // if we got no filter modules, start a scan
      // If there was no lines starting with "# MODULES " with actual module names,
      // but there are filter designs, this will create filter modules from the designs.
      // This is the only case where foton would create new filter modules that were not
      // listed in the MODULES section.  Normally, given a list of filter modules extracted
      // from the MODULES section, foton would discard any filter designs that don't match
      // one of the filter modules listed in the MODULES section.
      if (fModules.empty()) 
      {
	 linenum = 1 ; // Start with the first line again.
         for (const char* p = beg; p < end; p = nextline (p, end, linelen)) 
	 {
            if ((*p == '#') || isspace (*p)) 
	    {
               continue;
            }
            tokenlist tok;
            tokens (p, linelen, tok);
            if ((tok.size() > 0) && ismodname (tok[0].c_str()) && !find (tok[0].c_str())) 
	    {
               cout << linenum << ": " << "Warning: Filter module " << tok[0] << 
                  " added." << endl;
	       errorMessage("Warning: Filter module %s added", tok[0].c_str()) ;
	       // Create a new module with the file's sample rate and add it
	       // to the file's module list.
               add (tok[0].c_str(), getFSample());
            }
         }
      }
   
      // need at least one module.  If there isn't at least one, foton can't do anything.
      if (fModules.empty()) 
      {
         clear();
         cerr << linenum << ": " << "No filter module specification" << endl;
	 errorMessage("No filter module specification") ;
         return 0;
      }
   
      // guess sample rate if necessary
      for (FilterModuleList::iterator i = modules().begin(); i != modules().end(); ++i) 
      {
         if (i->getFSample() <= 1E-10) 
	 {
            i->defaultFSample ();
            cout << linenum << ": " << "Error: Setting sampling rate of " << i->getName() <<
               " to be " << i->getFSample() << endl;
	    errorMessage("Error: No sample rate specified. Setting rate of %s to be %f", i->getName(), i->getFSample()) ;
         }
      }
   
      // now read modules/sections
      string header;
      linenum = 1 ; // Start with the first line again.
      //for (const char* p = nextline (beg, end, linelen); p < end; p = nextline (p, end, linelen)) 
      for (const char* p = beg; p < end; p = nextline (p, end, linelen)) 
      {
         // skip filter info lines
	 // kModuleDef is "# MODULES"
	 // kModuleSamplingDef is "# SAMPLING"
	 // kSectionDesignDef is "# DESIGN" from FilterModules.hh
         if (((linelen > lenModDef) && (strncmp (p, kModuleDef, lenModDef) == 0)) ||
            ((linelen > lenSampDef) && (strncmp (p, kModuleSamplingDef, lenModDef) == 0)) ||
            ((linelen > lenDesignDef) && (strncmp (p, kSectionDesignDef, lenDesignDef) == 0))) 
	 {
            continue;
         }
         // add comments to header
         if (*p == '#') 
	 {
            if (header.empty()) header += '\n';
            header += string (p, linelen);
            continue;
         }
         // found something
         tokenlist tok;
         tokens (p, linelen, tok);
         if (tok.empty()) 
	 {
            header += '\n';
            continue;
         }

	 gain_only = 0 ;

         // check if correctly formatted section spec
	 // This assumes every filter section will have
	 // at least one SOS, since the last 4 tokens
	 // are the b1, b2, a1, a2 coefficients and
	 // the tok.size() needs to be 12.
         if ((tok.size() != 12) || !ismodname (tok[0]) || 
            !isintnum (tok[1]) || !isintnum (tok[2]) ||
            !isintnum (tok[3]) || !isnum (tok[4]) ||
            !isnum (tok[5]) || !isnum (tok[7]) ||
            !isnum (tok[8]) || !isnum (tok[9]) ||
            !isnum (tok[10]) || !isnum (tok[11])) 
	 {
            cerr << linenum << ": " << "Illegal filter section:" << endl <<
               "  " << string (p, linelen) << endl;
	    errorMessage("Illegal filter section: %s", string(p, linelen).c_str());
            continue;
         }
         FilterModule* mod = find (tok[0].c_str());
         if (!mod) 
	 {
            cerr << linenum << ": " << "Unknown filter module " << tok[0] << endl;
	    errorMessage("Unknown filter module %s", tok[0].c_str()) ;
            continue;
         }
         int index = atoi (tok[1].c_str());
         if ((index < 0) || (index >= kMaxFilterSections)) 
	 {
            cerr << linenum << ": " << "Illegal section number " << tok[1] << endl;
	    errorMessage("Illegal section number %s", tok[1].c_str()) ;
            continue;
         }
         input_switching  inpsw =  
            (input_switching)(atoi (tok[2].c_str()) / 10);
         if ((inpsw != kAlwaysOn) && (inpsw != kZeroHistory)) 
	 {
            cerr << linenum << ": " << "Illegal input switch " << tok[2] << endl;
	    errorMessage("Illegal input switch %s", tok[2].c_str()) ;
            continue;
         }
         output_switching outsw = 
            (output_switching)(atoi (tok[2].c_str()) % 10);
         if ((outsw != kImmediately) && (outsw != kRamp) &&
            (outsw != kInputCrossing) && (outsw != kZeroCrossing)) 
	 {
            cerr << linenum << ": " << "Illegal output switch " << tok[2] << endl;
	    errorMessage("Illegal output switch %s", tok[2].c_str()) ;
            continue;
         }
         int sosnum = atoi (tok[3].c_str());
//         if ((sosnum < 0) || (sosnum > kMaxFilterSOS))
// Change minimum sosnum to 1, front-ends can't load 0 SOS.	 
	 if (sosnum == 0)
	 {
	    sosnum = 1 ; // Leap of faith...  we hope there are coefficients to 
	    		 // go with this.  Gain only might have been written with
			 // 0 previously, this will attempt to repair the file.
	 }
         if ((sosnum < 1) || (sosnum > kMaxFilterSOS)) 
	 {
            cerr << linenum << ": " << "Illegal SOS number " << tok[3] << endl;
	    errorMessage("Illegal SOS number %s", tok[3].c_str()) ;
            continue;
         }
         double ramp = 0;
         double tolerance = 0;
         double timeout = 0;
         switch (outsw) 
	 {
            case kImmediately: 
               break;
            case kRamp:
               ramp = atof (tok[4].c_str()) / mod->getFSample();
               if (ramp <= 0) 
	       {
                  cerr << linenum << ": " << "Illegal ramp time " << tok[4] << endl;
		  errorMessage("Module %s section %d, Illegal ramp time %s", 
			tok[0].c_str(), index, tok[4].c_str()) ;
		  errorMessage("   Setting output switch to Immediate, ramp time to 0!") ;
		  // Try to fix the filter by changing the output switch and ramp time.
		  outsw = kImmediately ;
		  ramp = 0.0 ;
               }
               break;
            case kInputCrossing:
            case kZeroCrossing:
               tolerance = atof (tok[4].c_str());
               timeout = atof (tok[5].c_str()) / mod->getFSample();
               if (tolerance < 0) 
	       {
                  cerr << linenum << ": " << "Illegal switch tolerance " << tok[4] << endl;
		  errorMessage("Module %s section %d, Illegal switch tolerance %s", 
			tok[0].c_str(), index, tok[4].c_str()) ;
		  errorMessage("   Setting switch tolerance to 0!") ;
		  tolerance = 0.0 ;
               }
               if (timeout < 0) 
	       {
                  cerr << linenum << ": " << "Illegal timeout " << tok[5] << endl;
		  errorMessage("Module %s section %d, Illegal timeout %s", 
			tok[0].c_str(), index, tok[5].c_str()) ;
		  errorMessage("   Setting timeout to 0!") ;
		  timeout = 0.0 ;
               }
               break;
         }
// Before continuing, check for gain-only filter.  This is a special case
// of sosnum == 1 and tok[8], tok[9], tok[10], tok[11] all equal 0.0.
	 if (sosnum == 1 && atof(tok[8].c_str()) == 0.0 && atof(tok[9].c_str()) == 0.0
			 && atof(tok[10].c_str()) == 0.0 && atof(tok[11].c_str()) == 0.0)
	 {
	    // Gain-only filter.  Change the sosnum to 0 to keep this
	    // from being mis-interpreted internally.
	    sosnum = 0;
	    gain_only = 1 ;
	 }
         // OK, we got the 1st line: Now check for additional SOS lines
         for (int i = 1; i < sosnum; ++i) 
	 {
            p = nextline (p, end, linelen);
            if (p >= end) 
	    {
               cerr << linenum << ": " << "Not enough lines in file" << endl;
	       errorMessage("Not enough lines in file") ;
               break;
            }
            tokens (p, linelen, tok, true);
         }
         if ((sosnum > 0) && ((int)tok.size() != 8 + 4 * sosnum)) 
	 {
            cerr << linenum << ": " << "Not enough second order sections for " 
               << tok[0] << endl;
	    errorMessage("Not enough second order sections for %s", tok[0].c_str()) ;
            continue;
         }
         bool ok = true;
         for (int i = 12; i < 8 + 4 * sosnum; ++i) 
	 {
            if (!isnum (tok[i])) ok = false;
         }
         if (!ok) 
	 {
            cerr << linenum << ": " << "Filter coefficients for " << tok[0] 
               << " are not all numbers" << endl;
	    errorMessage("Filter coefficients for %s are not all numbers", tok[0].c_str()) ;
            continue;
         }
         // Finally we have a valid filter section!
         FilterSection& sect = mod->operator[] (index);
         sect.filter().reset();
         sect.filter().gain (atof (tok[7].c_str()));
         for (int i = 8; i < 8 + 4 * sosnum; i += 4) 
	 {
            sect.filter().biquad (1., atof (tok[i+2].c_str()),
                                 atof (tok[i+3].c_str()),
                                 atof (tok[i].c_str()),
                                 atof (tok[i+1].c_str()));
         }
         sect.setName (tok[6].c_str());
         sect.setInputSwitch (inpsw);
         sect.setOutputSwitch (outsw);
         sect.setRamp (ramp);
         sect.setTolerance (tolerance);
         sect.setTimeout (timeout);
         sect.setHeader (header.c_str());
	 sect.setGainOnly (gain_only) ;
	 if (gain_only)
	 {
	    cerr << "Setting gain-only gain string to " << tok[7] << " for " << tok[0] <<  endl ;
	    sect.setGainOnlyGain (tok[7]) ;
	 }
         header = "";
      }
   
      // last look for filter section design strings
      linenum = 1 ; // Start with the first line again.
      for (const char* p = beg; p < end; p = nextline (p, end, linelen)) 
      {
         // look for design string
         if ((linelen > (int)strlen (kSectionDesignDef)) &&
            (strncmp (p, kSectionDesignDef, 
                     strlen (kSectionDesignDef)) == 0)) 
	 {
            // tokenize string past the "# DESIGN" part.
            tokenlist tok;
            tokens (p + strlen (kSectionDesignDef), 
                   linelen - strlen (kSectionDesignDef), tok);
            // tok[0] will be a module name, tok[1] will be a section number
            // and tok[3] will be a design string.  Watch for a continuation
            // character ('\') at the end of the line!
            if ((tok.size() >= 3) && ismodname (tok[0]) && isintnum (tok[1]))
            {
               // found a valid design
               FilterModule* mod = find (tok[0].c_str());
               int index = atoi (tok[1].c_str());
               if (mod && (index >= 0) && (index < kMaxFilterSections)) 
	       {
                  // found a valid module
                  string s (p + strlen (kSectionDesignDef),
                           linelen - strlen (kSectionDesignDef));
                  int indent = strlen (kSectionDesignDef);
                  // delete module name
                  string::size_type pos = s.find (tok[0].c_str());
                  if (pos != string::npos) 
		  {
                     s.erase (0, pos + tok[0].size());
                     indent += pos + tok[0].size();
                  }
                  // delete section number
                  pos = s.find (tok[1].c_str());
                  if (pos != string::npos) 
		  {
                     s.erase (0, pos + tok[1].size());
                     indent += pos + tok[1].size();
                  }
                  // delete leading white space
                  while (!s.empty() && isspace (s[0])) 
		  {
                     s.erase (0, 1);
                     ++indent;
                  }
                  // delete trailing white space
                  while (!s.empty() && isspace (s[s.size()-1])) 
                     s.erase (s.size()-1, 1);
                  // look for line continuation
                  while (!s.empty() && (s[s.size()-1] == '\\') && (p < end)) 
		  {
                     // remove continuation characters
                     do 
		     {
                        s.erase (s.size()-1, 1);
                     } while (!s.empty() && (isspace (s[s.size()-1])));
                     // add new line
                     s += "\n";
                     // look for new line
                     p = nextline (p, end, linelen);
                     string n (p, linelen);
                     // delete leading white space and #
                     for (int i = 0; (i < indent) && !n.empty() && 
                         (isspace (n[0]) || (n[0] == '#')); ++i) 
		     {
                        n.erase (0, 1);
                     }
                     // delete trailing white space
                     while (!n.empty() && isspace (n[n.size()-1])) 
                        n.erase (n.size()-1, 1);
                     s += n;
                  }
                  // set design string
                  // Theoretically, a design string could exist even if a
                  // filter section for it isn't in the file.  But there'd
                  // be no switching parameters or section name.  Orphan'ed
                  // design strrings are discarded in checkDesign() below.
                  (*mod)[index].setDesign (s.c_str());
               }
               else if (!mod) 
	       {
                  cerr << linenum << ": " << "Design for unknown filter module " 
                     << tok[0] << endl;
		  errorMessage("Design for unknown filter module %s", tok[0].c_str()) ;
               }
               else 
	       {
                  cerr << linenum << ": " << "Illegal section number " << tok[0] <<
                     "/" << tok[1] << endl;
		  errorMessage("Illegal section number %s/%s", tok[0].c_str(), tok[1].c_str()) ;
               }
            }
            else 
	    {
               cerr << linenum << ": " << "Illegal design specification:" << endl 
                  << "  " << string (p, linelen) << endl;
	       errorMessage("Illegal design specification: %s", string(p, linelen).c_str()) ;
            }
         }
      }
   
      // now check filter if design strings are ok
      // checkDesign constructs an iir filter from the coefficients and from the 
      // design string.  If the two filters match (iircmp()) all is good.  If they
      // don't, a new design string is created from the coefficients.  So a design
      // string without a coefficient section is discarded here as there's no
      // coefficients.  checkDesign also modifies the design, so it's too late to
      // create coefficients after it's called.
      for (FilterModuleList::iterator i = fModules.begin(); i != fModules.end(); ++i) 
      {
	 i->checkDesign();
	 if (!i->errorsEmpty())
	 {
	    // Copy the errors from the module to the file error list. 
	    std::vector<std::string> *ferrors = getErrors() ;
	    std::vector<std::string> *merrors = i->getErrors() ;
	    for (std::vector<std::string>::iterator j = merrors->begin(); j != merrors->end(); ++j)
	    {
	       ferrors->push_back(*j) ;
	    }
	 }
      }
   
      return maxlen;
   }

//______________________________________________________________________________
   // Convert the filters to a new sample rate.  When done, set the fSample to
   // the new rate.  This is not commonly done, but is needed if you change the
   // rate of the model that uses these filters.
   void FilterFile::convertFilters(double newRate)
   {
      for (FilterModuleList::iterator i = fModules.begin(); i != fModules.end(); ++i)
      {
	 i->changeSampleRate(newRate) ;
      }
   }
//______________________________________________________________________________
   bool FilterFile::write (const char* filename)
   {
      return write (filename, (char *) NULL) ;
   }

   bool FilterFile::write (const char* filename, char *errmsg)
   {
      // Clear the file errors to start a new error log for writing.
      clearErrors() ;	
      linenum = 1 ; // Used by errorMessage to hint at where the problem might be.

      cerr << "write " << filename << endl ;
      if (!filename) {
	 if (errmsg)
	    sprintf(errmsg, "Illegal file name: %s", filename) ;
         cerr << "Illegal file name" << endl;
         return false;
      }
      if (!update()) {
	 if (errmsg)
	    strcpy(errmsg, "Invalid filter design") ;
         cerr << "Invalid filter design" << endl;
         return false;
      }
      ofstream out (filename);
      if (!out) {
	 if (errmsg)
	    sprintf(errmsg, "Unable to open file %s", filename) ;
         cerr << "Unable to open file " << filename << endl;
	 errorMessage("Unable to open file %s", filename) ;
         return false;
      }
      int size = 64*1024;
      char* p  = 0;
      int len;
      do {
         if (p) delete [] p;
         size *= 2;
         p = new char [size];
         if (!p)
         {
		if (errmsg)
		  strcpy(errmsg, "Memory exhausted attempting to write file") ;
                cerr << "Memory exhausted attempting to write file" << filename << endl ;
		errorMessage("Memory exhausted attempting to write file %s", filename) ;
                return (false) ;
	 }
         // Assuming we got the memory we requested, attempt to fill the array p with
         // the text that will be a filter file. If we need to write more characters
         // than were allocated, delete the whole thing, allocate twice as much memory,
         // and try again.

         len = write (p, size);
      } while (len >= size);
      out.write (p, len);
      delete [] p;
      if (!out) {
	 if (errmsg)
	    strcpy(errmsg, "Unable to write file") ;
         cerr << "Unable to write file " << filename << endl;
      }
      return !out.fail();
   }

//______________________________________________________________________________
   int FilterFile::write (char* beg, int maxlen)
   {
      // Format of filter definitions and coefficients.  These are extended precision.
      const char *format_str1 = "%-8s %i %2i %i %6i %6i %-10s %26.24e %20.16f %20.16f %20.16f %20.16f" ;
      const char *format_str2 = "%63s %20.16f %20.16f %20.16f %20.16f" ;

      if (!update()) {
         cerr << "Invalid filter design" << endl;
         return false;
      }

      // Decide what output format to use. 
      if (legacy_write)
      {
	 // Replace the format with the original strings.
	 format_str1 = "%-8s %i %2i %i %6i %6i %-10s %26.14f %20.14f %20.14f %20.14f %20.14f" ;
	 format_str2 = "%63s %20.14f %20.14f %20.14f %20.14f" ;
      }
   
      char* p = beg;
      char* end = beg + maxlen;
      // write magic header
      p = putline (p, end, kMagic);
      // write warning
      p = putline (p, end, 
                  "#\n# Computer generated file: DO NOT EDIT\n#");
      // write filter module names
      string line = kModuleDef;
      for (FilterModuleList::const_iterator mod = fModules.begin();
          mod != fModules.end(); ++mod) 
      {
         line += string (" ") + mod->getName();
         if (line.size() > 70) 
	 {
            p = putline (p, end, line.c_str());
            line = kModuleDef;
         }
      }
      if (line.size() > strlen (kModuleDef)) {
         p = putline (p, end, line.c_str());
      }
      p = putline (p, end, "#");
      /* For aLIGO write the sample rate once for the file since
       * all modules use the same sample rate.
       */
      if (!legacy_write)
      {
	 char buf[256] ;
	 // Write the sampling rate/
	 sprintf(buf, "%s RATE %s", kModuleSamplingDef, getFSampleStr()) ;
	 p = putline(p, end, buf) ;
	 p = putline(p, end, "#") ;
      }
      // write filter sections
      for (FilterModuleList::const_iterator mod = fModules.begin();
          mod != fModules.end(); ++mod) {
      
         // write title
         char buf[256];
         p = putline (p, end, "##########################################"
                     "######################################");
         sprintf (buf, "### %-72s ###", mod->getName());
         p = putline (p, end, buf);
         p = putline (p, end, "##########################################"
                     "######################################");
      
         // write sampling rate
	 // For aLIGO, don't write the sample rate.  It's written
	 // once for the file after the module names.
	 if (legacy_write)
	 {
	    sprintf (buf, "%s %s %g", kModuleSamplingDef,
		    mod->getName(), mod->getFSample());
	    p = putline (p, end, buf);
	 }
      
         // write design string
         for (int i = 0; i < kMaxFilterSections; ++i) 
	 {
            const FilterSection& sect = mod->operator[] (i);
            if (!sect.designEmpty()) 
	    {
               string s;
               char buf[256];

	       // This is 
	       // "# DESIGN   SFM_NAME n " where n is the filter section.
               sprintf (buf, "%s   %s %i ", kSectionDesignDef,
                       mod->getName(), i);
               s += buf;
               string linebreak = " \\## ";
               linebreak.append (s.size() - 2, ' ');
               string cmd = sect.getDesign();
	       // Strip any leading whitespace from the design string.  These can
	       // creep in if the design is hand edited, for example.
	       unsigned int space_cnt = 0 ;
	       while (space_cnt < cmd.length() &&
		      (cmd.at(space_cnt) == ' ' ||
		       cmd.at(space_cnt) == '\n' ||
		       cmd.at(space_cnt) == '\t'
		       )
		      )
		  ++space_cnt ;
	       if (space_cnt < cmd.length() && space_cnt > 0)
		  cmd = cmd.substr(space_cnt) ;

	       // Remove trailing characters.  The design string should end
	       // with a ')' character.
	       size_t cpos = cmd.find_last_of(')') ;
	       if (cpos != string::npos && cpos+1 < cmd.length()) {
		  cmd = cmd.substr(0,cpos+1) ;
	       }

	       // Now for the hard part - there may be blank lines which
	       // should be stripped out.  In this case, there will be a 
	       // '\n' followed by 0 or more whitespace followed by '\n'.
	       // There may be more than one...
	       size_t spos = 0 ; // Start for search.
	       while ((cpos = cmd.find_first_of('\n', spos)) != string::npos &&
		      cpos+1 < cmd.length()) {
		  cpos++ ;
		  if (cmd.at(cpos) == ' ' || cmd.at(cpos) == '\n' || cmd.at(cpos) == '\t') {
		     cmd.erase(cpos,1) ;
		  }
		  else {
		     spos = cpos ;
		  }
	       }
	       
               string::size_type pos;
               while ((pos = cmd.find ('\n')) != string::npos) 
	       {
                  cmd.erase (pos, 1);
                  cmd.insert (pos, linebreak.c_str());
               }
               while ((pos = cmd.find ("##")) != string::npos) 
	       {
                  cmd[pos] = '\n';
               }
               s += cmd;
               p = putline (p, end, s.c_str());
            }
         }
	 // This visually separates the designs from the coefficient strings.
         sprintf (buf, "### %-72s ###", "");
         p = putline (p, end, buf);
      
         // write coefficients
         for (int i = 0; i < kMaxFilterSections; ++i) 
	 {
            const FilterSection& sect = mod->operator[] (i);
            if (!sect.designEmpty()) 
	    {
	       //--- iirorder suppresses zeroes at fNy ... we don't want this
               //int order = iirorder (sect.filter().get());
               int order = iirsoscount(sect.filter().get());
               if (order < 0) {
                  cerr << "Not an IIR filter" << endl;
		  errorMessage("Module %s, section %s - Not an IIR filter.",
			       mod->getName(), sect.getName() ) ;
                  continue;
               } 
	       //--- Reserve space for zeroes.
	       else if (!order) {
		  order = 1;
	       }
               int nba;
               double* coeff = new double [1 + 4*order];
	       coeff[0] = 1.0;

               if (!iir2z (sect.filter().get(), nba, coeff, "o")) {
                  cerr << "Unable to obtain online filter coefficients" 
		       << endl;
		  errorMessage("Module %s, section %s - Unable to obtain online filter coefficients", mod->getName(), sect.getName() ) ;
		  delete[] coeff;
                  continue;
               }
               //p = putline (p, end, sect.getHeader());
               char buf[1024];
               int sosnum = (nba - 1) / 4;
               if (sosnum == 0) {
// aLIGO front ends can't handle 0 SOS.  So even if there aren't any SOS
// and we want a gain only filter, we have to set SOS to 1 with coefficients
// to 0.0.
//                  sosnum = 0;
                  sosnum = 1 ;
                  coeff[1] = coeff[2] = coeff[3] = coeff[4] = 0.0;
               }
	       // Added test for legacy write and sosnum < 1  - JCB
	       // sosnum should never be less than 0.
	       // If legacy_write is set, sosnum should never be less than 1.
	       // If not legacy_write && gain_only isn't allowed, sosnum should not be less than 1
               if ((sosnum < 0) || 
		   (legacy_write && sosnum < 1) || 
		   (!legacy_write && !gain_only_allowed && (sosnum < 1)) || 
		   (sosnum > kMaxFilterSOS)) 
	       {
                  cerr << "Invalid number of SOSs " << sosnum << endl;
		  errorMessage("Module %s, section %s - Invalid number of SOS (%d)", mod->getName(), sect.getName(), sosnum) ;
		  delete[] coeff;
                  continue;   // Don't write the filter section.
               }
               int arg1 = 0;
               int arg2 = 0;
               int sw = 10*int(sect.getInputSwitch()) +
                  int(sect.getOutputSwitch());
               switch (sect.getOutputSwitch()) {
                  case kImmediately: 
                     break;
                  case kRamp:
                     arg1 = int(sect.getRamp() * mod->getFSample() + 0.5);
                     break;
                  case kInputCrossing:
                  case kZeroCrossing:
                     arg1 = (int)(sect.getTolerance() + 0.5);
                     arg2 = (int)(sect.getTimeout() * mod->getFSample()  + 0.5);
                     break;
               }
               string sect_name = validSectionName (sect.getName());
	       // Changed to use a format string instead of a fixed format. - JCB
               sprintf (buf, format_str1, 
                       mod->getName(), i, sw, sosnum, arg1, arg2, 
                       sect_name.c_str(), coeff[0], coeff[1], coeff[2],
                       coeff[3], coeff[4]);
               p = putline (p, end, buf);
               for (int j = 1; j < sosnum; ++j) {
                  sprintf (buf, format_str2,
                          "", coeff[1+4*j], coeff[2+4*j], 
                          coeff[3+4*j], coeff[4+4*j]);
                  p = putline (p, end, buf);
               }
	       delete[] coeff;
            }
         }
      
         // empty line
         p = putline (p, end, "");
      }
   
      return p - beg;
   }

//______________________________________________________________________________
   std::string FilterFile::validSectionName (const char* name)
   {
      std::string s;
      for (const char* p = name; *p != 0; ++p) {
         if (isgraph (*p)) {
            s += *p;
         }
      }
      if (s.empty()) s = "Unknown";
      return s;
   }

// JCB - start
//______________________________________________________________________________
// If the file name is a symbolic link, follow the links to find the real name.
// Note the symbolic link may point to a symbolic link. 
//
// filename is the name of the file, a possible symbolic link which may be an absolute
// or relative path.
// dirname is the name of the starting directory, in which filename exists.  It may
// be empty, which would indicate the root directory.
// 
// Note that the return value will not be optimal, as the final path may include 
// "./" and "../" elements, but this won't matter since we can still open a file with
// those elements in the path.
//
// Symbolic links can point to non-existing files, if that happens this routine will
// return an empty string which should trigger an error in the calling routine.
   std::string FilterFile::getRealFilename(std::string filename, std::string dirname)
   {
      struct stat statbuf ;
      int	  nchrs ;
      string	  name ;
      string	  dir ;
#ifdef PATH_MAX
      const int	  bufsize = PATH_MAX ;
#else
#ifdef MAXPATHLEN
      const int	  bufsize = MAXPATHLEN ;
#else
      const int   bufsize = 4096 ;
#endif
#endif
      char	  linkbuf[bufsize] ;

      name = filename ;
      dir = dirname ;
      if (name.empty())
      {
	 // Not much we can do.
	 return name ;
      }

      // First, see if the filename starts with a '/', indicating an absolute path.
      // If not, we should prepend the current directory name, although it really doesn't
      // matter for the first stat.  
      if (name[0] != '/')
      {
	 name = dirname + "/" + name ;
      }
      
      if (my_debug) cerr << "getRealFilename(): " << name << '\n' ;

      // There is a good possibility the file doesn't exist if the user used "SaveAs".
      // So, if lstat has an error of ENOENT, just return the name.
      if (lstat(name.c_str(), &statbuf))
      {
	 if (errno == ENOENT)
	    return name ;

	 // An error occurred getting stat.
	 cerr << "stat error for  " << name << ": " << strerror(errno) << " at line " << __LINE__ << " in " << __FILE__ << endl ;
	 name.clear() ;
	 return name ;
      }

      while (S_ISLNK(statbuf.st_mode))
      {
	 // Get the directory from the name so if the link is relative we can
	 // reconstruct an absolute path.
	 size_t pos ;
	 pos = name.find_last_of("/") ;
	 dir = name.substr(0,pos) ;
	 if (my_debug) cerr << "getRealFilename(): directory of " << name << " is " << dir << '\n';

	 nchrs = readlink(name.c_str(), linkbuf, bufsize) ;
	 linkbuf[nchrs] = '\0' ; // since readlink doesn't null terminate... */

	 if (my_debug) cerr << "getRealFilename(): " << name << " points to " << linkbuf << '\n' ;

	 name = string(linkbuf) ;
	 // Check to make sure the name starts with '/'.  If it doesn't, it's a relative
	 // path and we need to make it a full path.  Otherwise, if it is a link pointed 
	 // to by a link, there's a good chance lstat won't find it.
	 if (name[0] != '/')
	 {
	    if (my_debug) cerr << "getRealFilename() prepending " << dir << " to " << name << '\n' ;
	    name = dir + "/" + name ;
	 }
	 if (lstat(name.c_str(), &statbuf))
	 {
	    // If the path doesn't exist, just return the name since the user may
	    // want to create the file.
	    if (errno == ENOENT)
	       return name ;
	    // An error occurred getting stat.
	    cerr << "stat error for  " << name << ": " << strerror(errno) << " at line " << __LINE__ << " in " << __FILE__ << endl ;
	    name.clear() ;
	    return name ;
	 }
      }
      return (name) ;

   }

//______________________________________________________________________________
   bool FilterFile::checkFileStat()
   {
      struct stat buf ;

      if (stat(fFilename.c_str(), &buf))
      {
	 // An error occurred getting stat.
	 cerr << "stat error for  " << fFilename << ": " << strerror(errno) << endl ;
      }
      // Compare the st_ino and st_mtime fields.  They need to be the same.
      cerr << (buf.st_ino != fStat.st_ino ? "file st_ino differs" : "file st_ino ok") << endl ;
      cerr << (buf.st_mtime != fStat.st_mtime ? "file st_mtime differs" : "file st_mtime ok") << endl ;
      return (buf.st_ino == fStat.st_ino && buf.st_mtime == fStat.st_mtime) ;
   }

//______________________________________________________________________________
   // This needs to be called after a file is saved since the inode number and mtime
   // for the file will have changed.
   void FilterFile::updateFileStat(const char *path)
   {
      if (path)
      {
	 if (stat(path, &fStat))
	 {
	    // An error occurred getting stat.
	    cerr << "stat error for  " << path << ": " << strerror(errno) << endl ;
	 }
      }
      return ;
   }

// JCB - end

// JCB - start
//______________________________________________________________________________
   // Merge matlab-created filters

//______________________________________________________________________________
   bool ishash (const char *p)
   {
      return !strcmp(p, "#") ;
   }

//______________________________________________________________________________
   bool ishash (const string &p)
   {
      return ishash(p.c_str()) ;
   }

// This function formats an error message and adds the string to the
// vector of error strings collected reading a file.  Use the function
// in the same manner as printf().
   void FilterFile::errorMessage(const char *msg, ...)
   {
      char		mbuf[1024] ;
      char		msgbuf[1024] ;
      va_list		argp ;

      // Textbook case of handling variable arguments being passed to another
      // function that takes variable arguments.
      va_start(argp, msg) ;
      vsprintf(msgbuf, msg, argp) ;
      va_end(argp) ;

      // Create a string from the msgbuf to add to the error vector
      sprintf(mbuf,"line %d: %s", linenum, msgbuf) ;
      string errstr(mbuf) ;

      // Put the new string at the end of the vector of errors.
      errors.push_back(errstr) ;
   }

// This function prints the file errors accumulated in the errors vector.
// The vector of errors is NOT cleared at the end, use clearErrors() if that
// is needed.
   void FilterFile::printFileErrors() 
   {
      if (!errors.empty())
      {
	 cout << "File errors:\n" ;
	 for (std::vector<std::string>::iterator i = errors.begin(); i != errors.end(); ++i)
	 {
	    cout << *i << endl ;
	 }
      }
   }

// This function just makes the accumulation of error
// messages more compact to make the code more readable.
   void FilterFile::mergeError(const char *msg, ...)
   {
      char		msgbuf[512] ;
      va_list 	argp ;

      if (my_debug) cerr << "FilterFile::mergeError(" << msg << "), mergeDebug = " << mergeDebug << '\n' ;
      if (mergeDebug)
      {
	 va_start(argp, msg) ;
	 vsprintf(msgbuf, msg, argp) ;
	 va_end(argp) ;
	 string errstr(msgbuf) ;
	 mergeErrors.push_back(errstr) ;
      }
   }

// Print the accumulated error messages
   void FilterFile::printMergeErrors()
   {
      if (!mergeErrors.empty())
      {
	 cout << "Merge errors:\n" ;
	 for (std::vector<std::string>::iterator i = mergeErrors.begin(); i != mergeErrors.end(); ++i)
	 {
	    cout << *i << endl ;
	 }
	 mergeErrors.clear() ; // Erase the contents.
      }
   }

//
// merge(filename)
// Read a file which should be generated from matlab that contains filter 
// section names, design strings, and switching parameters. For each module
// found, if it corresponds to an existing module replace the design of the
// current filter section with the design contained in this file.  
//
// The basic idea is that the RCG should generate a bare filter file that contains
// module names, but no filter designs.  If filter designs for the modules
// were defined in Matlab, this function would apply the designs to cause foton
// to create the complete standard filter module as if the designs had been 
// entered in foton.
//
// The format of a meaningful line of text in this file is:
// # <module_name> <section_number> <design_string> <switching> <ramp_time> <timeout_time> <section_name> 
//
// As an example:
// # ETMX_R0_S_ACT 8 notch(1,10,30) 12 3.0 0.0 notch_1
//
// Note the following: + module_name must be listed in the MODULES section at the
// 			 head of the filter file.  If it doesn't exist, the filter
//			 definition is ignored.
//		       + section_number is in the range 0-9
//		       + design_string must be a valid string containing no spaces.
//		       + switching is a two digit integer consisting of 
//			 (input switching + output switching), where input switching
//			 is one of:
//			    10 - Input is always applied to filter
//			    20 - Input switch will switch with output switch
//			 output switching is one of:
//			    1 - Immediate - switch off as soon as commanded
//			    2 - Ramp - ramp over time specified in ramp_time
//			    3 - Input crossing - output will switch when the filter
//				input and output are within a given value of each other
//   				(value is in ramp_time field)
//			    4 - Zero crossing - output switch when input crosses zero.
//		       + ramp_time can be a time (which is multiplied by sampling rate to 
//			 obtain ramp cycles) or a floating point value for input crossing
//		       + timeout_time - time in seconds. If the output switching isn't
//			 met in this time, the output will switch anyway.
//		       + section_name - Filter section name used in EPICS displays of
//			 the standard filter module.  No whitespace allowed.
//
// These functions are part of the FilterFile class because the merge needs to be done
// on a filter file that's already been read.
// Return 0 if successful.
//______________________________________________________________________________
   int	FilterFile::merge (const char *filename)
   {
      if (my_debug) cerr << "merge(" << filename << ")" << endl ;
      // Note the file name is the full path of the file.
      if (!filename)
      {
	 cerr << "No filename specified!" << endl ;
	 return -1 ;
      }

      gdsbase::mmap mergefile(filename, std::ios_base::in) ;
      if (!mergefile)
      {
	 cerr << "Unable to open file " << filename << endl ;
	 return -1 ;
      }

      return merge((const char*)mergefile.get(), mergefile.size()) ;
   }

//______________________________________________________________________________
// return 0 if successful.
   int FilterFile::merge (const char *beg, int maxlen)
   {
      const char 	*end = beg + maxlen ;	// Points to the end of the buffer holding the file.
      int 		 linelen = 0 ;		// linelen is the length of the line found by nextline().
      int 		 linenumber = 0 ;	// Keeps track of line in merge file.
      FilterModule	*module = (FilterModule *) NULL ;
      int		 retval = 0 ;
      int		 invalid = 0 ;

      // check magic string
      if ((maxlen < (int)strlen (kMatlabMagic)) ||
         (strncmp (beg, kMatlabMagic, strlen (kMatlabMagic)) != 0)) {
         cerr << "Not a Matlab Design file" << endl;
         return -1 ;
      }
      for (const char *p = beg; p < end; p = nextline(p, end, linelen)) 
      {
	 linenumber++ ;
	 invalid = 0 ;

	 // Get a vector of tokens from the current line at p.
	 tokenlist tok ;
	 tokens(p, linelen, tok) ;
	 // Each valid line needs 8 tokens.  Anything more or less, disregard.
	 if (tok.size() == 8)
	 {
	    int			section_number ;
	    input_switching	inpsw ;
	    output_switching	outsw ;
	    double		ramp_time = 0.0 ;
	    double		timeout_time = 0.0 ;
	    double		tolerance = 0.0 ;

	    if (mergeDebug > 1)
	    {
	       printf("merge: line %d, found 8 tokens:", linenumber) ;
	       for (unsigned int i = 0; i < tok.size(); i++)
	       {
		  printf(" %s", tok[i].c_str()) ;
	       }
	       printf("\n") ;
	    }

	    // The first token has to be a "#"
	    if (!ishash(tok[0]))
	    {
	       mergeError( "Line %d: Must start with '#'", linenumber) ;
	       invalid = 1 ;
	    }

	    // The second token needs to be a module name of a module
	    // that exists.
	    if (!ismodname(tok[1]) || !(module = find(tok[1].c_str())))
	    {
	       mergeError( "Line %d: Invalid module name %s or module not found", linenumber, tok[1].c_str()) ;
	       invalid = 1 ;
	    }

	    // The third token needs to be a section number, range 0-9 
	    if (!isintnum(tok[2]))
	    {
	       mergeError( "Line %d: Invalid section number", linenumber) ;
	       invalid = 1 ;
	    }
	    else
	    {
	       section_number = atoi(tok[2].c_str()) ;
	       if (section_number < 0 || section_number > 9)
	       {
		  mergeError( "Line %d: Invalid section number %d", linenumber, section_number) ;
		  invalid = 1 ;
	       }	
	    }

	    // Fifth token is a switching parameter.  Needs to be an int.
	    if (!isintnum(tok[4]))
	    {
	       mergeError( "Line %d: Invalid switching parameter", linenumber) ;
	       invalid = 1 ;
	    }
	    else
	    {
	       inpsw = (input_switching)(atoi(tok[4].c_str()) / 10) ;
	       if ((inpsw != kAlwaysOn) && (inpsw != kZeroHistory))
	       {
		  mergeError( "Line %d: Invalid input switch %d", linenumber, atoi(tok[4].c_str()) / 10) ;
		  invalid = 1 ;
	       }
	       outsw = (output_switching)(atoi(tok[4].c_str()) % 10) ;
	       if ((outsw != kImmediately) && (outsw != kRamp) &&
		  (outsw != kInputCrossing) && (outsw != kZeroCrossing))
	       {
		  mergeError( "Line %d: Invalid output switch %d", linenumber, atoi(tok[4].c_str()) % 10) ;
		  invalid = 1 ;
	       }
	    }

	    // Handle the ramp time/tolerance and timeout values.
	    // Sixth token is ramp time or tolerance, depending on outsw.
	    if (!invalid)
	    {
	       switch (outsw) 
	       {
		  case kImmediately:
		     // We don't care what ramp value is, it's not used.
		     // We'll also set it to zero.
		     ramp_time = 0.0 ;
		     tolerance = 0.0 ;
		     timeout_time = 0.0 ;
		     break ;
		  case kRamp:
		     // The ramp time is an actual time, not cycles.
		     ramp_time = atof(tok[5].c_str()) ;
		     tolerance = 0.0 ;
		     timeout_time = 0.0 ;
		     if (ramp_time <= 0)
		     {
			mergeError( "Line %d: Ramp time must be greater than 0.", linenumber) ;
			invalid = 1 ;
		     }
		     break ;
		  case kInputCrossing:
		  case kZeroCrossing:
		     // The 6th token is a tolerance, the 7th is a timeout in time, not cycles.
		     ramp_time = 0.0 ;
		     tolerance = atof(tok[5].c_str()) ;
		     timeout_time = atof(tok[6].c_str()) ;
		     if (tolerance < 0)
		     {
			mergeError( "Line %d: Tolerance must be greater than 0.", linenumber) ;
			invalid = 1 ;
		     }
		     if (timeout_time < 0)
		     {
			mergeError( "Line %d: Timeout must be greater than 0.", linenumber) ;
			invalid = 1 ;
		     }
		     break ;
	       }
	    }

	    // token[3] is a design string.
	    if (!invalid)
	    {
	       // The constructors of a FilterModule create all 10 FilterSection
	       // for the module, accessed through (*module)[section number], so
	       // all we need to do is update all the fields of the FilterSection
	       // and call (*module)[section number].update().  This should create
	       // the filter we want.
	       FilterSection *section = &((*module)[section_number]) ;

	       section->setName(tok[7].c_str()) ;
	       section->setInputSwitch(inpsw) ;
	       section->setOutputSwitch(outsw) ;
	       section->setRamp(ramp_time) ;
	       section->setTolerance(tolerance) ;
	       section->setTimeout(timeout_time) ;
	       section->setDesign(tok[3].c_str()) ;
	       
	       if (!(section->update()))
	       {
		  mergeError("Creation of filter failed from design = %s\n", section->getDesign()) ;
		  invalid = 1;
	       }
	       else
	       {
		  if (mergeDebug > 1)
		  {
		     printf("Filter creation for section %d succeeded!\n", section_number) ;
		     printf("    Filter section data:\n") ;
		     printf("      Name:          %s\n", section->getName()) ;
		     printf("      Input Switch:  %d\n", (int) section->getInputSwitch()) ;
		     printf("      Output Switch: %d\n", (int) section->getOutputSwitch()) ;
		     printf("      Ramp:          %f\n", section->getRamp()) ;
		     printf("      Timeout:       %f\n", section->getTimeout()) ;
		     printf("      Design String: %s\n", section->getDesign()) ;
		  }
	       }
	    }
	 }
	 else
	 {
	    // There was not the right number of tokens.
	    if (mergeDebug > 1)
	    {
	       printf("merge: %d tokens found on line %d (8 required for filter specification)\n", (int) tok.size(), linenumber) ;
	    }
	 }
	 retval |= invalid ;
      }
      return(retval) ;
   }


// JCB -end



//______________________________________________________________________________
    // JCB Implementation of SectCopy class.  A vector of these classes is created
    // when the Copy button is pressed in the Sections frame.  This should capture
    // enough data to allow a section to be created in another module (or section)
    // by pressing the paste button.  If a single section is copied, it can be pasted
    // in any filter section, the fIndex value would be ignored.  If multiple sections
    // are copied, a Paste operation will create the sections using the fIndex value
    // to put them in the same place in whatever module they are pasted in.

    // Constructor takes a pointer to a filter section.
    SectCopy::SectCopy(FilterSection *section)
    {
        if (section)
        {
            fIndex = section->getIndex() ;
            fInp_sw = section->getInputSwitch() ;
            fOut_sw = section->getOutputSwitch() ;
            fRamp = section->getRamp() ;
            fTolerance = section->getTolerance() ;
            fTimeout = section->getTimeout() ;
            fName = section->getName() ;
            fDesign = section->getDesign() ;
        }
        else
	    // This shouldn't happen.
            cerr << "SectCopy() - section pointer is null!" << endl ;
    }

    // Set the index explicitly.  Allowable values are -1, 0-9.
    void SectCopy::SetIndex(int index)
    {
        // Make sure the value passed is legal.
        if (index < -1 || index > 9)
            index= -1 ;
        fIndex = index ;
        return ;
    }

    // Copy the saved design information to the filter section.
    void SectCopy::PasteSection(FilterSection *section)
    {
        if (section)
        {
            section->setInputSwitch(fInp_sw) ;
            section->setOutputSwitch(fOut_sw) ;
            section->setRamp(fRamp) ;
            section->setTolerance(fTolerance) ;
            section->setTimeout(fTimeout) ;
            section->setName(fName.c_str()) ;
            section->setDesign(fDesign.c_str()) ;
        }
        return ;
    }

    // This is the list of copied filter sections.  It's a vector.
    SectCopyList fSectCopyList ;
// JCB - end

} // namespace filterwiz
