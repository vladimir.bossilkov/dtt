/* version $Id: TLGFilterDlg.hh 7980 2018-01-05 01:26:54Z john.zweizig@LIGO.ORG $ */
#ifndef _LIGO_TLGFILTERDLG_H
#define _LIGO_TLGFILTERDLG_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGFilterDlg						*/
/*                                                         		*/
/* Module Description: Filter Wizard Dialog boxes			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGFilterDlg.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include <TGTextView.h>         // JCB
#include <TRootEmbeddedCanvas.h>
#include <TLatex.h>
#include <string>
#include <vector>
#include "TLGFrame.hh"
#include "Complex.hh"

// JCB - start
class TGFSComboBox ;
class TGFileContainer ;
class TGPictureButton ;
class TGFileContainer ;
class TGListView ;
// JCB - end

namespace ligogui {
   class TLGNumericControlBox;
}

namespace filterwiz {

   class FilterDlgParser;

   const int kMaxPolyCoeff = 20;


/** Specification for gain.
    @memo Dialog box for entering a gain factor.
    @author Written August 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGGainDialog : public ligogui::TLGTransientFrame {
      friend class FilterDlgParser;
   protected:
      /// Return string
      TString*		fRet;
      /// Numereous layout hints
      TGLayoutHints*	fL[16];
      /// Frames
      TGCompositeFrame* fF[4];
      /// Labels
      TGLabel*		fLabel[2];
      /// Gain value
      ligogui::TLGNumericControlBox*	fGain;
      /// Gain format (dB or scalar)
      TGButton*		fFormat[2];
      /// Button frame
      TGCompositeFrame* fFButton;
      /// Ok& Cancel button
      TGButton*		fButton[2];
   
   public:
      /// Constructor
      TLGGainDialog (const TGWindow* p, const TGWindow* main,
                    TString& result);
      /// Destructor
      virtual ~TLGGainDialog();
      /// Close window method
      virtual void CloseWindow();
      /// Setup method
      virtual Bool_t Setup (const char* cmd);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


/** Specification for poles and zeros. Handles both s-plane and
    z-plane roots.
    @memo Dialog box for entering poles and zeros.
    @author Written August 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGZpkDialog : public ligogui::TLGTransientFrame {
      friend class FilterDlgParser;
   public:
      /// List of roots
      typedef std::vector<dComplex> RootList;
      /// Complex format
      enum ComplexFormat {
      /// Real/Imaginary
      kReIm = 0,
      /// Magnitude/Phase
      kMagPhase = 1,
      /// Magnitude/Q
      kMagQ = 2
      };
   
   protected:
      /// Sampling rate
      double		fSample;
      /// Return string
      TString*		fRet;
      /// S-plane poles and zeros?
      Bool_t		fSPlaneSpec;
      /// List of poles and zeros
      RootList		fPZ[2];
      /// S-plane location
      std::string	fPlane;
   
      /// Numereous layout hints
      TGLayoutHints*	fL[30];
      /// Numereous frames
      TGCompositeFrame* fF[10];
      /// Labels
      TGLabel*		fLabel[20];
      /// Gain value
      ligogui::TLGNumericControlBox*	fGain;
      /// Gain format (dB or scalar)
      TGButton*		fGainFormat[2];
      /// Complex format (Re/Im, Mag/Phase or Mag/Q)
      TGButton*		fCmplxFormat[3];
      /// Phase format (degree or rad)
      TGButton*		fPhaseFormat[2];
      /// Root location spec (s, f, or n)
      TGButton*		fRootFormat[3];
      /// Number
      ligogui::TLGNumericControlBox*	fNum[2];
      /// Real or complex roots
      TGButton*		fRealCmplx[2];
      /// Poles or Zeros?
      TGButton*		fPoleZero[2];
      /// List box for poles and zeros
      TGListBox*	fPZList[2];
      /// Add, remove, modify, clear and sort poles or zeros
      TGButton*		fPZBtn[5];
      /// Button frame
      TGCompositeFrame* fFButton;
      /// Ok& Cancel button
      TGButton*		fButton[2];
   
      /// Convert complex to string
      static std::string cmplx2str (dComplex x, 
                        ComplexFormat format = kReIm, 
                        bool degrees = true);
   public:
      /// Constructor
      TLGZpkDialog (const TGWindow* p, const TGWindow* main,
                   double fsample, TString& result, 
                   Bool_t splane = kTRUE);
      /// Destructor
      virtual ~TLGZpkDialog();
      /// Close window method
      virtual void CloseWindow();
      /// Setup method
      virtual Bool_t Setup (const char* cmd);
      /// Set s-plane location: "s", "f" or "n"
      virtual Bool_t SetPlane (const char* plane);
      /// Add a root
      virtual Bool_t AddRoot (int list, int* index = 0);
      /// Remove a root
      virtual Bool_t RemoveRoot (int list, int* index = 0);
      /// Update text
      virtual Bool_t UpdateText();
      /// Build list method
      virtual Bool_t Build (int level, Bool_t plist = kTRUE, 
                        Bool_t zlist = kTRUE);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


/** Specification for polynomial and direct form.
    @memo Dialog box for entering a polynomials.
    @author Written August 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGPolyDialog : public ligogui::TLGTransientFrame {
      friend class FilterDlgParser;
   protected:
      /// Sampling rate
      double		fSample;
      /// S-plane poles and zeros?
      Bool_t		fSPlaneSpec;
      /// Return string
      TString*		fRet;
      /// Numereous layout hints
      TGLayoutHints*	fL[10+4*kMaxPolyCoeff];
      /// Frames
      TGCompositeFrame* fF[3];
      /// Labels
      TGLabel*		fLabel[1];
      /// Gain value
      ligogui::TLGNumericControlBox*	fGain;
      /// Labels for poly
      TGLabel*		fPolyLabel[2][kMaxPolyCoeff];
      /// Formula
      TRootEmbeddedCanvas*	fFormula;
      /// formula text
      TLatex*		fFormulaText;
      /// Poly coeffciients
      ligogui::TLGNumericControlBox*	fPolyCoeff[2][kMaxPolyCoeff];   
      /// Button frame
      TGCompositeFrame* fFButton;
      /// Ok& Cancel button
      TGButton*		fButton[2];
   
   public:
      /// Constructor
      TLGPolyDialog (const TGWindow* p, const TGWindow* main,
                    TString& result, double sample,
                    Bool_t splane = kTRUE);
      /// Destructor
      virtual ~TLGPolyDialog();
      /// Close window method
      virtual void CloseWindow();
      /// Setup method
      virtual Bool_t Setup (const char* cmd);
      /// Set formula
      virtual Bool_t SetFormula();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


/** Specification for notches, resonant gains and comb filters.
    @memo Dialog box for entering notches, resonant gains and comb 
    filters.
    @author Written August 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGNotchDialog : public ligogui::TLGTransientFrame {
      friend class FilterDlgParser;
   public:
      /// subtype
      enum subtype_t {
      /// Notch
      kNotch,
      /// Resonant gain
      kResGain,
      /// Comb filter
      kComb
      };
   
   protected:
      /// Return string
      TString*		fRet;
      /// Subtype
      subtype_t		fType;
      /// Numereous layout hints
      TGLayoutHints*	fL[16];
      /// Numereous frames
      TGCompositeFrame* fF[4];
      /// Labels
      TGLabel*		fLabel[8];
      /// Frequency
      ligogui::TLGNumericControlBox*	fFreq;
      /// Q
      ligogui::TLGNumericControlBox*	fQ;
      /// Depth/Height
      ligogui::TLGNumericControlBox*	fDepth;
      /// Frequency
      ligogui::TLGNumericControlBox*	fHarmonics;
      /// Button frame
      TGCompositeFrame* fFButton;
      /// Ok& Cancel button
      TGButton*		fButton[2];
   
   public:
      /// Constructor
      TLGNotchDialog (const TGWindow* p, const TGWindow* main,
                     TString& result, subtype_t subt = kNotch);
      /// Destructor
      virtual ~TLGNotchDialog();
      /// Close window method
      virtual void CloseWindow();
      /// Setup method
      virtual Bool_t Setup (const char* cmd);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


/** Specification for elliptic, butterworth and chebyshev filters.
    @memo Dialog box for entering elliptic, butterworth and chebyshev 
    filters.
    @author Written August 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGEllipDialog : public ligogui::TLGTransientFrame {
      friend class FilterDlgParser;
   public:
      /// subtype
      enum subtype_t {
      /// Elliptic
      kEllip,
      /// Butterworth
      kButter,
      /// Chebyshev type 1
      kCheby1,
      /// Chebyshev type 2
      kCheby2
      };
   
   protected:
      /// Return string
      TString*		fRet;
      /// Subtype
      subtype_t		fType;
      /// Numereous layout hints
      TGLayoutHints*	fL[20];
      /// Numereous frames
      TGCompositeFrame* fF;
      /// Labels
      TGLabel*		fLabel[10];
      /// Filter type
      TGComboBox*	fFilterType;
      /// Order
      ligogui::TLGNumericControlBox*	fOrder;
      /// 1st frequency
      ligogui::TLGNumericControlBox*	fFreq1;
      /// 2nd frequency
      ligogui::TLGNumericControlBox*	fFreq2;
      /// Ripple
      ligogui::TLGNumericControlBox*	fRipple;
      /// Attenuation
      ligogui::TLGNumericControlBox*	fAtten;
      /// Button frame
      TGCompositeFrame* fFButton;
      /// Ok& Cancel button
      TGButton*		fButton[2];
   
   public:
      /// Constructor
      TLGEllipDialog (const TGWindow* p, const TGWindow* main,
                     TString& result, subtype_t subt = kEllip);
      /// Destructor
      virtual ~TLGEllipDialog();
      /// Close window method
      virtual void CloseWindow();
      /// Setup method
      virtual Bool_t Setup (const char* cmd);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


/** Specification for second order sections.
    @memo Dialog box for entering second order sections.
    @author Written August 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGSosDialog : public ligogui::TLGTransientFrame {
      friend class FilterDlgParser;
   protected:
      /// Sampling rate
      double		fSample;
      /// Id for list box
      int		fId;
      /// Return string
      TString*		fRet;
      /// Numereous layout hints
      TGLayoutHints*	fL[16];
      /// Numereous frames
      TGCompositeFrame* fF[4];
      /// Labels
      TGLabel*		fLabel[12];
      /// Order
      ligogui::TLGNumericControlBox*	fGain;
      /// a's and b's
      ligogui::TLGNumericControlBox*	fCoeff[4];
      /// Add & Remove button
      TGButton*		fCoeffBtn[3];
      /// List box
      TGListBox*	fCoeffSel;
      /// Button frame
      TGCompositeFrame* fFButton;
      /// Ok & Cancel button
      TGButton*		fButton[2];
   
   public:
      /// Constructor
      TLGSosDialog (const TGWindow* p, const TGWindow* main,
                   double sample, TString& result);
      /// Destructor
      virtual ~TLGSosDialog();
      /// Close window method
      virtual void CloseWindow();
      /// Setup method
      virtual Bool_t Setup (const char* cmd);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };

// JCB - start
// Import Design dialog box
//********************************************************************
    class TLGImportDialog : public ligogui::TLGTransientFrame{
    protected:
	/// Return value from import
	TString		*fRet ;
        /// Group - This is the group that holds the input widgets
        TGGroupFrame*     fG;
        /// Frame for OK, Cancel buttons.
        TGHorizontalFrame *fFButton ;
        /// Layout hints for widgets
        TGLayoutHints    *fL[9];
        /// Labels (Path:, File:)
        TGLabel*          fLabel[2];
        /// Path selection combo box.
        TGFSComboBox*     fPath;
        /// Directory up button
        TGPictureButton*  fCdup;
        /// Picture for fCdup directory up button.
        const TGPicture*  fPcdup;
        /// Directory list view
        TGListView*       fFv;
        /// Directory list view container
        TGFileContainer*  fFc;
        /// File selection combo box
        TGComboBox*       fFile;
        /// Ok button.
        TGTextButton    *fOk ;
        /// Cancel button
        TGTextButton    *fCancel ;
        /// Text box to display what's in the file. (file preview)
        TGTextView      *fText ;
        /// Design string from the file.
        std::string             fDesign ;


    public:
        /// Constructor
        TLGImportDialog(const TGWindow *p, const TGWindow *main, TString &result, const char *path) ;

        /// Destructor
        ~TLGImportDialog() ;

        /// Close Window - if user closes the dialog
        void CloseWindow() ;

        /// Process messages from the widgets.
        Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2) ;

        /// Populate the file combo box with file names.
        Bool_t AddFiles(const char *dir) ;

        /// Read the file selected by the user
        Bool_t ReadFile(const char *dir, const char *file) ;
    } ;
// JCB - end

// JCB - start
// Import Matlab Design dialog box
//********************************************************************
    class TLGImportMLDialog : public ligogui::TLGTransientFrame{
    protected:
	/// Return value from import
	char 		*fRet ;
        /// Group - This is the group that holds the input widgets
        TGGroupFrame*     fG;
        /// Frame for OK, Cancel buttons.
        TGHorizontalFrame *fFButton ;
        /// Layout hints for widgets
        TGLayoutHints    *fL[9];
        /// Labels (Path:, File:)
        TGLabel*          fLabel[2];
        /// Path selection combo box.
        TGFSComboBox*     fPath;
        /// Directory up button
        TGPictureButton*  fCdup;
        /// Picture for fCdup directory up button.
        const TGPicture*  fPcdup;
        /// Directory list view
        TGListView*       fFv;
        /// Directory list view container
        TGFileContainer*  fFc;
        /// File selection combo box
        TGComboBox*       fFile;
        /// Ok button.
        TGTextButton    *fOk ;
        /// Cancel button
        TGTextButton    *fCancel ;
        /// Text box to display what's in the file. (file preview)
        TGTextView      *fText ;
        /// Design string from the file.
        std::string             fDesign ;

        /// Process messages from the widgets.
        Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2) ;

        /// Populate the file combo box with file names.
        Bool_t AddFiles(const char *dir) ;


    public:
        /// Constructor
        TLGImportMLDialog(const TGWindow *p, const TGWindow *main, char *result, const char *path) ;

        /// Destructor
        ~TLGImportMLDialog() ;

        /// Close Window - if user closes the dialog
        void CloseWindow() ;
    } ;
// JCB - end

// Generic field dialog box, asks the user for input.
//********************************************************************
   class TLGInputDialog : public TGTransientFrame {
      protected:
         /// Return string
         char	                *fRet ;
	 /// Dialog box title
         std::string		fTitleTxt;
	 /// Label for input field
         std::string		fLabelTxt;
         /// Layout hints
         TGLayoutHints          *fL[7] ;
         /// Frames
         TGCompositeFrame       *fF ;
         TGCompositeFrame       *fFText ;
         /// Labels
         TGLabel                *fLabel ;
         /// Text Entry
         TGTextEntry            *fName ;
         /// Button frame
         TGCompositeFrame       *fFButton ;
         /// Ok, and Cancel buttons
         TGButton               *fButton[2] ;

      public:
         /// Constructor
         TLGInputDialog(const TGWindow *p, const TGWindow *main, 
			const char *dialog_name, const char *label,
			char *result) ;
         /// Destructor
         virtual ~TLGInputDialog() ;
         /// Close window method
         virtual void CloseWindow() ;
         /// Process Messages
         virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2) ;
   } ;

}


#endif // _LIGO_TLGFILTERDLG_H
