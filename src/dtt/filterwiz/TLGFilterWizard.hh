/* version $Id: TLGFilterWizard.hh 7856 2017-02-22 20:34:46Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGFILTERWIZARD_H
#define _LIGO_TLGFILTERWIZARD_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGFilterWizard						*/
/*                                                         		*/
/* Module Description: Filter Wizard					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGFilterWizard.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include "TLGEntry.hh"
#include "TLGMainWindow.hh"
#include "FilterDesign.hh"
#include "FilterFile.hh"
#include "TDGTextEdit.h"
#include <set>


   class TGFSComboBox;
   class TGListView;
   class TGFileContainer;
   class TGPictureButton;
   class TGTextView;
   class TGTextEdit;
   class PlotDescriptor;
namespace ligogui {
   class TLGNumericControlBox;

   class TLGComboBox;
   class TLGTextEntry ;
}
namespace calibration {
   class Table;
}

namespace filterwiz {


/** Filter wizard frame.
   
    @memo Filter wizard frame
    @author Written July 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGFilterWizWindow : public TGVerticalFrame, public TGWidget {
   public:
      /// Section selection
      typedef std::set<int> section_sel;
      /// set gain sign choice
      enum sign_type {
      /// same 
      kSignSame = 0,
      /// same 
      kSignPlus = 1,
      /// same 
      kSignMinus = 2};
   
   protected:
      /// simple edit? Only edits a single filter. Used for creating a filter from diaggui.
      bool		fSimple;
      /// Read only, controlled by command line option or file menu command.
      Bool_t		fReadOnly;
      /// Closed loop?
      Bool_t		fClosedLoop;
      /// Negative sign in closed loop?
      Bool_t		fClosedLoopNeg;
      /// Current filter design
      std::string*	fFilter;
      /// Current file system directory
      TString		fCurDir;
      /// Current filter file name
      TString		fCurFile;
      /// Current filter file contents
      FilterFile	fFilterFile;
      /// Current module
      FilterModule*	fCurModule;
      /// Section selection active? It's possible to have no active section checked in Section.
      Bool_t		fCurSectionActive;
      /// Current section indices - a set of ints which are the
      /// indicies of the filter sections selected in the Sections panel.
      section_sel	fCurSections;
      /// Switch selection active?
      Bool_t		fCurSwitchActive;
      /// Design selection active?
      Bool_t		fCurDesignActive;
      /// Has the file been changed?
      Bool_t		fDirty;
      // gain(1) filters allowed if true - JCB
      Bool_t		fGain1Allowed ;

      bool		fAnySample ;
      bool		fExperimentMode ;
   
      /// Frames
      TGCompositeFrame* fF[6];
      /// Group frames. Module Selection, Sections, Switching, Design, and Plotting frames.
      TGGroupFrame* 	fG[5];
      /// Layout hints
      TGLayoutHints*	fL[8];
      /// Labels - all of the various text labels on the window.
      TGLabel*		fLabel[30];
      /// Path - The pulldown menu that shows the full path to the current directory.
      TGFSComboBox*	fPath;
      /// Directory up button - Move up one level when pressed.
      TGPictureButton*	fCdup;
      /// Picture for fCdup - The directory up icon in the button.
      const TGPicture*	fPcdup;
      /// Directory list view - Lists all the directories below the current level.
      TGListView* 	fFv;
      /// Directory list view container
      TGFileContainer*	fFc;
      /// File - a pulldown menu that allows a file from the current directory to be selected.
      TGComboBox*	fFile;
      /// Module - SFM in the current file.
      TGComboBox*	fModule;
      /// Multiple sections - Allows single or multiple sections to be selected.
      TGComboBox*	fSectMul;
      /// Picture for SectSel
      const TGPicture*	fPSectSel[2];
      /// Select all/none section button
      TGButton*		fSectSel[2];
      /// Sections - Check box to indicate a section is active.
      TGCheckButton*	fSect[kMaxFilterSections];
      /// Section names - Editable names for the filter sections
      ligogui::TLGTextEntry*	fSectName[kMaxFilterSections];
      /// Copy-Paste-Cut-Undo buttons for the filter sections.
      TGButton          *fSectCP[4] ;   // JCB
      /// Input switching
      ligogui::TLGComboBox*	fSwitchInp;
      /// Output switching
      ligogui::TLGComboBox*	fSwitchOut;
      /// Switching parameters
      ligogui::TLGNumericControlBox*	fSwitchParam[3];
      /// Plotting parameters
      ligogui::TLGNumericControlBox*	fPlotParam[4];
      /// Plot type
      TGComboBox*	fPlotType;
      /// Sample rate
      // JCB - Change to combo box, need to be able to set sample
      //       rate if this is invoked from diaggui.  Make non-editable
      //       if invoked as foton.
      ligogui::TLGComboBox	*fSample ;

      /// Command entry
      TDGTextEdit*		fCommand;
      /// s-plane parameters
      TGTextView*	fAlt;
      /// s/z-plane formats
      TGButton*		fSZFormat[8];
      /// Design buttons for Gain, ZPK, RPoly, etc.
      TGButton*		fAdd[13];
      /// Import Design button (from external file)
      TGButton*         fImportDesign ; // JCB
      /// Set gain frequency
      ligogui::TLGNumericControlBox*	fSetGainF;
      /// Set gain value
      ligogui::TLGNumericControlBox*	fSetGainVal;
      /// Set gain unit
      ligogui::TLGComboBox*	fSetGainUnit;
      /// Set gain current value
      ligogui::TLGTextEntry*	fSetGainCurrent;
      /// set gain sign
      TGTextButton*	fSetGainSign;
      /// set gain
      TGButton*		fSetGain;   
      /// Set gain unit
      ligogui::TLGComboBox*	fSetGainCurrentUnit;
   
      /// Add files to file combobox. Only filter files are added.
      Bool_t AddFiles (const char* dir, const char* file);
   
      /// Set the states of the section editing buttons (enabled, disabled)
      /// according to whether sections are selected, have been copied or modified.
      void SetSectButtonStates() ; // JCB

      /// Reference for TGMsgBox classes to keep them within the window.
      const TGWindow    *fParent ;

      /// Button to show errors when file is read.  Takes the place of fLabel[1].
      TGButton		*fFileError ;

   public:
      /** Constructs a filter wizard window.
          @memo Constructor.
          @param p Parent window
          @param fdesign Filter design class
   	  @param id Widget id
       ******************************************************************/
      TLGFilterWizWindow (const TGWindow* parent, std::string& fdesign,
                        bool simple, const char* path, const char* file, 
                        Int_t id = -1, const char *module = 0, int section = 0,
			bool any_sample = 0, bool experiment_mode = 0);
      /// Destructor
      virtual ~TLGFilterWizWindow ();
   
      /// Setup files
      virtual Bool_t Setup (const char* dir, const char* file = 0, const char* module = 0);

      /// Update file selection area
      virtual Bool_t UpdateFileSelection();

      virtual Bool_t SetExperimentMode(Bool_t mode) ;

      /// Get readonly flag
      virtual Bool_t ReadOnly() const;

      /// Set readonly flag
      virtual Bool_t SetReadOnly (Bool_t readonly = kTRUE);

      /// Get Legacy Write flag
      virtual Bool_t LegacyWrite() { return fFilterFile.LegacyWrite(); } // JCB

      /// Set legacy write flag
      virtual void SetLegacyWrite(Bool_t legacyMode) { fFilterFile.setLegacyWrite(legacyMode); } // JCB

      // Get Gain 1 allowed flag
      Bool_t Gain1Allowed() { return fGain1Allowed; } // JCB

      // Set Gain 1 allowed flag
      void SetGain1Allowed(Bool_t allowed) { fGain1Allowed = allowed; } // JCB

      /// Get the 0 SOS allowed flag
      Bool_t Sos0Allowed() { return fFilterFile.sos0Allowed() ; } // JCB

      /// set the flag to allow 0 SOS.
      void Set0SosAllowed(Bool_t sos0) { fFilterFile.set0SosAllowed(sos0); } // JCB

      /// Get closed loop flag
      virtual Bool_t ClosedLoop() const { return fClosedLoop; }

      /// Set closed loop flag
      virtual void SetClosedLoop (Bool_t closedloop = kTRUE) { fClosedLoop = closedloop; }

      /// Get closed loop neg flag
      virtual Bool_t ClosedLoopNeg() const { return fClosedLoopNeg; }

      /// Set closed loop neg flag
      virtual void SetClosedLoopNeg (Bool_t closedloopneg = kTRUE) { fClosedLoopNeg = closedloopneg; }

      /// Is dirty
      virtual Bool_t IsDirty() const { return fDirty; }

      /// Set dirty flag
      virtual void SetDirty (Bool_t on = kTRUE);

      /// Check dirty flag and save if necessary
      virtual Bool_t CheckDirty();

      /// Check validity of filters
      virtual Bool_t IsValid (Bool_t msgbox = kTRUE);
   
      /// Add new module
      virtual Bool_t AddNewModule(const char *name) ;

      /// New file
      virtual Bool_t NewFile() ;

      /// Read file
      virtual Bool_t ReadFile (const char* file, const char* module = 0);

      /// Save function
      virtual Bool_t SaveFile();

      /// Save As function
      using TObject::SaveAs; // Get the clang compiler to not warn about hiding overloaded virtual function.
      virtual Bool_t SaveAs();

      /// Loads coeffcients into online filter module
      virtual Bool_t LoadCoeff ();

      /// Set gain
      virtual Bool_t SetGain (double f, double g, sign_type sign = kSignSame);
   
      /// Select module
      virtual Bool_t SelectModule (const char* module);

      /// Select section
      virtual Bool_t SelectSections();

      /// Update sections
      virtual Bool_t UpdateSections (Bool_t toGUI = kTRUE, Bool_t force = kFALSE);

      /// Update switch parameters
      virtual Bool_t UpdateSwitchParams();

      /// Update switching
      virtual Bool_t UpdateSwitching (Bool_t toGUI = kTRUE, Bool_t force = kFALSE);

      /// Update design
      virtual Bool_t UpdateDesign (Bool_t toGUI = kTRUE, Bool_t force = kFALSE);

      /// Update zero/pole and z-transform of design
      // Return the count of sos.
      virtual Int_t UpdateDesignZP();

      /// Update all
      virtual Bool_t Update (Bool_t toGUI = kTRUE, Bool_t force = kFALSE);

      /// Update filter (simple only)
      virtual Bool_t UpdateFilter();
   
      /// Make bode plot (plot desc owned by caller!)
      virtual PlotDescriptor* PlotBode ();

      /// Make response plot (plot desc owned by caller!)
      virtual PlotDescriptor* PlotResponse (const char* resp = "step");

      /// Makes the root plot (plot desc owned by caller!)
      virtual Bool_t PlotRoots (PlotDescriptor*& poles, PlotDescriptor*& zeros, bool splane = true);

      /// Returns a unit circle (plot desc owned by caller!)
      static PlotDescriptor* UnitCircle (int segments = 1024);
   
      /// Process add filter messages
      Bool_t AddFilter (Long_t parm1);

      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t parm2);

      // Set the fParent variable to allow TGMsgBox windows to be attached to the correct window.
      void setParentWindow(const TGWindow *parent) { fParent = parent ; } ;

      /// Merge matlab designs into current filter file - JCB
      bool MergeFile(const char *filename) { return fFilterFile.merge(filename) == 0; } ;

      /// Set the debug level for matlab file merge. - JCB
      void setMergeDebug(int val) { fFilterFile.setMergeDebug(val); } ;

      /// Print the errors from the matlab merge. - JCB
      void printMergeErrors() { return fFilterFile.printMergeErrors(); } ;

      /// Write a filter file. - JCB
      bool WriteFile(const char *filename) { return fFilterFile.write(filename); } ;

      /// Get the merge errors from the matlab merge. - JCB
      std::vector<std::string> *getMergeErrors() { return fFilterFile.getMergeErrors(); } ;

      /// Clear the merge errors from matlab merge. - JCB
      void clearMergeErrors() { fFilterFile.clearMergeErrors(); } ;

      /// Get the current directory
      const char *GetCurDir() {return fCurDir.Data(); } ; // JCB

      void FileErrorIndication(Bool_t error) ;
   };



/** Filter wizard main window. This is the main window of the 
    Filter Online Tool. It adds filter design and filter select 
    functions to the TLGMainWindow class.  TLGMainWindow also
    inherits from TLGMainFrame and TLGMainMenu.
   
    @memo Filter wizard main window
    @author Written July 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGFilterWizard : public ligogui::TLGMainWindow {
   protected:
      /// modal window?
      bool		fModal;

      /// return value
      bool*		fReturn;

      /// Current filter design
      std::string*	fFilter;

      /// Init path
      const char*	fIniPath;

      /// Init file
      const char*	fIniFile;

      /// Init module
      const char	*fIniModule ;

      /// Init section
      int		fIniSection ;

      bool		fAnySample ;

      bool		fExperimentMode ;

      /// Tab which contains filter design tools and graphics pads
      TGTab*		fMainTab;

      /// Filter design window
      TLGFilterWizWindow*	fFilterDesign;

      /// Monitor selection tab
      TGCompositeFrame* fFilterTab;

      /// Graphics pad tab
      TGCompositeFrame* fGraphicsTab;

      /// Layout hints for filter tab
      TGLayoutHints*	fFilterLayout;
   
   public:
      /** Constructs a main foton window. If modal is set false, this
          is an ordinary main window. If it is set true, this window
          should be used like a dialog box. The modal argument is then
          also used to return true if it was quit with OK, and false,
          if it was quit with cancel.
          @memo Constructor
          @param p Parent window
          @param modal true if modal window
          @param title Window title
          @param fdesign Filter design (only used if modal)
          @param path Path to look for filter files
          @param file File name of filter file
       ******************************************************************/
      TLGFilterWizard (const TGWindow *p, bool& modal,
                      const char* title, std::string& fdesign,
                      const char* path = 0, const char* file = 0,
		      const char *module = 0, int section = 0,
		      bool any_sample = 0, bool experiment_mode = 0);

      /** Destructs the foton window.
          @memo Destructor.
       ******************************************************************/
      virtual ~TLGFilterWizard ();
   
      /** Close the main window and exit the program.
          @memo Close method
          @return void
       ******************************************************************/
      virtual void CloseWindow();
   
      /** Add buttons to the button array. The buttons should be
          constructed with the specified button context and font.
          Also buttons should be added to the parent window with the
          supplied button layout hints.
          This function is called by the setup.
          @memo Add buttons.
          @param p Parent window
          @param btns array to store button pointers
          @param max Maximum size of button array
          @param btnLayout Button layout hints
          @param btnGC Button foreground graphics context
          @param btnFont Button font
          @return Number of buttons added
       ******************************************************************/
      virtual int AddButtons (TGCompositeFrame* p, TGButton* btns[], 
                        int max, TGLayoutHints* btnLayout,
                        GContext_t btnGC = fgButtonGC, 
                        FontStruct_t btnFont = fgButtonFont);

      /** Add the main window. By default just adds a multipad, but can
          be overriden to implement a more complicated main window such
          as a tab where one of the tabs is the plot window. This 
          function should return a pointer to the multipad window.
          This function is called by the setup and the returned
          multipad will be freed by the destructor.
          @memo Add main window.
          @param p Parent window
          @param mainLayout Main window layout hints
          @param plotset Set of plot descriptors
          @param padnum Number of plot pads
          @return multipad
       ******************************************************************/
      virtual ligogui::TLGMultiPad* AddMainWindow (TGCompositeFrame* p,
                        TGLayoutHints* mainLayout,
                        PlotSet& plotset, int padnum = 2);

      virtual Bool_t SetExperimentMode(Bool_t mode) ;

      /** Get readonly flag.
          @memo Get readonly method
          @return true if readonly
       ******************************************************************/
      virtual Bool_t ReadOnly() const;

      /** Set readonly flag.
          @memo Set readonly method
          @return true if successful
       ******************************************************************/
      virtual Bool_t SetReadOnly (Bool_t readonly = kTRUE);

      /** File save function.
          @memo File save method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileSave();

      /** File save as function.
          @memo File save as method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileSaveAs();

      /** File Write function. Writes a filter file to file filename. */
      /** This is a simple write file function that doesn't present any
        * GUI elements.  This also doesn't check the ReadOnly() status,
        * so use some caution.  The method was created to allow a 
        * command-line batch processing of matlab merge.
        */
      Bool_t WriteFile(const char *filename) { return fFilterDesign->WriteFile(filename); } ;
      Bool_t MergeFile(const char *filename) { return fFilterDesign->MergeFile(filename); } ;
      void printMergeErrors() { fFilterDesign->printMergeErrors(); } ;
      void clearMergeErrors() { fFilterDesign->clearMergeErrors(); } ;
      void setMergeDebug(int val) { fFilterDesign->setMergeDebug(val); } ;
      std::vector<std::string> *getMergeErrors() { return fFilterDesign->getMergeErrors(); } ;


      /** Loads coeffcients into online filter module.
          @memo Loads coeffcients method.
       ******************************************************************/
      virtual Bool_t LoadCoeff ();

      /** Handles the bode plot. Called when the Bode Plot button is pressed.
          @memo Bode plot method.
       ******************************************************************/
      virtual Bool_t BodePlot ();

      /** Handles the step/impulse/ramp response plots. Called when the
          Step Response button is pressed.
          @memo Response method.
       ******************************************************************/
      virtual Bool_t ResponsePlot (const char* resp = "step");

      /** Handles the root plot.  Called when the s-Plane Roots button is
          pressed.
          @memo Root plot method.
       ******************************************************************/
      virtual Bool_t RootPlot (bool splane = true);

      /** Add the file menu to the menu bar at the top of the window.
          @memo Add file menu.
          @return void
       ******************************************************************/
      virtual void AddMenuFile();

      /** Add the plot menu to the menu bar at the top of the window.
          @memo Add plot menu.
          @return void
       ******************************************************************/
      virtual void AddMenuPlot();

      virtual void AddMenuHelp();

      /** Process button messages for Bode Plot, Step Response, s-Plane Roots,
          Save,  Load Coefficient, Exit buttons from the bottom of the window.
	  Also handles Cancel and OK from dialogs for this window.  Any other button 
	  events are passed to the TLGMainWindow ProcessButton method.
          @memo Process button method.
          @param parm1 First message parameter - indicates which button was pressed.
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessButton (Long_t parm1, Long_t);

      /** Process menu messages for the File, Plot, and Help menus from the 
          main menu bar. Other menu messages are passed to the TLGMainWindow
	  ProcessMenu method.
          @memo Process menu.
	  @param param1 Indicates which menu item was selected.
	  @param param2 Additional information about the message.
          @return true if processed
       ******************************************************************/
      virtual Bool_t ProcessMenu (Long_t parm1, Long_t parm2);

      /** Set legacy write flag - enable iLIGO filter file output,
	  which uses 14 digit precision on floating point numbers and does
	  not output gain-only filters with 0 second-order sections.
          @memo Set legacy write method
	  @param legacyMode Boolean value is true if legacy mode is to be used.
          @return true if successful
       ******************************************************************/
      virtual Bool_t SetLegacyWrite (Bool_t legacyMode = kFALSE);

      /** Get legacy write flag.
          @memo Get legacy write flag from filter file class.
	  @return true if Legacy Write is enabled.
       ******************************************************************/
      virtual Bool_t LegacyWrite () ;

      /** Set Gain 1 allowed flag - This allows design strings of gain(1)
          which should result in zpk([],[],1) filters if enabled.
       ******************************************************************/
      virtual void SetGain1Allowed(Bool_t allowed = kFALSE) ;

      /** Get gain 1 allowed state.
       ******************************************************************/
      Bool_t Gain1Allowed() { return fFilterDesign->Gain1Allowed(); };

   };

}

#endif // _LIGO_TLGFILTERWIZARD_H
