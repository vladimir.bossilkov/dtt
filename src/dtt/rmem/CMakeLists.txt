CREATE_RPC_TARGET_NO_SVC(rtestpoint)

SET(TP_CLIENT_VERSION 1.0.0)

SET(RMEM_SRC
    #testpoint_server.c
    testpoint.c
    testpointinfo.c
    testpointmgr.cc
)

add_library(testpoint SHARED
        ${RMEM_SRC}
        )

target_compile_definitions(testpoint PRIVATE
        -D_TP_CLIENT_VERSION="${TP_CLIENT_VERSION}"
        )

target_include_directories(testpoint PRIVATE
        ${DTT_INCLUDES}
        ${RPC_OUTPUT_DIR}
        )

add_dependencies(testpoint
        rtestpoint_rpc

        )

target_link_libraries(testpoint PUBLIC
        sockutil
        #gdsmath
        fftw3f
        fftw3
        gdsbase
        z
        ${READLINE_LIBRARIES}
        tinfo
        dl
        rt
       
        pthread
        rtestpoint_rpc
        util_lib
        gdsrsched_rpc
        conf_lib
        sched_lib
        daq_lib
        rlaunch_rpc
        rchannel_rpc
        rleap_rpc
)

set_target_properties( testpoint
        PROPERTIES
        VERSION 0.0.0
        PUBLIC_HEADER testpoint.h
        PRIVATE_HEADER
            "testpointinfo.h;testpointmgr.hh;testpoint_structs.h;testpoint_interface_v3.h;rmorg.h;map_v3.h;map_v2.h;map_v1.h;map.h;hardware.h;daq_core_defs.h;daq_core.h"
        )

INSTALL_LIB(testpoint)