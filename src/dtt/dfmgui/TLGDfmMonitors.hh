/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGDfmMonitors						*/
/*                                                         		*/
/* Module Description: ROOT Dialogbox for DFM				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmdlgroot.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_TLGDFMMONITORS_H
#define _LIGO_TLGDFMMONITORS_H

#include "TLGFrame.hh"
#include "montype.hh"
#include <TGFrame.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <vector>
#include <map>


namespace dfm {

   class selservers;

/** @name Dfm monitor selection 
    This header defines a ROOT dialogbox for the DFM monitor selection.
   
    @memo Dfm monitor selection
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


   const Int_t kDfmMonList = 200;
   const Int_t kDfmMonUDN = 201;
   const Int_t kDfmMonArg = 202;
   const Int_t kDfmMonAdd = 203;
   const Int_t kDfmMonRem = 204;
   const Int_t kDfmMonKill = 205;


/** Monitor selection dialog box.
    @memo Monitor selection dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmMonitorDlg : public ligogui::TLGTransientFrame {
   protected:
      /// Data access object
      const selservers& 	fSelServer;
      /// current server list
      MonitorList	fCurMonitors;
      /// server list (return)
      MonitorList*	fMonitors;
      /// Kill monitors when done (return)?
      bool*		fKillAfter;
      /// Return value
      Bool_t*		fOk;
      /// Current index value
      Int_t		fIndex;
   
      /// Monitor selection group
      TGGroupFrame*	fMonGroup;
      /// Monitor setup group
      TGGroupFrame*	fSetupGroup;
      /// Frames
      TGCompositeFrame*	fF[2];
      /// Selected monitor list
      TGListBox*	fSel;
      /// UDN selection
      TGComboBox*	fUDN;
      /// Argument field
      TGTextEntry*	fArg;
      /// Kill after done
      TGButton*		fKill;
      /// Labels
      TGLabel*		fLabel[2];
   
      /// Button frame
      TGCompositeFrame*	fBtnFrame;
      /// Add button
      TGButton*		fAddButton;
      /// Remove button
      TGButton*		fRemButton;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
      /// Layout hints
      TGLayoutHints*	fL[6];
   
      /// List of SM partitions
      typedef std::map<std::string, int> smlist;
      /// SM partition iterator
      typedef smlist::iterator sm_iter;
      /// SM list
      smlist		fSMs;
   
   public:
   
      /** Constructs a monitor selection dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param main Main window
   	  @param ret True if successful, False if cancel
       ******************************************************************/
      TLGDfmMonitorDlg (const TGWindow *p, const TGWindow *main,
                       const selservers& sel, MonitorList& monitors, 
                       bool& killwhendone, Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmMonitorDlg ();
      /// Close window
      virtual void CloseWindow();
      /// Set values
      virtual void SetValues (int idx, bool rebuild = false);
      /// Get values
      virtual void GetValues ();
      /// Add new monitor
      virtual Bool_t AddMon ();
      /// Remove a monitor
      virtual Bool_t RemMon (int idx);
   
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//@}

}


#endif // _LIGO_TLGDFMMONITORS_H
