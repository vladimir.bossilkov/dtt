/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGDfmServer						*/
/*                                                         		*/
/* Module Description: ROOT Dialogbox for DFM server selection		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmdlgroot.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_TLGDFMSERVER_H
#define _LIGO_TLGDFMSERVER_H


#include "TLGFrame.hh"
#include "dataacc.hh"
#include <TGFrame.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>


namespace ligogui {
   class TLGNumericControlBox;
}

namespace dfm {


/** @name Dfm server selection 
    This header defines a ROOT dialogbox for the DFM server selection.
   
    @memo Dfm server selection
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

   const Int_t kDfmSrvAvail = 300;
   const Int_t kDfmSrvSel = 301;
   const Int_t kDfmSrvAdd = 302;
   const Int_t kDfmSrvRemove = 303;
   const Int_t kDfmSrvNewType = 305;
   const Int_t kDfmSrvNewAddr = 306;
   const Int_t kDfmSrvNewPort = 307;
   const Int_t kDfmSrvNewAdd = 308;


/** New server selection dialog box.
    @memo New server selection dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmServerDlg : public ligogui::TLGTransientFrame {
   private:
      /// Source selection
      Bool_t		fSourceSel;
      /// Data access object
      dataaccess* 	fDacc;
      /// current server list
      selserverlist	fCurServers;
      /// server list (return)
      selserverlist*	fServers;
      /// Server selection string
      TString*		fServer;
      /// Return value
      Bool_t*		fOk;
      /// next ID number
      int		fId;
      /// Add server changed?
      Bool_t		fAddDirty;
   
      /// Selected server group
      TGGroupFrame*	fSelGroup;
      /// Available server group
      TGGroupFrame*	fAvailGroup;
      /// New server group
      TGGroupFrame*	fNewGroup;
      /// Frames
      TGCompositeFrame*	fF[3];
      /// Selected list
      TGListBox*	fSel;
      /// Available list
      TGListBox*	fAvail;
      /// Add button
      TGButton*		fAddButton;
      /// Remove button
      TGButton*		fRemButton;
      /// New server type
      TGComboBox*	fNewType;
      /// New server address
      TGTextEntry*	fNewAddr;
      /// New server port
      ligogui::TLGNumericControlBox*	fNewPort;
      /// New button
      TGButton*		fNewButton;
      /// Labels
      TGLabel*		fLabel[3];
      /// Button frame
      TGCompositeFrame*	fBtnFrame;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
      /// Layout hints
      TGLayoutHints*	fL[10];
   
   public:
      /// Supported server types:
      enum suppserver {
      /// fantom
      kFantom = 1,
      /// NDS
      kNDS = 2,
      /// File
      kFile = 4,
      /// Tape
      kTape = 8,
      /// SM
      kSM = 16
      };
   
      /** Constructs a new server selection dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param main Main window
   	  @param dacc Data access server list
   	  @param sourcesel Source selection?
   	  @param list Selected list of servers (return)
   	  @param server Name of current server (return)
   	  @param ret True if successful, False if cancel
       ******************************************************************/
      TLGDfmServerDlg (const TGWindow *p, const TGWindow *main,
                      dataaccess& dacc, Bool_t sourcesel,
                      selserverlist& list, TString& server, Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmServerDlg ();
      /// Close window
      virtual void CloseWindow();
   
      /// Build selection list
      void Build (Bool_t sel = kTRUE, Bool_t init = kFALSE);
      /// Check LARS address
      Bool_t checkLARS (const char* serv, int port) const;
      /// Check NDS address
      Bool_t checkNDS (const char* serv, int port) const;
   
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//@}

}


#endif // _LIGO_TLGDFMSERVER_H
