/* -*- mode: c++; c-basic-offset: 3; -*- */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Program Name:  dmtviewer						     */
/*                                                                           */
/* Program Description:  ROOT gui for data monitor tool			     */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/* Header File List */
#include <time.h>
#include <thread_base.hh>
#include <sys/file.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "dmtviewer.hh"
#include "PlotSet.hh"
#include "TLGPad.hh"
#include "Xsil.hh"
#include "TLGSave.hh"
#include "monaccess.hh"
#include <TROOT.h>
#include <TApplication.h>
#include <TGMsgBox.h>
#include <string>
#include <iostream>
#include <cstdlib>

// Wider listboxes requires a wider overall window.  See
// TLGMonitor.cc for another WIDE_LISTBOXES define. The 
// two should be the same value.  
#ifndef WIDE_LISTBOXES
#define WIDE_LISTBOXES 1
#endif

namespace ligogui {
   using namespace std;
   using namespace monapi;

   // ROOT object
   TROOT root ("GUI", "DMT VIEWER");
   TString	protocol;


   // Command line arguments for dmt viewer
   const string 	_argHelp ("-h");
   const string 	_argHelpAlt ("-?");
   const string 	_argVerbose ("-v");
   const string 	_argDisplay ("-d");
   const string 	_argProtocol ("-p");
   const string 	_argRunMode ("-run");
   const string 	_argDisplayAlt ("-display");

   const char* const _helptext = 
   "Usage: dmtviewer [-d display] [filename]\n"
   "       -d 'display'  X windows display\n"
   "       -h            this help\n"
   "       -p 'protocol' Protocol used to get data (lmsg or web)\n"
   "       -run          Switch to run state after loading the display file\n"
   "  If no protocol is specified and the DMTWEBSERVER environment variable\n"
   "  is defined, the protocol is 'web'. If neither is specified but the\n"
   "  DMTNAMESERVER variable is defined, the protocol is lmsg. If none of\n"
   "  the above is specified, the default is 'web'.\n"
   "  Examples:\n"
   "      setenv DMTWEBSERVER sand.ligo-wa.caltech.edu:8090\n"
   "      setenv DMTWEBSERVER sand.ligo-wa/LHO,delaronde.ligo-la/LLO\n"
   "  The format is 'address'[:'port'][/'nick name']{, more servers...}\n";



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// UpdataDataTask                                                       //
//                                                                      //
// Get data from the DMT monitor servers                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   class UpdataDataTask : public NotificationMessage, thread::thread_base {
   public:
      /// Work list type
      typedef deque<TLGMonitorDatum> worklist;
      /// Result list
      typedef deque<PlotDescriptor*> datalist;
   
   protected:
      /// Plot pool
      PlotSet*		fPlot;
      /// Calibration table
      calibration::Table* fCalTable;
      /// Window for notification message
      DMTViewer*	fWin;
      /// Work queue
      worklist		fWorkQueue;
      /// Result queue
      datalist 		fResultQueue;
   
   public:
      /// Constructor
      UpdataDataTask (DMTViewer* win, PlotSet* pl, calibration::Table* cal) 
      : fPlot (pl), fCalTable (cal), fWin (win) {
      }
      /// Destructor
      virtual ~UpdataDataTask () {
         for (datalist::iterator i = fResultQueue.begin();
             i != fResultQueue.end(); i++) {
            delete *i;
         }
      }
      /// Process
      virtual void Process (TLGMainWindow& win) {
         for (datalist::iterator i = fResultQueue.begin();
             i != fResultQueue.end(); i++) {
            if (*i) {
               if (fCalTable) fCalTable->AddUnits ((*i)->Cal());
               fPlot->Add (*i);
               *i = 0;
            }
         }
         if (!fResultQueue.empty()) {
            fPlot->Update();
         }
         cout << "Queue size = " << fResultQueue.size() << endl;
         fWin->SetUpdateProgress (kFALSE);
      }
      /// Get the work list
      worklist& GetWorkList () {
         return fWorkQueue; }
      /// Get the queus
      datalist& GetQueue () {
         return fResultQueue; }
      /// Get the data 
      void GetData () {
         for (worklist::iterator i = fWorkQueue.begin();
             i != fWorkQueue.end(); i++) {
            PlotDescriptor* pd = i->GetData();
            if (pd) {
               fResultQueue.push_back (pd);
            }
         }
      }
      /// Get the data 
      PlotDescriptor* GetData (int index) {
         worklist::iterator i = fWorkQueue.begin();
         advance (i, index);
         return  i->GetData();
      }
      /// Send Notfication
      void Notify () {
         fWin->SendNotification (this);
      }

      /// start updates
      bool startUpdate(void) {
	 set_detached(true);
	 return !start_thread();
      }

      /// Threaded Update
      void* threadedUpdate();

      void* thread_entry() {
	 void* ret = 0;
	 if (protocol == "web") {
	    ret = threadedUpdate();
	 }
	 else {
	    GetData();
	 }
	 Notify();
	 return ret;
      }
   };

//______________________________________________________________________________
   class work_thread : public thread::thread_base {
   public:
      work_thread(void)
	 : _work(0), _index(0)
      {}
      void start(UpdataDataTask* work, int index) {
	 _work = work;
	 _index = index;
	 set_detached(false);
	 start_thread();
      }

      void* thread_entry(void) {
	 return _work->GetData(_index);
      }
   private:
      UpdataDataTask* _work;
      int             _index;
   };

//______________________________________________________________________________
   void*
   UpdataDataTask::threadedUpdate(void) {

      //  Spawn as many threads as there are requests.
      int max = GetWorkList().size();
      work_thread* wtlist = new work_thread[max];
      for (int i=0; i<max; ++i) {
	 wtlist[i].start(this, i);
         timespec tick = {0, 10000000};
         nanosleep (&tick, 0);
      }

      // wait for threads
      for (int i = 0; i < max; ++i) {
         void* pd = 0;
	 wtlist[i].join(&pd);
         if (pd) {
            GetQueue().push_back (reinterpret_cast<PlotDescriptor*>(pd));
         }
      }
      delete[] wtlist;
      return 0;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// DMTXMLRestorer                                                       //
//                                                                      //
// DMT XML restore class                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class DMTXMLRestorer : public TLGXMLRestorer {
   protected:
      /// Pointer to dmt viewer
      DMTViewer*	fDMTView;
      /// Monitor query 
      xsilHandlerQueryMonitor	fMon;
   public:
      /// Constructor
      DMTXMLRestorer (const char* filename, ERestoreFlag restoreflag, 
                     TString& error, DMTViewer* dmtview, 
                     std::ostream* os = 0)
      : TLGXMLRestorer (filename, restoreflag, error, os), 
      fDMTView (dmtview), fMon (&dmtview->GetMonitorList()) {
      }
      /// Setup
      virtual Bool_t Setup ();
   };

//______________________________________________________________________________
   Bool_t DMTXMLRestorer::Setup ()
   {
      Bool_t ret = TLGXMLRestorer::Setup();
      if (ret) {
         fXml.AddHandler (fMon);
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// DMTXMLSaver                                                          //
//                                                                      //
// DMT XML save class                                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class DMTXMLSaver : public TLGXMLSaver {
   protected:
      /// Pointer to dmt viewer
      DMTViewer*	fDMTView;
   public:
      /// Constructor
      DMTXMLSaver (const char* filename, ESaveFlag saveflag, 
                  TString& error, DMTViewer* dmtview, 
                  std::string* extra = 0)
      : TLGXMLSaver (filename, saveflag, error, extra), 
      fDMTView (dmtview) {
      }
      /// Setup
      virtual Bool_t Setup ();
   };

//______________________________________________________________________________
   Bool_t DMTXMLSaver::Setup ()
   {
      Bool_t ret = TLGXMLSaver::Setup();
      // save setup parameters of the dmt viewer
      if (ret) {
         if (!fDMTView->GetMonitorList().empty()) 
            *fOut << fDMTView->GetMonitorList() << endl;
         ret = !!*fOut;
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// DMTViewer                                                            //
//                                                                      //
// DMT viewer                                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   DMTViewer::DMTViewer (const TGWindow *p, const char* title)
   : TLGMainWindow (p, title), fRunning (kFALSE), 
   fUpdateInProgress (kFALSE) {
   }

//______________________________________________________________________________
   DMTViewer::~DMTViewer ()
   {
      if (fMPad) delete fMPad;
      fMPad = 0;
      delete fMonitorSel;
      delete fMainTab;
      delete fMonLayout;
   }

//______________________________________________________________________________
   TLGMultiPad* DMTViewer::AddMainWindow (TGCompositeFrame* p,
                     TGLayoutHints* mainLayout, PlotSet& plotset,
                     int padnum)
   {
      fMainTab = new TGTab (p, 10, 10);
      fMainTab->Associate (this);
      p->AddFrame (fMainTab, mainLayout);
      // add monitor tab
      fMonLayout = new TGLayoutHints (kLHintsCenterX | kLHintsCenterY, 
                           4, 4, 4, 4);
      fMonitorTab = fMainTab->AddTab (" Monitors ");
      fMonitorSel = new TLGMonitorSelection (fMonitorTab, fMonMgr, 
                           fDObjList, kMainFrameId + 1);
      fMonitorSel->Associate (this);
      fMonitorTab->AddFrame (fMonitorSel, fMonLayout);
   
      // add grpahics tab
      fGraphicsTab = fMainTab->AddTab (" Graphics ");
      if (padnum < 1) padnum = 1;
      TLGMultiPad* pad = new TLGMultiPad (fGraphicsTab, "Plot", plotset, 
                           kMainFrameId, padnum);
      pad->Associate (this);
      if (padnum > 0) pad->GetPad(0)->HidePanel (kFALSE);
      if (padnum > 1) pad->GetPad(1)->HidePanel (kFALSE);
      fGraphicsTab->AddFrame (pad, mainLayout);
      return pad;
   }

//______________________________________________________________________________
   Bool_t DMTViewer::ButtonUpdate ()
   {
      if (UpdateInProgress()) {
         return kFALSE;
      }
      UpdataDataTask* work = new UpdataDataTask (this, fPlot, fCalTable);
      for (TLGMonitorDatumList::iterator i = fDObjList.begin();
          i != fDObjList.end(); i++) {
         if (i->second->GetUpdateOpt() != TLGMonitorDatum::kMonUpdateNever) {
            work->GetWorkList().push_back (*(i->second));
            i->second->Touch();
         }
      }
      if (work->GetWorkList().empty()) {
         delete work;
      }
      else {
         // start new thread
         SetUpdateProgress (kTRUE);
         if (!work->startUpdate()) {
            delete work;
            SetUpdateProgress (kFALSE);
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t DMTViewer::ButtonRun ()
   {
      if (fRunning) {
         return kTRUE; 
      }
      fRunning = kTRUE;
      fRunNumber = 0;
      fButtons[kB_RUN]->SetState (kButtonDisabled);
      fButtons[kB_STOP]->SetState (kButtonUp);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t DMTViewer::ButtonStop ()
   {
      if (!fRunning) {
         return kTRUE; 
      }
      fRunning = kFALSE;
      fRunNumber = 0;
      SetStatusMsg ("");
      fButtons[kB_STOP]->SetState (kButtonDisabled);
      fButtons[kB_RUN]->SetState (kButtonUp);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t DMTViewer::HandleTimer (TTimer* timer)
   {
      // check if running (respect skip hearbeat)
      if (!fSkipHeartbeats && fRunning && !UpdateInProgress()) {
         UpdataDataTask* work = new UpdataDataTask (this, fPlot, fCalTable);
         for (TLGMonitorDatumList::iterator i = fDObjList.begin();
             i != fDObjList.end(); i++) {
            if (i->second->Ready()) {
               work->GetWorkList().push_back (*(i->second));
               i->second->Touch();
            }
         }
         if (work->GetWorkList().empty()) {
            delete work;
         }
         else {
            // start new thread
            SetUpdateProgress (kTRUE);
            if (!work->startUpdate()) {
               delete work;
               SetUpdateProgress (kFALSE);
            }
            else {
               char msg[1024];
               sprintf (msg, "Run number %i", ++fRunNumber);
               SetStatusMsg (msg);
            }
         }
      }
   
      return TLGMainWindow::HandleTimer (timer);
   }

//______________________________________________________________________________
   TLGRestorer* DMTViewer::GetXMLRestorer (const char* filename, 
                     ESaveRestoreFlag restoreflag, TString& error,
                     ostream* extra)
   {
      return new (nothrow) DMTXMLRestorer 
         (filename, (TLGXMLRestorer::ERestoreFlag)restoreflag, 
         error, this, extra);
   }

//______________________________________________________________________________
   TLGSaver* DMTViewer::GetXMLSaver (const char* filename, 
                     ESaveRestoreFlag saveflag, 
                     TString& error, string* extra)
   {
      return new (nothrow) DMTXMLSaver 
         (filename, (TLGSaver::ESaveFlag)saveflag, error, this, extra);
   }

//______________________________________________________________________________
   Bool_t DMTViewer::ClearResults (Bool_t askfirst, Bool_t all)
   {
      Bool_t ret = TLGMainWindow::ClearResults (askfirst, all);
      if (ret) {
         if (!fDObjList.empty()) {
            fDObjList.clear();
            fMonitorSel->BuildActiveList(0);
         }
      }
      return ret;
   }

//____________________________________________________________________________
  Bool_t DMTViewer::TabSelect (const char* tab) {
      return kTRUE; //fMainTab->SetTab(tab);
  }

//______________________________________________________________________________
   Bool_t DMTViewer::ShowPlots ()
   {
      if (!fDObjList.empty()) {
         fMonitorSel->BuildActiveList(0);
      }
      return TLGMainWindow::ShowPlots ();
   }



}
   using namespace ligogui;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// main                                                                 //
//                                                                      //
// Main Program                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   int main (int argc, char **argv)
   {
      // parse arguments
      Int_t	rootArgc = 1;
      char*	rootArgv[100] = {0};
      Int_t	dmtArgc = 1;
      char*	dmtArgv[100] = {0};
      bool 	verbose = false;
      bool      runMode = false;
      TString	filename;
   
      rootArgv[0] = strdup (argv[0]);
      dmtArgv[0] = strdup (argv[0]);
      for (int i = 1; i < argc; i++) {
         // look for verbose
         if (_argVerbose == argv[i]) {
            verbose = true;
         }
         // look for help
         else if ((_argHelp == argv[i]) ||  (_argHelpAlt == argv[i])) {
            cout << _helptext;
            return 0;
         }
         // look for run mode
         else if (_argRunMode == argv[i]) {
	     runMode = true;
	 }
         // look for x display
         else if (((_argDisplay == argv[i]) || (_argDisplayAlt == argv[i])) && 
                 (i + 1 < argc) && (argv[i+1][0] != '-')) {
            if (rootArgc < 99) {
               rootArgv[rootArgc] = strdup ("-display");
               rootArgv[rootArgc+1] = strdup (argv[i + 1]);
               rootArgc += 2;
            }
            i++;
         }
         // look for protocol
         else if ((_argProtocol == argv[i]) && 
                 (i + 1 < argc) && (argv[i+1][0] != '-')) {
            protocol = argv[i+1];
            i++;
         }
         // look for file name (must be last argument)
         else if ((i + 1 == argc) && (argv[i][0] != '-')) {
            if (dmtArgc < 99) {
               dmtArgv[dmtArgc] = strdup ("-f");
               dmtArgv[dmtArgc+1] = strdup (argv[i]);
               dmtArgc += 2;
               filename = argv[i];
            }
         }
         // otherwise assume ROOT argument
         else {
            if (rootArgc < 100) {
               rootArgv[rootArgc] = strdup (argv[i]);
               rootArgc++;
            }
         }
      }
   
      // if not verbose disable std out and err
      if (!verbose) {
         int i = open ("/dev/null", 2);
         (void) dup2(i, 1);
         (void) dup2(i, 2);   
      }
   
      // set protocol
      if ((protocol.Length() == 0) && ::getenv ("DMTWEBSERVER")) {
         protocol = "web";
      }
      else if ((protocol.Length() == 0) && ::getenv ("DMTNAMESERVER")) {
         protocol = "lmsg";
      }
      else if ((protocol.Length() == 0)) {
         protocol = "web";
      }
      cout << "Protocol is " << protocol << endl;
      monapi::monaccess::setDefaultInterface (protocol);
   
      // initializing root environment
      TApplication theApp ("DMT Viewer", &rootArgc, rootArgv);
   
      // create main window
      DMTViewer mainWindow (gClient->GetRoot(), "DMT Viewer");
#ifdef WIDE_LISTBOXES
      mainWindow.SetupWH (1250, 870, filename) ;
#else
      mainWindow.Setup (filename) ;
#endif
      // Start in run mode
      if (runMode) {
	  mainWindow.ShowPlots();
	  mainWindow.TabSelect(" Graphics ");
	  mainWindow.ButtonRun();
      }
      // run the GUI
      theApp.Run();

      //---------------------------  Delete arguments.
      for (int i=0; i<rootArgc; ++i) free(rootArgv[i]);
      for (int i=0; i<dmtArgc; ++i)  free(dmtArgv[i]);
      return 0;
   }
