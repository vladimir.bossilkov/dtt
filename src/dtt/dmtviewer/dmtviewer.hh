/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dmtviewer						*/
/*                                                         		*/
/* Module Description: Viewer program for DMT.				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 1.0	 26Aug00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dmtviewer.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_DMTVIEWER_H
#define _LIGO_DMTVIEWER_H

#include <string>
#include <iostream>
#include <TGTab.h>
#include "TLGMainWindow.hh"
#include "TLGMonitor.hh"


#ifndef __NO_NAMESPACE
namespace ligogui {
#endif


/** @name dmtviewer
    This is the viewer of the data monitoring tool. It implements a 
    graphical user for displying time and frequency traces. It 
    communicates with server processes of the data monitoring tool
    to obtain "on-line" data.
   
    \begin{verbatim}
    Usage: dmtviewer [-d display] [filename]
           -d 'display'  X windows display
           -p 'protocol' Protocol used to get data (lmsg or web)
           -h            this help
    If no protocol is specified and the DMTWEBSERVER environment variable 
    is defined, the protocol is 'web'. If neither is specified but the 
    DMTNAMESERVER variable is defined, the protocol is lmsg. If none of 
    the above is specified, the default is 'web'.
    Examples:
        setenv DMTWEBSERVER sand.ligo-wa.caltech.edu:8090
        setenv DMTWEBSERVER sand.ligo-wa/LHO,delaronde.ligo-la/LLO
    The format is 'address'[:'port'][/'nick name']{, more servers...}
    \end{verbatim}

    @memo Viewer of the data monitoring tool
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** DMT viewer main window. This is the main window of the 
    DMT viewer. It adds monitor and data object lookup to the 
    TLGMainWindow class.

    @memo DMT viewer main window
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DMTViewer : public TLGMainWindow {
   protected:
      /// Manages the selction of monitors and data objects
      monapi::TLGMonitorMgr	 fMonMgr;
      /// Manages a list of currently selected monitor/data objects
      monapi::TLGMonitorDatumList 	fDObjList;
      /// Is running
      Bool_t		fRunning;
      /// Run number
      Int_t		fRunNumber;
      /// Update is in progress
      Bool_t 		fUpdateInProgress;
      /// Tab with contains monitor selection and graphics pads
      TGTab*		fMainTab;
      /// Monitor/data object selection tab
      TLGMonitorSelection*	fMonitorSel;
      /// Monitor selection tab
      TGCompositeFrame* fMonitorTab;
      /// Graphics tab
      TGCompositeFrame* fGraphicsTab;
      /// Layout hints for monitor selection
      TGLayoutHints*	fMonLayout;
   
   public:
      /** Constructs a main diagnostics viewer window.
          @memo Constructor
          @param p Parent window
          @param title Window title
          @param filename Name of file to be loaded
          @param padnum Number of initial plot pads
       ******************************************************************/
      DMTViewer (const TGWindow *p, const char* title);
      /** Destructs the main diagnostics window.
          @memo Destructor.
       ******************************************************************/
      virtual ~DMTViewer ();
   
      /** Add buttons to the button array. The buttons should be
          constructed with the specified button context and font.
          Also buttons should be added to the parent window with the
          supplied button layout hints.
          This function is called by the constructor and the returned
          multipad will be freed by the desctructor.
          @memo Add main window.
          @param p Parent window
          @param btns array to store button pointers
          @param max Maximum size of button array
          @param btnLayout Button layout hints
          @param btnGC Button foreground graphics context
          @param btnFont Button font
          @return Number of buttons added
       ******************************************************************/
      virtual int AddButtons (TGCompositeFrame* p, TGButton* btns[], 
                        int max, TGLayoutHints* btnLayout,
                        GContext_t btnGC = fgButtonGC, 
                        FontStruct_t btnFont = fgButtonFont) {
         return AddStdButtons (kButtonUpdate | kButtonRun | 
                              kButtonStop | kButtonExit, 
                              p, btns, max, btnLayout, btnGC, btnFont); }
      /** Add the main window. By default just adds a multipad, but can
          be overriden to implement a more complicated main window such
          as a tab where one of the tabs is the plot window. This 
          function should return a pointer to the multipad window.
          This function is called by the constructor and the returned
          multipad will be freed by the desctructor.
          @memo Add main window.
          @param p Parent window
          @param mainLayout Main window layout hints
          @param plotset Set of plot descriptors
          @param padnum Number of plot pads
          @return multipad
       ******************************************************************/
      virtual TLGMultiPad* AddMainWindow (TGCompositeFrame* p,
                        TGLayoutHints* mainLayout,
                        PlotSet& plotset, int padnum = 2);
      /** Handles the update button.
          @memo Update button method.
       ******************************************************************/
      virtual Bool_t ButtonUpdate ();
      /** Handles the run button.
          @memo Run button method.
       ******************************************************************/
      virtual Bool_t ButtonRun ();
      /** Handles the stop button.
          @memo Stop button method.
       ******************************************************************/
      virtual Bool_t ButtonStop ();
      /** Heartbeat timer handler.
          @memo Heartbeat method.
          @param timer Timer
          @return true if successful
       ******************************************************************/
      virtual Bool_t HandleTimer (TTimer* timer);
      /** Remove results from plot set. If askfirst is true a popup
          dialog box will ask the user and only change clear results
          if the user confirms.
          @memo Remove results method.
          @param askfirst Ask the user before removing
          @param all clear all traces including references
          @return true if results were removed
       ******************************************************************/
      virtual Bool_t ClearResults (Bool_t askfirst = kFALSE, 
                        Bool_t all = kTRUE);
      /** Shows the plots with the settings which were previously saved.
          This function will create plot windows as necessary.
          @memo Show plots.
          @return true if successful
       ******************************************************************/
      virtual Bool_t ShowPlots ();
   
      /** Is an update in progress?.
          @memo Updata in progress method.
          @return true if update in progress
       ******************************************************************/
      virtual Bool_t UpdateInProgress () const {
         return fUpdateInProgress; }
      /** Set update in progress flag.
          @memo set updata in progress method.
          @param state update flag
          @return true if update in progress
       ******************************************************************/
      virtual void SetUpdateProgress (Bool_t state) {
         fUpdateInProgress = state; }
      /** Select the specified tab.
          @memo Select the specied tab.
          @param Select tab
          @return true if tab selected
       ******************************************************************/
      virtual Bool_t TabSelect (const char* tab);
      /** Returns the list of currently selected monitor/data objects.
          @memo Get monitor list method.
          @return monitor list
       ******************************************************************/
      const monapi::TLGMonitorDatumList& GetMonitorList() const {
         return fDObjList; }
      monapi::TLGMonitorDatumList& GetMonitorList() {
         return fDObjList; }
   
   protected:
      /** Returns an XML restore object. This function is called 
          by GetRestorer, if the file type is XML.
          @param filename Name of file to restore
          @param restoreflag restore flag
          @param error string to hold error message
          @param extra stream for holding unrecognized xml objects
          @return Restorer or 0 on error
       ******************************************************************/
      virtual TLGRestorer* GetXMLRestorer (const char* filename,
                        ESaveRestoreFlag restoreflag, TString& error,
                        std::ostream* extra = 0);
      /** Returns an XML save object. This function is called 
          by GetSaver, if the file type is XML.
          @param filename Name of file to restore
          @param restoreflag restore flag
          @param error string to hold error message
          @param extra stream for holding additional xml objects
          @return Saver or 0 on error
       ******************************************************************/
      virtual TLGSaver* GetXMLSaver (const char* filename,
                        ESaveRestoreFlag restoreflag, TString& error,
                        std::string* extra = 0);
   
   };

//@}

#ifndef __NO_NAMESPACE
}
#endif

#endif // _LIGO_DMTVIEWER_H



