/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: confinfo						*/
/*                                                         		*/
/* Module Description: implements functions for controlling the AWG	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE 1
#endif

/*#ifndef DEBUG
#define DEBUG
#endif*/

/* Header File List: */
#include <stdlib.h>
#include <tconv.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#ifdef OS_VXWORKS
#include <vxWorks.h>
#include <semLib.h>
#include <ioLib.h>
#include <selectLib.h>
#include <taskLib.h>
#include <sockLib.h>
#include <inetLib.h>
#include <hostLib.h>
#include <sysLib.h>
#include <timers.h>

#else
#include <stdio.h>
#include <sys/types.h>
#include <netdb.h>	/* For gethostbyname() */
#include <sys/socket.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>
#endif

#include "gdsstring.h"
#include "gdstask.h"
#include "confinfo.h"
#include "gdsndshost.h"
#include "gdsmain.h" // test to see if __init__ gets defined correctly.
#define _RPC_HDR	 // To get values from rpcinc.h
#include "rpcinc.h"  // For RPC_PROGNUM_AWG and RPC_PROGNUM_TESTPOINT
#include "rmorg.h"   // Need for TP_MAX_NODE. */
#include "gdsreadtppar.h"

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: 								*/
/* 	      _MAGICPORT	  well-known port address		*/
/* 	      _TIMEOUT	  	  timeout for request			*/
/* 	      _MAX		  maximum # of answers			*/
/*            _DEFAULT_TIMEOUT	  default timeout (1 sec)		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#define _MAGICPORT		5355
#define _TIMEOUT		(_ONESEC / 4)
#define _MAX			(TP_MAX_NODE * 2) /* TP_MAX_NODE defined in rmorg.h */
#define _DEFAULT_TIMEOUT	1.5

static int my_debug = 0 ;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: confbuf	list of configuration strings          		*/
/*          conftime	time when conf was last read	  		*/
/*          confmux	mutex to protect conf list	   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static char		confbuf[_MAX * 512];
   static tainsec_t	conftime = 0;
   static mutexID_t	confmux;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Forward declarations: 						*/
/*	initConfinfo		init the configuration information API	*/
/*	finiConfinfo		cleanup of the conf. information API	*/
/*      								*/
/* __init__ is defined in util/gdsmain.h				*/ 
/* For GNU C, __init__(name) becomes                                    */
/*     static void name(void) __ attribute__((constructor))             */
/* So, __init__(initConfinfo) should cause initConfinfo() to be called  */
/* as a constructor before main() is executed.                          */
/*----------------------------------------------------------------------*/
   __init__(initConfinfo);
#ifndef __GNUC__
#pragma init(initConfinfo)
#endif
   __fini__(finiConfinfo);
#ifndef __GNUC__
#pragma fini(finiConfinfo)
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: sortConfs					*/
/*                                                         		*/
/* Procedure Description: compares 2 configuration list entries		*/
/*                                                         		*/
/* Procedure Arguments: entry 1, entry 2				*/
/*                                                         		*/
/* Procedure Returns: 0 if equal, <0 if smaller, >0 if larger		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int compareConfs (const char** s1, const char** s2)
   {
      return gds_strcasecmp (*s1, *s2);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: getConfInfo					*/
/*                                                         		*/
/* Procedure Description: get configuration/services information	*/
/*                                                         		*/
/* Procedure Arguments: info id, timeout				*/
/*                                                         		*/
/* Procedure Returns: list of configuration strings			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   const char* const* getConfInfo (int id, double timeout)
   {
      const char* const* p;
   
      /* :IMPORTANT: TAInow may call getConfInfo the first time 
          it is called */
      TAInow();
   
      MUTEX_GET (confmux);
      /* If getConfInfo is called with a timeout of 0, the previous
       * confbuf is returned if this function has been called previously.
       * 
       * This is useful to avoid needless queries to the awgtpman
       * processes to see how many respond.
       */
      if ((timeout < 1E-9) && (conftime != 0)) {
         p = (const char* const*) confbuf;
      }
      else {
         p = getConfInfo_r (id, timeout, confbuf, sizeof (confbuf));
         conftime = 1;
      }
      MUTEX_RELEASE (confmux);
      return p;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: getConfInfo					*/
/*                                                         		*/
/* Procedure Description: get configuration/services information	*/
/*                                                         		*/
/* Procedure Arguments: info id, timeout				*/
/*                                                         		*/
/* Procedure Returns: list of configuration strings			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   const char* const* getConfInfo_r (int id, double timeout, 
   char* buffer, int len)
   {
      char**		p;		/* list index */
      struct sockaddr_in name;		/* internet address */
      socklen_t		size;		/* size of address */
      int 		nset;		/* # of selected descr. */
      fd_set 		readfds;	/* socket descr. list */
      struct timeval 	wait;		/* timeout wait */
      int		nbytes;		/* # of bytes in buffer */
      char		buf[1024];	/* buffer */
      tainsec_t		start;		/* start time */
      tainsec_t		diff;		/* time till timeout */
      int		num = 0;	/* # of answers */
      int		i;		/* answer index */
      int		j;		/* answer index */
      int		left;		/* bytes left in buffer */
      char*		b;		/* buffer pointer */
      char*		q;		/* temp. ptr to handle m.line */
      char*		qq;		/* temp. ptr to handle m.line */
      char		sender[32];	/* sender's address */
      char*		env_val ;	/* Pointer to return from getenv() */

   
      if ((id != 0) || ((_MAX+1) * sizeof (char*) >= len)) {
         return NULL;
      }
   
      /* setup conf info */
      p = (char**) buffer;
      *p = NULL;
      b = buffer + (_MAX+1) * sizeof (char*);
      left = len - (_MAX+1) * sizeof (char*);
   
      {
	 /* Insert a string for nds host and port numbers. */
	 char	ndshost0[256] ;
	 char	ndshost1[256] ;
	 char	host_ip[256] ;
	 int 	port0, port1 ;
	 int	retval ;

	 retval = getNDSHostPort(ndshost0, &port0, ndshost1, &port1) ;
	 if (retval >= 0 || retval < 5)
	 {
	     //now just accept environmental setting for nds address
	     //the older code would try to connect, then pick a different address if connection could not be made
	     //this could lead to incorrect addresses for NDS without restart of software
	     //if nds were temporarily down, for example.

	    /* Attempt to open the first host. */
	    printf("ndshosts: %s and %s\n", ndshost0, ndshost1);
	    if (getHostAddress(ndshost0,  host_ip, sizeof host_ip) >= 0)
	    {
	       /* While 127.0.0.1 is technically correct, it isn't a very
	        * useful IP address.  Not sure it's used anyway. 
		*/
	       sprintf(b, "nds * * %s %d * 127.0.0.1", host_ip, port0 ) ;
	       //printf("New config line: %s\n", b);
	    }
	    else if ( getHostAddress(ndshost1, host_ip, sizeof host_ip) >= 0)
	    {
	       sprintf(b, "nds * * %s %d * 127.0.0.1", host_ip, port1 ) ;
            //printf("New config line: %s\n", b);
	    }
	    else
	    {
	       sprintf(b, "nds * * 127.0.0.1 8088 * 127.0.0.1") ;
	    }
	    /* Fix up p, b and left. */
	    *p = b ; /* Point to the string just written. */
	    *(++p) = NULL ; /* Increment the string pointer and make the new location null */
	    ++num ; /* Increment the count of responses. */
	    left -= strlen(b) + 1 ; /* There's a '\0' at the end. */
	    b += strlen(b) + 1 ; /* Point beyond the '\0' at the end. */
	 }
      }

      if ((env_val = getenv("LIGO_INJ_MODEL")) != (char *) NULL) 
      {
	 modelInfo node[TP_MAX_NODE] ;
	 int	   nread = 0 ;
	 int 	   i ;
	 char	  *pch ;

	 /* Get the list of awg and tp servers from the LIGO_INJ_MODEL env. variable value. */
	 /* Look in the testpoint.par file, found in $CDSROOT/target/gds/param/testpoint.par */
	 if (readTestpointPar(node, &nread, TP_MAX_NODE) == 0)
	 {
	    if (my_debug)
	    {
	       fprintf(stderr, "Number read is %d\n", nread) ;
	       for (i = 0; i < nread; i++)
	       {
		  fprintf(stderr, "%d: dcuid= %d, system= %s\n", i, node[i].dcuid, node[i].modelname) ;
	       }
	    }

	    /* env_val should contain 1 or more model names separated by commas.  For each name,
	     * find the corresponding dcuid and hostname in the nodes read from the testpoint.par file
	     * and construct a string for the RPC address for the awg and testpoint managers.
	     */
	    pch = strtok(env_val, ",") ;
	    while (pch != (char *) NULL)
	    {
	       if (my_debug) fprintf(stderr, "Locating system for model %s\n", pch) ;

	       /* Find the model name in the nodes found in the testpoint.par file. */
	       for (i = 0; i < nread; i++)
	       {
		  if (strcmp(node[i].modelname, pch) == 0)
		  {
		     /* Found the node. */
		     struct hostent	*host ;
		     char		ipstr[INET_ADDRSTRLEN] ;

		     /* We have a host name, but we want the IP address. */
		     host = gethostbyname(node[i].hostname) ;
		     if (!host)
			break ;
		     (void) inet_ntop(host->h_addrtype, *(host->h_addr_list), ipstr, sizeof(ipstr)) ;

		     /* Construct a string for the awg. */
		     sprintf(b, "awg %d 0 %s %d 1 127.0.0.1", node[i].dcuid, ipstr, RPC_PROGNUM_AWG + node[i].dcuid) ;
		     /* Fix up p, b, and left. */
		     *p = b ; 		/* Point to the string just written. */
		     *(++p) = NULL ; 	/* Increment the string pointer and make the new location null. */
		     ++num ;		/* Increment the count of responses. */
		     left -= strlen(b)+1 ; /* There's a '\0' at the end. */
		     b += strlen(b) + 1 ; /* Point beyond the '\0' and the end. */

		     sprintf(b, "tp %d 0 %s %d 1 127.0.0.1", node[i].dcuid, ipstr, RPC_PROGNUM_TESTPOINT + node[i].dcuid) ;
		     /* Fix up p, b, and left. */
		     *p = b ; 		/* Point to the string just written. */
		     *(++p) = NULL ; 	/* Increment the string pointer and make the new location null. */
		     ++num ;		/* Increment the count of responses. */
		     left -= strlen(b)+1 ; /* There's a '\0' at the end. */
		     b += strlen(b) + 1 ; /* Point beyond the '\0' and the end. */
		     break ;
		  }
	       }
	       /* There may be another... */
	       pch = strtok(NULL, ",") ;
	    }
	 }
	 
      }
      else
      {
	 /* Get the list of awg and tp servers by broadcasting for them. */
	 int sock=-1;

	 /* create a socket */
	 sock = socket (PF_INET, SOCK_DGRAM, 0);
	 if (sock == -1) {
	    return NULL;
	 }
	 /* enable broadcast */
	 nset = 1;
	 if (setsockopt (sock, SOL_SOCKET, SO_BROADCAST, 
	 (char*) &nset, sizeof (nset)) == -1) {
	    close (sock);
	    return NULL;
	 }
      
	 /* bind socket */
	 name.sin_family = AF_INET;
	 name.sin_port = 0;
	 name.sin_addr.s_addr = htonl (INADDR_ANY);
	 if (bind (sock, (struct sockaddr*) &name, sizeof (name))) {
	    close (sock);
	    return NULL;
	 }
	 name.sin_port = (short) htons (_MAGICPORT);
      

	 /* set broadcast address */
	 /* Determine if the broadcast query to the awgtpmans goes to a 
	  * special network.  This is indicated by the environment variable
	  * LIGO_RT_BCAST being set. If it isn't, just blast it out the default.
	  * Allow LIGO_RT_BCAST to be a comma separated list of broadcast addresses.
	  */
	 if ((env_val = getenv("LIGO_RT_BCAST")) != (char *) NULL)
	 {
	    char	*pch = strtok(env_val, ",") ;

	    while (pch != (char *) NULL)
	    {
	      if (inet_pton(AF_INET, pch, &name.sin_addr) != 0)
	       {
		  if (my_debug) fprintf(stderr, "getConfInfo_r() - Setting broadcast address to %s\n", pch) ;
	       }

	       /* send request, which consists of 0000. */
	       *((long*) buf) = htonl (0);
	       nbytes = sendto (sock, buf, 4, 0, (struct sockaddr*) &name, sizeof (struct sockaddr_in));
	       if (nbytes < 0) 
	       {
		  close (sock);
		  return NULL;
	       }
	 
	       /* wait for answers */
	       start = TAInow();
	       if (timeout < 0) {
		  timeout = fabs (timeout);
	       } 
	       if (fabs (timeout) < 1E-9) {
		  timeout = _DEFAULT_TIMEOUT;
	       }
	       while (((diff = start + (tainsec_t)(timeout * 1E9) - TAInow()) > 0) && (num < _MAX)) 
	       {
		  /* poll socket */
		  FD_ZERO (&readfds);
		  FD_SET (sock, &readfds);
		  wait.tv_sec = diff / _ONESEC;
		  wait.tv_usec = (diff % _ONESEC) / 1000;
		  nset = select (FD_SETSIZE, &readfds, 0, 0, &wait);
		  if (nset < 0) {
		     close (sock);
		     return NULL;
		  }
		  else if (nset == 0) {
		     break;
		  }
		  /* get answer */
		  size = sizeof (struct sockaddr_in);
		  nbytes = recvfrom (sock, buf, sizeof (buf) - 1, 0,
		     (struct sockaddr*) &name, &size);
		  inet_ntop(AF_INET, &(name.sin_addr), sender, sizeof(sender));
		  buf[nbytes] = 0;
		  /*printf("Received `%s' from %s\n", buf, sender);*/
		  if ((nbytes >= 4) && (ntohl(*((long*) buf)) == 0)) 
		  {
		     if (left < strlen (buf + 4) + 1) {
			return NULL;
		     }
		     q = buf + 4;
		     do 
		     {
			qq = strchr (q, '\n');
			if (qq != NULL) *qq = 0;
			if (left < strlen (q) + strlen (sender) + 2) {
			   return NULL;
			}
			sprintf (b, "%s %s", q, sender);
			if (num < _MAX) {
			   *p = b;
			   *(++p) = NULL;
			   ++num;
			}
			left -= strlen (b) + 1;
			b += strlen (b) + 1;
			q = (qq == NULL) ? q + strlen (q) : qq + 1;
		     } while (*q);
		  }
	       }
	       pch = strtok(NULL, ",") ;
	    }
	 }
	 else
	 {
	    /* No LIGO_RT_BCAST variable found, send to local broadcast address. */
	    if (my_debug) fprintf(stderr, "getConfInfo_r() - Setting broadcast address to local broadcast\n") ;
	    name.sin_addr.s_addr = htonl(INADDR_BROADCAST) ;

	    /* send request, which consists of 0000. */
	    *((long*) buf) = htonl (0);
	    nbytes = sendto (sock, buf, 4, 0, (struct sockaddr*) &name, sizeof (struct sockaddr_in));
	    if (nbytes < 0) 
	    {
	       close (sock);
	       return NULL;
	    }
	 
	    /* wait for answers */
	    start = TAInow();
	    if (timeout < 0) {
	       timeout = fabs (timeout);
	    } 
	    if (fabs (timeout) < 1E-9) {
	       timeout = _DEFAULT_TIMEOUT;
	    }
	    while (((diff = start + (tainsec_t)(timeout * 1E9) - TAInow()) > 0) && (num < _MAX)) 
	    {
	       /* poll socket */
	       FD_ZERO (&readfds);
	       FD_SET (sock, &readfds);
	       wait.tv_sec = diff / _ONESEC;
	       wait.tv_usec = (diff % _ONESEC) / 1000;
	       nset = select (FD_SETSIZE, &readfds, 0, 0, &wait);
	       if (nset < 0) {
		  close (sock);
		  return NULL;
	       }
	       else if (nset == 0) {
		  break;
	       }
	       /* get answer */
	       size = sizeof (struct sockaddr_in);
	       nbytes = recvfrom (sock, buf, sizeof (buf) - 1, 0,
		  (struct sockaddr*) &name, &size);
	       inet_ntop(AF_INET, &name.sin_addr, sender, sizeof(sender));
	       buf[nbytes] = 0;
	       /*printf("Received `%s' from %s\n", buf, sender);*/
	       if ((nbytes >= 4) && (ntohl(*((long*) buf)) == 0)) 
	       {
		  if (left < strlen (buf + 4) + 1) {
		     return NULL;
		  }
		  q = buf + 4;
		  do 
		  {
		     qq = strchr (q, '\n');
		     if (qq != NULL) *qq = 0;
		     if (left < strlen (q) + strlen (sender) + 2) {
			return NULL;
		     }
		     sprintf (b, "%s %s", q, sender);
		     if (num < _MAX) {
			*p = b;
			*(++p) = NULL;
			++num;
		     }
		     left -= strlen (b) + 1;
		     b += strlen (b) + 1;
		     q = (qq == NULL) ? q + strlen (q) : qq + 1;
		  } while (*q);
	       }
	    }
	 }
	 close (sock);
      }
   
      /* sort answeres */
      qsort (buffer, num, sizeof (char*), 
         (int (*) (const void*, const void*)) compareConfs);
   
      /* remove duplicates */
      i = 0;
      p = (char**) buffer;
      while (i + 1 < num) 
      {
         if (gds_strcasecmp (p[i], p[i+1]) == 0) {
            for (j = i + 1; j < num; j++) {
               p[j] = p[j + 1];
            }
            num--;
         }
         else {
            i++;
         }
      }
   
      return (const char* const*) buffer;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: parseConfInfo				*/
/*                                                         		*/
/* Procedure Description: parses a conf. info string			*/
/*                                                         		*/
/* Procedure Arguments: info string, info record (return)		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int parseConfInfo (const char* info, confinfo_t* rec)
   {
      char*		p;		/* token */
      char		buf[1024];	/* buffer */
      char*		wrk;		/* work space */
   
      strncpy (buf, info, sizeof (buf));
      buf[sizeof (buf)-1] = 0;
   
      /* get interface */
      p = strtok_r (buf, " \t\n", &wrk);
      if (p == NULL) {
         return -1;
      }
      strncpy (rec->interface, p, sizeof (rec->interface));
      rec->interface[sizeof(rec->interface)-1] = 0;
   
      /* get interferometer number */
      p = strtok_r (NULL, " \t\n", &wrk);
      if (p == NULL) {
         return -2;
      }
      if ((strlen (p) > 0) && (p[0] == '*')) {
         rec->ifo = -1;
      }
      else {
         rec->ifo = atoi (p);
      }
   
      /* get id number */
      p = strtok_r (NULL, " \t\n", &wrk);
      if (p == NULL) {
         return -3;
      }
      if ((strlen (p) > 0) && (p[0] == '*')) {
         rec->num = -1;
      }
      else {
         rec->num = atoi (p);
      }
   
      /* get host name */
      p = strtok_r (NULL, " \t\n", &wrk);
      if (p == NULL) {
         return -4;
      }
      strncpy (rec->host, p, sizeof (rec->host));
      rec->host[sizeof(rec->host)-1] = 0;
   
      /* get port/prognum */
      p = strtok_r (NULL, " \t\n", &wrk);
      if (p == NULL) {
         return -5;
      }
      if ((strlen (p) > 0) && (p[0] == '*')) {
         rec->port_prognum = -1;
      }
      else {
         rec->port_prognum = atoi (p);
      }
   
      /* get program version */
      p = strtok_r (NULL, " \t\n", &wrk);
      if (p == NULL) {
         return -6;
      }
      if ((strlen (p) > 0) && (p[0] == '*')) {
         rec->progver = -1;
      }
      else {
         rec->progver = atoi (p);
      }
   
      /* get sender's address */
      p = strtok_r (NULL, " \t\n", &wrk);
      if (p == NULL) {
         return -7;
      }
      strncpy (rec->sender, p, sizeof (rec->sender));
      rec->sender[sizeof(rec->sender)-1] = 0;
//      printf("crec interface=%s ifo=%d num=%d host=%s port_prognum=%d progver=%d sender=%s\n",
//             rec->interface, rec->ifo, rec->num, rec->host, rec->port_prognum,
//             rec->progver, rec->sender);
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: initConfinfo				*/
/*                                                         		*/
/* Procedure Description: init API					*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void initConfinfo (void) 
   {
      if (MUTEX_CREATE (confmux) != 0) {
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: finiConfinfo				*/
/*                                                         		*/
/* Procedure Description: cleanup API					*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void finiConfinfo (void) 
   {
      MUTEX_DESTROY (confmux);
   }

