/* -*- mode: c++; c-basic-offset: 4; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: nds2input.h						*/
/*                                                         		*/
/* Module Description: gets data through the nds2 and stores it		*/
/*		       a storage object					*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 1Aug09   J.Zweizig    	Original version based on rtddinput  	*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: gdsdatum.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* John zweizig  (626)395-2486  (509) 372-2178   zweizig@ligocaltech	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6-10     		*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_NDS2INPUT_H
#define _GDS_NDS2INPUT_H

/* Header File List: */
#include "gmutex.hh"
#include "databroker.hh"
#include "nds.hh"
#include "threadsafequeue.h"
#include <thread>

namespace diag {


    /** @name Real-time data distribution input API
	Data is read through the real-time data distribution system,
	down-converted and decimated if necessary, partitioned and
	stored in the diagnostics storage object.

   
	@memo Reads data through the real-time data distribution.
	@author Written November 1998 by Daniel Sigg
	@version 0.1
    ************************************************************************/

    //@{

    /** @name Data types and constants
	Data types of the real-time data distribution input API

	@memo Data types of the real-time data distribution input API
    ************************************************************************/

    //@{
#if 0
    /** Compiler flag for a enabling dynamic configuration. When enabled
	the host address and interface information of network data server 
	are queried from the network rather than read in through a file.

	@author DS, June 98
	@see Test point API
    ************************************************************************/
#define _CONFIG_DYNAMIC
#endif

    //@}

    // Info needed to call a channel callback with a data buffer.
    struct InputCallback
    {
      taisec_t time;
      int epoch;
      unsigned long count;
      int err;
      std::shared_ptr<std::vector<float>> data;
      dataChannel *channel;
    };

    /***
     * An instance of an nds2 connection, used by nds2manager
     */
    class NDS2Connection
    {
    private:

      //takes data iterators and produces buffers for the buffers queue
      std::unique_ptr<std::thread> read_data_thread;

      //takes InputCallback structs from buffers_thread and calls the callbacks
      std::unique_ptr<std::thread> callback_thread;

      std::unique_ptr<std::thread> buffers_thread;

      // when true, object is being deleted and threads must close
      bool closing;

      std::map<std::string, dataChannel *> channel_map;

    public:


      NDS2Connection( class nds2Manager *manager, const NDS::connection::host_type& host,
                          NDS::connection::port_type port = NDS::connection::DEFAULT_PORT,
                          NDS::connection::protocol_type    protocol = NDS::connection::PROTOCOL_TRY );


      // the parent manager
      class nds2Manager *manager;

      virtual ~NDS2Connection();

      std::unique_ptr<NDS::connection> nds;

      // queue passes data from nds connection to a thread that transforms the
      // data buffers to callbacks
      ThreadSafeQueue<NDS::data_iterable> data_queue;

      ThreadSafeQueue<NDS::buffers_type> buffers_queue;

      ThreadSafeQueue<InputCallback> callback_queue;

      bool is_closing() { return closing; }

      void close() ;

      dataChannel *find_channel(std::string name);
    };


    /** Class for reading a set of channels from the real-time data 
	distribution system. This object manages a list of nds2Channel
	objects.
	Usage: In general, a diagnostics test should only use add and del
	methods at the beginning and end of the test, respectively. On the 
	other hand a diagnostics supervisory task should use set and clear
	to start and stop the data flow.

	@memo Class for channel input from the nds2.
	@author DS, November 98
	@see Real-time data distribution input API
    ************************************************************************/
    class nds2Manager : public dataBroker {
    public:
	/** Constructs a real-time data distribution management object.
	    @memo Default constructor
	    @param dat storage object
	    @param TPmgr test point manager
	    @param Lazytime Time to wait for cleanup after a lazy clear
	    @param usernds True if a user specified NDS
	******************************************************************/
	explicit nds2Manager (gdsStorage* dat = 0, 
			      testpointMgr* TPMgr = 0, 
			      double Lazytime = 0);

	/** Delete the nds2Manager. Close and delete the nds.
	    @memo Destructor
	******************************************************************/
	~nds2Manager(void);

	/** Establishes connection to the network data server. If the
	    server is a 0 pointer (default), the function returns with a
	    false.  The epoch time values are used in a set-epoch command if
	    non-zero, and the epoch_start is used as the gps time in the
	    get-channels command.
	    @memo Connect method.
	    @param server name of NDS
	    @param port port number of NDS
	    @return true if successful
	******************************************************************/
	virtual bool connect (const char* server, int port = 0, 
			      bool usernds = false, unsigned long epoch_start = 0,
			      unsigned long epoch_stop = 0);

	virtual bool connect() { 
	    return connect (0, 0); }

        /**
         * Re connect to a previously established connection.
         *
         * For NDS2, we reconnect to the server before every test.
         * @return true if successful
         */
        virtual bool reconnect();
   
	/** Requests channel data by sending a request to the NDS2 API.
	    (This will not set test points!) This method will request
	    on-line data from the NDS.
	    @memo Set method.
	    @param start time when channels are needed
	    @param active time when channels become available
	    @return true if successful
	******************************************************************/
	virtual bool set (tainsec_t start = 0, tainsec_t* active = 0);
   
	/** Requests channel data by sending a request to the NDS2 API.
	    (This will not set test points!) This method works with 
	    a start time and duration and therefore will lookup data
	    from the NDS archive. Start time and duration are given
	    in multiples of GPS seconds.
	    @memo Set method.
	    @param start start time of request
	    @param duration requested time interval
	    @return true if successful
	******************************************************************/
	virtual bool set (taisec_t start, taisec_t duration);
   
	/** Obtains channel information. Takes user nds into account. 
	    makes sure channel names are expanded correctly.
	    Given a name and optionally a data rate, find the gdsChnInfo_t
	    structure for the channel.
	    @memo info method.
	    @param name channel name
	    @param info channel info (return)
	    @param rate channel data rate
	    @return true if successful
	******************************************************************/
	virtual bool channelInfo (const std::string& name, 
				  gdsChnInfo_t& info, int rate = -1) const;
   
	/** Requests times of data availability from NDS.
	    @memo Get times method.
	    @param start start time of data (return)
	    @param duration time interval (return)
	    @return true if successful
	******************************************************************/
	virtual bool getTimes (taisec_t& start, taisec_t& duration);
   
	/** Returns the maximum time client should wait for data to
	    return; <=0 means wait forever. (offline access only)
	    @memo Timeout value.
	    @return maximum time for data to become available
	******************************************************************/
	virtual tainsec_t timeoutValue (bool online = false) const;

	/** Shut down the NDS connection.
	    @memo Shut down.
	******************************************************************/
	virtual void shut(void);

        void pub_channel_skip() { dataBroker::channel_skip(); }

        // process a single data block from an NDS::data_iterator
        bool ndsdata (NDS2Connection *nds2con,
                      std::shared_ptr<NDS::buffers_type> next_buffers, int err);

        //
        dataChannel *find_channel(std::string name);

    protected:
	/// User NDS?
	bool		userNDS;
	/// User NDS channel list
	NDS::channels_type userChnList;
        // epoch
        //NDS::epoch epoch;
	/// Real-time mode?
	bool		RTmode;
	/// fast/slow NDS writer?
	bool	        online_req;
	/// abort
	bool		abort;
	/// NDS interface object
	std::unique_ptr<NDS2Connection> nds;


   
    private:
	/// prevent copy
	nds2Manager (const nds2Manager&);
	nds2Manager& operator= (const nds2Manager&);
   
	/// mutex to protect task from being canceled
	thread::mutex		ndsmux;
	/// nds server name
	std::string     daqServer;
	/// nds server port
	int		daqPort;
        /// Convert DTT channel list to NDS2 query channel list
        NDS::channel::channel_names_type get_channel_names();
	/// nds start
	virtual bool ndsStart ();
	/// nds start with old data
	virtual bool ndsStart (taisec_t start, taisec_t duration);
	/// nds stop
	virtual bool dataStop ();
        // convert nds2 client dataType constants to DAQ dataType constants
        static int to_daq_dataType(NDS::channel::data_type t);



        friend class NDS2Connection;
    };

    //@}
}

#endif /* NDS2INPUT */
