/* -*- mode: c++; c-basic-offset: 4; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: nds2input						*/
/*                                                         		*/
/* Module Description: reads in channel data through the NDS2 interface	*/
/* implements decimation and zoom functions, partitions the data and	*/
/* stores it in a storage object					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

//#define DEBUG

// Header File List:
#include "nds2input.hh"
#include <daqc.h>
#include <strings.h>
#include <signal.h>
#include <pthread.h>
#include <algorithm>
#include <iostream>
#include "tconv.h"
#include "gdstask.h"
#include "map.h"

#if defined (_CONFIG_DYNAMIC)
#include "confinfo.h" 
#endif

using namespace std;
using namespace thread;

namespace diag {

    /*----------------------------------------------------------------------*/
    /*                                                         		    */
    /* Constants: PRM_FILE		  parameter file name		    */
    /*            PRM_SECTION	  section heading is channel name!	    */
    /*            PRM_SERVERNAME	  entry for server name		    */
    /*            PRM_SERVERPORT	  entry for server port		    */
    /*            DAQD_SERVER	  default server name for channel info	    */
    /*            DAQD_PORT		  default port for channel info	    */
    /*            __ONESEC		  one second (in nsec)		    */
    /*            _MIN_NDS_DELAY	  minimum delay allowed for NDS (s) */
    /*            _MAX_NDS_DELAY	  maximum delay allowed for NDS (s) */
    /*            _NDS_DELAY	  NDS delay for slow data (sec)		    */
    /*            taskNds2Name	  nds task priority			    */
    /*            taskNds2Priority	  nds task name			    */
    /*            taskCleanupName	  nds clenaup task priority	    */
    /*            taskCleanupPriority nds cleanup task name		    */
    /*            daqBufLen		  length of receiving socket buffer */
    /*            							    */
    /*----------------------------------------------------------------------*/
#define _CHNLIST_SIZE		200
#if !defined (_CONFIG_DYNAMIC)
#define PRM_FILE		gdsPathFile ("/param", "nds2.par")
#define PRM_SECTION		gdsSectionSite ("nds2")
#define PRM_SERVERNAME		"hostname"
#define PRM_SERVERPORT		"port"
#define DAQD_SERVER		"ldas-pcdev1.ligo.caltech.edu"
    //#define DAQD_PORT		31200
#endif
#define _MIN_NDS_DELAY		0.0
#define _MAX_NDS_DELAY		64.0 // 64 seconds
#define _NDS_DELAY		1

    static const int my_debug = 0 ;

    static const char	taskNds2Name[] = "tNDS2";
    static const int	taskNds2Priority = 0;
    static const char	taskCleanupName[] = "tNDS2cleanup";
    static const int	taskCleanupPriority = 20;
    static const int 	daqBufLen = 1024*1024;
    static const long	taskNds2OnlineTimeout = 64;
    static const long	taskNds2OfflineTimeout = 24 * 3600;  // 1 day!
    static const bool 	kNds2Debug = false;

    static const double __ONESEC = double(_ONESEC);



    static void channel_callback(NDS2Connection *nds2con)
    {
      while(!nds2con->is_closing())
      {
        try {
          auto callback = nds2con->callback_queue.pop(std::chrono::milliseconds (1000));

          callback->channel->callback(callback->time, callback->epoch,
            (float *)&((*callback->data)[0]),
                                      callback->count, callback->err);
        }
        catch (QueueTimeoutException) {
          // timeout, do nothing
        }
      }
    }

    static void ndstask (NDS2Connection *nds2con)
    {
	int		len;	// length of read buffer
	int		seqNum = -1;
   
	// wait for data
	while (! nds2con->is_closing()) {
	    // get the mutex
            try {
              auto data_iterator = nds2con->data_queue.pop(std::chrono::milliseconds (1000));
              //loop over the new iterator
              for(auto block: *data_iterator) {
                if(nds2con->is_closing()) {
                  break;
                }
                nds2con->buffers_queue.push(block);
              }
            }
            catch(QueueTimeoutException)
            {
              //timed out from Pop().  Do nothing.
            }

	}
        len = 1;
    }

    static void buffer_handler(NDS2Connection *nds2con)
    {
      bool dump_remaining = false;
      while(! nds2con->is_closing())
      {
        try {
          auto buffers = nds2con->buffers_queue.pop(std::chrono::milliseconds (1000));
          if(dump_remaining)
          {
            continue;
          }
          // do nothing on a timeout
          try {
            // process one slice of data
            nds2con->manager->ndsdata(nds2con, buffers, 0);

          } catch (std::runtime_error exc) {
            cerr << "Error reading NDS2 data: " << exc.what() << endl;
            nds2con->manager->pub_channel_skip();
            nds2con->manager->shut();

            // dump remaining items in the queue.  Don't process.
            dump_remaining = true;
          }
        }
        catch (QueueTimeoutException)
        {

        }
      }
    }

    NDS2Connection::NDS2Connection( class nds2Manager *manager_, const NDS::connection::host_type& host,
                                   NDS::connection::port_type port,
                                   NDS::connection::protocol_type    protocol)
    {
      nds.reset(new NDS::connection(host, port, protocol));

      closing = false;
      this->manager = manager_;

      read_data_thread.reset( new std::thread(ndstask, this));
      callback_thread.reset( new std::thread(channel_callback, this));
      buffers_thread.reset( new std::thread(buffer_handler, this));
    }

    NDS2Connection::~NDS2Connection() {
      close();
    }

    void NDS2Connection::close() {
      if(! closing) {
        closing = true;

        cerr << "NDS2Connection: shutting down threads" << endl;
        read_data_thread->join();
        cerr << "NDS2Connection: data thread shut down" << endl;
        callback_thread->join();
        cerr << "NDS2Connection: callback thread shut down" << endl;
        buffers_thread->join();
        cerr << "NDS2Connection: buffers thread shut down" << endl;
        nds->close();
      }
    }

    dataChannel * NDS2Connection::find_channel(std::string name) {
      auto item = channel_map.find(name);
      if(item != channel_map.end())
      {
        return (*item).second;
      }
      else
      {
        auto chn = manager->find_channel(name);
        channel_map[name] = chn;
        return chn;
      }
    }

    /*----------------------------------------------------------------------*/
    /*                                                         		*/
    /* Class Name: nds2Manager						*/
    /*                                                         		*/
    /*----------------------------------------------------------------------*/

    //==================================  Constructor
    nds2Manager::nds2Manager (gdsStorage* dat, testpointMgr* TPMgr, 
			      double Lazytime) 
	: dataBroker (dat, TPMgr, Lazytime), userNDS (false), 
	  RTmode (false), online_req(false), abort(false)
    {
	daqPort = 0;
    }

    //==================================  Destructor
    nds2Manager::~nds2Manager (void) {
    }

    class chnorder2 {
    public:
	chnorder2() {}

        /// Return true if c1 < c2, first by name, then by rate
        /// \param c1 A channel
        /// \param c2 A channel
        /// \return true if c1 < c2
	bool operator() (const NDS::channel & c1,
			 const NDS::channel & c2) const {
	    int compare = strcasecmp (c1.Name().c_str(), c2.Name().c_str());

            //c1 name strictly greater
            if (compare > 0) {
              return false;
            }
            //c1 name strictly less
            if (compare < 0) {
              return true;
            }
            // if the names are the same, compare the rates
            return c1.SampleRate() < c2.SampleRate();
	}
    };

    bool nds2Manager::reconnect() {
      try {
        nds.reset(new NDS2Connection(this, NDS::connection::host_type(daqServer),
                                      NDS::connection::port_type(daqPort)));
        if(! nds)
        {
          cerr << "Could not allocate NDS2 connection object" << std::endl;
          return false;
        }
      }
      catch(std::runtime_error ex)
      {
        cerr << ex.what() << endl;
        return false;
      }
      return true;
    }

    bool nds2Manager::connect (const char* server, int port, bool usernds,
			       unsigned long epoch_start, unsigned long epoch_stop)
    {
	int		status;
   
	// get NDS2 parameters
	if (server == 0) {
	    return false ;
	}

        daqServer = server;
        daqPort = (port <= 0) ? DAQD_PORT : port;

	// connect to NDS
        if(!reconnect())
        {
          return false;
        }

	// get channel list if user NDS
	userNDS = usernds;
	if (usernds) {
	    // Set the epoch if the start != stop.
            NDS::epoch epoch;
            NDS::channel_predicate_object pred;

	    if (epoch_start != epoch_stop) {
              epoch = NDS::epoch(NDS::buffer::gps_second_type(epoch_start),
                                       NDS::buffer::gps_second_type(epoch_stop));
	    }

            pred.set(epoch);

            //get only raw and reduced data set channels
            pred.set(NDS::channel::CHANNEL_TYPE_RAW);
            pred.set(NDS::channel::CHANNEL_TYPE_RDS);

	    if (my_debug) cerr << "nds2Manager::connect() - get channel list" << endl ;
            userChnList = nds->nds->find_channels(pred);

	    if (my_debug) cerr << "nds2Manager::connect() - sort channel list" << endl ;
	    sort (userChnList.begin(), userChnList.end(), chnorder2());
	    
	    if (my_debug) cerr << "nds2Manager::connect() - done" << endl ;
	}
   
	return true;
    }


    bool nds2Manager::set (tainsec_t start, tainsec_t* active)
    {
	tainsec_t		chnactive;	// time when channel active
   

	// set minimum active time
	if (active != 0) {
	    *active = start;
	}
	cleartime = 0;
	// check if already set
   
	if (!areSet()) {
	    // setup channels
	    for (channellist::iterator iter = channels.begin();
		 iter != channels.end(); iter++) {
		// add channel to list
		if (iter->isSet()) {
		    continue;
		}
		// activate channel
		if (!iter->subscribe (start, &chnactive)) {
		    // error
		    for (channellist::reverse_iterator iter2 (iter);
			 iter2 != channels.rend(); iter2++) {
			iter2->unsubscribe();
		    }
		    return false;
		}
		if (active != 0) {
		    *active = max (chnactive, *active);
		}
	    }
	}
   
	// all set: start nds2
	if (!ndsStart ()) {
	    for (channellist::iterator iter = channels.begin();
		 iter != channels.end(); iter++) {
		iter->unsubscribe();
	    }     
	    return false;
	}
	// round active time to next second after adding max. filter delays
	if (active != 0) {
	    tainsec_t 	now = TAInow();
	    now = _ONESEC * ((now + _ONESEC - 1) / _ONESEC);
	    *active = max (now, *active);
	}
   
	return true;
    }


    bool nds2Manager::set (taisec_t start, taisec_t duration)
    {
	cerr << "TIME STAMP BEFORE START = " << timeStamp() << endl;

	cleartime = 0;
   
	// setup channels
	if (my_debug) cerr << "nds2Manager::set() - setup channels for NDS2" << endl;
	for (channellist::iterator iter = channels.begin();
	     iter != channels.end(); iter++) {
	    // add channel to list
	    //cerr << "Add channel: " << iter->getChnName() << " rate: " << iter->getDatarate() << endl;
	    if (iter->isSet()) {
		continue;
	    }
	    // activate channel
	    if (!iter->subscribe (start, 0)) {
		// error
		for (channellist::reverse_iterator iter2 (iter);
		     iter2 != channels.rend(); iter2++) {
		    iter2->unsubscribe();
		}
		return false;
	    }
	}
   
	// all set: start nds2
	cerr << "start NDS2 @ " << start << ":" << duration << endl;
	if (!ndsStart (start, duration)) {
	    for (channellist::iterator iter = channels.begin();
		 iter != channels.end(); iter++) {
		iter->unsubscribe();
	    }     
	    return false;
	}
   
	cerr << "start NDS2 @ " << start << ":" << duration << " done" << endl;
	return true;
    }

    /// convert nds2 client dataType constants to DAQ dataType constants
    /// \param t An NDS2Client dataType constant
    /// \return A DAQ data type constant.0
    int nds2Manager::to_daq_dataType(NDS::channel::data_type t)
    {
      switch(t)
      {
      case NDS::channel::DATA_TYPE_INT16:
        return DAQ_DATATYPE_16BIT_INT;
      case NDS::channel::DATA_TYPE_INT32:
        return DAQ_DATATYPE_32BIT_INT;
      case NDS::channel::DATA_TYPE_INT64:
        return DAQ_DATATYPE_64BIT_INT;
      case NDS::channel::DATA_TYPE_FLOAT32:
        return DAQ_DATATYPE_FLOAT;
      case NDS::channel::DATA_TYPE_FLOAT64:
        return DAQ_DATATYPE_DOUBLE;
      case NDS::channel::DATA_TYPE_COMPLEX32:
        return DAQ_DATATYPE_COMPLEX;
      case NDS::channel::DATA_TYPE_UINT32:
        return DAQ_DATATYPE_32BIT_UINT;
      default:
        return 0;
      }
    }

    bool nds2Manager::channelInfo(const string& name, gdsChnInfo_t& info, int rate) const
    {
	if (my_debug) cerr << "nds2Manager::channelInfo(" << name << ", ..., " << rate << ")" << endl;
	if (!userNDS) {
	    cout << "Get channel info from channelHandler." << endl;
	    return channelHandler::channelInfo (name, info, rate);
	}
	else {
	    cout << "Get nds2 channel info for " << name << ", rate = " << rate << endl;

            // find channel

            // 'item' is a dummy channel used for comparison purposes
            // in the call to lower_bound()
            // only rate and name are used.
	    NDS::channel item(name, NDS::channel::CHANNEL_TYPE_STATIC,
                              NDS::channel::DATA_TYPE_FLOAT32,
                              rate, 1.0, 1.0, 0.0, "");

	    // The lower_bound function uses chnorder2() to find the
	    // first channel that matches the criteria that makes 
	    // chnorder2() happy. 
	    NDS::channels_type::const_iterator chn =
		lower_bound (userChnList.begin(), userChnList.end(),
			     item, chnorder2());

	    memset (&info, 0, sizeof (gdsChnInfo_t));
	    if ((chn == userChnList.end()) ||
		(strcasecmp (item.Name().c_str(), chn->Name().c_str()) != 0)) {
		cout << "NDS2 has no channel info for " << name << endl;
		return false;
	    }
	    else {
		strncpy (info.chName, chn->Name().c_str(),
			 sizeof (info.chName)-1);
		info.chName[sizeof(info.chName)-1] = 0;
		info.chGroup = int(chn->Type());
		info.dataRate = int(chn->SampleRate());
		info.bps = chn->DataTypeSize();
		info.dataType = to_daq_dataType(chn->DataType());
		info.gain = chn->Gain();
		info.slope = chn->Slope();
		info.offset = chn->Offset();
		strncpy (info.unit, chn->Units().c_str(), sizeof (info.unit)-1);
		info.unit[sizeof(info.unit)-1] = 0;
		if (my_debug) cerr << "channelInfo() - found info, rate = " << info.dataRate << endl ;
		return true;
	    }
	}
    }

    void nds2Manager::shut(void) {
	TID = 0;
        nds->close();

    }

    bool nds2Manager::getTimes (taisec_t& start, taisec_t& duration)
    {
	start = 0;
	duration = 0;
	// get time segments
	return true; //(nds.Times (start, duration) == 0);
    }


    tainsec_t nds2Manager::timeoutValue (bool online) const
    {
	if (online) return tainsec_t(taskNds2OnlineTimeout)  * _ONESEC;
	else        return tainsec_t(taskNds2OfflineTimeout) * _ONESEC;
    }

    /// copy a NDS::buffer of type T, returning a vector of floats
    /// of the same size
    /// with values from buffer converted to floats
    template <class T>
    static vector<float> copy_buffer_to_float(NDS::buffer &buffer)
    {

      // For int32, this gives different value by about 0.2% than a normal cast
      //vector<float> float_buff(buffer.cbegin<T>(), buffer.cend<T>());

      //use normal cast instead to match old results
      vector<float> float_buff(buffer.Samples());
      for(int i=0; i < buffer.Samples(); ++i)
      {
        float_buff[i] = (float)buffer.at<T>(i);
      }
      return float_buff;
    }


    template <>
    vector<float> copy_buffer_to_float<complex<float>>(NDS::buffer &buffer)
    {
      vector<float> float_buff(buffer.Samples() * 2);
      for(int i = 0; i < buffer.Samples(); ++i)
      {
        auto value = buffer.at<complex<float>>(i);
        float_buff[2*i] = value.real();
        float_buff[2*i+1] = value.imag();
      }
      return float_buff;
    }

    /// Read in data from a connection.  Copy the
    /// \param err
    /// \return
    bool nds2Manager::ndsdata (NDS2Connection *nds2con, shared_ptr<NDS::buffers_type> buffers, int err)
    {


      // get time from first buffer
      auto first_buffer = &buffers->at(0);

      // we assume everything false on a second boundary, as per previous code.
      int epoch = 0;

      taisec_t time = first_buffer->Start();
      tainsec_t end_ns =  first_buffer->Stop() * _ONESEC;
      tainsec_t timestamp = first_buffer->Start() * _ONESEC + epoch * _EPOCH;

      tainsec_t duration = end_ns - timestamp;

#ifdef DEBUG
      cerr << "time GPS = " << 	time << " slice = " << epoch
           << " duration sec = " << double(duration) / __ONESEC << endl;
#endif

      // check if we lost data
      if ((nexttimestamp != 0) &&
          (timestamp > nexttimestamp + 1000)) {
        cerr << "NDS2 RECEIVING ERROR: # of epochs lost = " <<
            (timestamp - (nexttimestamp - 1000)) / _EPOCH << endl;
      }

      // check NDS2 time
#ifdef GDS_ONLINE
      if (RTmode) {
        double delay = (double) (TAInow() - timestamp) / __ONESEC;
        double maxdelay = _MAX_NDS_DELAY + duration / __ONESEC;
        if ((delay < _MIN_NDS_DELAY) || (delay > maxdelay)) {
          cerr << "TIMEOUT ERROR: NDS2 delay = " << delay << endl;
          //return false;
        }
      }
#endif


      //------------------------------  loop over channels
      for (auto buffer = buffers->begin(); buffer != buffers->end(); ++buffer) {
        // find daq channel and invoke callback
        auto then = std::chrono::system_clock::now();
        auto chn = nds2con->find_channel( buffer->Name());

        auto now = std::chrono::system_clock::now();
        auto delta = std::chrono::duration_cast<std::chrono::microseconds>(now - then);

        cout << "fetch channel: " << buffer->Name() << " ndata: " << buffer->Samples() << endl;

        //--------------------------  invoke callback
        shared_ptr<std::vector<float>> float_buff = make_shared<std::vector<float>>();
        auto count = buffer->Samples();
        switch(buffer->DataType())
        {
        case NDS::channel::DATA_TYPE_INT16:
          *float_buff = copy_buffer_to_float<int16_t>(*buffer);
          break;
        case NDS::channel::DATA_TYPE_INT32:
          *float_buff = copy_buffer_to_float<int32_t>(*buffer);
          break;
        case NDS::channel::DATA_TYPE_INT64:
          *float_buff = copy_buffer_to_float<int64_t>(*buffer);
          break;
        case NDS::channel::DATA_TYPE_FLOAT32:
          *float_buff = copy_buffer_to_float<float>(*buffer);
          break;
        case NDS::channel::DATA_TYPE_FLOAT64:
          *float_buff = copy_buffer_to_float<double>(*buffer);
          break;
        case NDS::channel::DATA_TYPE_COMPLEX32:
          *float_buff = copy_buffer_to_float<complex<float>>(*buffer);
          break;
        case NDS::channel::DATA_TYPE_UINT32:
          *float_buff = copy_buffer_to_float<uint32_t>(*buffer);
          break;
        default:
          return false;
        }

        // fill in callback
        shared_ptr<InputCallback> callback = make_shared<InputCallback>();
        callback->time = time;
        callback->epoch = epoch;
        callback->channel = chn;
        callback->count = count;
        callback->data = float_buff;

        nds2con->callback_queue.push( callback );
      }

#ifdef DEBUG
      cerr << "nds2 callback done "
           << double((time*_ONESEC+epoch*_EPOCH) % (1000 * _ONESEC)) / 1E9
           << " at " << double(TAInow() % (1000 * _ONESEC)) / 1E9 << endl;
      cerr << "time stamp = " << timeStamp() << endl;
#endif

      // set time of last successful NDS2 data transfer
      nexttimestamp = timestamp + duration;
      lasttime = TAInow();
      return true;
    }

    /// Convert DTT channel list to NDS2 query channel list
    /// \return
    NDS::channel::channel_names_type nds2Manager::get_channel_names() {
      NDS::channel::channel_names_type chan_names;
      for(auto chn:channels) {
        chan_names.emplace_back(chn.getChnName());
      }
      return chan_names;
    }

    /// Start nds read for online data
    /// \return true if successful
    bool nds2Manager::ndsStart ()
    {
	// check if already running 
	if (TID != 0) {
	    return true;
	}

	// set last time
	nexttimestamp = 0;
	starttime = 0;
	stoptime = 0; // no end
	lasttime = TAInow();
	double stride = 1.0;

	// establish connection

	online_req = true;

        auto chan_names = get_channel_names();

        try {
          auto data_iterator = make_shared<NDS::data_iterable>(nds->nds->iterate(
              NDS::request_period(), chan_names));
          nds->data_queue.push(data_iterator);
        }
        catch (std::runtime_error ex) {
          cerr << "Error starting NDS stream: " << ex.what() << endl;
          return false;
        }
	
	// create nds2 task
	int		attr;	// task create attribute
	attr = PTHREAD_CREATE_DETACHED;
	if (taskCreate (attr, taskNds2Priority, &TID, 
			taskNds2Name, (taskfunc_t) ndstask, 
			(taskarg_t) this) != 0) {
	    return false;
	}
	cerr << "nds2 started" << endl;
   
	return true;
    }

    /// start nds for fixed historical data
    /// \return true if successful
    bool nds2Manager::ndsStart (taisec_t start, taisec_t duration)
    {
	// check if already running 
	if (TID != 0) {
	    return true;
	}

	// wait for data to become available
	tainsec_t tEnd = (start + duration + _NDS_DELAY) * _ONESEC;
	while (TAInow() < tEnd) {
	    timespec wait = {0, 250000000};
	    nanosleep (&wait, 0);
	}

	// set last time
	nexttimestamp = start * _ONESEC;
	starttime = start * _ONESEC;
	stoptime = (start + duration) * _ONESEC;
	lasttime = TAInow();

	// start net writer
	RTmode = false;
	if (my_debug) {
          cerr << "nds2Manager::ndsStart() - nds2 start old data" << endl;
        }

	online_req = false;
	if (my_debug) cerr << "nds2Manager::ndsStart() - nds.RequestData(" << start << ", " << duration << ", ...)" << endl ;

        auto chan_names = get_channel_names();

        // check if data available
        try {
          if(nds->nds->check(start, start+duration, chan_names))
          {
            cerr << "NDS2 data is available" << endl;
          }
          else
          {
            cerr << "Warning: NDS2 data is either unavailable or will be found on tape" << endl;
          }
        }
        catch (NDS::connection::daq_error ex){
          string msg = ex.what();

          cerr << "DAQ Error checking for data availability: " << ex.what() << endl;

          if(msg.find("[26]") != msg.npos)
          {
            throw DataOnTapeError("Data may be retrieved on set, but the NDS2 Client was not configured to retrieve data from tape");
          }
          else if(msg.find("timed out") != msg.npos)
          {
            throw QueryTimedOutError("Request for data timed out.  No data was received.");
          }
          else
          {
            throw NoDataError("Data was not found for one or more channels, or there was a gap in the data");
          }
        }
        catch (std::runtime_error ex)
        {
          cerr << "Error checking for data availability: " << ex.what() << endl;
          return false;
        }

        try {
          auto data_iterator = make_shared<NDS::data_iterable>(nds->nds->iterate(
              NDS::request_period(start,
                                  start + duration, 128), chan_names
              ));
          nds->data_queue.push(data_iterator);
        }
        catch (std::runtime_error ex) {
          cerr << "Error starting NDS stream: " << ex.what() << endl;
          return false;
        }
   

   
	return true;
    }


    bool nds2Manager::dataStop ()
    {
        nds->close();
	return true;
    }

    dataChannel* nds2Manager::find_channel(std::string name) {
      return &*(find(name));
    }

}
