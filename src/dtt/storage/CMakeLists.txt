SET(STORAGE_SRC
    channelinput.cc
    databroker.cc
    diagdatum.cc
    gdsdatum.cc
    lidaxinput.cc
    nds2input.cc
    rtddinput.cc
    
)

add_library(storage_lib OBJECT
    ${STORAGE_SRC}
)

target_include_directories(storage_lib PRIVATE
    ${DTT_INCLUDES}
)