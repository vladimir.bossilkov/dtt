//
// Created by erik.vonreis on 2/5/22.
//

#ifndef CDS_CRTOOLS_THREADSAFEQUEUE_H
#define CDS_CRTOOLS_THREADSAFEQUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>
#include <time.h>

namespace diag {
/**
   * Thrown when a queue operation times out
 */
class QueueTimeoutException : public std::runtime_error {
public:
  QueueTimeoutException() : std::runtime_error("") {}
};

/**
   * Thread safe queue that takes safe pointers (or pointers) of type T
   * And returns safe pointers of type T.
 */
template <class T> class ThreadSafeQueue {
protected:
  std::deque<std::shared_ptr<T>> _queue;
  std::mutex _mut;
  std::condition_variable _notempty;

public:
  void push(std::shared_ptr<T> item) {
    std::lock_guard<std::mutex> lock(_mut);
    _queue.push_back(item);
    auto size = _queue.size();
    _notempty.notify_one();
  }

  /**
     * item must be allocated. The caller relinquishes
     * ownership of item, which will become a shared_ptr
     * @param item
   */
  void push(const T *item) {
    std::shared_ptr<T> shared_item;
    shared_item.reset(item);
    this->push(shared_item);
  }

  bool empty() {
    return _queue.empty();
  }

  /**
     * Pop an item as a shared_ptr
     * @param timeout_ms
     * @return QueueTimeoutException thrown when timed out.
   */
  std::shared_ptr<T> pop(std::chrono::milliseconds timeout_ms) {

    std::unique_lock<std::mutex> lock(_mut);
    if(_notempty.wait_for(lock, timeout_ms, [this](){return !this->empty();})) {
      auto item = _queue.front();
      _queue.pop_front();
      return item;
    }

    throw QueueTimeoutException();
  }
};
}
#endif // CDS_CRTOOLS_THREADSAFEQUEUE_H
