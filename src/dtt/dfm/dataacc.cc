/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <time.h>
#include "framefast/fftype.hh"
#include "dataacc.hh"
#include "dfmapi.hh"
#include "fname.hh"
#include "framemux.hh"
#include "fchannel.hh"
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <iostream>
#include <memory>
#include <algorithm>

namespace dfm {
   using namespace std;

   const double epsilon = 1E-7;

   const char* const kLarsServiceName = "LARS";
   const char* const kNdsServerName = "NDS";
   const char* const kSendsServerName = "SENDS";
   const char* const kFileServerName = "Local file system";
   const char* const kTapeServerName = "Local tape drive/robot";
   const char* const kSMServerName = "Shared memory partition";
   const char* const kFuncServerName = "Function callback";


   static int my_debug=0 ;

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmaccess                                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class dfmaccess {
   public:
      /// dfm API pointer list
      typedef std::vector<dfmapi*> dfmlist;
   
      /// Constructor
      dfmaccess (dataaccess& acc, bool* abort = 0);
      /// Destructor
      ~dfmaccess();
      /// dfm request
      bool req (const dataaccess& data, bool stagingonly = false);
      /// dfm done
      bool done ();
      /// dfm abort (MT safe)
      void abort ();
      /// Return most recent error message
      std::string errormsg() const {
         return fErrMsg; }
      /// Input list
      fantom::smart_ilist& inp() {
         return fInp; }
      /// Output list
      fantom::smart_olist& out() {
         return fOut; }
      /// Input list
      fantom::framemux& mux() {
         return fMux; }
   protected:
      /// Pointer to access
      dataaccess*		fAcc;
      /// Pointer to abort
      bool*		fAbort;
      /// dfm list
      dfmlist		fDFM;
      /// input list
      fantom::smart_ilist fInp;
      /// output list
      fantom::smart_olist fOut;      
      /// frame mux (multi-input / multi-output)
      fantom::framemux	fMux;
      /// Error message
      std::string	fErrMsg;
   
      /// add an input channel
      bool inpadd (const selservers& sel, const selserverentry& e, 
                  fantom::channelquerylist* qlist, bool stagingonly);
      /// add an output channel
      bool outadd (const selservers& dest, const selserverentry& e);
   };

//______________________________________________________________________________
   dfmaccess::dfmaccess (dataaccess& acc, bool* abort)
   : fAcc (&acc), fAbort (abort), fMux (fInp, fOut, abort)
   {
   }

//______________________________________________________________________________
   dfmaccess::~dfmaccess()
   {
      abort();
      done();
   }

//______________________________________________________________________________
   bool dfmaccess::inpadd (const selservers& sel, 
                     const selserverentry& e, 
                     fantom::channelquerylist* qlist,
                     bool stagingonly)
   {
      // update channels
      ((selserverentry&)e).updateChannels();
      // get data server
      string sname ((const char*) e.getName());
      dataserver* ds = fAcc->get (sname);
      if (ds == 0) {
         fErrMsg = "No data server selected.";
         return false;
      }
      dfmapi* dfm = createDFMapi (ds->getType());
      if ((dfm ==  0) || !*dfm) {
         delete dfm;
         fErrMsg = "Unable to create data server API.";
         return false;
      }
      if (!dfm->supportInput()) {
         delete dfm;
         fErrMsg = "Data server does not support input.";
         return false;
      }
      if (stagingonly && !dfm->supportStaging()) {
         delete dfm;
         fErrMsg = "Data server does not support staging.";
         return false;
      }
      if (!dfm->open (ds->getAddr())) {
         delete dfm;
         fErrMsg = "Unable to open connection to data server.";
         return false;
      }
      if (!dfm->requestData (fMux, 
                           sel.selectedTime(), sel.selectedDuration(), 
                           e.getUDN(), stagingonly ? 
                           dfmapi::stagingonly : dfmapi::stagedata, 
                           sel.selectedStaging(), qlist)) {
         delete dfm;
         fErrMsg = "Unable to create data input request.";
         return false;
      }
      if (stagingonly) {
         delete dfm;
      }
      else {
         fDFM.push_back (dfm);
      }
      return true;
   }

//______________________________________________________________________________
   bool dfmaccess::outadd (const selservers& dest, 
                     const selserverentry& e)
   {
      string sname ((const char*) e.getName());
      if (e.getUDN().size() != 1) {
         fErrMsg = "Multiple or no UDN in output.";
         return false;
      }
      dataserver* ds = fAcc->get (sname);
      if (ds == 0) {
         fErrMsg = "No data server selected.";
         return false;
      }
      dfmapi* dfm = createDFMapi (ds->getType());
      if ((dfm ==  0) || !*dfm) {
         delete dfm;
         fErrMsg = "Unable to create data server API.";
         return false;
      }
      if (!dfm->supportOutput()) {
         delete dfm;
         fErrMsg = "Data server does not support output.";
         return false;
      }
      if (!dfm->open (ds->getAddr(), false)) {
         delete dfm;
         fErrMsg = "Unable to open connection to data server.";
         return false;
      }
      fantom::channelquerylist qchn (e.channels());
      if (!dfm->sendData (fMux, e.getUDN().begin()->first, 
                         e.format().c_str(), 
                         qchn.empty() ? 0 : &qchn)) {
         delete dfm;
         fErrMsg = "Unable to create data output request.";
         return false;
      }
      fDFM.push_back (dfm);
      return true;
   }

//______________________________________________________________________________
   bool dfmaccess::req (const dataaccess& data, bool stagingonly)
   {
      // create output requests
      if (!stagingonly) {
         if (data.dest().isMultiple()) {
            for (const_selserveriter i = data.dest().begin(); 
                i != data.dest().end(); ++i) {
               const selserverentry* e = &*i;
               if (!outadd (data.dest(), *e)) {
                  return false;
               }
            }
         }
         else {
            if (!outadd (data.dest(), data.dest().selectedS())) {
               return false;
            }
         }
      }
      // get output channel list
      typedef std::unique_ptr<fantom::channelquerylist> chn_ptr;
      chn_ptr chns (new fantom::channelquerylist);
      if (!data.getOutputChannelList (*chns)) {
         chns.reset ();
      }
   
      // create input requests
      if (data.sel().isMultiple()) {
         for (const_selserveriter i = data.sel().begin(); 
             i != data.sel().end(); ++i) {
            //const selserverentry* e = &*i;
            if (!inpadd (data.sel(), *i, chns.get(), stagingonly)) {
               return false;
            }
         }
      }
      else {
         if (!inpadd (data.sel(), data.sel().selectedS(), chns.get(), 
                     stagingonly)) {
            return false;
         }
      }
      return true;
   }

//______________________________________________________________________________
   void dfmaccess::abort () 
   {
      if (fAbort) *fAbort = true; 
      for (dfmlist::iterator i = fDFM.begin(); i != fDFM.end(); ++i) {
         (*i)->abort();
      }
   }

//______________________________________________________________________________
   bool dfmaccess::done ()
   {
      fInp.Clear();
      fOut.Clear();
      for (dfmlist::iterator i = fDFM.begin(); i != fDFM.end(); ++i) {
         delete *i; *i = 0;
      }
      fDFM.clear();
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dataservername                                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dataservername::dataservername (dataservicetype type, 
                     const std::string& addr)
   {
      switch (type) {
         case st_LARS:
            {
               string a = trim (addr.c_str());
               if (a.empty()) {
                  fName = string (kLarsServiceName);
               }
               else {
                  if (a.find (':') == string::npos) {
                     char buf[64];
                     sprintf (buf, ":%i", kLARSPORT);
                     a += buf;
                  }
                  fName = string (kLarsServiceName) + " " + a;
               }
               break;
            }
         case st_NDS:
            {
               string a = trim (addr.c_str());
               if (a.find (':') == string::npos) {
                  char buf[64];
                  sprintf (buf, ":%i", kNDSPORT);
                  a += buf;
               }
               fName = string (kNdsServerName) + " " + a;
               break;
            }
         case st_SENDS:
            {
               string a = trim (addr.c_str());
               if (a.find (':') == string::npos) {
                  char buf[64];
                  sprintf (buf, ":%i", kSENDSPORT);
                  a += buf;
               }
               fName = string (kSendsServerName) + " " + a;
               break;
            }
         case st_File:
            {
            
               fName = kFileServerName;
               break;
            }
         case st_Tape:
            {
            
               fName = kTapeServerName;
               break;
            }
         case st_SM:
            {
            
               fName = kSMServerName;
               break;
            }
         case st_Func:
            {
            
               fName = kFuncServerName;
               break;
            }
         default:
            {
               fName = "";
               break;
            }
      }
   }

//______________________________________________________________________________
   dataservicetype dataservername::getType() const
   {
      if (strcasecmp (fName.c_str(), kFileServerName) == 0) {
         return st_File;
      }
      else if (strcasecmp (fName.c_str(), kTapeServerName) == 0) {
         return st_Tape;
      }
      else if (strcasecmp (fName.c_str(), kSMServerName) == 0) {
         return st_SM;
      }
      else if (strncasecmp (fName.c_str(), kLarsServiceName,
                           strlen (kLarsServiceName)) == 0) {
         return st_LARS;
      }
      else if (strncasecmp (fName.c_str(), kNdsServerName,
                           strlen (kNdsServerName)) == 0) {
         return st_NDS;
      }
      else if (strncasecmp (fName.c_str(), kSendsServerName,
                           strlen (kSendsServerName)) == 0) {
         return st_SENDS;
      }
      else if (strcasecmp (fName.c_str(), kFuncServerName) == 0) {
         return st_Func;
      }
      else {
         return st_Invalid;
      }
   }

//______________________________________________________________________________
   string dataservername::getAddr() const
   {
      dataservicetype dtype = getType();
      if (dtype == st_LARS) {
         const char* p = fName.c_str() + strlen (kLarsServiceName);
         return trim (p);
      }
      else if (dtype == st_NDS) {
         const char* p = fName.c_str() + strlen (kNdsServerName);
         return trim (p);
      }
      else if (dtype == st_SENDS) {
         const char* p = fName.c_str() + strlen (kSendsServerName);
         return trim (p);
      }
      else {
         return "";
      }
   }

//______________________________________________________________________________
   bool dataservername::operator< (const dataservername& name) const
   {
      return strcasecmp (*this, name) < 0;
   }

//______________________________________________________________________________
   bool dataservername::operator== (const dataservername& name) const
   {
      return strcasecmp (*this, name) == 0;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dataserver                                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dataserver::dataserver () : fType (st_Invalid), fUpdate (false)
   {
   }

//______________________________________________________________________________
   dataserver::dataserver (dataservicetype type, const std::string& addr)
   : fType (type), fAddr (addr), fUpdate (false)
   {
   }

//______________________________________________________________________________
   dataserver::~dataserver ()
   {
   }

//______________________________________________________________________________
   UDNInfo* dataserver::get (const UDN& udn) const
   {
      if (my_debug) cerr << "dataserver::get(" << udn << ")" << endl ;
      //((dataserver*)this)->updateUDNs (false);
      if (my_debug){
	 cerr << "  There are " << fUDN.size() << " entries in the map." << endl ;
	 cerr << "  Candidates are: "<< endl ;
	 for (const_UDNiter i = fUDN.begin(); i != fUDN.end(); ++i)
	    cerr << "     " << i->first << endl ;
      }
      const_UDNiter f = fUDN.find (udn);
      if (f != fUDN.end()) {
	 if (my_debug) cerr << "dataserver::get() succeeded, return ptr to UDNInfo." << endl ;
         return (UDNInfo*) &(f->second);
      }
      else {
	 if (my_debug) cerr << "dataserver::get() failed, return 0." << endl ;
         return 0;
      }
   }

//______________________________________________________________________________
   UDNInfo* dataserver::insert (const UDN& udn)
   {
      if (my_debug) cerr << "dataserver::insert(" << udn << ")" << endl ;
      return insert (udn, UDNInfo());
   }

//______________________________________________________________________________
   UDNInfo* dataserver::insert (const UDN& udn, const UDNInfo& info)
   {
      if (my_debug) cerr << "dataserver::insert(" << udn << ", info)" << endl ;
      if (my_debug) cerr << "  info.channels.size() is " << info.channels().size() << endl ;
      UDNiter f = fUDN.find (udn);
      if (f != fUDN.end()) {
         f->second = info;
	 if (my_debug) cerr << "  fUDN.find(udn) succeeded, return ptr to info." << endl ;
         return &f->second;
      }
      else {
	 if (my_debug) cerr << "  fUDN.find(udn) failed." << endl ;
         pair <UDNiter, bool> f = fUDN.insert (
                              UDNList::value_type (udn, info));
         if (f.second) {
	    if (my_debug) cerr << "    fUDN.insert() succeeded. return ptr." << endl ;
            return &f.first->second;
         }
         else {
	    if (my_debug) cerr << "    fUDN.insert() failed, return 0." << endl ;
            return 0;
         }
      }
   }

//______________________________________________________________________________
   void dataserver::erase (const UDN& udn)
   {
      if (my_debug) cerr << "dataserver::erase(" << udn << ")" << endl ;
      fUDN.erase (udn);
   }

//______________________________________________________________________________
   void dataserver::clear()
   {
      if (my_debug) cerr << "dataserver::clear() - clears entire map." << endl ;
      fUDN.clear ();
   }

//______________________________________________________________________________
   int dataserver::size() const
   {
      if (my_debug) cerr << "dataserver::size() returns " << fUDN.size() << endl ;
      return fUDN.size();
   }

//______________________________________________________________________________
   bool dataserver::empty() const
   {
      return fUDN.empty();
   }

//______________________________________________________________________________
   bool dataserver::login (const UDN& udn,
                     const char* uname, const char* pword)
   {
      if (my_debug) cerr << "dataserver::login(" << udn << ", " << uname << ", " << pword << ")" << endl ;
      dfmapi* dfm = createDFMapi (fType);
      if ((dfm ==  0) || !*dfm) {
         delete dfm;
         return false;
      }
      if (!dfm->open (fAddr)) {
         delete dfm;
         return false;
      }
      if (!dfm->login (udn, uname, pword)) {
         delete dfm;
         return false;
      }
      delete dfm;
      return true;
   }

//______________________________________________________________________________
   bool dataserver::updateUDNs (bool force)
   {
      if (my_debug) cerr << "dataserver::updateUDNs( force = " << (force ? "TRUE" : "FALSE") << ")" << endl ; 
      if (!force && fUpdate) {
	 if (my_debug) cerr << "  fUpdate is true, return true." << endl ;
         return true;
      }
      if (force) clear();
      dfmapi* dfm = createDFMapi (fType);
      if ((dfm ==  0) || !*dfm) {
         delete dfm;
         return false;
      }
      if (my_debug) cerr << "  dataserver::updateUDNs() caling dfmapi::open(" << fAddr << ")" << endl ;
      if (!dfm->open (fAddr)) {
         delete dfm;
	 if (my_debug) cerr << "    failed to open " << fAddr << endl ;
         return false;
      }
      if (my_debug) cerr << "  dataserver::updateUDNs() calling dfmapi::cachedUDNs()" << endl ;
      if (!dfm->cachedUDNs (dataservername (fType, fAddr), 
                           fUDN, force)) {
         delete dfm;
	 if (my_debug) cerr << "    cachedUDNs() failed, return false" << endl ;
         return false;
      }
      delete dfm;
      fUpdate = true;
      if (my_debug) cerr << "dataServer::updateUDNs() return true, fUpdate = true" << endl ;
      return true;
   }

//______________________________________________________________________________
   bool dataserver::lookupUDN (const UDN& udn, bool force)
   {
      if (my_debug) cerr << "dataserver::lookupUDN(" << udn << (force ? ", force=TRUE)" : ", force=FALSE)") << endl ;
      if (!fUpdate) {
	 if (my_debug) cerr << "lookupUDN() - calling updateUDNs(" << (force ? "TRUE)" : "FALSE)") << endl ;
         if (!updateUDNs (force)) {
	    if (my_debug) cerr << "  lookupUDN() - updateUDNs() returned false,  return false." << endl ;
            return false;
         }
	 if (my_debug) cerr << "lookupUDN() - updateUDNs() returned true" << endl ;
      }
      UDNInfo* uinfo = get (udn);
      if (!force && uinfo && (uinfo->getType() != fantom::NONE)) {
	 if (my_debug) cerr << "lookupUDN() - get(" << udn << ") returned, return true." << endl ;
         return true;
      }
      //cerr << "lookupUDN() - uinfo returned by get() has " << uinfo->channels().size() << " channels" << endl ;
      cerr << "LOOKUP UDN = " << (const char*)udn << endl;
      dfmapi* dfm = createDFMapi (fType);
      if ((dfm ==  0) || !*dfm) {
	 cerr << "Unable to create " << udn << endl;
         delete dfm;
         return false;
      }
      if (my_debug) cerr << "lookupUDN() - calling dfm->open(" << fAddr << ")" << endl ;
      if (!dfm->open (fAddr)) {
	 cerr << "Unable to open " << udn << endl;
         delete dfm;
         return false;
      }
      UDNInfo info;
      if (my_debug) cerr << "lookupUDN() - calling dfm->cachedUDNInfo(" << udn << ", info, " << (force ? ", force=TRUE)" : ", force=FALSE)") << endl ;
      if (!dfm->cachedUDNInfo (udn, info, force)) {
	 cerr << "no cached info " << udn << endl;
         delete dfm;
         return false;
      }
      if (my_debug) cerr << "lookupUDN() - cachedUDNInfo() returned, number of channels is " 
	 << info.channels().size()  << endl ;

      delete dfm;
      dfm::UDNInfo *rc = insert (udn, info) ;
      if (my_debug) cerr << "lookupUDN() - after insert, number of channels is " << info.channels().size()  << endl ;
      if (rc && my_debug) cerr << "   Return of insert: number of channels is " << rc->channels().size() << endl ;
      return rc != 0;
   }

//______________________________________________________________________________
   bool dataserver::lookupUDNs (bool force)
   {
      if (my_debug) cerr << "dataserver::lookupUDNs()" << endl ;
      if (!fUpdate) {
         if (!updateUDNs (force)) {
	    if (my_debug) cerr << "  dataserver::lookupUDNs() - updateUDNs() returned false" << endl ;
            return false;
         }
      }
      dfmapi* dfm = createDFMapi (fType);
      if ((dfm ==  0) || !*dfm) {
         delete dfm;
         return false;
      }
      if (my_debug) cerr << " dataserver::lookupUDNs() - calling dfm->open(" << fAddr << ")" << endl ;
      if (!dfm->open (fAddr)) {
         delete dfm;
	 if (my_debug) cerr << "  open failed." << endl ;
         return false;
      }
      for (UDNiter i = begin(); i != end(); ++i) {
	 if (my_debug) cerr << " dataserver::lookupUDNs(), dfm->cachedUDNInfo(" << i->first << ", ...) " << endl ;
         if (!dfm->cachedUDNInfo (i->first, i->second, force)) {
            delete dfm;
	    if (my_debug) cerr << "  cachedUDNInfo failed, return false." << endl ;
            return false;
         }
      }
      delete dfm;
      if (my_debug) cerr << "dataserver::lookupUDNs() return true" << endl ;
      return true;
   }

//______________________________________________________________________________
   bool dataserver::supportMultiUDN() const
   {
      dfmapi* dfm = createDFMapi (fType);
      bool ret = false;
      if (dfm && !!*dfm) {
         ret = dfm->supportMultiUDN();
      }
      delete dfm;
      return ret;
   }

//______________________________________________________________________________
   bool dataserver::supportStaging() const
   {
      dfmapi* dfm = createDFMapi (fType);
      bool ret = false;
      if (dfm && !!*dfm) {
         ret = dfm->supportStaging();
      }
      delete dfm;
      return ret;
   }

//______________________________________________________________________________
   bool dataserver::supportInput() const
   {
      dfmapi* dfm = createDFMapi (fType);
      bool ret = false;
      if (dfm && !!*dfm) {
         ret = dfm->supportInput();
      }
      delete dfm;
      return ret;
   }

//______________________________________________________________________________
   bool dataserver::supportOutput() const
   {
      dfmapi* dfm = createDFMapi (fType);
      bool ret = false;
      if (dfm && !!*dfm) {
         ret = dfm->supportOutput();
      }
      delete dfm;
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// selserverentry                                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   selserverentry::selserverentry (dataservername name, 
                     const UDNList& udns) 
   : fName (name), fUDN (udns), fFType (fantom::FF), fFLength (1),
   fFNum (1), fFCompr (0), fFVers (framefast::kDefaultFrameVersion), 
   fId (0) 
   {
   }

//______________________________________________________________________________
   bool selserverentry::setChannels (const char* chns)
   {
      return String2Channels (fChannels, chns);
   }

//______________________________________________________________________________
   std::string selserverentry::getChannels () const
   {
      string s;
      if (!Channels2String (fChannels, s)) s = "";
      return s;
   }

//______________________________________________________________________________
   bool selserverentry::updateChannels()
   {
      // clear channel list of UDNs
      for (UDNiter i = fUDN.begin(); i != fUDN.end(); ++i) {
         i->second.clearChn();
      }
      if (fUDN.empty()) {
         return true;
      }
      UDNInfo& first = fUDN.begin()->second;
      for (chniter i = fChannels.begin(); i != fChannels.end(); ++i) {
         const char* udn = i->UDN();
         // no name = insert at first position
         if ((udn == 0) || (*udn == 0)) {
	    if (i->Rate() < 1.0) {
	       cerr << "*** Warning in selserverentry::updateChannels: Rate < 1"
		    << endl;
	       first.insertChn (i->Name(), 1);
	    } else {
	       first.insertChn (i->Name(), i->Rate());
	    }
         }
         // otherwise add accordingly
         else {
            UDNiter f = fUDN.find (UDN (udn));
            if (f != fUDN.end()) {
	       if (i->Rate() < 1.0) {
		  cerr << "*** Warning in selserverentry::updateChannels: Rate < 1"
		       << endl;
		  f->second.insertChn (i->Name(), 1);
	       } else {
		  f->second.insertChn (i->Name(), i->Rate());
	       }
            }
         }
      }
      return false;
   }

//______________________________________________________________________________
   bool selserverentry::setChannels (const UDN& udn, const char* chns)
   {
      UDNiter u = fUDN.find (udn);
      if (u == fUDN.end()) {
         return false;
      }
      else {
         return String2Channels (u->second.channels(), chns);
      }
   }

//______________________________________________________________________________
   bool selserverentry::setChannels (const UDN& udn, 
                     const channellist& chns) 
   {
      UDNiter u = fUDN.find (udn);
      if (u == fUDN.end()) {
         return false;
      }
      else {
         u->second.channels() = chns;
         return true; 
      }
   }

//______________________________________________________________________________
   std::string selserverentry::getChannels (const UDN& udn) const
   {
      string s;
      const_UDNiter u = fUDN.find (udn);
      if (u != fUDN.end()) {
         if (!Channels2String (u->second.channels(), s)) s = "";
      }
      return s;
   }

//______________________________________________________________________________
   const fantom::channellist* selserverentry::channels (
                     const UDN& udn) const 
   {
      const_UDNiter u = fUDN.find (udn);
      if (u == fUDN.end()) {
         return 0;
      }
      else {
         return &u->second.channels();
      }
   }

//______________________________________________________________________________
   void selserverentry::selectFormat (const char* format)
   {
      fantom::string_to_fformat (format, fFType, fFLength, fFNum, 
                           fFCompr, fFVers);
   }

//______________________________________________________________________________
   std::string selserverentry::format () const
   {
      return fantom::fformat_to_string (fFType, fFLength, fFNum, 
                           fFCompr, fFVers);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// selservers                                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   selservers::selservers()
      : fMulServers(false), fStart (Now()), fDuration (1.), fStagingKeep (1800.)
   {
      fStart.setN (0);
      fSelServer = fActiveM.begin();
   }

//______________________________________________________________________________
   selservers::selservers (const selservers& ss)
   {
      *this = ss;
   }

//______________________________________________________________________________
   selservers& selservers::operator= (const selservers& ss)
   {
      if (this != &ss) {
         fMulServers = ss.fMulServers;
         fActiveS = ss.fActiveS;
         fActiveM = ss.fActiveM;
         fStart = ss.fStart;
         fDuration = ss.fDuration;
         fStagingKeep = ss.fStagingKeep;
         fSelServer = fActiveM.begin();
         int d = ss.fSelServer - ss.fActiveM.begin();
         if ((d >= 0) && (d < (int)fActiveM.size())) {
            advance (fSelServer, d);
         }
      }
      return *this;
   }

//______________________________________________________________________________
   bool selservers::add (dataservername name, const UDNList& udns)
   {
      selserverentry e (name, udns);
      return add (e);
   }

//______________________________________________________________________________
   bool selservers::add (const selserverentry& sel)
   {
      if (fActiveM.empty()) {
         fActiveS = sel;
      }
      fActiveM.push_back (sel);
      if (fActiveM.size() == 1) {
         fSelServer = fActiveM.begin();
      }
      fMulServers = fActiveM.size() > 1;
      return true;
   }

//______________________________________________________________________________
   void selservers::erase (selserveriter pos)
   {
      int d = pos - fSelServer;
      fActiveM.erase (pos);
      if (d == 0) {
         fSelServer = fActiveM.begin();
      }
      else if (d < 0) {
         --fSelServer;
      }
   }

//______________________________________________________________________________
   void selservers::clear()
   {
      fActiveM.clear();
      fActiveS = selserverentry ();
      fSelServer = fActiveM.begin();
   }

//______________________________________________________________________________
   bool selservers::selectServer (const std::string& sname,
                     const serverlist* servers)
   {
      if (isMultiple()) {
         selserveriter f = find (fActiveM.begin(), fActiveM.end(), 
                              selserverentry (dataservername (sname)));
         if (f == fActiveM.end()) {
            return false;
         }
         else {
            fSelServer = f;
            return true;
         }
      }
      else if (servers == 0) {
         fActiveS.setName (dataservername (sname));
         return true;
      }
      else {
         const_serveriter f = servers->find (dataservername (sname));
         if (f == servers->end()) {
            return false;
         }
         else {
            fActiveS.setName (f->first);
            return true;
         }
      }
   }

//______________________________________________________________________________
   std::string selservers::selectedServer ()
   {
      if (isMultiple()) {
         if (fSelServer != fActiveM.end()) {
            return fSelServer->getName().get();
         }
         else {
            return "";
         }
      }
      else {
         return fActiveS.getName().get();
      }
   }

//______________________________________________________________________________
   selserverentry* selservers::selectedEntry()
   {
      if (isMultiple()) {
         if (fSelServer == fActiveM.end()) {
            return 0;
         }
         else {
            return &*fSelServer;
         }
      }
      else {
         return &fActiveS;
      }
   }

//______________________________________________________________________________
   const selserverentry* selservers::selectedEntry() const
   {
      return const_cast<selservers*>(this)->selectedEntry();
   }

//______________________________________________________________________________
   bool selservers::selectUDN (const UDNList& udn)
   {
      if (isMultiple()) {
         if (fSelServer == fActiveM.end()) {
            return false;
         }
         fSelServer->setUDN (udn);
      }
      else {
         fActiveS.setUDN (udn);
      }
      return true;
   }

//______________________________________________________________________________
   bool selservers::selectedUDN (UDNList& udn) const
   {
      if (isMultiple()) {
         if (fSelServer == fActiveM.end()) {
            return false;
         }
         udn = fSelServer->getUDN ();
      }
      else {
         udn = fActiveS.getUDN ();
      }
      return true;
   }

//______________________________________________________________________________
   bool selservers::selectChannels (const char* chns)
   {
      if (isMultiple()) {
         if (fSelServer == fActiveM.end()) {
            return false;
         }
         return fSelServer->setChannels (chns);
      }
      else {
         return fActiveS.setChannels (chns);
      }
   }

//______________________________________________________________________________
   std::string selservers::selectedChannels () const
   {
      string s;
      if (isMultiple()) {
         if (fSelServer == fActiveM.end()) {
            s = "";
         }
         else {
            s = fSelServer->getChannels ();
         }
      }
      else {
         s = fActiveS.getChannels ();
      }
      return s;
   }

//______________________________________________________________________________
   bool selservers::selectTime (const Time& start, 
                     const Interval& duration, const serverlist* servers)
   {
      fStart = start;
      fDuration = duration;
      if (!servers) {
         return true;
      }
      // check if time interval is within
      for (selserveriter i = fActiveM.begin(); i != fActiveM.end(); ++i) {
         // get server
         string sname;
         const UDNList* u = 0;
         if (isMultiple()) {
            sname = i->getName();
            u = &i->getUDN();
         }
         else { 
            sname = fActiveS.getName();
            u = &fActiveS.getUDN();
         }
         const_serveriter f = servers->find (dataservername (sname));
         if ((f == servers->end()) || (u == 0)) {
            continue;
         }
         const dataserver* ds = &f->second;
         // check each of the selected UDN
         for (const_UDNiter i = u->begin(); i != u->end(); ++i) {
            UDNInfo* info = ds->get (i->first);
            if ((info == 0) ||
               (info->beginDSeg() == info->endDSeg())) {
               continue;
            }
            UDNInfo::dsegiter last = info->endDSeg();
            --last;
         // cerr << "start = " << start << "  duration = " << duration <<
            // " first = " << info->beginDSeg()->first << " last = " <<
            // last->first << " " << last->second << endl;
            if ((start + duration < info->beginDSeg()->first) ||
               (start >= last->first + last->second)) {
               return false;
            }
         }
         if (!isMultiple()) {
            break;
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool selservers::selectStaging (const Interval& keep)
   {
      fStagingKeep = keep;
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dataaccess                                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   static const dataservicetype kSuppArr[] = 
   {st_LARS, st_NDS, st_SENDS, st_File, st_Tape, st_SM, st_Func};

   const dataaccess::serversupp dataaccess::kSuppAll 
   (kSuppArr, kSuppArr + 6);

//______________________________________________________________________________
   dataaccess::dataaccess () 
   : fAbort (0), fDFM (0)
   {
   }

//______________________________________________________________________________
   dataaccess::dataaccess (serversupp supp)
   : fAbort (0), fDFM (0)
   {
      support (supp);
   }

//______________________________________________________________________________
   dataaccess::~dataaccess() 
   {
      done();
   }

//______________________________________________________________________________
   bool dataaccess::support (dataservicetype supp, bool add) 
   {
      // make sure we don't add/subtract twice
      if ((fSupport.count (supp) == 0) ^ (add != 0)) {
         return true;
      }
      // add
      if (add) {
         // update support flag
         fSupport.insert (supp);
         // if file, tape or SM, add server
         switch (supp) {
            case st_LARS:
               {
                  dataservername dn (st_LARS, "");
                  dataserver ds (st_LARS, "");
                  if (!insert (dn.get(), ds)) {
                     fErrMsg = "Unable to add fantom server.";
                     return false;
                  }
                  lookupServers (st_LARS);
                  // set input default to fantom
                  if (sel().selectedServer().empty()) {
                     sel().setMultiple (false);
                     sel().selectServer (dn.get());
                  }
                  break;
               }
            case st_NDS:
               {
                  lookupServers (st_NDS);
                  break;
               }
            case st_SENDS:
               {
                  lookupServers (st_SENDS);
                  break;
               }
            case st_File:
               {
                  dataserver ds (st_File, "");
                  if (!insert (kFileServerName, ds)) {
                     fErrMsg = "Unable to add file server.";
                     return false;
                  }
                  // set input default to file
                  if (sel().selectedServer().empty()) {
                     sel().setMultiple (false);
                     sel().selectServer (kFileServerName);
                  }
                  // set output default to file
                  if (dest().selectedServer().empty()) {
                     dest().setMultiple (false);
                     dest().selectServer (kFileServerName);
                  }
                  break;
               }
            case st_Tape:
               {
                  dataserver ds (st_Tape, "");
                  if (!insert (kTapeServerName, ds)) {
                     fErrMsg = "Unable to add tape server.";
                     return false;
                  }
                  break;
               }
            case st_SM: 
               {
                  dataserver ds (st_SM, "");
                  if (!insert (kSMServerName, ds)) {
                     fErrMsg = "Unable to add shared memory server.";
                     return false;
                  }
                  break;
               }
            case st_Func: 
               {
                  dataserver ds (st_Func, "");
                  if (!insert (kFuncServerName, ds)) {
                     fErrMsg = "Unable to add function callback server.";
                     return false;
                  }
                  break;
               }
            default:
               {
                  break;
               }
         }
      }
      // subtract
      else {
         // remove corresponding servers
         for (serveriter i = fServers.begin(); i != fServers.end(); ) {
            if (i->second.getType() == supp) {
               fServers.erase (i++);
            }
            else {
               ++i;
            }
         }
         // update support flag
         fSupport.erase (supp);
      }
      return true;
   }

//______________________________________________________________________________
   bool dataaccess::support (const serversupp& supp) 
   {
      bool ret = true;
      for (serversupp::const_iterator i = kSuppAll.begin(); 
          i != kSuppAll.end(); ++i) {
         if (!support (*i, supp.count (*i) > 0)) {
            ret = false;
         }
      }
      return true;
   }

//______________________________________________________________________________
   int dataaccess::lookupServers (dataservicetype type)
   {
      int snum = 0;
      switch (type) {
         case st_NDS:
            {
               // get environment variable
               const char* serv = getenv (kNdsServer);
               if (!serv || !*serv) {
                  break;
               }
               // scan environment variable for config file names
               char* s = new (nothrow) char [strlen (serv) + 10];
               strcpy (s, serv);
               char* last;
               char* p = strtok_r (s, ",", &last);
               while (p) {
                  string addr = trim (p);
                  p = strtok_r (0, ",", &last);
                  // add NDS server
                  dataservername dn (st_NDS, addr.c_str());
                  dataserver ds (st_NDS, addr.c_str());
                  if (!insert (dn.get(), ds)) {
                     fErrMsg = "Unable to add NDS server.";
                  }
                  else {
                     snum++;
                  }
               }
               delete [] s;
               break;
            }
         case st_SENDS:
            {
               // get environment variable
               const char* serv = getenv (kSendsServer);
               if (!serv || !*serv) {
                  break;
               }
               // scan environment variable for config file names
               char* s = new (nothrow) char [strlen (serv) + 10];
               strcpy (s, serv);
               char* last;
               char* p = strtok_r (s, ",", &last);
               while (p) {
                  string addr = trim (p);
                  p = strtok_r (0, ",", &last);
                  // add NDS server
                  dataservername dn (st_SENDS, addr.c_str());
                  dataserver ds (st_SENDS, addr.c_str());
                  if (!insert (dn.get(), ds)) {
                     fErrMsg = "Unable to add SENDS server.";
                  }
                  else {
                     snum++;
                  }
               }
               delete [] s;
               break;
            }
         default:
            {
               break;
            }
      }
      return snum;
   }

//______________________________________________________________________________
   dataserver* dataaccess::get (const std::string& sname) const
   {
      const_serveriter f = fServers.find (dataservername (sname));
      if (f == fServers.end()) {
         fErrMsg = "Data server type not found.";
         return 0;
      }
      else {
         return (dataserver*) (&f->second);
      }
   }

//______________________________________________________________________________
   bool dataaccess::insert (const std::string& sname, 
                     const dataserver& ds)
   {
      if (my_debug) cerr << "dataaccess::insert(" << sname << ", ...)" << endl ;
      if (fSupport.count (ds.getType()) <= 0) {
         fErrMsg = "Data server type not supported.";
         return false;
      }
      dataserver* f = get (sname);
      if (f) {
         *f = ds;
         return true;
      }
      else {
         pair <serveriter, bool> f = 
            fServers.insert (serverlist::value_type 
                            (dataservername (sname), ds));
         if (!f.second) {
            fErrMsg = "Unable to add data server.";
         }
         return f.second;
      }
   }

//______________________________________________________________________________
   void dataaccess::erase (const std::string& sname)
   {
      fServers.erase (dataservername (sname));
      // check selected server (single)
      if (sel().selectedS().getName() == dataservername (sname)) {
         ((selserverentry&)(sel().selectedS())).setName (dataservername (""));
      }
      // check multi server list
      for (selserveriter i = sel().begin(); i != sel().end(); ) {
         if (*i == dataservername (sname)) {
            sel().erase (i);
         }
         else {
            ++i;
         }
      }
      // check selected client (single)
      if (dest().selectedS().getName() == dataservername (sname)) {
         ((selserverentry&)(dest().selectedS())).setName (dataservername (""));
      }
      // check multi client list
      for (selserveriter i = dest().begin(); i != dest().end(); ) {
         if (*i == dataservername (sname)) {
            dest().erase (i);
         }
         else {
            ++i;
         }
      }
   }

//______________________________________________________________________________
   void dataaccess::clear (bool all)
   {
      if (all) fServers.clear ();
      sel().clear();
      dest().clear();
   }

//______________________________________________________________________________
   bool dataaccess::addEntry (bool isClient, const std::string& name, 
                     const std::string& udn,
                     const fantom::channellist& chns, 
                     const std::string& format)
   {
      // check if valid entry
      if (name.empty() || udn.empty()) {
         return false;
      }
      dataserver* ds = get (name);
      dataservername dname (name.c_str());
      // If NDS lookup UDNs/channels/etc.
      if (dname.getType() == st_NDS) {
         dataserver dsrv (st_NDS, dname.getAddr());
         insert (dname.get(), dsrv);
         ds = get (name);
         if (ds) ds->lookupUDNs();
      } else if (dname.getType() == st_SENDS) {
         dataserver dsrv (st_SENDS, dname.getAddr());
         insert (dname.get(), dsrv);
         ds = get (name);
         if (ds) ds->lookupUDNs();
      }
      if (!ds) {
         return false;
      }
      // add server/udn
      UDNList ul;
      UDN u (udn.c_str());
      ul[u] = UDNInfo();
      selserverentry e (dataservername (name.c_str()), ul);
      // set channels
      e.setChannels (chns);
      // set format
      if (isClient) {
         e.selectFormat (format.c_str());
      }
      selservers& sels = isClient ? dest() : sel();
      switch (ds->getType()) {
         // add udn to data server if necessary
         case st_File:
         case st_Tape:
         case st_SM:
         case st_Func:
            {
               ds->insert (u);
               if (!isClient) ds->lookupUDN (u);
               break;
            }
         // lookup UDN
         case st_LARS:
            {
               if (!isClient) ds->lookupUDN (u);
               break;
            }
         // NDS already dealt with above
         default:
            {
               break;
            }
      }
      // add selected server/udn
      return sels.add (e);
   }

//______________________________________________________________________________     
   bool dataaccess::getInputChannelList (
                     fantom::channellist& chnavail) const
   {
      chnavail.clear();
      // multiple selection
      if (sel().isMultiple()) {
         for (const_selserveriter seliter = sel().begin(); 
             seliter != sel().end(); ++seliter) {
            dataserver* ds = get ((const char*)seliter->getName());
            if (!ds) {
               continue;
            }
            // go through UDN list
            fantom::channelquerylist q (seliter->channels());
            for (const_UDNiter i = seliter->getUDN().begin();
                i != seliter->getUDN().end(); ++i) {
               UDNInfo* f = ds->get (i->first);
               if (f != 0) {
                  FilterChannels (f->channels(), chnavail, 
                                 q.empty() ? 0 : &q);
               }
            }
         }
      }
      // single selection
      else {
         const selserverentry* entry = sel().selectedEntry();
         dataserver* ds = 0;
         if (entry) {
            ds = get ((const char*)entry->getName());
         }
         // go through UDN list
         if (ds) {
            fantom::channelquerylist q = sel().selectedS().channels();
            for (const_UDNiter i = entry->getUDN().begin(); 
                i != entry->getUDN().end(); ++i) {
               const UDNInfo* f = ds->get (i->first);
               if (f != 0) {
                  FilterChannels (f->channels(), chnavail, 
                                 q.empty() ? 0 : &q);
               }
            }
         }
      }
      SortChannels (chnavail);
      return true;
   }

//______________________________________________________________________________     
   bool dataaccess::getOutputChannelList (
                     fantom::channelquerylist& outchns) const    
   {
      outchns.clear();
      // multiple selection
      if (dest().isMultiple()) {
         for (const_selserveriter destiter = dest().begin(); 
             destiter != dest().end(); ++destiter) {
            // get channel list
            outchns.add (destiter->channels());
         }
      }
      // single selection
      else {
         outchns.add (dest().selectedS().channels());
      }
      return true;
   }

//______________________________________________________________________________
   bool dataaccess::staging()
   {
      if (fAbort) *fAbort = false;
      dfmaccess* dfm = new (nothrow) dfmaccess (*this, fAbort);
      if (!dfm) {
         return false;
      }
      bool ret = dfm->req (*this, true);
      if (!ret) {
         fErrMsg = dfm->errormsg();
      }
      delete dfm;
      return ret;
   }

//______________________________________________________________________________
   bool dataaccess::request ()
   {
      done();
      if (fAbort) *fAbort = false;
      // setup dfm api inputs & outputs
      dfmaccess* dfm = new (nothrow) dfmaccess (*this, fAbort);
      if (!dfm) {
         return false;
      }
      if (dfm->req (*this, false)) {
         fDFM = dfm;
      }
      else {
         fErrMsg = dfm->errormsg();
         delete dfm;
         return false;
      }
   
      return true;
   }

//______________________________________________________________________________
   void dataaccess::abort()
   {
      if (fAbort) *fAbort = true;
      if (fDFM) {
         fDFM->abort();
      }
   }

//______________________________________________________________________________
   Time dataaccess::processTime() const
   {
      return Time (fDFM ? fDFM->mux().clock() : Time (0,0));
   }

//______________________________________________________________________________
   Interval dataaccess::process()
   {
      return Interval (fDFM ? fDFM->mux().process() : -1.);
   }

//______________________________________________________________________________
   void dataaccess::flush()
   {
      if (fDFM) fDFM->mux().flushOutput();
   }

//______________________________________________________________________________
   bool dataaccess::processAll()
   {
      if (!fDFM) {
         return false;
      }
   
      Time T1 = sel().selectedStop() - Interval (epsilon);
      while (processTime() < T1) {
         // process next frame
         if (process() <= Interval (0)) {
            return false;  // incomplete!
         }
      }
      flush();
      return true;
   }

//______________________________________________________________________________
   void dataaccess::done()
   {
      if (fDFM) delete fDFM; 
      fDFM = 0;
   }

//______________________________________________________________________________
   bool dataaccess::inlog (fantom::fmsgqueue& mq)
   {
      mq.clear();
      if (!fDFM) {
         return false;
      }
      else {
         for (fantom::smart_ilist::iterator i = fDFM->mux().inp().begin();
             i != fDFM->mux().inp().end(); ++i) {
            fantom::fmsgqueue::fmsg msg;
            while (i->second->poplog (msg)) {
               msg.setparam (3, i->first);
               mq.push (msg);
            }
         }
         return true;
      }
   }

//______________________________________________________________________________
   bool dataaccess::outlog (fantom::fmsgqueue& mq)
   {
      mq.clear();
      if (!fDFM) {
         return false;
      }
      else {
         for (fantom::smart_olist::iterator i = fDFM->mux().out().begin();
             i != fDFM->mux().out().end(); ++i) {
            fantom::fmsgqueue::fmsg msg;
            while (i->second->poplog (msg)) {
               msg.setparam (3, i->first);
               mq.push (msg);
            }
         }
         return true;
      }
   }

//______________________________________________________________________________
   void dataaccess::ClearCache()
   {
      dfmapi::ClearCache();
   }

}
