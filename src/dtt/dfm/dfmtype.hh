/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dfmtype							*/
/*                                                         		*/
/* Module Description: Basic data types for dfm				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 26Sep00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: fftype.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_DFMTYPE_H
#define _LIGO_DFMTYPE_H

#include "framefast/fftype.hh"
#include "fantomtype.hh"
/** @name Atomic dfmapi data types
    Data types used by the lidax IO API.
   
    @memo Atomic dfmapi data types
    @author Written October 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

namespace dfm {

/** Type of data service
    
    @memo Service type.
 ************************************************************************/
   enum dataservicetype {
   /// Invalid server
   st_Invalid = 0,
   /// LARS/DFM server
   st_LARS = 1,
   /// NDS server
   st_NDS = 2,
   /// NDS server
   st_SENDS = 3,
   /// Local file system
   st_File = 4,
   /// Local tape drive/robot
   st_Tape = 5,
   /// Online shared memory
   st_SM = 6,
   /// User provided callback services
   st_Func = 7
   };


/** Environment variables for setting up data servers and UDN lists.
    
    @memo Environment variables.
 ************************************************************************/
//@{

/** Describes a list of NDS servers. The NDSSERVER environment 
    variable lists a set of NDS addresse--separated by commas.
    The server address is of the format:
    \begin{verbatim}
    'ip address'[:'port number']
    \end{verbatim}
    The port number is optional and defaults to 8088.

    Example:
    \begin{verbatim}
    setenv NDSSERVER red.ligo-wa:8088,london.ligo-la
    \end{verbatim}

    @memo NDS server list.
 ************************************************************************/
   const char* const kNdsServer = "NDSSERVER";


/** Describes a list of NDS2 servers. The NDS2SERVER environment 
    variable lists a set of NDS addresse--separated by commas.
    The server address is of the format:
    \begin{verbatim}
    'ip address'[:'port number']
    \end{verbatim}
    The port number is optional and defaults to 8088.

    Example:
    \begin{verbatim}
    setenv NDS2SERVER red.ligo-wa:8088,lonon.ligo-la
    \end{verbatim}

    @memo NDS2 server list.
 ************************************************************************/
   const char* const kSendsServer = "NDS2SERVER";

/** Describes a list of local resources. The UDNFILE environment 
    variable lists a set of configuration files--separated by 
    colons--containing valid file and directory UDNs. A configuration 
    file can contain comments (lines starting with #); all other 
    lines are interpreted as a UDN.
   
    Example:
    \begin{verbatim}
    setenv UDNFILE /home/sigg/file1.udn:/home/sigg/file2.udn
    \end{verbatim}
    
    Example /home/sigg/file1.udn:
    \begin{verbatim}
    # E2 data on fortress
    dir:///export/raid3/E2/00-11-08_19:59:35.@
    dir:///export/raid2/E2/00-11-09_09:17:47.@
    dir:///export/raid2/E2/00-11-12_17:10:19.@
    dir:///export/raid2/E2/00-11-14_00:25:08.@
    \end{verbatim}

    @memo Local disk resources.
 ************************************************************************/
   const char* const kFileUDN = "UDNFILE";

/** Describes a shared memory partition. The LIGOSMPART environment 
    variable lists the name of the default shared memory partition;
    whereas the LIGOSMPARTS environment variable lists a set of
    colon separeted shared memory partition names.
    
    Example:
    \begin{verbatim}
    setenv LIGOSMPART LHO_Online
    setenv LIGOSMPARTS sigg_1:sigg_2
    \end{verbatim}

    @memo Shared memory partition.
 ************************************************************************/
   const char* const kSmUDN = "LIGOSMPART";
   const char* const kSmUDNs = "LIGOSMPARTS";

//@}

}
//@}


#endif // _LIGO_DFMTYPE_H
