/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: udn							*/
/*                                                         		*/
/* Module Description: Universal Data set Descriptor			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: udn.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_DFMUDN_H
#define _LIGO_DFMUDN_H


#include "Time.hh"
#include "Interval.hh"
#include "fchannel.hh"
#include "dfmtype.hh"
#include <string>
#include <map>
#include <vector>


namespace dfm {


/** @name Universal Data set Descriptor 
    This header defines support methods for handling UDNs.
   
    @memo UDN API
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Unified data descriptor
    
    @memo Unified data descriptor.
 ************************************************************************/
   class UDN {
   public:
      /// Constructor
      explicit UDN (const char* name = 0) : 
      fName (name ? name : ""), fValid (true) {
         check(); }
      /// UDN as const char
      operator const char* () const {
         return fValid ? fName.c_str() : ""; }
      /// UDN is invalid?
      bool empty(void) const {
	 return fName.empty(); }
      bool operator! () const {
         return !fValid; }
      /// compare smaller
      bool operator< (const UDN& udn) const;
      /// compare equal
      bool operator== (const UDN& udn) const;   
   
   protected:
      /// Name of UDN
      std::string	fName;
      /// Valid?
      bool		fValid;
   
      /// check if UDN is valid
      void check();
   };


/** Unified data descriptor information
    
    @memo Unified data descriptor information.
 ************************************************************************/
   class UDNInfo {
   public:
      /// UDN type
      typedef framefast::frametype UDNType;
      /// Channel list
      typedef fantom::channellist channellist;
      /// Channel iterator
      typedef fantom::chniter chniter;
      /// Const channel iterator
      typedef fantom::const_chniter const_chniter;
   
      /// Data interval list
      typedef std::map <Time, Interval> dataseglist;
      /// Data segment list iterator
      typedef dataseglist::iterator dsegiter;
   
      /// Default constructor
      UDNInfo () : fType (framefast::NONE) {
      }
      /// Destructor
      ~UDNInfo() {
      }
   
      /// Get type
      UDNType getType () const {
         return fType; }
      /// Set type
      void setType (UDNType type) {
         fType = type; }
   
      /// Get channel list
      channellist& channels() {
         return fChannels; }
      /// Get channel list
      const channellist& channels() const {
         return fChannels; }
      /// Set channel list (must be sorted!)
      void setChannels (const channellist& chns) {
         fChannels = chns; }
      /// Get channel
      chniter findChn (const char* chn);
      /// Add channel
      std::pair <chniter, bool> insertChn (const char* chn, 
                        int rate = 0);
      /// Delete channel
      void eraseChn (const char* chn);
      /// Delete all channels
      void clearChn();
      /// Channel begin (list is sorted and has no duplicates)
      chniter beginChn() {
         return fChannels.begin(); }
      /// Channel end (list is sorted and has no duplicates)
      chniter endChn() {
         return fChannels.end(); }
      /// Channel begin (list is sorted and has no duplicates)
      const_chniter beginChn() const {
         return fChannels.begin(); }
      /// Channel end (list is sorted and has no duplicates)
      const_chniter endChn() const {
         return fChannels.end(); }
   
      /// Set data segment list (must be sorted!)
      void setDataSegs (const dataseglist& dsegs) {
         fDIntervals = dsegs; }
      /// Get data segment list
      const dataseglist& dataSegs() const {
         return fDIntervals; }
      /// Get data segment
      dsegiter findDSeg (const Time& start);
      /// Add data segment
      void insertDSeg (const Time& start, const Interval& duration);
      /// Delete data segment
      void eraseDSeg (const Time& start);
      /// Delete all data segmentss
      void clearDSeg();
      /// Data segment begin
      dsegiter beginDSeg() {
         return fDIntervals.begin(); }
      /// Data segment end
      dsegiter endDSeg() {
         return fDIntervals.end(); }
   
   protected:
      /// UDN type
      UDNType		fType;
      /// Channel list (list is sorted and has no duplicates)
      channellist	fChannels;
      /// List of data segments
      dataseglist	fDIntervals;
   };


/** List of unified data descriptor with information
    
    @memo Unified data descriptor list.
 ************************************************************************/
   typedef std::map <UDN, UDNInfo> UDNList;
   /// UDN list iterator
   typedef UDNList::iterator UDNiter;
   /// Const UDN list iterator
   typedef UDNList::const_iterator const_UDNiter;


//@}

}

#endif // _LIGO_DFMUDN_H
