/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  awggui                                                      */
/*                                                                           */
/* Module Description:  ROOT gui for defining the AWG.                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/** @name awggui
    This is the main program for the arbitrary waveform generator.
   
    @memo Arbitrary waveform generator
    @author Written November 1999 by Christine Patton
    @version 1.0
 ************************************************************************/

/* Header File List */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <ctype.h>
#include <dlfcn.h>
#include <fstream>
#include <iostream>

#include <TROOT.h>
#include <TApplication.h>
#include <TVirtualX.h>

#include <TGListBox.h>
#include <TGClient.h>
#include <TGString.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGComboBox.h>
#include <TGFileDialog.h>
#include <TSystem.h>
#include <TEnv.h>
#include "BugReportDlg.h"

#ifndef _AWG_LIB
#define _AWG_LIB
#endif

#include "PConfig.h"
#include "TLGChannelBox.hh"
#include "TLGErrorDlg.hh"
#include "awgtype.h"
#include "awgapi.h"
#include "awgfunc.h"
#include "testpoint.h"
#include "FilterDesign.hh"
#include "iirutil.hh"
#include "gdserr.h" // JCB

   using namespace ligogui;
   using namespace std;

#define TRUE   1
#define FALSE 0
#define ONE_PI		PI

//#define ARBITRARY_WAVEFORM

// The definition of PI should be provided by math.h as M_PI
// although I note there is a difference in the last digits between
// OS X and GNU.  OS X uses ...50288, GNU uses ...5029
#define PI		3.1415926535897932384626433832795029
   /* radians per degree */
#define RAD_PER_DEG	(PI/180.0)

#define ADD_FUNCTION 1

/* Constants */

   enum EMenuCommandIdentifiers {
   M_FILE_WAVEFORM,  
   M_FILE_CONFIGURATION,
   M_FILE_SAVE,
   M_FILE_SAVEAS,
   M_FILE_EXIT,
   
   M_STATUS_CONFIGURATION,
   M_STATUS_STATISTICS,
   
   M_HELP_CONTENTS,
   M_HELP_SEARCH,
   M_HELP_NOTES,
   M_HELP_ABOUT,
   M_HELP_BUG,
   
   };

   enum EWidgetId {
   k_ID_CHANNEL = 101,
   k_ID_WAVEFORMTEXT,
   k_ID_WAVEFORMBTN,
   k_ID_LOW_F,
   k_ID_LOW_UNIT,
   k_ID_LOW_A,
   k_ID_OFFSET,
   k_ID_PHASE,
   k_ID_PHASE_UNIT,
   k_ID_HIGH_F,
   k_ID_HIGH_UNIT,
   k_ID_HIGH_A,
   k_ID_RATE,
   k_ID_RATE_UNIT,
   k_ID_RATIO,
   k_ID_WAVEFORM_SINE,
   k_ID_WAVEFORM_SQUARE,
   k_ID_WAVEFORM_RAMP,
   k_ID_WAVEFORM_TRIANGLE,
   k_ID_WAVEFORM_OFFSET,
   k_ID_WAVEFORM_UNIFORM,
   k_ID_WAVEFORM_NORMAL,
   k_ID_WAVEFORM_ARBITRARY,
   k_ID_WAVEFORM_SWEEP,
   k_ID_FILTER,
   k_ID_FOTON,
   k_ID_TYPE_LINEAR,
   k_ID_TYPE_LOG,
   k_ID_DIRECTION_UP,
   k_ID_DIRECTION_DOWN,
   k_ID_DIRECTION_UPDOWN,
   k_ID_TRIGGER_SINGLE,
   k_ID_TRIGGER_AUTO,
   k_ID_PHASE_INOUT,
   k_ID_GAIN,
   k_ID_RAMPTIME,
   k_ID_RAMPSET,
   k_ID_SETRUN,
   k_ID_STOP,
   k_ID_TRIGGER,
#ifdef ADD_FUNCTION
   k_ID_ADD,
#endif
   k_ID_EXIT
   };

   static const int my_debug = 0 ;

   static const char *filetypes[] = { 
   "Text files",    "*.txt",
   "All files",    "*",
   0,              0 };

#if 0
   static const char *gSaveAsTypes[] = { "PostScript",   "*.ps",
   "Encapsulated PostScript", "*.eps",
   "Text files",    "*.txt",
   "All files",    "*",
   0,              0 };
#endif

   const TString aboutmsg =
   "LIGO)))) Laser Interferometer Gravitational-wave Observatory\n"
   "Arbitrary Waveform Generator\n\n"
   "by Daniel Sigg et al., 1998 - 2019, copyright\n"
   "version " CDS_VERSION "\n\n"
   "http://www.ligo.caltech.edu\n"
   "http://gitlab.ligo.org/cds/dtt" ;

   const char *awggui_changes[] = {
   "Change history of the CDS realeases can also be found at",
   "https://git.ligo.org/cds/dtt/wikis/ChangeHistory",
   "",
   "Version CDS 0.4.0 March 2021",
   "- Runs using GDS 2.19.4 and ROOT 6.22/06",
   "",
   "Version CDS 0.3.0 August 2020",
   "- Project location changed to git.ligo.org/cds/dtt",
   "",
   "Version CDS 0.2.0 January 2020",
   "",
   "- A discontinuity in the ramp down on swept sine when the excitation is ",
   "aborted has been fixed.  Issue #59",
   "",
   "Version 2.17.9 (trunk), July 2016",
   "",
   "* Fix Bugzilla 628 to add ability to save and read back configurations of",
   "  awggui.  Also complete Status menu items to report the statistics and",
   "  configuration.",
   "",
   "Version 2.17.1, May 2015",
   "",
   "* Fix Bugzilla 851 to add ability to enter numbers smaller than 1e-6 by",
   "  changing the format of floating point numbers from %f to %e when ",
   "  commands are sent to the awg.  There are still limits to the values",
   "  the awg will accept, but awggui doesn't validate the values entered ",
   "  by the user.",
   "",
   "Version 2.16.17.1, January 2015",
   "",
   "* Embed change notes into code to eliminate the dependence on an",
   "  external change notes file",
   "",
   "Version 2.16.12, March 2014",
   "",
   "* Added the ability to add additional waveforms to an excitation.  This",
   "  option uses the 'add <slotnum> ...' command, which has the same syntax",
   "  as the 'set <slotnum> ...' command of the awg.",
   "",
   "Version 2.16.5, October 2012",
   "",
   "* Updated diag to use larger buffers so more than 128 tp and awg processes",
   "  could be used.  ",
   "",
   "* Added the ability to read release notes from the Help menu in diaggui.",
   "",
   "* Expand channel field width in fields and menus that display channel",
   "  names.",
   "",
   "* Add levels of hierarchy in display of channel names in selection menus.",
   "",
   "* Fixed bugzilla 325, segfault using --help option.",
   (char *) NULL
   } ;

   struct awg {
      char chnname[1024];
      int slotNum;
      double periodicFreq;
      double periodicAmp;
      double offset;
      double phase;
      double highFreq;
      double highAmp;
      double rateFreq;
      double ratio;
      int waveType;
      char sweepType[10];
      char sweepDir[10];
      char trigger[10];
      double phaseInOut;
      double gain;
      double ramptime;
   } awgCmd;

   struct configuration {
      string       add_set ;
      string       chnname ;
      string       fFreqBuf ;
      int          fFreqCombo ;
      string       fAmpBuf ;
      string       fOffsetBuf ;
      string       fPhaseBuf ;
      int          fRadiansCombo ;
      string fHighFreqBuf ;
      int          fHighCombo ;
      string       fHighAmpBuf ;
      string       fRateBuf ;
      int          fRateCombo ;
      string       fRatioBuf ;
      string       fPhaseInOutBuf ;
      string       fGainBuf ;
      string       fRampBuf ;
      string       fFilterSpec ;
      int          fWaveRadio[9] ;
      int          fSweepTypeRadio[2] ;
      int          fSweepDirectRadio[3] ;
   } ;


//______________________________________________________________________________
extern "C" {
   typedef void (*func_t) (void);
   typedef bool (*wizfunc_t) (const string& name, string& filter);
}

//______________________________________________________________________________
#ifdef P__WIN32
#define RTLD_LOCAL	0
   const char* const libname = "libfilterwiz.dll";
#else

#ifdef P__DARWIN
   const char* const libname = "libfilterwiz.dylib" ;
#else
   const char* const libname = "libfilterwiz.so";
#endif

#endif
   const char* const fn_wiz  = "wizard___dynamic";

   static bool libloaded = false;        // library loaded
   static void* handle = 0;              // library handle
   static func_t dispatch[10];           // dispatch table
   static const char* const funcname[] = // list of functions
   {fn_wiz, 0};

//______________________________________________________________________________
   static func_t getFunc (int num = 0)
   {
      // dynamically load plot library
      if (!libloaded) {
         // load librray
         handle = ::dlopen (libname, RTLD_NOW | RTLD_LOCAL);
         if (handle == 0) {
            cerr << "Unable to load library " << libname << endl;
            return 0;
         }
         // resolve dispatch table
         for (int i = 0; funcname[i]; ++i) {
            dispatch[i] = (func_t) dlsym (handle, funcname[i]);
            if (dispatch[i] == 0) {
               cerr << "load failed for " << funcname[i] << endl;
               return 0;
            }
         }
         libloaded = true;
      }
      return dispatch[num];
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: resolveFilter				*/
/*                                                         		*/
/* Procedure Description: resolve filter string 		 	*/
/*                        (must be done here since it is C++)		*/
/*                                                         		*/
/* Procedure Arguments: awg command					*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void resolveFilter (string& cmd)
   {
      // check for filter string
      if (strncasecmp (cmd.c_str(), "filter", 6) != 0) {
         return;
      }
      const char* p = cmd.c_str() + 6;
      // scan slot number
      int sl, n;
      if (sscanf (p, "%i%n", &sl, &n) != 1) {
         return;
      }
      p += n;
      while (*p && isspace (*p)) ++p;
      // check if this is a filter command string (rather than numbers)
      if (!isalpha (*p)) {
         return;
      }
      // get sampling rate from pagesize
      char buf[256];
      sprintf (buf, "show %i", sl);
      char* show = awgCommand (buf);
      char* s = strstr (show, "pagesize:");
      if (!s) {
         free (show);
         return;
      }
      int psz = atoi (s + 9);
      free (show);
      if (psz <= 0) {
         return;
      }
      // transform into 2nd order coefficient list
      FilterDesign ds (16.*(double)psz);
      if (!ds.filter (p) || !isiir (ds())) {
         return;
      }
      int nba = 4 * iirsoscount (ds()) + 1;
      double* ba = new double[nba];
      if (iir2z (ds(), nba, ba)) {
         sprintf (buf, "filter %i %22.14g", sl, ba[0]);
         cmd = buf;
         for (int i = 1; i < nba; ++i) {
            sprintf (buf, " %20.14g", ba[i]);
            cmd += buf;
         }
         //printf ("NEW FILTER CMD: %s\n", cmd.c_str());
      }
      delete [] ba;
   }


//______________________________________________________________________________
   class AwgMainFrame : public TGMainFrame {
   
   private:
      Bool_t             fDirty;
      Bool_t             fGainDirty;
      TGCompositeFrame	*fButtonFrame, *fTextFrame, *fWaveFrame, *fSweepFrame;
      TGCompositeFrame	*fPeriodicFrame, *fHighFrame, *fRampFrame;
      TGMenuBar		*fMenuBar;
      TGPopupMenu	*fMenuFile, *fMenuStatus, *fMenuHelp;
      TGLayoutHints	*fMenuBarLayout, *fMenuBarItemLayout, *fMenuBarHelpLayout;
      TGLayoutHints	*fRadioButtonLayout;
      TGGroupFrame	*fPeriodicGroup, *fLowGroup, *fHighGroup, *fRateGroup;
      TGGroupFrame	*fWaveGroup, *fFilterGroup, *fSweepGroup, *fTriggerGroup;
      TGGroupFrame	*fSweepTypeGroup, *fSweepDirectGroup, *fRatioGroup;
      TGGroupFrame	*fPhaseInOutGroup, *fGainRampGroup;
      TGTextBuffer	*fWaveBuffer, *fFreqBuf, *fHighFreqBuf, *fAmpBuf;
      TGTextBuffer	*fOffsetBuf, *fPhaseBuf, *fRatioBuf, *fHighAmpBuf, *fRateBuf;
      TGTextBuffer	*fPhaseInOutBuf, *fGainBuf, *fRampBuf;
      TGTextEntry	*fWaveText, *fFreqText, *fAmpText, *fOffsetText, *fRatioText;
      TGTextEntry	*fPhaseText, *fHighFreqText, *fRateFreqText, *fHighAmpText;
      TGTextEntry	*fPhaseInOutText, *fGainText, *fRampText;
      TGTextEntry	*fFilterSpec;
      TGButton		*fFilterWiz;
      TGLabel		*fWaveTitle, *fChanTitle, *fFreqTitle, *fLowVoltTitle;
      TGLabel		*fAmpTitle, *fOffsetTitle, *fPhaseTitle;
      TGLabel		*fPeriodicVoltTitle, *fHighVoltTitle, *fHighAmpTitle;
      TGLabel		*fRateFreqTitle, *fHighFreqTitle, *fRatioTitle;
      TGLabel		*fPhaseInOutTitle, *fPhaseInOutUnits, *fGainTitle;
      TGLabel		*fRampTitle, *fRampUnits;
      TGButton		*fGainButton, *fExitButton; 
      TGButton		*fSetButton, *fClearButton, *fLoadButton, *fTriggerButton;
#ifdef ADD_FUNCTION
      TGButton		*fAddButton ;
#endif
      TGRadioButton	*fWaveRadio[9], *fSweepTypeRadio[2], *fTriggerRadio[2];
      TGRadioButton	*fSweepDirectRadio[3];
      TGComboBox	*fFreqCombo, *fHighCombo, *fRateCombo, *fRadiansCombo;
      // TGComboBox        *fChanCombo;
      TLGChannelCombobox *fChanCombo;
   
      Bool_t SetPhaseInOut ();
      Bool_t SetGain (Bool_t init = kFALSE);
      Bool_t HandleText (Long_t parm1);
      Bool_t HandleButtons (Long_t parm1);
      Bool_t HandleRadioButtons (Long_t parm1);
      void   SaveConfiguration(const char *add_set);
      void   WriteConfiguration() ;
      void   ReadConfiguration() ;
      vector<struct configuration> configs ; 
      TString 		fCurFile ;
      TString 		fCurDir ;
#ifdef ARBITRARY_WAVEFORM
      TString 		fWaveformFilename ;
      std::fstream	fWaveformFile ;
#endif
      const char *FreqCombo2Str(int value) ;
      const char *AngleCombo2Str(int value) ;
      const char *WaveformButton2Str(int value) ;
      const char *SweepType2Str(int value) ;
      const char *SweepDir2Str(int value) ;
   
   public:
      // If a file name needed to be passed from the command line,
      // it would be passed to the AwgMainFrame constructor as the last 
      // argument.
      AwgMainFrame(const TGWindow *p, UInt_t w, UInt_t h, const char *filename = 0);
      virtual ~AwgMainFrame();
      Bool_t ReadChannel ();
      void SetDirty (Bool_t newstate);
   
      Bool_t IsDirty () {
         return fDirty; }
      void SetGainDirty (Bool_t newstate);
      Bool_t IsGainDirty () {
         return fGainDirty; }
   
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
   };

const char *
AwgMainFrame::FreqCombo2Str(int value)
{
   switch (value) {
      case 1 : return ("Hz") ;
      case 2 : return ("KHz") ;
      case 3 : return ("sec") ;
      case 4 : return ("msec") ;
      default: return ("--") ;
   }
}

const char *
AwgMainFrame::AngleCombo2Str(int value)
{
   switch (value) {
      case 1 : return ("Rad") ;
      case 2 : return ("Deg") ;
      default: return ("--") ;
   }
}
const char *
AwgMainFrame::WaveformButton2Str(int value) 
{
   switch (value) {
      case 0 : return ("Sine") ;
      case 1 : return ("Square") ;
      case 2 : return ("Ramp") ;
      case 3 : return ("Triangle") ;
      case 4 : return ("Offset") ;
      case 5 : return ("Uniform") ;
      case 6 : return ("Normal") ;
      case 7 : return ("Arbitrary") ;
      case 8 : return ("Sweep") ;
      default: return ("--") ;
   }
}
const char *
AwgMainFrame::SweepType2Str(int value) 
{
   switch (value) {
      case 0 : return ("Linear") ;
      case 1 : return ("Log") ;
      default: return ("--") ;
   }
}
const char *
AwgMainFrame::SweepDir2Str(int value) 
{
   switch (value) {
      case 0 : return ("Up") ;
      case 1 : return ("Down") ;
      case 3 : return ("Up/Down") ;
      default: return ("--") ;
   }
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  AwgMainFrame                                    */
/*                                                                           */
/* Procedure Description:  Construct AWG GUI window.                         */
/*                                                                           */
/* Procedure Arguments:  const TGWindow *p, UInt_t w, UInt_t h               */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/


   AwgMainFrame::AwgMainFrame(const TGWindow *p, UInt_t w, UInt_t h, const char *filename)
   : TGMainFrame(p, w, h), fDirty (kFALSE), fGainDirty (kFALSE)
   {
      char* buffer;
      int len;
      int i;
      int retval;
   
   /* Create AWG main frame. A TGMainFrame is a top level window. */
   
   /*------------------------------------------------------------------------*/
   /* Create menubar and popup menus. The hint objects are used to place     */
   /* and group the different menu widgets with respect to each other.       */
   /*------------------------------------------------------------------------*/
   
      fMenuBarLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           0, 0, 1, 1);
      fMenuBarItemLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
      fMenuBarHelpLayout = new TGLayoutHints(kLHintsTop | kLHintsRight);
   
      fMenuFile = new TGPopupMenu(fClient->GetRoot());
#ifdef ARBITRARY_WAVEFORM
      fMenuFile->AddEntry("Open &Waveform...", M_FILE_WAVEFORM);
#endif
      fMenuFile->AddEntry("Open &Configuration...", M_FILE_CONFIGURATION);
      fMenuFile->AddEntry("&Save Configuration", M_FILE_SAVE);
      fMenuFile->AddEntry("Save Config &As...", M_FILE_SAVEAS);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry("E&xit", M_FILE_EXIT);
   
      fMenuStatus = new TGPopupMenu(fClient->GetRoot());
      fMenuStatus->AddEntry("&Configuration", M_STATUS_CONFIGURATION);
      fMenuStatus->AddEntry("&Statistics", M_STATUS_STATISTICS); 
   
      fMenuHelp = new TGPopupMenu(fClient->GetRoot());
      // Add the standard help menu items to the menu.
      fMenuHelp->AddEntry("Report a &Bug", M_HELP_BUG);
      fMenuHelp->AddEntry("Release Notes", M_HELP_NOTES) ;
      //fMenuHelp->AddEntry("&Contents", M_HELP_CONTENTS);
      //fMenuHelp->AddEntry("&Search...", M_HELP_SEARCH);
      fMenuHelp->AddSeparator();
      fMenuHelp->AddEntry("&About", M_HELP_ABOUT);
   
   /* Menu button messages are handled by the main frame (i.e. "this")
      ProcessMessage() method. */
   
      fMenuFile->Associate(this);
      fMenuStatus->Associate(this);
      fMenuHelp->Associate(this);
   
   /* Put menu items onto menu bar */
   
      fMenuBar = new TGMenuBar(this, 1, 1, kHorizontalFrame | kRaisedFrame);
      fMenuBar->AddPopup("&File", fMenuFile, fMenuBarItemLayout);
      fMenuBar->AddPopup("&Status", fMenuStatus, fMenuBarItemLayout);
      fMenuBar->AddPopup("&Help", fMenuHelp, fMenuBarHelpLayout);
   
   /* Put menu bar onto window */
   
      AddFrame(fMenuBar, fMenuBarLayout);
   
   /*------------------------------------------------------------------------*/
   /* Set font for label text.                                               */
   /*------------------------------------------------------------------------*/
   
      FontStruct_t labelfont;
      labelfont = gClient->GetFontByName(gEnv->GetValue("Gui.NormalFont",
                                             "-adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1"));
   
   /* Define new graphics context. Only the fields specified in
      fMask will be used. */
   
      GCValues_t   gval;
      gval.fMask = kGCForeground | kGCFont;
      gval.fFont = gVirtualX->GetFontHandle(labelfont);
   
   /*------------------------------------------------------------------------*/
   /* Create frame to display selected Waveform and Channel names and group  */
   /* the text widgets with respect to each other and the main frame.        */
   /*------------------------------------------------------------------------*/
   
      fTextFrame = new TGCompositeFrame(this, 60, 20, kHorizontalFrame |
                           kSunkenFrame);
   
   /* Put Channel list widget in fTextFrame */
   
      fChanTitle = new TGLabel(fTextFrame, new TGString("Channel:"));
   
      /* JCB - Make a bit of extra space. */
      fTextFrame->AddFrame(fChanTitle, new TGLayoutHints(kLHintsLeft |
                                            kLHintsCenterY, 5, 4, 2, 2));
/*      fTextFrame->AddFrame(fChanTitle, new TGLayoutHints(kLHintsLeft |
                                            kLHintsCenterY, 10, 5, 2, 2)); */
      // JCB - Change kChannelTreeLevel3 to kChannelTreeLevel6 for channel hierarchy
      fChanCombo = new TLGChannelCombobox(fTextFrame, k_ID_CHANNEL, 0, kChannelTreeLevel3 | kChannelTreeLevel6, kTRUE);
      fTextFrame->AddFrame(fChanCombo, 
                          new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 0, 2, 5, 5)); 
      fChanCombo->SetPopupHeight (350);

      /* Get the channel names. First, find out how much space it's going to take for the list.  */
      len = awgGetChannelNames(NULL, 0, 0);
      /* Get the memory for the list. */
      buffer = (char*) malloc(len+10) ;
      if (buffer == 0) {
	 gdsDebug("AwgMainFrame constructor malloc(len+10) failed.") ; /* JCB */
         return;
      }
   
      /* Read the list of names into the buffer, attach the names to the combo box. */
      if (awgGetChannelNames(buffer, len+9, 0) > 0) 
      {
         fChanCombo->SetChannels (buffer);
         fChanCombo->BuildChannelTree ();
      }
      else
      {
         new TGMsgBox(fClient->GetRoot(), this, "Error", 
                     "Unable to obtain channel names!", 
                     kMBIconStop, kMBOk, &retval);
      }
   
      /* JCB - Make this bigger so we can see the names. */
/*       fChanCombo->Resize(200, 22); */
      fChanCombo->Resize(280, 22);
      fChanCombo->Associate(this);
   
   /* Put Waveform text entry widget in fTextFrame */
   
      fWaveBuffer = new TGTextBuffer(80);
      fWaveBuffer->AddText(0, "File Name");
      fWaveText = new TGTextEntry(fTextFrame, fWaveBuffer, k_ID_WAVEFORMTEXT);
      fWaveText->Resize(150, fWaveText->GetDefaultHeight());
      fWaveTitle = new TGLabel(fTextFrame, new TGString("Waveform Data File:"));
   
#if 0
      fLoadButton = new TGTextButton(fTextFrame, "Load Waveform Data", k_ID_WAVEFORMBTN);
      fLoadButton->Associate(this);
#endif
   
      /* JCB - make a bit more space. */
      fTextFrame->AddFrame(fWaveTitle, new TGLayoutHints(kLHintsLeft |
                                            kLHintsCenterY, 15, 5, 2, 2));
/*      fTextFrame->AddFrame(fWaveTitle, new TGLayoutHints(kLHintsLeft |
                                            kLHintsCenterY, 30, 5, 2, 2)); */
      fTextFrame->AddFrame(fWaveText, new TGLayoutHints(kLHintsLeft |
                                            kLHintsCenterY, 0, 2, 5, 5));
      /* JCB Make a bit more space. */
#if 0
      fTextFrame->AddFrame(fLoadButton, new TGLayoutHints(kLHintsLeft |
                                            kLHintsCenterY, 8, 4, 2, 2));
#endif
/*      fTextFrame->AddFrame(fLoadButton, new TGLayoutHints(kLHintsLeft |
                                            kLHintsCenterY, 20, 2, 2, 2)); */
   
   /* Put fTextFrame onto window */
   
      AddFrame(fTextFrame, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2));
   
      fWaveText->SetState(kFALSE);
   
   /*------------------------------------------------------------------------*/
   /* Create frame to display Periodic and Low/Sample group entries and      */
   /* group the text widgets with respect to each other and the main frame.  */
   /*------------------------------------------------------------------------*/
   
      fPeriodicFrame = new TGCompositeFrame(this, 60, 60, kHorizontalFrame |
                           kSunkenFrame);
   
      fPeriodicGroup = new TGGroupFrame(fPeriodicFrame, "Periodic",
                           kHorizontalFrame);
      fLowGroup = new TGGroupFrame(fPeriodicFrame, "Low/Sample", kHorizontalFrame);
   
   /* Create Low/Sample group widgets */
   
      fFreqText = new TGTextEntry(fLowGroup, 
                           fFreqBuf = new TGTextBuffer(100), k_ID_LOW_F);
      fFreqText->Resize(100, fFreqText->GetDefaultHeight());
      fFreqBuf->AddText(0, "0.0");
      fFreqText->Associate(this);
      fFreqTitle = new TGLabel(fLowGroup, new TGString("f"));
   
   /* Drop-down-list box to select Hz or kHz */
   
      fFreqCombo = new TGComboBox(fLowGroup, k_ID_LOW_UNIT);
      fFreqCombo->AddEntry("Hz", 1);
      fFreqCombo->AddEntry("KHz", 2);
      fFreqCombo->AddEntry("sec", 3);
      fFreqCombo->AddEntry("msec", 4);
      fFreqCombo->Resize(50, 22);
      fFreqCombo->Associate(this);
      fFreqCombo->Select(1);
   
      fAmpText = new TGTextEntry(fLowGroup,
                           fAmpBuf = new TGTextBuffer(100), k_ID_LOW_A);
      fAmpText->Resize(100, fAmpText->GetDefaultHeight());
      fAmpBuf->AddText(0, "0.0");
      fAmpText->Associate(this);
      fAmpTitle = new TGLabel(fLowGroup, new TGString("A"));
      fLowVoltTitle = new TGLabel(fLowGroup, new TGString(" "));
   
   /* Create Periodic group widgets */
   
      fOffsetText = new TGTextEntry(fPeriodicGroup, fOffsetBuf = new TGTextBuffer(100), 
                           k_ID_OFFSET);
      fOffsetText->Resize(100, fOffsetText->GetDefaultHeight());
      fOffsetBuf->AddText(0, "0.0");
      fOffsetText->Associate(this);
      fOffsetTitle = new TGLabel(fPeriodicGroup, new TGString("Offset"));
   
      fPhaseText = new TGTextEntry(fPeriodicGroup, fPhaseBuf = new TGTextBuffer(100), 
                           k_ID_PHASE);
      fPhaseText->Resize(100, fPhaseText->GetDefaultHeight());
      fPhaseBuf->AddText(0, "0.0");
      fPhaseText->Associate(this);
      fPhaseTitle = new TGLabel(fPeriodicGroup, new TGString("Phase"));
      fPeriodicVoltTitle = new TGLabel(fPeriodicGroup, new TGString(" "));
   
   /* Drop-down-list box to select Radians or Degrees */
   
      fRadiansCombo = new TGComboBox(fPeriodicGroup, k_ID_PHASE_UNIT);
      fRadiansCombo->AddEntry("Rad", 1);
      fRadiansCombo->AddEntry("Deg", 2);
      fRadiansCombo->Resize(50, 22);
      fRadiansCombo->Associate(this);
      fRadiansCombo->Select(1);
   
   
   /*  Add widgets to Low/Sample group frame */
   
      fLowGroup->AddFrame(fFreqTitle, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                           2, 0, 10, 0));
      fLowGroup->AddFrame(fFreqText, new TGLayoutHints(kLHintsTop | kLHintsLeft, 5,
                                           0, 8, 0));
      fLowGroup->AddFrame(fFreqCombo, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                           5, 0, 8, 0));
      fLowGroup->AddFrame(fAmpTitle, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                           10, 0, 10, 0));
      fLowGroup->AddFrame(fAmpText, new TGLayoutHints(kLHintsTop | kLHintsLeft, 5,
                                           0, 8, 0));
      fLowGroup->AddFrame(fLowVoltTitle, new TGLayoutHints(kLHintsTop |
                                           kLHintsLeft, 2, 0, 10, 0));
   
   /*  Add Low/Sample group frame to Periodic frame */
   
      fPeriodicGroup->AddFrame(fLowGroup, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 2, 0, 5, 0));
   
   /*  Add widgets to Periodic group frame */
   
      fPeriodicGroup->AddFrame(fOffsetTitle, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 10, 0, 30, 0));
      fPeriodicGroup->AddFrame(fOffsetText, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 5, 0, 25, 0));
      fPeriodicGroup->AddFrame(fPeriodicVoltTitle, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 2, 0, 30, 0));
      fPeriodicGroup->AddFrame(fPhaseTitle, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 10, 0, 30, 0));
      fPeriodicGroup->AddFrame(fPhaseText, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 5, 0, 25, 0));
      fPeriodicGroup->AddFrame(fRadiansCombo, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft, 5, 0, 25, 0));
   
      fPeriodicFrame->AddFrame(fPeriodicGroup, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft | kLHintsExpandX, 2, 2, 2, 5));
   
   /*  Add Periodic frame to Main window frame */
   
      AddFrame(fPeriodicFrame, new TGLayoutHints(kLHintsTop | kLHintsLeft |
                                kLHintsExpandX, 2, 2, 2, 2));
   
   
   /*------------------------------------------------------------------------*/
   /* Create frame to display High and Rate group entries and group          */
   /* the text widgets with respect to each other and the main frame.        */
   /*------------------------------------------------------------------------*/
   
      fHighFrame = new TGCompositeFrame(this, 60, 20, kHorizontalFrame |
                           kSunkenFrame);
   
      fHighGroup = new TGGroupFrame(fHighFrame, "High", kHorizontalFrame);
      fRateGroup = new TGGroupFrame(fHighFrame, "Rate", kHorizontalFrame);
      fRatioGroup = new TGGroupFrame(fHighFrame, "Ratio", kHorizontalFrame);
   
   /* Create High group widgets */
   
      fHighFreqText = new TGTextEntry(fHighGroup, 
                           fHighFreqBuf = new TGTextBuffer(100), k_ID_HIGH_F);
      fHighFreqText->Resize(100, fHighFreqText->GetDefaultHeight());
      fHighFreqBuf->AddText(0, "0.0");
      fHighFreqText->Associate(this);
      fHighFreqTitle = new TGLabel(fHighGroup, new TGString("f"));
   
   /* Drop-down-list box to select Hz or kHz */
   
      fHighCombo = new TGComboBox(fHighGroup, k_ID_HIGH_UNIT);
      fHighCombo->AddEntry("Hz", 1);
      fHighCombo->AddEntry("KHz", 2);
      fHighCombo->AddEntry("sec", 3);
      fHighCombo->AddEntry("msec", 4);
      fHighCombo->Resize(50, 22);
      fHighCombo->Associate(this);
      fHighCombo->Select(1);
   
      fHighAmpText = new TGTextEntry(fHighGroup, 
                           fHighAmpBuf = new TGTextBuffer(100), k_ID_HIGH_A);
      fHighAmpText->Resize(100, fHighAmpText->GetDefaultHeight());
      fHighAmpBuf->AddText(0, "0.0");
      fHighAmpText->Associate(this);
      fHighAmpTitle = new TGLabel(fHighGroup, new TGString("A"));
      fHighVoltTitle = new TGLabel(fHighGroup, new TGString("V"));
   
   
   /*  Add widgets to High group frame */
   
      fHighGroup->AddFrame(fHighFreqTitle, new TGLayoutHints(kLHintsTop |
                                            kLHintsLeft, 5, 0, 10, 0));
      fHighGroup->AddFrame(fHighFreqText, new TGLayoutHints(kLHintsTop |
                                            kLHintsLeft, 5, 0, 8, 0));
      fHighGroup->AddFrame(fHighCombo, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                            5, 0, 8, 0));
      fHighGroup->AddFrame(fHighAmpTitle, new TGLayoutHints(kLHintsTop |
                                            kLHintsLeft, 10, 0, 10, 0));
      fHighGroup->AddFrame(fHighAmpText, new TGLayoutHints(kLHintsTop |
                                            kLHintsLeft, 5, 0, 8, 0));
      fHighGroup->AddFrame(fHighVoltTitle, new TGLayoutHints(kLHintsTop |
                                            kLHintsLeft, 2, 0, 10, 0));
   
      fHighFrame->AddFrame(fHighGroup, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                            2, 2, 2, 5));
   
   /* Create Rate group widgets */
   
      fRateFreqText = new TGTextEntry(fRateGroup, fRateBuf = new TGTextBuffer(100), 
                           k_ID_RATE);
      fRateFreqText->Resize(100, fRateFreqText->GetDefaultHeight());
      fRateBuf->AddText(0, "0.0");
      fRateFreqText->Associate(this);
      fRateFreqTitle = new TGLabel(fRateGroup, new TGString("f"));
   
   /* Drop-down-list box to select Hz or kHz */
   
      fRateCombo = new TGComboBox(fRateGroup, k_ID_RATE_UNIT);
      fRateCombo->AddEntry("Hz", 1);
      fRateCombo->AddEntry("KHz", 2);
      fRateCombo->AddEntry("sec", 3);
      fRateCombo->AddEntry("msec", 4);
      fRateCombo->Resize(50, 22);
      fRateCombo->Associate(this);
      fRateCombo->Select(1);
   
   /*  Add widgets to Rate group frame */
   
      fRateGroup->AddFrame(fRateFreqTitle, new TGLayoutHints(kLHintsTop |
                                            kLHintsLeft, 5, 0, 10, 0));
      fRateGroup->AddFrame(fRateFreqText, new TGLayoutHints(kLHintsTop |
                                            kLHintsLeft, 5, 0, 8, 0));
      fRateGroup->AddFrame(fRateCombo, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                            5, 0, 8, 0));
   
   /*  Add Rate group frame to High frame */
   
      fHighFrame->AddFrame(fRateGroup, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                            10, 2, 2, 5));
   
   
   /* Create Ratio group widgets */
   
      fRatioText = new TGTextEntry(fRatioGroup, fRatioBuf = new TGTextBuffer(100), 
                           k_ID_RATIO);
      fRatioText->Resize(100, fRatioText->GetDefaultHeight());
      fRatioBuf->AddText(0, "50.0");
      fRatioText->Associate(this);
      fRatioTitle = new TGLabel(fRatioGroup, new TGString("%"));
   
   
   /*  Add widgets to Ratio group frame */
   
      fRatioGroup->AddFrame(fRatioTitle, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 5, 0, 10, 0));
      fRatioGroup->AddFrame(fRatioText, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 5, 0, 8, 0));
   
   /*  Add Ratio group frame to High frame */
   
      fHighFrame->AddFrame(fRatioGroup, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                            10, 2, 2, 5));
   
   /*  Add High frame to Main window frame */
   
      AddFrame(fHighFrame, new TGLayoutHints(kLHintsLeft | kLHintsExpandX, 2, 2, 2, 2));
   
      fHighFreqText->SetState(kFALSE);
      fHighAmpText->SetState(kFALSE);
      fRateFreqText->SetState(kFALSE);
      fRatioText->SetState(kFALSE);
   
   /*------------------------------------------------------------------------*/
   /* Create frame to display Waveform radio buttons and group               */
   /* the buttons with respect to each other and the main frame.             */
   /*------------------------------------------------------------------------*/
   
      fWaveFrame = new TGCompositeFrame(this, 60, 20, kVerticalFrame |
                           kSunkenFrame);
      fWaveGroup = new TGGroupFrame(fWaveFrame, "Waveform", kHorizontalFrame);
      fRadioButtonLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 0);
   
      fWaveRadio[0] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Sine"),	k_ID_WAVEFORM_SINE);
      fWaveRadio[1] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Square"),	k_ID_WAVEFORM_SQUARE);
      fWaveRadio[2] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Ramp"),	k_ID_WAVEFORM_RAMP);
      fWaveRadio[3] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Triangle"),k_ID_WAVEFORM_TRIANGLE);
      fWaveRadio[4] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Offset"),  k_ID_WAVEFORM_OFFSET);
      fWaveRadio[5] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Uniform"), k_ID_WAVEFORM_UNIFORM);
      fWaveRadio[6] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Normal"),  k_ID_WAVEFORM_NORMAL);
      fWaveRadio[7] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Arbitrary"),k_ID_WAVEFORM_ARBITRARY);
      fWaveRadio[8] = new TGRadioButton(fWaveGroup, 
                           new TGHotString("Sweep"),	k_ID_WAVEFORM_SWEEP);
   
      for (i = 0; i < 9; ++i) {
         fWaveGroup->AddFrame(fWaveRadio[i], fRadioButtonLayout);
         fWaveRadio[i]->Associate(this);
      }
   
      fWaveFrame->AddFrame(fWaveGroup, new TGLayoutHints(kLHintsTop | 
                                            kLHintsExpandX, 2, 2, 2, 5));
   
      fFilterGroup = new TGGroupFrame(fWaveFrame, "Filter", kHorizontalFrame);
      fFilterSpec = new TGTextEntry (fFilterGroup, "", k_ID_FILTER);
      fFilterSpec->Associate (this);
      fFilterSpec->SetMaxLength (4*1024);
      fFilterGroup->AddFrame (fFilterSpec, new TGLayoutHints(kLHintsTop | 
                                             kLHintsExpandX, 5, 5, 5, 0));
      fFilterWiz = new TGTextButton (fFilterGroup, "   Foton...   ", k_ID_FOTON);
      fFilterWiz->Associate (this);
      fFilterGroup->AddFrame (fFilterWiz, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft, 5, 5, 5, 0));
   
      fWaveFrame->AddFrame(fFilterGroup, new TGLayoutHints(kLHintsTop | 
                                            kLHintsExpandX, 2, 2, 2, 5));
      AddFrame(fWaveFrame, new TGLayoutHints(kLHintsLeft | 
                                kLHintsExpandX, 2, 2, 2, 2));
   
   /*------------------------------------------------------------------------*/
   /* Create frame to display Sweep radio buttons and group                  */
   /* the buttons with respect to each other and the main frame.             */
   /*------------------------------------------------------------------------*/
   
      fSweepFrame = new TGCompositeFrame(this, 60, 20, kHorizontalFrame | kSunkenFrame);
      fSweepGroup = new TGGroupFrame(fSweepFrame, "Sweep", kHorizontalFrame);
      fSweepTypeGroup = new TGGroupFrame(fSweepFrame, "Type", kHorizontalFrame);
      fSweepDirectGroup = new TGGroupFrame(fSweepFrame, "Direction", kHorizontalFrame);
   
      fSweepTypeRadio[0] = new TGRadioButton(fSweepTypeGroup, 
                           new TGHotString("Linear"),  k_ID_TYPE_LINEAR);
      fSweepTypeRadio[1] = new TGRadioButton(fSweepTypeGroup, 
                           new TGHotString("Log"),     k_ID_TYPE_LOG);
   
      for (i = 0; i < 2; ++i) {
         fSweepTypeGroup->AddFrame(fSweepTypeRadio[i], fRadioButtonLayout);
         fSweepTypeRadio[i]->Associate(this);
      }
   
      fSweepDirectRadio[0] = new TGRadioButton(fSweepDirectGroup, 
                           new TGHotString("Up"),      k_ID_DIRECTION_UP);
      fSweepDirectRadio[1] = new TGRadioButton(fSweepDirectGroup, 
                           new TGHotString("Down"),    k_ID_DIRECTION_DOWN);
      fSweepDirectRadio[2] = new TGRadioButton(fSweepDirectGroup, 
                           new TGHotString("Up/Down"), k_ID_DIRECTION_UPDOWN);
   
      for (i = 0; i < 3; ++i) {
         fSweepDirectGroup->AddFrame(fSweepDirectRadio[i], fRadioButtonLayout);
         fSweepDirectRadio[i]->Associate(this);
      }
   
      fSweepGroup->AddFrame(fSweepTypeGroup, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft, 2, 0, 5, 0));
      fSweepGroup->AddFrame(fSweepDirectGroup, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft, 10, 0, 5, 0));
      fSweepFrame->AddFrame(fSweepGroup, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft, 2, 2, 2, 5));
   
   /*------------------------------------------------------------------------*/
   /* Create frame to display Trigger radio buttons and group                */
   /* the buttons with respect to each other and the main frame.             */
   /*------------------------------------------------------------------------*/
   
      fTriggerGroup = new TGGroupFrame(fSweepFrame, "Trigger", kHorizontalFrame);
   
      fTriggerRadio[0] = new TGRadioButton(fTriggerGroup, 
                           new TGHotString("Single"),   k_ID_TRIGGER_SINGLE);
      fTriggerRadio[1] = new TGRadioButton(fTriggerGroup, 
                           new TGHotString("Auto"),     k_ID_TRIGGER_AUTO);
   
      for (i = 0; i < 2; ++i) {
         fTriggerGroup->AddFrame(fTriggerRadio[i], fRadioButtonLayout);
         fTriggerRadio[i]->Associate(this);
      }
   
      fSweepFrame->AddFrame(fTriggerGroup, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft, 10, 2, 19, 2));
   
   
      AddFrame(fSweepFrame, new TGLayoutHints(kLHintsLeft | 
                                kLHintsExpandX, 2, 2, 2, 2));
   
   /* Set initial state of radio buttons for each group. */
   
      fWaveRadio[0]->SetState(kButtonDown);
      awgCmd.waveType = k_ID_WAVEFORM_SINE;
      fSweepTypeRadio[0]->SetState(kButtonDown);
      strcpy(awgCmd.sweepType, "linear");
      fSweepDirectRadio[0]->SetState(kButtonDown);
      strcpy(awgCmd.sweepDir, "+");
      fTriggerRadio[1]->SetState(kButtonDown);
      strcpy(awgCmd.trigger, "c");
   
   /*------------------------------------------------------------------------*/
   /* Create frame to display phase in/out and overall gain and ramping and  */
   /* group the text widgets with respect to each other and the main frame.  */
   /*------------------------------------------------------------------------*/
   
      fRampFrame = new TGCompositeFrame(this, 60, 60, kHorizontalFrame |
                           kSunkenFrame);
   
   /* Create phase in/out group widgets */
   
      fPhaseInOutGroup = new TGGroupFrame(fRampFrame, "Phase In/Out",
                           kHorizontalFrame);
      fPhaseInOutText = new TGTextEntry(fPhaseInOutGroup, 
                           fPhaseInOutBuf = new TGTextBuffer(100), k_ID_PHASE_INOUT);
      fPhaseInOutText->Resize(100, fPhaseInOutText->GetDefaultHeight());
      fPhaseInOutBuf->AddText(0, "1.0");
      fPhaseInOutText->Associate(this);
      fPhaseInOutTitle = new TGLabel(fPhaseInOutGroup, new TGString("Dt"));
      fPhaseInOutUnits = new TGLabel(fPhaseInOutGroup, new TGString("s"));
   
      fPhaseInOutGroup->AddFrame(fPhaseInOutTitle, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft, 2, 0, 10, 0));
      fPhaseInOutGroup->AddFrame(fPhaseInOutText, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft, 5, 0, 8, 0));
      fPhaseInOutGroup->AddFrame(fPhaseInOutUnits, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 2, 0, 10, 0));
      fRampFrame->AddFrame(fPhaseInOutGroup, new TGLayoutHints(kLHintsTop |
                                            kLHintsLeft, 2, 0, 5, 0));
   
   /* Create overall gain and ramping group widgets */
   
      fGainRampGroup = new TGGroupFrame(fRampFrame, "Overall Gain", kHorizontalFrame);
   
   /* Create gain group widgets */
   
      fGainText = new TGTextEntry(fGainRampGroup,
                           fGainBuf = new TGTextBuffer(100), k_ID_GAIN);
      fGainText->Resize(100, fGainText->GetDefaultHeight());
      fGainBuf->AddText(0, "1.0");
      fGainText->Associate(this);
      fGainTitle = new TGLabel(fGainRampGroup, new TGString("Gain"));
   
      fRampText = new TGTextEntry(fGainRampGroup, 
                           fRampBuf = new TGTextBuffer(100), k_ID_RAMPTIME);
      fRampText->Resize(100, fRampText->GetDefaultHeight());
      fRampBuf->AddText(0, "1.0");
      fRampText->Associate(this);
      fRampTitle = new TGLabel(fGainRampGroup, new TGString("    Ramp time"));
      fRampUnits = new TGLabel(fGainRampGroup, new TGString("s"));
   
      fGainButton = new TGTextButton(fGainRampGroup, "       Set       ", k_ID_RAMPSET);
      fGainButton->Associate(this);
      fGainButton->SetState (kButtonDisabled);
   
      fGainRampGroup->AddFrame(fGainTitle, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 2, 0, 10, 0));
      fGainRampGroup->AddFrame(fGainText, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 5, 0, 8, 0));
      fGainRampGroup->AddFrame(fRampTitle, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 2, 0, 10, 0));
      fGainRampGroup->AddFrame(fRampText, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 2, 0, 8, 0));
      fGainRampGroup->AddFrame(fRampUnits, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft, 2, 0, 10, 0));
      fGainRampGroup->AddFrame(fGainButton, new TGLayoutHints(kLHintsTop |
                                             kLHintsRight, 2, 0, 8, 0));
      fRampFrame->AddFrame(fGainRampGroup, new TGLayoutHints(kLHintsTop |
                                            kLHintsExpandX | kLHintsLeft, 2, 0, 5, 0));
   
   /*  Add Ramp frame to Main window frame */
   
      AddFrame(fRampFrame, new TGLayoutHints(kLHintsTop | kLHintsLeft |
                                kLHintsExpandX, 2, 2, 2, 2));
   
   
   /*------------------------------------------------------------------------*/
   /* Create frame to display buttons at the bottom and                      */
   /* group the text widgets with respect to each other and the main frame.  */
   /*------------------------------------------------------------------------*/
   
      fButtonFrame = new TGCompositeFrame(this, 60, 20, kHorizontalFrame | kSunkenFrame);
   
      fSetButton = new TGTextButton(fButtonFrame, "Set/Run", k_ID_SETRUN);
      fSetButton->Associate(this);
#ifdef ADD_FUNCTION
      fAddButton = new TGTextButton(fButtonFrame, "Add", k_ID_ADD) ;
      fAddButton->Associate(this) ;
      fAddButton->SetState(kButtonDisabled) ; /* Disable until Set is pressed */
#endif
      fClearButton = new TGTextButton(fButtonFrame, "Stop", k_ID_STOP);
      fClearButton->Associate(this);
      fClearButton->SetState (kButtonDisabled);
      fTriggerButton = new TGTextButton(fButtonFrame, "Trigger", k_ID_TRIGGER);
      fTriggerButton->Associate(this);
      fExitButton = new TGTextButton(fButtonFrame, "Exit", k_ID_EXIT);
      fExitButton->Associate(this);
   
      fButtonFrame->AddFrame(fSetButton, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft | kLHintsExpandX, 15, 2, 2, 2));
#ifdef ADD_FUNCTION
      fButtonFrame->AddFrame(fAddButton, new TGLayoutHints(kLHintsTop |
                                             kLHintsLeft | kLHintsExpandX, 15, 2, 2, 2));
#endif
      fButtonFrame->AddFrame(fClearButton, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft | kLHintsExpandX, 15, 2, 2, 2));
      fButtonFrame->AddFrame(fTriggerButton, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft | kLHintsExpandX, 15, 2, 2, 2));
      fButtonFrame->AddFrame(fExitButton, new TGLayoutHints(kLHintsTop | 
                                             kLHintsLeft | kLHintsExpandX, 15, 2, 2, 2));
   
      AddFrame(fButtonFrame, new TGLayoutHints(kLHintsBottom | 
                                kLHintsExpandX, 0, 0, 1, 0));
   
   // Disable arbitrary waveform and trigger for now
      fTriggerButton->SetState (kButtonDisabled);
#ifndef ARBITRARY_WAVEFORM
#if 0
      fLoadButton->SetState (kButtonDisabled);
#endif
      fWaveRadio[7]->SetState (kButtonDisabled);
#endif
      for (i = 0; i < 2; ++i) {
         fTriggerRadio[i]->SetState (kButtonDisabled);
      }
   
      // saved configs starts as empty.
      configs.clear() ;

      // fCurFile and fCurDir hold the current file name and directory for the 
      // configuration file.
      fCurFile.Clear() ;
      fCurDir.Clear() ;

      // If the user provided a filename on the command line, read it.
      if (filename != 0) {
	 fCurDir = gSystem->DirName(filename) ;
	 fCurFile = gSystem->BaseName(filename) ;
	 ReadConfiguration() ;
      }

      SetWindowName("Arbitrary Waveform Generator");
   
      MapSubwindows();
   
   // we need to use GetDefault...() to initialize the layout algorithm...
      Resize(GetDefaultSize());
   
      MapWindow();
   }



/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  ~AwgMainFrame                                   */
/*                                                                           */
/* Procedure Description:  Delete AWG GUI window.                            */
/*                                                                           */
/* Procedure Arguments:  const TGWindow *p, UInt_t w, UInt_t h               */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/

   AwgMainFrame::~AwgMainFrame()
   {
   /* Delete all created widgets */
      int i;
   
#ifdef ADD_FUNCTION
      delete fAddButton ;
#endif
      delete fSetButton;
      delete fClearButton;
#if 0
      delete fLoadButton;
#endif
      delete fTriggerButton;
      delete fExitButton;
      delete fFreqCombo; 
      delete fHighCombo; 
      delete fRateCombo; 
      delete fChanCombo;
      delete fWaveText; 
      delete fFreqText; 
      delete fAmpText; 
      delete fOffsetText; 
      delete fRatioText;
      delete fPhaseText; 
      delete fHighFreqText; 
      delete fRateFreqText; 
      delete fHighAmpText;
      delete fWaveTitle; 
      delete fChanTitle; 
      delete fFreqTitle; 
      delete fLowVoltTitle;
      delete fAmpTitle; 
      delete fOffsetTitle; 
      delete fPhaseTitle; 
      delete fRatioTitle;
      delete fPeriodicVoltTitle; 
      delete fHighVoltTitle; 
      delete fHighAmpTitle;
      delete fRateFreqTitle; 
      delete fHighFreqTitle;
      for (i = 0; i < 9; ++i) {
         delete fWaveRadio[i];
      }
      delete fFilterSpec;
      delete fFilterWiz;
      for (i = 0; i < 2; ++i) {
         delete fSweepTypeRadio[i];
      }
      for (i = 0; i < 3; ++i) {
         delete fSweepDirectRadio[i];
      }
      for (i = 0; i < 2; ++i) {
         delete fTriggerRadio[i];
      }
      delete fPeriodicGroup; 
      delete fLowGroup; 
      delete fHighGroup; 
      delete fRateGroup;
      delete fWaveGroup;
      delete fFilterGroup;
      delete fSweepGroup; 
      delete fTriggerGroup; 
      delete fRatioGroup;
      delete fPeriodicFrame; 
      delete fHighFrame; 
      delete fButtonFrame; 
      delete fTextFrame; 
      delete fWaveFrame; 
      delete fSweepFrame;
      delete fRampFrame; 
      delete fPhaseInOutGroup; 
      delete fPhaseInOutText; 
      delete fPhaseInOutTitle; 
      delete fPhaseInOutUnits; 
      delete fGainRampGroup; 
      delete fGainText; 
      delete fGainTitle; 
      delete fRampText; 
      delete fRampTitle; 
      delete fRampUnits; 
      delete fGainButton;
   
      delete fMenuBarLayout;
      delete fMenuBarItemLayout;
      delete fMenuBarHelpLayout;
      delete fRadioButtonLayout; 
      delete fMenuFile;
      delete fMenuStatus;
      delete fMenuHelp;
   }


/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  ReadChannel                                     */
/*                                                                           */
/* Procedure Description:  Read the channel name                             */
/*                                                                           */
/* Procedure Arguments:  -					             */
/*                                                                           */
/* Procedure Returns: true if changed                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
   Bool_t AwgMainFrame::ReadChannel()
   {
      int retval;
      int status;
      string SelectedChan;
   
      /* Get the channel name from the Channel: combobox. */
      SelectedChan = fChanCombo->GetChannelNameStripped();

   
      /* channel selected? */
      if (SelectedChan.empty()) {
         new TGMsgBox(fClient->GetRoot(), this, 
                     "Error", "No Channel Selected!", 
                     kMBIconStop, kMBOk, &retval);
         awgCmd.slotNum = 0;
         strcpy (awgCmd.chnname, "");
         return kFALSE;
      }

      /* If this is the same channel as previously selected, return.
       * Otherwise, remove the excitation from the old channel and
       * free the awg slot.
       */
      /* same as before? */
      if (SelectedChan == awgCmd.chnname)  {
         return kTRUE;
      }
   
      /* first clear old stuff */
      if(awgCmd.slotNum > 0){
         tpClearName (awgCmd.chnname);
         status = awgRemoveChannel(awgCmd.slotNum);
         if(status < 0) {
            new TGMsgBox(fClient->GetRoot(), this, 
                        "Error", "Could Not Remove Channel!", 
                        kMBIconStop, kMBOk, &retval);
         }
      }
      /* This is a new channel, so get a slot number for the channel. */
      /* set new channel */
      awgCmd.slotNum = awgSetChannel(SelectedChan.c_str());
      if (awgCmd.slotNum < 0) {
         new TGMsgBox(fClient->GetRoot(), this, 
                     "Error", "Could Not Set Channel!", 
                     kMBIconStop, kMBOk, &retval);
         awgCmd.slotNum = 0;
         strcpy (awgCmd.chnname, "");
         return kFALSE;
      }
      strncpy (awgCmd.chnname, SelectedChan.c_str(), sizeof (awgCmd.chnname));
      awgCmd.chnname[sizeof(awgCmd.chnname)-1] = 0;
      if (my_debug) cout << "select new channel " << awgCmd.chnname << endl;
   
      /* This finds the channel by name, querying each awgtpman until it finds it.
       * Not very efficient, we should know which dcuid has the channel at this point.
       */
      if (tpRequestName (SelectedChan.c_str(), -1, 0, 0) < 0) {
         new TGMsgBox(fClient->GetRoot(), this, 
                     "Error", "Could Not Set Testpoint!", 
                     kMBIconStop, kMBOk, &retval);
         return kFALSE;
      }
   
      return kTRUE;
   }


/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  SetDirty                                        */
/*                                                                           */
/* Procedure Description:  Sets the dirty flag and activates/deactivates     */
/*                         the set button                                    */
/*                                                                           */
/* Procedure Arguments:  new state                                           */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
   void AwgMainFrame::SetDirty (Bool_t newstate)
   {
      if (fDirty == newstate) {
         return;
      }
      if (newstate) {
         fSetButton->SetState (kButtonUp);
      }
      else {
         fSetButton->SetState (kButtonDisabled);
      }
      fDirty = newstate;
   }


/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  SetGainDirty      			             */
/*                                                                           */
/* Procedure Description:  Sets the dirty flag and activates/deactivates     */
/*                         the set button                                    */
/*                                                                           */
/* Procedure Arguments:  new state			                     */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
   void AwgMainFrame::SetGainDirty (Bool_t newstate)
   {
      if (fGainDirty == newstate) {
         return;
      }
      if (newstate) {
         fGainButton->SetState (kButtonUp);
      }
      else {
         fGainButton->SetState (kButtonDisabled);
      }
      fGainDirty = newstate;
   }


/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  SetPhaseInOut                                   */
/*                                                                           */
/* Procedure Description:  Set ramp.              		             */
/*                                                                           */
/* Procedure Arguments:  				                     */
/*                                                                           */
/* Procedure Returns: true if successful; false on error                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
   Bool_t AwgMainFrame::SetPhaseInOut()
   {
      int retval;
      char cmdString[1024];
   
      HandleText(k_ID_PHASE_INOUT);
   
      sprintf(cmdString, "ramp %e", awgCmd.phaseInOut);
      if (my_debug) cout << cmdString << endl;
      if(awgcmdline (cmdString) < 0) {
         new TGMsgBox(fClient->GetRoot(), this, 
                     "Error", "Invalid phase in/out!", 
                     kMBIconStop, kMBOk, &retval);
         return kFALSE;
      }
   
      return kTRUE;
   }


/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  SetGain	                                     */
/*                                                                           */
/* Procedure Description:  Set overall gain.          		             */
/*                                                                           */
/* Procedure Arguments:  				                     */
/*                                                                           */
/* Procedure Returns: true if successful; false on error                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
   Bool_t AwgMainFrame::SetGain (Bool_t init)
   {
      int retval;
      char cmdString[1024];
   
      if (!init) {
         if (!IsGainDirty()) {
            return kTRUE;
         }
         if (fClearButton->GetState() == kButtonDisabled) {
            return kFALSE;
         }
      }
   
      HandleText(k_ID_GAIN);
      if (init) {
         awgCmd.ramptime = 0;
      }
      else {
         HandleText(k_ID_RAMPTIME);
      }
   
      sprintf(cmdString, "gain %d %e %e", 
             awgCmd.slotNum, awgCmd.gain, awgCmd.ramptime);
      if (my_debug) cout << cmdString << endl;
      if(awgcmdline (cmdString) < 0) {
         new TGMsgBox(fClient->GetRoot(), this, 
                     "Error", "Invalid gain ramp!", 
                     kMBIconStop, kMBOk, &retval);
         return kFALSE;
      }
      SetGainDirty (kFALSE);
   
      return kTRUE;
   }


/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  CloseWindow                                     */
/*                                                                           */
/* Procedure Description:  Construct AWG GUI window.                         */
/*                                                                           */
/* Procedure Arguments:  const TGWindow *p, UInt_t w, UInt_t h               */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/

   void AwgMainFrame::CloseWindow()
   {
      /* make sure awg is off */
      if(awgCmd.slotNum > 0){
         tpClearName (awgCmd.chnname);
         awgRemoveChannel(awgCmd.slotNum);
      }
   
   /* Got close message for this MainFrame. Calls parent CloseWindow()
    (which destroys the window) and terminate the application.
    The close message is generated by the window manager when its close
    window menu item is selected. */
   
      TGMainFrame::CloseWindow();
      gApplication->Terminate(0);
   }



/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  HandleText                                      */
/*                                                                           */
/* Procedure Description:  Construct AWG GUI window.                         */
/*                                                                           */
/* Procedure Arguments:  const TGWindow *p, UInt_t w, UInt_t h               */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
   Bool_t AwgMainFrame::HandleText(Long_t parm1)
   {
   
      char* EndofStr;
      int retval;
   
      switch (parm1) {
         case k_ID_LOW_F:  /* Periodic Frequency */
            awgCmd.periodicFreq = strtod(fFreqBuf->GetString(), &EndofStr);
            // handle units first
            switch (fFreqCombo->GetSelected()) {
               case 2:
                  awgCmd.periodicFreq *= 1000.0;
                  break;
               case 3:
                  if (awgCmd.periodicFreq > 1E-12) {
                     awgCmd.periodicFreq = 1.0 / awgCmd.periodicFreq;
                  }
                  break;
               case 4:
                  if (awgCmd.periodicFreq > 1E-9) {
                     awgCmd.periodicFreq = 1E3 / awgCmd.periodicFreq;
                  }
                  break;
               case 1:
               default:
                  break;
            }
         
            if(EndofStr != (fFreqBuf->GetString() + strlen(fFreqBuf->GetString()))) {
               fFreqBuf->Clear();
               fFreqBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fFreqText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Frequency!", 
                           "Frequencies must be numeric -- Resetting to default (0.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
         
            if(awgCmd.periodicFreq < 0.0) {
               fFreqBuf->Clear();
               fFreqBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fFreqText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Frequency!", 
                           "Frequencies must be non-negative -- Resetting to default (0.0)",
                           kMBIconStop, kMBOk, &retval);
            
            }
            break;
         case k_ID_LOW_A:  /* Periodic Amplitude */
            awgCmd.periodicAmp = strtod(fAmpBuf->GetString(), &EndofStr);
            if(EndofStr != (fAmpBuf->GetString() + strlen(fAmpBuf->GetString()))) {
               fAmpBuf->Clear();
               fAmpBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fAmpText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Periodic Amplitude!", 
                           "Amp Voltage must be numeric -- Resetting to default (0.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
         
            break;
         case k_ID_OFFSET:  /* Offset */
            awgCmd.offset = strtod(fOffsetBuf->GetString(), &EndofStr);
         
            if(EndofStr != (fOffsetBuf->GetString() + strlen(fOffsetBuf->GetString()))) {
               fOffsetBuf->Clear();
               fOffsetBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fOffsetText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Offset!", 
                           "Offset Voltage must be numeric -- Resetting to default (0.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
         
            break;
         case k_ID_PHASE:  /* Phase */
            awgCmd.phase = strtod(fPhaseBuf->GetString(), &EndofStr);
            // handle units first
            if (fRadiansCombo->GetSelected() == 2) {
               awgCmd.phase *= RAD_PER_DEG;
            }
         
            if(EndofStr != (fPhaseBuf->GetString() + strlen(fPhaseBuf->GetString()))) {
               fPhaseBuf->Clear();
               fPhaseBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fPhaseText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Phase!", 
                           "Phase must be numeric -- Resetting to default (0.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
         
            break;
         case k_ID_HIGH_F:  /* High Frequency */
            awgCmd.highFreq = strtod(fHighFreqBuf->GetString(), &EndofStr);
            // handle units first
            switch (fHighCombo->GetSelected()) {
               case 2:
                  awgCmd.highFreq *= 1000.0;
                  break;
               case 3:
                  if (awgCmd.highFreq > 1E-12) {
                     awgCmd.highFreq = 1.0 / awgCmd.highFreq;
                  }
                  break;
               case 4:
                  if (awgCmd.highFreq > 1E-9) {
                     awgCmd.highFreq = 1E3 / awgCmd.highFreq;
                  }
                  break;
               case 1:
               default:
                  break;
            }
         
            if(EndofStr != (fHighFreqBuf->GetString() + strlen(fHighFreqBuf->GetString()))) {
               fHighFreqBuf->Clear();
               fHighFreqBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fHighFreqText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Frequency!", 
                           "Frequencies must be numeric -- Resetting to default (0.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
         
            if(awgCmd.highFreq < 0.0) {
               fHighFreqBuf->Clear();
               fHighFreqBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fHighFreqText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Frequency!", 
                           "Frequencies must be non-negative -- Resetting to default (0.0)",
                           kMBIconStop, kMBOk, &retval);
            
            }
            break;
         case k_ID_HIGH_A:  /* High Amplitude */
            awgCmd.highAmp = strtod(fHighAmpBuf->GetString(), &EndofStr);
         
            if(EndofStr != (fHighAmpBuf->GetString() + strlen(fHighAmpBuf->GetString()))) {
               fHighAmpBuf->Clear();
               fHighAmpBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fHighAmpText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in High Amplitude!", 
                           "Amp Voltage must be numeric -- Resetting to default (0.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
         
            break;
         case k_ID_RATE:  /* Rate Frequency */
            awgCmd.rateFreq = strtod(fRateBuf->GetString(), &EndofStr);
            // handle units first
            switch (fRateCombo->GetSelected()) {
               case 2:
                  awgCmd.rateFreq *= 1000.0;
                  break;
               case 3:
                  if (awgCmd.rateFreq > 1E-12) {
                     awgCmd.rateFreq = 1.0 / awgCmd.rateFreq;
                  }
                  break;
               case 4:
                  if (awgCmd.rateFreq > 1E-9) {
                     awgCmd.rateFreq = 1E3 / awgCmd.rateFreq;
                  }
                  break;
               case 1:
               default:
                  break;
            }
         
            if(EndofStr != (fRateBuf->GetString() + strlen(fRateBuf->GetString()))) {
               fRateBuf->Clear();
               fRateBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fRateFreqText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Frequency!", 
                           "Frequencies must be numeric -- Resetting to default (0.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
         
            if(awgCmd.rateFreq < 0.0) {
               fRateBuf->Clear();
               fRateBuf->AddText(0, "0.0");
               fClient->NeedRedraw(fRateFreqText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Frequency!", 
                           "Frequencies must be non-negative -- Resetting to default (0.0)",
                           kMBIconStop, kMBOk, &retval);
            
            }
            break;
         case k_ID_RATIO:  /* Ratio */
            awgCmd.ratio = strtod(fRatioBuf->GetString(), &EndofStr);
         
            if(EndofStr != (fRatioBuf->GetString() + strlen(fRatioBuf->GetString()))) {
               fRatioBuf->Clear();
               fRatioBuf->AddText(0, "50");
               fClient->NeedRedraw(fRatioText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Ratio!", 
                           "Ratio must be numeric -- Resetting to default (50)", 
                           kMBIconStop, kMBOk, &retval);
            }
         
            if(awgCmd.ratio < 0.0) {
               fRatioBuf->Clear();
               fRatioBuf->AddText(0, "50");
               fClient->NeedRedraw(fRatioText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Ratio!", 
                           "Ratio must be non-negative -- Resetting to default (50)",
                           kMBIconStop, kMBOk, &retval);
            
            }
            break;
         case k_ID_PHASE_INOUT:  /* phase in/out */
            awgCmd.phaseInOut = strtod (fPhaseInOutBuf->GetString(), &EndofStr);
         
            if(EndofStr != (fPhaseInOutBuf->GetString() + 
                           strlen(fPhaseInOutBuf->GetString())) ||
              (awgCmd.phaseInOut < 0.0)) {
               fPhaseInOutBuf->Clear();
               fPhaseInOutBuf->AddText(0, "1.0");
               awgCmd.phaseInOut = 1.0;
               fClient->NeedRedraw(fPhaseInOutText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Phase In/Out!", 
                           "Time must be non-negative -- Resetting to default (1.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
            break;
         case k_ID_GAIN:  /* overall gain */
            awgCmd.gain = strtod (fGainBuf->GetString(), &EndofStr);
         
            if(EndofStr != (fGainBuf->GetString() + 
                           strlen(fGainBuf->GetString()))) {
               fGainBuf->Clear();
               fGainBuf->AddText(0, "1.0");
               awgCmd.gain = 1.0;
               fClient->NeedRedraw(fGainText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Gain!", 
                           "Gain must be numeric -- Resetting to default (1.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
            break;
         case k_ID_RAMPTIME:  /* ramp time */
            awgCmd.ramptime = strtod (fRampBuf->GetString(), &EndofStr);
         
            if(EndofStr != (fRampBuf->GetString() + 
                           strlen(fRampBuf->GetString())) ||
              awgCmd.ramptime < 0) {
               fRampBuf->Clear();
               fRampBuf->AddText(0, "1.0");
               awgCmd.ramptime = 1.0;
               fClient->NeedRedraw(fRampText);
               new TGMsgBox(fClient->GetRoot(), this, "Error in Ramp time!", 
                           "Time must be non-negative -- Resetting to default (1.0)", 
                           kMBIconStop, kMBOk, &retval);
            }
            break;
      }
      return kTRUE; 
   }


/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  HandleButton                                    */
/*                                                                           */
/* Procedure Description:  Handle button msg	   .                         */
/*                                                                           */
/* Procedure Arguments:  parm1					             */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
   Bool_t AwgMainFrame::HandleButtons (Long_t parm1)
   {
      int retval;
      char cmdString[1024];
      const char *set_add_cmd = "set" ;
   
      switch(parm1) {
         // set/run
#ifdef ADD_FUNCTION
	 case k_ID_ADD:
	    set_add_cmd = "add" ;
	    /* Fall through, add command has the same syntax as set command. */
#endif
         case k_ID_SETRUN: 
            {
               if (!ReadChannel()) {
                  break;
               }
	       if (parm1 == k_ID_SETRUN) {
		  // Clear the saved configs since a set command starts a 
		  // new excitation.
		  configs.clear() ;
	       }
               Bool_t error = kFALSE;
               // filter first
               string filterspec = fFilterSpec->GetText();
               while (!filterspec.empty() && isspace (filterspec[0])) {
                  filterspec.erase (0, 1);
               }
               if (!filterspec.empty()) {
                  sprintf(cmdString, "filter %d %s", awgCmd.slotNum, 
                         filterspec.c_str());
                  if (my_debug) cout << "cmdString = " << cmdString << endl;
                  filterspec = cmdString;
                  resolveFilter (filterspec);
                  if (my_debug) cout << "cmdString = " << filterspec.c_str() << endl;
                  if(awgcmdline (filterspec.c_str()) < 0) {
                     error = kTRUE;
                     new TGMsgBox(fClient->GetRoot(), this, 
                                 "Error", "Invalid Filter Spec!", 
                                 kMBIconStop, kMBOk, &retval);
                  }
               }
            
               // gain & phase in/out next;
               if (!SetGain (kTRUE) || !SetPhaseInOut()) {
                  error = kTRUE;
                  new TGMsgBox(fClient->GetRoot(), this, 
                              "Error", "Invalid Phase In/Out or Gain Spec!", 
                              kMBIconStop, kMBOk, &retval);
               }
            
               // set waveform
               switch(awgCmd.waveType) {
                  case k_ID_WAVEFORM_SINE: /* Sine */
                     HandleText(k_ID_LOW_F);
                     HandleText(k_ID_LOW_A);
                     HandleText(k_ID_OFFSET);
                     HandleText(k_ID_PHASE);
                     sprintf(cmdString, "%s %d sine %e %e %e %e", 
			    set_add_cmd,
                            awgCmd.slotNum, awgCmd.periodicFreq, 
                            awgCmd.periodicAmp, awgCmd.offset, awgCmd.phase);
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
                  case k_ID_WAVEFORM_SQUARE: /* Square */
                     HandleText(k_ID_LOW_F);
                     HandleText(k_ID_LOW_A);
                     HandleText(k_ID_OFFSET);
                     HandleText(k_ID_PHASE);
                     HandleText(k_ID_RATIO);
                     if (fabs (awgCmd.ratio - 50) < 1E-6) {
                        sprintf(cmdString, "%s %d square %e %e %e %e", 
			       set_add_cmd,
                               awgCmd.slotNum, awgCmd.periodicFreq, 
                               awgCmd.periodicAmp, awgCmd.offset, 
                               awgCmd.phase);
                     }
                     else {
                        sprintf(cmdString, "%s %d square %e %e %e %e %e", 
			       set_add_cmd,
                               awgCmd.slotNum, awgCmd.periodicFreq, 
                               awgCmd.periodicAmp, awgCmd.offset, 
                               awgCmd.phase, awgCmd.ratio/ 100.0);
                     }
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
                  case k_ID_WAVEFORM_RAMP: /* Ramp */
                     HandleText(k_ID_LOW_F);
                     HandleText(k_ID_LOW_A);
                     HandleText(k_ID_OFFSET);
                     HandleText(k_ID_PHASE);
                     sprintf(cmdString, "%s %d ramp %e %e %e %e", 
			    set_add_cmd,
                            awgCmd.slotNum, awgCmd.periodicFreq, 
                            awgCmd.periodicAmp, awgCmd.offset, 
                            awgCmd.phase);
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
                  case k_ID_WAVEFORM_TRIANGLE: /* Triangle */
                     HandleText(k_ID_LOW_F);
                     HandleText(k_ID_LOW_A);
                     HandleText(k_ID_OFFSET);
                     HandleText(k_ID_PHASE);
                     sprintf(cmdString, "%s %d triangle %e %e %e %e", 
			    set_add_cmd,
                            awgCmd.slotNum, awgCmd.periodicFreq, 
                            awgCmd.periodicAmp, awgCmd.offset, awgCmd.phase);
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
                  case k_ID_WAVEFORM_OFFSET: /* Offset */
                     HandleText(k_ID_LOW_A);
                     sprintf(cmdString, "%s %d const %e", 
			    set_add_cmd,
                            awgCmd.slotNum, awgCmd.periodicAmp);
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
                  case k_ID_WAVEFORM_UNIFORM: /* Uniform */
                     HandleText(k_ID_LOW_F);
                     HandleText(k_ID_LOW_A);
                     HandleText(k_ID_OFFSET);
                     HandleText(k_ID_HIGH_F);
                     sprintf(cmdString, "%s %d uniform %e %e %e %e", 
			    set_add_cmd,
                            awgCmd.slotNum, awgCmd.periodicFreq, 
                            awgCmd.highFreq, awgCmd.periodicAmp, 
                            awgCmd.offset);
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
                  case k_ID_WAVEFORM_NORMAL: /* Normal */
                     HandleText(k_ID_LOW_F);
                     HandleText(k_ID_LOW_A);
                     HandleText(k_ID_OFFSET);
                     HandleText(k_ID_HIGH_F);
                     sprintf(cmdString, "%s %d normal %e %e %e %e", 
			    set_add_cmd,
                            awgCmd.slotNum, awgCmd.periodicFreq, 
                            awgCmd.highFreq, awgCmd.periodicAmp, 
                            awgCmd.offset);
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
                  case k_ID_WAVEFORM_ARBITRARY: /* Arbitrary */
		  {
		     // Check to see if the waveform file has been opened.
#ifdef ARBITRARY_WAVEFORM
		     if (!fWaveformFile.is_open())
		     {
			// Toss up an error message.
			error = kTRUE ;
			new TGMsgBox(fClient->GetRoot(), this,
				    "Error". "Waveform file not open",
				     kMBIconStop, kMBOk, &retval);
			break ;
		     }
#endif
                     HandleText(k_ID_LOW_F);
                     HandleText(k_ID_RATE);
                     sprintf(cmdString, "%s %d arbitrary %e %e %s %s %s", 
			    set_add_cmd,
                            awgCmd.slotNum, awgCmd.periodicFreq, 
                            awgCmd.rateFreq, awgCmd.sweepType, 
                            awgCmd.sweepDir, awgCmd.trigger);
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
		  }
                  case k_ID_WAVEFORM_SWEEP: /* Sweep */
                     HandleText(k_ID_LOW_F);
                     HandleText(k_ID_LOW_A);
                     HandleText(k_ID_HIGH_F);
                     HandleText(k_ID_HIGH_A);
                     HandleText(k_ID_RATE);
                     sprintf(cmdString, "%s %d sweep %e %e %e %e %e %s %s", 
			    set_add_cmd,
                            awgCmd.slotNum, awgCmd.periodicFreq, 
                            awgCmd.highFreq, awgCmd.periodicAmp, 
                            awgCmd.highAmp, 1./awgCmd.rateFreq, 
                            awgCmd.sweepType, awgCmd.sweepDir);
                     if (my_debug) cout << cmdString << endl;
                     if(awgcmdline (cmdString) < 0) {
                        error = kTRUE;
                        new TGMsgBox(fClient->GetRoot(), this, 
                                    "Error", "Invalid Command String!", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     break;
               }
	       // Send set_add_cmd so we know if it's a set or add
	       // command, and the filterspec that's had it's whitespace
	       // cleaned up.  The rest of the settings are in awgCmd.
	       SaveConfiguration(set_add_cmd) ;

               if (!error) {
                  fClearButton->SetState (kButtonUp);
#ifdef ADD_FUNCTION
		  fAddButton->SetState(kButtonUp) ;
#endif
                  SetDirty (kFALSE);
               }
               break;
            }
         // stop
         case k_ID_STOP:
            {
               if (!SetPhaseInOut()) {
                  new TGMsgBox(fClient->GetRoot(), this, 
                              "Error", "Invalid Phase In/Out Spec!", 
                              kMBIconStop, kMBOk, &retval);
               }
               sprintf(cmdString, "stop %d", awgCmd.slotNum);
               if (my_debug) cout << cmdString << endl;
               if(awgcmdline (cmdString) < 0) {
                  new TGMsgBox(fClient->GetRoot(), this, 
                              "Error", "Waveform Not Cleared!", 
                              kMBIconStop, kMBOk, &retval);
               }
               fClearButton->SetState (kButtonDisabled);
#ifdef ADD_FUNCTION
	       fAddButton->SetState(kButtonDisabled) ;
#endif
	       // Get the last configuration put into the vector of
	       // configs and delete the set_add value.  Since the
	       // stop button is only active when set/add has been
	       // pushed there should be a configuration in configs.
	       if (!configs.empty()) {
		  configs.back().add_set.clear() ;
	       }

               SetDirty (kTRUE);
               break;
            }
         // trigger
         case k_ID_TRIGGER:
            {
               printf("Button was pressed, id = %ld\n", parm1);
               break;
            }
         // exit
         case k_ID_EXIT: 
            {
               CloseWindow();
               break;
            }
         // foton
         case k_ID_FOTON: 
            {
               // get function pointer
               wizfunc_t wizf = 0;
               wizf = (wizfunc_t)getFunc();
               if (!wizf) {
                  cerr << "Unable to get foton entry point" << endl;
                  return kTRUE;
               }
               // call function
               string filterspec = fFilterSpec->GetText();
               if (!wizf ("DTT", filterspec)) {
                  return kTRUE;
               }
               fFilterSpec->SetText (filterspec.c_str());
               break;
            }
         // set new overall gain
         case k_ID_RAMPSET: 
            {
               SetGain();
               SetGainDirty (kFALSE);
               break;
            }
         // all others
         default:
            return kFALSE;
      }
   
      return kTRUE;
   }


/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  HandleRadioButton   	                     */
/*                                                                           */
/* Procedure Description:  Handle button msg	                             */
/*                                                                           */
/* Procedure Arguments:  parm1					             */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
   Bool_t AwgMainFrame::HandleRadioButtons (Long_t parm1)
   {
      int i;
   
      if (parm1 >= k_ID_WAVEFORM_SINE && parm1 <= k_ID_WAVEFORM_SWEEP) {
         awgCmd.waveType = parm1;
         for (i=0; i<9; ++i) {
            if (fWaveRadio[i]->WidgetId() != parm1) {
               fWaveRadio[i]->SetState(kButtonUp);
            }
         }
         fWaveRadio[7]->SetState (kButtonDisabled);
      }
      if (parm1 == k_ID_WAVEFORM_SINE || 
         parm1 == k_ID_WAVEFORM_RAMP || 
         parm1 == k_ID_WAVEFORM_TRIANGLE) {
         fFreqText->SetState(kTRUE);
         fAmpText->SetState(kTRUE);
         fOffsetText->SetState(kTRUE);
         fPhaseText->SetState(kTRUE);
         fHighFreqText->SetState(kFALSE);
         fHighAmpText->SetState(kFALSE);
         fRatioText->SetState(kFALSE);
         fRateFreqText->SetState(kFALSE);
      }
      if (parm1 == k_ID_WAVEFORM_SQUARE) {
         fFreqText->SetState(kTRUE);
         fAmpText->SetState(kTRUE);
         fOffsetText->SetState(kTRUE);
         fPhaseText->SetState(kTRUE);
         fHighFreqText->SetState(kFALSE);
         fHighAmpText->SetState(kFALSE);
         fRateFreqText->SetState(kFALSE);
         fRatioText->SetState(kTRUE);
      }
      if (parm1 == k_ID_WAVEFORM_OFFSET) {
         fFreqText->SetState(kFALSE);
         fAmpText->SetState(kTRUE);
         fOffsetText->SetState(kFALSE);
         fPhaseText->SetState(kFALSE);
         fHighFreqText->SetState(kFALSE);
         fHighAmpText->SetState(kFALSE);
         fRateFreqText->SetState(kFALSE);
         fRatioText->SetState(kFALSE);
      }
      if (parm1 == k_ID_WAVEFORM_UNIFORM || 
         parm1 == k_ID_WAVEFORM_NORMAL) {
         fFreqText->SetState(kTRUE);
         fAmpText->SetState(kTRUE);
         fOffsetText->SetState(kTRUE);
         fHighFreqText->SetState(kTRUE);
         fHighAmpText->SetState(kFALSE);
         fPhaseText->SetState(kFALSE);
         fRateFreqText->SetState(kFALSE);
         fRatioText->SetState(kFALSE);
      }
      if (parm1 == k_ID_WAVEFORM_ARBITRARY) {
         fFreqText->SetState(kTRUE);
         fAmpText->SetState(kFALSE);
         fOffsetText->SetState(kFALSE);
         fPhaseText->SetState(kFALSE);
         fHighFreqText->SetState(kFALSE);
         fHighAmpText->SetState(kFALSE);
         fRateFreqText->SetState(kTRUE);
         fRatioText->SetState(kFALSE);
      }
      if (parm1 == k_ID_WAVEFORM_SWEEP) {
         fFreqText->SetState(kTRUE);
         fAmpText->SetState(kTRUE);
         fHighFreqText->SetState(kTRUE);
         fHighAmpText->SetState(kTRUE);
         fRateFreqText->SetState(kTRUE);
         fOffsetText->SetState(kFALSE);
         fPhaseText->SetState(kFALSE);
         fRatioText->SetState(kFALSE);
      }
      if (parm1 >= k_ID_TYPE_LINEAR && parm1 <= k_ID_TYPE_LOG) {
         if(parm1 == k_ID_TYPE_LINEAR){
            strcpy(awgCmd.sweepType, "linear");
         }
         else{
            strcpy(awgCmd.sweepType, "log");
         }
         for (i=0; i<2; ++i) {
            if (fSweepTypeRadio[i]->WidgetId() != parm1)
               fSweepTypeRadio[i]->SetState(kButtonUp);
         }
      }
      if (parm1 >= k_ID_DIRECTION_UP && 
         parm1 <= k_ID_DIRECTION_UPDOWN) {
         if(parm1 == k_ID_DIRECTION_UP) {
            strcpy(awgCmd.sweepDir, "+");
         }
         else if(parm1 == k_ID_DIRECTION_DOWN){
            strcpy(awgCmd.sweepDir, "-");
         }
         else{
            strcpy(awgCmd.sweepDir, " ");
         }
         for (i=0; i<3; ++i) {
            if (fSweepDirectRadio[i]->WidgetId() != parm1)
               fSweepDirectRadio[i]->SetState(kButtonUp);
         }
      }
      if (parm1 >= k_ID_TRIGGER_SINGLE && parm1 <= k_ID_TRIGGER_AUTO) {
         if(parm1 == k_ID_TRIGGER_SINGLE) {
            strcpy(awgCmd.trigger, "w");
         }
         else{
            strcpy(awgCmd.trigger, "c");
         }
         // for (i=0; i<2; ++i) {
            // if (fTriggerRadio[i]->WidgetId() != parm1)
               // fTriggerRadio[i]->SetState(kButtonUp);
         // }
      }
   
      return kTRUE;
   }



/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  ProcessMessage                                  */
/*                                                                           */
/* Procedure Description:  Construct AWG GUI window.                         */
/*                                                                           */
/* Procedure Arguments:  const TGWindow *p, UInt_t w, UInt_t h               */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/

Bool_t AwgMainFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
{
    /* Handle messages sent to the AwgMainFrame object. E.g. all menu button
       messages. */

    int status;

    switch (GET_MSG(msg))
    {

        case kC_COMMAND:
            switch (GET_SUBMSG(msg))
            {
                // comboboxes
                case kCM_COMBOBOX:
                    switch (parm1)
                    {
                        /* Channel Names */
                        case k_ID_CHANNEL:
                        {
                            status = ReadChannel();
                            SetDirty(status);
                            SetGainDirty(status);
                            break;
                        }
                        case k_ID_LOW_UNIT: /* Low Hz/kHz ComboBox */
                        case k_ID_PHASE_UNIT: /* Rad/Deg Combo Box */
                        case k_ID_HIGH_UNIT: /* High Hz/kHz ComboBox */
                        case k_ID_RATE_UNIT: /* Rate Hz/kHz ComboBox */
                        {
                            SetDirty(kTRUE);
                            break;
                        }
                    }
                    break;
                    // buttons
                case kCM_BUTTON:
                    HandleButtons(parm1);
                    break;
                    // radio buttons
                case kCM_RADIOBUTTON:
                    HandleRadioButtons(parm1);
                    SetDirty(kTRUE);
                    break;
                    // menus
                case kCM_MENU:
                {
                    TGFileInfo fi;
                    fi.fFilename = StrDup(fCurFile.Data());
                    fi.fIniDir = StrDup(fCurDir.Data());
                    fi.fMultipleSelection = kFALSE;
                    fi.fFileTypeIdx = 0;

#if ROOT_VERSION_CODE >= ROOT_VERSION(3, 1, 6)
                    fi.fFileTypes = const_cast<const char **>(filetypes);
#else
                    fi.fFileTypes = const_cast<char**>(filetypes);
#endif

                    switch (parm1)
                    {
#ifdef ARBITRARY_WAVEFORM
                        case M_FILE_WAVEFORM:
                {
                           new TGFileDialog(fClient->GetRoot(), this, kFDOpen,&fi);

               if (!fi.fFilename)
               {
                  // If the fFilename came back null, the user canceled.
                  break ;
               }
               fWaveformFilename = TString(fi.fFilename) ;
               TString FilePart = gSystem->BaseName(fWaveformFilename.Data()) ;
               // Attempt to open the file.
               fWaveformFile.open(fWaveformFilename.Data(), std::fstream::in) ;
               if (!fWaveformFile.is_open()) {
                  // Display a warning that the file could not be opened.
                  string errmsg = string("Cannot open ") + string(FilePart.Data()) ;
                  new TGMsgBox (gClient->GetRoot(), this, "Error", errmsg.c_str(),
                         kMBIconExclamation, kMBOk, 0) ;
                  fWaveformFilename.Clear() ;
               }
               // Fill in the Waveform Data File: widget.
               fWaveBuffer->Clear() ;
               fWaveBuffer->AddText(0, FilePart.Data()) ;
               fClient->NeedRedraw(fWaveText) ;

#if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                           delete [] fi.fFilename;
#endif
                           break;
                }
#endif

                        case M_FILE_CONFIGURATION:
                        {
                            new TGFileDialog(fClient->GetRoot(), this, kFDOpen, &fi);

                            if (!fi.fFilename)
                            {
                                // If the fFilename came back null, the user canceled.
                                break;
                            }
                            string filename = fi.fFilename;

                            fCurDir = gSystem->DirName(filename.c_str());
                            fCurFile = gSystem->BaseName(filename.c_str());

                            // Since a new configuration is being read, clear the old one first.
                            configs.clear();
                            ReadConfiguration();

#if ROOT_VERSION_CODE < ROOT_VERSION(3, 1, 6)
                            delete [] fi.fFilename;
#endif
                            break;
                        }
                        case M_FILE_SAVE:
                        {
                            if (fCurFile.Length() != 0)
                            {
                                WriteConfiguration();
                                break;
                            }
                            // Allow this to fall through to save as if the file name
                            // hasn't been defined yet.
                        }
                        case M_FILE_SAVEAS:
                        {

                            new TGFileDialog(fClient->GetRoot(), this, kFDSave, &fi);

                            if (!fi.fFilename)
                            {
                                // If the fFilename came back null, the user canceled.
                                break;
                            }
                            string filename = fi.fFilename;

                            fCurDir = gSystem->DirName(filename.c_str());
                            fCurFile = gSystem->BaseName(filename.c_str());

                            WriteConfiguration();

#if ROOT_VERSION_CODE < ROOT_VERSION(3, 1, 6)
                            delete [] fi.fFilename;
#endif
                            break;
                        }
                        case M_FILE_EXIT:
                            CloseWindow();   /* this also terminates theApp */
                            break;

                        case M_STATUS_CONFIGURATION:
                            // format the current configuration and output
                            // the results into a vector of strings to display
                            // in a popup.
                        {
                            vector<string> *string_vect = new vector<string>;
                            char buf[128];

                            for (std::vector<struct configuration>::iterator i = configs.begin();
                                 i != configs.end(); ++i)
                            {
                                if (!i->chnname.empty())
                                {
                                    sprintf(buf, "Channel name      = %s", i->chnname.c_str());
                                    string_vect->push_back(string(buf));
                                }
                                sprintf(buf, "  Low Frequency   = %s %s", i->fFreqBuf.c_str(),
                                        FreqCombo2Str(i->fFreqCombo));
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Low Amplitude   = %s", i->fAmpBuf.c_str());
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Offset          = %s", i->fOffsetBuf.c_str());
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Phase           = %s %s", i->fPhaseBuf.c_str(),
                                        AngleCombo2Str(i->fRadiansCombo));
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  High Frequency  = %s %s", i->fHighFreqBuf.c_str(),
                                        FreqCombo2Str(i->fHighCombo));
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  High Amplitude  = %s", i->fHighAmpBuf.c_str());
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Rate            = %s %s", i->fRateBuf.c_str(),
                                        FreqCombo2Str(i->fRateCombo));
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Ratio           = %%%s", i->fRatioBuf.c_str());
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Phase in/out    = %s", i->fPhaseInOutBuf.c_str());
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Gain            = %s", i->fGainBuf.c_str());
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Gain Ramp Time  = %s", i->fRampBuf.c_str());
                                string_vect->push_back(string(buf));
                                sprintf(buf, "  Filter          = %s", i->fFilterSpec.c_str());
                                string_vect->push_back(string(buf));
                                // GetState() returns an enum EButtonState which is one of
                                // kButtonUp = 0, kButtonDown = 1, kButtonEngaged = 2, kButtonDisabled = 3.
                                for (int j = 0; j < 9; ++j)
                                {
                                    if (i->fWaveRadio[j] == kButtonDown)
                                    {
                                        sprintf(buf, "  Waveform        = %s", WaveformButton2Str(j));
                                        break;
                                    }
                                }
                                string_vect->push_back(string(buf));
                                for (int j = 0; j < 2; ++j)
                                {
                                    if (i->fSweepTypeRadio[j] == kButtonDown)
                                    {
                                        sprintf(buf, "  Sweep Type      = %s", SweepType2Str(j));
                                        break;
                                    }
                                }
                                string_vect->push_back(string(buf));
                                for (int j = 0; j < 3; ++j)
                                {
                                    if (i->fSweepDirectRadio[j] == kButtonDown)
                                    {
                                        sprintf(buf, "  Sweep Direction = %s", SweepDir2Str(j));
                                        break;
                                    }
                                }
                                string_vect->push_back(string(buf));
                                if (!(i->add_set.empty()))
                                {
                                    sprintf(buf, "  Add/Set         = %s", i->add_set.c_str());
                                    string_vect->push_back(string(buf));
                                }

                            }
                            new TLGErrorDialog(gClient->GetRoot(), (TGWindow *) this,
                                               string_vect, "AWG Configuration");
                        }
                            break;

                        case M_STATUS_STATISTICS:
                            // collect the statistics, then format the results
                            // into a vector of string and display them in a popup.
                        {
                            vector<string> *string_vect = new vector<string>;

                            awgStat_t stat;

                            if (awgCmd.slotNum != 0)
                            {
                                int ifo = (awgCmd.slotNum / 1000) - 1;
                                int awg = awgCmd.slotNum - ((ifo + 1) * 1000);
                                int retval = 0;
                                char buf[128];

                                retval = awgStatistics(awgCmd.slotNum, &stat);
                                sprintf(buf, "Staistics of node %i/awg %i (time in ms/heartbeat)", ifo, awg);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.0f : number of waveform processing cycles", stat.pwNum);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : average time to process waveforms", stat.pwMean / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : standard deviation of processing waveforms",
                                        stat.pwStddev / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : maximum time needed to process waveforms", stat.pwMax / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.0f : number of reflective memory writes", stat.rmNum);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : average time to write to reflective memory", stat.rmMean / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : standard deviation of writing to reflective memory",
                                        stat.rmStddev / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : maximum time needed to write to reflective memory",
                                        stat.rmMax / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : spare time after writing to reflective memory",
                                        stat.rmCrit / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.0f : number of late writes to reflective memory", stat.rmNumCrit);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.0f : number of writes to the DAC", stat.dcNum);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : average time to write to the DAC", stat.dcMean / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : standard deviation of writing to the DAC", stat.dcStddev / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.3f : maximum time needed to write to the DAC", stat.dcMax / 1E6);
                                string_vect->push_back(string(buf));
                                sprintf(buf, "%10.0f : number of missed writes to the DAC", stat.dcNumCrit);
                                string_vect->push_back(string(buf));
                                new TLGErrorDialog(gClient->GetRoot(), (TGWindow *) this,
                                                   string_vect, "AWG Statistics");
                            }
                        }
                            break;

                        case M_HELP_NOTES:
                        {
                            // The inheritance is AwgMainFrame:TGCompositeFrame:TGFrame:TGWindow
                            vector<string> *string_vect = new vector<string>;
                            for (const char **p = awggui_changes; *p; p++)
                            {
                                string_vect->push_back(string(*p));
                            }
                            new TLGErrorDialog(gClient->GetRoot(), (TGWindow *) this,
                                               string_vect, "Release Notes");
                            break;
                        }
                        case M_HELP_ABOUT:
                        {
                            new TGMsgBox(fClient->GetRoot(), this, "About",
                                         aboutmsg, 0, kMBOk);
                            break;
                        }
                        case M_HELP_BUG:
                        {
                            const char bug_url[]="https://git.ligo.org/cds/dtt/issues/new";
                            new ligogui::BugReportDlg(0, 0, bug_url, "contact+cds-dtt-5116-issue-@support.ligo.org");
                            break ;
                        }
                    }
                    break;
                }

                default:
                    break;
            }

            break;


        case kC_TEXTENTRY:   /*  Textfield Event */
            switch (GET_SUBMSG(msg))
            {
                case kTE_ENTER:
                    status = HandleText(parm1);
                    break;
                case kTE_TEXTCHANGED:
                    if (parm1 == k_ID_PHASE_INOUT)
                    {
                    }
                    else if (parm1 == k_ID_GAIN || parm1 == k_ID_RAMPTIME)
                    {
                        SetGainDirty(kTRUE);
                    }
                    else
                    {
                        SetDirty(kTRUE);
                    }
                    break;
            }
            break;

    }
    return kTRUE;
}

   void AwgMainFrame::SaveConfiguration(const char *add_set)
   {
      struct configuration	newconfig ;

      if (add_set != (char *) NULL)
	 newconfig.add_set     = string(add_set) ;
      newconfig.chnname        = string(fChanCombo->GetChannel()) ;
      newconfig.fFreqBuf       = string(fFreqBuf->GetString()) ;
      newconfig.fFreqCombo     = fFreqCombo->GetSelected() ;
      newconfig.fAmpBuf        = string(fAmpBuf->GetString()) ;
      newconfig.fOffsetBuf     = string(fOffsetBuf->GetString()) ;
      newconfig.fPhaseBuf      = string(fPhaseBuf->GetString()) ;
      newconfig.fRadiansCombo  = fRadiansCombo->GetSelected() ;
      newconfig.fHighFreqBuf   = string(fHighFreqBuf->GetString()) ;
      newconfig.fHighCombo     = fHighCombo->GetSelected() ;
      newconfig.fHighAmpBuf    = string(fHighAmpBuf->GetString()) ;
      newconfig.fRateBuf       = string(fRateBuf->GetString()) ;
      newconfig.fRateCombo     = fRateCombo->GetSelected() ;
      newconfig.fRatioBuf      = string(fRatioBuf->GetString()) ;
      newconfig.fPhaseInOutBuf = string(fPhaseInOutBuf->GetString()) ;
      newconfig.fGainBuf       = string(fGainBuf->GetString()) ;
      newconfig.fRampBuf       = string(fRampBuf->GetString()) ;
      newconfig.fFilterSpec    = string(fFilterSpec->GetText()) ;
      for (int i = 0; i < 9; ++i)
	 newconfig.fWaveRadio[i] = (int) fWaveRadio[i]->GetState() ;
      newconfig.fSweepTypeRadio[0] = (int) fSweepTypeRadio[0]->GetState() ;
      newconfig.fSweepTypeRadio[1] = (int) fSweepTypeRadio[1]->GetState() ;
      newconfig.fSweepDirectRadio[0] = (int) fSweepDirectRadio[0]->GetState() ;
      newconfig.fSweepDirectRadio[1] = (int) fSweepDirectRadio[1]->GetState() ;
      newconfig.fSweepDirectRadio[2] = (int) fSweepDirectRadio[2]->GetState() ;

      configs.push_back(newconfig) ;
      
      if (my_debug) {
	 cerr << "AwgMainFrame::SaveConfiguration(" << (add_set ? add_set : "NULL") << ")" << endl ;
	 cerr << "  chnname      = " << configs.back().chnname << endl ;
	 cerr << "  fFreqBuf       = " << configs.back().fFreqBuf << endl ;
	 cerr << "  fFreqCombo     = " << configs.back().fFreqCombo << endl ;
	 cerr << "  fAmpBuf        = " << configs.back().fAmpBuf << endl ;
	 cerr << "  fOffsetBuf     = " << configs.back().fOffsetBuf << endl ;
	 cerr << "  fPhaseBuf      = " << configs.back().fPhaseBuf << endl ;
	 cerr << "  fRadiansCombo  = " << configs.back().fRadiansCombo << endl ;
	 cerr << "  fHighFreqBuf   = " << configs.back().fHighFreqBuf << endl ;
	 cerr << "  fHighCombo     = " << configs.back().fHighCombo << endl ;
	 cerr << "  fHighAmpBuf    = " << configs.back().fHighAmpBuf << endl ;
	 cerr << "  fRateBuf       = " << configs.back().fRateBuf << endl ;
	 cerr << "  fRateCombo     = " << configs.back().fRateCombo << endl ;
	 cerr << "  fRatioBuf      = " << configs.back().fRatioBuf << endl ;
	 cerr << "  fPhaseInOutBuf = " << configs.back().fPhaseInOutBuf << endl ;
	 cerr << "  fGainBuf       = " << configs.back().fGainBuf << endl ;
	 cerr << "  fRampBuf       = " << configs.back().fRampBuf << endl ;
	 cerr << "  fFilterSpec    = " << configs.back().fFilterSpec << endl ;
	 // GetState() returns an enum EButtonState which is one of
	 // kButtonUp = 0, kButtonDown = 1, kButtonEngaged = 2, kButtonDisabled = 3.
	 for (int i = 0; i < 9; ++i)
	    cerr << "  fWaveRadio[" << i << "] = " << configs.back().fWaveRadio[i] << endl ;
	 cerr << "  fSweepTypeRadio[0] = " << configs.back().fSweepTypeRadio[0] << endl ;
	 cerr << "  fSweepTypeRadio[1] = " << configs.back().fSweepTypeRadio[1] << endl ;
	 cerr << "  fSweepDirectRadio[0] = " << configs.back().fSweepDirectRadio[0] << endl ;
	 cerr << "  fSweepDirectRadio[1] = " << configs.back().fSweepDirectRadio[1]  << endl ;
	 cerr << "  fSweepDirectRadio[2] = " << configs.back().fSweepDirectRadio[2] << endl ;
      }
   }

   void AwgMainFrame::WriteConfiguration()
   {
      // Attempt to open the file.
      string filename = string(fCurDir.Data()) + string("/") + string(fCurFile.Data()) ;

      std::fstream fs(filename, std::fstream::out) ;
      if (fs.is_open())
      {
	 if (configs.empty())
	 {
	    // Create a configuration from the current settings.
	    SaveConfiguration((char *) NULL) ;
	 }
	 for (std::vector<struct configuration>::iterator i = configs.begin() ;
	       i != configs.end() ; ++i) {
	    if (!i->chnname.empty())
	       fs << "chnname " << i->chnname << endl ;
	    fs << "fFreqBuf " << i->fFreqBuf << endl ;
	    fs << "fFreqCombo " << i->fFreqCombo << endl ;
	    fs << "fAmpBuf " << i->fAmpBuf << endl ;
	    fs << "fOffsetBuf " << i->fOffsetBuf << endl ;
	    fs << "fPhaseBuf " << i->fPhaseBuf << endl ;
	    fs << "fRadiansCombo " << i->fRadiansCombo << endl ;
	    fs << "fHighFreqBuf " << i->fHighFreqBuf << endl ;
	    fs << "fHighCombo " << i->fHighCombo << endl ;
	    fs << "fHighAmpBuf " << i->fHighAmpBuf << endl ;
	    fs << "fRateBuf " << i->fRateBuf << endl ;
	    fs << "fRateCombo " << i->fRateCombo << endl ;
	    fs << "fRatioBuf " << i->fRatioBuf << endl ;
	    fs << "fPhaseInOutBuf " << i->fPhaseInOutBuf << endl ;
	    fs << "fGainBuf " << i->fGainBuf << endl ;
	    fs << "fRampBuf " << i->fRampBuf << endl ;
	    if (i->fFilterSpec.length() != 0)
	       fs << "fFilterSpec " << i->fFilterSpec << endl ;
	    for (int j = 0; j < 9; ++j)
	       fs << "fWaveRadio[" << j << "] " << i->fWaveRadio[j] << endl ;
	    fs << "fSweepTypeRadio[0] " << i->fSweepTypeRadio[0] << endl ;
	    fs << "fSweepTypeRadio[1] " << i->fSweepTypeRadio[1] << endl ;
	    fs << "fSweepDirectRadio[0] " << i->fSweepDirectRadio[0] << endl ;
	    fs << "fSweepDirectRadio[1] " << i->fSweepDirectRadio[1] << endl ;
	    fs << "fSweepDirectRadio[2] " << i->fSweepDirectRadio[2] << endl ;
	    // Do this last, we'll use it as a trigger to take action on 
	    // readback of the file.
	    if (!(i->add_set.empty()))
	       fs << "add_set " << i->add_set << endl ;
	 }
	 fs.close() ;
      }
   }

   // This routine reads a file that was written, but if the users decide to 
   // start editing it or trying to write their own it'll have to be made
   // much more robust.
   void AwgMainFrame::ReadConfiguration()
   {
      string filename = string(fCurDir.Data()) + string("/") + string(fCurFile.Data()) ;

      // Attempt to open the file.
      std::fstream fs(filename, std::fstream::in) ;
      if (fs.is_open())
      {
	 string attr, value ;
	 while (!fs.eof()) {
	    fs >> attr >> value ;
	    if (attr == string("chnname"))
	       fChanCombo->SelectChannel(value.c_str()) ;
	    else if (attr == string("fFreqBuf")) {
	       fFreqBuf->Clear() ;
	       fFreqBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fFreqText);
	       HandleText(k_ID_LOW_F);
	    }
	    else if (attr == string("fFreqCombo"))
	       fFreqCombo->Select(atoi(value.c_str())) ;
	    else if (attr == string("fAmpBuf")) {
	       fAmpBuf->Clear() ;
	       fAmpBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fAmpText);
	       HandleText(k_ID_LOW_A);
	    }
	    else if (attr == string("fOffsetBuf")) {
	       fOffsetBuf->Clear() ;
	       fOffsetBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fOffsetText);
	       HandleText(k_ID_OFFSET);
	    }
	    else if (attr == string("fPhaseBuf")) {
	       fPhaseBuf->Clear() ;
	       fPhaseBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fPhaseText) ;
	       HandleText(k_ID_PHASE);
	    }
	    else if (attr == string("fRadiansCombo"))
	       fRadiansCombo->Select(atoi(value.c_str())) ;
	    else if (attr == string("fHighFreqBuf")) {
	       fHighFreqBuf->Clear() ;
	       fHighFreqBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fHighFreqText) ;
	       HandleText(k_ID_HIGH_F);
	    }
	    else if (attr == string("fHighCombo"))
	       fHighCombo->Select(atoi(value.c_str())) ;
	    else if (attr == string("fHighAmpBuf")) {
	       fHighAmpBuf->Clear() ;
	       fHighAmpBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fHighAmpText) ;
	       HandleText(k_ID_HIGH_A);
	    }
	    else if (attr == string("fRateBuf")) {
	       fRateBuf->Clear() ;
	       fRateBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fRateFreqText) ;
	       HandleText(k_ID_RATE);
	    }
	    else if (attr == string("fRateCombo")) {
	       fRateCombo->Select(atoi(value.c_str())) ;
	    }
	    else if (attr == string("fRatioBuf")) {
	       fRatioBuf->Clear() ;
	       fRatioBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fRatioText) ;
	       HandleText(k_ID_RATIO);
	    }
	    else if (attr == string("fPhaseInOutBuf")) {
	       fPhaseInOutBuf->Clear() ;
	       fPhaseInOutBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fPhaseInOutText) ;
	       HandleText(k_ID_PHASE_INOUT);
	    }
	    else if (attr == string("fGainBuf")) {
	       fGainBuf->Clear() ;
	       fGainBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fGainText) ;
	       HandleText(k_ID_GAIN);
	    }
	    else if (attr == string("fRampBuf")) {
	       fRampBuf->Clear() ;
	       fRampBuf->AddText(0, value.c_str()) ;
	       fClient->NeedRedraw(fRampText) ;
	       HandleText(k_ID_RAMPTIME);
	    }
	    else if (attr == string("fFilterSpec")) {
	       fFilterSpec->SetText(value.c_str()) ;
	    }
	    else if (attr == string("fWaveRadio[0]"))
	       fWaveRadio[0]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fWaveRadio[1]"))
	       fWaveRadio[1]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fWaveRadio[2]"))
	       fWaveRadio[2]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fWaveRadio[3]"))
	       fWaveRadio[3]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fWaveRadio[4]"))
	       fWaveRadio[4]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fWaveRadio[5]"))
	       fWaveRadio[5]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fWaveRadio[6]"))
	       fWaveRadio[6]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fWaveRadio[7]"))
	       fWaveRadio[7]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fWaveRadio[8]"))
	       fWaveRadio[8]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fSweepTypeRadio[0]"))
	       fSweepTypeRadio[0]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fSweepTypeRadio[1]"))
	       fSweepTypeRadio[1]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fSweepDirectRadio[0]"))
	       fSweepDirectRadio[0]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fSweepDirectRadio[1]"))
	       fSweepDirectRadio[1]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("fSweepDirectRadio[2]"))
	       fSweepDirectRadio[2]->SetState((EButtonState) atoi(value.c_str())) ;
	    else if (attr == string("add_set")) {
	       if (value == string("set")) {
		  int ramptime = atoi(fPhaseInOutBuf->GetString()) ;

		  // Handle the radio buttons for the waveforms
		  for (int i = 0; i < 9; ++i) {
		     if (fWaveRadio[i]->GetState() == kButtonDown) {
			HandleRadioButtons(k_ID_WAVEFORM_SINE+i) ;
			break ;
		     }
	          }

		  // Trigger the waveform.
		  HandleButtons(k_ID_SETRUN) ;
		  // Wait for signal to ramp up before continuing.  There may
		  // be an "add" command which would add another component to
		  // the excitation.
		  if (ramptime > 0) {
		     sleep(ramptime) ;
		  }

	       } else if (value == string("add")) {
		  int ramptime = atoi(fPhaseInOutBuf->GetString()) ;

		  // Handle the radio buttons for the waveforms
		  for (int i = 0; i < 9; ++i) {
		     if (fWaveRadio[i]->GetState() == kButtonDown) {
			HandleRadioButtons(k_ID_WAVEFORM_SINE+i) ;
			break ;
		     }
	          }

		  // Trigger the waveform.
		  HandleButtons(k_ID_ADD) ;
		  // Wait for signal to ramp up before continuing.  There may
		  // be an "add" command which would add another component to
		  // the excitation.
		  if (ramptime > 0) {
		     sleep(ramptime) ;
		  }
	       }
	    }
	 }
      }
      //else the file couldn't be read.  We could complain about that here.
   }
      




/*---------------------------------------------------------------------------*/
/*                                                                           */
/* External Procedure Name:  InitGui                                         */
/*                                                                           */
/* Procedure Description:  Construct AWG GUI window.                         */
/*                                                                           */
/* Procedure Arguments:  const TGWindow *p, UInt_t w, UInt_t h               */
/*                                                                           */
/* Procedure Returns:                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/

   TROOT root("GUI", "AWG GUI");

   const string         _argDisplay ("-d");
   const string         _argDisplayAlt2 ("--display") ;
   const string         _argDisplayAlt ("-display");
   const string         _argHelp ("-h");
   const string         _argHelpAlt ("-?");
   const string         _argHelpAlt2 ("--help") ;

   const char* const _helptext =
   "Usage: awggui -flags [configuration file]\n"
#if 0
   "       --display 'display'\n"
   "       -d 'display'  X windows display\n\n"
#endif
   "       --help\n"
   "       -?\n"
   "       -h            this help\n";

   char* strnew (const char* p)
   {
      char*     t = new (nothrow) char [strlen (p) + 1];
      if (t != 0) {
         strcpy (t, p);
      }
      return t;
   }

   int main(int argc, char **argv)
   {
      Int_t	rootArgc = 1 ;
      char     *rootArgv[100] = {0} ;
      char     *filename = 0 ;

      rootArgv[0] = strnew (argv[0]) ;

      for (int i = 1; i < argc; i++) {
         if ((_argHelp == argv[i]) ||  (_argHelpAlt == argv[i]) || (_argHelpAlt2 == argv[i]))
         {
            cout << _helptext;
            return 0;
         }
#if 0
	 // look for x display
         else if (((_argDisplay == argv[i]) || (_argDisplayAlt == argv[i]) || (_argDisplayAlt2 == argv[i])) &&
                 (i + 1 < argc) && (argv[i+1][0] != '-'))
         {
            if (rootArgc < 99) {
               rootArgv[rootArgc] = strnew ("-display");
               rootArgv[rootArgc+1] = strnew (argv[i + 1]);
               rootArgc += 2;
               i++;
            }
         }
#endif
	 // Look for a file name for a configuration file.
	 else if ((i + 1 == argc) && (argv[i][0] != '-'))
	 {
	    filename = argv[i] ;
	 }
         // otherwise assume ROOT argument
         else
         {
            if (rootArgc < 100)
            {
               rootArgv[rootArgc] = strnew (argv[i]);
               rootArgc++;
            }
         }
      }

      memset (&awgCmd, 0, sizeof (awgCmd));
   
      TApplication theApp("App", &rootArgc, rootArgv);
   
      // If a configuration file were to be read, add it to the end
      // of the arguments to mainWindow().
      AwgMainFrame mainWindow(gClient->GetRoot(), 600, 240, filename);
   
      theApp.Run();
   
      return 0;
   }
