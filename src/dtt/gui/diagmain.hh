/* version $Id: diagmain.hh 7574 2016-02-16 20:17:52Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: diagmain						*/
/*                                                         		*/
/* Module Description: Main window of diagnostics test tools		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 23Nov99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: diagmain.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_DIAGMAIN_H
#define _GDS_DIAGMAIN_H

#include <string>
#include <TGFrame.h>
#include <TGMenu.h>
#include <TGTab.h>
#include <TGButton.h>
#include <TGStatusBar.h>
#include <TTimer.h>
#include "TLGPad.hh"
#include "diagplot.hh"

   class PlotSet;

namespace ligogui {
   class TLGPrintParam;
   struct ExportOption_t;
   typedef ExportOption_t ImportOption_t;
   struct ReferenceTraceList_t;
   struct MathTable_t;
   struct OptionAll_t;
}

namespace calibration {
   class Table;
   class Calibration;
}

namespace diag {


   class basic_commandline;
   class DiagTabControl;
   struct TestParam_t;
   class NotificationList;

/** @name diagmain
    This header exports the main window for the diagnostics test
    program.
   
    @memo Main window of the diagnostics test program
    @author Written December 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{
/** @name Constants
    Constants for the main diagnostics test window.
   
    @memo Main diagnostics window constants
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// State of the diagnostiocs kernel
   enum DiagState_t {
   /// Inactive
   dsInactive = 0,
   /// test running
   dsRunning = 1,
   /// test paused
   dsPaused = 2,
   /// test aborted
   dsAborted = 3,
   /// test failed
   dsFailed = 4,
   /// test terminated successfully
   dsDone = 5
   };
//@}


/** Diagnostics main window. This is the main window of the diagnostics
    tools. It implements a menu, a status bar, a series of buttons
    and the main tab control center.
   
    @memo Diagnostics main window
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DiagMainWindow : public TGMainFrame {
      friend class ActionPlots;
   protected:
      /// Pointer to command line interpreter
      basic_commandline*	fCmdLine;
      /// Test parameters
      TestParam_t*	fParam;

      //base window title
      std::string fwindow_name;
      /// List of measurement channels
      char*		fMeasChannelList;
      /// List of excitation channels
      char*		fExcChannelList;
      /// Plot set
      PlotSet* 		fPlot;
      /// File name
      TString		fFilename;
      /// File save flag
      Int_t		fFileSaveFlag;
      /// File restore flag
      Int_t		fFileRestoreFlag;
      /// Settings save flag
      Bool_t		fSettingsSaveFlag;
      /// Settings restore flag
      Bool_t		fSettingsRestoreFlag;
      /// Calibration save flag
      Bool_t		fCalibrationSaveFlag;
      /// Calibration restore flag
      Bool_t		fCalibrationRestoreFlag;
      // Autostart the test when successfully loading a file from the command line
      Bool_t        fAutostart;
      // if true, diaggui will try to show an obvious disconnection method on test failure.
      Bool_t        fEnableDisconnectMessage;
      /// State of the diagnostics kernel
      DiagState_t	fState;
      /// current iteration/measurement number (while running)
      Int_t		fCurMeasurement;
      /// true if the next test result belongs to a new measurement
      Int_t		fNewMeasurement;
      /// current average number (while running)
      Int_t		fCurAverage;
      /// last status message
      TString		fStatusMsg;
      /// Heartbeat timer
      TTimer*		fHeartbeat;
      /// X11 watchdog
      TTimer*		fXExitTimer;
      /// Notification messages
      NotificationList*	fNotifyMsgs;
      /// List of stored options
      ligogui::OptionAll_t** fStoreOptions;
      /// Size of stored options list
      static const Int_t fStoreOptionsMax;
      /// Printing defaults
      ligogui::TLGPrintParam* fPrintDef;
      /// Import option defaults
      ligogui::ImportOption_t* fImportDef;
      /// Export option defaults
      ligogui::ExportOption_t* fExportDef;
      /// List of reference traces
      ligogui::ReferenceTraceList_t* fRefTraces;
      /// Math function table
      ligogui::MathTable_t* fMathTable;
      /// Calibration table
      calibration::Table* fCalTable;
      /// reset plot settings
      ligogui::TLGMultiPad::ActionPlotPads*	fAction;
      /// Auxiliary data object list
      AuxDataDescList	fAuxList;
   
      /// Menu bar
      TGMenuBar*	fMenuBar;
      /// File menu
      TGPopupMenu*	fMenuFile;
      /// Save/restore flag submenu
      TGPopupMenu*	fMenuFileFlag[2];
      /// File print graph submenu
      TGPopupMenu*	fMenuFilePrintGraph;
      /// Edit menu
      TGPopupMenu*	fMenuEdit;
      /// Measurement menu
      TGPopupMenu*	fMenuMeas;
      /// Plot menu
      TGPopupMenu*	fMenuPlot;
      /// Window menu
      TGPopupMenu*	fMenuWindow;
      /// Zoom window submenu
      TGPopupMenu*	fMenuWindowZoom;
      /// Active window submenu
      TGPopupMenu*	fMenuWindowActive;
      // Utilities menu
      // TGPopupMenu*	fMenuUtil;
      /// Help menu
      TGPopupMenu*	fMenuHelp;
      /// Menu bar layout hint
      TGLayoutHints*	fMenuBarLayout;
      /// Menu items layout hint
      TGLayoutHints*	fMenuBarItemLayout; 
      /// Menu help item layout hint
      TGLayoutHints*	fMenuBarHelpLayout;
   
      // main tab control
      DiagTabControl*	fMainCtrl;
      // main tab frame
      TGCompositeFrame*	fMainCtrlFrame;
      // main tab layout hints
      TGLayoutHints*	fMainCtrlLayout;
      // main tab frame layout hints
      TGLayoutHints*	fMainCtrlFrameLayout;
   
      /// Start button
      TGButton*		fStart;
      /// Abort button
      TGButton*		fAbort;
      /// Pause button
      TGButton*		fPause;
      /// Resume button
      TGButton*		fResume;
      // button frame
      TGCompositeFrame*	fButtonFrame;
      // button layout hints
      TGLayoutHints*	fButtonLayout;
      // button frame layout hints
      TGLayoutHints*	fButtonFrameLayout;
      // Button foreground context
      static GContext_t fgButtonGC;
   
      /// Status bar
      TGStatusBar*	fStatus;
      /// Status bar layout hint
      TGLayoutHints*	fStatusBarLayout;
      /// Busy cursor
      static Cursor_t	fWaitCursor;

      /// Reference for TGMsgBox classes to keep them within the window.
      const TGWindow	*fParent ;
   
      /** Read parameters from diagnostics kernel.
          @memo Read measurement parameters.
          @param id Measurement type
          @return true if successful
       ******************************************************************/
      virtual Bool_t ReadParam ();
      /** Write parameters to diagnostics kernel.
          @memo Write measurement parameters.
          @param id Measurement type
          @return true if successful
       ******************************************************************/
      virtual Bool_t WriteParam ();

      /** Enable the user to report a DTT bug to developers.
       *
       * @return true if channel to report bug was successfully opened.
       */
      Bool_t ReportBug();

      /** update the M_EDIT_READFROMTAPE menu item
       *
       */
       void update_read_from_tape();
   
   public:
      /** Constructs a main diagnostics window. Supported measurement 
          types: 0 - FFT, 1 - Swept sine, 2 - Sine response, 
          3 - Triggered time series
          @memo Constructor
          @param p Parent window
          @param cmdline Pointer to command line
          @param meastype Default measurement type
       ******************************************************************/
      DiagMainWindow (const TGWindow *p, basic_commandline* cmdline, 
                     Int_t meastype = 0, const char* filename = 0, bool autostart=false,
                     bool enable_disconnect_message=false);
      /** Destructs the main diagnostics window.
          @memo Destructor.
       ******************************************************************/
      virtual ~DiagMainWindow();
   
      /** Close the main window and exit the program.
          @memo Close method
          @return void
       ******************************************************************/
      virtual void CloseWindow();
   
      /** Update the plot data.
          @memo Update plot method.
          @param stepnum Step number to update
          @return true if successful
       ******************************************************************/
      virtual Bool_t UpdatePlot (Int_t stepnum = -1, 
                        Bool_t updatePads = kTRUE);
   
      /** Get available time of data.
          @memo Get time.
          @return true if successful
       ******************************************************************/
      virtual Bool_t GetTimes (unsigned long& start, 
                        unsigned long& duration);
   
      /** Add other data. Add reference traces, auxiliary data and raw 
          data objects.
          @memo Add data method.
          @return true if successful
       ******************************************************************/
      virtual Bool_t AddOtherData ();
   
      /** Update data. This method updates the data in the diagnostics
          kernel for all 'dirty' plot descriptors in the plot set.
          @memo Update data method.
          @return true if successful
       ******************************************************************/
      virtual Bool_t UpdateData ();
   
      /** Save calibration data.
          @memo Save calibration method.
          @return true if successful
       ******************************************************************/
      virtual Bool_t SaveCalibrationData ();
   
      /** Restore calibration data.
          @memo Restore calibration method.
          @return true if successful
       ******************************************************************/
      virtual Bool_t RestoreCalibrationData ();
   
      /** Show the default plot for the current measurement type.
          @memo Show default plot method.
   	  @param updatePads if true update plot pads
          @return void
       ******************************************************************/
      virtual void ShowDefaultPlot (Bool_t updatePads = kTRUE,
                        ligogui::TLGMultiPad* mpads = 0);
   
      /** Returns the currently selected measurement type in the
          diagnostics kernel.
          @memo Get measurement type.
          @return 0 - FFT, 1 - Swept sine, 2 - Sine response, 
                  3 - Triggered time series, -1 Unknown
       ******************************************************************/
      virtual Int_t GetMeasurementType () const;
   
      /** Sets a new measurement type in the diagnostics kernel.
          @memo Set measurement type.
          @param type: 0 - FFT, 1 - Swept sine, 2 - Sine response, 
                  3 - Triggered time series, -1 Unknown
          @return true if changed from before
       ******************************************************************/
      virtual Bool_t SetMeasurementType (Int_t type);
   
      /** Remove results from plot set. If askfirst is true a popup
          dialog box will ask the user and only change clear results
          if the user confirms.
          @memo Remove results method.
          @param askfirst Ask the user before removing
          @param all clear all traces including references
          @return true if results were removed
       ******************************************************************/
      virtual Bool_t ClearResults (Bool_t askfirst = kFALSE, 
                        Bool_t all = kTRUE);
   
      /** Transfers parameter between GUI (data structure) and
          diagnostics kernel.
          @memo Transfer parameters.
          @param toGUI if true read from kernel, otherwise write
          @param newparam If false parameters are not read from kernel
                 when transferring to the GUI
          @return true if successful
       ******************************************************************/
      virtual Bool_t TransferParameters (Bool_t toGUI = kTRUE, 
                        Bool_t newparam = kFALSE);
   
      /** Transfers plot settings between GUI (data structure) and
          diagnostics kernel.
          @memo Transfer plot settings.
          @param toGUI if true read from kernel, otherwise write
          @param index1 first index of plot settings
          @param index2 second index of plot settings
          @param opt plot settings
          @return true if successful
       ******************************************************************/
      virtual Bool_t TransferPlotSettings (Bool_t toGUI, 
                        Int_t index1, Int_t index2, 
                        ligogui::OptionAll_t* pset);
   
      /** Transfers calibration settings between GUI (data structure) and
          diagnostics kernel.
          @memo Transfer calibration settings.
          @param toGUI if true read from kernel, otherwise write
          @param index  index of calibration record
          @param cal calibration record
          @return true if successful
       ******************************************************************/
      virtual Bool_t TransferCalibrationSettings (Bool_t toGUI, 
                        Int_t index, calibration::Calibration& cal);
   
      /** Saves the plot settings of all currently open plot pads.
          @memo Save plot settings.
          @return true if successful
       ******************************************************************/
      virtual Bool_t SavePlotSettings ();
   
      /** Shows the plots with the settings which were previously saved.
          This function will create plot windows as necessary.
          @memo Show plots.
          @return true if successful
       ******************************************************************/
      virtual Bool_t ShowPlots ();
   
      /** Get state. This method will returns the state flag for the 
          diagnostics kernel.
          @memo Get state method.
          @return test state
       ******************************************************************/
      virtual DiagState_t GetState () const {
         return fState; }
   
      /** Set state. This method will set the state flag for the 
          diagnostics kernel. (It will not change the state itself!)
          @memo Set state method.
          @param state New test state
       ******************************************************************/
      virtual void SetState (DiagState_t state);
   
      /** Set status message. This method will set the message string in
          status bar.
          @memo Set state message method.
          @param msg message string
       ******************************************************************/
      virtual void SetStatusMsg (const char* msg);
   
      /** Returns true if a test is running.
          @memo Is running method.
          @return true if test is running
       ******************************************************************/
      virtual Bool_t IsRunning () const {
         return (fState == dsRunning) || (fState == dsPaused); }
   
      /** Print
          @memo Print plot pad(s).
       ******************************************************************/
      virtual void Print (const char* format) const;
   
      /** Clears the IO and UND cache.
          @memo Clear cache method.
       ******************************************************************/
      void ClearCache();
   
      /** Process GUI messages.
          @memo Process message method.
          @param msg Message id
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
      /** Process menu messages.
          @memo Process menu method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessMenu (Long_t parm1, Long_t);
      /** Process button messages.
          @memo Process button method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessButton (Long_t parm1, Long_t);
      /** Process status messages.
          @memo Process status method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessStatus (Long_t parm1, Long_t);
      /** Process Notification messages.
          @memo Process notification method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessNotify (Long_t parm1, Long_t parm2);
   
      /** Heartbeat timer handler.
          @memo Heartbeat method.
          @param timer Timer
          @return true if successful
       ******************************************************************/
      virtual Bool_t HandleTimer (TTimer* timer);
      /** This method is called by the diagnostics kernel to notify
          the GUI interface.
          @memo Notify method.
          @param msg Notification mssage
          @return true if successful
       ******************************************************************/
      virtual Bool_t Notification (const char* msg);

      // Set the fParent variable.
      void setParentWindow (const TGWindow *parent) { fParent = parent; } ;

      // Make a pass through the channels selected for measurments and 
      // report any that don't exist in the channel list with a popup
      // that allows the user to abandon the measurement.
      Bool_t CheckChannels() ;
   };
//@}

}
#endif // _GDS_DIAGMAIN_H
