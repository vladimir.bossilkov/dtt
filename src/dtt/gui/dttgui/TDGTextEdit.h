//
// Created by erik.vonreis on 11/11/21.
//

#ifndef CDS_CRTOOLS_TDGTEXTEDIT_H
#define CDS_CRTOOLS_TDGTEXTEDIT_H

#include "TLGTextEditor.hh"

class TDGTextEdit: public ligogui::TLGTextEdit {

public:
  //______________________________________________________________________________

  /// Create TLGTextEdit widget
  TDGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h,
              Int_t id = -1, UInt_t sboptions = 0,
              ULong_t back = GetWhitePixel());
  /// Create TLGTextEdit widget
  TDGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h,
              TGText* text, Int_t id = -1, UInt_t sboptions = 0,
              ULong_t back = GetWhitePixel());
  /// Create TLGTextEdit widget
  TDGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h,
              const char* string, Int_t id = -1,
              UInt_t sboptions = 0, ULong_t back = GetWhitePixel());

  std::string GetString(char cmt=0) const;

};

#endif // CDS_CRTOOLS_TDGTEXTEDIT_H
