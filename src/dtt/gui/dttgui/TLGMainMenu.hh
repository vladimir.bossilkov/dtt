/* Version $Id: TLGMainMenu.hh 7856 2017-02-22 20:34:46Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGMAINMENU_H
#define _LIGO_TLGMAINMENU_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGMainWindow						*/
/*                                                         		*/
/* Module Description: Main window of diagnostics viewer		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 23Nov99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGMainWindow.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGMenu.h>
#include <TGFrame.h>

namespace ligogui {

   class TLGMultiPad;


/** @name TLGMainMenu
    This header exports the main menu for plotting applications.
   
    @memo Main window of the plotting applications
    @author Written December 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

/** @name Constants
    Constants for the main menu.
   
    @memo Main menu constants
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

   /// Menu command idendifiers
   enum EMenuCommandIdentifiers {
   /// File: new
   kM_FILE_NEW = 100,  
   /// File: open
   kM_FILE_OPEN = 101,
   /// File: save
   kM_FILE_SAVE = 102,
   /// File: save as
   kM_FILE_SAVEAS = 103,
   /// File: import
   kM_FILE_IMPORT = 104,
   /// File: export
   kM_FILE_EXPORT = 105,
   /// File restore flag: all
   kM_FILE_RFLAG_ALL = 106,
   /// File restore flag: standard
   kM_FILE_RFLAG_STD = 107,
   /// File restore flag: parameters only
   kM_FILE_RFLAG_PRM = 108,
   /// File restore flag: plot settings
   kM_FILE_RFLAG_SET = 109,
   /// File restore flag: calibration information
   kM_FILE_RFLAG_CAL = 110,
   /// File save flag: all
   kM_FILE_SFLAG_ALL = 111,
   /// File save flag: standard
   kM_FILE_SFLAG_STD = 112,
   /// File save flag: parameters only
   kM_FILE_SFLAG_PRM = 113,
   /// File save flag: plot settings
   kM_FILE_SFLAG_SET = 114,
   /// File save flag: calibration information
   kM_FILE_SFLAG_CAL = 115,
   /// File: print
   kM_FILE_PRINT = 116,
   /// File: print setup
   kM_FILE_PRINTSETUP = 117,
   /// File: print A
   kM_FILE_PRINT_GRAPHA = 118,
   /// File: print B
   kM_FILE_PRINT_GRAPHB = 119,
   /// File: exit
   kM_FILE_EXIT = 120,
   
   kM_FILE_NEWMOD = 121 ,
   
   /// Plot: reference traces
   kM_PLOT_REFERENCE = 400,
   /// Plot: math
   kM_PLOT_MATH = 401,
   /// Plot: calibration editor
   kM_PLOT_CALIBRATION_EDIT = 402,
   /// Plot: calibration import
   kM_PLOT_CALIBRATION_READ = 403,
   /// Plot: calibration export
   kM_PLOT_CALIBRATION_WRITE = 404,
   
   /// Window: new
   kM_WINDOW_NEW = 500,
   /// Window: zoom out
   kM_WINDOW_ZOOkM_OUT = 501,
   /// Window: zoom current
   kM_WINDOW_ZOOkM_CUR = 502,
   /// Window: zoom A
   kM_WINDOW_ZOOkM_A = 503,
   /// Window: zoom B
   kM_WINDOW_ZOOkM_B = 504,
   /// Window: set next
   kM_WINDOW_ACTIVE_NEXT = 510,
   /// Window: set A
   kM_WINDOW_ACTIVE_A = 511,
   /// Window: set B
   kM_WINDOW_ACTIVE_B = 512,
   /// Window: Layout options
   kM_WINDOW_LAYOUT = 513,
   
   /// Help: contents
   kM_HELP_CONTENTS = 900,
   /// Help: search
   kM_HELP_SEARCH = 901,
   /// Help: about
   kM_HELP_ABOUT = 910
   };

   /// Save restore flag
   enum ESaveRestoreFlag {
   /// Save/restore everything
   kSaveRestoreAll = 0,
   /// Save/restore common objects only
   kSaveRestoreStandard = 1,
   /// Save/restore parameters only
   kSaveRestoreParameterOnly = 2
   };

//@}


/** Main menu.
   
    @memo Main menu.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMainMenu {
   protected:
      /// Parent frame
      TGCompositeFrame* fParent;
      /// Pointer to multi pad
      TLGMultiPad* 	fMultiPad;
      /// File save flag
      ESaveRestoreFlag	fFileSaveFlag;
      /// File restore flag
      ESaveRestoreFlag	fFileRestoreFlag;
      /// Settings save flag
      Bool_t		fSettingsSaveFlag;
      /// Settings restore flag
      Bool_t		fSettingsRestoreFlag;
      /// Calibration save flag
      Bool_t		fCalibrationSaveFlag;
      /// Calibration restore flag
      Bool_t		fCalibrationRestoreFlag;
   
      /// Menu bar
      TGMenuBar*	fMenuBar;
      /// File menu
      TGPopupMenu*	fMenuFile;
      /// Save/restore flag submenu
      TGPopupMenu*	fMenuFileFlag[2];
      /// File print graph submenu
      TGPopupMenu*	fMenuFilePrintGraph;
      /// Edit menu
      TGPopupMenu*	fMenuEdit;
      /// Plot menu
      TGPopupMenu*	fMenuPlot;
      /// Window menu
      TGPopupMenu*	fMenuWindow;
      /// Zoom window submenu
      TGPopupMenu*	fMenuWindowZoom;
      /// Active window submenu
      TGPopupMenu*	fMenuWindowActive;
      /// Help menu
      TGPopupMenu*	fMenuHelp;
      /// Menu bar layout hint
      TGLayoutHints*	fMenuBarLayout;
      /// Menu items layout hint
      TGLayoutHints*	fMenuBarItemLayout; 
      /// Menu help item layout hint
      TGLayoutHints*	fMenuBarHelpLayout;
   
      /// Create a menu class (does not initializes the menu!)
      TLGMainMenu();
      /// Destroy menu
      virtual ~TLGMainMenu();
   
      /** Initializes the menu.
          @memo Setup menu.
          @return void
       ******************************************************************/
      virtual void MenuSetup (TGCompositeFrame* frame, TLGMultiPad* mpad);
      /** Set multipad.
          @memo Set multipad.
          @return void
       ******************************************************************/
      virtual void SetMultiPad (TLGMultiPad* mpad) {
         fMultiPad = mpad; }
      /** Add the file menu.
          @memo Add file menu.
          @return void
       ******************************************************************/
      virtual void AddMenuFile();
      /** Add the edit menu. Does nothing. Can be used to add menues
          between the file and the plot menu.
          @memo Add edit menu.
          @return void
       ******************************************************************/
      virtual void AddMenuEdit();
      /** Add the plot menu.
          @memo Add plot menu.
          @return void
       ******************************************************************/
      virtual void AddMenuPlot();
      /** Add the window menu.
          @memo Add window menu.
          @return void
       ******************************************************************/
      virtual void AddMenuWindow();
      /** Add the help menu.
          @memo Add help menu.
          @return void
       ******************************************************************/
      virtual void AddMenuHelp();
   
      /** File new function.
          @memo File new method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileNew() {
         return kFALSE; }
      /** File open function.
          @memo File open method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileOpen() {
         return kFALSE; }
      /** File save function.
          @memo File save method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileSave() {
         return kFALSE; }
      /** File save as function.
          @memo File save as method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileSaveAs() {
         return kFALSE; }
   
      /** File export function.
          @memo File export method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileExport();
      /** File import function.
          @memo File import method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileImport();
   
      /** Process menu messages.
          @memo Process menu.
          @return true if processed
       ******************************************************************/
      virtual Bool_t ProcessMenu (Long_t parm1, Long_t parm2);
   };
//@}

}
#endif // _LIGO_TLGMAINMENU_H
