/* Version $Id: TLGMainWindow.hh 7027 2014-03-13 18:23:27Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGMAINWINDOW_H
#define _LIGO_TLGMAINWINDOW_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGMainWindow						*/
/*                                                         		*/
/* Module Description: Main window of diagnostics viewer		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 23Nov99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGMainWindow.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGMenu.h>
#include <TGTab.h>
#include <TGButton.h>
#include <TGStatusBar.h>
#include <TTimer.h>
#include <memory>
#include <deque>
#include <string>
#include "TLGFrame.hh"
#include "TLGMainMenu.hh"

   class PlotSet;

namespace calibration {
   class Table;
}
namespace ligogui {

   class TLGMultiPad;
   class ActionPlots;
   class TLGPrintParam;
   struct ExportOption_t;
   typedef ExportOption_t ImportOption_t;
   struct ReferenceTraceList_t;
   struct MathTable_t;
   class OptionArray;
   class TLGSaver;
   class TLGRestorer;
   class ExtraXML;


/** @name TLGMainWindow
    This header exports the main window for the diagnostics viewer.
   
    @memo Main window of the diagnostics viewer
    @author Written December 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

/** @name Constants
    Constants for the main diagnostics viewer window.
   
    @memo Main diagnostics window constants
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

   /// maximum number of buttons
   const int kButtonMax = 10;
   /// maximum number of multipad windows which have stored plot options
   const int kPlotMax = 11;
   /// maximum number of stored plot options per multipad window
   const int kPlotOptionsMax = 100;


   /// Flag for clear button
   const int kButtonClear = 0x01;
   /// Flag for update button
   const int kButtonUpdate = 0x02;
   /// Flag for run button
   const int kButtonRun = 0x04;
   /// Flag for pause button
   const int kButtonStart = 0x08;
   /// Flag for pause button
   const int kButtonPause = 0x10;
   /// Flag for resume button
   const int kButtonResume = 0x20;
   /// Flag for abort button
   const int kButtonAbort = 0x40;
   /// Flag for stop button
   const int kButtonStop = 0x80;
   /// Flag for exit button
   const int kButtonExit = 0x100;

  /// Widget Id of main frame
   const int kMainFrameId = 50;

   /// Button command idendifiers
   enum EButtonCommandIdentifiers {
   /// Update button
   kB_CLEAR = 0,
   /// Update button
   kB_UPDATE = 1,
   /// Run button
   kB_RUN = 2,
   /// Run button
   kB_START = 3,
   /// Pause button
   kB_PAUSE = 4,
   /// Resume button
   kB_RESUME = 5,
   /// Abort button
   kB_ABORT = 6,
   /// Stop button
   kB_STOP = 7,
   /// Exit button
   kB_EXIT = 8
   };


//@}

   class TLGMainWindow;

/** Notfication message.
    If an asynchronous task wants to have access to the user interface,
    it has to send a notfication message. The main user interface 
    message loop will periodically check if new messages have arrived,
    and if yes, call the Process method of the message object. If
    a task wants to send a notification message, it has to use the
    MT-safe SendNotification method of the Diagnostics viewer main 
    window object.

    @memo Notfication message
    @author Written August 2000 by Daniel Sigg/
    @version 1.0
 ************************************************************************/
   class NotificationMessage {
   public:
      /// Default constructor
      NotificationMessage () {
      }
      /// Desctructor
      virtual ~NotificationMessage () {
      }
      /// Process message
      virtual void Process (TLGMainWindow& win) {
      };
   };


   class MsgQueueMutex;


/** Diagnostics viewer main window. This is the main window of the 
    diagnostics viewer. It implements a menu, a status bar, a series of 
    buttons and the main window in the center center.
   
    @memo Diagnostics viewer main window
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMainWindow : public TLGMainFrame, public TLGMainMenu {
      friend class ActionPlots;
   
   protected:
      /// message queue type
      typedef std::deque<NotificationMessage*> messagequeue;
   
      /// Plot set
      PlotSet* 		fPlot;
      /// Window title
      TString		fWindowTitle;
      /// File name
      TString		fFilename;
      /// File type
      Int_t		fFiletype;
      /// Heartbeat timer
      TTimer*		fHeartbeat;
      /// Skip Heartbeats
      Int_t		fSkipHeartbeats;
      /// X11 watchdog timer
      TTimer*		fX11Watchdog;
   
      /** List of stored options
          The first index describes the multipad for which the 
          plot options are stored, starting with 1. Index 0
          is reserved for the 'stored' plot options.
          The second index describes the individual options asscoiated
          with the pads of a mulipad window. */
      OptionArray*	fStoreOptions;
      /// Multipad layouts 
      Int_t		fMPadLayout[kPlotMax];
      /// Printing defaults
      TLGPrintParam* fPrintDef;
      /// Import option defaults
      ImportOption_t* 	fImportDef;
      /// Export option defaults
      ExportOption_t* 	fExportDef;
      /// List of reference traces
      ReferenceTraceList_t* fRefTraces;
      /// Math function table
      MathTable_t* 	fMathTable;
      /// Calibration table
      calibration::Table* fCalTable;
      /// action plot
      ActionPlots*	fAction;
      /// Unknown xml objects storage
      ExtraXML*		fXMLObjects;
   
      /// message queue mutex
      MsgQueueMutex*	fMsgQueueLock;
      /// Notification message queue
      messagequeue	fMsgQueue;
   
      /// main window frame
      TGCompositeFrame*	fMainWindowFrame;
      /// main window layout hints
      TGLayoutHints*	fMainWindowLayout;
      /// Multipad plot window
      TLGMultiPad*	fMPad;
   
      /// Array of buttons
      TGButton*		fButtons[kButtonMax];
      /// button frame
      TGCompositeFrame*	fButtonFrame;
      /// button layout hints
      TGLayoutHints*	fButtonLayout;
      /// button frame layout hints
      TGLayoutHints*	fButtonFrameLayout;
      /// Button foreground context
      static GContext_t fgButtonGC;
      /// Button font
      static FontStruct_t fgButtonFont;
   
      /// Status bar
      TGStatusBar*	fStatus;
      /// Status bar layout hint
      TGLayoutHints*	fStatusBarLayout;
      /// Busy cursor
      static Cursor_t	fWaitCursor;
   
      /** Get the status bar. Can be 0 when no status bar is required.
          This function is called by the constructor and the returned
          status bar will be freed by the desctructor.
          @memo Get status bar.
          @return void
       ******************************************************************/
      virtual TGStatusBar* GetStatusBar();
   
      /** Add the main window. By default just adds a multipad, but can
          be overriden to implement a more complicated main window such
          as a tab where one of the tabs is the plot window. This 
          function should return a pointer to the multipad window.
          This function is called by the constructor and the returned
          multipad will be freed by the desctructor.
          @memo Add main window.
          @param p Parent window
          @param mainLayout Main window layout hints
          @param plotset Set of plot descriptors
          @param padnum Number of plot pads
          @return multipad
       ******************************************************************/
      virtual TLGMultiPad* AddMainWindow (TGCompositeFrame* p,
                        TGLayoutHints* mainLayout,
                        PlotSet& plotset, int padnum = 2);
      /** Add buttons to the button array. The buttons should be
          constructed with the specified button context and font.
          Also buttons should be added to the parent window with the
          supplied button layout hints.
          This function is called by the constructor and the returned
          multipad will be freed by the desctructor.
          @memo Add main window.
          @param p Parent window
          @param btns array to store button pointers
          @param max Maximum size of button array
          @param btnLayout Button layout hints
          @param btnGC Button foreground graphics context
          @param btnFont Button font
          @return Number of buttons added
       ******************************************************************/
      virtual int AddButtons (TGCompositeFrame* p, TGButton* btns[], 
                        int max, TGLayoutHints* btnLayout,
                        GContext_t btnGC = fgButtonGC, 
                        FontStruct_t btnFont = fgButtonFont);
      /** Add standard buttons to the button array. A flag specifies
          a set of kButtonXXX flags: kButtonUpdate, kButtonRun,
   	  kButtonPause, kButtonResume, kButtonAbort, kButtonStop
          and kButtonExit. This function can be called from the 
          AddButton function above to add a standard set of buttons.
          @memo Add main window.
          @param p Parent window
          @param btns array to store button pointers
          @param max Maximum size of button array
          @param btnGC Button foreground graphics context
          @param btnFont Button font
          @return Number of buttons added
       ******************************************************************/
      virtual int AddStdButtons (int flag, TGCompositeFrame* p, 
                        TGButton* btns[], int max, 
                        TGLayoutHints* btnLayout,
                        GContext_t btnGC = fgButtonGC, 
                        FontStruct_t btnFont = fgButtonFont);
      /** This function is called before the object gets destroyed.
          It can be used to cleanup before the application is terminated.
          @return True if continue with shutdown
       ******************************************************************/
      virtual Bool_t Shutdown();
   
      /** This function is called after a File|New command or upon
          startup.
          @return True if continue with shutdown
       ******************************************************************/
      virtual void InitSettings() {
      }
   
      /** Returns an XML restore object. This function is called 
          by GetRestorer, if the file type is XML.
          @param filename Name of file to restore
          @param restoreflag restore flag
          @param error string to hold error message
          @param extra stream for holding unrecognized xml objects
          @return Restorer or 0 on error
       ******************************************************************/
      virtual TLGRestorer* GetXMLRestorer (const char* filename,
                        ESaveRestoreFlag restoreflag, 
                        TString& error, std::ostream* extra = 0);
      /** Returns an XML save object. This function is called 
          by GetSaver, if the file type is XML.
          @param filename Name of file to restore
          @param restoreflag restore flag
          @param error string to hold error message
          @param extra stream for holding additional xml objects
          @return Saver or 0 on error
       ******************************************************************/
      virtual TLGSaver* GetXMLSaver (const char* filename,
                        ESaveRestoreFlag restoreflag, 
                        TString& error, std::string* extra = 0);
   
      /** Returns the restore help object which corresponds to the 
          specified file type.
          @param filetype File type
          @param filename Name of file to restore
          @param restoreflag restore flag
          @param error string to hold error message
          @return Restorer or 0 on error
       ******************************************************************/
      virtual TLGRestorer* GetRestorer (Int_t filetype, 
                        const char* filename,
                        ESaveRestoreFlag restoreflag, TString& error);
      /** Returns the save help object which corresponds to the 
          specified file type.
          @param filetype File type
          @param filename Name of file to save
          @param saveflag save flag
          @param error string to hold error message
          @return Saver or 0 on error
       ******************************************************************/
      virtual TLGSaver* GetSaver (Int_t filetype, const char* filename, 
                        ESaveRestoreFlag saveflag, TString& error);
   
   public:
      /** Constructs a main diagnostics viewer window.
          @memo Constructor
          @param p Parent window
          @param title Window title
       ******************************************************************/
      TLGMainWindow (const TGWindow *p, const char* title);
   
      /** Setup of the main diagnostics viewer window. This method MUST
          be called exactly ONCE after constructing a new TLGMainWindow 
          object to properly initialize the diagnostics viewer. It will 
          add menues, buttons, status bar and main window. It will also 
          load data from file if a filename is supplied.
          @memo Setup method
          @param filename Name of file to be loaded
          @param padnum Number of initial plot pads
          @return true if successful
       ******************************************************************/
      virtual Bool_t Setup (const char* filename = 0, Int_t padnum = 2);

      /** Setup of main diagnostics viewer window.  Same as Setup() 
          above, but this allows the main window width and height to
	  be specified to something other than the default 1100, 870.
       ******************************************************************/
      virtual Bool_t SetupWH (int w = 1100, int h = 870, const char* filename = 0, Int_t padnum = 2);
   
      /** Destructs the main diagnostics window.
          @memo Destructor.
       ******************************************************************/
      virtual ~TLGMainWindow();
   
      /** Restore from file function. This function is called by the 
          FileOpen function.
          @memo Restore from file method
          @param filetype Format of restore file
          @param filename Name of file to restore
          @param error String to hold error message
          @return true if successful
       ******************************************************************/
      virtual Bool_t RestoreFromFile (Int_t filetype, 
                        const char* filename, TString& error);
      /** Save to file function. This function is called by the 
          FileSave and FileSaveAs functions.
          @memo Save to file method
          @param filetype Format of save file
          @param filename Name of file to save
          @param error String to hold error message
          @return true if successful
       ******************************************************************/
      virtual Bool_t SaveToFile (int filetype, const char* filename, 
                        TString& error);
   
      /** Close the main window and exit the program.
          @memo Close method
          @return void
       ******************************************************************/
      virtual void CloseWindow();
   
      /** File new function.
          @memo File new method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileNew();
      /** File open function.
          @memo File open method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileOpen();
      /** File save function.
          @memo File save method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileSave();
      /** File save as function.
          @memo File save as method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileSaveAs();
      /** File export function.
          @memo File export method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileExport();
      /** File import function.
          @memo File import method
          @return true if successful
       ******************************************************************/
      virtual Bool_t FileImport();
   
      /** Get the options from the plot pads.
          @memo Get plot option method
          @return true if successful
       ******************************************************************/
      virtual Bool_t GetPlotSettings ();
      /** Set the options of the plot pads.
          @memo Set plot option method
          @return true if successful
       ******************************************************************/
      virtual Bool_t SetPlotSettings ();
   
      /** Show the default plot for the current measurement type.
          @memo Show default plot method.
   	  @param updatePads if true update plot pads
   	  @param mpads pad which needs default settings2
   	  @param hint Try to setup specified measurement type
          @return void
       ******************************************************************/
      virtual Bool_t ShowDefaultPlot (Bool_t updatePads = kTRUE,
                        TLGMultiPad* mpads = 0, Int_t hint = -1);
   
      /** Remove results from plot set. If askfirst is true a popup
          dialog box will ask the user and only change clear results
          if the user confirms.
          @memo Remove results method.
          @param askfirst Ask the user before removing
          @param all clear all traces including references
          @return true if results were removed
       ******************************************************************/
      virtual Bool_t ClearResults (Bool_t askfirst = kFALSE, 
                        Bool_t all = kTRUE);
   
      /** Shows the plots with the settings which were previously saved.
          This function will create plot windows as necessary.
          @memo Show plots.
          @return true if successful
       ******************************************************************/
      virtual Bool_t ShowPlots ();
   
      /** Set status message. This method will set the message string in
          status bar.
          @memo Set state message method.
          @param msg message string
       ******************************************************************/
      virtual void SetStatusMsg (const char* msg);
   
      /** Handles the clear button.
          @memo Clear button method.
       ******************************************************************/
      virtual Bool_t ButtonClear ();
      /** Handles the update button.
          @memo Update button method.
       ******************************************************************/
      virtual Bool_t ButtonUpdate ();
      /** Handles the run button.
          @memo Run button method.
       ******************************************************************/
      virtual Bool_t ButtonRun ();
      /** Handles the start button.
          @memo Start button method.
       ******************************************************************/
      virtual Bool_t ButtonStart ();
      /** Handles the pause button.
          @memo Pause button method.
       ******************************************************************/
      virtual Bool_t ButtonPause ();
      /** Handles the resume button.
          @memo Resume button method.
       ******************************************************************/
      virtual Bool_t ButtonResume ();
      /** Handles the abort button.
          @memo Abort button method.
       ******************************************************************/
      virtual Bool_t ButtonAbort ();
      /** Handles the stop button.
          @memo Stop button method.
       ******************************************************************/
      virtual Bool_t ButtonStop ();
      /** Handles the exit button.
          @memo Exit button method.
       ******************************************************************/
      virtual Bool_t ButtonExit ();
   
      /** Process GUI messages.
          @memo Process message method.
          @param msg Message id
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
      /** Process button messages.
          @memo Process button method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessButton (Long_t parm1, Long_t);
      /** Send a notification message. The message will be adopted.
          @memo Send a notification message.
          @param msg Notification message
          @return true if successful
       ******************************************************************/
      virtual Bool_t SendNotification (NotificationMessage* msg);
   
      /** Heartbeat timer handler.
          @memo Heartbeat method.
          @param timer Timer
          @return true if successful
       ******************************************************************/
      virtual Bool_t HandleTimer (TTimer* timer);
   };
//@}

}
#endif // _LIGO_TLGMAINWINDOW_H
