/* version $Id: TLGCalDlg.cc 7193 2014-10-10 17:25:24Z james.batch@LIGO.ORG $ */
#include "tconv.h"
#include "Time.hh"
#include "TLGCalDlg.hh"
#include "Calibrations.hh"
#include "TLGEntry.hh"
#include "TLGTextEditor.hh"
#include "DataDesc.hh"
#include "PlotSet.hh"
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <RVersion.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TMath.h>
#include <TVirtualX.h>
#include <iostream>
#include <cmath>

static int my_debug = 0 ;

namespace ligogui {
   using namespace std;


   static const char* const gCalibrationTypes[] = { 
   "Calibration", "*.cal",
   "XML", "*.xml",
   "ASCII", "*.txt",
   "All files", "*",
   0, 0 };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Show calibration table edit dialog box                               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t CalibrationTableDlg (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, calibration::Table& cal)
   {
      Bool_t ret = kFALSE;
      new TLGCalibrationDialog (p, main, pset, cal, ret);
      if (ret) {
         for (PlotSet::iterator i = pset.begin(); i != pset.end(); i++) {
            cal.AddUnits (i->Cal());
         }
         pset.Update();
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Show calibration table import box                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t CalibrationTableImport (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, calibration::Table& cal)
   {
      // ask for filename: file open dialog
      TGFileInfo	info;
      info.fFilename = 0;
      info.fIniDir = 0;
   
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gCalibrationTypes);
   #else
      info.fFileTypes = const_cast<char**>(gCalibrationTypes);
   #endif
   #if 1
      info.fFileTypeIdx = 0 ; //Point to .cal type
      {
	 new TLGFileDialog(main, &info, kFDOpen) ;
      }
      if (!info.fFilename)
   #else
      TString defext = ".cal";
      if (!TLGFileDialog (main, info, kFDOpen, defext))
   #endif
      {
      #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
         delete [] info.fFilename;
      #endif
         return kFALSE;
      }
      // now import to file
      Bool_t ret = cal.Import (info.fFilename);
   #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
      delete [] info.fFilename;
   #endif
      // error message if failed
      if (!ret) {
         TString msg = "Import of calibration records failed.";
         Int_t ret;
         new TGMsgBox (p, main, "Error", msg, kMBIconStop, 
                      kMBOk, &ret);
      }
      else {
         for (PlotSet::iterator i = pset.begin(); i != pset.end(); i++) {
            cal.AddUnits (i->Cal());
         }
         pset.Update();
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Show calibration table export box                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t CalibrationTableExport (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, calibration::Table& cal)
   {
      if (cal.Len() <= 0) {
         TString msg = "Nothing to export.";
         Int_t ret;
         new TGMsgBox (p, main, "Warning", msg, kMBIconExclamation, 
                      kMBOk, &ret);
         return kFALSE;
      }
      // ask for filename: file save as dialog
      TGFileInfo	info;
      info.fFilename = 0;
      info.fIniDir = 0;
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gCalibrationTypes);
   #else
      info.fFileTypes = const_cast<char**>(gCalibrationTypes);
   #endif
   #if 1
      info.fFileTypeIdx = 0 ; // Point to .cal
      {
	 new TLGFileDialog(main, &info, kFDSave) ;
      }
      if (!info.fFilename)
   #else
      TString defext = ".cal";
      if (!TLGFileDialog (main, info, kFDSave, defext))
   #endif
      {
      #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
         delete [] info.fFilename;
      #endif
         return kFALSE;
      }
      // now export to file
      Bool_t ret = cal.Export (info.fFilename);
   #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
      delete [] info.fFilename;
   #endif
      // error message if failed
      if (!ret) {
         TString msg = "Export of calibration records failed.";
         Int_t ret;
         new TGMsgBox (p, main, "Error", msg, kMBIconStop, 
                      kMBOk, &ret);
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ChannelNameCmp			                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool TLGCalibrationDialog::ChannelNameCmp::operator() (
                     const std::string& c1, const std::string& c2) const
   {
      return strcasecmp (c1.c_str(), c2.c_str()) < 0;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Calibration edit new dialog	                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGCalibrationNewDialog : public TLGTransientFrame {
   protected:
      /// Channel, ref, unit
      TString*		fCRU[3];
      /// Time
      Time*		fTime;
      /// Return value
      Bool_t*		fOk;
      /// lines
      TGCompositeFrame* fF[5];
      /// labels
      TGLabel*		fLabel[4];
      /// entry fields
      TLGTextEntry*	fText[3];
      /// numerical entry fields for time
      TLGNumericControlBox*	fNum[2];
      /// buttons
      TGButton*		fButton[3];
      /// layout hints
      TGLayoutHints*	fL[4];
   
   public:
      TLGCalibrationNewDialog (const TGWindow *p, const TGWindow *main,
                        TString& chn, TString& ref, TString& unit,
                        Time& time, Bool_t& ret);
      virtual ~TLGCalibrationNewDialog ();
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };

//______________________________________________________________________________
   TLGCalibrationNewDialog::TLGCalibrationNewDialog (const TGWindow *p, 
                     const TGWindow *main, TString& chn, TString& ref, 
                     TString& unit, Time& time, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fOk (&ret)
   {
      fCRU[0] = &chn;
      fCRU[1] = &ref;
      fCRU[2] = &unit;
      fTime = &time;
   
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 2, 2, 2, 2);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsCenterY, 2, 2, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 6, 6, 12, 4);
      // entry lines
      const char* const label[4] = 
         {"Channel: ", "Reference: ", "Unit: ", "Time:  "};
      for (int i = 0; i < 4; i++) {
         fF[i] = new TGHorizontalFrame (this, 10, 10);
         AddFrame (fF[i], fL[0]);
         fLabel[i] = new TGLabel (fF[i], label[i]);
         fF[i]->AddFrame (fLabel[i], fL[1]);
         if (i < 3) {
            fText[i] = new TLGTextEntry (fF[i], *fCRU[i], 3+i);
            fText[i]->Associate (this);
            if (i == 0) fText[i]->SetWidth (450);
            fF[i]->AddFrame (fText[i], i == 0 ? fL[1] : fL[2]);
         }
         else {
            fNum[0] = new TLGNumericControlBox (fF[i], 0, 12, 3+i,
                                 kNESDayMYear);
            fNum[0]->Associate (this);
            fF[i]->AddFrame (fNum[0], fL[2]);
            fNum[1] = new TLGNumericControlBox (fF[i], 0, 10, 4+i,
                                 kNESHourMinSec);
            fNum[1]->Associate (this);
            utc_t utc;
            TAItoUTC (time.getS(), &utc);
            utc.tm_year += 1900;
            utc.tm_mon += 1;
            fNum[0]->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
            fNum[1]->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
            fF[i]->AddFrame (fNum[1], fL[2]);
         }
      }
      // buttons
      fButton[2] = new TGTextButton (fF[3], new TGHotString ("Now"), 2);
      fButton[2]->Associate (this);
      fF[3]->AddFrame (fButton[2], fL[2]);
      fF[4] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[4], fL[0]);
      fButton[0] = new TGTextButton (fF[4], 
                           new TGHotString ("       &Ok       "), 1);
      fButton[0]->Associate (this);
      fF[4]->AddFrame (fButton[0], fL[3]);
      fButton[1] = new TGTextButton (fF[4], 
                           new TGHotString ("     &Cancel     "), 0);
      fButton[1]->Associate (this);
      fF[4]->AddFrame (fButton[1], fL[3]);
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      SetWindowName ("New calibration record");
      SetIconName ("New Calibration");
      SetClassHints ("CalEditNewDlg", "CalEditNewDlg");
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGCalibrationNewDialog::~TLGCalibrationNewDialog ()
   {
      for (int i = 0; i < 2; i++) {
         delete fNum[i];
      }
      for (int i = 0; i < 3; i++) {
         delete fButton[i];
         delete fText[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fLabel[i];
         delete fL[i];
      }
      for (int i = 0; i < 5; i++) {
         delete fF[i];
      }
   }

//______________________________________________________________________________
   void TLGCalibrationNewDialog::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   TString EliminateBlanks (const TString& text)
   {
      TString s = text;
      while ((s.Length() > 0) && 
            ((s[0] == ' ') || (s[0] == '\t') || (s[0] == '\n'))) {
         s.Remove (0, 1);
      }
      int i;
      while (((i = s.Length()) > 0) && 
            ((s[i-1] == ' ') || (s[i-1] == '\t') || (s[i-1] == '\n'))) {
         s.Remove (i-1, 1);
      }
      return s;
   }

//______________________________________________________________________________
   Bool_t TLGCalibrationNewDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // ok
            case 1:
               {
                  // get channel, ref, unit
                  for (int i = 0; i < 3; i++) {
                     *fCRU[i] = EliminateBlanks (fText[i]->GetText());
                  }
                  // get time
                  utc_t utc;
                  fNum[0]->GetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
                  fNum[1]->GetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
                  if (utc.tm_year < 1990) {
                     *fTime = Time (0, 0);
                  }
                  else {
                     utc.tm_year -= 1900;
                     utc.tm_mon -= 1;
                     *fTime = Time (UTCtoTAI (&utc), 0);
                  }
                  // set return value and quit
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
            // cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // now
            case 2:
               {
                  utc_t utc;
                  TAItoUTC (Now().getS(), &utc);
                  utc.tm_year += 1900;
                  utc.tm_mon += 1;
                  fNum[0]->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
                  fNum[1]->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
                  break;
               }
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Calibration edit dialog  	                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGCalibrationDialog::TLGCalibrationDialog (const TGWindow *p, 
                     const TGWindow *main,
                     PlotSet& pset, calibration::Table& cal, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), 
   fPlotList (&pset), fCal (&cal), fSelValid (kFALSE), fOk (&ret), 
   fDirty (kTRUE)
   {
      fSelCal = new calibration::Calibration;
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 4, 4, 4, 4);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsExpandY | kLHintsTop, 0, 0, 0, 0);
      fL[2] = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 4, 4, 2, 2);
      fL[9] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 12, 4, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 12, 12, 4);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsBottom, 0, 12, 0, 4);
      fL[4] = new TGLayoutHints (kLHintsRight | kLHintsExpandX |
                           kLHintsBottom, 4, 4, 12, 8);
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 0);
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 0, 0);
      fL[8] = new TGLayoutHints (kLHintsLeft | kLHintsExpandY |
                           kLHintsTop, 0, 0, 0, 0);
      fL[10] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, 4, 4, 4);
      fL[11] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsExpandY | kLHintsTop, 4, 4, 12, 0);
      fL[12] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsCenterY, 4, 4, 4, 4);
      fL[13] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsCenterY, 4, 4, 8, 8);
      fL[14] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop | kLHintsExpandY, 4, 4, 8, 8);
      // group frames
      fFTop = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fFTop, fL[5]);
      fFCoeff = new TGVerticalFrame (this, 10, 10);
      AddFrame (fFCoeff, fL[7]);
      fFButton = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fFButton, fL[7]);
      fFChn = new TGVerticalFrame (fFTop, 10, 10);
      fFTop->AddFrame (fFChn, fL[5]);
      fFRefUnit = new TGVerticalFrame (fFTop, 10, 10);
      fFTop->AddFrame (fFRefUnit, fL[8]);
      // channel selection
      fChnSel = new TGListBox (fFChn, kCalEditChn);
      fChnSel->Associate (this);
      fChnSel->Resize (450, 160);
      fFChn->AddFrame (fChnSel, fL[0]);
      // reference point/unit/time selection
      fF4 = new TGHorizontalFrame (fFRefUnit, 10, 10);
      fFRefUnit->AddFrame (fF4, fL[3]);
      fF5 = new TGHorizontalFrame (fFRefUnit, 10, 10);
      fFRefUnit->AddFrame (fF5, fL[6]);
      fF2 = new TGHorizontalFrame (fFRefUnit, 10, 10);
      fFRefUnit->AddFrame (fF2, fL[6]);
      fF1 = new TGHorizontalFrame (fFRefUnit, 10, 10);
      fFRefUnit->AddFrame (fF1, fL[6]);
      fF3 = new TGHorizontalFrame (fFRefUnit, 10, 10);
      fFRefUnit->AddFrame (fF3, fL[6]);
      fRefLabel = new TGLabel (fF1, "Ref: ");
      fF1->AddFrame (fRefLabel, fL[9]);
      fUnitLabel = new TGLabel (fF2, "Unit: ");
      fF2->AddFrame (fUnitLabel, fL[9]);
      fTimeLabel = new TGLabel (fF5, "Time: ");
      fF5->AddFrame (fTimeLabel, fL[9]);
      fEnable = new TGCheckButton (fF4, "Enable", kCalEditEnable);
      fEnable->Associate (this);
      fF4->AddFrame (fEnable, fL[9]);
      fRef = new TGComboBox (fF1, kCalEditRef);
      fRef->Associate (this);
      fRef->Resize (200, 23);
      fF1->AddFrame (fRef, fL[2]);
      fUnit = new TGComboBox (fF2, kCalEditUnit);
      fUnit->Associate (this);
      fUnit->Resize (200, 23);
      fF2->AddFrame (fUnit, fL[2]);
      fTime = new TGComboBox (fF5, kCalEditTime);
      fTime->Associate (this);
      fTime->Resize (200, 23);
      fF5->AddFrame (fTime, fL[2]);
      fDefault = new TGCheckButton (fF3, "Default", kCalEditDefault);
      fDefault->Associate (this);
      fF3->AddFrame (fDefault, fL[9]);
      // calibration edit tab
      fTab = new TGTab (fFCoeff, 240, 200);
      fTabTime = fTab->AddTab (" Time Domain ");
      fTabFreq = fTab->AddTab ("  Pole/Zeros  ");
      fTabTrans = fTab->AddTab (" Trans. Func. ");
      fTabDef  = fTab->AddTab (" Preferences ");
      fTabComment  = fTab->AddTab ("  Comments  ");
      fFCoeff->AddFrame (fTab, fL[11]);
      fFT[0] = new TGHorizontalFrame (fTabTime, 10, 10);
      fTabTime->AddFrame (fFT[0], fL[7]);
      fFT[1] = new TGHorizontalFrame (fTabTime, 10, 10);
      fTabTime->AddFrame (fFT[1], fL[7]);
      fFT[2] = new TGHorizontalFrame (fTabTime, 10, 10);
      fTabTime->AddFrame (fFT[2], fL[7]);
      fFT[3] = new TGHorizontalFrame (fTabFreq, 10, 10);
      fTabFreq->AddFrame (fFT[3], fL[7]);
      fFT[4] = new TGHorizontalFrame (fTabFreq, 10, 10);
      fTabFreq->AddFrame (fFT[4], fL[7]);
      fFT[5] = new TGHorizontalFrame (fTabFreq, 10, 10);
      fTabFreq->AddFrame (fFT[5], fL[7]);
      fFT[6] = new TGHorizontalFrame (fTabFreq, 10, 10);
      fTabFreq->AddFrame (fFT[6], fL[7]);
      fFT[7] = new TGHorizontalFrame (fTabFreq, 10, 10);
      fTabFreq->AddFrame (fFT[7], fL[7]);
      fFT[10] = new TGHorizontalFrame (fTabTrans, 10, 10);
      fTabTrans->AddFrame (fFT[10], fL[7]);
      fFT[11] = new TGHorizontalFrame (fTabTrans, 10, 10);
      fTabTrans->AddFrame (fFT[11], fL[7]);
      fFT[8] = new TGHorizontalFrame (fTabDef, 10, 10);
      fTabDef->AddFrame (fFT[8], fL[7]);
      fFT[9] = new TGHorizontalFrame (fTabDef, 10, 10);
      fTabDef->AddFrame (fFT[9], fL[7]);
      fConvSel = new TGCheckButton (fFT[0], "Conversion: ", 
                           kCalEditConvSel);
      fConvSel->Associate (this);
      fFT[0]->AddFrame (fConvSel, fL[10]);
      fOffsSel = new TGCheckButton (fFT[1], "Offset:         ", 
                           kCalEditOffsSel);
      fOffsSel->Associate (this);
      fFT[1]->AddFrame (fOffsSel, fL[10]);
      fDelaySel = new TGCheckButton (fFT[2], "Time delay:  ", 
                           kCalEditDelaySel);
      fDelaySel->Associate (this);
      fFT[2]->AddFrame (fDelaySel, fL[10]);
      fPoleZeroSel = new TGCheckButton (fFT[3], "Pole-zero", 
                           kCalEditPoleZeroSel);
      fPoleZeroSel->Associate (this);
      fFT[3]->AddFrame (fPoleZeroSel, fL[10]);
      fConv = new TLGNumericControlBox (fFT[0], 0, 16, kCalEditConv);
      fConv->Associate (this);
      fFT[0]->AddFrame (fConv, fL[10]);
      fOffs = new TLGNumericControlBox (fFT[1], 0, 16, kCalEditOffs);
      fOffs->Associate (this);
      fFT[1]->AddFrame (fOffs, fL[10]);
      fDelay = new TLGNumericControlBox (fFT[2], 0, 16, kCalEditDelay);
      fDelay->Associate (this);
      fFT[2]->AddFrame (fDelay, fL[10]);
      fGainLabel = new TGLabel (fFT[4], "  Gain:   ");
      fFT[4]->AddFrame (fGainLabel, fL[9]);
      fPoleLabel = new TGLabel (fFT[5], "  Poles: ");
      fFT[5]->AddFrame (fPoleLabel, fL[9]);
      fZeroLabel = new TGLabel (fFT[6], "  Zeros: ");
      fFT[6]->AddFrame (fZeroLabel, fL[9]);
      fGain = new TLGNumericControlBox (fFT[4], 0, 16, kCalEditGain);
      fGain->Associate (this);
      fFT[4]->AddFrame (fGain, fL[10]);
      fPoles = new TLGTextEntry (fFT[5], "", kCalEditPoles);
      fPoles->SetMaxLength (4*1024);
      fPoles->Associate (this);
      fFT[5]->AddFrame (fPoles, fL[12]);
      fZeros = new TLGTextEntry (fFT[6], "", kCalEditZeros);
      fZeros->Associate (this);
      fZeros->SetMaxLength (4*1024);
      fFT[6]->AddFrame (fZeros, fL[12]);
      fHelpLabel = new TGLabel (fFT[7], 
                           "  Example: 0.3 5.1, 1e-1  for poles/zeros at "
                           "0.3+i*5.1, 0.3-i*5.1 and 0.1 Hz");
      fFT[7]->AddFrame (fHelpLabel, fL[13]);
   
      fTransSel = new TGCheckButton (fFT[10], "Transfer function", 
                           kCalEditTransSel);
      fTransSel->Associate (this);
      fFT[10]->AddFrame (fTransSel, fL[10]);
      fFunctionLabel = new TGLabel (fFT[11], "  Function: ");
      fFT[11]->AddFrame (fFunctionLabel, fL[9]);
      fTransEditButton = new TGTextButton (fFT[11], 
                           new TGHotString (" Edit... "), 
                           kCalEditTransEdit);
      fTransEditButton->Associate (this);
      fFT[11]->AddFrame (fTransEditButton, fL[9]);
   
      fMagSel = new TGCheckButton (fFT[8], "Preferred Magnitude:  ", 
                           kCalEditMagSel);
      fMagSel->Associate (this);
      fFT[8]->AddFrame (fMagSel, fL[10]);
      fDSel = new TGCheckButton (fFT[9], "Preferred Derivative: ", 
                           kCalEditDSel);
      fDSel->Associate (this);
      fFT[9]->AddFrame (fDSel, fL[10]);
      fMag = new TLGNumericControlBox (fFT[8], 0, 4, kCalEditMag, 
                           kNESInteger);
      fMag->Associate (this);
      fFT[8]->AddFrame (fMag, fL[10]);
      fD = new TLGNumericControlBox (fFT[9], 0, 4, kCalEditD,
                           kNESInteger);
      fD->Associate (this);
      fFT[9]->AddFrame (fD, fL[10]);
      fComment = new TDGTextEdit (fTabComment, 10, 10, kCalEditComment);
      fComment->Associate (this);
      fTabComment->AddFrame (fComment, fL[14]);
   
      // setup buttons
      fCancelButton = new TGTextButton (fFButton, 
                           new TGHotString ("     &Cancel     "), 
                           kCalEditCancel);
      fCancelButton->Associate (this);
      fFButton->AddFrame (fCancelButton, fL[4]);
      fOkButton = new TGTextButton (fFButton, 
                           new TGHotString ("       &Ok       "), 
                           kCalEditOk);
      fOkButton->Associate (this);
      fFButton->AddFrame (fOkButton, fL[4]);
      fClearButton = new TGTextButton (fFButton, 
                           new TGHotString ("    &Delete    "), 
                           kCalEditClear);
      fClearButton->Associate (this);
      fFButton->AddFrame (fClearButton, fL[4]);
      fPasteButton = new TGTextButton (fFButton, 
                           new TGHotString ("      Paste      "), 
                           kCalEditPaste);
      fPasteButton->Associate (this);
      fPasteButton->SetEnabled(kFALSE) ; // Disable paste until copy has been pressed.
      fFButton->AddFrame (fPasteButton, fL[4]);
      fCopyButton = new TGTextButton (fFButton, 
                           new TGHotString ("      Copy      "), 
                           kCalEditCopy);
      fCopyButton->Associate (this);
      fCopyButton->SetEnabled(kFALSE) ;	// Initially, copy is disabled until something can be copied.
      fFButton->AddFrame (fCopyButton, fL[4]);
      fSetButton = new TGTextButton (fFButton, 
                           new TGHotString ("      &Set      "), 
                           kCalEditSet);
      fSetButton->Associate (this);
      fFButton->AddFrame (fSetButton, fL[4]);
      fNewButton = new TGTextButton (fFButton, 
                           new TGHotString ("      &New      "), 
                           kCalEditNew);
      fNewButton->Associate (this);
      fFButton->AddFrame (fNewButton, fL[4]);
   
      // initialize parameters
      fCurChannel = "";
      fCurRef = "";
      fCurUnit = "";
      fCurTime = "";
      fCurTimeGPS = Time(0,0);
      BuildChannelList();
      ShowCal (0);
      SetDirty (kFALSE);
      if (fOk) *fOk = kFALSE;
   
      fCopyCal = new calibration::Calibration() ;
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      SetWindowName ("Calibration table");
      SetIconName ("Calibration table");
      SetClassHints ("CalEditDlg", "CalEditDlg");
   
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGCalibrationDialog::~TLGCalibrationDialog ()
   {
      delete fCancelButton;
      delete fOkButton;
      delete fClearButton;
      delete fSetButton;
      delete fNewButton;
      delete fCopyButton ;
      delete fPasteButton ;
      delete fHelpLabel;
      delete fComment;
      delete fD;
      delete fMag;
      delete fDSel;
      delete fMagSel;
      delete fTransEditButton;
      delete fFunctionLabel;
      delete fTransSel;
      delete fZeros;
      delete fPoles;
      delete fGain;
      delete fZeroLabel;
      delete fPoleLabel;
      delete fGainLabel;
      delete fDelay;
      delete fOffs;
      delete fConv;
      delete fPoleZeroSel;
      delete fDelaySel;
      delete fOffsSel;      
      delete fConvSel;
      delete fTab;
      delete fDefault;
      delete fTime;
      delete fUnit;
      delete fRef;
      delete fEnable;
      delete fTimeLabel;
      delete fUnitLabel;
      delete fRefLabel;
      delete fChnSel;
      for (int i = 0; i < 12; i++) {
         delete fFT[i];
      }
      delete fF5;
      delete fF4;
      delete fF3;
      delete fF2;
      delete fF1;
      delete fFButton;
      delete fFCoeff;
      delete fFRefUnit;
      delete fFChn;
      delete fFTop;
      for (int i = 0; i < 15; i++) {
         delete fL[i];
      }
      delete fSelCal;
      if (fCopyCal)
	 delete fCopyCal ;
   }

//______________________________________________________________________________
   void TLGCalibrationDialog::CloseWindow()
   {
      DeleteWindow();
   }

//______________________________________________________________________________
   void TLGCalibrationDialog::SetDirty (Bool_t state)
   {
      if (state != fDirty) {
         fDirty = state;
         fSetButton->SetState (state ? kButtonUp : kButtonDisabled);
      }
   }

//______________________________________________________________________________
   void TLGCalibrationDialog::BuildChannelList ()
   {
      fOldest = Time (0, 0);
      unsigned int	sec;
      unsigned int	nsec;
      for (PlotSet::iterator i = fPlotList->begin(); 
          i != fPlotList->end(); i++) {
         // determine oldest plot descriptor
         if (i->Param().GetStartTime (sec, nsec)) {
            Time cur (sec, 0);
            if ((fOldest == Time (0, 0)) ||
               ((cur != Time (0, 0)) && (cur < fOldest))) {
               fOldest = cur;
            }
         }
         if (i->GetAChannel() &&
            (strchr (i->GetAChannel(), '(') == 0) &&
            (strchr (i->GetAChannel(), '[') == 0) &&
            (strstr (i->GetAChannel(), "_!_") == 0)) {
            fCal->AddChannel (i->GetAChannel());
         }
         if (i->GetBChannel() &&
            (strchr (i->GetBChannel(), '(') == 0) &&
            (strchr (i->GetBChannel(), '[') == 0) &&
            (strstr (i->GetBChannel(), "_!_") == 0)) {
            fCal->AddChannel (i->GetBChannel());
         }
      }
   }

//______________________________________________________________________________
   void TLGCalibrationDialog::ShowCal (Int_t level)
   {
      if (my_debug) cerr << "TLGCalibrationDialog::ShowCal(" << level << ")" << endl ;
      // rebuild channel list
      if (level <= 0) {
	 if (my_debug) cerr << "  Rebuild channel list" << endl ;
         fChnSel->RemoveAll ();
         for (calibration::Table::ChannelList::const_iterator i =
             fCal->GetChannels().begin(); i != fCal->GetChannels().end();
             ++i) {
            fChnSel->AddEntry (i->second.GetName(),
                              i->second.GetUniqueID());
         }
         fChnSel->MapSubwindows();
         fChnSel->Layout();
      }
      // check if channel exists
      const calibration::Table::ChannelItem* c = 
         fCal->FindChannel (fCurChannel);
      if (c == 0) {
	 if (my_debug) cerr << "  Channel " << fCurChannel << " didn't exist" << endl ;
         fChnSel->Select (fChnSel->GetSelected(), kFALSE);
         fCurChannel = "";
         fCurRef = "";
         fCurUnit = "";
         fCurTime = "";
         fCurTimeGPS = Time(0,0);
         fRef->RemoveAll ();
         fUnit->RemoveAll ();
         fTime->RemoveAll ();
         fEnable->SetState (kButtonUp);
         fDefault->SetState (kButtonUp);
         fConvSel->SetState (kButtonUp);
         fOffsSel->SetState (kButtonUp);
         fDelaySel->SetState (kButtonUp);
         fPoleZeroSel->SetState (kButtonUp);
         fTransSel->SetState (kButtonUp);
         fConv->SetNumber (1.0);
         fOffs->SetNumber (1.0);
         fDelay->SetNumber (1.0);
         fGain->SetNumber (1.0);
         fPoles->SetText ("");
         fZeros->SetText ("");
         fSelValid = kFALSE;
         *fSelCal = calibration::Calibration();
         fMag->SetIntNumber (0);
         fD->SetIntNumber (0);
	 fComment->Clear() ;
         fComment->LoadBuffer ("");
         return;
      }
      // set channel
      fChnSel->Select (c->GetUniqueID());
      fEnable->SetState (c->IsEnabled() ? kButtonDown : kButtonUp);
   
      // rebuild reference list
      if (level <= 1) {
	 if (my_debug) cerr << "  Rebuild reference list" << endl ;
         fRef->RemoveAll ();	// Remove all entries from the fRef TGComboBox.
         fRefList.clear();
         int i0 = fCal->FindFirst (fCurChannel);
         if (i0 < 0) {
            fCurRef = "";
         }
         else {
            int i = i0;
            while ((i < fCal->Len()) && (strcasecmp ((*fCal)[i].GetChannel(), fCurChannel) == 0)) {
               fRefList [(*fCal)[i].GetRef()] = i;
               ++i;
            }
            sortedlist::const_iterator obj = fRefList.end();
            for (sortedlist::const_iterator j = fRefList.begin(); j != fRefList.end(); ++j) {
               fRef->AddEntry (j->first.c_str(), j->second);
               if (strcasecmp (j->first.c_str(), fCurRef) == 0) {
                  obj = j;
               }
            }
            if (obj == fRefList.end()) {
               fCurRef = (*fCal)[i0].GetRef();
               obj = fRefList.find ((const char*)fCurRef);
            }
            if (obj == fRefList.end()) {
               fRef->SetTopEntry (new TGTextLBEntry (fRef, new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | kLHintsExpandX));
               fRef->MapSubwindows();
               fCurRef = "";
            }
            else {
               fRef->Select (obj->second);
            }
         }
      }
   
      // rebuild unit list
      if (level <= 2) {
	 if (my_debug) cerr << "  Rebuild unit list" << endl ;
         fUnit->RemoveAll ();	// Remove all entries from the fUnit TGComboBox.
         fUnitList.clear();
         int i0 = fCal->FindFirst (fCurChannel);
         if (i0 < 0) {
            fCurUnit = "";
         }
         else {
            int i = i0;
            Bool_t first = kFALSE;
            while ((i < fCal->Len()) && 
                  (strcasecmp ((*fCal)[i].GetChannel(), fCurChannel) == 0)) {
               if (strcasecmp ((*fCal)[i].GetRef(), fCurRef) == 0) {
                  fUnitList [(*fCal)[i].GetUnit()] = i;
                  if (!first) {
                     i0 = i;
                     first = kTRUE;
                  }
               }
               ++i;
            }
            sortedlist::const_iterator obj = fUnitList.end();
            for (sortedlist::const_iterator j = fUnitList.begin();
                j != fUnitList.end(); ++j) {
               fUnit->AddEntry (j->first.c_str(), j->second);
               if (strcasecmp (j->first.c_str(), fCurUnit) == 0) {
                  obj = j;
               }
            }
            if (obj == fUnitList.end()) {
               fCurUnit = (*fCal)[i0].GetUnit();
               obj = fUnitList.find ((const char*)fCurUnit);
            }
            if (obj == fUnitList.end()) {
               fUnit->SetTopEntry 
                  (new TGTextLBEntry (fUnit, new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                    kLHintsExpandX));
               fUnit->MapSubwindows();
               fCurUnit = "";
            }
            else {
               fUnit->Select (obj->second);
            }
         }
      }
   
      // rebuild time list
      if (level <= 3) {
	 if (my_debug) cerr << "  Rebuild time list" << endl ;
         fTime->RemoveAll ();	// Remove all entries from the fTime TGComboBox.
         fTimeList.clear();
         int i0 = fCal->FindFirst (fCurChannel);
         if (i0 < 0) {
            fCurTime = "";
            fCurTimeGPS = Time (0,0);
         }
         else {
            int i = i0;
            Bool_t first = kFALSE;
            while ((i < fCal->Len()) && 
                  (strcasecmp ((*fCal)[i].GetChannel(), fCurChannel) == 0)) {
               if ((strcasecmp ((*fCal)[i].GetRef(), fCurRef) == 0) &&
                  (strcasecmp ((*fCal)[i].GetUnit(), fCurUnit) == 0)) {
                  char t0[256];
                  TimeStr ((*fCal)[i].GetTime(), t0, "%Y/%m/%d %H:%N:%S");
                  fTimeList [t0] = i;
                  if (!first) {
                     i0 = i;
                     first = kTRUE;
                  }
               }
               ++i;
            }
            sortedlist::const_iterator obj = fTimeList.end();
            for (sortedlist::const_iterator j = fTimeList.begin();
                j != fTimeList.end(); ++j) {
               fTime->AddEntry (j->first.c_str(), j->second);
               if (strcasecmp (j->first.c_str(), fCurTime) == 0) {
                  obj = j;
               }
            }
            if (obj == fTimeList.end()) {
               fCurTimeGPS = (*fCal)[i0].GetTime();
               char t0[256];
               TimeStr (fCurTimeGPS, t0, "%Y/%m/%d %H:%N:%S");
               fCurTime = t0;
               obj = fTimeList.find (t0);
            }
            if (obj == fTimeList.end()) {
               fTime->SetTopEntry 
                  (new TGTextLBEntry (fTime, new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                    kLHintsExpandX));
               fTime->MapSubwindows();
               fCurTime = "";
               fCurTimeGPS = Time (0,0);
            }
            else {
               fTime->Select (obj->second);
            }
         }
      }
   
      // set calibration record info
      if (my_debug) cerr << "  Set calibration record info. Search for Calibration," << endl ;
      if (my_debug) cerr << "    fCal->Search(" << fCurChannel << ", " << fCurRef << ", " << fCurUnit << ", " << fCurTimeGPS << ")" << endl ;
      const calibration::Calibration* cal = 
         fCal->Search (fCurChannel, fCurRef, fCurUnit, fCurTimeGPS);
      if (cal == 0) {
	 if (my_debug) cerr << "      Calibration not found." << endl ;
         fDefault->SetState (kButtonUp);
         fConvSel->SetState (kButtonUp);
         fOffsSel->SetState (kButtonUp);
         fDelaySel->SetState (kButtonUp);
         fPoleZeroSel->SetState (kButtonUp);
         fTransSel->SetState (kButtonUp);
         fConv->SetNumber (1.0);
         fOffs->SetNumber (1.0);
         fDelay->SetNumber (1.0);
         fGain->SetNumber (1.0);
         fPoles->SetText ("");
         fZeros->SetText ("");
         fMag->SetIntNumber (0);
         fD->SetIntNumber (0);
	 fComment->Clear() ;
         fComment->LoadBuffer ("");
         fSelValid = kFALSE;
         *fSelCal = calibration::Calibration();
      }
      else {
	 if (my_debug) cerr << "      Calibration found, reading values" << endl ;
         fDefault->SetState (cal->GetDefault() ? kButtonDown : kButtonUp);
         fConvSel->SetState (cal->GetType() & CALAMPLITUDE ? 
                            kButtonDown : kButtonUp);
         fOffsSel->SetState (cal->GetType() & CALOFFSET ? 
                            kButtonDown : kButtonUp);
         fDelaySel->SetState (cal->GetType() & CALTIMEDELAY ? 
                             kButtonDown : kButtonUp);
         fPoleZeroSel->SetState (cal->GetType() & CALPOLEZERO ? 
                              kButtonDown : kButtonUp);
         fTransSel->SetState (cal->GetType() & CALTRANSFERFUNCTION ? 
                             kButtonDown : kButtonUp);
         fConv->SetNumber (cal->GetConversion());
         fOffs->SetNumber (cal->GetOffset());
         fDelay->SetNumber (cal->GetTimeDelay());
         double gain; int pnum; int znum; const float* pzs = 0;
         if (cal->GetPoleZeros (gain, pnum, znum, pzs)) {
            fGain->SetNumber (gain);
            char buf[1024];
            for (int i = 0; i < 2; i++) {
               int indx = 0;
               buf[0] = 0;
               for (int j = 0; j < (i == 0 ? pnum : znum); j++) {
                  if (indx > 900) {
                     break;
                  }
                  if (pzs[4*j+2*i+1] == 0) {
                     sprintf (buf + indx, "%s%g", indx == 0 ? "" : ", ",
                             pzs[4*j+2*i]);
                  }
                  else if (pzs[4*j+2*i+1] > 0) {
                     sprintf (buf + indx, "%s%g %g", indx == 0 ? "" : ", ",
                             pzs[4*j+2*i], pzs[4*j+2*i+1]);
                  }
                  indx += strlen (buf + indx);
               }
               if (i == 0) {
                  fPoles->SetText (buf);
               }
               else {
                  fZeros->SetText (buf);
               }
            }
         }
         else {
            fGain->SetNumber (1.0);
            fPoles->SetText ("");
            fZeros->SetText ("");
         }
         fMagSel->SetState (kButtonDown);
         fMag->SetIntNumber (cal->GetPreferredMag());
         fDSel->SetState (kButtonDown);
         fD->SetIntNumber (cal->GetPreferredD());
         fComment->LoadBuffer (cal->GetComment() ? cal->GetComment() : "");
         *fSelCal = *cal;
         fSelValid = kTRUE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGCalibrationDialog::EditTransferFunction() 
   {
      if (!fSelValid) {
         TString msg = "No calibration record selected.\n";
         Int_t ret;
         new TGMsgBox (gClient->GetRoot(), this, 
                      "Warning", msg, kMBIconExclamation, 
                      kMBOk, &ret);
         return kFALSE;
      }
      // Fill text buffer
      TGText text;
      text.InsLine (0, "#Format: frequency (Hz)  ratio (dB)  phase (deg)");
      const float* trans = 0;
      int len = fSelCal->GetTransferFunction (trans);
      for (int i = 0; i < len; ++i) {
         char buf[1024];
         sprintf (buf, "%12g %12g %12g", trans[3*i], 
                 20*log10(fabs (trans[3*i+1])), 180/TMath::Pi()*trans[3*i+2]);
         text.InsLine (i + 1, buf);
      }
      Bool_t ret;
      // Call dialog box
      new ligogui::TLGTextEditor (gClient->GetRoot(), this, 
                           "Transfer Function Editor", 80, 60, text, ret);
      if (ret) {
         // Read values
         int max = text.RowCount();
         float* trans = new float [3*max];
         int len = 0;
         for (int i = 0; i < max; ++i) {
            char* p = text.GetLine (TGLongPosition (0, i), text.ColCount());
            if (p && (strchr (p, '#') == 0)) {
               trans[3*len] = 0;
               trans[3*len+1] = 0;
               trans[3*len+2] = 0;
               char* pp = p;
               while (isspace (*pp)) ++pp;
               bool err = !*pp; // must be non-empty
               trans[3*len] = strtod (pp, &pp);
               while (isspace (*pp)) ++pp;
               if (!*pp) err = true; // must be non-empty
               trans[3*len+1] = strtod (pp, &pp);
               while (isspace (*pp)) ++pp;
               if (!*pp) err = true; // must be non-empty
               trans[3*len+2] = strtod (pp, &pp);
               while (isspace (*pp)) ++pp;
               if (*pp) err = true; // must be empty
               if (!err) ++len;
               // sscanf (p, "%g%g%g", trans+3*len, trans+3*len+1, trans+3*len+2);
               // ++len;
            }
            delete [] p;
         }
         for (int i = 0; i < len; ++i) {
            trans[3*i+1] = pow (10, trans[3*i+1]/20.);
            trans[3*i+2] *= TMath::Pi()/180.;
         }
         fSelCal->SetTransferFunction (trans, len);
         delete [] trans;
         fTransSel->SetState (len > 0 ? kButtonDown : kButtonUp);
         SetDirty();
      }
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGCalibrationDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Edit transfer function
            case kCalEditTransEdit:
               {
                  EditTransferFunction();
                  break;
               }
            // Ok
            case kCalEditOk:
               {
                  if (fSelValid && IsDirty()) {
                     TString msg = "Do you want to keep the current changes?\n";
                     Int_t ret;
                     new TGMsgBox (gClient->GetRoot(), this, 
                                  "Save changes", msg, kMBIconQuestion, 
                                  kMBYes | kMBNo, &ret);
                     if (ret == kMBYes) {
                        calibration::Calibration* cal = 
                           fCal->Search (fCurChannel, fCurRef, fCurUnit, 
                                        fCurTimeGPS);
                        if (cal) {
                           Transfer (*cal);
                           if (fOk) *fOk = kTRUE;
                        }
                     }
                  }
                  DeleteWindow();
                  break;
               }
            // cancel
            case kCalEditCancel:
               {
                  DeleteWindow();
                  break;
               }
            // new
            case kCalEditNew:
               {
                  if (fSelValid && IsDirty()) {
                     TString msg = "Do you want to keep the current changes?\n";
                     Int_t ret;
                     new TGMsgBox (gClient->GetRoot(), this, 
                                  "Save changes", msg, kMBIconQuestion, 
                                  kMBYes | kMBNo, &ret);
                     if (ret == kMBYes) {
                        calibration::Calibration* cal = 
                           fCal->Search (fCurChannel, fCurRef, fCurUnit, 
                                        fCurTimeGPS);
                        if (cal) {
                           Transfer (*cal);
                           if (fOk) *fOk = kTRUE;
                        }
                     }
                  }
                  TString chn = fCurChannel;
                  TString ref = fCurRef;
                  TString unit = fCurUnit;
                  Time time = fOldest;
                  Bool_t ret;
                  new TLGCalibrationNewDialog (gClient->GetRoot(), this, 
                                       chn, ref, unit, time, ret);
                  if (ret) {
                     // check if already exists
                     if (fCal->Search (chn, ref, unit, time) != 0) {
                        TString msg = "Calibration record already exists.";
                        new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                     msg, kMBIconStop, kMBOk);
                     }
                     else {
                        // add a new calibration record
                        fCal->Add (chn, ref, unit, time);
                        // set its default values
                        calibration::Calibration* cal = 
                           fCal->Search (chn, ref, unit, time);
                        if (cal) {
                        // current
                           fCurChannel = chn;
                           fCurRef = ref;
                           fCurUnit = unit;
                           char t0[256];
                           TimeStr (time, t0, "%Y/%m/%d %H:%N:%S");
                           fCurTime = t0;
                           fCurTimeGPS = time;
                           Transfer (*cal);
                           if (fOk) *fOk = kTRUE;
			   // Enable the copy button.
			   if (my_debug) cerr << "  The New dialog resulted in a calibration being found, enable copy" << endl ;
			   fCopyButton->SetEnabled(kTRUE) ;
                        }
			else
			{
			   // Disable the copy button if there's no cal.
			   if (my_debug) cerr << "  The New dialog resulted in an error, disable copy" << endl ;
			   fCopyButton->SetEnabled(kFALSE) ;
			}
                        if (fCal->AddChannel (chn)) {
                           ShowCal (0);
                        }
                        else {
                           ShowCal (1);
                        }
                        SetDirty (kFALSE);
                     }
                  }
                  break;
               }
	    // paste
	    case kCalEditPaste:
	       {
		  TString chn = fCurChannel ; 	// This is the currently selected channel to paste to.
		  // Check if a calibration already exists.
		  if (fCal->Search(chn, fCopyRef, fCopyUnit, fCopyTime)) {
		     TString msg = "Calibration record already exists.";
		     new TGMsgBox (gClient->GetRoot(), this, "Error", msg, kMBIconStop, kMBOk);
		  }
		  else 
		  {
		     // Add a new calibration record.
		     fCal->Add(chn, fCopyRef, fCopyUnit, fCopyTime) ;
		     // Set the values.  This seems a bit obscure, but to get the Calibration, we have
		     // to search for the record we just added.
		     calibration::Calibration *cal = fCal->Search (chn, fCopyRef, fCopyUnit, fCopyTime) ;
		     if (cal)
		     {
			// Put the copied values into the current values.
			fCurRef = fCopyRef ;
			fCurUnit = fCopyUnit ;
			char t0[256] ;
			TimeStr(fCopyTime, t0, "%Y/%m/%d %H:%N:%S");
			fCurTime = t0 ;
			fCurTimeGPS = fCopyTime ;

			// Set the state of the check buttons.
			fDefault->SetState (fCopyCal->GetDefault() ? kButtonDown : kButtonUp);
			fConvSel->SetState (fCopyCal->GetType() & CALAMPLITUDE ? 
					   kButtonDown : kButtonUp);
			fOffsSel->SetState (fCopyCal->GetType() & CALOFFSET ? 
					   kButtonDown : kButtonUp);
			fDelaySel->SetState (fCopyCal->GetType() & CALTIMEDELAY ? 
					    kButtonDown : kButtonUp);
			fPoleZeroSel->SetState (fCopyCal->GetType() & CALPOLEZERO ? 
					     kButtonDown : kButtonUp);
			fTransSel->SetState (fCopyCal->GetType() & CALTRANSFERFUNCTION ? 
					    kButtonDown : kButtonUp);
			// Fill in numeric entry boxes from the copied calibration.
			fConv->SetNumber (fCopyCal->GetConversion());
			fOffs->SetNumber (fCopyCal->GetOffset());
			fDelay->SetNumber (fCopyCal->GetTimeDelay());
			double gain; int pnum; int znum; const float* pzs = 0;
			if (fCopyCal->GetPoleZeros (gain, pnum, znum, pzs)) {
			   fGain->SetNumber (gain);
			   char buf[1024];
			   for (int i = 0; i < 2; i++) {
			      int indx = 0;
			      buf[0] = 0;
			      for (int j = 0; j < (i == 0 ? pnum : znum); j++) {
				 if (indx > 900) {
				    break;
				 }
				 if (pzs[4*j+2*i+1] == 0) {
				    sprintf (buf + indx, "%s%g", indx == 0 ? "" : ", ",
					    pzs[4*j+2*i]);
				 }
				 else if (pzs[4*j+2*i+1] > 0) {
				    sprintf (buf + indx, "%s%g %g", indx == 0 ? "" : ", ",
					    pzs[4*j+2*i], pzs[4*j+2*i+1]);
				 }
				 indx += strlen (buf + indx);
			      }
			      if (i == 0) {
				 fPoles->SetText (buf);
			      }
			      else {
				 fZeros->SetText (buf);
			      }
			   }
			}
			else {
			   fGain->SetNumber (1.0);
			   fPoles->SetText ("");
			   fZeros->SetText ("");
			}
			fMagSel->SetState (kButtonDown);
			fMag->SetIntNumber (fCopyCal->GetPreferredMag());
			fDSel->SetState (kButtonDown);
			fD->SetIntNumber (fCopyCal->GetPreferredD());
			fComment->LoadBuffer (fCopyCal->GetComment() ? fCopyCal->GetComment() : "");
			// Now that all the values are filled in, 
			*fSelCal = *cal;
			fSelValid = kTRUE;
			// A bunch of changes were made, so set dirty.
			SetDirty() ;
			// This is like pushing the "Set" button after changing something.
			Transfer(*cal) ;
			if (fOk) *fOk = kTRUE ;
			SetDirty(kFALSE) ;
			// This rebuilds the ref, unit, and time combo boxes.
			ShowCal(1) ;
			// In case the Copy button was disabled, enable it since the ref is copyable.
			if (my_debug) cerr << " The copy button is enabled because of a paste" << endl ;
			fCopyButton->SetEnabled(kTRUE) ;
			
		     }
		  }

		  break ;
	       }
	    // Copy
	    case kCalEditCopy:
	       {
                  fCopyRef = fCurRef;
                  fCopyUnit = fCurUnit;
                  fCopyTime = fCurTimeGPS;

		  Transfer (*fCopyCal) ;
		  // Now that something's been copied, allow it to be pasted.
		  fPasteButton->SetEnabled(kTRUE) ;
		  break ;
	       }
            // set
            case kCalEditSet:
               {
                  calibration::Calibration* cal = 
                     fCal->Search (fCurChannel, fCurRef, fCurUnit, 
                                  fCurTimeGPS);
                  if (cal) {
                     Transfer (*cal);
                     if (fOk) *fOk = kTRUE;
                  }
                  SetDirty (kFALSE);
                  break;
               }
            // clear, a.k.a Delete
            case kCalEditClear:
               {
                  if (fCal->Delete (fCurChannel, fCurRef, fCurUnit,
                                   fCurTimeGPS)) {
                     fCurRef = "";
                     fCurUnit = "";
                     fCurTime = "";
                     fCurTimeGPS = Time(0,0);
                     ShowCal (1);
                     SetDirty (kFALSE);
                     if (fOk) *fOk = kTRUE;
		     // Figure out if there's a calibration selected for the copy button.
		     if (fCurRef.IsNull())
		     {
			if (my_debug) cerr << "  Disable copy button because the current cal is deleted." << endl ;
			fCopyButton->SetEnabled(kFALSE) ;
		     }
		     else
		     {
			if (my_debug) cerr << "  Copy button is enabled because there's still a current calibration after delete." << endl ;
			fCopyButton->SetEnabled(kTRUE) ;
		     }
                  }
                  break;
               }
         }
      }
      // Buttons
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_CHECKBUTTON)) {
         switch (parm1) {
            // enable channel
            case kCalEditEnable:
               {
                  fCal->EnableChannel (fCurChannel, 
                                      fEnable->GetState() == kButtonDown);
                  if (fOk) *fOk = kTRUE;
                  break;
               }
            // default calibration
            case kCalEditDefault:
               {
                  SetDirty();
                  break;
               }
            default:
               {
                  SetDirty();
                  break;
               }
         }
      }
      // list boxes
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_LISTBOX)) {
         switch (parm1) {
            // channel selection
            case kCalEditChn:
               {
                  if (fSelValid && IsDirty()) {
                     TString msg = "Do you want to keep the current changes?\n";
                     Int_t ret;
                     new TGMsgBox (gClient->GetRoot(), this, 
                                  "Save changes", msg, kMBIconQuestion, 
                                  kMBYes | kMBNo, &ret);
                     if (ret == kMBYes) {
                        calibration::Calibration* cal = 
                           fCal->Search (fCurChannel, fCurRef, fCurUnit, 
                                        fCurTimeGPS);
                        if (cal) {
                           Transfer (*cal);
                           if (fOk) *fOk = kTRUE;
                        }
                     }
                  }
                  TGTextLBEntry* entry = (TGTextLBEntry*)fChnSel->GetSelectedEntry();
                  if (entry) {
                     fCurChannel = entry->GetText()->GetString();
                  }
                  else {
                     fCurChannel = "";
                  }
                  ShowCal (1);
		  // If there is a calibration for the channel, allow it to be copied.
		  if (fCurRef.IsNull())
		  {
		     // Disable the copy button since nothing can be copied.
		     if (my_debug) cerr << "  Channel selected, but fCurRef is NULL, disallow copy." << endl ;
		     fCopyButton->SetEnabled(kFALSE) ;
		  }
		  else
		  {
		     // If the fCurRef isn't null, so allow copying.
		     if (my_debug) cerr << "  Channel selected, allow copy." << endl ;
		     fCopyButton->SetEnabled(kTRUE) ;
		  }
                  SetDirty (kFALSE);
                  break;
               }
         }
      }
      // combo boxes
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_COMBOBOX)) {
         switch (parm1) {
            // reference point
            case kCalEditRef:
               {
                  if (fSelValid && IsDirty()) {
                     TString msg = "Do you want to keep the current changes?\n";
                     Int_t ret;
                     new TGMsgBox (gClient->GetRoot(), this, 
                                  "Save changes", msg, kMBIconQuestion, 
                                  kMBYes | kMBNo, &ret);
                     if (ret == kMBYes) {
                        calibration::Calibration* cal = 
                           fCal->Search (fCurChannel, fCurRef, fCurUnit, 
                                        fCurTimeGPS);
                        if (cal) {
                           Transfer (*cal);
                           if (fOk) *fOk = kTRUE;
                        }
                     }
                  }
                  TGTextLBEntry* entry = 
                     (TGTextLBEntry*)fRef->GetSelectedEntry();
                  if (entry) {
                     fCurRef = entry->GetText()->GetString();
                  }
                  else {
                     fCurRef = "";
                  }
                  ShowCal (2);
                  SetDirty (kFALSE);
                  break;
               }
            // unit
            case kCalEditUnit:
               {
                  if (fSelValid && IsDirty()) {
                     TString msg = "Do you want to keep the current changes?\n";
                     Int_t ret;
                     new TGMsgBox (gClient->GetRoot(), this, 
                                  "Save changes", msg, kMBIconQuestion, 
                                  kMBYes | kMBNo, &ret);
                     if (ret == kMBYes) {
                        calibration::Calibration* cal = 
                           fCal->Search (fCurChannel, fCurRef, fCurUnit, 
                                        fCurTimeGPS);
                        if (cal) {
                           Transfer (*cal);
                           if (fOk) *fOk = kTRUE;
                        }
                     }
                  }
                  TGTextLBEntry* entry = 
                     (TGTextLBEntry*)fUnit->GetSelectedEntry();
                  if (entry) {
                     fCurUnit = entry->GetText()->GetString();
                  }
                  else {
                     fCurUnit = "";
                  }
                  ShowCal (3);
                  SetDirty (kFALSE);
                  break;
               }
            // time
            case kCalEditTime:
               {
                  if (fSelValid && IsDirty()) {
                     TString msg = "Do you want to keep the current changes?\n";
                     Int_t ret;
                     new TGMsgBox (gClient->GetRoot(), this, 
                                  "Save changes", msg, kMBIconQuestion, 
                                  kMBYes | kMBNo, &ret);
                     if (ret == kMBYes) {
                        calibration::Calibration* cal = 
                           fCal->Search (fCurChannel, fCurRef, fCurUnit, 
                                        fCurTimeGPS);
                        if (cal) {
                           Transfer (*cal);
                           if (fOk) *fOk = kTRUE;
                        }
                     }
                  }
                  TGTextLBEntry* entry = 
                     (TGTextLBEntry*)fTime->GetSelectedEntry();
                  if (entry) {
                     fCurTime = entry->GetText()->GetString();
                     utc_t utc;
                     sscanf ((const char*)fCurTime, 
                            "%i%*c%i%*c%i%i%*c%i%*c%i",
                            &utc.tm_year, &utc.tm_mon, &utc.tm_mday,
                            &utc.tm_hour, &utc.tm_min, &utc.tm_sec);
                  
                     utc.tm_year -= 1900;
                     utc.tm_mon -= 1;
                     fCurTimeGPS = Time (UTCtoTAI (&utc), 0);
                  }
                  else {
                     fCurTime = "";
                     fCurTimeGPS = Time(0);
                  }
                  ShowCal (4);
                  SetDirty (kFALSE);
                  break;
               }
         }
      }
      // text entry fields
      else if ((GET_MSG (msg) == kC_TEXTENTRY) &&
              (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) {
         SetDirty();
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGCalibrationDialog::Transfer (calibration::Calibration& cal)
   {
      fSelCal->SetChannel (fCurChannel);
      fSelCal->SetRef (fCurRef);
      fSelCal->SetUnit (fCurUnit);
      fSelCal->SetTime (fCurTimeGPS);
      if (fConvSel->GetState() == kButtonDown) {
         fSelCal->SetConversion (fConv->GetNumber());
      }
      else {
         fSelCal->ResetConversion();
      }
      if (fOffsSel->GetState() == kButtonDown) {
         fSelCal->SetOffset (fOffs->GetNumber());
      }
      else {
         fSelCal->ResetOffset();
      }
      if (fDelaySel->GetState() == kButtonDown) {
         fSelCal->SetTimeDelay (fDelay->GetNumber());
      }
      else {
         fSelCal->ResetTimeDelay();
      }
      fSelCal->SetDefault (fDefault->GetState() == kButtonDown);
      // read pole-zeros
      if (fPoleZeroSel->GetState() == kButtonDown) {
         int pnum = 0; int znum = 0; 
         float pzs[4*100];
         double gain = fGain->GetNumber();
         for (int i = 0; i < 2; i++) {
            const char* p = (i == 0) ?
               fPoles->GetText() : fZeros->GetText();
            while ((p != 0) && (*p != 0) && 
                  ((i == 0 ? pnum : znum) < 98)) {
               // read real part
               char* next;
               double x = strtod (p, &next);
               if (next == p) {
                  break;
               }
               p = next;
               // check for imaginary part
               while (*p == ' ') p++;
               if ((*p != ',') && (*p != 0)) {
                  double y = strtod (p, &next);
                  if (next == p) {
                     break;
                  }
                  pzs[i == 0 ? 4*pnum : 4*znum+2] = x;
                  pzs[i == 0 ? 4*pnum+1 : 4*znum+2+1] = y;
                  if (i == 0) pnum++; 
                  else znum++;
                  pzs[i == 0 ? 4*pnum : 4*znum+2] = x;
                  pzs[i == 0 ? 4*pnum+1 : 4*znum+2+1] =-y;
                  if (i == 0) pnum++; 
                  else znum++;
                  p = next;
                  while (*p == ' ') p++;
               }
               else {
                  pzs[i == 0 ? 4*pnum : 4*znum+2] = x;
                  pzs[i == 0 ? 4*pnum+1 : 4*znum+2+1] = 0;
                  if (i == 0) pnum++; 
                  else znum++;
               }
               if (*p == ',') p++;
            }
         }
         fSelCal->SetPoleZeros (gain, pnum, znum, pzs);
         cerr << "SET CAL POLE/ZERO " << pnum << " " << znum << endl;
      }
      else {
         fSelCal->SetPoleZeros (1, 0, 0, 0);
      }
      if (fTransSel->GetState() == kButtonUp) {
         fSelCal->SetTransferFunction (0, 0);
      }
      if (fMagSel->GetState() == kButtonDown) {
         fSelCal->SetPreferredMag (fMag->GetIntNumber ());
      }
      if (fDSel->GetState() == kButtonDown) {
         fSelCal->SetPreferredD (fD->GetIntNumber ());
      }
      string comment = fComment->GetString();
      fSelCal->SetComment (!comment.empty() ? comment.c_str() : 0);
   
      cal = *fSelCal;
   }

}
