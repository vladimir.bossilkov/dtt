add_library(dttgui SHARED TLGCalDlg.cc TLGExport.cc TLGMainMenu.cc TLGMainWindow.cc TLGMath.cc TLGOptions.cc
		TLGPad.cc TLGPlot.cc TLGPrint.cc TLGSave.cc TDGTextEdit.cpp
        BugReportDlg.cpp)

MESSAGE(STATUS "LIGOGUI_INCLUDE_DIR=${LIGOGUI_INCLUDE_DIR}")

target_include_directories(dttgui PUBLIC
        #${EXTERNAL_INCLUDE_DIRECTORIES}
        #${LIGOGUI_INCLUDE_DIR}

        )
target_link_libraries(dttgui PUBLIC
		ligogui

		xsil
		lxr
		#metaio
		gdscntr
		#gdsmath
		expat
		gdsbase
		gdsalgo

		fftw3
		fftw3f
		${ROOT_LIBRARIES}
        ROOT::Gui
		)

set_target_properties( dttgui
		PROPERTIES
		VERSION 0.0.0
        PRIVATE_HEADER
            "ComponentHolder.h;BugReportDlg.h;TLGCalDlg.hh;TLGExport.hh;TLGMainMenu.hh;TLGMainWindow.hh;TLGMath.hh;TLGOptions.hh;TLGPad.hh;TLGPlot.hh;TLGPrint.hh;TLGSave.hh"
		)

INSTALL_LIB(dttgui ${INCLUDE_INSTALL_DIR}/dtt/dttgui)

SET(dict_headers
        ComponentHolder.h
		TLGCalDlg.hh
		TLGExport.hh
		TLGMainMenu.hh
		TLGMainWindow.hh
		TLGMath.hh
		TLGOptions.hh
		TLGPad.hh
		TLGPlot.hh
		TLGPrint.hh
		TLGSave.hh
		)

create_cint_dict(dttgui dttgui 0.0.0
			-c
			-I${GDS_INCLUDE_DIR}
            -I${LIGOGUI_INCLUDE_DIR}         
		    -I${GDS_INCLUDE_DIR}/xml
			-I${CMAKE_SOURCE_DIR}/src/base/time
		    -I${CMAKE_SOURCE_DIR}/src/containers/PlotSet
			-I${CMAKE_SOURCE_DIR}/src/config
            -I${CMAKE_CURRENT_SOURCE_DIR}
			${dict_headers}
			dttgui_linkdef.h
			)

INSTALL(FILES ${dict_headers}
		DESTINATION ${INCLUDE_INSTALL_DIR}/dtt/ligogui
		COMPONENT libraries
		)

target_link_libraries(Rdttgui PUBLIC
		dttgui)