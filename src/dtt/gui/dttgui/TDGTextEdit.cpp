//
// Created by erik.vonreis on 11/11/21.
//

#include "TDGTextEdit.h"

TDGTextEdit::TDGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h,
                         Int_t id, UInt_t sboptions, ULong_t back)
    : TLGTextEdit (parent, w, h, id, sboptions, back)
{
}
//______________________________________________________________________________
TDGTextEdit::TDGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h,
                         TGText* text, Int_t id, UInt_t sboptions,
                         ULong_t back)
    : TLGTextEdit (parent, w, h, text, id, sboptions, back)
{
}
//______________________________________________________________________________
TDGTextEdit::TDGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h,
                         const char* string, Int_t id, UInt_t sboptions,
                         ULong_t back)
    : TLGTextEdit (parent, w, h, string, id, sboptions, back)
{
}

//______________________________________________________________________________
std::string TDGTextEdit::GetString(char cmt) const
{
  const char dle(16);
  //   Note that this function starts with a root TString containing
  //   the entire text of the TextEdit object (lines sparated by
  //   newline characters), removes the funky tab padding characters
  //   and reformats it into a STL string.
  //

  // ROOT version 6.22/00 introduced a bug
  // where calls to AsString()
  // resulted in a double free, crashing the program
  // the #else block of the following #ifdef is a slow workaround
  // a fix has been accepted but wasn't part of 6.24/00
  TString txt = GetText()->AsString();
  int N = txt.Length();

  std::string cmd;
  cmd.reserve(N);

  bool incmt = false;
  for (int i=0; i<N; ++i) {
    char txti = txt[i];
    incmt = (incmt && txti != '\n') || (txti == cmt);
    if (!incmt && txti != dle) cmd += txti;
  }
  return cmd;
}