/* version $Id: TLGPad.hh 7786 2016-11-07 16:15:58Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGPAD_H
#define _LIGO_TLGPAD_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGPad							*/
/*                                                         		*/
/* Module Description: Graphics Pad:	 				*/
/*		       single and multi pad for graphics plotting,	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGPad.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <memory>
#include <TSortedList.h>
#include <TGFrame.h>
#include <TGButton.h>
#include <TGMenu.h>
#include <TRootEmbeddedCanvas.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TMarker.h>
#include <TPaveText.h>
#include <TH1.h> // hist
#include "TLGFrame.hh"
#include "TLGOptions.hh"
#include "TLGMainMenu.hh"
#include "VirtualPlotPad.hh"

   class TText;
   class TLine;
   class TLegend;
   class PlotSet;
   class BasicDataDescriptor;
   class PlotDescriptor;

namespace calibration  {
   class Table;
}
namespace ligogui {

   class TLGPad;
   class TLGPadMain;
   struct ExportOption_t;
   typedef ExportOption_t ImportOption_t;
   struct ReferenceTraceList_t;
   struct MathTable_t;
   class TLGPrintParam;


/** @name TLGPad
    This header exports a plotting pad, a multiple pads object
    and a main window consisting of muliple pads.
   
    @memo Plotting pad(s)
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{
/** @name Constants
    Constants for pad parameters and widget IDs.
   
    @memo Option type constants
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// Maximum time difference allowed to be adjusted (1 year)
   const int kMaxTimeAdjust = 365*24*3600;
   /// Maximum number of pads allowed in a multipad window
   const int kGMaxPad = 25;
   /// Maximum number of pads per dimension
   const int kGMaxGrid = 16;

   /// Maximum number of delayed cursor requests
   const int kMaxCursorStack = 20;
   /// Curser timer delay in ms
   const int kCursorDelay = 50;

   /// Widget ID of reset button in multi pad
   const int kGMPadResetID = 149;
   /// Widget ID of zoom button in multi pad
   const int kGMPadZoomID = 150;
   /// Widget ID of active button in multi pad
   const int kGMPadActiveID = 151;
   /// Widget ID of new button in multi pad
   const int kGMPadNewID = 152;
   /// Widget ID of options button in multi pad
   const int kGMPadOptionID = 153;
   /// Widget ID of import button in multi pad
   const int kGMPadImportID = 154;
   /// Widget ID of export button in multi pad
   const int kGMPadExportID = 155;
   /// Widget ID of reference button in multi pad
   const int kGMPadReferenceID = 156;
   /// Widget ID of calibration button in multi pad
   const int kGMPadCalibrationID = 157;
   /// Widget ID of math button in multi pad
   const int kGMPadMathID = 158;
   /// Widget ID of print button in multi pad
   const int kGMPadPrintID = 159;

   /// Widget ID of option panel hide button
   const int kGOptHideID = 160;
   /// Widget ID of option panel dialog window button
   const int kGOptDialogID = 161;
   /// Widget ID of graphics canvas
   const int kGCanvasID = 163;

   /// Default canvas width
   const int kCanvasWidth = 600;
   /// Default canvas height
   const int kCanvasHeight = 400;
//@}

/**
 * Contains status info that we want to display prominently on a plot
 *
 * @memo Obvious Status Message
 * @author Erik von Reis
 */
class TLProgramStatus {
private:
    bool disconnected;


public:
    TLProgramStatus(): disconnected(false) {}
    bool Disconnected() {return disconnected;}
    void Disconnected(bool discon) {disconnected = discon;}

};

/** Extensions to the TGraph ROOT object. For internal use only.
   
    @memo TGraph extensions.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGraphExtensions {
   protected:
      /// Pad which owns the graph
      TLGPad*		fParent;
      /// Minumum range in X
      Double_t		fMinimumX;
      /// Maximum range in X
      Double_t		fMaximumX;
      /// Bar option?
      Bool_t		fBarOpt;
      /// Bar spacing
      Float_t		fBarDelta;
      /// Width of bar
      Float_t		fBarWidth;
      /// Offset of bar
      Float_t		fBarOffset;
   
   public:
      explicit TLGraphExtensions (TLGPad* p) 
      : fParent (p), fMinimumX (-1111), fMaximumX (-1111),
	fBarOpt(kFALSE), fBarDelta (1.), fBarWidth (1.),  fBarOffset (0.) {
      }
      virtual ~TLGraphExtensions (void) {}
      virtual void SetMinimumX (Double_t min = -1111) {
         fMinimumX = min; }
      virtual void SetMaximumX (Double_t max = -1111) {
         fMaximumX = max; }
      virtual Double_t GetMinimumX () const {
         return fMinimumX; }
      virtual Double_t GetMaximumX () const {
         return fMaximumX; }
      virtual void SetBarDelta (Float_t d) {
         fBarDelta = d; }
      virtual Float_t GetBarDelta () const {
         return fBarDelta; }
      virtual void SetBarWidth (Float_t w) {
         fBarWidth = w; }
      virtual Float_t GetBarWidth () const {
         return fBarWidth; }
      virtual void SetBarOffset (Float_t ofs) {
         fBarOffset = ofs; }
      virtual Float_t GetBarOffset () const {
         return fBarOffset; }
   };

#ifndef __CINT__
/** A graph which inherits form TGraph and TLGraphExtensions. 
    For internal use only.
   
    @memo TLGraph object.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGraph : public TLGraphExtensions, public TGraph {
   public:
      TLGraph (TLGPad* p, Int_t n, const Float_t* x, const Float_t* y);
      virtual void Draw (Option_t* opt);
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,4,2)
      virtual void ComputeRange (Double_t& xmin, Double_t& ymin, 
                        Double_t& xmax, Double_t& ymax) const;
   #else
      virtual void ComputeRange (Double_t& xmin, Double_t& ymin, 
                        Double_t& xmax, Double_t& ymax);
   #endif
      virtual void ComputeLimits (Double_t& xmin, Double_t& ymin, 
                        Double_t& xmax, Double_t& ymax);
#if ROOT_VERSION_CODE < ROOT_VERSION(5,20,0)
      virtual void PaintGraph(Int_t npoints, const Double_t *x, 
                        const Double_t *y, 
                        const Option_t *chopt);
#else
      virtual void SetMinimumX (Double_t min = -1111);
      virtual void SetMaximumX (Double_t max = -1111);
#endif
      void ComputeLogs(Int_t npoints, Int_t opt);
   };
#endif

/** Layout manager for plot pad. For internal use only.
   
    @memo Pad layout manager.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGPadLayout : public TGLayoutManager {
   protected:
      /// pointer to pad
      TLGPad*		fPad;
      /// List of frames
      TList*		fList;
   
   public:
      TLGPadLayout (TLGPad* pad);
   
      virtual void Layout ();
      virtual TGDimension GetDefaultSize () const;
   };


/** A GUI widget which maintains a plot. Mostly for internal use.
   
    @memo Plot pad.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGPad : public VirtualPlotPad, public TGCompositeFrame, 
   public TGWidget {
   
      friend class TLGPadLayout;
      friend class TLGraph;
      TLGPad (const TLGPad&);
      TLGPad& operator= (const TLGPad&);
   
   public:
      /// Data copy options (order must be the same as EYUnitsType)
      enum EDataCopy {
      /// As is
      kDCpyAsIs = 4,
      /// Magnitude
      kDCpyMagnitude = 0,
      /// dB Magnitude
      kDCpydBMagnitude = 1,
      /// Real
      kDCpyReal = 2,
      /// Imaginary
      kDCpyImaginary = 3,
      /// Phase (degree)
      kDCpyPhaseDeg = 5,
      /// Phase (rad)
      kDCpyPhaseRad = 6,
      /// Continuous phase (degree)
      kDCpyPhaseDegCont = 7,
      /// Continuous phase (rad)
      kDCpyPhaseRadCont = 8
      };
   
      /// Set of display parameters
      struct DisplayParam {
         /// valid set?
         Bool_t		fValid;
      	 /// last extra time shift
         Double_t	fLastTimeShift;
         /// last X conversion
         EXRangeType	fLastXConv;
         /// last Y conversion
         EDataCopy 	fLastDCpy;
         /// last X units
         TString	fUnitX;
         /// last Y units
         TString	fUnitY;
         /// last X unit magnitude
         Int_t		fMagX;
         /// last Y unit magnitude
         Int_t		fMagY;
         /// last lower array index
         Int_t		fN1;
         /// last upper array index
         Int_t		fN2;
         /// last scaling factors
         Float_t	fScaling[2];
         /// last offset values
         Float_t	fOffset[2];
         /// last bining
         Int_t		fBin;
         /// last bin spacing
         Bool_t		fLogspacing;
      
      	 /// Default constructor
         DisplayParam();
      };
   
   
   protected:
      /// Data pool
      PlotSet*		fPlotSet;
      /// Options values
      OptionAll_t	fOptions;
      /// List of stored options
      OptionAll_t**	fStoreOptions;
      /// Size of stored options list
      Int_t		fStoreOptionsMax;
      /// List of x units
      TSortedList	fXUnitList;
      /// List of y units
      TSortedList	fYUnitList;
   
      /// Currently drawn plot type
      TString		fPlotType;
      /// range: minimum
      Double_t		fRangeMin[2];
      /// range: smalles positive number
      Double_t		fRangeMinPos[2];
      /// range: maximum
      Double_t		fRangeMax[2];
      /** update manual range selection on next redraw
          BIT(0) - low x axis, BIT(1) - high x axis
          BIT(2) - low y axis, BIT(3) - high y axis */
      Int_t		fManualRangeUpdate;
      /// X-axis limits: lower and upper bound
      Double_t		fLimitX[2];
      /// Y-axis limits: lower and upper bound
      Double_t		fLimitY[2];
      /// Extra time shift
      Double_t		fExtraTimeShift;
      /// Extra X slope
      Double_t		fExtraXSlope;
      /// Extra X offset
      Double_t		fExtraXOffset;
      /// Zero time of last time series
      UInt_t		fT0;
      /// Zero time (nsec) of last time series
      UInt_t            fT0NSec;
      /// plot data
      BasicDataDescriptor* fData[kMaxTraces];
      /// original plot descriptor of data
      const PlotDescriptor* 	fOriginalPlotD[kMaxTraces];
      /// last set of display parameters
      DisplayParam	fLastDisp[kMaxTraces];
   
      /// Canvas style 
      TStyle*		fStyle;
      /// Panel enable?
      Bool_t		fEnablePanel;
      /// Popup dialog panel allowed?
      Bool_t		fEnablePanelDialog;
      /// Hide option panel?
      Bool_t		fHidePanel;
      /// Option panel at left position?
      Bool_t		fLeftPanel;
      /// Pad name
      TString		fPadName;
      /// Panel visible?
      Bool_t		fPanelVisible;
      /// histogram frame
      TH1F* fHFrame;
   
      /// hide button
      TGPictureButton*	fOptionsHide;
      /// hide button picture
      const TGPicture*	fPicHide;
      /// hide button picture
      const TGPicture*	fPicUnhide;
      /// hide button
      TGPictureButton*	fOptionsDialog;
      /// hide button picture
      const TGPicture*	fPicDialog;
      /// Option tabs window
      TLGOptionTab*	fOptionTabs;
      /// Graphics window
      TRootEmbeddedCanvas* fGraph;
      /// Layout hints
      TGLayoutHints*	fL;
   
      /// graph objects
      TObject*		fPlot[kMaxTraces];
      /// graph object id
      Int_t		fPlotId[kMaxTraces];
      /// graph options
      char		fPlotDrawOption[kMaxTraces][8];
   
      /// Legend 
      TLegend*		fLegend;
      /// Disconnect Message
      TPaveText*     fDisconnectMessage;
      /// Parameter text objects
      TText*		fParamText[3];
      /// cursor line objects
      TLine*		fCursorLine[2];
      /// cursor marker objects
      TMarker*		fCursorMarker[2];
      /// Timer for cursor processing delay
      TTimer*		fCursorTimer;
      /// List of stacked up cursor requests
      Long_t		fCursorStack[2*kMaxCursorStack];
      /// Length of stack
      Int_t		fCursorStackSize;
   
      /// stat box hist (mito)
      TPaveText* fStatBox;
   
      /// Option dialogbox
      TLGOptionDialog*	fOptionDialogbox;

      TLProgramStatus fProgramStatus;
   
      virtual Float_t ConvX (Float_t y) const;
      virtual Double_t ConvX (Double_t y) const;
      virtual Float_t ConvY (Float_t y, Bool_t pwrcorr = kFALSE) const;
      virtual Double_t ConvY (Double_t y, Bool_t pwrcorr = kFALSE) const;
      virtual Bool_t DisplayChanged (Int_t trace, 
                        const PlotDescriptor* plotd,
                        Int_t& n1, Int_t& n2, Int_t& bin, 
                        Bool_t& logspacing, EDataCopy& cpy);
   
      virtual void SetPlotData (Int_t trace, const PlotDescriptor* plotd);
      virtual void DrawPlot ();
      virtual void DrawPlot (Int_t trace);
      virtual void GetRange (Int_t axis, Double_t& min, Double_t& max,
                        Double_t& minpos,
                        const Double_t* low = 0, const Double_t* high = 0);
      virtual void GetHistRange(Double_t& xmin,Double_t& ymin, 
                        Double_t& xmax, Double_t& ymax); // hist (mito)
      virtual Int_t PlotTypeId (const char* plottype);
      virtual void UpdateOptionPanels (Bool_t panel = kTRUE, 
                        Bool_t dialog = kTRUE);
      virtual void UpdateLegend ();
      virtual void UpdateDisconnectMessage();
      virtual void UpdateParameters (Bool_t* allthesame = 0);
      virtual void UpdateCursor (Bool_t recompute = kTRUE, 
                        Bool_t draw = kFALSE, Bool_t update = kTRUE,
                        Int_t changeopt = 0, Bool_t premodified = kFALSE);
      void ComputeLogs (Int_t nx, Int_t ny, double x[], double y[]);

   public:
      TLGPad (const TGWindow* p, const char* name, Int_t id, 
             PlotSet& plots, OptionAll_t** list = 0, Int_t max = 0,
             const OptionAll_t* optinit = 0);
      virtual ~TLGPad ();
   
      /// Get the ROOT canvas of the pad.
      virtual TCanvas* GetCanvas () const {
         return fGraph->GetCanvas(); }
      /// Get the plot option struct.
      virtual OptionAll_t* GetPlotOptions () {
         return &fOptions; }
      /// Set a pointer to the list of stored options
      virtual void SetStoreOptionList (OptionAll_t** list, Int_t max);
   
      /// Hide/show the dialog option panel.
      virtual void PanelDialog (Bool_t newstate);
      /// Hide/show the embedded option dialog.
      virtual void HidePanel (Bool_t newstate);
      /// Panel hidden?
      virtual Bool_t GetHideState() const {
         return fHidePanel; }
      /// Display embedded option panel on the left (default).
      virtual void LeftPanel (Bool_t newstate);
      /// Panel left?
      virtual Bool_t GetLeftState() const {
         return fLeftPanel; }
      /// Enable/disable embedded option panel.
      virtual void EnablePanel (Bool_t newstate);
      /// Embedded option panel enabled?
      virtual Bool_t GetEnableState() const {
         return fEnablePanel; }
      /// Enable/disable option panel dialog box.
      virtual void EnablePanelDialog (Bool_t newstate);
      /// Option panel dialog box enabled?
      virtual Bool_t GetEnableDialogState() const {
         return fEnablePanelDialog; }
   
      /// Updates the plot. If force is true regenerates the data
      virtual void UpdatePlot (const BasicPlotDescriptor* plotd = 0,
                        bool force = true);
      /// Updates the option panel with the current options
      virtual void UpdateOptions () {
         UpdateOptionPanels(); } 
      /// Updates the list of units
      virtual void UpdateUnits (); 
      /// Updates option panel and plot
      virtual void Update (bool force = false);

      /** Configure a pad for a specific plot type.
   
       Currently supported configurations (conf/variant):
       \begin{verbatim}
       0 - Time series
           0 - Magnitude
   	   1 - Real part
   	   2 - Imaginary part
   	   3 - Phase (Degree)
       1 - Power spectrum
           0 - Magnitude (log scale)
           1 - Magnitude (linear scale)
       2 - Coherence
   	   0 - Magnitude
       3 - Cross correlation
           0 - dB Magnitude
   	   1 - Phase (Degree)
       4 - Transfer function (Bode plot)
    	   0 - dB Magnitude
   	   1 - Phase (Degree)
       5 - Coherence function
   	   0 - Magnitude
       6 - Transfer coefficients
    	   0 - dB Magnitude
   	   1 - Phase (Degree)
       7 - Coherence coefficients
   	   0 - Magnitude
       8 - Harmonic coefficients
   	   0 - dB Magnitude
       9 - Intermodulation coefficients
   	   0 - dB Magnitude
       \end{verbatim} 
       */
      virtual void Configure (Int_t conf, Int_t variant);
      /// Shows a plot
      virtual Int_t ShowPlot (const PlotDescriptor* data, 
                        const char* plottype = 0, Int_t variant = 0, 
                        Bool_t update = kTRUE);
      /// Shows muliple plots in the same pad. Graph type must be identical
      virtual Int_t ShowMultiPlot (const PlotDescriptor* data[], 
                        Int_t plotnum, const char* plottype = 0,
                        Int_t variant = 0, Bool_t update = kTRUE);
      /// Print current plot to a postscript file
      virtual Bool_t PostScript (const TString& filename, Int_t wtype = -111);
      /// Fill a plot set with references to the displayed traces
      virtual Bool_t Fill (PlotSet& pl, const char* winid = 0, 
                        const char* padid = 0);
   
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
      /// Process timer events
      virtual Bool_t HandleTimer (TTimer* timer);

      virtual TLProgramStatus *ProgramStatus(){return &fProgramStatus;}
   };



   class TLGMultiPad;

/** A helper class which manages the pad grid. For internal use only.
   
    @memo Multiple pad layout grid.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMultiPadLayout : public TGLayoutManager {
   protected:
      /// pointer to pad
      TLGMultiPad*	fMPad;
   
   public:
      TLGMultiPadLayout (TLGMultiPad* mpad);
   
      virtual void Layout ();
      virtual TGDimension GetDefaultSize () const;
   };


/** A GUI widget which maintains a multiple plot pads. 
    Mostly for internal use.
   
    @memo Multiple plot pads.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMultiPadLayoutGrid {
   protected:
      /// Layout id
      Int_t		fLayout;
      /// Layout grid with position ids
      Int_t		fGrid[kGMaxGrid * kGMaxGrid];
      /// List of pad position ids
      Int_t		fPadPos[kGMaxPad];
      /// Number of rows in layout grid
      Int_t		fRows;
      /// Number of columns in layout grid
      Int_t		fCols;
   
   public:
      TLGMultiPadLayoutGrid (Int_t rows, Int_t columns);
      explicit TLGMultiPadLayoutGrid (Int_t layout = 1);
   
      /** Set the pad layout. Supported outlines:
          \begin{verbatim}
          1 : one single pad
          2 : two vertical pads
          3 : three pads in a 2x2 grid (lower-right position invalid)
   	  4 : four pads in 2x2 grid
   	  5 : five pads in 3x2 grid (lower-right position invalid)
   	  6 : six pads in 3x2 grid
   	  7 : seven pads in 3x3 grid (some positions invalid)
   	  8 : eight pads in 3x3 grid (lower-right position invalid)
          9 : nine pads in a 3x3 grid
          10: ten pads in a 4x3 grid (some positions invalid)
          11: eleven pads in a 4x3 grid (lower-right position invalid)
   	  12: twelve pads in 4x3 grid
   	  13: thirteen pads in 4x4 grid (some positions invalid)
   	  14: fourteen pads in 4x4 grid (some positions invalid)
   	  15: fifteen pads in 4x4 grid (lower-right position invalid)
   	  16: sixteen pads in a 4x4 grid
          101: two horizontal pads
          102: three pads in a 3x1 grid
   	  103: four pads in 4x1 grid
   	  104: three pads in 2x2 grid (one large 1x2 pad)
   	  105: six pads in 3x3 grid (one large 2x2 pad)
          \end{verbatim}
   
       ******************************************************************/
      void SetPadLayout (Int_t layout = 1);
      void SetPadLayout (Int_t rows, Int_t columns);
      Int_t GetPadLayout () { return fLayout; }
      Bool_t GetPadCoordinates (Int_t pad, Int_t& x, Int_t& y, 
                        UInt_t& w, UInt_t& h);
      void SetPadPosition (Int_t pad, Int_t pos);
      Int_t GetPadPosition (Int_t pad);
      void SwapPads (Int_t pad1, Int_t pad2);
      Int_t& operator ()(Int_t row, Int_t col);
   };


/** A GUI widget which maintains a multiple plot pads. 
   
    @memo Multiple plot pads.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMultiPad : public TGCompositeFrame, public TGWidget, 
   public TLGMultiPadLayoutGrid {
   
      friend class TLGMultiPadLayout;
      TLGMultiPad (const TLGMultiPad&);
      TLGMultiPad& operator= (const TLGMultiPad&);
   
   public:
   
      /// Reset plot settings object
      class ActionPlotPads {
      public:
         /// Default constructor
         ActionPlotPads () {}
         /// Default destructor
	 virtual ~ActionPlotPads () {}
      	 /// reset
         virtual bool Reset (const PlotSet& pset, TLGMultiPad& mpad) {
            return true; }
      	 /// import
         virtual bool Import (ImportOption_t* defim,
                           const PlotSet& pset, TLGMultiPad& mpad);
      	 /// export
         virtual bool Export (ExportOption_t* defex,
                           const PlotSet& pset, TLGMultiPad& mpad);
      };
   
   protected:
      /// Data pool
      PlotSet*		fPlotSet;
      /// List of pads
      TLGPad*		fPadList[kGMaxPad];
      /// Number of pads in list
      Int_t		fPadNum;
      /// name of multipad
      TString		fPadName;
      /// Button array
      TList		fButtons;
   
      /// Zoomed pad
      Int_t		fPadZoom; 
      /// Hide state before zoom
      Bool_t		fHidePanelBeforeZoom;
   
      /// List of stored options
      OptionAll_t**	fStoreOptions;
      /// Size of stored options list
      Int_t		fStoreOptionsMax;
      /// Pointer to default print parameters
      TLGPrintParam* 	fDefPrintSetup;
      /// Pointer to default import option parameters
      ImportOption_t*	fDefImportOpt;
      /// Pointer to default export option parameters
      ExportOption_t*	fDefExportOpt;
      /// Pointer to list of reference traces
      ReferenceTraceList_t*	fRefTraces;
      /// Math function table
      MathTable_t*		fMathTable;
      /// Calibration table
      calibration::Table*	fCalTable;
      /// Pointer to reset object
      ActionPlotPads*	fAction;
   
      virtual Bool_t ProcessButtons (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   
   public:
      TLGMultiPad (const TGWindow* p, const char* name, 
                  PlotSet& plots, Int_t id = 0, Int_t layout = 1);
      virtual ~TLGMultiPad ();
   
      /// Get plot set
      PlotSet* GetPlotSet() const {
         return fPlotSet; }
   
      /// Get an individual plot pad.
      virtual TLGPad* GetPad (Int_t pad) const {
         return ((pad < 0) || (pad >= fPadNum)) ? 0 : fPadList[pad]; }
      /// Get the options associated with a plot pad.
      virtual OptionAll_t* GetPlotOptions (Int_t pad) {
         return ((pad < 0) || (pad >= fPadNum)) ? 
            0 : fPadList[pad]->GetPlotOptions(); }
      /// Set a pointer to the list of stored options
      virtual void SetStoreOptionList (OptionAll_t** list, Int_t max);
      /// Get the ROOT canvas associated with a plot pad.
      virtual TCanvas* GetPadCanvas (Int_t pad) const {
         return ((pad < 0) || (pad >= fPadNum)) ? 
            0 : fPadList[pad]->GetCanvas(); }
      /// Get the numebr of pads
      virtual Int_t GetPadNumber () const {
         return fPadNum; }
      /// Set the layout and adjusts the number of pads.
      void SetPadLayoutAndNumber (Int_t layout);
      /// Set the number of pads. See also TLGMultiPadLayoutGrid::SetPadNumber
      // The force argument causes a layout() to be done even if the number
      // of pads hasn't changed to fix Bugzilla 1041.
      virtual void SetPadNumber (Int_t pads, Int_t force = 0);
      /// Add a user button to the multi pad
      virtual void AddButton (TGButton* btn);
   
      /// Set the active pad.
      virtual Int_t SetActivePad (Int_t pad);
      /// Get the active pad.
      virtual Int_t GetActivePad () const;
      /// Zoom into specified pad.
      virtual Int_t Zoom (Int_t pad);
   
      /// Update the plots
      virtual void UpdatePlot (const BasicPlotDescriptor* plotd = 0);
      /// Update both options and the plots
      virtual void Update (bool force = false);
   
      /// Show a plot
      virtual Int_t ShowPlot (const PlotDescriptor* data, 
                        const char* plottype = 0, Int_t pad = 0, 
                        Bool_t update = kTRUE);
      /// Show a plot
      virtual Int_t ShowPlot (const PlotDescriptor* data, 
                        const char* plottype, Int_t pad, 
                        Int_t variant, Bool_t update = kTRUE);
      /// Show multiple plots in the same pad
      virtual Int_t ShowMultiPlot (const PlotDescriptor* data[], 
                        Int_t plotnum, const char* plottype = 0, 
                        Int_t pad = 0, Bool_t update = kTRUE);
      /// Show multiple plots in the same pad
      virtual Int_t ShowMultiPlot (const PlotDescriptor* data[], 
                        Int_t plotnum, const char* plottype, 
                        Int_t pad, Int_t variant, Bool_t update = kTRUE);
   
      /// Set a pointer to a default print setup parameters
      virtual void SetDefPrintSetup (TLGPrintParam* defprint) {
         fDefPrintSetup = defprint; }
      /// Get a pointer to a default print setup parameters
      virtual TLGPrintParam* GetDefPrintSetup() const {
         return fDefPrintSetup; }
      /// Print plots using the supplied print parameters
      virtual Bool_t PrintPS (TLGPrintParam& print, Int_t* errcode = 0);
      /// Print plots showing the print dialog box first
      virtual Bool_t PrintPSDlg (const TLGPrintParam* defprint = 0);
   
      /// Set a pointer to a default import option parameters
      virtual void SetDefImportOpt (ImportOption_t* defim) {
         fDefImportOpt = defim; }
      /// Get a pointer to a default import option parameters
      virtual ImportOption_t* GetDefImportOpt() const {
         return fDefImportOpt; }
      /// Imports data from file showing the import option dialog first
      virtual Bool_t ImportDlg (ImportOption_t* defim = 0);
      /// Set a pointer to a default export option parameters
      virtual void SetDefExportOpt (ExportOption_t* defex) {
         fDefExportOpt = defex; }
      /// Get a pointer to a default export option parameters
      virtual ExportOption_t* GetDefExportOpt() const {
         return fDefExportOpt; }
      /// Exports data to file showing the export option dialog first
      virtual Bool_t ExportDlg (ExportOption_t* defex = 0);
      /// Set a pointer to a list of reference traces
      virtual void SetReferenceTraces (ReferenceTraceList_t* ref) {
         fRefTraces = ref; }
      /// Shows the reference traces dialog
      virtual Bool_t ReferenceTracesDlg (ReferenceTraceList_t* ref = 0);
      /// Set a pointer to a table of math functions
      virtual void SetMathTable (MathTable_t* math = 0) {
         fMathTable = math; }
      /// Shows the math editor dialog
      virtual Bool_t MathDlg (MathTable_t* math = 0);
      /// Set a pointer to a callibration table
      virtual void SetCalibrationTable (calibration::Table* cal) {
         fCalTable = cal; }
      /// Get a pointer to a default export option parameters
      virtual calibration::Table* GetCalibrationTable() const {
         return fCalTable; }
      /// Shows the calibration edit dialog
      virtual Bool_t CalibrationEditDlg (calibration::Table* cal = 0);
      /// Shows the calibration import dialog
      virtual Bool_t CalibrationImportDlg (calibration::Table* cal = 0);
      /// Shows the calibration exportdialog
      virtual Bool_t CalibrationExportDlg (calibration::Table* cal = 0);
      /// Get the reset plot settings object pointer
      virtual void SetActionPlotPads (ActionPlotPads* action) {
         fAction = action; }
      /// Resets the plot settings in the pads to their defaults
      virtual void ResetPads();
      /// Fill a plot set with references to the displayed traces
      virtual Bool_t Fill (PlotSet& pl, const char* winid = 0);
   
      /// Shows the option dialog box
      virtual Bool_t OptionDlg();
      /// Create a new main window which uses the current as a template
      virtual TLGPadMain* NewWindow();
   
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   
   };


   const int kLayoutNum = 21;

/** A dialogbox for selecting a multi pad grid. For internal use only.
   
    @memo Multi pad layout dialogbox.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGLayoutDialog : public TLGTransientFrame {
   
   protected:
      /// Layout pointer
      Int_t*		fLayout;
      /// Currently selected layout
      Int_t		fSelected;	// value range 1-16, 101-105
      /// Layout selection frame
      TGCompositeFrame*	fLayoutFrame;
      /// Option tab panel
      TGRadioButton*	fLayoutSelection[kLayoutNum];
      /// Button frame
      TGCompositeFrame*	fButtonFrame;
      /// OK button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
      /// Layout hints for buttons
      TGLayoutHints*	fL1;
      /// Layout hints for option tab and button frame
      TGLayoutHints*	fL2;
   
   public:
      TLGLayoutDialog (const TGWindow *p, const TGWindow *main,
                      Int_t* layout);
      virtual ~TLGLayoutDialog ();
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };



/** A main window which displays one or multiple plots. 
   
    @memo Multiple plot main window.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGPadMain : public VirtualPlotWindow, public TLGMainFrame,
   public TLGMainMenu {
      TLGPadMain (const TLGPadMain&);
      TLGPadMain& operator= (const TLGPadMain&);
   
   protected:
      /// Name of window
      TString		fName;
      /// Data pool
      PlotSet*		fPlotSet;
      /// Layout hints
      TGLayoutHints*    fL;
      /// Multipad
      TLGMultiPad*	fMPad;
      /// Init the window
      virtual void Init (Bool_t decorate_menu);
   
   public:
      /// Create new plot pad main window
      TLGPadMain (const TGWindow *p, PlotSet& plots,  
                 const char* name = 0, UInt_t w = 600, UInt_t h = 400, 
                 UInt_t options = kMainFrame | kVerticalFrame);
      /// Create new plot pad main window with a menu
      TLGPadMain (const TGWindow *p, PlotSet& plots,  
                 Bool_t decorate_menu,
                 const char* name = 0, UInt_t w = 600, UInt_t h = 400, 
                 UInt_t options = kMainFrame | kVerticalFrame);
      /// Destroy window
      virtual ~TLGPadMain();
   
      /// Get the multi pad object
      virtual TLGMultiPad* GetPads() const {
         return fMPad; }
      /// Close the window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
   };


//@}
#ifndef __NO_NAMESPACE
}
#endif


#endif
