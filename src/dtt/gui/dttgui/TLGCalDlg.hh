/* Version $Id: TLGCalDlg.hh 7193 2014-10-10 17:25:24Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGCALDLG_H
#define _LIGO_TLGCALDLG_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGCalDlg						*/
/*                                                         		*/
/* Module Description: Calibration dialog box.		      		*/
/* 							   		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 17Mai00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGCalDlg.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

//#include "Table.hh"
#include "Time.hh"
#include <string>
#include <map>
#include <TGFrame.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTab.h>
#include <TGButton.h>
#include "TLGFrame.hh"
#include "TDGTextEdit.h"

#if defined(__SUNPRO_CC) && defined(G__DICTIONARY)
   namespace ligogui {}
   using namespace ligogui;
#endif

   class PlotSet;

namespace calibration {
   class Table;
   class Calibration;
}

namespace ligogui {

   class TLGTextEntry;
   class TLGNumericControlBox;

/** @name TLGCalDlg
    This header supports editing, importing and exporting of calibration
    records.

    The calibration table is defined in "TLGCal.h".
   
    @memo Calibration dialog boxes
    @author Written Mai 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** @name Constants
    Constants for math option parameters, messages and widget IDs.
   
    @memo Constants
    @author Written Mai 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

   /// Widget ID of ok button in calibration dialog
   const int kCalEditOk = 1;
   /// Widget ID of cancel button in calibration dialog
   const int kCalEditCancel = 2;
   /// Widget ID of new button in calibration dialog
   const int kCalEditNew = 3;
   /// Widget ID of delete button in calibration dialog
   const int kCalEditClear = 4;
   /// Widget ID of set all button in calibration dialog
   const int kCalEditSet = 5;
   /// Widget ID of channel listbox in calibration dialog
   const int kCalEditChn = 10;
   /// Widget ID of enable channel in calibration dialog
   const int kCalEditEnable = 11;
   /// Widget ID of reference point in calibration dialog
   const int kCalEditRef = 12;
   /// Widget ID of unit in calibration dialog
   const int kCalEditUnit = 13;
   /// Widget ID of time in calibration dialog
   const int kCalEditTime = 14;
   /// Widget ID of default calibration in calibration dialog
   const int kCalEditDefault = 15;
   /// Widget ID of conversion selection in calibration dialog
   const int kCalEditConvSel = 20;
   /// Widget ID of conversion in calibration dialog
   const int kCalEditConv = 21;
   /// Widget ID of offset selection in calibration dialog
   const int kCalEditOffsSel = 22;
   /// Widget ID of offset in calibration dialog
   const int kCalEditOffs = 23;
   /// Widget ID of time delay selection  in calibration dialog
   const int kCalEditDelaySel = 24;
   /// Widget ID of time delay in calibration dialog
   const int kCalEditDelay = 25;
   /// Widget ID of pole zero selection in calibration dialog
   const int kCalEditPoleZeroSel = 26;
   /// Widget ID of gain in calibration dialog
   const int kCalEditGain = 27;
   /// Widget ID of poles in calibration dialog
   const int kCalEditPoles = 28;
   /// Widget ID of zeros in calibration dialog
   const int kCalEditZeros = 28;
   /// Widget ID of transfer fucntions selection in calibration dialog
   const int kCalEditTransSel = 29;
   /// Widget ID of transfer fucntions edit in calibration dialog
   const int kCalEditTransEdit = 30;
   /// Widget ID of transfer fucntions read in calibration dialog
   const int kCalEditTransRead = 31;
   /// Widget ID of magnitude selection in calibration dialog
   const int kCalEditMagSel = 32;
   /// Widget ID of magnitude in calibration dialog
   const int kCalEditMag = 33;
   /// Widget ID of derivative selection in calibration dialog
   const int kCalEditDSel = 34;
   /// Widget ID of derivative in calibration dialog
   const int kCalEditD = 35;
   /// Widget ID of comment in calibration dialog
   const int kCalEditComment = 36;
   /// Widget ID of Copy button in calibration dialog
   const int kCalEditCopy = 37 ;
   /// Widget ID of Paste button in calibration dialog
   const int kCalEditPaste = 38 ;

//@}


/** Function to bring up the calibration table dialog box. 
    @memo Calibration table dialog.
    @param pset Plot set
    @param cal Calibration table
    @return true if changes were made
 ************************************************************************/
   Bool_t CalibrationTableDlg (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, calibration::Table& cal);


/** Function to import a calibration records from file. 
    @memo Calibration table import function.
    @param pset Plot set
    @param cal Calibration table
    @return true if changes were made
 ************************************************************************/
   Bool_t  CalibrationTableImport (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, calibration::Table& cal);

/** Function to export a calibration records from file. 
    @memo Calibration table export function.
    @param pset Plot set
    @param cal Calibration table
    @return true if successful
 ************************************************************************/
   Bool_t  CalibrationTableExport (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, calibration::Table& cal);


/** Calibration editor.
    @memo Dialog box for entering and modifying calibration data.
    @author Written Mai 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGCalibrationDialog : public TLGTransientFrame {
   public:
   #ifndef __CINT__
      /// Compare two channel names
      struct ChannelNameCmp {
         bool operator () (const std::string& c1, 
                          const std::string& c2) const;
      };
      typedef std::map<std::string, int, ChannelNameCmp> sortedlist;
   #else
      typedef std::map<std::string, int> sortedlist;
   #endif
   protected:
      /// Plot types
      PlotSet* 		fPlotList;
      /// Oldest plot desccriptor
      Time		fOldest;
      /// Calibration list
      calibration::Table*	fCal; 
      /// Currently selected channel
      TString		fCurChannel;
      /// Currently selected reference point
      TString		fCurRef;
      /// Currently selected unit
      TString		fCurUnit;
      /// Currently selected time
      TString		fCurTime;
      /// Currently selected time in GPS
      Time		fCurTimeGPS;
      /// True if a valid channel, reference, unit and time are selected
      Bool_t		fSelValid;
      /// Currently selected calibration record
      calibration::Calibration* fSelCal;
      /// Return parameter
      Bool_t*		fOk;
      /// Modified flag
      Bool_t		fDirty;
      /// Reference point list
      sortedlist	fRefList;
      /// Unit list
      sortedlist	fUnitList;
      /// Time list
      sortedlist	fTimeList;
   
      /// top frame
      TGCompositeFrame* fFTop;
      /// Channel list box frame
      TGCompositeFrame* fFChn;
      /// Reference/unit/time selection frame
      TGCompositeFrame* fFRefUnit;
      /// calibration coeff. selection frame
      TGCompositeFrame* fFCoeff;
      /// Button frame
      TGCompositeFrame* fFButton;
      /// 1st line
      TGCompositeFrame* fF1;
      /// 2nd line
      TGCompositeFrame* fF2;
      /// 3rd line
      TGCompositeFrame* fF3;
      /// 4rd line
      TGCompositeFrame* fF4;
      /// 5th line
      TGCompositeFrame* fF5;
      /// Numereous layout hints
      TGLayoutHints*	fL[16];
      /// Reference selection list box
      TGListBox*	fChnSel;
      /// Reference point label
      TGLabel*		fRefLabel;
      /// Unit label
      TGLabel*		fUnitLabel;
      /// Time label
      TGLabel*		fTimeLabel;
      /// Enable selection
      TGCheckButton*	fEnable;
      /// Reference selection
      TGComboBox*	fRef;
      /// Unit selection
      TGComboBox*	fUnit;
      /// Time selection
      TGComboBox*	fTime;
      /// Default selection
      TGCheckButton*	fDefault;
      /// Calibration edit tab
      TGTab*		fTab;
      /// Time domain tab frame
      TGCompositeFrame* fTabTime;
      /// Pole/zero tab frame
      TGCompositeFrame* fTabFreq;
      /// transfer function frame
      TGCompositeFrame* fTabTrans;
      /// Defaults tab frame
      TGCompositeFrame* fTabDef;
      /// Comment
      TGCompositeFrame* fTabComment;
      /// tab lines
      TGCompositeFrame* fFT[12];
      /// Conversion selection
      TGCheckButton*	fConvSel;
      /// Offset selection
      TGCheckButton*	fOffsSel;
      /// Time delay selection
      TGCheckButton*	fDelaySel;
      /// Pole zero selection
      TGCheckButton*	fPoleZeroSel;
      /// Transfer function selection
      TGCheckButton*	fTransSel;
      /// Conversion 
      TLGNumericControlBox*	fConv;
      /// Offset 
      TLGNumericControlBox*	fOffs;
      /// Time delay 
      TLGNumericControlBox*	fDelay;
      /// Gain label
      TGLabel*		fGainLabel;
      /// Pole label
      TGLabel*		fPoleLabel;
      /// Zero label
      TGLabel*		fZeroLabel;
      /// Gain
      TLGNumericControlBox*	fGain;
      /// Poles
      TLGTextEntry*	fPoles;
      /// Zeros
      TLGTextEntry*	fZeros;
      /// Function
      TGLabel*		fFunctionLabel;
      /// Transfer function edit button
      TGButton* 	fTransEditButton;
      /// Preferred magnitude selection
      TGCheckButton*	fMagSel;
      /// Preferred derivative selection
      TGCheckButton*	fDSel;
      /// Preferred magnitude 
      TLGNumericControlBox*	fMag;
      /// Preferred derivative 
      TLGNumericControlBox*	fD;
      /// Comment
      TDGTextEdit*	fComment;
      /// Help label
      TGLabel*		fHelpLabel;
      /// New button
      TGButton* 	fNewButton;
      /// Set button
      TGButton*		fSetButton;
      /// Clear button
      TGButton*		fClearButton;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
      /// Copy button
      TGButton		*fCopyButton ;
      /// Paste button
      TGButton          *fPasteButton ;

      // Calibration to hold copied values.
      calibration::Calibration* fCopyCal;
      TString		fCopyRef ;
      TString		fCopyUnit ;
      Time		fCopyTime ;
   
   public:
      /// Constructor
      TLGCalibrationDialog (const TGWindow *p, const TGWindow *main,
                        PlotSet& pset, calibration::Table& cal,
                        Bool_t& ret);
      /// Destructor
      virtual ~TLGCalibrationDialog ();
      /// Close window method
      virtual void CloseWindow();
   
      /// Has anything been changed?
      virtual Bool_t IsDirty() const {
         return fDirty; }
      /// Set/reset change bit
      virtual void SetDirty (Bool_t state = kTRUE);
   
      /// Show calibration listbox/selection
      virtual void ShowCal (Int_t level);
      /// Build channel list
      virtual void BuildChannelList();
      /// Edit transfer function
      virtual Bool_t EditTransferFunction();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   
   protected:
      /// Transfer data from GUI to calibration record
      void Transfer (calibration::Calibration& cal);
   // variables to hold what is being copied.

   };
}

#endif // _LIGO_TLGCALDLG_H

