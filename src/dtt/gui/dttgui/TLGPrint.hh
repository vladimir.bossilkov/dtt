/* Version $Id: TLGPrint.hh 6788 2013-02-01 19:41:47Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGPRINT_H
#define _LIGO_TLGPRINT_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGPrint						*/
/*                                                         		*/
/* Module Description: Support functions for printing.	      		*/
/* 							   		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 17Mai00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGPrint.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "TLGFrame.hh"
#include <TGFrame.h>
#include <TPostScript.h>
#include <TString.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>


namespace ligogui {

/** @name TLGPrint
    This header exports functions to supporting printing.
   
    @memo Print functions and options
    @author Written Mai 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** Modified postscript class. Supports multiple page/zone PS files.
   
    @memo Modified postscript class.
    @author Written Mai 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGPostScript : public TPostScript {
   
   public:
      TLGPostScript () {
      }
      TLGPostScript (const char* fname, Int_t wtype) {
         Open (fname, wtype);
      }
      virtual void Open (const char* fname, Int_t wtype);
      virtual void NewPage();
   };


/** This structure is used by the print functions to store printing
    parameters and setup values.
   
    @memo Print parameters.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGPrintParam {
   public:
      /// Print or Print setup
      enum EPrintDialog {
      /// Print
      kPrint = 0,
      /// Print Setup
      kPrintSetup = 1
      };
      /// Output formats
      enum EPFileFormat {
      /// Postscript
      kPostscript = 0,
      /// PDF
      kPDF = 1,
      /// EPS
      kEPS = 2,
      /// EPSI
      kEPSI = 3,
      /// JPEG
      kJPEG = 4,
      /// PNG
      kPNG = 5,
      /// Adobe Illustrator
      kAI = 6
      };
   
      /// Default constructor
      TLGPrintParam ();
   /** Constructor
      \begin{verbatim}
          format: 'orientation''layout''selection':'file'\n"
                  Orientation - P (portrait), L (landscape, default)\n"
                  Layout - 1 (default), 2 or 4\n"
                  Selection - F (1st, default), S (2nd), A (all)\n"
                  File - extension determines plot format\n"
                         no extension assumes printer name\n"
      \end{verbatim}
    *********************************************************************/
      explicit TLGPrintParam (const char* format);
      /// Destructor
      ~TLGPrintParam ();
   
      /// Init parameters
      void init();
   
      /// Show a dialog box: Returns true if user quits with OK.
      Bool_t ShowDialog (const TGWindow* p, const TGWindow* main,
                        EPrintDialog pd = kPrint);
   
   /** Routine to obtain a list of printer names. The returned list
       contains a list of printer names. Each name is terminetd by
       a newline character. The length of the return list must be at 
       least 32 characters It should be long enough to store all
       printer names on the system.
   
       @param list Will contain printer names upon return
       @param maxlen Maximum size of passed list array
       @return true if successful
       @memo List of printer names.
    *********************************************************************/
      static Bool_t GetPrinterList (char* list, int maxlen);
   
   /** Check supported output formats. Returns true if specified format
       is supported for printing or storing to file. Error codes are
       -1 if ghostscript is not installed and -2 if epstool is not 
       installed.
   
       @param format Output format
       @param errcode Error code (return)
       @return true if supported
       @memo Check output formats.
    *********************************************************************/
      Bool_t IsFormatSupported();
   
   /** Setup for printing. This will check the print parameters,
       build a filename for the output and constructs the print command.
       Use the returned filename to print a (encapsulated) postscript 
       file and then call finish send the file to the printer or convert 
       to its final format.
   
       @param filename Name of print file (return)
       @return true if successful
       @memo Setup print job.
    *********************************************************************/
      Bool_t Setup (TString& filename);
   
   /** Finish printing. This sends the file to the printer or converts 
       to the final format. Also cleans up any temporary files.
   
       @param cleanuponly Only cleans up, no printing
       @return true if successful
       @memo Finish print job.
    *********************************************************************/
      Bool_t Finish (Bool_t cleanuponly = kFALSE);
   
      /// Get error code
      Int_t ErrorCode() const {
         return fErrorCode; }
   
      /// Printer name
      TString		fPrinter;
      /** Print command: %File will be replaced with the filename,
          will be replaced with the printer name.
          Default is "lp -c -d%Printer %File" 
          The %Printer string and the option right before it (no space!) 
          will be removed if printing to the default printer. */
      TString 		fPrintCommand;
      /// Print to file
      Bool_t		fPrintToFile;
      /// Output format for printing to file
      EPFileFormat	fFileFormat;
      /// Print file name
      TString		fFilename;
      /// Paper size width (in cm)
      Float_t		fPaperSizeWidth;
      /// Paper size height (in cm)
      Float_t		fPaperSizeHeight;
      /// Page Layout: 0 - 1 up, 1 - 2 up, 2 - 4 up.
      Int_t		fPageLayout;
      /// Page orientaion: 0 - Portrait, 1 - Landscape
      Int_t		fPageOrientation;
      /** Plot selection: 0 - current pad, 1 - first pad, 
          2 - second pad, 3 - all pads */
      Int_t		fPlotSelection;
   
   protected:
      /// Error code
      Int_t		fErrorCode;
      /// Output filename
      TString		fOutFilename;
      /// Do we use a temporary file?
      Bool_t 		fTempfileUse;
      /// Print command
      TString		fPrintCmd;
   };


/** Dialog box for selecting printer and printing setup.
   
    @memo Dialog box for printing.
    @author Written Mai 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGPrintDialog : public TLGTransientFrame {
   public:
      // Print dialog type
      typedef TLGPrintParam::EPrintDialog EPrintDialog;
   
   protected:
      /// Print parameters
      TLGPrintParam*	fPDlg;
      /// Return parameter
      Bool_t*		fOk;
      /// Print dialog type
      EPrintDialog	fPrintDlgType;
      /// Printer group
      TGGroupFrame*	fGroupPrinter;
      /// Layout group
      TGGroupFrame*	fGroupLayout;
      /// Orientation group
      TGGroupFrame*	fGroupOrientation;
      /// Pad selection group
      TGGroupFrame*	fGroupSelection;
      /// Numereous frames
      TGCompositeFrame*	fFrame[7];
      /// Numereous layout hints
      TGLayoutHints*	fL[6];
      /// Printer label
      TGLabel*		fLPrinter;
      /// Printer selction combobox
      TGComboBox*	fPrinter;
      /// Paper size label
      TGLabel*		fLPaperSize;
      /// Paper size combobox
      TGComboBox*	fPaperSize;
      /// Print command label
      TGLabel*		fLPrintCommand;
      /// Print commmand edit box
      TGTextEntry*	fPrintCommand;
      /// Print to file check button
      TGButton*		fPrintToFile;
      /// Output file format
      TGComboBox*	fFileFormat;
      /// Layout radio buttons
      TGButton*		fLayout[3];
      /// Orientation radio buttons
      TGButton*		fOrientation[2];
      /// Pad selection radio buttons
      TGButton*		fSelection[4];
      /// Button frame
      TGCompositeFrame*	fButtonFrame;
      /// OK button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
   public:
      /// Create a print dialog box
      TLGPrintDialog (const TGWindow *p, const TGWindow *main, 
                     TLGPrintParam& pdlg, Bool_t& ret,
                     EPrintDialog pd = TLGPrintParam::kPrint);
      /// Destroy the print dialog box
      virtual ~TLGPrintDialog();
      /// Close the window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//@}
}

#endif // _LIGO_TLGPRINT_H
