/* -*- mode: c++; c-basic-offset: 3; -*- */
/* version $Id: TLGOptions.cc 7225 2014-11-19 17:46:02Z james.batch@LIGO.ORG $ */
#include "TLGOptions.hh"
#include "PlotSet.hh"
#include "TLGEntry.hh"
#include "TLGColor.hh"
#include <stdlib.h>
#include <string.h>
#include <iostream>
// #include <TAttTextCanvas.h>
#include <TGListBox.h>
#include <TGMsgBox.h>
#include <TGTableLayout.h>
#include <TVirtualX.h>

static const int my_debug = 0 ;

namespace ligogui {
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// OptionArray                                                          //
//                                                                      //
// Array of options	                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   OptionArray::OptionArray (Int_t maxwin, Int_t maxpad) 
   : fMaxWin (maxwin), fMaxPad (maxpad) 
   {
      fOptions = new OptionMatrix_t [fMaxWin];
      for (int i = 0; i < fMaxWin; i++) {
         fOptions[i] = new OptionArray_t [fMaxPad];
         for (int j = 0; j < fMaxPad; j++) {
            fOptions[i][j] = 0;
         }
      }
   }

//______________________________________________________________________________
   OptionArray::~OptionArray () 
   {
      for (int i = 0; i < fMaxWin; i++) {
         for (int j = 0; j < fMaxPad; j++) {
            if (fOptions[i][j]) delete fOptions[i][j];
         }
         delete [] fOptions[i]; 
      }
      delete [] fOptions; 
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptions                                                           //
//                                                                      //
// Graphics option (generic)                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptions::TLGOptions (const TGWindow* p, const char* optname, 
                     Int_t id, OptionValues_t* optvals)
   : TGCompositeFrame (p, kOptionWidth, kOptionHeight, 
                     kVerticalFrame | kFixedSize), 
   TGWidget (id)
   {
      fDefw = kOptionWidth;
      fDefh = kOptionHeight;
      SetOptionName (optname);
      SetOptionValues (optvals);
   }

//______________________________________________________________________________
   TLGOptions::~TLGOptions ()
   {
   }

//______________________________________________________________________________
   Bool_t TLGOptions::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      //cerr << "TLGOptions::ProcessMessage" << endl ;
      switch (GET_MSG (msg)) {
         // command messages
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  // buttons, combobox
                  case kCM_BUTTON:
                  case kCM_CHECKBUTTON:
                  case kCM_RADIOBUTTON:
                  case kCM_COMBOBOX:
                  case kCM_LISTBOX:
                     {
			//cerr << "  BUTTON, CHECKBUTTON, RADIOBUTTON, COMBOBOX, LISTBOX"	 << endl ;
                        ReadOptions();
                        SendUpdateMessage (parm1, parm2);
                        break;
                     }
               }
               break;
            }
         // textentry
         case kC_TEXTENTRY:
            {
               switch (GET_SUBMSG (msg)) {
                  case kTE_TEXTUPDATED:
                     {
			//cerr << "  kTE_TEXTUPDATED" << endl ;
                        ReadOptions();
                        SendUpdateMessage (parm1, parm2);
                        break;
                     }
               }
               break;
            }
         // list tree (e.g., channel selection listbox
         case kC_LISTTREE:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCT_ITEMCLICK:
                     {
			//cerr << "  kCT_ITEMCLICK" << endl ;
                        ReadOptions();
                        SendUpdateMessage (parm1, parm2);
                        break;
                     }
               }
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGOptions::SendUpdateMessage (Long_t parm1, Long_t parm2)
   {
      const Long_t update = MK_MSG ((EWidgetMessageTypes) kC_OPTION, 
                           (EWidgetMessageTypes) kCM_OPTCHANGED);
      //cerr << "TLGOptions: Send update message to fMsgWindow" << endl ;
      SendMessage (fMsgWindow, update, 0, parm1);
   }

//______________________________________________________________________________
   void TLGOptions::SendCursorMessage (Long_t parm1, Long_t parm2)
   {
      const Long_t cupdate = MK_MSG ((EWidgetMessageTypes) kC_OPTION, 
                           (EWidgetMessageTypes) kCM_OPTCURSOR);
      SendMessage (fMsgWindow, cupdate, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TGTabSmallElement                                                    //
//                                                                      //
// Traces tab                                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   class TGTabSmallElement : public TGTabElement {
   public:
      TGTabSmallElement (const TGWindow *p, TGString *text, 
                        UInt_t w, UInt_t h,
                        GContext_t norm, FontStruct_t font,
                        UInt_t options = kRaisedFrame, 
                        ULong_t back = fgDefaultFrameBackground) 
      : TGTabElement (p, text, w, h, norm, font, options, back) {
      }
      virtual TGDimension GetDefaultSize() const
      {
         return TGDimension(TMath::Max(fTWidth+12, (UInt_t)38), fTHeight+6);
      }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TGTabTraces                                                          //
//                                                                      //
// Traces tab                                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   class TGTabTraces : public TGTab {
   public:
      TGTabTraces (const TGWindow* p) : TGTab (p, 10, 10) {
      }
      virtual TGCompositeFrame* AddTab (TGString *text) {
         TGCompositeFrame *cf;
         AddFrame (new TGTabSmallElement(this, text, 50, 20, fNormGC, 
                                    fFontStruct), 0);
         cf = new TGCompositeFrame(this, fWidth, fHeight-21);
         AddFrame(cf, 0);
	 cf->SetEditDisabled(kEditDisableResize);
         return cf;
      }
      virtual TGCompositeFrame* AddTab(const char *text) {
         return TGTab::AddTab (text); 
      }
#if ROOT_VERSION_CODE >= ROOT_VERSION(5,18,0)
      virtual void AddTab(TGString *text, TGCompositeFrame *cf) {
         AddFrame (new TGTabSmallElement(this, text, 50, 20, fNormGC, 
                                    fFontStruct), 0);
         AddFrame(cf, 0);
	 cf->SetEditDisabled(kEditDisableResize);
      }
      virtual void AddTab(const char *text, TGCompositeFrame *cf) {
	 TGTab::AddTab(text, cf);
      }
#endif
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionTraces                                                      //
//                                                                      //
// Graphics option: Traces                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionTraces::TLGOptionTraces (const TGWindow* p, Int_t id, 
                     OptionTraces_t* optvals, const PlotMap& plotlist)
   : TLGOptions (p, kGOptTracesName, id, optvals)
   {
      fPlotList = &plotlist;
      fCurTrace = 0;
   
      // Layout hints
      fL1 = new TGLayoutHints (kLHintsLeft | kLHintsTop, 2, 2, 2, 2);
      fL2 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 2, 2, 2, 2);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 2, 2, 2);
      fL4 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 2, 0, 2, 2);
      fL5 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 2, 2, 6, 2);
      fL6 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, -2, -2, 4, 2);
      fL7 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, -4, -4, 2, 0);
      fL8 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 2, 0, 2, 0);
      fL9 = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 3, 0, 0, 0);
      fL10 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, -4, -4, 2, -4);
   
      // Graph type
      fGTF = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fGTF, fL5);
      fGraphTypeLabel = new TGLabel (fGTF, "Graph:");
      fGTF->AddFrame (fGraphTypeLabel, fL3);
      fGraphType = new TGComboBox (fGTF, kGOptTraceGraphType);
      fGraphType->Associate (this);
      fGraphType->SetHeight (22);
      fGTF->AddFrame (fGraphType, fL4);
   
      // Trace tab
      fTraces = new TGTabTraces (this);
      fTraces->Associate (this);
      AddFrame (fTraces, fL6);
      for (int i = 0; i < kMaxTraces; i++) {
         char label[4];
         sprintf (label, "%i", i);
         fTraces->AddTab (label);
      }
   
      // Create trace elements
      // active box
      fActive = new TGCheckButton (this, "Active", 
                           kGOptTraceActive);
      fActive->Associate (this);
      AddFrame (fActive, fL5);
      // Channel group
      fCF = new TGGroupFrame (this, "Channels");
      AddFrame (fCF, fL2);
      // A channel
      fAF = new TGHorizontalFrame (fCF, 10, 10);
      fCF->AddFrame (fAF, fL7);
      fAChannelLabel = new TGLabel (fAF, "A:");
      fAF->AddFrame (fAChannelLabel, fL3);
      fAChannel = new TGComboBox (fAF, kGOptTraceAChannel);
      fAChannel->Associate (this);
      fAChannel->SetHeight (22);
      fAF->AddFrame (fAChannel, fL4);
      // B channel
      fBF = new TGHorizontalFrame (fCF, 10, 10);
      fCF->AddFrame (fBF, fL10);
      fBChannelLabel = new TGLabel (fBF, "B:");
      fBF->AddFrame (fBChannelLabel, fL3);
      fBChannel = new TGComboBox (fBF, kGOptTraceBChannel);
      fBChannel->Associate (this);
      fBChannel->SetHeight (22);
      fBF->AddFrame (fBChannel, fL8);
      // Style group
      fSF = new TGGroupFrame (this, "Style");
      AddFrame (fSF, fL2);
      // Line
      fLF = new TGHorizontalFrame (fSF, 10, 10);
      fSF->AddFrame (fLF, fL7);
      fLine = new TGCheckButton (fLF, "Line", kGOptTraceLine);
      fLine->Associate (this);
      fLF->AddFrame (fLine, fL1);
      fLineSize = new TLGNumericControlBox (fLF, 0, 4, kGOptTraceLineSize, 
                           kNESInteger, kNEAPositive); // Bugzilla 761 - Was real, non-negative.
      fLineSize->Associate (this);
      fLF->AddFrame (fLineSize, fL9);
      fLineStyle = new TLGLineStyleComboBox (fLF, kGOptTraceLineStyle);
      fLineStyle->Associate (this);
      fLF->AddFrame (fLineStyle, fL9);
      fLineColor = new TLGColorComboBox (fLF, kGOptTraceLineColor);
      fLineColor->Associate (this);
      fLF->AddFrame (fLineColor, fL9);
   
      // Marker
      fMF = new TGHorizontalFrame (fSF, 10, 10);
      fSF->AddFrame (fMF, fL7);
      fMarker = new TGCheckButton (fMF, "Symbol", kGOptTraceMarker);
      fMarker->Associate (this);
      fMF->AddFrame (fMarker, fL1);
      fMarkerSize = new TLGNumericControlBox (fMF, 0, 4,
                           kGOptTraceMarkerSize, kNESRealOne,
                           kNEANonNegative);
      fMarkerSize->Associate (this);
      fMF->AddFrame (fMarkerSize, fL9);
      fMarkerStyle = new TLGMarkerStyleComboBox (fMF, kGOptTraceMarkerStyle);
      fMarkerStyle->Associate (this);
      fMF->AddFrame (fMarkerStyle, fL9);
      fMarkerColor = new TLGColorComboBox (fMF, kGOptTraceMarkerColor);
      fMarkerColor->Associate (this);
      fMF->AddFrame (fMarkerColor, fL9);
   
      // Bar
      fFF = new TGHorizontalFrame (fSF, 10, 10);
      fSF->AddFrame (fFF, fL10);
      fBar = new TGCheckButton (fFF, "Bar", kGOptTraceBar);
      fBar->Associate (this);
      fFF->AddFrame (fBar, fL1);
      fBarSize = new TLGNumericControlBox (fFF, 0, 4, kGOptTraceBarSize,
                           kNESRealTwo, kNEANonNegative);
      fBarSize->Associate (this);
      fFF->AddFrame (fBarSize, fL9);
      fBarStyle = new TLGFillStyleComboBox (fFF, kGOptTraceBarStyle);
      fBarStyle->Associate (this);
      fFF->AddFrame (fBarStyle, fL9);
      fBarColor = new TLGColorComboBox (fFF, kGOptTraceBarColor);
      fBarColor->Associate (this);
      fFF->AddFrame (fBarColor, fL9);
   
      // build plot type entry lists
      UpdateOptions();
   }

//______________________________________________________________________________
   TLGOptionTraces::~TLGOptionTraces ()
   {
      delete fGraphTypeLabel;
      delete fGraphType;
      delete fActive;
      delete fAChannelLabel;
      delete fAChannel;
      delete fAF;
      delete fBChannelLabel;
      delete fBChannel;
      delete fBF;
      delete fCF;
      delete fLine;
      delete fLineColor;
      delete fLineStyle;
      delete fLineSize;
      delete fLF;
      delete fMarker;
      delete fMarkerColor;
      delete fMarkerStyle;
      delete fMarkerSize;
      delete fMF;
      delete fBar;
      delete fBarColor;
      delete fBarStyle;
      delete fBarSize;
      delete fFF;
      delete fSF;
      delete fTraces;
      delete fGTF;
   
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
      delete fL7;
      delete fL8;
      delete fL9;
      delete fL10;
   }

//______________________________________________________________________________
   void TLGOptionTraces::BuildPlotType (Int_t level)
   {
      OptionTraces_t*	vals = (OptionTraces_t*) fOptionValues;
      if (vals == 0) {
         return;
      }
      // rebuild graph selection
      if (level <= 0) {
         // build a new graph selection list
         fGraphType->RemoveEntries (0, 1000);
         const PlotListLink* g = fPlotList->Get();
         if (g != 0) {
            g = g->Child();
         }
         //const PlotListLink* gsave = g;
         if (g != 0) {
            while (g != 0) {
               fGraphType->AddEntry (g->GetName(), 
                                    fPlotList->GetId (g->GetName()));
               g = g->Next();
            }
            // set selected graph
            Int_t id = fPlotList->GetId (vals->fGraphType);
            if (id >= 0) {
               fGraphType->Select (id);
               // vals->fGraphTypeValid = kTRUE;
            }
            else {
               // clean top entry
               fGraphType->SetTopEntry 
                  (new TGTextLBEntry (fGraphType, new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                    kLHintsExpandX));
               fGraphType->MapSubwindows();
               // vals->fGraphTypeValid = kFALSE;
               // fGraphType->Select (0);
               // vals->fGraphType = gsave->GetName();
            }
         }
         else {
            // clean top entry
            fGraphType->SetTopEntry 
               (new TGTextLBEntry (fGraphType, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fGraphType->MapSubwindows();
            // vals->fGraphTypeValid = kFALSE;
            // vals->fGraphType = "";
         }
      
      }
      // rebuild A channel selection
      if (level <= 1) {
         // build a new A channel selection list
         fAChannel->RemoveEntries (0, 1000);
         const PlotListLink* a = fPlotList->Get (vals->fGraphType);
         if (a != 0) {
            a = a->Child();
         }

	 // Make sure list box is an integral multiple of the height of an entry.
	 fAChannel->GetListBox()->IntegralHeight(kTRUE) ;	// JCB

         //const PlotListLink* asave = a;
         if (a != 0) {
            while (a != 0) {
               fAChannel->AddEntry 
                  (a->GetName(), fPlotList->GetId (
                                    vals->fGraphType, a->GetName()));
               a = a->Next();
            }
            // set selected graph
            Int_t id = fPlotList->GetId 
               (vals->fGraphType, vals->fAChannel[fCurTrace]);
            if (id >= 0) {
               fAChannel->Select (id);
            }
            else {
               // clean A channel top entry
               fAChannel->SetTopEntry 
                  (new TGTextLBEntry (fAChannel, new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                    kLHintsExpandX));
               fAChannel->MapSubwindows();
            }
            // for (Int_t tr = 0; tr < kMaxTraces; tr++) {
               // Int_t id = fPlotList->GetId (vals->fGraphType, vals->fAChannel[tr]);
               // if (id < 0) {
               //    vals->fAChannel[tr] = asave->GetName();
               //    vals->fActive[tr] = (tr == 0);
               //    id = 0;
               //    vals->fAChannelValid[tr] = kFALSE;
               // }
               // else {
               //    vals->fAChannelValid[tr] = kTRUE;
               // }  
               // if ((tr == fCurTrace) && (id >= 0)) {
                  // fAChannel->Select (id);
               // }
            // }
         }
         else {
            // clean A channel top entry
            fAChannel->SetTopEntry 
               (new TGTextLBEntry (fAChannel, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fAChannel->MapSubwindows();
            //for (Int_t tr = 0; tr < kMaxTraces; tr++) {
               // vals->fAChannel[tr] = "";
               // vals->fAChannelValid[tr] = kFALSE;
            //}
         }
	 // Resize the list box
	 fAChannel->GetListBox()->Resize(100,100) ; // JCB
      }
      // rebuild B channel selection
      if (level <= 2) {
         // build a new B channel selection list
         fBChannel->RemoveEntries (0, 1000);
         const PlotListLink* b = fPlotList->Get (vals->fGraphType, 
                              vals->fAChannel[fCurTrace]);
         if (b != 0) {
            b = b->Child();
         }

	 // Make sure list box is an integral multiple of the height of an entry.
	 fBChannel->GetListBox()->IntegralHeight(kTRUE) ;	// JCB

         //const PlotListLink* bsave = b;
         if (b == 0) {
            // disable B selection combobox
            fBChannel->SetTopEntry 
               (new TGTextLBEntry (fBChannel, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fBChannel->MapSubwindows();
            //for (Int_t tr = 0; tr < kMaxTraces; tr++) {
               // vals->fBChannelValid[tr] = kFALSE;
               // vals->fBChannel[tr] = "";
            //}
         }
         else {
            // setup B channel list
            while (b != 0) {
               fBChannel->AddEntry 
                  (b->GetName(), fPlotList->GetId 
                  (vals->fGraphType, 
                  vals->fAChannel[fCurTrace], b->GetName()));
               b = b->Next();
            }
            // set selected graph
            Int_t id = fPlotList->GetId 
               (vals->fGraphType, vals->fAChannel[fCurTrace],
               vals->fBChannel[fCurTrace]);
            if (id >= 0) {
               fBChannel->Select (id);
            }
            else {
               // clean B channel top entry
               fBChannel->SetTopEntry 
                  (new TGTextLBEntry (fBChannel, new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                    kLHintsExpandX));
               fBChannel->MapSubwindows();
            }
            // for (Int_t tr = 0; tr < kMaxTraces; tr++) {
               // Int_t id = fPlotList->GetId (vals->fGraphType, 
                                    // vals->fAChannel[tr], vals->fBChannel[tr]);
               // if (id < 0) {
               //    // vals->fBChannelValid[tr] = kFALSE;
               //    // vals->fBChannel[tr] = bsave->GetName();
               //    // vals->fActive[tr] = (tr == 0);
               //    // id = 0;
               // }
               // else {
               //    // vals->fBChannelValid[tr] = kTRUE;
               // }
               // if ((tr == fCurTrace) && (id >= 0)) {
                  // fBChannel->Select (id);
               // }
            // }
         }
	 // Resize the list box
	 fBChannel->GetListBox()->Resize(100,100) ; // JCB
      }
   }

//______________________________________________________________________________
   void TLGOptionTraces::UpdateOptions ()
   {
      if (my_debug) cerr << "TLGOptionTraces::UpdateOptions() " << endl ;
      OptionTraces_t*	vals = (OptionTraces_t*) fOptionValues;
      BuildPlotType (0);
      if (vals != 0) {
         Int_t i = fCurTrace;
         fActive->SetState (vals->fActive[i] ? kButtonDown : kButtonUp);
         fLine->SetState ((vals->fPlotStyle[i] == kPlotStyleLine) || 
                         (vals->fPlotStyle[i] == kPlotStyleLineMarker) ? 
                         kButtonDown : kButtonUp);
         fLineColor->Select (vals->fLineAttr[i].GetLineColor());
         fLineStyle->Select (vals->fLineAttr[i].GetLineStyle());
         fLineSize->SetNumber (vals->fLineAttr[i].GetLineWidth());
         fMarker->SetState ((vals->fPlotStyle[i] == kPlotStyleMarker) ||
                           (vals->fPlotStyle[i] == kPlotStyleLineMarker) ? 
                           kButtonDown : kButtonUp);
         fMarkerColor->Select (vals->fMarkerAttr[i].GetMarkerColor());
         fMarkerStyle->Select (vals->fMarkerAttr[i].GetMarkerStyle());
         fMarkerSize->SetNumber (vals->fMarkerAttr[i].GetMarkerSize());
         fBar->SetState (vals->fPlotStyle[i] == kPlotStyleBar ? 
                        kButtonDown : kButtonUp);
         fBarColor->Select (vals->fBarAttr[i].GetFillColor());
         fBarStyle->Select (vals->fBarAttr[i].GetFillStyle());
         fBarSize->SetNumber (vals->fBarWidth[i]);
	 if (my_debug) cerr << "  fBarSize->SetNumber(" << vals->fBarWidth[i] << ")" << endl ;
      }
      if (my_debug) cerr << "TLGOptionTraces::UpdateOptions() end" << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionTraces::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionTraces_t* vals = (OptionTraces_t*) fOptionValues;
   
      //cerr << "TLGOptionTraces::ProcessMessage" << endl ;
      if ((GET_MSG (msg) == kC_COMMAND) && (vals != 0)) {
         switch (GET_SUBMSG (msg)) {
            case kCM_TAB:
               {
		  //cerr << "  kCM_TAB" << endl ;
                  if ((parm1 >= 0) && (parm1 < kMaxTraces)) {
                     fCurTrace = parm1;
                     UpdateOptions();
                  }
                  return kTRUE;
               }
            case kCM_COMBOBOX: 
               {
		  //cerr << "  kCM_COMBOBOX" << endl ;
                  // new graph type
                  if (parm1 == kGOptTraceGraphType) {
		     //cerr << "    new graph type" << endl ;
                     TGTextLBEntry* entry = 
                        (TGTextLBEntry*) fGraphType->GetSelectedEntry();
                     if (entry) {
                        // vals->fGraphTypeValid = kTRUE;
                        const char* sel = entry->GetText()->GetString();
                        if (vals->fGraphType == sel) {
                           return kTRUE;
                        }
                        vals->fGraphType = sel;
                     }
                     BuildPlotType (1);
                  }
                  // new A channel
                  else if (parm1 == kGOptTraceAChannel) {
                     TGTextLBEntry* entry = 
                        (TGTextLBEntry*) fAChannel->GetSelectedEntry();
		     //cerr << "    new A channel" << endl ;
                     if (entry) {
                        // vals->fAChannelValid[fCurTrace] = kTRUE;
                        const char* sel = entry->GetText()->GetString();
                        if (vals->fAChannel[fCurTrace] == sel) {
                           return kTRUE;
                        }
                        vals->fAChannel[fCurTrace] = sel;
                     }
                     BuildPlotType (2);
                  }
                  // new B channel
                  else if (parm1 == kGOptTraceBChannel) {
                     TGTextLBEntry* entry = 
                        (TGTextLBEntry*) fBChannel->GetSelectedEntry();
		     //cerr << "    new B channel" << endl ;
                     if (entry) {
                        // vals->fBChannelValid[fCurTrace] = kTRUE;
                        const char* sel = entry->GetText()->GetString();
                        if (vals->fBChannel[fCurTrace] == sel) {
                           return kTRUE;
                        }
                        vals->fBChannel[fCurTrace] = sel;
                     }
                  }
                  // line color 
                  if (parm1 == kGOptTraceLineColor) {
		     //cerr << "    line color" << endl ;
                     if (vals->fLineAttr[fCurTrace].GetLineColor() == parm2) {
                        return kTRUE;
                     }
                     vals->fLineAttr[fCurTrace].SetLineColor (parm2);
                  }
                  // line style 
                  else if (parm1 == kGOptTraceLineStyle) {
		     //cerr << "    line style" << endl ;
                     if (vals->fLineAttr[fCurTrace].GetLineStyle() == parm2) {
                        return kTRUE;
                     }
                     vals->fLineAttr[fCurTrace].SetLineStyle (parm2);
                  }
                  // marker color 
                  else if (parm1 == kGOptTraceMarkerColor) {
		     //cerr << "    marker color" << endl ;
                     if (vals->fMarkerAttr[fCurTrace].GetMarkerColor() == parm2) {
                        return kTRUE;
                     }
                     vals->fMarkerAttr[fCurTrace].SetMarkerColor (parm2);
                  }
                  // marker style 
                  else if (parm1 == kGOptTraceMarkerStyle) {
		     //cerr << "    marker style" << endl ;
                     if (vals->fMarkerAttr[fCurTrace].GetMarkerStyle() == parm2) {
                        return kTRUE;
                     }
                     vals->fMarkerAttr[fCurTrace].SetMarkerStyle (parm2);
                  }
                  // bar color 
                  else if (parm1 == kGOptTraceBarColor) {
		     //cerr << "    bar color" << endl ;
                     if (vals->fBarAttr[fCurTrace].GetFillColor() == parm2) {
                        return kTRUE;
                     }
                     vals->fBarAttr[fCurTrace].SetFillColor (parm2);
                  }
                  // fill style 
                  else if (parm1 == kGOptTraceBarStyle) {
		     //cerr << "    fill style" << endl ;
                     if (vals->fBarAttr[fCurTrace].GetFillStyle() == parm2) {
                        return kTRUE;
                     }
                     vals->fBarAttr[fCurTrace].SetFillStyle (parm2);
                  }
                  break;
               }
            case kCM_CHECKBUTTON:
               {
		  //cerr << "  kCM_CHECKBUTTON" << endl ;
                  // active trace button
                  if (parm1 == kGOptTraceActive) {
		     //cerr << "    active trace button" << endl ;
                     vals->fActive[fCurTrace] = 
                        fActive->GetState() == kButtonDown;
                  }
                  // line check button
                  else if (parm1 == kGOptTraceLine) {
		     //cerr << "    line check button" << endl ;
                     if (fLine->GetState() == kButtonDown) {
                        if (vals->fPlotStyle[fCurTrace] == kPlotStyleMarker) {
                           vals->fPlotStyle[fCurTrace] = kPlotStyleLineMarker;
                        }
                        else {
                           vals->fPlotStyle[fCurTrace] = kPlotStyleLine;
                        }
                     }
                     else {
                        if (vals->fPlotStyle[fCurTrace] == kPlotStyleLine) {
                           fMarker->SetState (kButtonDown);
                        }
                        vals->fPlotStyle[fCurTrace] = kPlotStyleMarker;
                     }
                     fBar->SetState (kButtonUp);
                  }
                  // marker check button
                  else if (parm1 == kGOptTraceMarker) {
		     //cerr << "    marker check button" << endl ;
                     if (fMarker->GetState() == kButtonDown) {
                        if (vals->fPlotStyle[fCurTrace] == kPlotStyleLine) {
                           vals->fPlotStyle[fCurTrace] = kPlotStyleLineMarker;
                        }
                        else {
                           vals->fPlotStyle[fCurTrace] = kPlotStyleMarker;
                        }
                     }
                     else {
                        if (vals->fPlotStyle[fCurTrace] == kPlotStyleMarker) {
                           fLine->SetState (kButtonDown);
                        }
                        vals->fPlotStyle[fCurTrace] = kPlotStyleLine;
                     }
                     fBar->SetState (kButtonUp);
                  }
                  // bar check button
                  else if (parm1 == kGOptTraceBar) {
		     //cerr << "    bar check button" << endl ;
                     if (fBar->GetState() == kButtonDown) {
                        fLine->SetState (kButtonUp);
                        vals->fPlotStyle[fCurTrace] = kPlotStyleBar;
                     }
                     else {
                        fLine->SetState (kButtonDown);
                        vals->fPlotStyle[fCurTrace] = kPlotStyleLine;
                     }
                     fMarker->SetState (kButtonUp);
                  }
                  break;
               }
         }
      }
      
      // numeric entry
      else if ((GET_MSG (msg) == kC_TEXTENTRY) && 
              (GET_SUBMSG (msg) == kTE_TEXTUPDATED) && (vals != 0)) {
	 //cerr << "  numeric entry" << endl ;
         // line width 
         if (parm1 == kGOptTraceLineSize) {
	    //cerr << "    line width" << endl ;
            vals->fLineAttr[fCurTrace].SetLineWidth (
					   Width_t(fLineSize->GetNumber()) );
         }
         // marker size 
         else if (parm1 == kGOptTraceMarkerSize) {
	    //cerr << "    marker size" << endl ;
            vals->fMarkerAttr[fCurTrace].SetMarkerSize (
                                 fMarkerSize->GetNumber());
         }
         // bar width 
         else if (parm1 == kGOptTraceBarSize) {
	    //cerr << "    bar width" << endl ;
	    if (my_debug) cerr << "TLGOptions::ProcessMessage() - fBarWidth set to " << fBarSize->GetNumber() << endl ;
            vals->fBarWidth[fCurTrace] = fBarSize->GetNumber();
         }
      }
   
      if (parm1 >= kGOptTraceAChannel) {
         parm1 += fCurTrace;
      }
      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionRange                                                       //
//                                                                      //
// Graphics option: Range                                               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionRange::TLGOptionRange (const TGWindow* p, Int_t id, 
                     OptionRange_t* optvals, const PlotMap& ptypes)
   : TLGOptions (p, kGOptRangeName, id, optvals), fPlotList (&ptypes)
   {
      //OptionRange_t* vals = (OptionRange_t*) fOptionValues;
   
      // Layout hints
      fL1 = new TGLayoutHints (kLHintsLeft | kLHintsTop, 2, 2, 2, 2);
      fL2 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 2, 2, 2, 2);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 3, 2, 2);
      fL4 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 0, 0, 1, 1);
      fL5 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 2, 2, 6, 2);
      fL6 = new TGLayoutHints (kLHintsExpandX | kLHintsExpandY, -2, -2, 4, -2);
      fL7 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 4);
      fL8 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 2, 0, 2, 0);
      fL9 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 0, 0, 1, -4);
   
      // Loop over axis
      for (int i = 1; i >= 0; i--) {
         fGroup[i] = new TGGroupFrame (this, 
                              (i == 0 ? "X axis" : "Y axis"));
         AddFrame (fGroup[i], fL7);
         // Scale group
         fF2[i] = new TGHorizontalFrame (fGroup[i], 100, 100);
         fGroup[i]->AddFrame (fF2[i], fL4);
         fScaleLabel[i] = new TGLabel (fF2[i], "Scale:   ");
         fF2[i]->AddFrame (fScaleLabel[i], fL3);
         fScaleLinear[i] = new TGRadioButton (fF2[i], "linear", 
                              kGOptRangeXLinear + 10 * i);
         fScaleLinear[i]->Associate (this);
         fF2[i]->AddFrame (fScaleLinear[i], fL3);
         fScaleLog[i] = new TGRadioButton (fF2[i], "log", 
                              kGOptRangeXLog + 10 * i);
         fScaleLog[i]->Associate (this);
         fF2[i]->AddFrame (fScaleLog[i], fL3);
      
         // Range group
         fF3[i] = new TGHorizontalFrame (fGroup[i], 100, 100);
         fGroup[i]->AddFrame (fF3[i], fL4);
         fRangeLabel[i] = new TGLabel (fF3[i], "Range:  ");
         fF3[i]->AddFrame (fRangeLabel[i], fL3);
         fRangeAutomatic[i] = new TGRadioButton (fF3[i], "automatic", 
                              kGOptRangeXAutomatic + 10 * i);
         fRangeAutomatic[i]->Associate (this);
         fF3[i]->AddFrame (fRangeAutomatic[i], fL3);
         fRangeManual[i] = new TGRadioButton (fF3[i], "manual", 
                              kGOptRangeXManual + 10 * i);
         fRangeManual[i]->Associate (this);
         fF3[i]->AddFrame (fRangeManual[i], fL3);
      
         // Range from/to group
         fF4[i] = new TGHorizontalFrame (fGroup[i], 100, 100);
         fGroup[i]->AddFrame (fF4[i], fL9);
         fRangeFromLabel[i] = new TGLabel (fF4[i], "From");
         fF4[i]->AddFrame (fRangeFromLabel[i], fL3);
         fRangeFrom[i] = new TLGNumericControlBox (fF4[i], 0, 12,
                              kGOptRangeXFrom + 10 * i);
         fRangeFrom[i]->Associate (this);
         fF4[i]->AddFrame (fRangeFrom[i], fL3);
         fRangeToLabel[i] = new TGLabel (fF4[i], " To");
         fF4[i]->AddFrame (fRangeToLabel[i], fL3);
         fRangeTo[i] = new TLGNumericControlBox (fF4[i], 0, 12,
                              kGOptRangeXTo + 10 * i);
         fRangeTo[i]->Associate (this);
         fF4[i]->AddFrame (fRangeTo[i], fL3);
      }
      // bin group
      fGB = new TGGroupFrame (this, "Bin");
      AddFrame (fGB, fL7);
      fFB = new TGHorizontalFrame (fGB, 100, 100);
      fGB->AddFrame (fFB, fL9);
      fBin = new TLGNumericControlBox (fFB, 1, 8,
                           kGOptRangeBin, kNESInteger, kNEAPositive);
      fBin->Associate (this);
      fFB->AddFrame (fBin, fL3);
      fBinLabel = new TGLabel (fFB, "   ");
      fFB->AddFrame (fBinLabel, fL3);
      fBinLogSpacing = new TGCheckButton (fFB, "Log spacing", 
                           kGOptRangeBinLogSpacing);
      fBinLogSpacing->Associate (this);
      fFB->AddFrame (fBinLogSpacing, fL3);
      // fill in options
      UpdateOptions();
   }

//______________________________________________________________________________
   TLGOptionRange::~TLGOptionRange ()
   {
      for (int i = 0; i < 2; i++) {
         delete fScaleLabel[i];
         delete fScaleLinear[i];
         delete fScaleLog[i];
         delete fF2[i];
         delete fRangeLabel[i];
         delete fRangeAutomatic[i];
         delete fRangeManual[i];
         delete fRangeFromLabel[i];
         delete fRangeFrom[i];
         delete fRangeToLabel[i];
         delete fRangeTo[i];
         delete fF3[i];
         delete fF4[i];
         delete fGroup[i];
      }
      delete fBinLogSpacing;
      delete fBinLabel;
      delete fBin;
      delete fFB;
      delete fGB;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
      delete fL7;
      delete fL8;
      delete fL9;
   }

//______________________________________________________________________________
   // UpdateOptions() puts the values in fOptionValues into the fields of the
   // range tab panel.
   void TLGOptionRange::UpdateOptions ()
   {
      //cerr << "TLGOptionRange::UpdateOptions() " << endl ;
      OptionRange_t* vals = (OptionRange_t*) fOptionValues;
      if (vals != 0) {
         for (int i = 0; i < 2; i++) {
            if (vals->fAxisScale[i] == kAxisScaleLinear) {
               fScaleLinear[i]->SetState (kButtonDown);
               fScaleLog[i]->SetState (kButtonUp);
            }
            else {
               fScaleLog[i]->SetState (kButtonDown);
               fScaleLinear[i]->SetState (kButtonUp);
            }
            if (vals->fRange[i] == kRangeAutomatic) {
               fRangeAutomatic[i]->SetState (kButtonDown);
               fRangeManual[i]->SetState (kButtonUp);
            }
            else {
               fRangeManual[i]->SetState (kButtonDown);
               fRangeAutomatic[i]->SetState (kButtonUp);
            }
            // check from < to
            if (vals->fRangeFrom[i] > vals->fRangeTo[i]) {
               Double_t temp = vals->fRangeFrom[i];
               vals->fRangeFrom[i] = vals->fRangeTo[i];
               vals->fRangeTo[i] = temp;
            }
            fRangeFrom[i]->SetNumber (vals->fRangeFrom[i]);
            fRangeTo[i]->SetNumber (vals->fRangeTo[i]);
         }
         fBin->SetIntNumber (vals->fBin);
         if (vals->fBinLogSpacing) {
            fBinLogSpacing->SetState (kButtonDown);
         }
         else {
            fBinLogSpacing->SetState (kButtonUp);
         }
      }
      //cerr << "TLGOptionRange::UpdateOptions() end " << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionRange::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionRange_t* vals = (OptionRange_t*) fOptionValues;
   
      switch (GET_MSG (msg)) {
         // command messages
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  // checkbox
                  case kCM_CHECKBUTTON:
                     {
                        // log spacing
                        if (parm1 == kGOptRangeBinLogSpacing) {
                           vals->fBinLogSpacing = 
                              fBinLogSpacing->GetState() == kButtonDown;
                        }
                        break;
                     }
                  // radio buttons
                  case kCM_RADIOBUTTON:
                     {
                        int i = (parm1 - kGOptRangeX) / 10;
                        if ((i < 0) || (i >= 2)) { 
                           break;
                        }
                        int id = (parm1 - kGOptRangeX) % 10 + kGOptRangeX;
                        if (id == kGOptRangeXLinear) {
                           if (fScaleLog[i]->GetState() == kButtonUp) {
                              return kTRUE;
                           }
                           fScaleLog[i]->SetState (kButtonUp);
                           vals->fAxisScale[i] = kAxisScaleLinear;
                           if (i == 0) {
                              vals->fBinLogSpacing = kFALSE;
                              fBinLogSpacing->SetState (kButtonUp);
                           }
                        }
                        else if (id == kGOptRangeXLog) {
                           if (fScaleLinear[i]->GetState() == kButtonUp) {
                              return kTRUE;
                           }
                           fScaleLinear[i]->SetState (kButtonUp);
                           vals->fAxisScale[i] = kAxisScaleLog;
                           if (i == 0) {
                              vals->fBinLogSpacing = kTRUE;
                              fBinLogSpacing->SetState (kButtonDown);
                           }
                        }
                        else if (id == kGOptRangeXAutomatic) {
                           if (fRangeManual[i]->GetState() == kButtonUp) {
                              return kTRUE;
                           }
                           fRangeManual[i]->SetState (kButtonUp);
                           vals->fRange[i] = kRangeAutomatic;
                        }
                        else if (id == kGOptRangeXManual) {
                           if (fRangeAutomatic[i]->GetState() == kButtonUp) {
                              return kTRUE;
                           }
                           fRangeAutomatic[i]->SetState (kButtonUp);
                           vals->fRange[i] = kRangeManual;
                        }
                        break;
                     }
               }
               break;
            }
         // textentry
         case kC_TEXTENTRY:
            {
               switch (GET_SUBMSG (msg)) {
                  case kTE_TEXTUPDATED:
                     {
                        int i = (parm1 - kGOptRangeX) / 10;
                        if ((i < 0) || (i >= 2)) { 
                           break;
                        }
                        int id = (parm1 - kGOptRangeX) % 10 + kGOptRangeX;
                        if (id == kGOptRangeXFrom) {
                           fRangeManual[i]->SetState (kButtonDown);
                           fRangeAutomatic[i]->SetState (kButtonUp);
                           vals->fRange[i] = kRangeManual;
                           vals->fRangeFrom[i] = fRangeFrom[i]->GetNumber();
                           // double r = fRangeFrom[i]->GetNumber ();
                           // if (r < vals->fRangeTo[i]) {
                              // vals->fRangeFrom[i] = r;
                           // }
                           // else {
                              // fRangeFrom[i]->SetNumber (vals->fRangeFrom[i]);
                           // }
                        }
                        else if (id == kGOptRangeXTo) {
                           fRangeManual[i]->SetState (kButtonDown);
                           fRangeAutomatic[i]->SetState (kButtonUp);
                           vals->fRange[i] = kRangeManual;
                           vals->fRangeTo[i] = fRangeTo[i]->GetNumber();
                           // double r = fRangeTo[i]->GetNumber ();
                           // if (r > vals->fRangeFrom[i]) {
                              // vals->fRangeTo[i] = r;
                           // }
                           // else {
                              // fRangeTo[i]->SetNumber (vals->fRangeTo[i]);
                           // }
                        }
                        else if (parm1 == kGOptRangeBin) {
                           vals->fBin = fBin->GetIntNumber();
                        }
                        break;
                     }
               }
               break;
            }
      }
      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionUnits                                                       //
//                                                                      //
// Graphics option: Units                                               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionUnits::TLGOptionUnits (const TGWindow* p, Int_t id, 
                     OptionUnits_t* optvals, TLGOptionTab* tab,
                     TList* xunits, TList* yunits)
   : TLGOptions (p, kGOptUnitsName, id, optvals), fTab (tab),
   fXUnitList (xunits), fYUnitList (yunits)
   {
      // Layout hints
      fL1 = new TGLayoutHints (kLHintsLeft | kLHintsTop, 2, 2, 2, 2);
      fL2 = new TGLayoutHints (kLHintsRight | kLHintsTop, 2, 12, 2, 2);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 3, 2, 2);
      fL4 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 0, 0, 2, 2);
      fL5 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 2, 0);
      fL6 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 2, -4);
      fL7 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 4);
      fL8 = new TGLayoutHints (kLHintsExpandX | kLHintsExpandY | kLHintsTop, 
                           0, 0, 5, 4);
      fL9 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 2, -8);
      // Group frames
      fG2 = new TGGroupFrame (this, "Units");
      AddFrame (fG2, fL7);
      fG1 = new TGGroupFrame (this, "Display");
      AddFrame (fG1, fL7);
      fG3 = new TGGroupFrame (this, "Scaling");
      AddFrame (fG3, fL7);
      fG4 = new TGHorizontalFrame (this, 100, 100);
      AddFrame (fG4, fL8);
      // Display group
      //    x
      fF1 = new TGHorizontalFrame (fG1, 100, 100);
      fG1->AddFrame (fF1, fL5);
      fXValuesLabel = new TGLabel (fF1, "X: ");
      fF1->AddFrame (fXValuesLabel, fL3);
      fXValues = new TGComboBox (fF1, kGOptUnitXValues);
      fXValues->SetHeight (22);
      fXValues->Associate (this);
      fXValues->AddEntry ("Standard", (Int_t) kUnitNormal);
      // fXValues->AddEntry ("Date/time format", (Int_t) kUnitTime);
      // fXValues->AddEntry ("Angular frequency", (Int_t) kUnitAngularF);
      fF1->AddFrame (fXValues, fL4);
      //    y
      fF2 = new TGHorizontalFrame (fG1, 100, 100);
      fG1->AddFrame (fF2, fL9);
      fYValuesLabel = new TGLabel (fF2, "Y: ");
      fF2->AddFrame (fYValuesLabel, fL3);
      fYValues = new TGComboBox (fF2, kGOptUnitYValues);
      fYValues->SetHeight (22);
      fYValues->Associate (this);
      fYValues->AddEntry ("Magnitude", (Int_t) kUnitMagnitude);
      fYValues->AddEntry ("dB Magnitude", (Int_t) kUnitdBMagnitude);
      fYValues->AddEntry ("Real part", (Int_t) kUnitReal);
      fYValues->AddEntry ("Imaginary part", (Int_t) kUnitImaginary);
      fYValues->AddEntry ("Real/imaginary part (even/odd traces)", 
                         (Int_t) kUnitRealImaginary);
      fYValues->AddEntry ("Phase (degree)", (Int_t) kUnitPhaseDeg);
      fYValues->AddEntry ("Phase (rad)", (Int_t) kUnitPhaseRad);
      fYValues->AddEntry ("Continuous phase (degree)", 
                         (Int_t) kUnitPhaseDegCont);
      fYValues->AddEntry ("Continuous phase (rad)", 
                         (Int_t) kUnitPhaseRadCont);
      fF2->AddFrame (fYValues, fL4);
   
      // Units group
      //    X
      fF3 = new TGHorizontalFrame (fG2, 100, 100);
      fG2->AddFrame (fF3, fL6);
      fXUnitLabel = new TGLabel (fF3, "X: ");
      fF3->AddFrame (fXUnitLabel, fL3);
      fXMag = new TGComboBox (fF3, kGOptUnitXMag);
      fXMag->Resize (50, 22);
      fXMag->Associate (this);
      fXMag->AddEntry ("-", 0);
      fXMag->AddEntry ("m", -3);
      fXMag->AddEntry ("k", 3);
      fXMag->AddEntry ("u", -6);
      fXMag->AddEntry ("M", 6);
      fXMag->AddEntry ("n", -9);
      fXMag->AddEntry ("G", 9);
      fXMag->AddEntry ("p", -12);
      fXMag->AddEntry ("T", 12);
      fXMag->AddEntry ("fm", -15);
      fXMag->AddEntry ("P", 15);
      fF3->AddFrame (fXMag, fL3);
      fXUnit = new TGComboBox (fF3, kGOptUnitXUnits);
      fXUnit->Resize (160, 22);
      fXUnit->Associate (this);
      fF3->AddFrame (fXUnit, fL3);
      //    Y
      fF4 = new TGHorizontalFrame (fG2, 100, 100);
      fG2->AddFrame (fF4, fL9);
      fYUnitLabel = new TGLabel (fF4, "Y: ");
      fF4->AddFrame (fYUnitLabel, fL3);
      fYMag = new TGComboBox (fF4, kGOptUnitYMag);
      fYMag->Resize (50, 22);
      fYMag->Associate (this);
      fYMag->AddEntry ("-", 0);
      fYMag->AddEntry ("m", -3);
      fYMag->AddEntry ("k", 3);
      fYMag->AddEntry ("u", -6);
      fYMag->AddEntry ("M", 6);
      fYMag->AddEntry ("n", -9);
      fYMag->AddEntry ("G", 9);
      fYMag->AddEntry ("p", -12);
      fYMag->AddEntry ("T", 12);
      fYMag->AddEntry ("fm", -15);
      fYMag->AddEntry ("P", 15);
      fF4->AddFrame (fYMag, fL3);
      fYUnit = new TGComboBox (fF4, kGOptUnitYUnits);
      fYUnit->Resize (160, 22);
      fYUnit->Associate (this);
      fF4->AddFrame (fYUnit, fL3);
   
      // Scaling group
      //    X
      fF5 = new TGHorizontalFrame (fG3, 100, 100);
      fG3->AddFrame (fF5, fL5);
      fXSlopeLabel = new TGLabel (fF5, "X Slope: ");
      fF5->AddFrame (fXSlopeLabel, fL3);
      fXSlope = new TLGNumericControlBox (fF5, 0, 8, kGOptUnitXSlope);
      fXSlope->Associate (this);
      fF5->AddFrame (fXSlope, fL3);
      fXOffsetLabel = new TGLabel (fF5, "  Offset: ");
      fF5->AddFrame (fXOffsetLabel, fL3);
      fXOffset = new TLGNumericControlBox (fF5, 0, 8, kGOptUnitXOffset);
      fXOffset->Associate (this);
      fF5->AddFrame (fXOffset, fL3);
      //   Y
      fF6 = new TGHorizontalFrame (fG3, 100, 100);
      fG3->AddFrame (fF6, fL9);
      fYSlopeLabel = new TGLabel (fF6, "Y Slope: ");
      fF6->AddFrame (fYSlopeLabel, fL3);
      fYSlope = new TLGNumericControlBox (fF6, 0, 8, kGOptUnitYSlope);
      fYSlope->Associate (this);
      fF6->AddFrame (fYSlope, fL3);
      fYOffsetLabel = new TGLabel (fF6, "  Offset: ");
      fF6->AddFrame (fYOffsetLabel, fL3);
      fYOffset = new TLGNumericControlBox (fF6, 0, 8, kGOptUnitYOffset);
      fYOffset->Associate (this);
      fF6->AddFrame (fYOffset, fL3);
   
      // Calibration button
      fCalibration = new TGTextButton (fG4, "   Calibration...   ", 
                           kGOptUnitCalibration);
      fCalibration->Associate (this);
      fG4->AddFrame (fCalibration, fL2);
   
      UpdateOptions ();
   }

//______________________________________________________________________________
   TLGOptionUnits::~TLGOptionUnits ()
   {
      delete fXValues;
      delete fXValuesLabel;
      delete fYValues;
      delete fYValuesLabel;
      delete fXUnitLabel;
      delete fYUnitLabel;
      delete fXUnit;
      delete fYUnit;
      delete fXMag;
      delete fYMag;
      delete fXSlopeLabel;
      delete fYSlopeLabel;
      delete fXOffsetLabel;
      delete fYOffsetLabel;
      delete fXSlope;
      delete fYSlope;
      delete fXOffset;
      delete fYOffset;
      delete fCalibration;
      delete fF1;
      delete fF2;
      delete fF3;
      delete fF4;
      delete fF5;
      delete fF6;
      delete fG1;
      delete fG2;
      delete fG3;
      delete fG4;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
      delete fL7;
      delete fL8;
      delete fL9;
   }

//______________________________________________________________________________
   void TLGOptionUnits::UpdateOptions ()
   {
      static const char* const units[2][6] = 
         {{"None", "Default", "s", "Hz", "rad/s", 0}, 
         {"None", "Default", 0}};
   
      //cerr << "TLGOptionUnits::UpdateOptions() " << endl ;
      OptionUnits_t* vals = (OptionUnits_t*) fOptionValues;
   
      if (vals != 0) {
         // Converison
         fXValues->Select (vals->fXValues);
         fYValues->Select (vals->fYValues);
         // Units
         for (int i = 0 ; i < 2; i++) {
            TGComboBox* unitbox = (i == 0) ? fXUnit : fYUnit;
            TList* unitlist = (i == 0) ? fXUnitList : fYUnitList;
            TString& unitsel = (i == 0) ? vals->fXUnit : vals->fYUnit;
         
            Int_t sel = -1;
            unitbox->RemoveEntries (0, 10000);
            if (!unitlist) {
               const char* const* p = units[i];
               Int_t i = 0;
               while (*p != 0) {
                  if (strcmp (*p, unitsel) == 0) {
                     sel = i;
                  }
                  unitbox->AddEntry (*p, i++);
                  p++;
               }
               if (sel == -1) {
                  unitsel = units[i][0];
                  sel = 0;
               }
            }
            else {
               // Add none and default first
               Int_t i = 0;
               sel = 0;
               unitbox->AddEntry ("none", i++);
               if (unitlist->FindObject ("default") != 0) {
                  sel = 1;
                  unitbox->AddEntry ("default", i++);
               }
               // add rest from list
               TIter next (unitlist);
               while (TObject* obj = next()) {
                  if ((strcmp ("none", obj->GetName()) == 0) || 
                     (strcmp ("default", obj->GetName()) == 0)) {
                     continue;
                  }
                  if (strcmp (obj->GetName(), unitsel) == 0) {
                     sel = i;
                  }
                  unitbox->AddEntry (obj->GetName(), i++);
               }
               // make sure selection is valid
               if (sel == 0) {
                  unitsel = "none";
               }
               else if ((sel == 1) && 
                       (unitlist->FindObject ("default") != 0)){
                  unitsel = "default";
               }
            }
            // select unit
            if (sel >= 0) unitbox->Select (sel);
         }
         // Magnitude
         fXMag->Select (vals->fXMag);
         fYMag->Select (vals->fYMag);
         // Slope and offset
         fXSlope->SetNumber (vals->fXSlope);
         fXOffset->SetNumber (vals->fXOffset);
         fYSlope->SetNumber (vals->fYSlope);
         fYOffset->SetNumber (vals->fYOffset);
      }
      //cerr << "TLGOptionUnits::UpdateOptions() end " << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionUnits::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionUnits_t* vals = (OptionUnits_t*) fOptionValues;
   
      // command message: buttons
      if ((GET_MSG (msg) == kC_COMMAND) && 
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         if (parm1 == kGOptUnitCalibration) {
            fTab->CalibrationDialog ();
         }
         return kTRUE;
      }
      // command message: combobox
      else if ((GET_MSG (msg) == kC_COMMAND) && 
              (GET_SUBMSG (msg) == kCM_COMBOBOX)) {
         // X conversion
         if (parm1 == kGOptUnitXValues) {
            if (vals->fXValues == (EXRangeType) parm2) {
               return kTRUE;
            }
            vals->fXValues = (EXRangeType) parm2; 
            fTab->UpdateAxis (0);
         }
         // Y convserion
         else if (parm1 == kGOptUnitYValues) {
            if (vals->fYValues == (EYRangeType) parm2) {
               return kTRUE;
            }
            vals->fYValues = (EYRangeType) parm2;
            fTab->UpdateAxis (1);
         }
         // X units
         else if (parm1 == kGOptUnitXUnits) {
            TGTextLBEntry* entry = (TGTextLBEntry*)
               fXUnit->GetSelectedEntry();
            if ((entry == 0) || 
               (strcmp (vals->fXUnit, entry->GetText()->
                       GetString()) == 0)) {
               return kTRUE;
            }
            vals->fXUnit = entry->GetText()->GetString();
         }
         // Y units
         else if (parm1 == kGOptUnitYUnits) {
            TGTextLBEntry* entry = (TGTextLBEntry*)
               fYUnit->GetSelectedEntry();
            if (!entry || 
               (strcmp (vals->fYUnit, entry->GetText()->
                       GetString()) == 0)) {
               return kTRUE;
            }
            vals->fYUnit = entry->GetText()->GetString();
         }
         // X magnitude
         else if (parm1 == kGOptUnitXMag) {
            if (vals->fXMag == parm2) {
               return kTRUE;
            }
            vals->fXMag = parm2; 
         }
         // Y magnitude
         else if (parm1 == kGOptUnitYMag) {
            if (vals->fYMag == parm2) {
               return kTRUE;
            }
            vals->fYMag = parm2; 
         }
      }
      
      // textentry
      else if ((GET_MSG (msg) == kC_TEXTENTRY) && 
              (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) {
         if (parm1 == kGOptUnitXSlope) {
            vals->fXSlope = fXSlope->GetNumber ();
         }
         else if (parm1 == kGOptUnitXOffset) {
            vals->fXOffset = fXOffset->GetNumber ();
         }
         else if (parm1 == kGOptUnitYSlope) {
            vals->fYSlope = fYSlope->GetNumber ();
         }
         else if (parm1 == kGOptUnitYOffset) {
            vals->fYOffset = fYOffset->GetNumber ();
         }
      }
   
      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionCursor                                                      //
//                                                                      //
// Graphics option: Cursor control                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionCursor::TLGOptionCursor (const TGWindow* p, Int_t id, 
                     OptionCursor_t* optvals, const PlotMap& ptypes)
   : TLGOptions (p, kGOptCursorName, id, optvals), fPlotList (&ptypes)
   {
      OptionCursor_t* vals = (OptionCursor_t*) fOptionValues;
      fCurTrace = 0;
      vals->fTrace = fCurTrace;
      fCurStat = 0;
   
      // Layout hints
      fL1 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 4);
      fL2 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 0);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 5, 2, 2);
      fL4 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 0, 0, 2, 2);
      fL5 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, -8);
      fL6 = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 0, 5, 2, 2);
      fL7 = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 4);
      fL8 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, -2, -2, 4, 2);
      fL9 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 5, 0, 0);
      fL10 = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 4, -4);
   
      // Trace tab
      fTraceLabel = new TGLabel (this, "Trace:");
      AddFrame (fTraceLabel, fL10);
      fTraces = new TGTabTraces (this);
      fTraces->Associate (this);
      AddFrame (fTraces, fL8);
      for (int i = 0; i < kMaxTraces; i++) {
         char label[4];
         sprintf (label, "%i", i);
         fTraces->AddTab (label);
      }
   
      // group frames
      fF0 = new TGHorizontalFrame (this, 100, 100);
      AddFrame (fF0, fL1);
      fF1 = new TGGroupFrame (fF0, "Active");
      fF0->AddFrame (fF1, fL7);
      fF2 = new TGGroupFrame (fF0, "Style");
      fF0->AddFrame (fF2, fL7);
      fF5 = new TGGroupFrame (fF0, "Type");
      fF0->AddFrame (fF5, fL1);
      fF3 = new TGGroupFrame (this, "Values");
      AddFrame (fF3, fL1);
      fF4 = new TGGroupFrame (this, "Statistics");
      AddFrame (fF4, fL1);
   
      // active 
      for (int i = 0; i < 2; i++) {
         fActive[i] = new TGCheckButton (fF1, (i == 0) ? "1 " : "2 ", 
                              kGOptCursorActive + i);
         fActive[i]->Associate (this);
         fF1->AddFrame (fActive[i], (i == 0) ? fL2 : fL5);
      }
   
      // style
      fF6[0] = new TGHorizontalFrame (fF2, 100, 100);
      fF2->AddFrame (fF6[0], fL5);
      fF6[1] = new TGVerticalFrame (fF6[0], 100, 100);
      fF6[0]->AddFrame (fF6[1], fL2);
      fF6[2] = new TGVerticalFrame (fF6[0], 100, 100);
      fF6[0]->AddFrame (fF6[2], fL2);
      for (int i = 0; i < 4; i++) {
         const char* text[4] = 
            {"None ", "Cross ", "Vert.", "Horiz."};
         fStyle[i] = new TGRadioButton (fF6[i/2+1], text[i], 
                              kGOptCursorStyle + i);
         fStyle[i]->Associate (this);
         fF6[i/2+1]->AddFrame (fStyle[i], (i % 2 == 0) ? fL2 : fL2);
      }
      // type
      for (int i = 0; i < 2; i++) {
         fType[i] = new TGRadioButton (fF5, (i == 0) ? 
                              "Abs." : "Delta", kGOptCursorType + i);
         fType[i]->Associate (this);
         fF5->AddFrame (fType[i], (i == 0) ? fL2 : fL5);
      }
      // values
      fF7 = new TGHorizontalFrame (fF3, 100, 100);
      fF3->AddFrame (fF7, fL2);
      fXLabel[0] = new TGLabel (fF7, "X1:");
      fF7->AddFrame (fXLabel[0], fL3);
      fX[0] = new TLGNumericControlBox (fF7, 0, 12, kGOptCursorX, kNESReal);
      fX[0]->Associate (this);
      fF7->AddFrame (fX[0], fL3);
      fYLabel[0] = new TGLabel (fF7, "  Y1:");
      fF7->AddFrame (fYLabel[0], fL3);
      fY[0] = new TLGNumericControlBox (fF7, 0, 12, kGOptCursorY, kNESReal);
      fY[0]->Associate (this);
      fF7->AddFrame (fY[0], fL4);
      fF8 = new TGHorizontalFrame (fF3, 100, 100);
      fF3->AddFrame (fF8, fL5);
      fXLabel[1] = new TGLabel (fF8, "X2:");
      fF8->AddFrame (fXLabel[1], fL3);
      fX[1] = new TLGNumericControlBox (fF8, 0, 12, kGOptCursorX+1, kNESReal);
      fX[1]->Associate (this);
      fF8->AddFrame (fX[1], fL3);
      fYLabel[1] = new TGLabel (fF8, "  Y2:");
      fF8->AddFrame (fYLabel[1], fL3);
      fY[1] = new TLGNumericControlBox (fF8, 0, 12, kGOptCursorY+1, kNESReal);
      fY[1]->Associate (this);
      fF8->AddFrame (fY[1], fL4);
      // statistics
      fF9 = new TGHorizontalFrame (fF4, 100, 100);
      fF4->AddFrame (fF9, fL5);
      fStatistics = new TGComboBox (fF9, kGOptCursorStatistics);
      fStatistics->SetHeight (22);
      fStatistics->SetWidth (100);
      fStatistics->Associate (this);
      fStatistics->AddEntry ("X/Y diff", 0);
      fStatistics->AddEntry ("n/rms", 1);
      fStatistics->AddEntry ("Mean/sigma", 2);
      fStatistics->AddEntry ("Center/Width", 6);
      fStatistics->AddEntry ("Peak X/Y", 3);
      fStatistics->AddEntry ("Sum/sqr.sum", 4);
      fStatistics->AddEntry ("Area/rms area", 5);
      fStatistics->Select (0);
      fF9->AddFrame (fStatistics, fL3);
      for (int i = 0; i < 2; i++) {
         fVal[i] = new TLGNumericEntry (fF9, 0.0, kGOptCursorVal+i, kNESReal);
         fVal[i]->Associate (this);
         fF9->AddFrame (fVal[i], fL4);
      }
   
      UpdateOptions();
   }

//______________________________________________________________________________
   TLGOptionCursor::~TLGOptionCursor ()
   {
      delete fTraceLabel;
      delete fTraces;
      delete fActive[0];
      delete fActive[1];
      delete fType[0];
      delete fType[1];
      delete fStyle[0];
      delete fStyle[1];
      delete fStyle[2];
      delete fStyle[3];
      delete fXLabel[0];
      delete fXLabel[1];
      delete fYLabel[0];
      delete fYLabel[1];
      delete fX[0];
      delete fX[1];
      delete fY[0];
      delete fY[1];
      delete fStatistics;
      delete fVal[0];
      delete fVal[1];
      delete fF1;
      delete fF2;
      delete fF3;
      delete fF4;
      delete fF5;
      delete fF6[0];
      delete fF6[1];
      delete fF6[2];
      delete fF7;
      delete fF8;
      delete fF9;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
      delete fL7;
      delete fL8;
      delete fL9;
      delete fL10;
   }

//______________________________________________________________________________
   void TLGOptionCursor::UpdateOptions ()
   {
      OptionCursor_t* vals = (OptionCursor_t*) fOptionValues;
   
      //cerr << "TLGOptionCursor::UpdateOptions() " << endl ;
      // active
      for (int i = 0; i < 2; i ++) {
         fActive[i]->SetState (vals->fActive[i] ? kButtonDown : kButtonUp);
         if (vals->fActive[i]) {
            fX[i]->SetNumber (vals->fX[i]);
         }
      }
      // style
      for (int i = 0; i < 4; i ++) {
         fStyle[i]->SetState (i == vals->fStyle ? kButtonDown : kButtonUp);
      }
      // type
      for (int i = 0; i < 2; i ++) {
         fType[i]->SetState (i == (int)vals->fType ? kButtonDown : kButtonUp);
      }
      // values
      if (vals->fStyle != kCursorHorizontal) {
         // vertical cursors
         for (int i = 0; i < 2; i++) {
            if (vals->fActive[i]) {
               fX[i]->SetState (kTRUE);
               if (vals->fX[i] != fX[i]->GetNumber()) {
                  fX[i]->SetNumber (vals->fX[i]);
               }
               if (vals->fValid[fCurTrace]) {
                  fY[i]->SetNumber (vals->fY[fCurTrace][i]);
               }
               else {
                  fY[i]->SetText ("");
               }
               if ((vals->fTrace >= 0) && (vals->fTrace < kMaxTraces) &&
                  vals->fValid[vals->fTrace]) {
                  fX[i]->SetButtonToNum (kFALSE);
               }
               else {
                  fX[i]->SetButtonToNum (kTRUE);
               }
            }
            else {
               fX[i]->SetState (kFALSE);
               fX[i]->SetText ("");
               fY[i]->SetText ("");
            }
            fY[i]->SetState (kFALSE);
         }
      }
      else {
         // horizontal cursors
         for (int i = 0; i < 2; i++) {
            if (vals->fActive[i]) {
               fY[i]->SetState (kTRUE);
               if (vals->fH[i] != fY[i]->GetNumber()) {
                  fY[i]->SetNumber (vals->fH[i]);
               }
            }
            else {
               fY[i]->SetState (kFALSE);
               fY[i]->SetText ("");
            }
            fX[i]->SetText ("");
            fX[i]->SetState (kFALSE);
         }
      }
      // statistics
      fVal[0]->SetState (kFALSE);
      fVal[1]->SetState (kFALSE);
      if (vals->fValid[fCurTrace] && (vals->fStyle != kCursorHorizontal)) {
         //fStatistics->SetState (kTRUE);
         switch (fCurStat) {
            // diff
            case 0:
            default:
               {
                  fVal[0]->SetNumber (vals->fXDiff[fCurTrace]);
                  fVal[1]->SetNumber (vals->fYDiff[fCurTrace]);
                  break;
               }
            // N & rms
            case 1:
               {
                  fVal[0]->SetNumber (vals->fN[fCurTrace]);
                  fVal[1]->SetNumber (vals->fRMS[fCurTrace]);
                  break;
               }
            // Mean & Sigma
            case 2: 
               {
                  fVal[0]->SetNumber (vals->fMean[fCurTrace]);
                  fVal[1]->SetNumber (vals->fStdDev[fCurTrace]);
                  break;
               }
            // Peak X/Y
            case 3: 
               {
                  fVal[0]->SetNumber (vals->fPeakX[fCurTrace]);
                  fVal[1]->SetNumber (vals->fPeakY[fCurTrace]);
                  break;
               }
            // Sum & sqr sum
            case 4: 
               {
                  fVal[0]->SetNumber (vals->fSum[fCurTrace]);
                  fVal[1]->SetNumber (vals->fSqrSum[fCurTrace]);
                  break;
               }
            // Area & RMS area
            case 5: 
               {
                  fVal[0]->SetNumber (vals->fArea[fCurTrace]);
                  fVal[1]->SetNumber (vals->fRMSArea[fCurTrace]);
                  break;
               }
            // Center & Width area
            case 6: 
               {
                  fVal[0]->SetNumber (vals->fCenter[fCurTrace]);
                  fVal[1]->SetNumber (vals->fWidth[fCurTrace]);
                  break;
               }
         }
      }
      else {
         //fStatistics->SetState (kFALSE);
         fVal[0]->SetText ("");
         fVal[1]->SetText ("");
      }
      //cerr << "TLGOptionCursor::UpdateOptions() end " << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionCursor::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionCursor_t* vals = (OptionCursor_t*) fOptionValues;
   
      switch (GET_MSG (msg)) {
         // command messages
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  // tab 
                  case kCM_TAB:
                     {
                        if ((parm1 >= 0) && (parm1 < kMaxTraces)) {
                           fCurTrace = parm1;
                           vals->fTrace = fCurTrace;
                           UpdateOptions();
                           SendCursorMessage (0);
                        }
                        return kTRUE;
                     }
                  // checkbox
                  case kCM_CHECKBUTTON:
                     {
                       // Active
                        if (parm1 == kGOptCursorActive) {
                           vals->fActive[0] = 
                              (fActive[0]->GetState() == kButtonDown);
                           SendCursorMessage (1);
                        }
                        else if (parm1 == kGOptCursorActive + 1) {
                           vals->fActive[1] = 
                              (fActive[1]->GetState() == kButtonDown);
                           SendCursorMessage (1);
                        }
                        return kTRUE;
                     }
                  // radio buttons
                  case kCM_RADIOBUTTON:
                     {
                        // Style
                        int id = parm1 - kGOptCursorStyle;
                        if ((id >= 0) && (id < 4)) {
                           Bool_t changed = kFALSE;
                           for (int i = 0; i < 4; i++) {
                              if ((i != id) && 
                                 (fStyle[i]->GetState() == kButtonDown)) {
                                 changed = kTRUE;
                                 break;
                              }
                           }
                           if (!changed) {
                              return kTRUE;
                           }
                           for (int i = 0; i < 4; i++) {
                              fStyle[i]->SetState (i == id ?
                                                  kButtonDown : kButtonUp);
                           }
                           ECursorStyle oldstyle = vals->fStyle;
                           vals->fStyle = (ECursorStyle) id;
                           if ((oldstyle == kCursorHorizontal) != 
                              (vals->fStyle == kCursorHorizontal)) {
                              UpdateOptions();
                           }
                           SendCursorMessage (0);
                        }
                        // Type
                        else if (parm1 == kGOptCursorType) {
                           if (fType[1]->GetState() == kButtonUp) {
                              return kTRUE;
                           }
                           fType[1]->SetState (kButtonUp);
                           vals->fType = kCursorAbsolute;
                           SendCursorMessage (1);
                        }
                        else if (parm1 == kGOptCursorType + 1) {
                           if (fType[0]->GetState() == kButtonUp) {
                              return kTRUE;
                           }
                           fType[0]->SetState (kButtonUp);
                           vals->fType = kCursorDifference;
                           SendCursorMessage (1);
                        }
                        return kTRUE;
                     }
                  // combo box
                  case kCM_COMBOBOX:
                     {
                        // Statitsics selection 
                        if (parm1 == kGOptCursorStatistics) {
                           if (fCurStat != parm2) {
                              fCurStat = parm2;
                              UpdateOptions();
                           }
                        }
                        return kTRUE;
                     }
                  case kCM_BUTTON:
                     {
                        // Numeric entry buttons 
                        if (parm1 == kGOptCursorX) {
                           SendCursorMessage (1, 1000000 + parm2);
                        }
                        else if (parm1 == kGOptCursorX + 1) {
                           SendCursorMessage (1, 2000000 + parm2);
                        }
                        return kTRUE;
                     }
               }
               break;
            }
         // textentry
         case kC_TEXTENTRY:
            {
               switch (GET_SUBMSG (msg)) {
                  case kTE_TEXTUPDATED:
                     {
                        if (parm1 == kGOptCursorX) {
                           vals->fX[0] = fX[0]->GetNumber();
                           SendCursorMessage (1);
                        }
                        else if (parm1 == kGOptCursorX + 1) {
                           vals->fX[1] = fX[1]->GetNumber();
                           SendCursorMessage (1);
                        }
                        else if (parm1 == kGOptCursorY) {
                           vals->fH[0] = fY[0]->GetNumber();
                           SendCursorMessage (0);
                        }
                        else if (parm1 == kGOptCursorY + 1) {
                           vals->fH[1] = fY[1]->GetNumber();
                           SendCursorMessage (0);
                        }
                        return kTRUE;
                     }
               }
               break;
            }
      }
   
      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionConfig                                                      //
//                                                                      //
// Graphics option: Config                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionConfig::TLGOptionConfig (const TGWindow* p, Int_t id, 
                     OptionConfig_t* optvals, TLGOptionTab* tab)
   : TLGOptions (p, kGOptConfigName, id, optvals), fTab (tab)
   {
      // Layout hints
      fL1 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 4);
      fL2 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 5, 5, 4, 4);
      fL4 = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 5, 5, 2, 2);
      fL5 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -4);
      fL6 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 20, 0, 4, 0);
   
      // group frames
      fG1 = new TGGroupFrame (this, "Auto configuration");
      AddFrame (fG1, fL1);
      fFB = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fFB, fL3);
   
      // Options group
      fAutoConf = new TGCheckButton (fG1, "Plot settings", 
                           kGOptConfigAutoConf);
      fAutoConf->Associate (this);
      fG1->AddFrame (fAutoConf, fL2);
      fRespectUser = new TGCheckButton (fG1, "Respect user selection", 
                           kGOptConfigRespectUser);
      fRespectUser->Associate (this);
      fG1->AddFrame (fRespectUser, fL6);
      fAutoAxes = new TGCheckButton (fG1, "Axes title", 
                           kGOptConfigAutoAxes);
      fAutoAxes->Associate (this);
      fG1->AddFrame (fAutoAxes, fL2);
      fAutoBin = new TGCheckButton (fG1, "Bin", 
                           kGOptConfigAutoBin);
      fAutoBin->Associate (this);
      fG1->AddFrame (fAutoBin, fL5);
      fAutoTimeAdjust = new TGCheckButton (fG1, "Time Adjust", 
                           kGOptConfigAutoTimeAdjust);
      fAutoTimeAdjust->Associate (this);
      fG1->AddFrame (fAutoTimeAdjust, fL5);
   
      // Save/restore buttons
      fRestore = new TGTextButton (fFB, "   Restore...   ", kGOptConfigRestore);
      fRestore->Associate (this);
      fFB->AddFrame (fRestore, fL4);
      fSave = new TGTextButton (fFB, "    Store...    ", kGOptConfigSave);
      fSave->Associate (this);
      fFB->AddFrame (fSave, fL4);
   
      UpdateOptions ();
   }

//______________________________________________________________________________
   TLGOptionConfig::~TLGOptionConfig ()
   {
      delete fAutoTimeAdjust;
      delete fAutoBin;
      delete fAutoAxes;
      delete fAutoConf;
      delete fRespectUser;
      delete fSave;
      delete fRestore;
      delete fG1;
      delete fFB;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
   }

//______________________________________________________________________________
   void TLGOptionConfig::UpdateOptions ()
   {
      //cerr << "TLGOptionConfig::UpdateOptions() " << endl ;
      OptionConfig_t* vals = (OptionConfig_t*) fOptionValues;
      if (vals != 0) {
         fAutoConf->SetState (vals->fAutoConf ? kButtonDown : kButtonUp);
         fRespectUser->SetState (vals->fRespectUser ? kButtonDown : kButtonUp);
         fAutoAxes->SetState (vals->fAutoAxes ? kButtonDown : kButtonUp);
         fAutoBin->SetState (vals->fAutoBin ? kButtonDown : kButtonUp);
         fAutoTimeAdjust->SetState (vals->fAutoTimeAdjust ? kButtonDown : kButtonUp);
      }
      //cerr << "TLGOptionConfig::UpdateOptions() end " << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionConfig::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionConfig_t* vals = (OptionConfig_t*) fOptionValues;
   
      // Check button
      if ((GET_MSG (msg) == kC_COMMAND) && 
         (GET_SUBMSG (msg) == kCM_CHECKBUTTON)) {
         // automatic plot configure
         if (parm1 == kGOptConfigAutoConf) {
            vals->fAutoConf = fAutoConf->GetState() == kButtonDown;
         }
         // automatic axes configure
         else if (parm1 == kGOptConfigRespectUser) {
            vals->fRespectUser = fRespectUser->GetState() == kButtonDown;
         }
         // automatic axes configure
         else if (parm1 == kGOptConfigAutoAxes) {
            vals->fAutoAxes = fAutoAxes->GetState() == kButtonDown;
         }
         // automatic binning
         else if (parm1 == kGOptConfigAutoBin) {
            vals->fAutoBin = fAutoBin->GetState() == kButtonDown;
         }
         // automatic time adjust
         else if (parm1 == kGOptConfigAutoTimeAdjust) {
            vals->fAutoTimeAdjust = fAutoTimeAdjust->GetState() == kButtonDown;
         }
      }
      
      // Save/restore button
      else if ((GET_MSG (msg) == kC_COMMAND) && 
              (GET_SUBMSG (msg) == kCM_BUTTON)) {
         if (parm1 == kGOptConfigSave) {
            ((TLGOptionTab*)fTab)->SaveRestoreDialog (kTRUE);
         }
         else if (parm1 == kGOptConfigRestore) {
            ((TLGOptionTab*)fTab)->SaveRestoreDialog (kFALSE);
         }
         return kTRUE;
      }
   
      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionStyle                                                       //
//                                                                      //
// Graphics option: Plot style                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   const char* const kAdjustText[3] = {"Left", "Center", "Right"};
   const char* const kMarginText[4] = {"L", "R", "T", "B"};

//______________________________________________________________________________
   TLGOptionStyle::TLGOptionStyle (const TGWindow* p, Int_t id, 
                     OptionStyle_t* optvals)
   : TLGOptions (p, "", id, optvals)
   {
      // Layout hints
      fL1 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 4);
      fL2 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 0);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 5, 2, 2);
      fL4 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 0, 5, 2, 2);
      fL5 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, -6);
      fL6 = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 0, 5, 2, 2);
   
      // group frames
      fG1 = new TGGroupFrame (this, "Title");
      AddFrame (fG1, fL1);
      fG2 = new TGGroupFrame (this, "Margins");
      AddFrame (fG2, fL1);
      // line frames
      fF1 = new TGHorizontalFrame (fG1, 100, 100);
      fG1->AddFrame (fF1, fL2);
      fF2 = new TGHorizontalFrame (fG1, 100, 100);
      fG1->AddFrame (fF2, fL2);
      fF3 = new TGHorizontalFrame (fG1, 100, 100);
      fG1->AddFrame (fF3, fL5);
      fF4 = new TGHorizontalFrame (fG2, 100, 100);
      fG2->AddFrame (fF4, fL5);
   
      // title group
      fTitle = new TLGTextEntry (fF1, "", kGOptStyleTitle);
      fTitle->Associate (this);
      fF1->AddFrame (fTitle, fL4);
      fTitleFont = new TLGFontSelection (fF2, kGOptStyleTitleFont, kTRUE);
      fTitleFont->Associate (this);
      fF2->AddFrame (fTitleFont, fL6);
      for (Int_t j = 0; j < 3; j++) {
         fTitleAdjust[j] = new TGRadioButton (fF3, kAdjustText[j], 
                              kGOptStyleTitleAdjust + j);
         fTitleAdjust[j]->Associate (this);
         fF3->AddFrame (fTitleAdjust[j], fL3);
      }
      fTitleColor = new TLGColorComboBox (fF3, kGOptStyleTitleColor);
      fTitleColor->Associate (this);
      fF3->AddFrame (fTitleColor, fL6);
   
      // Margin group
      for (Int_t i = 0; i < 4; i++) {
         fMarginLabel[i] = new TGLabel (fF4, kMarginText[i]);
         fF4->AddFrame (fMarginLabel[i], fL3);
         fMargin[i] = new TLGNumericControlBox (fF4, 0, 4,
                              kGOptStyleMargin + i, kNESRealTwo,
                              kNEANonNegative, kNELLimitMinMax, 0.01, 0.98);
         fMargin[i]->Associate (this);
         fF4->AddFrame (fMargin[i], fL3);
      }
   
      UpdateOptions ();
   }

//______________________________________________________________________________
   TLGOptionStyle::~TLGOptionStyle ()
   {
      delete fTitle;
      delete fTitleColor;
      delete fTitleFont;
      delete fTitleAdjust[0];
      delete fTitleAdjust[1];
      delete fTitleAdjust[2];
      for (Int_t i = 0; i < 4; i++) {
         delete fMarginLabel[i];
         delete fMargin[i];
      }
      delete fG1;
      delete fG2;
      delete fF1;
      delete fF2;
      delete fF3;
      delete fF4;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
   }

//______________________________________________________________________________
   void TLGOptionStyle::UpdateOptions ()
   {
      OptionStyle_t*	vals = (OptionStyle_t*) fOptionValues;
      //cerr << "TLGOptionStyle::UpdateOptions() " << endl ;
      if (vals != 0) {
         fTitle->SetText (vals->fTitle);
         fTitleColor->Select (vals->fTitleAttr.GetTextColor());
         fTitleFont->SetFont (vals->fTitleAttr.GetTextFont());
         fTitleFont->SetFontSize (vals->fTitleAttr.GetTextSize());
         Int_t k = vals->fTitleAttr.GetTextAlign() / 10 - 1;
         for (Int_t j = 0; j < 3; j++) {
            fTitleAdjust[j]->SetState (((j == k) ? kButtonDown : kButtonUp));
         }
         for (Int_t i = 0; i < 4; i++) {
            fMargin[i]->SetNumber (vals->fMargin[i]);
         }
      }
      //cerr << "TLGOptionStyle::UpdateOptions() end " << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionStyle::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionStyle_t* vals = (OptionStyle_t*) fOptionValues;
      Bool_t readtxt = kFALSE;
   
      // Radio buttons
      if ((GET_MSG (msg) == kC_COMMAND) && 
         (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) {
         // title adjustment
         if ((parm1 >= kGOptStyleTitleAdjust) &&
            (parm1 < kGOptStyleTitleAdjust + 3)) {
            Int_t k = parm1 - kGOptStyleTitleAdjust;
            if (k == vals->fTitleAttr.GetTextAlign() / 10 - 1) {
               return kTRUE;
            }
            vals->fTitleAttr.SetTextAlign (10 * (k + 1) + 3);
            for (Int_t j = 0; j < 3; j++) {
               fTitleAdjust[j]->SetState ((j == k) ? 
                                    kButtonDown : kButtonUp);
            }
         }
         readtxt = kTRUE;
      }
      
      // Combo boxes
      else if ((GET_MSG (msg) == kC_COMMAND) && 
              (GET_SUBMSG (msg) == kCM_COMBOBOX)) {
         // Title color
         if (parm1 == kGOptStyleTitleColor) {
            if (vals->fTitleAttr.GetTextColor() == parm2) {
               return kTRUE;
            }
            vals->fTitleAttr.SetTextColor(parm2);
         }
         // Title font selection
         else if (parm1 == kGOptStyleTitleFont) {
            if (vals->fTitleAttr.GetTextFont() == parm2) {
               return kTRUE;
            }
            vals->fTitleAttr.SetTextFont (parm2);
         }
         readtxt = kTRUE;
      }
   
      // Text and numeric entry
      if ((GET_MSG (msg) == kC_TEXTENTRY) && 
         (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) {
         // title
         if (parm1 == kGOptStyleTitle) {
         }
         // title size
         else if (parm1 == kGOptStyleTitleFont) {
            vals->fTitleAttr.SetTextSize (fTitleFont->GetFontSize());
         }
         // plot margin
         else if ((parm1 >= kGOptStyleMargin) && 
                 (parm1 < kGOptStyleMargin + 4)) {
            Int_t i = parm1 - kGOptStyleMargin;
            vals->fMargin[i] = fMargin[i]->GetNumber();
            Int_t opp = (i % 2 == 0) ? i + 1 : i - 1;
            if (vals->fMargin[i] + vals->fMargin[opp] > 0.99) {
               vals->fMargin[opp] = 0.99 - vals->fMargin[i];
               fMargin[opp]->SetNumber (vals->fMargin[opp]);
            }
         }
         readtxt = kTRUE;
      }
      if (readtxt) {
         vals->fTitle = fTitle->GetText();
      }
   
      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionAxis                                                        //
//                                                                      //
// Graphics option: Axis                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionAxis::TLGOptionAxis (const TGWindow* p, Int_t id, 
                     OptionAxis_t* optvals, Bool_t xaxis)
   : TLGOptions (p, xaxis ? kGOptAxisXName : kGOptAxisYName, id, optvals),
   fXAxis (xaxis)
   {
      Int_t	optOfs = fXAxis ? 0 : 50;
   
      // Layout hints
      fL1 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 4);
      fL2 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 0);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 5, 2, 2);
      fL4 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 0, 5, 2, 2);
      fL5 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, -6);
      fL6 = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 0, 5, 2, 2);
   
      // group frames
      fF1 = new TGGroupFrame (this, "Title");
      AddFrame (fF1, fL1);
      fF2 = new TGGroupFrame (this, "Ticks/Axis");
      AddFrame (fF2, fL1);
      fF3 = new TGGroupFrame (this, "Labels");
      AddFrame (fF3, fL1);
      fF9 = new TGGroupFrame (this, "Font");
      AddFrame (fF9, fL1);
      // line frames
      fF4 = new TGHorizontalFrame (fF1, 100, 100);
      fF1->AddFrame (fF4, fL2);
      fF5 = new TGHorizontalFrame (fF1, 100, 100);
      fF1->AddFrame (fF5, fL5);
      fF6 = new TGHorizontalFrame (fF2, 100, 100);
      fF2->AddFrame (fF6, fL2);
      fF7 = new TGHorizontalFrame (fF2, 100, 100);
      fF2->AddFrame (fF7, fL5);
      fF8 = new TGHorizontalFrame (fF3, 100, 100);
      fF3->AddFrame (fF8, fL5);
      fF10 = new TGHorizontalFrame (fF9, 100, 100);
      fF9->AddFrame (fF10, fL5);
   
      // title
      fTitle = new TLGTextEntry (fF4, "", kGOptAxisXTitle + optOfs);
      fTitle->Associate (this);
      fF4->AddFrame (fTitle, fL4);
      fTitleSizeLabel = new TGLabel (fF5, "Size:");
      fF5->AddFrame (fTitleSizeLabel, fL3);
      fTitleSize = new TLGNumericControlBox (fF5, 0, 5,
                           kGOptAxisXTitleSize + optOfs, kNESRealThree,
                           kNEANonNegative);
      fTitleSize->Associate (this);
      fF5->AddFrame (fTitleSize, fL3);
      fTitleOfsLabel = new TGLabel (fF5, "Offset:");
      fF5->AddFrame (fTitleOfsLabel, fL3);
      fTitleOfs = new TLGNumericControlBox (fF5, 0, 4,
                           kGOptAxisXTitleOfs + optOfs, kNESRealTwo,
                           kNEANonNegative);
      fTitleOfs->Associate (this);
      fF5->AddFrame (fTitleOfs, fL3);
      fTitleColor = new TLGColorComboBox (fF5, 
                           kGOptAxisXTitleColor + optOfs);
      fTitleColor->Associate (this);
      fF5->AddFrame (fTitleColor, fL6);
   
      // ticks
      fGrid = new TGCheckButton (fF6, "Grid",
                           kGOptAxisXGrid + optOfs);
      fGrid->Associate (this);
      fF6->AddFrame (fGrid, fL6);
      fTicksBoth = new TGCheckButton (fF6, "Both sides",
                           kGOptAxisXTicksBoth + optOfs);
      fTicksBoth->Associate (this);
      fF6->AddFrame (fTicksBoth, fL6);
      fTicksLenLabel = new TGLabel (fF6, "Length:");
      fF6->AddFrame (fTicksLenLabel, fL3);
      fTicksLen = new TLGNumericControlBox (fF6, 0, 5,
                           kGOptAxisXTicksLen + optOfs, kNESRealThree,
                           kNEANonNegative);
      fTicksLen->Associate (this);
      fF6->AddFrame (fTicksLen, fL3);
      fTicksDivLabel = new TGLabel (fF7, "Divisions: ");
      fF7->AddFrame (fTicksDivLabel, fL3);
      for (int i = 0; i < 3; i++) {
         fTicksDiv[i] = new TLGNumericControlBox (fF7, 0, 3,
                              kGOptAxisXTicksDiv + i + optOfs, kNESInteger,
                              kNEANonNegative, kNELLimitMinMax, 0, 99);
         fTicksDiv[i]->Associate (this);
         fF7->AddFrame (fTicksDiv[i], fL3);
      }
      fAxisColor = new TLGColorComboBox (fF7, 
                           kGOptAxisXAxisColor + optOfs);
      fAxisColor->Associate (this);
      fF7->AddFrame (fAxisColor, fL6);
   
      // labels
      fLabelsSizeLabel = new TGLabel (fF8, "Size:");
      fF8->AddFrame (fLabelsSizeLabel, fL3);
      fLabelsSize = new TLGNumericControlBox (fF8, 0, 5,
                           kGOptAxisXLabelsSize + optOfs, kNESRealThree,
                           kNEANonNegative);
      fLabelsSize->Associate (this);
      fF8->AddFrame (fLabelsSize, fL3);
      fLabelsOfsLabel = new TGLabel (fF8, "  Offset:");
      fF8->AddFrame (fLabelsOfsLabel, fL3);
      fLabelsOfs = new TLGNumericControlBox (fF8, 0, 5,
                           kGOptAxisXLabelsOfs + optOfs, kNESRealThree,
                           kNEANonNegative);
      fLabelsOfs->Associate (this);
      fF8->AddFrame (fLabelsOfs, fL3);
      fLabelsColor = new TLGColorComboBox (fF8, 
                           kGOptAxisXLabelsColor + optOfs);
      fLabelsColor->Associate (this);
      fF8->AddFrame (fLabelsColor, fL6);
   
      // style
      fFontSel = new TLGFontSelection (fF10, kGOptAxisXFont + optOfs);
      fFontSel->Associate (this);
      fF10->AddFrame (fFontSel, fL3);
      fTitleCenter = new TGCheckButton (fF10, "Center",
                           kGOptAxisXTitleCenter + optOfs);
      fTitleCenter->Associate (this);
      fF10->AddFrame (fTitleCenter, fL6);
   
      UpdateOptions();
   }

//______________________________________________________________________________
   TLGOptionAxis::~TLGOptionAxis ()
   {
      delete fTitle;
      delete fTitleCenter;
      delete fTitleSizeLabel;
      delete fTitleSize;
      delete fTitleOfsLabel;
      delete fTitleOfs;
      delete fTicksLenLabel;
      delete fTicksLen;
      delete fTicksBoth;
      delete fTicksDivLabel;
      for (int i = 0; i < 3; i++) {
         delete fTicksDiv[i];
      }
      delete fAxisColor;
      delete fLabelsSizeLabel;
      delete fLabelsSize;
      delete fLabelsOfsLabel;
      delete fLabelsOfs;
      delete fLabelsColor;
      delete fGrid;
      delete fFontSel;
      delete fTitleColor;
      delete fF1;
      delete fF2;
      delete fF3;
      delete fF4;
      delete fF5;
      delete fF6;
      delete fF7;
      delete fF8;
      delete fF9;
      delete fF10;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
   }

//______________________________________________________________________________
   void TLGOptionAxis::UpdateOptions ()
   {
      OptionAxis_t* vals = (OptionAxis_t*) fOptionValues;
   
      //cerr << "TLGOptionAxis::UpdateOptions() " << endl ;
      fTitle->SetText (vals->fAxisTitle);
      if (vals->fCenterTitle) {
         fTitleCenter->SetState (kButtonDown);
      }
      else {
         fTitleCenter->SetState (kButtonUp);
      }
      fTitleSize->SetNumber (vals->fAxisAttr.GetTitleSize());
      fTitleOfs->SetNumber (vals->fAxisAttr.GetTitleOffset());
      fTicksLen->SetNumber (vals->fAxisAttr.GetTickLength());
      if (vals->fBothSides) {
         fTicksBoth->SetState (kButtonDown);
      }
      else {
         fTicksBoth->SetState (kButtonUp);
      }
      fTicksDiv[0]->SetIntNumber (vals->fAxisAttr.GetNdivisions() % 100);
      fTicksDiv[1]->SetIntNumber (vals->fAxisAttr.GetNdivisions() / 100 % 100);
      fTicksDiv[2]->SetIntNumber (vals->fAxisAttr.GetNdivisions() / 10000);
      fAxisColor->Select (vals->fAxisAttr.GetAxisColor());
      fLabelsSize->SetNumber (vals->fAxisAttr.GetLabelSize());
      fLabelsOfs->SetNumber (vals->fAxisAttr.GetLabelOffset());
      fLabelsColor->Select (vals->fAxisAttr.GetLabelColor());
      if (vals->fGrid) {
         fGrid->SetState (kButtonDown);
      }
      else {
         fGrid->SetState (kButtonUp);
      }
      fFontSel->SetFont (vals->fAxisAttr.GetLabelFont());
      fTitleColor->Select (vals->fAxisAttr.GetTitleColor());
      //cerr << "TLGOptionAxis::UpdateOptions() end" << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionAxis::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionAxis_t* vals = (OptionAxis_t*) fOptionValues;
      Bool_t readtxt = kFALSE;
   
      switch (GET_MSG (msg)) {
         // command messages
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  // buttons, checkbox
                  case kCM_CHECKBUTTON:
                     {
                        int id = fXAxis ? parm1 : parm1 - 50;
                       // title centering
                        if (id == kGOptAxisXTitleCenter) {
                           vals->fCenterTitle = 
                              (fTitleCenter->GetState() == kButtonDown);
                        }
                        // ticks on both sides
                        else if (id == kGOptAxisXTicksBoth) {
                           vals->fBothSides = 
                              (fTicksBoth->GetState() == kButtonDown);
                        }
                        // grid
                        else if (id == kGOptAxisXGrid) {
                           vals->fGrid = 
                              (fGrid->GetState() == kButtonDown);
                        }
                        readtxt = kTRUE;
                        break;
                     }
                  case kCM_COMBOBOX:
                     {
                        int id = fXAxis ? parm1 : parm1 - 50;
                        // Axis color
                        if (id == kGOptAxisXAxisColor) {
                           if (vals->fAxisAttr.GetAxisColor() == parm2) {
                              return kTRUE;
                           }
                           vals->fAxisAttr.SetAxisColor(parm2);
                        }
                        // Labels color
                        else if (id == kGOptAxisXLabelsColor) {
                           if (vals->fAxisAttr.GetLabelColor() == parm2) {
                              return kTRUE;
                           }
                           vals->fAxisAttr.SetLabelColor(parm2);
                        }
                        // Title color
                        else if (id == kGOptAxisXTitleColor) {
                           if (vals->fAxisAttr.GetTitleColor() == parm2) {
                              return kTRUE;
                           }
                           vals->fAxisAttr.SetTitleColor(parm2);
                        }
                        // font selection
                        else if (id == kGOptAxisXFont) {
                           if (vals->fAxisAttr.GetLabelFont() == parm2) {
                              return kTRUE;
                           }
                           vals->fAxisAttr.SetLabelFont(parm2);
                           vals->fAxisAttr.SetTitleFont(parm2);
                        }
                        readtxt = kTRUE;
                        break;
                     }
               }
               break;
            }
         // textentry
         case kC_TEXTENTRY:
            {
               switch (GET_SUBMSG (msg)) {
                  case kTE_TEXTUPDATED:
                     {
                        int id = fXAxis ? parm1 : parm1 - 50;
                        // title
                        if (id == kGOptAxisXTitle) {
                        }
                        // title size
                        else if (id == kGOptAxisXTitleSize) {
                           vals->fAxisAttr.SetTitleSize 
                              (fTitleSize->GetNumber());
                        }
                        // title offset
                        else if (id == kGOptAxisXTitleOfs) {
                           vals->fAxisAttr.SetTitleOffset 
                              (fTitleOfs->GetNumber());
                        }
                        // ticks length
                        else if (id == kGOptAxisXTicksLen) {
                           vals->fAxisAttr.SetTickLength 
                              (fTicksLen->GetNumber());
                        }
                        // ticks division 1
                        else if (id == kGOptAxisXTicksDiv) {
                           Int_t div = vals->fAxisAttr.GetNdivisions();
                           div = 100 * (div / 100) + 
                              fTicksDiv[0]->GetIntNumber();
                           vals->fAxisAttr.SetNdivisions (div);
                        }
                        // ticks division 2
                        else if (id == kGOptAxisXTicksDiv + 1) {
                           Int_t div = vals->fAxisAttr.GetNdivisions();
                           div = 10000 * (div / 10000) + div % 100 +
                              100 * fTicksDiv[1]->GetIntNumber();
                           vals->fAxisAttr.SetNdivisions (div);
                        }
                        // ticks division 3
                        else if (id == kGOptAxisXTicksDiv + 2) {
                           Int_t div = vals->fAxisAttr.GetNdivisions();
                           div = div % 10000 + 
                              10000 * fTicksDiv[2]->GetIntNumber();
                           vals->fAxisAttr.SetNdivisions (div);
                        }
                        // labels size
                        else if (id == kGOptAxisXLabelsSize) {
                           vals->fAxisAttr.SetLabelSize 
                              (fLabelsSize->GetNumber());
                        }
                        // labels offset
                        else if (id == kGOptAxisXLabelsOfs) {
                           vals->fAxisAttr.SetLabelOffset 
                              (fLabelsOfs->GetNumber());
                        }
                        readtxt = kTRUE;
                        break;
                     }
               }
               break;
            }
      }
      if (readtxt) {
         vals->fAxisTitle = fTitle->GetText();
      }
      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionLegend                                                      //
//                                                                      //
// Graphics option: Legend                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionLegend::TLGOptionLegend (const TGWindow* p, Int_t id, 
                     OptionLegend_t* optvals)
   : TLGOptions (p, kGOptLegendName, id, optvals)
   {

        auto main_vert_layout =  new TGVerticalLayout(this);
        this->SetLayoutManager(main_vert_layout);

        // show
        fShow = new TGCheckButton(this, "Show", kGOptLegendShow);
        fShow->Associate (this);
        this->AddFrame(fShow);

        // ********* placement group ********
        auto placement_hint = new TGLayoutHints( kLHintsExpandX );
        auto placement_group = new TGGroupFrame(this, "Placement");


        auto place_layout = new TGTableLayout(placement_group, 3, 6);
        //auto place_layout = new TGVerticalLayout(placement_group);
        auto p1 = place_layout;
        placement_group->SetLayoutManager(place_layout);



        {
          //placement radios
          auto top_left_hint = new TGTableLayoutHints(0,2,0,1,
                                                      kLHintsNormal,
                                                      2,2,2,2
                                                      );
          fPlace_top_left = new TGRadioButton(placement_group, "Top left   ", kGOptLegendPlaceTopLeft);
          fPlace_top_left->Associate(this);
          placement_group->AddFrame(fPlace_top_left, top_left_hint);

          auto top_right_hint = new TGTableLayoutHints(2,4,0,1,
                                                      kLHintsNormal,
                                                          2,2,2,2
                                                       );
          fPlace_top_right = new TGRadioButton(placement_group, "Top Right   ", kGOptLegendPlaceTopRight);
          fPlace_top_right->Associate(this);
          placement_group->AddFrame(fPlace_top_right, top_right_hint);

          auto bottom_right_hint = new TGTableLayoutHints(2,4,1,2,
                                                          kLHintsNormal,
                                                              2,2,2,2
                                                          );
          fPlace_bottom_right = new TGRadioButton(placement_group, "Bottom Right   ", kGOptLegendPlaceBottomRight);
          fPlace_bottom_right->Associate(this);
          placement_group->AddFrame(fPlace_bottom_right, bottom_right_hint);

          auto bottom_left_hint = new TGTableLayoutHints(0,2,1,2,
                                                         kLHintsNormal,
                                                         2,2,2,2
                                                         );
          fPlace_bottom_left = new TGRadioButton(placement_group, "Bottom left   ", kGOptLegendPlaceBottomLeft);
          fPlace_bottom_left->Associate(this);
          placement_group->AddFrame(fPlace_bottom_left, bottom_left_hint);

          //placement tweek controls
          auto labelx_hint = new TGTableLayoutHints( 0,1,2,3,
                                                                            kLHintsCenterY|kLHintsRight
                                                    );
          auto place_label_x = new TGLabel(placement_group, "X:");
          placement_group->AddFrame(place_label_x, labelx_hint);

          auto xadj_hint = new TGTableLayoutHints(1,2,2,3,
                                                                         kLHintsLeft,
                                                                              5,0,2,0);
          fXAdj = new TLGNumericControlBox(placement_group, 0, 5, kGOptLegendXAdjust, kNESRealTwo);
          fXAdj->Associate(this);
          placement_group->AddFrame(fXAdj, xadj_hint);

          //placement tweek controls
          auto labely_hint = new TGTableLayoutHints( 2,3,2,3,
                                                                            kLHintsCenterY|kLHintsRight
                                                    );
          auto place_label_y = new TGLabel(placement_group, "Y:");
          placement_group->AddFrame(place_label_y, labely_hint);

          auto yadj_hint = new TGTableLayoutHints(3,4,2,3,
                                                                         kLHintsLeft,
                                                                          5,0,2,0);
          fYAdj = new TLGNumericControlBox(placement_group, 0, 5, kGOptLegendYAdjust, kNESRealTwo);
          fYAdj->Associate(this);
          placement_group->AddFrame(fYAdj, yadj_hint);

          auto label_size_hint =  new TGTableLayoutHints( 4,5,2,3,
                                                                               kLHintsCenterY | kLHintsRight
                                                        );
          auto label_size = new TGLabel(placement_group, "Size:");
          placement_group->AddFrame(label_size, label_size_hint);

          auto size_hint = new TGTableLayoutHints(5,6,2,3,
                                                  kLHintsNormal,
                                                  5,0,2,0
                                                  );
          fSize = new TLGNumericControlBox(placement_group, 0, 4,
                           kGOptLegendTextSize, kNESRealOne,
                           kNEANonNegative);
          fSize->Associate(this);
          placement_group->AddFrame(fSize, size_hint);
        }

        this->AddFrame (placement_group, placement_hint);

        // ************  Symbol Style ************
        auto symb_style_group = new TGGroupFrame(this, "Symbol style");

        auto symb_style_layout = new TGHorizontalLayout(symb_style_group);
        symb_style_group->SetLayoutManager(symb_style_layout);

        {
          fSymbolStyleSame = new TGRadioButton(
                                          symb_style_group,
                                          "Same as trace   ",
                                          kGOptLegendSymbolSame);
          fSymbolStyleSame->Associate(this);
          symb_style_group->AddFrame(fSymbolStyleSame);

          fSymbolStyleNone = new TGRadioButton(
                                          symb_style_group,
                                          "None   ",
                                          kGOptLegendSymbolNone);
          fSymbolStyleNone->Associate(this);
          symb_style_group->AddFrame(fSymbolStyleNone);
        }
        this->AddFrame (symb_style_group, new TGLayoutHints(kLHintsExpandX));

        // ************ Text ********************
        auto text_group = new TGGroupFrame(this, "Text");
        this->AddFrame (text_group, new TGLayoutHints(kLHintsExpandX));

        auto text_layout = new TGTableLayout(text_group,3,4);
        text_group->SetLayoutManager(text_layout);

        {
          auto text_auto_hint = new TGTableLayoutHints(0,1,0,1);
          fTextSelAuto = new TGRadioButton(text_group, "Auto   ",
                                               kGOptLegendTextSelAuto);
          fTextSelAuto->Associate(this);
          text_group->AddFrame(fTextSelAuto, text_auto_hint);

          auto text_user_hint = new TGTableLayoutHints(1,2,0,1);
          fTextSelUser = new TGRadioButton(text_group, "User   ",
                                                    kGOptLegendTextSelUser);
          fTextSelUser->Associate(this);
          text_group->AddFrame(fTextSelUser, text_user_hint);

          auto text_tab_hint = new TGTableLayoutHints(0,4,1,2,
                                                      kLHintsNormal|kLHintsExpandX,
                                                      0,0,5,0);
          fTextTab = new TGTabTraces(text_group);
          fTextTab->Associate (this);
          text_group->AddFrame (fTextTab, text_tab_hint);
          for (int i = 0; i < kMaxTraces; i++) {
            char label[4];
            sprintf (label, "%i", i);
            fTextTab->AddTab (label);
          }

          auto text_hint = new TGTableLayoutHints(0,4,2,3,
                                                  kLHintsExpandX|kLHintsLeft,
                                                  0,0,5,0);
          fText = new TLGTextEntry(text_group, "",
                                            kGOptLegendText);
          fText->Associate (this);
          text_group->AddFrame (fText, text_hint);
          fText->Resize(400,25);
        }

     fCurTrace = 0;
     UpdateOptions();

   }

//______________________________________________________________________________
   void TLGOptionLegend::UpdateOptions ()
   {
      OptionLegend_t* vals = (OptionLegend_t*) fOptionValues;
   
      //cerr << "TLGOptionLegend::UpdateOptions()" << endl ;
      fShow->SetState (vals->fShow ? kButtonDown : kButtonUp);

     // ** placement **

      if(vals->fPlacement == kLegendTopLeft) {
        fPlace_top_left->SetState(kButtonDown);
      }
      else {
        fPlace_top_left->SetState(kButtonUp);
      }

      if(vals->fPlacement == kLegendTopRight) {
        fPlace_top_right->SetState(kButtonDown);
      }
      else {
        fPlace_top_right->SetState(kButtonUp);
      }

      if(vals->fPlacement == kLegendBottomRight) {
        fPlace_bottom_right->SetState(kButtonDown);
      }
      else {
        fPlace_bottom_right->SetState(kButtonUp);
      }

      if(vals->fPlacement == kLegendBottomLeft) {
        fPlace_bottom_left->SetState(kButtonDown);
      }
      else {
        fPlace_bottom_left->SetState(kButtonUp);
      }

      fXAdj->SetNumber (vals->fXAdjust);
      fYAdj->SetNumber (vals->fYAdjust);
      fSize->SetNumber (vals->fSize);

     // *** Symbols ***
     if(vals->fSymbolStyle == kLegendSameAsTrace) {
        fSymbolStyleSame->SetState(kButtonDown);
      }
      else {
        fSymbolStyleSame->SetState(kButtonUp);
      }
      if(vals->fSymbolStyle == kLegendNone) {
        fSymbolStyleNone->SetState(kButtonDown);
      }
      else {
        fSymbolStyleNone->SetState(kButtonUp);
      }

     // **** TEXT ****
      if(vals->fTextStyle == kLegendAutoText) {
        fTextSelAuto->SetState(kButtonDown);
      }
      else {
        fTextSelAuto->SetState(kButtonUp);
      }
      if(vals->fTextStyle == kLegendUserText) {
        fTextSelUser->SetState(kButtonDown);
      }
      else {
        fTextSelUser->SetState(kButtonUp);
      }
      if ((fCurTrace < 0) || (fCurTrace >= kMaxTraces)) {
         fCurTrace = 0;
      }
      fText->SetState (vals->fTextStyle == kLegendUserText);
      fText->SetText (vals->fText[fCurTrace]);
      //cerr << "TLGOptionLegend::UpdateOptions() end" << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionLegend::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionLegend_t* vals = (OptionLegend_t*) fOptionValues;



      // check buttons
      if ((GET_MSG (msg) == kC_COMMAND) && 
         (GET_SUBMSG (msg) == kCM_CHECKBUTTON)) {
         if (parm1 == kGOptLegendShow) {
            vals->fShow = fShow->GetState() == kButtonDown;
         } 
      }
      // radio button
      else if ((GET_MSG (msg) == kC_COMMAND) && 
              (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) {
         // placement
         switch(parm1)
         {
         case kGOptLegendPlaceTopLeft:
           vals->fPlacement = kLegendTopLeft;
           break;
         case kGOptLegendPlaceTopRight:
           vals->fPlacement = kLegendTopRight;
           break;
         case kGOptLegendPlaceBottomLeft:
           vals->fPlacement = kLegendBottomLeft;
           break;
         case kGOptLegendPlaceBottomRight:
           vals->fPlacement = kLegendBottomRight;
           break;
         case kGOptLegendSymbolSame:
           vals->fSymbolStyle = kLegendSameAsTrace;
           break;
         case kGOptLegendSymbolNone:
           vals->fSymbolStyle = kLegendNone;
           break;
         case kGOptLegendTextSelAuto:
           vals->fTextStyle = kLegendAutoText;
           break;
         case kGOptLegendTextSelUser:
           vals->fTextStyle = kLegendUserText;
           break;
         }
        UpdateOptions();
      }
      // tab widget
      else if ((GET_MSG (msg) == kC_COMMAND) && 
              (GET_SUBMSG (msg) == kCM_TAB)) {
         if ((parm1 >= 0) && (parm1 < kMaxTraces)) {
            if (fCurTrace != parm1) {
               fCurTrace = parm1;
               fText->SetText(vals->fText[fCurTrace]);
            }
         }
         return kTRUE;
      }
      // text & numeric entry
      else if ((GET_MSG (msg) == kC_TEXTENTRY) && 
              (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) {
         if (parm1 == kGOptLegendXAdjust) {
            vals->fXAdjust = fXAdj->GetNumber();
         }
         else if (parm1 == kGOptLegendYAdjust) {
            vals->fYAdjust = fYAdj->GetNumber();
         }
         else if (parm1 == kGOptLegendTextSize) {
            vals->fSize = fSize->GetNumber();
         }
         else if (parm1 == kGOptLegendText) {
           //this should only happen on text change
           if ((fCurTrace >= 0) && (fCurTrace < kMaxTraces)) {
             vals->fText[fCurTrace] = fText->GetText();
           }
         }
      }

      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionParam                                                       //
//                                                                      //
// Graphics option: Parameters                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionParam::TLGOptionParam (const TGWindow* p, Int_t id, 
                     OptionParam_t* optvals)
   : TLGOptions (p, kGOptParamName, id, optvals)
   {
      // Layout hints
      fL1 = new TGLayoutHints (kLHintsLeft | kLHintsTop, 3, 2, 2, 2);
      fL2 = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 0, 0, 1, -10);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 3, 2, 2);
      fL4 = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 4);
      // group frames
      fFS = new TGHorizontalFrame (this, 100, 100);
      AddFrame (fFS, fL4);
      fGT = new TGGroupFrame (this, "Time format");
      AddFrame (fGT, fL4);
      fGV = new TGGroupFrame (this, "Variable");
      AddFrame (fGV, fL4);
      // show
      fShow = new TGCheckButton (fFS, "Show", kGOptParamShow);
      fShow->Associate (this);
      fFS->AddFrame (fShow, fL1);
      // time/date format
      fF[0] = new TGHorizontalFrame (fGT, 100, 100);
      fGT->AddFrame (fF[0], fL2);
      fTimeFormat[0] = new TGRadioButton (fF[0], "Date/time UTC   ", 
                           kGOptParamTimeFormat);
      fTimeFormat[0]->Associate (this);
      fF[0]->AddFrame (fTimeFormat[0], fL3);
      fTimeFormat[1] = new TGRadioButton (fF[0], "GPS seconds", 
                           kGOptParamTimeFormat + 1);
      fTimeFormat[1]->Associate (this);
      fF[0]->AddFrame (fTimeFormat[1], fL3);
      // Variable
      fVar[0] = new TGCheckButton (fGV, "Start time", kGOptParamVar);
      fVar[0]->Associate (this);
      fGV->AddFrame (fVar[0], fL3);
      fVar[1] = new TGCheckButton (fGV, "Number of averages", 
                           kGOptParamVar + 1);
      fVar[1]->Associate (this);
      fGV->AddFrame (fVar[1], fL3);
      fVar[2] = new TGCheckButton (fGV, "Third parameter",
                           kGOptParamVar + 2);
      fVar[2]->Associate (this);
      fGV->AddFrame (fVar[2], fL3);
      // hist (mito)
      fVar[3] = new TGCheckButton (fGV, "Statistics",
                           kGOptParamVar + 3);
      fVar[3]->Associate (this);
      fGV->AddFrame (fVar[3], fL3);
   	// hist (mito) 8/8/2002
      fVar[4] = new TGCheckButton (fGV, "Histogram Under/Overflow",
                           kGOptParamVar + 4);
      fVar[4]->Associate (this);
      fGV->AddFrame (fVar[4], fL3);
   
      UpdateOptions();
   }

//______________________________________________________________________________
   TLGOptionParam::~TLGOptionParam ()
   {
      delete fVar[0];
      delete fVar[1];
      delete fVar[2];
      delete fVar[3];
      delete fVar[4];
      delete fTimeFormat[0];
      delete fTimeFormat[1];
      delete fShow;
      delete fF[0];
      delete fGV;
      delete fGT;
      delete fFS;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
   }

//______________________________________________________________________________
   void TLGOptionParam::UpdateOptions ()
   {
      OptionParam_t* vals = (OptionParam_t*) fOptionValues;
   
      //cerr << "TLGOptionParam::UpdateOptions()" << endl ;
      fShow->SetState (vals->fShow ? kButtonDown : kButtonUp);
      fTimeFormat[0]->SetState (vals->fTimeFormatUTC ?
                           kButtonDown : kButtonUp);
      fTimeFormat[1]->SetState (!vals->fTimeFormatUTC ?
                           kButtonDown : kButtonUp);
      fVar[0]->SetState (vals->fT0 ? kButtonDown : kButtonUp);
      fVar[1]->SetState (vals->fAvg ? kButtonDown : kButtonUp);
      fVar[2]->SetState (vals->fSpecial ? kButtonDown : kButtonUp);
      fVar[3]->SetState (vals->fStat ? kButtonDown : kButtonUp); // hist (mito)
      fVar[4]->SetState (vals->fUOBins ? kButtonDown : kButtonUp); // hist (mito) 8/8/2002
      //cerr << "TLGOptionParam::UpdateOptions() end" << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionParam::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      OptionParam_t* vals = (OptionParam_t*) fOptionValues;
   
      // check buttons
      if ((GET_MSG (msg) == kC_COMMAND) && 
         (GET_SUBMSG (msg) == kCM_CHECKBUTTON)) {
         if (parm1 == kGOptParamShow) {
            vals->fShow = fShow->GetState() == kButtonDown;
         }
         else if (parm1 == kGOptParamVar) {
            vals->fT0 = fVar[0]->GetState() == kButtonDown;
         } 
         else if (parm1 == kGOptParamVar + 1) {
            vals->fAvg = fVar[1]->GetState() == kButtonDown;
         } 
         else if (parm1 == kGOptParamVar + 2) {
            vals->fSpecial = fVar[2]->GetState() == kButtonDown;
         }
         else if (parm1 == kGOptParamVar + 3) {
            vals->fStat = fVar[3]->GetState() == kButtonDown;
         }
         else if (parm1 == kGOptParamVar + 4) {
            vals->fUOBins = fVar[4]->GetState() == kButtonDown;
         }
      }
      // radio button
      else if ((GET_MSG (msg) == kC_COMMAND) && 
              (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) {
         Int_t id = parm1 - kGOptParamTimeFormat;
         if (((id == 0) && vals->fTimeFormatUTC)  ||
            ((id == 1) && !vals->fTimeFormatUTC)) {
            return kTRUE;
         }
         vals->fTimeFormatUTC = (id == 0);
         fTimeFormat[0]->SetState (vals->fTimeFormatUTC ?
                              kButtonDown : kButtonUp);
         fTimeFormat[1]->SetState (!vals->fTimeFormatUTC ?
                              kButtonDown : kButtonUp);
      }
      return TLGOptions::ProcessMessage (msg, parm1, parm2);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// SetDefaultGraphicsOptions                                            //
//                                                                      //
// Set the option values to their defaults                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void SetDefaultGraphicsOptions (OptionAll_t& opt)
   {
      opt.fName = "Default";
   
      // traces
      opt.fTraces.fGraphType = "";
      opt.fTraces.fActive[0] = kFALSE;
      opt.fTraces.fActive[1] = kFALSE;
      opt.fTraces.fActive[2] = kFALSE;
      opt.fTraces.fActive[3] = kFALSE;
      opt.fTraces.fActive[4] = kFALSE;
      opt.fTraces.fActive[5] = kFALSE;
      opt.fTraces.fActive[6] = kFALSE;
      opt.fTraces.fActive[7] = kFALSE;
      opt.fTraces.fAChannel[0] = "";
      opt.fTraces.fAChannel[1] = "";
      opt.fTraces.fAChannel[2] = "";
      opt.fTraces.fAChannel[3] = "";
      opt.fTraces.fAChannel[4] = "";
      opt.fTraces.fAChannel[5] = "";
      opt.fTraces.fAChannel[6] = "";
      opt.fTraces.fAChannel[7] = "";
      opt.fTraces.fBChannel[0] = "";
      opt.fTraces.fBChannel[1] = "";
      opt.fTraces.fBChannel[2] = "";
      opt.fTraces.fBChannel[3] = "";
      opt.fTraces.fBChannel[4] = "";
      opt.fTraces.fBChannel[5] = "";
      opt.fTraces.fBChannel[6] = "";
      opt.fTraces.fBChannel[7] = "";
      // trace style
      opt.fTraces.fPlotStyle[0] = kPlotStyleLine;
      opt.fTraces.fPlotStyle[1] = kPlotStyleLine;
      opt.fTraces.fPlotStyle[2] = kPlotStyleLine;
      opt.fTraces.fPlotStyle[3] = kPlotStyleLine;
      opt.fTraces.fPlotStyle[4] = kPlotStyleLine;
      opt.fTraces.fPlotStyle[5] = kPlotStyleLine;
      opt.fTraces.fPlotStyle[6] = kPlotStyleLine;
      opt.fTraces.fPlotStyle[7] = kPlotStyleLine;
      // trace style: line
      opt.fTraces.fLineAttr[0].SetLineColor (2);
      opt.fTraces.fLineAttr[1].SetLineColor (4);
      opt.fTraces.fLineAttr[2].SetLineColor (3);
      opt.fTraces.fLineAttr[3].SetLineColor (28);
      opt.fTraces.fLineAttr[4].SetLineColor (6);
      opt.fTraces.fLineAttr[5].SetLineColor (7);
      opt.fTraces.fLineAttr[6].SetLineColor (5);
      opt.fTraces.fLineAttr[7].SetLineColor (1);
      opt.fTraces.fLineAttr[0].SetLineStyle (1);
      opt.fTraces.fLineAttr[1].SetLineStyle (1);
      opt.fTraces.fLineAttr[2].SetLineStyle (1);
      opt.fTraces.fLineAttr[3].SetLineStyle (1);
      opt.fTraces.fLineAttr[4].SetLineStyle (1);
      opt.fTraces.fLineAttr[5].SetLineStyle (1);
      opt.fTraces.fLineAttr[6].SetLineStyle (1);
      opt.fTraces.fLineAttr[7].SetLineStyle (1);
      opt.fTraces.fLineAttr[0].SetLineWidth (1);
      opt.fTraces.fLineAttr[1].SetLineWidth (1);
      opt.fTraces.fLineAttr[2].SetLineWidth (1);
      opt.fTraces.fLineAttr[3].SetLineWidth (1);
      opt.fTraces.fLineAttr[4].SetLineWidth (1);
      opt.fTraces.fLineAttr[5].SetLineWidth (1);
      opt.fTraces.fLineAttr[6].SetLineWidth (1);
      opt.fTraces.fLineAttr[7].SetLineWidth (1);
      // trace style: : markers
      opt.fTraces.fMarkerAttr[0].SetMarkerColor (2);
      opt.fTraces.fMarkerAttr[1].SetMarkerColor (4);
      opt.fTraces.fMarkerAttr[2].SetMarkerColor (3);
      opt.fTraces.fMarkerAttr[3].SetMarkerColor (28);
      opt.fTraces.fMarkerAttr[4].SetMarkerColor (6);
      opt.fTraces.fMarkerAttr[5].SetMarkerColor (7);
      opt.fTraces.fMarkerAttr[6].SetMarkerColor (5);
      opt.fTraces.fMarkerAttr[7].SetMarkerColor (1);
      opt.fTraces.fMarkerAttr[0].SetMarkerStyle (20);
      opt.fTraces.fMarkerAttr[1].SetMarkerStyle (22);
      opt.fTraces.fMarkerAttr[2].SetMarkerStyle (21);
      opt.fTraces.fMarkerAttr[3].SetMarkerStyle (23);
      opt.fTraces.fMarkerAttr[4].SetMarkerStyle (29);
      opt.fTraces.fMarkerAttr[5].SetMarkerStyle (27);
      opt.fTraces.fMarkerAttr[6].SetMarkerStyle (2);
      opt.fTraces.fMarkerAttr[7].SetMarkerStyle (5);
      opt.fTraces.fMarkerAttr[0].SetMarkerSize (1);
      opt.fTraces.fMarkerAttr[1].SetMarkerSize (1);
      opt.fTraces.fMarkerAttr[2].SetMarkerSize (1);
      opt.fTraces.fMarkerAttr[3].SetMarkerSize (1);
      opt.fTraces.fMarkerAttr[4].SetMarkerSize (1);
      opt.fTraces.fMarkerAttr[5].SetMarkerSize (1);
      opt.fTraces.fMarkerAttr[6].SetMarkerSize (1);
      opt.fTraces.fMarkerAttr[7].SetMarkerSize (1);
      // trace style: : bars
      opt.fTraces.fBarAttr[0].SetFillColor (2);
      opt.fTraces.fBarAttr[1].SetFillColor (4);
      opt.fTraces.fBarAttr[2].SetFillColor (3);
      opt.fTraces.fBarAttr[3].SetFillColor (28);
      opt.fTraces.fBarAttr[4].SetFillColor (6);
      opt.fTraces.fBarAttr[5].SetFillColor (7);
      opt.fTraces.fBarAttr[6].SetFillColor (5);
      opt.fTraces.fBarAttr[7].SetFillColor (1);
      opt.fTraces.fBarAttr[0].SetFillStyle (1001);
      opt.fTraces.fBarAttr[1].SetFillStyle (1001);
      opt.fTraces.fBarAttr[2].SetFillStyle (1001);
      opt.fTraces.fBarAttr[3].SetFillStyle (1001);
      opt.fTraces.fBarAttr[4].SetFillStyle (1001);
      opt.fTraces.fBarAttr[5].SetFillStyle (1001);
      opt.fTraces.fBarAttr[6].SetFillStyle (1001);
      opt.fTraces.fBarAttr[7].SetFillStyle (1001);
      opt.fTraces.fBarWidth[0] = 0.1;
      opt.fTraces.fBarWidth[1] = 0.1;
      opt.fTraces.fBarWidth[2] = 0.1;
      opt.fTraces.fBarWidth[3] = 0.1;
      opt.fTraces.fBarWidth[4] = 0.1;
      opt.fTraces.fBarWidth[5] = 0.1;
      opt.fTraces.fBarWidth[6] = 0.1;
      opt.fTraces.fBarWidth[7] = 0.1;
   
      // range
      for (int i = 0; i < 2; i++) {
         opt.fRange.fAxisScale[i] = kAxisScaleLinear;
         opt.fRange.fRange[i] = kRangeAutomatic;
         opt.fRange.fRangeFrom[i] = -10.0;
         opt.fRange.fRangeTo[i] = 10.0;
      }
      opt.fRange.fBin = 1;
      opt.fRange.fBinLogSpacing = kFALSE;
   
      // units 
      opt.fUnits.fXValues = kUnitNormal;
      opt.fUnits.fYValues = kUnitMagnitude;
      opt.fUnits.fXUnit = "None";
      opt.fUnits.fYUnit = "None";
      opt.fUnits.fXMag = 0;
      opt.fUnits.fYMag = 0;
      opt.fUnits.fXSlope = 1.;
      opt.fUnits.fXOffset = 0.;
      opt.fUnits.fYSlope = 1.;
      opt.fUnits.fYOffset = 0.;
   
      // cursor control
      opt.fCursor.fActive[0] = kFALSE;
      opt.fCursor.fActive[1] = kFALSE;
      opt.fCursor.fStyle = kCursorCross;
      opt.fCursor.fType = kCursorAbsolute;
      opt.fCursor.fX[0] = 0;
      opt.fCursor.fX[1] = 1000;
      opt.fCursor.fH[0] = 0;
      opt.fCursor.fH[1] = 100;
      for (int i = 0; i < kMaxTraces; i++) {
         opt.fCursor.fValid[i] = kFALSE;
         opt.fCursor.fY[i][0] = 0;
         opt.fCursor.fY[i][1] = 0;
         opt.fCursor.fN[i] = 0;
         opt.fCursor.fXDiff[i] = 0;
         opt.fCursor.fYDiff[i] = 0;
         opt.fCursor.fMean[i] = 0;
         opt.fCursor.fRMS[i] = 0;
         opt.fCursor.fStdDev[i] = 0;
         opt.fCursor.fSum[i] = 0;
         opt.fCursor.fSqrSum[i] = 0;
         opt.fCursor.fArea[i] = 0;
         opt.fCursor.fRMSArea[i] = 0;
         opt.fCursor.fPeakX[i] = 0;
         opt.fCursor.fPeakY[i] = 0;
      }
   
      // configuration
      opt.fConfig.fAutoConf = kTRUE;
      opt.fConfig.fRespectUser = kTRUE;
      opt.fConfig.fAutoAxes = kTRUE;
      opt.fConfig.fAutoBin = kTRUE;
      opt.fConfig.fAutoTimeAdjust = kTRUE;
   
      // plot style
      opt.fStyle.fTitle = "";
      opt.fStyle.fTitleAttr.SetTextAlign (13);
      opt.fStyle.fTitleAttr.SetTextAngle (0);
      opt.fStyle.fTitleAttr.SetTextColor (1);
      opt.fStyle.fTitleAttr.SetTextFont (62);
      opt.fStyle.fTitleAttr.SetTextSize (0.05);
      opt.fStyle.fMargin[0] = 0.1;
      opt.fStyle.fMargin[1] = 0.05;
      opt.fStyle.fMargin[2] = 0.1;
      opt.fStyle.fMargin[3] = 0.18;
   
      // X axis
      opt.fAxisX.fAxisTitle = "";
      opt.fAxisX.fAxisAttr.SetAxisColor (1);
      opt.fAxisX.fAxisAttr.SetLabelColor (1);
      opt.fAxisX.fAxisAttr.SetLabelFont (62);
      opt.fAxisX.fAxisAttr.SetLabelOffset (0.005);
      opt.fAxisX.fAxisAttr.SetLabelSize (0.04);
      opt.fAxisX.fAxisAttr.SetNdivisions (510);
      opt.fAxisX.fAxisAttr.SetTickLength (0.03);
      opt.fAxisX.fAxisAttr.SetTitleOffset (1.4);
      opt.fAxisX.fAxisAttr.SetTitleSize (0.042);
      opt.fAxisX.fAxisAttr.SetTitleColor (1);
      opt.fAxisX.fAxisAttr.SetTitleFont (62);
      opt.fAxisX.fGrid = kTRUE;
      opt.fAxisX.fBothSides = kTRUE;
      opt.fAxisX.fCenterTitle = kTRUE;
      // Y axis
      opt.fAxisY.fAxisTitle = "";
      opt.fAxisY.fAxisAttr.SetAxisColor (1);
      opt.fAxisY.fAxisAttr.SetLabelColor (1);
      opt.fAxisY.fAxisAttr.SetLabelFont (62);
      opt.fAxisY.fAxisAttr.SetLabelOffset (0.005);
      opt.fAxisY.fAxisAttr.SetLabelSize (0.04);
      opt.fAxisY.fAxisAttr.SetNdivisions (510);
      opt.fAxisY.fAxisAttr.SetTickLength (0.03);
      opt.fAxisY.fAxisAttr.SetTitleOffset (0.95);
      opt.fAxisY.fAxisAttr.SetTitleSize (0.045);
      opt.fAxisY.fAxisAttr.SetTitleColor (1);
      opt.fAxisY.fAxisAttr.SetTitleFont (62);
      opt.fAxisY.fGrid = kTRUE;
      opt.fAxisY.fBothSides = kTRUE;
      opt.fAxisY.fCenterTitle = kTRUE;
   
      // legend
      opt.fLegend.fShow = kTRUE;
      opt.fLegend.fPlacement = kLegendTopRight;
      opt.fLegend.fXAdjust = 0.;
      opt.fLegend.fYAdjust = 0.;
      opt.fLegend.fSymbolStyle = kLegendSameAsTrace;
      opt.fLegend.fTextStyle = kLegendAutoText;
      opt.fLegend.fSize = 1.0;
      for (int i = 0; i < kMaxTraces; i++) {
         opt.fLegend.fText[i] = "";
      }
   
      // parameter
      opt.fParam.fShow = kTRUE;
      opt.fParam.fT0 = kTRUE;
      opt.fParam.fAvg = kTRUE;
      opt.fParam.fSpecial = kTRUE;
      opt.fParam.fStat = kTRUE; // hist (mito)
      opt.fParam.fUOBins = kTRUE; // hist (mito) 8/8/2002
      opt.fParam.fTimeFormatUTC = kTRUE;
      opt.fParam.fTextSize = 0.04;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGSaveRestoreSettingsDlg                                            //
//                                                                      //
// Dialog box for storing and restoring plot settings                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGSaveRestoreSettingsDlg : public TLGTransientFrame {
   protected:
      // Options to save/restore
      OptionAll_t*	fOpt;
      // Options storage array
      OptionAll_t**	fStorage;
      // Options storage array length
      Int_t		fStorageMax;
      // Store (true) or restore (false)
      Bool_t		fSave;
      // Return value
      Bool_t*		fRet;
   
      // Master frame
      TGCompositeFrame*	fF;
      // Button frame
      TGCompositeFrame*	fFB;
      // Selection frame left panel
      TGCompositeFrame*	fFS1;
      // Selection frame right panel
      TGCompositeFrame*	fFS2;
      // Name label
      TGLabel*		fNameLabel;
      // Name entry
      TLGTextEntry*	fName;
      // Name selection list box
      TGListBox*	fNameSel;
      // Save/restore button
      TGButton*		fSaveRestore;
      // Delete button
      TGButton*		fDelete;
      // Cancel button
      TGButton*		fCancel;
      /// Layout hints 
      TGLayoutHints*	fL1;
      /// Layout hints 
      TGLayoutHints*	fL2;
      /// Layout hints 
      TGLayoutHints*	fL3;
      /// Layout hints
      TGLayoutHints*	fL4;
      /// Layout hints 
      TGLayoutHints*	fL5;
      /// Layout hints 
      TGLayoutHints*	fL6;
   
      Int_t GetIndex (const char* name, TString* trim = 0);
   
   public:
      TLGSaveRestoreSettingsDlg (const TGWindow *p, const TGWindow *main,
                        OptionAll_t& opt, OptionAll_t* storage[], 
                        Int_t maxStorage, Bool_t save, Bool_t& ret);
      virtual ~TLGSaveRestoreSettingsDlg ();
      virtual void CloseWindow();
   
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };

//______________________________________________________________________________
   Int_t TLGSaveRestoreSettingsDlg::GetIndex (const char* n,
                     TString* trim){
      TString name = n;
      // trim spaces
      while ((name.Length() > 0) && 
            ((name[0] == ' ') || (name[0] == '\t'))) {
         name.Remove (0, 1);
      }
      while ((name.Length() > 0) && 
            ((name[name.Length()-1] == ' ') || 
            (name[name.Length()-1] == '\t'))) {
         name.Remove (name.Length()-1, 1);
      }
      if (trim != 0) {
         *trim = name;
      }
      // go through list
      for (int i = 0; i < fStorageMax; i++) {
         if ((fStorage[i] != 0) && 
            (fStorage[i]->fName.CompareTo (name, 
                              TString::kIgnoreCase) == 0)) {
            return i;
         }
      }
   
      // not found
      return -1;
   }

//______________________________________________________________________________
extern "C" 
   Int_t compareOpts (const void* opt1, const void* opt2)
   {
      if (*(OptionAll_t**)opt1 == 0) {
         return -1;
      }
      if (*(OptionAll_t**)opt2 == 0) {
         return 1;
      }
      return (*(OptionAll_t**)opt1)->fName.CompareTo 
         ((*(OptionAll_t**)opt2)->fName, TString::kIgnoreCase);
   }

//______________________________________________________________________________
   TLGSaveRestoreSettingsDlg::TLGSaveRestoreSettingsDlg 
   (const TGWindow *p, const TGWindow *main, OptionAll_t& opt, 
   OptionAll_t* storage[], Int_t maxStorage, Bool_t save, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fOpt (&opt), fStorage (storage),
   fStorageMax (maxStorage), fSave (save), fRet (&ret)
   {
      // layout hints
      fL1 = new TGLayoutHints (kLHintsLeft | kLHintsTop |
                           kLHintsExpandY, 2, 2, 2, 2);
      fL2 = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 2, 2, 2, 2);
      fL3 = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop | kLHintsExpandY, 2, 2, 2, 8);
      fL4 = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsBottom, 12, 2, 8, 8);
      fL5 = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 2, 2, 6, 2);
      fL6 = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 0, -10);      
      // setup frames
      fF = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF, fL1);
      fFS1 = new TGVerticalFrame (fF, 10, 10);
      fF->AddFrame (fFS1, fL1);
      fFS2 = new TGVerticalFrame (fF, 10, 10);
      fF->AddFrame (fFS2, fL1);
      fFB = new TGVerticalFrame (fF, 10, 10);
      fF->AddFrame (fFB, fL1);
   
      // create name selection
      fNameLabel = new TGLabel (fFS1, "Name:");
      fFS1->AddFrame (fNameLabel, fL5);
      fName = new TLGTextEntry (fFS2, "", 10);
      fFS2->AddFrame (fName, fL2);
      fNameSel = new TGListBox (fFS2, 11);
      fNameSel->Associate (this);
      fFS2->AddFrame (fNameSel, fL3);
      fNameSel->Resize (220, 200);
   
      // create buttons
      fCancel = new TGTextButton (fFB, "   Cancel   ", 0);
      fCancel->Associate (this);
      fFB->AddFrame (fCancel, fL4);
      fDelete = new TGTextButton (fFB, "   Delete   ", 3);
      fDelete->Associate (this);
      fFB->AddFrame (fDelete, fL4);
      fSaveRestore = new TGTextButton (fFB, fSave ? "    Store    " : 
                           "   Restore   ", fSave ? 1 : 2);
      fSaveRestore->Associate (this);
      fFB->AddFrame (fSaveRestore, fL4);
   
      // sort & fill in names
      ::qsort (fStorage, fStorageMax, sizeof (OptionAll_t*), compareOpts);
      for (int i = 0; i < fStorageMax; i++) {
         if (fStorage[i] != 0) {
            fNameSel->AddEntry (fStorage[i]->fName, i);
         }
      }
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
   
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates 
            (main->GetId(), GetParent()->GetId(),
            (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
            (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
            ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      TString name = 
         fSave ? "Store plot settings" : "Restore plot settings";
      SetWindowName (name);
      SetIconName (name);
      SetClassHints ("SaveRestoreSettingsDlg", "SaveRestoreSettingsDlg");
   
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGSaveRestoreSettingsDlg::~TLGSaveRestoreSettingsDlg ()
   {
      delete fCancel;
      delete fDelete;
      delete fSaveRestore;
      delete fNameSel;
      delete fName;
      delete fNameLabel;
      delete fFS2;
      delete fFS1;
      delete fFB;
      delete fF;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
   }

//______________________________________________________________________________
   void TLGSaveRestoreSettingsDlg::CloseWindow()
   {
      if (fRet) *fRet = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGSaveRestoreSettingsDlg::ProcessMessage (Long_t msg, 
                     Long_t parm1, Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // save
            case 1:
               {
                  TString trim;
                  Int_t id = GetIndex (fName->GetText(), &trim);
                  // if exists, ask for permission to overwrite
                  if (id >= 0) {
                     Int_t ret;
                     TString msg = TString ("Plot setting ") + 
                        fName->GetText() + " exists.\nOverwrite it?";
                     new TGMsgBox (gClient->GetRoot(), this, "Replace", msg,
                                  kMBIconQuestion, kMBYes | kMBNo, &ret);
                     if (ret != kMBYes) {
                        break;
                     }
                     delete fStorage[id];
                     fStorage[id] = 0;
                  }
                  // valid name?
                  else if (trim.Length() == 0) {
                     TString msg = TString ("Please specify a valid name.");
                     new TGMsgBox(fClient->GetRoot(), this, "Error", 
                                 msg, kMBIconStop, kMBOk);
                     break;
                  }
                  // look for free slot
                  else {
                     // get first free slot
                     id = -1;
                     for (int i = 0; i < fStorageMax; i++) {
                        if (fStorage[i] == 0) {
                           id = i;
                           break;
                        }
                     }
                     if (id < 0) {
                        TString msg = TString ("No free slot. "
                                             "Delete an old setting first.");
                        new TGMsgBox(fClient->GetRoot(), this, "Error", 
                                    msg, kMBIconStop, kMBOk);
                        break;
                     }
                  }
                  // now save it
                  fStorage[id] = new OptionAll_t;
                  if (fStorage[id] != 0) {
                     *(fStorage[id]) = *fOpt;
                     fStorage[id]->fName = trim;
                     if (fRet) *fRet = kTRUE;
                     DeleteWindow();
                  }
                  break;
               }
            // restore
            case 2: 
               {
                  Int_t id = GetIndex (fName->GetText());
                  if (id < 0) {
                     TString msg = TString ("Please select a valid name.");
                     new TGMsgBox(fClient->GetRoot(), this, "Error", 
                                 msg, kMBIconStop, kMBOk);
                  }
                  else {
                     *fOpt = *(fStorage[id]);
                     if (fRet) *fRet = kTRUE;
                     DeleteWindow();
                  }
                  break;
               }
            // delete
            case 3: 
               {
                  Int_t id = GetIndex (fName->GetText());
                  if (id < 0) {
                     TString msg = TString ("Please select a valid name.");
                     new TGMsgBox(fClient->GetRoot(), this, "Error", 
                                 msg, kMBIconStop, kMBOk);
                  }
                  else {
                     delete fStorage[id];
                     fStorage[id] = 0;
                     fNameSel->RemoveEntry (id);
                     id++;
                     while (id < fStorageMax) {
                        if (fStorage[id] != 0) {
                           break;
                        }
                     }
                     if (id < fStorageMax) {
                        fName->SetText (fStorage[id]->fName);
                        fNameSel->Select (id);
                     }
                     else {
                        fName->SetText ("");
                     }
                  }
                  break;
               }
            // cancel
            case 0:
               {
                  if (fRet) *fRet = kFALSE;
                  DeleteWindow();
                  break;
               }
         }
      }
      // List box
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_LISTBOX)) {
         if ((parm2 >= 0) && (parm2 < fStorageMax) &&
            (fStorage[parm2] != 0)) {
            fName->SetText (fStorage[parm2]->fName);
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionTab                                                         //
//                                                                      //
// Graphics option: Tab widget managing all options                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionTab::TLGOptionTab (const TGWindow* p, Int_t id, 
                     OptionAll_t* optvals, const PlotMap& plotlist,
                     OptionAll_t** list, Int_t max, TList* xunits, 
                     TList* yunits)
   : TLGMultiTab (p, kOptionWidth, kOptionHeight, 2), 
   fPlotList (&plotlist), fStoreOptions (list), fStoreOptionsMax (max),
   fXUnitList (xunits), fYUnitList (yunits)
   {
      fWidgetId = id;
      SetOptionValues (optvals);
   
      fL = new TGLayoutHints (kLHintsLeft | kLHintsTop, 2, 2, 2, 2);
   
      // create option tabs
      // traces
      fTab[0] = AddTab (kGOptTracesName);
      fTabTraces = new TLGOptionTraces (fTab[0], kGOptTracesID, 
                           &fOptionValues->fTraces, *fPlotList);
      fTabTraces->Associate (this);
      fTab[0]->AddFrame (fTabTraces, fL);
      // range
      fTab[1] = AddTab (kGOptRangeName);
      fTabRange = new TLGOptionRange (fTab[1], kGOptRangeID, 
                           &fOptionValues->fRange, *fPlotList);
      fTabRange->Associate (this);
      fTab[1]->AddFrame (fTabRange, fL);
      // units
      fTab[2] = AddTab (kGOptUnitsName);
      fTabUnits = new TLGOptionUnits (fTab[2], kGOptUnitsID, 
                           &fOptionValues->fUnits, this, 
                           fXUnitList, fYUnitList);
      fTabUnits->Associate (this);
      fTab[2]->AddFrame (fTabUnits, fL);
      // cursor
      fTab[3] = AddTab (kGOptCursorName);
      fTabCursor = new TLGOptionCursor (fTab[3], kGOptCursorID, 
                           &fOptionValues->fCursor, *fPlotList);
      fTabCursor->Associate (this);
      fTab[3]->AddFrame (fTabCursor, fL);
      // config
      fTab[4] = AddTab (kGOptConfigName);
      fTabConfig = new TLGOptionConfig (fTab[4], kGOptConfigID, 
                           &fOptionValues->fConfig, this);
      fTabConfig->Associate (this);
      fTab[4]->AddFrame (fTabConfig, fL);
      // style
      fTab[5] = AddTab (kGOptStyleName);
      fTabStyle = new TLGOptionStyle (fTab[5], kGOptStyleID, 
                           &fOptionValues->fStyle);
      fTabStyle->Associate (this);
      fTab[5]->AddFrame (fTabStyle, fL);
      // x axis
      fTab[6] = AddTab (kGOptAxisXName);
      fTabAxisX = new TLGOptionAxis (fTab[6], kGOptAxisXID, 
                           &fOptionValues->fAxisX, kTRUE);
      fTabAxisX->Associate (this);
      fTab[6]->AddFrame (fTabAxisX, fL);
      // y axis
      fTab[7] = AddTab (kGOptAxisYName);
      fTabAxisY = new TLGOptionAxis (fTab[7], kGOptAxisYID, 
                           &fOptionValues->fAxisY, kFALSE);
      fTabAxisY->Associate (this);
      fTab[7]->AddFrame (fTabAxisY, fL);
      // legend
      fTab[8] = AddTab (kGOptLegendName);
      fTabLegend = new TLGOptionLegend (fTab[8], kGOptLegendID, 
                           &fOptionValues->fLegend);
      fTabLegend->Associate (this);
      fTab[8]->AddFrame (fTabLegend, fL);
      // parameter
      fTab[9] = AddTab (kGOptParamName);
      fTabParam = new TLGOptionParam (fTab[9], kGOptParamID, 
                           &fOptionValues->fParam);
      fTabParam->Associate (this);
      fTab[9]->AddFrame (fTabParam, fL);

   }

//______________________________________________________________________________
   TLGOptionTab::~TLGOptionTab () 
   {
      delete fTabTraces;
      delete fTabRange;
      delete fTabUnits;
      delete fTabCursor;
      delete fTabConfig;
      delete fTabStyle;
      delete fTabAxisX;
      delete fTabAxisY;
      delete fTabLegend;
      delete fTabParam;
      delete fL;
   }

//______________________________________________________________________________
   Bool_t TLGOptionTab::CalibrationDialog ()
   {
      const Long_t cal = MK_MSG ((EWidgetMessageTypes) kC_OPTION, 
                           (EWidgetMessageTypes) kCM_CAL);
      SendMessage (fMsgWindow, cal, fWidgetId, 0);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGOptionTab::SaveRestoreDialog (Bool_t save)
   {
      Bool_t			ret;
   
      if ((fOptionValues == 0) || (fStoreOptions == 0) ||
         (fStoreOptionsMax <= 0)) {
         TString msg = TString ("Store/restore function not supported.");
         new TGMsgBox (fClient->GetRoot(), this, "Error", 
                      msg, kMBIconStop, kMBOk);
         return kFALSE;
      }
      new TLGSaveRestoreSettingsDlg (fClient->GetRoot(), this,
                           *fOptionValues, fStoreOptions, 
                           fStoreOptionsMax, save, ret);
      if (!save && ret) {
         // update options
         UpdateOptions();
         const Long_t update = MK_MSG ((EWidgetMessageTypes) kC_OPTION, 
                              (EWidgetMessageTypes) kCM_OPTCHANGED);
         SendMessage (fMsgWindow, update, 1, 0);
         const Long_t cupdate = MK_MSG ((EWidgetMessageTypes) kC_OPTION, 
                              (EWidgetMessageTypes) kCM_OPTCURSOR);
         SendMessage (fMsgWindow, cupdate, 1, 0);
      }
      return ret;
   }

//______________________________________________________________________________
   void TLGOptionTab::UpdateAxis (Int_t a)
   {
      if (!fOptionValues->fConfig.fAutoAxes) {
         return;
      }
      OptionAxis_t* axis = (a == 0) ? 
         &fOptionValues->fAxisX : &fOptionValues->fAxisY;
      EYRangeType conv = (a == 0) ? 
         (EYRangeType) fOptionValues->fUnits.fXValues : 
         fOptionValues->fUnits.fYValues;
      TString* plottype = &fOptionValues->fTraces.fGraphType;
   
      // update X axis
      if ((axis->fAxisTitle == "Time") ||
         (axis->fAxisTitle == "Frequency")) {
         if (*plottype == kPTTimeSeries) {
            axis->fAxisTitle = "Time";
         }
         else if ((*plottype == kPTFrequencySeries) ||
                 (*plottype == kPTPowerSpectrum) ||
                 (*plottype == kPTCoherence) ||
                 (*plottype == kPTCrossCorrelation) ||
                 (*plottype == kPTTransferFunction) ||
                 (*plottype == kPTCoherenceFunction) ||
                 (*plottype == kPTTransferCoefficients) ||
                 (*plottype == kPTCoherenceCoefficients) ||
                 (*plottype == kPTHarmonicCoefficients) ||
                 (*plottype == kPTIntermodulationCoefficients)) {
            axis->fAxisTitle = "Frequency";
         }
      }
      // update Y axis
      else if ((axis->fAxisTitle == "Magnitude") ||
              (axis->fAxisTitle == "Signal") ||
              (axis->fAxisTitle == "Phase") ||
              (axis->fAxisTitle == "Coherence") ||
              (axis->fAxisTitle == "Real") ||
              (axis->fAxisTitle == "Imaginary") ||
              (axis->fAxisTitle == "Real/Imaginary")) {
         if (conv == kUnitReal) {
            axis->fAxisTitle = "Real";
         }
         else if (conv == kUnitImaginary) {
            axis->fAxisTitle = "Imaginary";
         }
         else if (conv == kUnitRealImaginary) {
            axis->fAxisTitle = "Real/Imaginary";
         }
         else if ((conv == kUnitPhaseDeg) ||
                 (conv == kUnitPhaseRad) ||
                 (conv == kUnitPhaseDegCont) ||
                 (conv == kUnitPhaseRadCont)) {
            axis->fAxisTitle = "Phase";
         }
         else if (conv == kUnitdBMagnitude) {
            axis->fAxisTitle = "Magnitude";
         }
         else if (conv == kUnitMagnitude) {
            if (*plottype == kPTTimeSeries) {
               axis->fAxisTitle = "Signal";
            }
            else if ((*plottype == kPTCoherence) ||
                    (*plottype == kPTCoherenceFunction) ||
                    (*plottype == kPTCoherenceCoefficients)) {
               axis->fAxisTitle = "Coherence";
            }
            else {
               axis->fAxisTitle = "Magnitude";
            }
         }
      }
   }

//______________________________________________________________________________
   void TLGOptionTab::UpdateOptions ()
   {
      //cerr << "TLGOptionTab::UpdateOptions()" << endl ;
      UpdateAxis (0);
      UpdateAxis (1);
      if (fTabTraces) fTabTraces->UpdateOptions();
      if (fTabRange) fTabRange->UpdateOptions();
      if (fTabUnits) fTabUnits->UpdateOptions();
      if (fTabCursor) fTabCursor->UpdateOptions();
      if (fTabConfig) fTabConfig->UpdateOptions();
      if (fTabStyle) fTabStyle->UpdateOptions();
      if (fTabAxisX) fTabAxisX->UpdateOptions();
      if (fTabAxisY) fTabAxisY->UpdateOptions();
      if (fTabLegend) fTabLegend->UpdateOptions();
      if (fTabParam) fTabParam->UpdateOptions();
      //cerr << "TLGOptionTab::UpdateOptions() end" << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionTab::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_OPTION:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_OPTCHANGED:
                     {
                        SendMessage (fMsgWindow, msg, 0, 0);
                        break;
                     }
                  case kCM_OPTCURSOR:
                     {
                        SendMessage (fMsgWindow, msg, parm1, parm2);
                        break;
                     }
                  case kCM_OPTCURSORNEW:
                     {
                        if (fTabCursor) {
                           fTabCursor->UpdateOptions();
                        }
                        break;
                     }
               }
               break;
            }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGOptionDialog                                                      //
//                                                                      //
// Option dialog box                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGOptionDialog::TLGOptionDialog (const TGWindow *p, 
                     const TGWindow *main,
                     const char* name, OptionAll_t* optvals, 
                     const PlotMap& plotlist,
                     OptionAll_t** list, Int_t max,
                     TList* xunits, TList* yunits)
   : TLGTransientFrame (p, main, 10, 10), fModified (kFALSE),
   fPlotList (&plotlist), fOptionValues (optvals)
   {
      if (!fOptionValues) {
         delete this;
      }
      fOptionTemp = *fOptionValues;
   
      // set dialog box title
      SetWindowName (name);
      SetIconName (name);
      SetClassHints ("TLGOptionDialog", "TLGOptionDialog");
   
      // create child windows
      fL1 = new TGLayoutHints (kLHintsBottom | kLHintsExpandX, 2, 2, 2, 2);
      fL2 = new TGLayoutHints (kLHintsLeft | kLHintsTop, 2, 2, 2, 2);
   
      fOptionTabs = new TLGOptionTab (this, kGOptTabID, &fOptionTemp, 
                           *fPlotList, list, max, xunits, yunits);
      fOptionTabs->Associate (this);
      AddFrame (fOptionTabs, fL2);
   
      fButtonFrame = new TGCompositeFrame 
         (this, 100, 20, kHorizontalFrame | kSunkenFrame);
      fOkButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("   &Ok   "), 1);
      fOkButton->Associate (this);
      fButtonFrame->AddFrame (fOkButton, fL1);
      fUpdateButton = new TGTextButton (fButtonFrame, 
                           new TGHotString (" &Update "), 2);
      fUpdateButton->Associate (this);
      fUpdateButton->SetState (kButtonDisabled);
      fButtonFrame->AddFrame (fUpdateButton, fL1);
      fCancelButton = new TGTextButton (fButtonFrame, 
                           new TGHotString (" &Cancel "), 0);
      fCancelButton->Associate (this);
      fButtonFrame->AddFrame (fCancelButton, fL1);
      AddFrame (fButtonFrame, fL1);
   
      // resize & move to center
      MapSubwindows ();
      Resize (GetDefaultSize());
      Window_t wdum;
      int ax;
      int ay;
      gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                           ((TGFrame*)main)->GetWidth() - (fWidth >> 1), 
                           (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                           ax, ay, wdum);
      Move (ax, ay);
      SetWMPosition (ax, ay);
      SetMWMHints (kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
      MapWindow();
   }

//______________________________________________________________________________
   TLGOptionDialog::~TLGOptionDialog ()
   {
      delete fOkButton;
      delete fUpdateButton;
      delete fCancelButton;
      delete fButtonFrame;
      delete fOptionTabs;
   
      delete fL1;
      delete fL2;
   }

//______________________________________________________________________________
   void TLGOptionDialog::CloseWindow()
   {
      SendMessage (fMain, MK_MSG ((EWidgetMessageTypes)kC_OPTION, 
                                 (EWidgetMessageTypes)kCM_OPTCLOSE), fId, 0);
      DeleteWindow();
   }

//______________________________________________________________________________
   void TLGOptionDialog::UpdateOptions ()
   
   {
      //cerr << "TLGOptionDialog::UpdateOptions() " << endl ;
      if (fOptionValues) {
         fOptionTemp = *fOptionValues;
      }
      if (fOptionTabs) fOptionTabs->UpdateOptions(); 
      //cerr << "TLGOptionDialog::UpdateOptions() end" << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGOptionDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      const Long_t update = MK_MSG ((EWidgetMessageTypes) kC_OPTION, 
                           (EWidgetMessageTypes) kCM_OPTUPDATE);
      const Long_t close = MK_MSG ((EWidgetMessageTypes)kC_OPTION, 
                           (EWidgetMessageTypes)kCM_OPTCLOSE);
   
      //cerr << "TLGOptionDialog::ProcessMessage" << endl ;
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               if (GET_SUBMSG (msg) == kCM_BUTTON) {
                  switch (parm1) {
                     // ok & update
                     case 1:
                     case 2:
                        {
			   //cerr << "TLGOptionDialog::ProcessMessage ok & update" << endl ;
                           if (parm1 == 1) {
                              SendMessage (fMain, close, fId, 0);
                           }
                           gVirtualX->SetInputFocus (fId);
                           if (fModified && fOptionValues) {
                              *fOptionValues = fOptionTemp;
                              SendMessage (fMain, update, 0, 0);
                              fModified = kFALSE;
                              fUpdateButton->SetState (kButtonDisabled);
                           }
                           if (parm1 == 1) {
                              DeleteWindow();
                           }
                           break;
                        }
                     // cancel
                     case 0:
                        {
			   //cerr << "TLGOptionDialog::ProcessMessage cancel" << endl ;
                           SendMessage (fMain, close, fId, 0);
                           DeleteWindow();
                           break;
                        }
                  }
               }
               break;
            }
         case kC_OPTION:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_OPTCLOSE:
                     {
			//cerr << "TLGOptionDialog::ProcessMessage kCM_OPTCLOSE" << endl ;
                        DeleteWindow();
                        break;
                     }
                  case kCM_OPTCHANGED:
                     {
			//cerr << "TLGOptionDialog::ProcessMessage kCM_OPTCHANGED" << endl ;
                        // forced update by restore
                        if (parm1 == 1) {
                           if (fOptionValues) {
                              *fOptionValues = fOptionTemp;
                           }
                           SendMessage (fMain, msg, 1, 0);
                           fModified = kFALSE;
                           fUpdateButton->SetState (kButtonDisabled);
                        }
                        // normal option change: only set modified flag
                        else {
                           fModified = kTRUE;
                           fUpdateButton->SetState (kButtonUp);
                        }
                        break;
                     }
                  case kCM_OPTCURSOR:
                     {
			//cerr << "TLGOptionDialog::ProcessMessage kCM_OPTCURSOR" << endl ;
                        fOptionValues->fCursor = fOptionTemp.fCursor;
                        SendMessage (fMain, msg, fId, parm2);
                        break;
                     }
                  case kCM_OPTCURSORNEW:
                     {
			//cerr << "TLGOptionDialog::ProcessMessage kCM_OPTCURSORNEW" << endl ;
                        fOptionTemp.fCursor = fOptionValues->fCursor;
                        if (fOptionTabs) {
                           SendMessage (fOptionTabs, msg, parm1, parm2);
                        }
                        break;
                     }
                  case kCM_CAL:
                     {
			//cerr << "TLGOptionDialog::ProcessMessage kCM_CAL" << endl ;
                        SendMessage (fMain, msg, fId, parm1);
                        break;
                     }
               }
               break;
            }
      }
      return kTRUE;
   }

}
