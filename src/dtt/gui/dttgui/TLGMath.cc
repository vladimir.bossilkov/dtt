/* Version $Id: TLGMath.cc 7757 2016-10-07 16:25:19Z james.batch@LIGO.ORG $ */

#include "TLGMath.hh"
#include "TLGEntry.hh"
#include "PlotSet.hh"
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <iostream>
#include <TVirtualX.h>


namespace ligogui {
   using namespace std;

   static const int my_debug = 0 ;

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Set default reference traces list                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void SetDefaultReferenceTraces (ReferenceTraceList_t& ref)
   {
      for (int i = 0; i < kMaxReferenceTraces; i++) {
         ref.fTraces[i].fValid = kFALSE;
         ref.fTraces[i].fModified = kRefTraceModNo;
         ref.fTraces[i].fGraph = "";
         ref.fTraces[i].fAChn = "";
         ref.fTraces[i].fBChn = "";
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Set default math table                                               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void SetDefaultMathTable (MathTable_t& math)
   {
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Show reference trace dialog box                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t ReferenceTraceDlg (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, ReferenceTraceList_t& ref,
                     calibration::Table* caltable)
   {
      Bool_t ret;
      new TLGReferenceDialog (p, main, pset, ref, ret, caltable);
      if (ret) {
         pset.Update();
      }
      return ret;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Show math table dialog box         	                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t MathEditor (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, MathTable_t& math)
   {
      Bool_t ret = kFALSE;
      // new TLGMathEditor (p, main, pset, math, ret);
      TString msg = TString ("Math feature not yet implemented.");
      new TGMsgBox (p, main, "Warning", msg, kMBIconStop, kMBOk);
      if (ret) {
         pset.Update();
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Reference trace dialog      	                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGReferenceDialog::TLGReferenceDialog (const TGWindow *p, 
                     const TGWindow *main,
                     PlotSet& pset, ReferenceTraceList_t& ref,
                     Bool_t& ret, calibration::Table* caltable)
   : TLGTransientFrame (p, main, 10, 10, kMainFrame | kHorizontalFrame), 
   fPlotList (&pset), fCal (caltable), fRef (&ref), fOk (&ret)
   {
      if (my_debug) cerr << "TLGReferenceDialog::TLGReferenceDialog()" << endl ;
      fTemp = *fRef;
      for (int i = 0; i < kMaxReferenceTraces; i++) {
         fTemp.fTraces[i].fModified = kRefTraceModNo;
      }
   
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, 4, 4, 4);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsExpandY | kLHintsTop, 0, 0, 0, 0);
      fL[2] = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 4, 4, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsBottom, 0, 0, 0, 4);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsBottom, 8, 4, 4, 4);
      fL[5] = new TGLayoutHints (kLHintsLeft |
                           kLHintsExpandY | kLHintsTop, 0, 0, 0, 0);
      // group frames
      fFRef = new TGVerticalFrame (this, 10, 10);
      AddFrame (fFRef, fL[5]);
      fFTrace = new TGVerticalFrame (this, 10, 10);
      AddFrame (fFTrace, fL[1]);
      fFButton = new TGVerticalFrame (this, 10, 10);
      AddFrame (fFButton, fL[5]);
      // reference trace selection
      fRefSel = new TGListBox (fFRef, kRefTraceRef);
      fRefSel->Associate (this);
      fRefSel->Resize (160, 200);
      fFRef->AddFrame (fRefSel, fL[0]);
      // trace selection
      fF3 = new TGHorizontalFrame (fFTrace, 10, 10);
      fFTrace->AddFrame (fF3, fL[3]);
      fF2 = new TGHorizontalFrame (fFTrace, 10, 10);
      fFTrace->AddFrame (fF2, fL[3]);
      fF1 = new TGHorizontalFrame (fFTrace, 10, 10);
      fFTrace->AddFrame (fF1, fL[3]);
      fGraphLabel = new TGLabel (fF1, "  Graph: ");
      fF1->AddFrame (fGraphLabel, fL[0]);
      fALabel = new TGLabel (fF2, "  A: ");
      fF2->AddFrame (fALabel, fL[0]);
      fBLabel = new TGLabel (fF3, "  B: ");
      fF3->AddFrame (fBLabel, fL[0]);
      fGraph = new TGComboBox (fF1, kRefTraceGraph);
      fGraph->Associate (this);
      fGraph->Resize (420, 23);
      fF1->AddFrame (fGraph, fL[2]);
      fAchn = new TGComboBox (fF2, kRefTraceAchn);
      fAchn->Associate (this);
      fAchn->Resize (420, 23);
      fF2->AddFrame (fAchn, fL[2]);
      fBchn = new TGComboBox (fF3, kRefTraceBchn);
      fBchn->Associate (this);
      fBchn->Resize (420, 23);
      fF3->AddFrame (fBchn, fL[2]);
      // setup buttons
      fOkButton = new TGTextButton (fFButton, 
                           new TGHotString ("       &Ok       "), 
                           kRefTraceOk);
      fOkButton->Associate (this);
      fOkButton->SetToolTipText ("Quit reference trace dialog");
      fFButton->AddFrame (fOkButton, fL[4]);
      fCancelButton = new TGTextButton (fFButton, 
                           new TGHotString ("     &Cancel     "), 
                           kRefTraceCancel);
      fCancelButton->Associate (this);
      fCancelButton->SetToolTipText ("Quit reference trace dialog without changes");
      fFButton->AddFrame (fCancelButton, fL[4]);
      fClearButton = new TGTextButton (fFButton, 
                           new TGHotString ("    &Delete    "), 
                           kRefTraceClear);
      fClearButton->Associate (this);
      fClearButton->SetToolTipText ("Delete a reference trace from the list");
      fFButton->AddFrame (fClearButton, fL[4]);
      fUpdateAllButton = new TGTextButton (fFButton, 
                           new TGHotString ("    U&pdate All    "), 
                           kRefTraceUpdateAll);
      fUpdateAllButton->SetToolTipText ("Update all reference traces with new data");
      fUpdateAllButton->Associate (this);
      fFButton->AddFrame (fUpdateAllButton, fL[4]);
      fUpdateButton = new TGTextButton (fFButton, 
                           new TGHotString ("     &Update     "), 
                           kRefTraceUpdate);
      fUpdateButton->Associate (this);
      fUpdateButton->SetToolTipText ("Update a reference trace with new data");
      fFButton->AddFrame (fUpdateButton, fL[4]);
      fNewButton = new TGTextButton (fFButton, 
                           new TGHotString ("      &Add      "), 
                           kRefTraceNew);
      fNewButton->Associate (this);
      fNewButton->SetToolTipText ("Add a reference trace to the list");
      fFButton->AddFrame (fNewButton, fL[4]);
   
      // initialize parameters
      fCurRef = -1;
      BuildRefList();
      BuildPlotType (0);
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      SetWindowName ("Reference traces");
      SetIconName ("Reference traces");
      SetClassHints ("RefTracesDlg", "RefTracesDlg");
   
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGReferenceDialog::~TLGReferenceDialog ()
   {
      if (my_debug) cerr << "TLGReferenceDialog::~TLGReferenceDialog()" << endl ;
      delete fCancelButton;
      delete fOkButton;
      delete fClearButton;
      delete fUpdateAllButton;
      delete fUpdateButton;
      delete fNewButton;
      delete fBchn;
      delete fAchn;
      delete fGraph;
      delete fBLabel;
      delete fALabel;
      delete fGraphLabel;
      delete fRefSel;
      delete fF1;
      delete fF2;
      delete fF3;
      delete fFButton;
      delete fFTrace;
      delete fFRef;
      for (int i = 0; i < 6; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGReferenceDialog::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      if (my_debug) cerr << "  TLGReferenceDialog::CloseWindow() - Calling DeleteWindow()" << endl ;
      DeleteWindow();
   }

//______________________________________________________________________________
// This function maintains the TGListBox that shows the reference traces on the
// left side of the dialog box.  It first removes all entries, then adds entries
// that are valid from the array of ReferenceTrace_t structures in fTemp.
   void TLGReferenceDialog::BuildRefList ()
   {
      fRefSel->RemoveEntries (0, kMaxReferenceTraces);
      for (int i = 0; i < kMaxReferenceTraces; i++) {
         if (fTemp.fTraces[i].fValid) {
            char buf[32];
            sprintf (buf, "  Ref %i", i); 
            switch (fTemp.fTraces[i].fModified) {
               case kRefTraceModNo:
               default:
                  break;
               case kRefTraceModAdd: 
                  buf[0] = '+';
                  break;
               case kRefTraceModDelete: 
                  buf[0] = '-';
                  break;
               case kRefTraceModUpdate: 
                  buf[0] = '*';
                  break;
            }
            fRefSel->AddEntry (buf, i);
         }
      }
      fRefSel->MapSubwindows();
      fRefSel->Layout();
      if (fCurRef >= 0) {
         fRefSel->Select (fCurRef);
      }
   }

//______________________________________________________________________________
   void TLGReferenceDialog::BuildPlotType (Int_t level)
   {
      // rebuild graph selection
      if (level <= 0) {
         // build a new graph selection list
         fGraph->RemoveEntries (0, 1000);
         const PlotListLink* g = fPlotList->GetPlotMap().Get();
         if (g != 0) {
            g = g->Child();
         }
         const PlotListLink* gsave = g;
         if (g != 0) {
            while (g != 0) {
               fGraph->AddEntry (g->GetName(), 
                                fPlotList->GetPlotMap().GetId (g->GetName()));
               g = g->Next();
            }
            // set selected graph
            Int_t id = fPlotList->GetPlotMap().GetId (fCurGraph);
            if (id >= 0) {
               fGraph->Select (id);
            }
            else {
               fGraph->Select (0);
               fCurGraph = gsave->GetName();
            }
         }
         else {
            // disable graph selection combobox
            fGraph->SetTopEntry 
               (new TGTextLBEntry (fGraph, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fGraph->MapSubwindows();
            fCurGraph = "";
         }
      
      }
      // rebuild A channel selections
      if (level <= 1) {
         // build a new A channel selection list
         fAchn->RemoveEntries (0, 1000);
         const PlotListLink* a = fPlotList->GetPlotMap().Get (fCurGraph);
         if (a != 0) {
            a = a->Child();
         }
         Int_t idsave = -1;
         const PlotListLink* asave = 0;
         while (a != 0) {
            if (strstr (a->GetName(), "(REF") == 0) {
               Int_t id = fPlotList->GetPlotMap().GetId (
                                    fCurGraph, a->GetName());
               fAchn->AddEntry (a->GetName(), id);
               if (idsave == -1) {
                  idsave = id;
                  asave = a;
               }
            }
            a = a->Next();
         }
         if (idsave == -1) {
            // disable A channel selection combobox
            fAchn->SetTopEntry 
               (new TGTextLBEntry (fAchn, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fAchn->MapSubwindows();
            fCurAchn = "";
         }
         else {
            // set selected graph
            Int_t id = fPlotList->GetPlotMap().GetId (fCurGraph, fCurAchn);
            if ((id >= 0) && (strstr (fCurAchn, "(REF") == 0)) {
               fAchn->Select (id);
            }
            else {
               fAchn->Select (idsave);
               fCurAchn = asave->GetName();
            }
         }
      }
      // rebuild B channel selection
      if (level <= 2) {
         // build a new B channel selection list
         fBchn->RemoveEntries (0, 1000);
         const PlotListLink* b = fPlotList->GetPlotMap().Get (fCurGraph, fCurAchn);
         if (b != 0) {
            b = b->Child();
         }
         Int_t idsave = -1;
         const PlotListLink* bsave = 0;
        // setup B channel list
         while (b != 0) {
            if (strstr (b->GetName(), "(REF") == 0) {
               Int_t id = fPlotList->GetPlotMap().GetId (
                                    fCurGraph, fCurAchn, b->GetName());
               fBchn->AddEntry (b->GetName(), id);
               if (idsave == -1) {
                  idsave = id;
                  bsave = b;
               }
            }
            b = b->Next();
         }
         if (idsave == -1) {
            // disable B selection combobox
            fBchn->SetTopEntry 
               (new TGTextLBEntry (fBchn, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fBchn->MapSubwindows();
            fCurBchn = "";
         }
         else {
            // set selected graph
            Int_t id = fPlotList->GetPlotMap().GetId 
               (fCurGraph, fCurAchn, fCurBchn);
            if ((id >= 0) && (strstr (fCurBchn, "(REF") == 0)) {
               fBchn->Select (id);
            }
            else {
               fBchn->Select (idsave);
               fCurBchn = bsave->GetName();
            }
         }
      }
   }

//______________________________________________________________________________
   TString RefStr (const TString& s, Int_t i) 
   {
      if (s.Length() > 0) {
         char buf[32];
         sprintf (buf, "(REF%i)", i);
         return TString (s + buf);
      }
      else {
         return s;
      }
   }

//______________________________________________________________________________
   Bool_t TLGReferenceDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Ok button
            case kRefTraceOk:
               {
                  Bool_t ret = kFALSE;
		  // Look at all the traces in fTemp.
                  for (int i = 0; i < kMaxReferenceTraces; i++) {
                     // update
		     // If the trace exists in fRef and fTemp, and the fTemp
		     // trace has been updated, remove the fRef trace and replace
		     // it with the fTemp trace.
                     if (fTemp.fTraces[i].fValid &&
                        (fRef->fTraces[i].fValid) &&
                        (fTemp.fTraces[i].fModified == kRefTraceModUpdate)) {
                        const PlotDescriptor* pd = 
                           fPlotList->Get (fRef->fTraces[i].fGraph,
                                          RefStr (fRef->fTraces[i].fAChn, i),
                                          RefStr (fRef->fTraces[i].fBChn, i));
                        fPlotList->Remove (pd);
                        fRef->fTraces[i] = fTemp.fTraces[i];
                        pd = fPlotList->Get (fRef->fTraces[i].fGraph,
                                            fRef->fTraces[i].fAChn,
                                            fRef->fTraces[i].fBChn);
                        if(nullptr == pd) {
                          ret = kFALSE;
                          continue;
                        }
                        PlotDescriptor* rd = 
                           pd->Clone (fRef->fTraces[i].fGraph,
                                     RefStr (fRef->fTraces[i].fAChn, i),
                                     RefStr (fRef->fTraces[i].fBChn, i), fCal);
                        if (rd != 0) {
                           rd->SetDirty();
                           rd->SetPersistent();
                           fPlotList->Add (rd);
                        }
                        fRef->fTraces[i].fModified = kRefTraceModUpdate;
                        ret = kTRUE; 
                     }
                     // add
		     // If the trace has been added in fTemp, replace any trace
		     // in fRef with the one in fTemp.
                     else if (fTemp.fTraces[i].fValid &&
                             (fTemp.fTraces[i].fModified == kRefTraceModAdd)) {
                        if (fRef->fTraces[i].fValid) {
                           const PlotDescriptor* pd = 
                              fPlotList->Get(fRef->fTraces[i].fGraph,
                                            RefStr (fRef->fTraces[i].fAChn, i),
                                            RefStr (fRef->fTraces[i].fBChn, i));
                           fPlotList->Remove (pd);
                        }
                        fRef->fTraces[i] = fTemp.fTraces[i];
                        const PlotDescriptor* pd = 
                           fPlotList->Get (fRef->fTraces[i].fGraph,
                                          fRef->fTraces[i].fAChn,
                                          fRef->fTraces[i].fBChn);

                        auto data_desc = dynamic_cast<const DataDescriptor *>(pd->GetData());
                        if(NULL != data_desc) {
                          printf("original data descriptor dx=%f\n",
                                 data_desc->GetDX());
                        }
                        else {
                          printf("original data descriptor was null\n");
                        }

                        PlotDescriptor* rd = 
                           pd->Clone (fRef->fTraces[i].fGraph,
                                     RefStr (fRef->fTraces[i].fAChn, i),
                                     RefStr (fRef->fTraces[i].fBChn, i), fCal);

                        auto data_desc2 = dynamic_cast<const DataDescriptor *>(rd->GetData());
                        if(NULL != data_desc2) {
                          printf("cloned data descriptor dx=%0.15f\n", data_desc2->GetDX());
                        }
                        else {
                          printf("cloned data descriptor was null\n");
                        }
                        if (rd != 0) {
                           rd->SetDirty();
                           rd->SetPersistent();
                           fPlotList->Add (rd);
                        }
                        fRef->fTraces[i].fValid = kTRUE;
                        fRef->fTraces[i].fModified = kRefTraceModAdd;
                        ret = kTRUE;
                     }
                     // delete
		     // If the trace doesn't exist in fTemp but does exist in
		     // fRef, remove the trace in fRef.
                     else if (!fTemp.fTraces[i].fValid &&
                             fRef->fTraces[i].fValid) {
                        const PlotDescriptor* pd = 
                           fPlotList->Get(fRef->fTraces[i].fGraph,
                                         RefStr (fRef->fTraces[i].fAChn, i),
                                         RefStr (fRef->fTraces[i].fBChn, i));
                        if (pd) {
                           fPlotList->Remove (pd);
                        }
                        fRef->fTraces[i].fValid = kFALSE;
                        fRef->fTraces[i].fModified = kRefTraceModDelete;
                        ret = kTRUE; 
                     }
                  }
               
                  // set return value and quit
                  if (fOk) *fOk = ret;
		  if (my_debug) cerr << "  TLGReferenceDialog::ProcessMessage(OK) - calling DeleteWindow()" << endl ;
                  DeleteWindow();
                  break;
               }
            // Cancel button
            case kRefTraceCancel:
               {
                  if (fOk) *fOk = kFALSE;
		  if (my_debug) cerr << "  TLGReferenceDialog::ProcessMessage(Cancel) - calling DeleteWindow()" << endl ;
                  DeleteWindow();
                  break;
               }
            // Add button
            case kRefTraceNew:
               {
                  if ((fCurGraph == "") || (fCurAchn == "")) {
                     TString msg = TString ("Select graph and channels first.");
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  msg, kMBIconStop, kMBOk);
                     break;
                  }
                  if (strstr (fCurAchn, "(REF") != 0) {
                     TString msg = TString ("Cannot reference another reference.");
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  msg, kMBIconStop, kMBOk);
                     break;
                  }
                  Int_t i = 0;
                  while ((i < kMaxReferenceTraces) && fTemp.fTraces[i].fValid) {
                     i++;
                  }
                  if (i == kMaxReferenceTraces) {
                     TString msg = TString ("No free slot.");
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  msg, kMBIconStop, kMBOk);
                  }
                  else {
                     fTemp.fTraces[i].fValid = kTRUE;
                     fTemp.fTraces[i].fModified = kRefTraceModAdd;
                     fTemp.fTraces[i].fGraph = fCurGraph;
                     fTemp.fTraces[i].fAChn = fCurAchn;
                     fTemp.fTraces[i].fBChn = fCurBchn;
                     BuildRefList();
                  }
                  break;
               }
            // Update button
            case kRefTraceUpdate:
               {
                  if ((fCurRef >= 0) && (fCurRef < kMaxReferenceTraces) &&
                     fTemp.fTraces[fCurRef].fValid &&
                     (fTemp.fTraces[fCurRef].fModified == kRefTraceModNo)) {
                     fTemp.fTraces[fCurRef].fModified = kRefTraceModUpdate;
                     BuildRefList();
                  }
                  break;
               }
            // Update All button
            case kRefTraceUpdateAll:
               {
                  for (int i = 0; i < kMaxReferenceTraces; i++) {
                     if (fTemp.fTraces[i].fValid && 
                        (fTemp.fTraces[i].fModified == kRefTraceModNo)) {
                        fTemp.fTraces[i].fModified = kRefTraceModUpdate;
                     }
                  }
                  BuildRefList();
                  break;
               }
            // Delete button
            case kRefTraceClear:
               {
                  if ((fCurRef >= 0) && (fCurRef < kMaxReferenceTraces) &&
                     fTemp.fTraces[fCurRef].fValid) {
                     fTemp.fTraces[fCurRef].fValid = kFALSE;
                     fTemp.fTraces[fCurRef].fModified = kRefTraceModDelete;
                     // set current ref to next entry
                     while ((fCurRef < kMaxReferenceTraces - 1) &&
                           !fTemp.fTraces[fCurRef].fValid) {
                        fCurRef++;
                     }
                     while ((fCurRef > 0) && 
                           !fTemp.fTraces[fCurRef].fValid) {
			if (my_debug) cerr << "    Decrementing fCurRef" << endl ;
                        fCurRef--;
                     }
		     if (my_debug) cerr << "       fCurRef = " << fCurRef << endl ;
                     BuildRefList();

		     // Display the proper info for the fCurRef.
		     // (Copied from the section below where a selection is made in the list box)
		     if (fCurGraph != fTemp.fTraces[fCurRef].fGraph) {
			fCurGraph = fTemp.fTraces[fCurRef].fGraph;
			fCurAchn = fTemp.fTraces[fCurRef].fAChn;
			fCurBchn = fTemp.fTraces[fCurRef].fBChn;
			BuildPlotType (0);
		     }
		     else if (fCurAchn != fTemp.fTraces[fCurRef].fAChn) {
			fCurAchn = fTemp.fTraces[fCurRef].fAChn;
			fCurBchn = fTemp.fTraces[fCurRef].fBChn;
			BuildPlotType (1);
		     }
		     else if (fCurBchn != fTemp.fTraces[fCurRef].fBChn) {
			fCurBchn = fTemp.fTraces[fCurRef].fBChn;
			BuildPlotType (0);
		     }
		     
                  }
                  break;
               }
         }
      }
      // list boxes
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_LISTBOX)) {
	 // Omit check for parm2 != fCurRef so we can see what 
	 // a reference contains when there's only one.
         //if ((parm1 == kRefTraceRef) && (parm2 != fCurRef) &&
         if ((parm1 == kRefTraceRef) && (parm2 >= 0) && (parm2 < kMaxReferenceTraces)) {
            fCurRef = parm2;
            if (fCurGraph != fTemp.fTraces[fCurRef].fGraph) {
               fCurGraph = fTemp.fTraces[fCurRef].fGraph;
               fCurAchn = fTemp.fTraces[fCurRef].fAChn;
               fCurBchn = fTemp.fTraces[fCurRef].fBChn;
               BuildPlotType (0);
            }
            else if (fCurAchn != fTemp.fTraces[fCurRef].fAChn) {
               fCurAchn = fTemp.fTraces[fCurRef].fAChn;
               fCurBchn = fTemp.fTraces[fCurRef].fBChn;
               BuildPlotType (1);
            }
            else if (fCurBchn != fTemp.fTraces[fCurRef].fBChn) {
               fCurBchn = fTemp.fTraces[fCurRef].fBChn;
               BuildPlotType (0);
            }
         }
      }
      // combo boxes
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_COMBOBOX)) {
	 // Graph combo box
         // new data/plot type
         if (parm1 == kRefTraceGraph) {
            TGTextLBEntry* entry = 
               (TGTextLBEntry*) fGraph->GetSelectedEntry();
            if (entry) {
               const char* sel = entry->GetText()->GetString();
               if (fCurGraph == sel) {
                  return kTRUE;
               }
               fCurGraph = sel;
            }
            BuildPlotType (1);
         }
         // new A channel combo box
         else if (parm1 == kRefTraceAchn) {
            TGTextLBEntry* entry = 
               (TGTextLBEntry*) fAchn->GetSelectedEntry();
            if (entry) {
               const char* sel = entry->GetText()->GetString();
               if (fCurAchn == sel) {
                  return kTRUE;
               }
               fCurAchn = sel;
            }
            BuildPlotType (2);
         }
         // new B channel combo box
         else if (parm1 == kRefTraceBchn) {
            TGTextLBEntry* entry = 
               (TGTextLBEntry*) fBchn->GetSelectedEntry();
            if (entry) {
               const char* sel = entry->GetText()->GetString();
               if (fCurBchn == sel) {
                  return kTRUE;
               }
               fCurBchn = sel;
            }
         }
      }
      return kTRUE;
   }



}
