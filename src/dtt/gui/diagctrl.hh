/* version $Id: diagctrl.hh 8024 2018-04-18 18:04:26Z ed.maros@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: diagctrl						*/
/*                                                         		*/
/* Module Description: Conrtol tab of diagnostics test tools		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 23Nov99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: diagctrl.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_DIAGCTRL_H
#define _GDS_DIAGCTRL_H

#define TEST_RATE 1

#include <memory>
#include <TGFrame.h>
#include <TGButton.h>
#include <TGLabel.h>
#include <TGComboBox.h>
#include <TGTab.h>
#include <TGTextEntry.h>
#include <TGTextEdit.h>
#include "PlotSet.hh"
#include "dataacc.hh"
#include "gdsmain.h"

namespace ligogui {
   class TLGChannelCombobox;
   class TLGNumericControlBox;
   class ChannelEntry;
   class TLGTextEditor;
   class TLGMultiPad;
   class TLGComboEditBox;
}

namespace dfm {
   class TLGDfmSelection;
}

namespace diag {

   class DiagMainWindow;


/** @name diagctrl
    This header exports the main graphical control interface for 
    performing diagnostics tests.
   
    @memo Diagnostics control interface
    @author Written December 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{
/** @name Constants
    Constants for the diagnostics control interface.
   
    @memo Diagnostics control interface constants
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// Status messages
   const Int_t kC_STATUS = 130;
   /// Status submessages: message
   const Int_t kCS_MESSAGE = 1;
   /// Status submessages: iterator
   const Int_t kCS_ITERATOR = 2;
   /// Status submessages: measurement
   const Int_t kCS_MEASUREMENT = 3;
   /// Status submessages: run
   const Int_t kCS_RUN = 4;

   /// Notify messages
   const Int_t kC_NOTIFY = 131;


   /// Maximum number of measurement channels
   // For really wide channel combo boxes.
   // 8 rows of two channels.
   const Int_t kMaxMeasChannel = 96;
   /** Number of displayed measurement channels 
       (must be an integer fraction of kMaxMeasChannel) */
   const Int_t kShowMeasChannel = 16;
   /// Maximum number of excitation channels
   const Int_t kMaxExcChannel = 20;
   /** Number of displayed excitation channels 
       (must be an integer fraction of kMaxExcChannel) */
   const Int_t kShowExcChannel = 4;
   /// Maximum number of lidax data input servers
   const Int_t kMaxLidaxServer = 20;
   /// Maximum number of channels per lidax data input server
   const Int_t kMaxLidaxChannel = 10000;

   /// Measurement selection radio buttons widget ids
   const Int_t kMeasSel = 1000;
   /// Data broker selection radio buttons
   const Int_t kDataInput = kMeasSel + 4;  		// 1004
   /// Data broker reconnect check box
   const Int_t kDataReconnect = kDataInput + 4;		// 1008
   /// Data broker reconnect check box
   const Int_t kDataCacheClear = kDataReconnect + 1;	// 1009
   /// NDS name
   const Int_t kNDSName = kDataCacheClear + 1;		// 1010
   /// NDS port
   const Int_t kNDSPort = kNDSName + 1;			// 1011
   /// Lidax box
   const Int_t kLidaxSource = kNDSPort + 1;		// 1012

   /// Measurement channel number radio buttons widget ids
   const Int_t kMeasNum = kLidaxSource + 1;		// 1013
   /// Measurement channel selection widget ids
   const Int_t kMeasChn = kMeasNum + kMaxMeasChannel / kShowMeasChannel; // 1019
   /// Measurement channel selection active widget ids
   const Int_t kMeasChnActive = kMeasChn + kShowMeasChannel; // 1035
   /// FFT start frequency widget id
   const Int_t kFFTStart = kMeasChnActive + kShowMeasChannel;
   /// FFT stop frequency widget id
   const Int_t kFFTStop = kFFTStart + 1;
   /// FFT bandwidth widget id
   const Int_t kFFTBW = kFFTStop + 1;
   /// FFT timespan widget id
   const Int_t kFFTTimeSpan = kFFTBW + 1;
   /// FFT settling time widget id
   const Int_t kFFTSettling = kFFTTimeSpan + 1;
   /// FFT window widget id
   const Int_t kFFTWindow = kFFTSettling + 1;
   /// FFT overlap widget id
   const Int_t kFFTOverlap = kFFTWindow + 1;
   /// FFT remove DC term widget id
   const Int_t kFFTRemoveDC = kFFTOverlap + 1;
   /// FFT Number of A channels widget id
   const Int_t kFFTAChannels = kFFTRemoveDC + 1;
   /// FFT averages widget id
   const Int_t kFFTAverages = kFFTAChannels + 1;
   /// FFT average type widget ids
   const Int_t kFFTAverageType = kFFTAverages + 1;
   /// FFT Burst Noise Quiet Time ids
   const Int_t kFFTBurstNoiseQuietTime = kFFTAverageType + 1;

   /// Swept sine start frequency widget id
   const Int_t kSSStart = kFFTBurstNoiseQuietTime + 3;
   /// Swept sine start frequency widget id
   const Int_t kSSStop = kSSStart + 1;
   /// Swept sine points widget id
   const Int_t kSSPoints = kSSStop + 1;
   /// Swept sine settling time widget id
   const Int_t kSSSettling = kSSPoints + 1;
   /// Swept sine Sweep direction widget ids
   const Int_t kSSSweepDir = kSSSettling + 1;
   /// Swept sine sweep type widget ids
   const Int_t kSSSweepType = kSSSweepDir + 2;
//    /// Swept sine file read widget id
   // const Int_t kSSSweepFile = kSSSweepType + 3;
//    /// Swept sine file read widget id
   // const Int_t kSSSweepFileRead = kSSSweepFile + 1;
//    /// Swept sine file format widget id
   // const Int_t kSSSweepFileFormat = kSSSweepFileRead + 1;
   /// Swept sine edit widget id
   const Int_t kSSSweepEdit = kSSSweepType + 3;
   /// Swept sine format widget id
   const Int_t kSSSweepFormat = kSSSweepEdit + 1;
   /// Swept sine sweep point editor widget id
   const Int_t kSSSweepEditor = kSSSweepFormat + 1;
   /// Swept sine window widget id
   const Int_t kSSWindow = kSSSweepEditor + 1;
   /// Swept sine power spectrum widget id
   const Int_t kSSPowerSpec = kSSWindow + 1;
   /// Swept sine Number of A channels widget id
   const Int_t kSSAChannels = kSSPowerSpec + 1;
   /// Swept sine use double precision frequency buckets
   const Int_t kSSDoublePrecFreqs = kSSAChannels + 1;
   /// Swept sine measurement time selection widget ids
   const Int_t kSSMeasTimeSel = kSSDoublePrecFreqs + 1;
   /// Swept sine measurement time widget id
   const Int_t kSSMeasTime = kSSMeasTimeSel + 2;
   /// Swept sine averages widget id
   const Int_t kSSAverages = kSSMeasTime + 2;
   /// Swept sine harmonic order widget id
   const Int_t kSSHarmonicOrder = kSSAverages + 1;

   /// Sine response measurement time selection widget ids
   const Int_t kSRMeasTimeSel = kSSHarmonicOrder + 1;
   /// Sine response measurement time widget id
   const Int_t kSRMeasTime = kSRMeasTimeSel + 2;
   /// Sine response settling time widget id
   const Int_t kSRSettling = kSRMeasTime + 2;
   /// Sine response window widget id
   const Int_t kSRWindow = kSRSettling + 1;
   /// Sine response power spectrum widget id
   const Int_t kSRPowerSpec = kSRWindow + 1;
   /// Sine response averages widget id
   const Int_t kSRAverages = kSRPowerSpec + 1;
   /// Sine response average type widget ids
   const Int_t kSRAverageType = kSRAverages + 1;
   /// Sine response harmonic order widget id
   const Int_t kSRHarmonicOrder = kSRAverageType + 3;

   /// Triggered time response measurement time widget id
   const Int_t kTSMeasTime = kSRHarmonicOrder + 1;
   /// Triggered time response bandwidth widget id
   const Int_t kTSBW = kTSMeasTime + 1;
   /// Triggered time response settling time widget id
   const Int_t kTSSettling = kTSBW + 1;
   /// Triggered time response pre-trigger time widget id
   const Int_t kTSPreTrigTime = kTSSettling + 1;
   /// Triggered time response dead time widget id
   const Int_t kTSDeadTime = kTSPreTrigTime + 1;
   /// Triggered time response include statistics widget id
   const Int_t kTSIncStat = kTSDeadTime + 1;
   /// Triggered time response include statistics widget id
   const Int_t kTSFilterSpec = kTSIncStat + 1;
   /// Triggered time response foton widget id
   const Int_t kTSFoton = kTSFilterSpec + 1;
   /// Triggered time response filter spec widget id
   const Int_t kTSAverages = kTSFoton + 1;
   /// Triggered time response average type widget ids
   const Int_t kTSAverageType = kTSAverages + 1;
   // Burst noise quiet time id
   const Int_t kTSBurstNoiseQuietTime = kTSAverageType +1;

   /// Start time radio button widget ids
   const Int_t kTimeStart = kTSBurstNoiseQuietTime + 3;
   /// Start time future time entry widget id
   const Int_t kTimeFuture = kTimeStart + 5;
   /// Start time past time entry widget id
   const Int_t kTimePast = kTimeFuture + 1;
   /// Start time GPS sec time entry widget id
   const Int_t kTimeGPS = kTimePast + 1;
   /// Start time GPS nsec time entry widget id
   const Int_t kTimeGPSN = kTimeGPS + 1;
   /// Start time date entry widget id
   const Int_t kTimeDate = kTimeGPSN + 1;
   /// Start time time entry widget id
   const Int_t kTimeTime = kTimeDate + 1;
   /// Start time time entry widget id
   const Int_t kTimeNow = kTimeTime + 1;
   /// Start time time entry widget id
   const Int_t kTimeLookup = kTimeNow + 1;
   /// Start time slow down widget id
   const Int_t kTimeSlowDown = kTimeLookup + 1;

   /// Measuremenmt time widget id
   const Int_t kInfoMeasTime = kTimeSlowDown + 1;
   /// Measuremenmt information comment widget id
   const Int_t kInfoComment = kInfoMeasTime + 1;

   /// Excitation number selection
   const Int_t kExcNum = kInfoComment + 1;
   /// Excitation channel active
   const Int_t kExcChnActive = kExcNum + kMaxExcChannel / kShowExcChannel;
   /// Excitation channel name
   const Int_t kExcChnName = kExcChnActive +  kShowExcChannel;
   /// Excitation channel readback type
   const Int_t kExcChnRBType = kExcChnName +  kShowExcChannel;
   /// Excitation channel readback name
   const Int_t kExcChnRBName = kExcChnRBType +  3 * kShowExcChannel;
   /// Excitation channel waveform
   const Int_t kExcChnWaveform = kExcChnRBName +  kShowExcChannel;
   /// Excitation channel waveform file
   const Int_t kExcChnWaveformFile = kExcChnWaveform +  kShowExcChannel;
   /// Excitation channel waveform file selection
   const Int_t kExcChnWaveformFileSel = kExcChnWaveformFile +  kShowExcChannel;
   /// Excitation channel frequency
   const Int_t kExcChnFreq = kExcChnWaveformFileSel +  kShowExcChannel;
   /// Excitation channel amplitude
   const Int_t kExcChnAmpl = kExcChnFreq +  kShowExcChannel;
   /// Excitation channel offset
   const Int_t kExcChnOffs = kExcChnAmpl +  kShowExcChannel;
   /// Excitation channel phase
   const Int_t kExcChnPhase = kExcChnOffs +  kShowExcChannel;
   /// Excitation channel ratio
   const Int_t kExcChnRatio = kExcChnPhase +  kShowExcChannel;
   /// Excitation channel frequency range
   const Int_t kExcChnFreqRange = kExcChnRatio +  kShowExcChannel;
   /// Excitation channel amplitude range
   const Int_t kExcChnAmplRange = kExcChnFreqRange +  kShowExcChannel;
   /// Excitation channel filter command
   const Int_t kExcChnFilterCmd = kExcChnAmplRange +  kShowExcChannel;
   /// Excitation channel filter wiz button
   const Int_t kExcChnFilterWiz = kExcChnFilterCmd +  kShowExcChannel;

   /// FFT ramp up/down time
   // This appears on the Fourier Tools panel, but is defined here because
   // it's too hard to rearrange all the constants!
   const Int_t kFFTRampDown = kExcChnFilterWiz + kShowExcChannel ;
   const Int_t kFFTRampUp = kFFTRampDown + kShowExcChannel ;
   /// SS ramp up/down time
   const Int_t kSSRampDown = kFFTRampUp + kShowExcChannel ;
   const Int_t kSSRampUp = kSSRampDown + kShowExcChannel ;
   /// Sine response ramp up/down time
   const Int_t kSRRampDown = kSSRampUp + kShowExcChannel ;
   const Int_t kSRRampUp = kSRRampDown + kShowExcChannel ;
   /// Time series ramp up/down time
   const Int_t kTSRampDown = kSRRampUp + kShowExcChannel ;
   const Int_t kTSRampUp = kTSRampDown + kShowExcChannel ;
   // Specify the epoch as a start and stop GPS time for NDS2
   /// NDS2 Server name
   const Int_t kNDS2Name = kTSRampUp + kShowExcChannel ;
   /// NDS2 Server Port
   const Int_t kNDS2Port = kNDS2Name + kShowExcChannel ;
   /// GPS start time of NDS2 Epoch
   const Int_t kNDS2EpochStartGPS = kNDS2Port + kShowExcChannel ;
   /// Start date of NDS2 Epoch
   const Int_t kNDS2EpochStartDate = kNDS2EpochStartGPS + kShowExcChannel ;
   /// Start time of NDS2 Epoch
   const Int_t kNDS2EpochStartTime = kNDS2EpochStartDate + kShowExcChannel ;
   /// GPS stop time of NDS2 Epoch
   const Int_t kNDS2EpochStopGPS = kNDS2EpochStartTime + kShowExcChannel ;
   /// Stop date of NDS2 Epoch
   const Int_t kNDS2EpochStopDate = kNDS2EpochStopGPS + kShowExcChannel ;
   /// Stop time of NDS2 Epoch
   const Int_t kNDS2EpochStopTime = kNDS2EpochStopDate + kShowExcChannel ;
   /// Named epoch for NDS2
   const Int_t kNDS2EpochName = kNDS2EpochStopTime + kShowExcChannel ;

//@}

/** @name Types
    Types for the diagnostics control interface.
   
    @memo Diagnostics control interface strcuture type
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

#ifdef TEST_RATE
   typedef std::pair <std::string, int> ChannelNameRate ;
#endif

   struct NDS2Epoch_t {
      /// Epoch name
      TString		fEpochName ;
      /// Epoch GPS Time Range
      ULong_t		fEpoch[2] ;
   } ;

   /// Data Source parameters
   struct DataSourceParam_t {
      /// Online/Lidax data source?
      Bool_t		fOnline;
      /// Reconnect?
      Bool_t		fReconnect;
      /// User NDS?
      enum nds_type { kNoNDS,
		      kUserNDS1,
		      kUserNDS2
      };
      nds_type          fUserNDS;
      /// NDS name
      TString		fNDSName;
      /// NDS port
      Int_t		fNDSPort;
      /// Pointer to lidax data access object
      dfm::dataaccess 	fDacc;
      /// NDS2 server name
      TString		fNDS2Name ;
      /// NDS port
      Int_t		fNDS2Port ;
      /// Epoch name
      TString		fNDS2EpochName ;
      /// Epoch time, 0 is start, 1 is end.
      ULong_t		fNDS2Epoch[2] ;
   };

   /// Excitation parameters
   struct ExcitationParam_t {
      /// Was this channel ever used?
      Bool_t		fUsed;
      /// Channel active?
      Bool_t		fActive;
      /// Channel name
      TString		fName;
      /// Readback channel type : 0 - default, 1 - none , 2 - user
      Int_t		fRBType;
      /// Readback channel name
      TString		fRBName;
      /// Waveform
      Int_t		fWaveform;
      /// Waveform file
      TString		fWaveformFile;
      /// Frequency (Hz)
      Double_t		fFreq;
      /// Amplitude
      Double_t		fAmpl;
      /// Offset
      Double_t		fOffs;
      /// Phase (rad)
      Double_t		fPhase;
      /// Ratio
      Double_t		fRatio;
      /// Frequency range
      Double_t		fFreqRange;
      /// Amplitude range
      Double_t		fAmplRange;
      /// Filter command
      TString		fFilterCmd;
   };


   /// Measurement parameters
   struct MeasParam_t {
      /// Measurement type
      Int_t		fMeasType;
      /// Was this channel ever used?
      Bool_t		fMeasChnUsed[kMaxMeasChannel];
      /// Measurement channel active?
      Bool_t		fMeasActive[kMaxMeasChannel];
      /// Measurement channel names
#ifdef TEST_RATE
      // Make this a pair that contains name and rate.
      ChannelNameRate   fMeasChn[kMaxMeasChannel];
#else
      TString		fMeasChn[kMaxMeasChannel];
#endif
      /// Start frequency (Hz)
      Double_t		fStart;
      /// Stop frequency (Hz)
      Double_t		fStop;
      /// Resolution Bandwidth (Hz)
      Double_t		fResolutionBW;
      /// Signal Bandwidth (Hz)
      Double_t		fSignalBW;
      /// Settling time
      Double_t		fTimeSettling;
      /// Ramp down time (fft or Swept sine)
      Double_t		fRampDownTime ;
      /// Ramp up time (fft or Swept Sine)
      Double_t		fRampUpTime ;
      /// Window type
      Int_t		fWindow;
      /// Overlap
      Double_t		fOverlap;
      /// Remove DC term?
      Bool_t		fRemoveDC;
      /// Number of A channels
      Int_t		fAChannels;
      /// Number of averages
      Int_t		fAverages;
      /// Average type
      Int_t		fAverageType;
      /// Burst noise quite time.  This is the time
      /// after the noise stops, but we're still measuring
      /// to capture any ringing.
      Double_t     fBurstNoiseQuietTime_s;
      /// Number of points
      Int_t		fPoints;
      // whether to store double precision frequency values
      Int_t             fDoublePrecFreq;
      /// Measurement time
      Double_t		fTimeMeas[2];
      /// Harmonic order
      Int_t		fHarmonicOrder;
      /// Include power spectra?
      Bool_t		fPowerSpec;
      /// Sweep direction
      Int_t		fSweepDir;
      /// Sweep type
      Int_t		fSweepType;
      /// Sweep points data array
      std::unique_ptr<BasicDataDescriptor> fSweepPoints;
      /// Pre-trigger time
      Double_t		fTimePreTrig;
      /// Dead time
      Double_t		fTimeDead;
      /// Include statistical values?
      Bool_t		fStatistics;
      /// Filter string
      TString		fFilter;
   };


   /// Iterator parameters
   struct IterParam_t {
      /// Iterator type
      Int_t		fIterType;
      /// Number of iterations
      Int_t		fRepeat;
   };


   /// Synchronization
   struct SyncParam_t {
      /// Syncronization type
      Int_t		fType;
      /// Start time in GPS sec/nsec
      ULong_t	 	fStart[2];
      /// Wait time
      Double_t		fWait;
      /// Slow down time (sec per measurement)
      Double_t		fSlowDown;
   };


   /// Test parameters
   struct TestParam_t {
      /// Start time in GPS sec/nsec
      ULong_t		fMeasTime[2];
      /// Comment / Description
      TString		fComment;
      /// Data source
      DataSourceParam_t	fData;
      /// Measurement parameters
      MeasParam_t	fMeas;
      /// Excitation parameters
      ExcitationParam_t	fExc[kMaxExcChannel];
      /// Iterator parameters
      IterParam_t	fIter;
      /// Synchronization parameters
      SyncParam_t	fSync;
   };


/** A routine which fills a test parameter with default values.
   
    @memo Default test parameter values.
    @author Written December 1999 by Daniel Sigg
    @version 1.0
    @param p Test parameter structure
    @param measId Measurement type
    @return void
 ************************************************************************/
   void defaultTestParameters (TestParam_t& p, Int_t measId = 0);
//@}


/** A tab widget which implements most graphical controls and the 
    plotting pads for the diagnostics test program.
   
    @memo Tab control for diagnostics test controls.
    @author Written December 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DiagTabControl : public TGTab {
     using TGTab::SetTab;
   protected:
      /// List of measurement channels which don't exist in current
      /// channel list.  Filled in by SelectMeasurementChannels().
      std::vector<std::string> fBogusList ;

      /// List of pre-defined epochs obtained from an NDS2 server.
      typedef std::vector<NDS2Epoch_t *> epochList_t ;

      // The actual list.
      epochList_t 	fEpochList ;

      void ParseEpochListString(std::string *list) ;
   protected:
      /// main diagnostics window 
      DiagMainWindow*	fDiag;
      /// Pointer to test parameters
      TestParam_t* 	fParam;
      /// Pointer to measurement channels
      const char*	fMeasChannelList;
      /// Pointer to measurement channels
      ligogui::ChannelEntry*	fMeasChnList;
      /// Number of measurement channels
      UInt_t		fMeasChnLen;
      /// Pointer to excitation channels
      const char*	fExcChannelList;
      /// Pointer to excitation channels
      ligogui::ChannelEntry*	fExcChnList;
      /// Number of excitation channels
      UInt_t		fExcChnLen;
      /// Pointer to data channels
      ligogui::ChannelEntry*	fDataChnList;
      /// Number of data channels
      UInt_t		fDataChnLen;
      /// Use online channels?
      Bool_t 		fOnlineChannels;
      /// Use user NDS channels?
      Bool_t 		fUserNDSChannels;
      /// Name of user NDS
      TString		fUserNDSName;
      /// Pointer to plot set
      PlotSet*	fPlot;
      /// Busy cursor
      static Cursor_t	fWaitCursor;
   
      /// Currently selected measurement
      Int_t		fMeasSel;
      /// Currently selected iterator
      Int_t		fIterSel;
      /// Currently selected measurement channel range
      Int_t		fMeasRangeSel;
      /// Currently selected excitation channel range
      Int_t		fExcRangeSel;
   
      /// Input tab
      TGCompositeFrame* fInputTab;
      /// Measurement tab
      TGCompositeFrame* fMeasTab;
      /// Excitation tab
      TGCompositeFrame* fExcTab;
      /// Result tab
      TGCompositeFrame* fResTab;
      /// Iteartor tab
      TGCompositeFrame* fIterTab;
      /// Synchronization tab
      TGCompositeFrame* fSyncTab;
      /// Environment tab
      TGCompositeFrame* fEnvTab;
      /// Defaults tab
      TGCompositeFrame* fDefTab;
   
      /// Data source selection frame
      TGGroupFrame*	fInputF1;
      /// NDS source selection frame
      TGGroupFrame*	fInputF2;
      /// Data source selection frames for lines
      TGCompositeFrame*	fDataSelFrame[2];
      /// Group frame layout hints
      TGLayoutHints*	fInputGroupLayout;
      /// Data source selection layout hints
      TGLayoutHints*	fInput1Layout[4];
      /// Input labels
      TGLabel*		fDataSelLabel[2];
      /// Data source selection radio buttons
      TGButton*		fDataInput[4];
      /// Data source reconnect button
      TGButton*		fReconnectInput;
      /// Cache clear
      TGButton*		fCacheClear;
      /// NDS name
      ligogui::TLGComboEditBox*	fNDSName;
      /// NDS port
      ligogui::TLGNumericControlBox* fNDSPort;
      /// Data source group
      dfm::TLGDfmSelection*	fSource;

      /// NDS2 Source selection frame
      TGGroupFrame	*fInputF3 ;
      /// Horizontal frames to organize NDS2 input widgets
      TGHorizontalFrame	*fNDS2ServerFrame,
      			*fNDS2EpochFrame,
			*fNDS2EpochHFrame[4] ;
      /// Group frames to organize NDS2 epoch start and stop times
      TGGroupFrame	*fNDS2EpochStartFrame,
      			*fNDS2EpochStopFrame ;
      /// NDS2 labels
      TGLabel		*fNDS2Label[13] ;
      /// NDS2 Server Name entry
      ligogui::TLGComboEditBox		*fNDS2Name ;
      /// NDS2 Port number entry
      ligogui::TLGNumericControlBox	*fNDS2Port ;
      /// NDS2 Epoch Name entry	
      TGComboBox			*fNDS2EpochName ;
      /// NDS2 Epoch Start GPS time
      ligogui::TLGNumericControlBox	*fNDS2EpochStartGPS ;
      /// NDS2 Epoch Start Date
      ligogui::TLGNumericControlBox     *fNDS2EpochStartDate ;
      /// NDS2 Epoch Start Time
      ligogui::TLGNumericControlBox     *fNDS2EpochStartTime ;
      /// NDS2 Epoch Stop GPS time
      ligogui::TLGNumericControlBox     *fNDS2EpochStopGPS ;
      /// NDS2 Epoch Stop Date
      ligogui::TLGNumericControlBox     *fNDS2EpochStopDate ;
      /// NDS2 Epoch Stop Time
      ligogui::TLGNumericControlBox     *fNDS2EpochStopTime ;
      /// Measurement selection frame
      TGGroupFrame*	fMeasF1;
      /// Measurement channel selection frame
      TGGroupFrame*	fMeasF2;
      /// Test selection overlay frame
      TGCompositeFrame* fMeasTest;
      /// FFT selection frame
      TGGroupFrame*	fMeasFFT;
      /// Swept sine selection frame
      TGGroupFrame*	fMeasSS;
      /// Sine response selection frame
      TGGroupFrame*	fMeasSR;
      /// Triggered time response selection frame
      TGGroupFrame*	fMeasTS;
      /// Group frame layout hints
      TGLayoutHints*	fMeasGroupLayout;
      /// Test overlay frame layout hints
      TGLayoutHints*	fMeasTestLayout;
      /// Measurement selection frames for lines
      TGCompositeFrame*	fMeasSelFrame[2];
      /// Measurement selection layout hints
      TGLayoutHints*	fMeas1Layout[3];
      /// Test selection radio buttons
      TGButton*		fMeasType[4];
      /// Measurement channel selection radio buttons
      TGButton*		fMeasNum[kMaxMeasChannel / kShowMeasChannel];
      /// Channel selection layout hints
      TGLayoutHints*	fMeas2Layout[4];
      /// Channel line frame
      /* JCB */
#if 1
      TGCompositeFrame*	fMChnF[(kShowMeasChannel + 1) / 2]; // Based on number of columns - JCB
      // TGCompositeFrame*	fMChnF[(kShowMeasChannel + 2) / 3];
#else
      TGCompositeFrame*	fMChnF[(kShowMeasChannel + 3) / 4];
#endif
      /// Channel active check buttons
      TGButton*		fMChnActive[kShowMeasChannel];
      /// Channel labels
      TGLabel*		fMChnLabel[kShowMeasChannel];
      /// Channel selection
      ligogui::TLGChannelCombobox* fMChn[kShowMeasChannel];
   
      /// FFT frame for lines
      TGCompositeFrame*	fFFTFrame[3];
      /// FFT layout hints for lines
      TGLayoutHints*	fFFTLayout[3];
      /// Labels used by FFT
      TGLabel*		fFFTLabel[14] ;
      /// FFT start frequency
      ligogui::TLGNumericControlBox* fFFTStart;
      /// FFT stop frequency
      ligogui::TLGNumericControlBox* fFFTStop;
      /// FFT bandwidth
      ligogui::TLGNumericControlBox* fFFTBW;
      /// FFT time span
      ligogui::TLGNumericControlBox* fFFTTimeSpan;
      /// FFT settling time
      ligogui::TLGNumericControlBox* fFFTSettling;
      /// FFT Ramp Down time
      ligogui::TLGNumericControlBox* fFFTRampDown;
      /// FFT Ramp Up time
      ligogui::TLGNumericControlBox* fFFTRampUp ;
      /// FFT averages
      ligogui::TLGNumericControlBox* fFFTAverages;
      /// FFT Burst Noise Quite Time
      ligogui::TLGNumericControlBox* fFFTBurstNoiseQuietTime_s;
      /// Average type radio buttons
      TGButton*		fFFTAverageType[3];
      /// FFT window
      TGComboBox*	fFFTWindow;
      /// FFT window overlap
      ligogui::TLGNumericControlBox* fFFTOverlap;
      /// FFT remove DC check button
      TGButton* 	fFFTRemoveDC;
      /// FFT Number of A channels
      ligogui::TLGNumericControlBox* fFFTAChannels;
   
      /// Swept sine frame for lines
      TGCompositeFrame*	fSSFrame[4];
      /// Swept sine layout hints for lines
      TGLayoutHints*	fSSLayout[4];
      /// Labels used by Swept sine
      TGLabel*		fSSLabel[16];
      /// Swept sine start frequency
      ligogui::TLGNumericControlBox* fSSStart;
      /// Swept sine stop frequency
      ligogui::TLGNumericControlBox* fSSStop;
      /// Swept sine points
      ligogui::TLGNumericControlBox* fSSPoints;
      /// Swept sine settling time
      ligogui::TLGNumericControlBox* fSSSettling;
      /// Swept sine ramp down time.
      ligogui::TLGNumericControlBox* fSSRampDown ;
      /// Swept sine ramp up time.
      ligogui::TLGNumericControlBox* fSSRampUp ;
      /// Swept sine sweep direction
      TGButton*		fSSSweepDir[2];
      /// Swept sine sweep type
      TGButton*		fSSSweepType[3];
      // Swept sine frequency bucket precision
      TGButton*         fSSDoublePrecFreq;
      /// Swept sine measurement time selection
      TGButton*		fSSMeasTimeSel[2];
      /// Swept sine measurement time selection
      ligogui::TLGNumericControlBox* fSSMeasTime[2];
      /// Swept sine averages
      ligogui::TLGNumericControlBox* fSSAverages;
      /// Swept sine harmonic order
      ligogui::TLGNumericControlBox* fSSHarmonicOrder;
      /// Swept sine window
      TGComboBox*	fSSWindow;
      /// Swept sine Number of A channels
      ligogui::TLGNumericControlBox* fSSAChannels;
      /// Swept sine power sepctrum
      TGButton* 	fSSPowerSpec;
   //    /// Swept sine sweep points file
      // TGTextEntry*	fSSSweepFile;
   //    /// Swept sine sweep points file format
      // TGComboBox*	fSSSweepFileFormat;
   //    /// Swept sine read file
      // TGButton*		fSSSweepFileRead;
      /// Swept sine sweep format
      TGComboBox*	fSSSweepFormat;
      /// Swept sine sweep edit
      TGButton*		fSSSweepEdit;
      /// Swept sine sweep point editor
      ligogui::TLGTextEditor*	fSSSweepEditor;
      /// Swept sine sweep point edit text
      TGText*		fSSSweepEditText;
      /// Swept sine sweep point editor return value
      Bool_t		fSSSweepEditorRet;
      /// Swept sine sweep point editor done
      Bool_t		fSSSweepEditorDone;
   
      /// Sine response frame for lines
      TGCompositeFrame*	fSRFrame[3];
      /// Sine response layout hints for lines
      TGLayoutHints*	fSRLayout[4];
      /// Labels used by Sine response
      TGLabel*		fSRLabel[11];
      /// Sine response measurement time selection
      TGButton*		fSRMeasTimeSel[2];
      /// Sine response measurement time selection
      ligogui::TLGNumericControlBox* fSRMeasTime[2];
      /// Sine response settling time
      ligogui::TLGNumericControlBox* fSRSettling;
      /// Sine response ramp down time.
      ligogui::TLGNumericControlBox* fSRRampDown ;
      /// Sine response ramp up time.
      ligogui::TLGNumericControlBox* fSRRampUp ;
      /// Sine response window
      TGComboBox*	fSRWindow;
      /// Sine response power sepctrum
      TGButton* 	fSRPowerSpec;
      /// Sine response averages
      ligogui::TLGNumericControlBox* fSRAverages;
      /// Sine response average type radio buttons
      TGButton*		fSRAverageType[3];
      /// Sine response harmonic order
      ligogui::TLGNumericControlBox* fSRHarmonicOrder;
   
      /// Triggered time response frame for lines
      TGCompositeFrame*	fTSFrame[4];
      /// Triggered time response layout hints for lines
      TGLayoutHints*	fTSLayout[5];
      /// Labels used by Triggered time response
      TGLabel*		fTSLabel[12];
      /// Triggered time response measurement time
      ligogui::TLGNumericControlBox* fTSMeasTime;
      /// Triggered time response bandwidth
      ligogui::TLGNumericControlBox* fTSBW;
      /// Triggered time response settling time
      ligogui::TLGNumericControlBox* fTSSettling;
      /// Triggered time ramp down time.
      ligogui::TLGNumericControlBox* fTSRampDown ;
      /// Triggered time ramp up time.
      ligogui::TLGNumericControlBox* fTSRampUp ;
      /// Triggered time response pre-trigger time
      ligogui::TLGNumericControlBox* fTSPreTrigTime;
      /// Triggered time response dead time
      ligogui::TLGNumericControlBox* fTSDeadTime;
      /// Triggered time response include statistics
      TGButton* 	fTSIncStat;
      /// Triggered time response averages
      ligogui::TLGNumericControlBox* fTSAverages;
      /// Triggered time burst noise quiet time
      ligogui::TLGNumericControlBox* fTSBurstNoiseQuietTime_s;
      /// Triggered time response average type radio buttons
      TGButton*		fTSAverageType[3];
      /// Triggered time response filter spec
      TGTextEntry* 	fTSFilterSpec;
      /// Triggered time response foton
      TGButton* 	fTSFoton;
   
      /// Start time selection frame
      TGGroupFrame*	fTimeF;
      /// Start time frame for lines
      TGCompositeFrame*	fTimeFrame[9];
      /// Start time layout hints for lines
      TGLayoutHints*	fTimeLayout[8];
      /// Labels used by Start time 
      TGLabel*		fTimeLabel[8];
      /// Start time radio buttons
      TGButton*		fTimeSel[5];
      /// Start time in the past
      ligogui::TLGNumericControlBox* fTimePast;
      /// Start time in the future
      ligogui::TLGNumericControlBox* fTimeFuture;
      /// Start time in GPS sec
      ligogui::TLGNumericControlBox* fTimeGPS;
      /// Start time in GPS nsec
      ligogui::TLGNumericControlBox* fTimeGPSN;
      /// Start time in date format
      ligogui::TLGNumericControlBox* fTimeDate;
      /// Start time in time format
      ligogui::TLGNumericControlBox* fTimeTime;
      /// Time now button
      TGButton*		fTimeNow;
      /// Time lookup button
      TGButton*		fTimeLookup;
      /// Slow down box
      ligogui::TLGNumericControlBox* fSlowDown;
   
      /// Information selection frame
      TGGroupFrame*	fInfoF;
      /// Information frame for lines
      TGCompositeFrame*	fInfoFrame[2];
      /// Information layout hints for lines
      TGLayoutHints*	fInfoLayout[4];
      /// Labels used by Information 
      TGLabel*		fInfoLabel[2];
      /// Measurememnt time
      TGTextEntry*	fMeasTime;
      /// Comment
      TGTextEntry*	fComment;
   
      /// Excitation channel selection frame
      TGGroupFrame*	fExcSelF;
      /// Excitation channel selection frame layout hints
      TGLayoutHints*	fExcSelLayout;
      /// Excitation channel frames
      TGGroupFrame*	fExcF[kShowExcChannel];
      /// Excitation channel frame names
      TGString*		fExcFName[kShowExcChannel];
      /// Excitation channel selection layout hints
      TGLayoutHints*	fExcNumLayout;
      /// Excitation channel selection radio buttons
      TGButton*		fExcNum[kMaxExcChannel / kShowExcChannel];
   
      /// Excitation channel frames for lines
      TGCompositeFrame*	fExcChnFrame[kShowExcChannel][5]; // JCB changed to 5
      /// Excitation channel layout hints for lines
      TGLayoutHints*	fExcChnLayout[5];
      /// Labels used by Excitation channel
      TGLabel*		fExcChnLabel[kShowExcChannel][12];
      /// Excitation channel active
      TGButton*		fExcChnActive[kShowExcChannel];
      /// Excitation channel name
      ligogui::TLGChannelCombobox* fExcChnName[kShowExcChannel];
      /// Excitation channel readback type
      TGButton*		fExcChnRBType[kShowExcChannel][3];
      /// Excitation channel readback name
      ligogui::TLGChannelCombobox* fExcChnRBName[kShowExcChannel];
      /// Excitation channel waveform
      TGComboBox* 	fExcChnWaveform[kShowExcChannel];
      /// Excitation channel waveform file
      TGTextEntry* 	fExcChnWaveformFile[kShowExcChannel];
      /// Excitation channel waveform file selection
      TGButton* 	fExcChnWaveformFileSel[kShowExcChannel];
      /// Excitation channel frequency
      ligogui::TLGNumericControlBox* fExcChnFreq[kShowExcChannel];
      /// Excitation channel ampliude
      ligogui::TLGNumericControlBox* fExcChnAmpl[kShowExcChannel];
      /// Excitation channel offset
      ligogui::TLGNumericControlBox* fExcChnOffs[kShowExcChannel];
      /// Excitation channel phase
      ligogui::TLGNumericControlBox* fExcChnPhase[kShowExcChannel];
      /// Excitation channel ratio
      ligogui::TLGNumericControlBox* fExcChnRatio[kShowExcChannel];
      /// Excitation channel frequency range
      ligogui::TLGNumericControlBox* fExcChnFreqRange[kShowExcChannel];
      /// Excitation channel ampliude range
      ligogui::TLGNumericControlBox* fExcChnAmplRange[kShowExcChannel];
      /// Filter command
      TGTextEntry*	fExcChnFilterCmd[kShowExcChannel];
      /// Filter wiz button
      TGButton*		fExcChnFilterWiz[kShowExcChannel];
   
      /// Plot pad
      ligogui::TLGMultiPad*	fPad;
      /// Pad layout hints
      TGLayoutHints*	fPadLayout;

      /// when true, updates between timespan and bw are more relaxed,
      /// which allows the breaking of endless loops of updates,
      /// since bw update to timespan doesn't exactly equal the reverse.
      double last_auto_bw = -1;
      double last_auto_timespan = -1;
   
      /** Read information parameters.
          @memo Read information parameters.
          @return void
       ******************************************************************/
      virtual void ReadInfo ();
      /** Write information parameters.
          @memo Write information parameters.
          @param mTimeOnly Write measurement time only
          @return void
       ******************************************************************/
      virtual void WriteInfo (Bool_t mTimeOnly = kFALSE);
      /** Read parameter of input selection.
          @memo Read input selection parameters.
          @return void
       ******************************************************************/
      virtual void ReadDataSourceParam ();
      /** Write parameter of input selection.
          @memo Write input selection parameters.
          @return void
       ******************************************************************/
      virtual void WriteDataSourceParam ();
      /** Read parameter of specified measurement.
          @memo Read measurement parameters.
          @param id Measurement type
          @return void
       ******************************************************************/
      virtual void ReadMeasParam (Int_t id);
      /** Write parameter of specified measurement.
          @memo Write measurement parameters.
          @param id Measurement type
          @return void
       ******************************************************************/
      virtual void WriteMeasParam (Int_t id);
      /** Read measurement channels.
          @memo Read measurement channels.
          @param id Channel range
          @return void
       ******************************************************************/
      virtual void ReadMeasChannel (Int_t id);
      /** Write measurement channels.
          @memo Write measurement channels.
          @param id Channel range
          @return void
       ******************************************************************/
      virtual void WriteMeasChannel (Int_t id);
      /** Read excitation channels.
          @memo Read excitation channels.
          @param id Channel range
          @return void
       ******************************************************************/
      virtual void ReadExcChannel (Int_t id);
      /** Write excitation channels.
          @memo Write excitation channels.
          @param id Channel range
          @return void
       ******************************************************************/
      virtual void WriteExcChannel (Int_t id);
      /** Read parameter of specified iterator.
          @memo Read iterator parameters.
          @param id Iterator type
          @return void
       ******************************************************************/
      virtual void ReadIterParam (Int_t id);
      /** Write parameter of specified iterator.
          @memo Write iterator parameters.
          @param id Iterator type
          @return void
       ******************************************************************/
      virtual void WriteIterParam (Int_t id);
   
      // Select the measurment channels that are displayed in the TLGCombobox
      // menus in the measurment channels section.  Channel names are written
      // to the menus before the channel list is read, so it's unknown if the
      // channels actually exist.  This is called from UpdateChannels() a
      // couple of times.
      void SelectMeasurementChannels() ;


   public:
      std::vector<std::string> *GetBogusChannels() ;
      void ResolveMeasurementChannels() ;

      /** Constructs a diagnostics tab control.
          @memo Constructor
          @param p Parent window
          @param param Test parameters
          @param measchns Measurement channel list
          @param excchns Excitation channel list
          @param list option storage list
          @param max size of option storage list
       ******************************************************************/
      DiagTabControl (const TGWindow* p, DiagMainWindow* diag,
                     TestParam_t& param, PlotSet* plots,
                     const char* measchns, const char* excchns);
      /** Destructs the diagnostics tab control.
          @memo Destructor.
       ******************************************************************/
      virtual ~DiagTabControl ();
   
      /** Sets the measurement time.
          @memo Set measurement time.
          @return void
       ******************************************************************/
      virtual void SetMeasurementTime () {
         WriteInfo (kTRUE); }
      /** Set measurement: 0 - Fourier tools, 1 - Swept sine 
          response, 2 - Sine response, and 3 - Triggered time response.
          If ask is true and the current measuremnt contains result 
          plots, a popup dialog box will ask the user if is is ok
          to change the measurement type (which will automatically
          clear the results).
          @memo Set measurement method.
          @param id Measurement type
          @param ask Ask to clear results before changing measurement
   	         type
          @param newParam true if parameters are new
       ******************************************************************/
      virtual void SetMeasurement (Int_t id, Bool_t ask = kFALSE,
                        Bool_t newParam = kFALSE);
      /** Get measurement: Reads the paarmeters of the currently selcted
          measurement type.
          @memo Get measurement method.
          @param id Measurement type
       ******************************************************************/
      virtual void GetMeasurement ();
      /** Get measurement type: 0 - Fourier tools, 1 - Swept sine 
          response, 2 - Sine response, and 3 - Triggered time response.
          @return Measurement type
       ******************************************************************/
      virtual Int_t GetSelectedMeasurement () const {
         return fMeasSel; }
      /** Set iterator: 0 - Repeat, 1 - Scan, and 2 - Optimize.
          @memo Set iterator.
          @param id Iterator type
       ******************************************************************/
      virtual void SetIterator (Int_t id);
      /** Get iterator: Reads the parameters of the currently selected
          iterator.
          @memo Set iterator.
       ******************************************************************/
      virtual void GetIterator ();
      /** Get iterator type: 0 - Repeat, 1 - Scan, and 2 - Optimize.
          @memo Get iterator type.
          @return Iterator type
       ******************************************************************/
      virtual Int_t GetSelectedIterator () const {
         return fIterSel; }
      /** Set measurement channels: Selects the currently displayed
          range of measurement channels.
          @memo Set measurement channels.
          @param range Measurement channel range
       ******************************************************************/
      virtual void SetMeasurementChannels (Int_t range);
      /** Get measurement channels: Reads the currently displayed
          range of measurement channels.
          @memo Get measurement channels.
       ******************************************************************/
      virtual void GetMeasurementChannels ();
      /** Set excitation channels: Selects the currently displayed
          range of excitation channels.
          @memo Set excitation channels.
          @param range Excitation channel range
       ******************************************************************/
      virtual void SetExcitationChannels (Int_t range);
      /** Get excitation channels: Reads the currently displayed
          range of excitation channels.
          @memo Get excitation channels.
       ******************************************************************/
      virtual void GetExcitationChannels ();
   
      /** Transferes parameter between GUI and data structure.
          @memo Transfer parameters.
          @param toGUI if true write data to GUI, otherwise read
       ******************************************************************/
      virtual void TransferParameters (Bool_t toGUI = kTRUE);
      /** Update channels.
          @memo Update channels.
       ******************************************************************/
      virtual void UpdateChannels ();
   
      /** Returns a pointer to the graphics pad.
          @memo Get pad method.
          @return Pointer to graphics pad
       ******************************************************************/
      virtual ligogui::TLGMultiPad* GetPad() const {
         return fPad; }
   
      /** Retruns true if results are present.
          @memo Have results.
          @return true if results, false otherwise
       ******************************************************************/
      virtual bool HaveResults () const {
         return !fPlot->Empty(); }
   
      /** Tab changed.
          @memo Tab changed.
          @param tapIndex Index of new tab
          @return true if tab was changed
       ******************************************************************/
#if ROOT_VERSION_CODE < ROOT_VERSION(5,14,0) 
      virtual void ChangeTab (Int_t tabIndex);
      virtual Bool_t SetTab (Int_t tabIndex);
#else
      virtual void ChangeTab (Int_t tabIndex, Bool_t emit = kTRUE);
      virtual Bool_t SetTab (Int_t tabIndex, Bool_t emit = kTRUE);
#endif
      virtual Bool_t HandleButton(Event_t *event);

      /** Test NDS2 connection.  Also get list of epochs. */
      void NDS2ConnectionTest();

      /** Process GUI messages.
          @memo Process message method.
          @param msg Message id
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
      /** Process button messages.
          @memo Process button method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessButton (Long_t parm1, Long_t);
      /** Process combobox messages.
          @memo Process combobox method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessCombobox (Long_t parm1, Long_t);
      /** Process text entry messages.
          @memo Process text entry method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessTextentry (Long_t parm1, Long_t);
      /** Process text changed messages.
          @memo Process text entry method.
          @param parm1 First message parameter
          @param parm2 Second message parameter
          @return true if successful
       ******************************************************************/
      virtual Bool_t ProcessTextchanged (Long_t parm1, Long_t);
      /**
       * BandWidth (BW) is 1/timespan.  Note that BW is always rounded to
       * the nearest power of two.
       * When timespan is changed, use this function to update BW
       * To a value that will generate a measurement of at least as long
       * as timespan.  The actual timespan will be longer than entered
       * if it the entered value is not a power of two.
       * Note that BW is the actual active value used to calculate
       * the time of the measurements.
       * @return true if successful.
       */
      virtual Bool_t UpdateBWFromTimeSpan();
      /**
       * Timespan is 1/BandWidth (BW) (to the nearest power of two)
       * When updated from BandWidth, timespan will be set to
       * the actual timespan used for the measurement.
       * To what the
       * @return
       */
      virtual Bool_t UpdateTimeSpanFromBW();
   
   };
//@}

}
#endif // _GDS_DIAGCTRL_H

