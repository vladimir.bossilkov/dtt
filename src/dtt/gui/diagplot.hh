/* version $Id: diagplot.hh 7008 2014-02-14 23:50:45Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: diagplot						*/
/*                                                         		*/
/* Module Description: Plot utilities of diagnostics test tools		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 1Dec99   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: diagctrl.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_DIAGPLOT_H
#define _GDS_DIAGPLOT_H

#include <set>
#include <TString.h>
#include "PlotSet.hh"


namespace diag {
   class basic_commandline;
}

namespace calibration {
   class Table;
}

namespace ligogui {

   struct ReferenceTraceList_t; // Defined in ligogui/TLGMath.hh

/** @name diagplot
    This header exports utility functions and classes for plotting
    the results of a diagnostics test.
   
    @memo Diagnostics plot utilities
    @author Written December 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

/** This data descriptor implements access to data owned by a
    diagnostics kernel. Data is only transferred from the kernel when
    needed by default.
   
    @memo Object for describing diagnostics data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DiagDataDescriptor : public BasicDataDescriptor {
   private:
      /// Disable copy constructor
      DiagDataDescriptor (const DiagDataDescriptor&);
      /// Disable assignment operator
      DiagDataDescriptor& operator= (const DiagDataDescriptor&);
   
   protected:
      /// Pointer to command line object
      diag::basic_commandline*	fCmdLine;
      /// Name of data object
      TString		fName;
      /// XY or just Y with x0 and dx
      bool		fXYData;
      /// X values stored as a double
      bool              fXDouble;
      /// dx
      Double_t		fDX;
      /// X offset into data object
      Int_t		fOffsetX;
      /// Y offset into data object
      Int_t		fOffsetY;
      /// Length of original data array
      Int_t		fLength;
      /// Data stored in a table?
      bool		fTableData;
      /// Table width
      Int_t		fTableWidth;
      /// Table column
      Int_t		fTableCol;
      /// Eliminate zeros?
      bool		fNoZero;
      /// Length of data set
      mutable Int_t	fN;
      /// X array
      mutable Float_t*	fX;
      /// Y array
      mutable Float_t*	fY;
      /// table
      mutable Float_t*	fTable;
   
   
      /** Gets the data from the diagnostics kernel.
          @memo Get data method.
          @return true if successful
       ******************************************************************/
      virtual bool GetData() const;
   
   public:
      /** Constructs a diagnostics data descriptor. The convention for
          complex data arrays is to store real and imaginary part
          of each value in the array as two consecutive numbers.
          @memo Constructor.
          @param cmd Pointer to command line interface
          @param name Name of data object
          @param length Length of data
          @param offsetX X array offset into data object
          @param offsetY Y array offset into data object
          @param cmplx if kTRUE Y array is complex
          @param cpyOnNeed Copy data from kernel when needed
          @param eliminateZeros If true (0,0) data points are 
                 automatically eliminated from the data set
          @return void
       ******************************************************************/
      DiagDataDescriptor (diag::basic_commandline* cmd, const TString& name, 
                        Int_t length, Int_t offsetX, Int_t offsetY, bool xdouble,
                        bool cmplx = kFALSE, bool cpyOnNeed = kTRUE,
                        bool eliminateZeros = kFALSE);
      /** Constructs a diagnostics data descriptor. The convention for
          complex data arrays is to store real and imaginary part
          of each value in the array as two consecutive numbers.
          @memo Constructor.
          @param cmd Pointer to command line interface
          @param name Name of data object
          @param length Length of data
          @param x0 X start
          @param dx X spacing
          @param offsetY Y array offset into data object
          @param cmplx if kTRUE Y array is complex
          @param cpyOnNeed Copy data from kernel when needed
          @param eliminateZeros If true (0,0) data points are 
                 automatically eliminated from the data set
          @return void
       ******************************************************************/
      DiagDataDescriptor (diag::basic_commandline* cmd, const TString& name, 
                        Int_t length, Double_t x0, Double_t dx, 
                        Int_t offsetY = 0,
                        bool cmplx = kFALSE, bool cpyOnNeed = kTRUE,
                        bool eliminateZeros = kFALSE);
      /** Constructs a diagnostics data descriptor. The convention for
          complex data arrays is to store real and imaginary part
          of each value in the array as two numbers.
          This constructor assumes the data is stored in a table
          of coefficients where each row represents a different frequency.
          @memo Constructor.
          @param cmd Pointer to command line interface
          @param name Name of data object
          @param cmplx if kTRUE Y array is complex
          @param length Length of data
          @param tablewidth Width of table
          @param tablecol Column number in table
          @param cpyOnNeed Copy data from kernel when needed
          @return void
       ******************************************************************/
      DiagDataDescriptor (diag::basic_commandline* cmd, 
                        const TString& name, bool cmplx, 
                        Int_t length, Int_t tablewidth, Int_t tablecol, 
                        bool cpyOnNeed = kTRUE); 
      /** Constructs a diagnostics data descriptor. The convention for
          complex data arrays is to store real and imaginary part
          of each value in the array as two numbers.
          This constructor assumes the data is stored in a table
          of coefficients where each row represents a different frequency.
          @memo Constructor.
          @param cmd Pointer to command line interface
          @param name Name of data object
          @param cmplx if kTRUE Y array is complex
          @param length Length of data
          @param tablewidth Width of table
          @param tablecol Column number in table
          @param x0 X start
          @param dx X spacing
          @param cpyOnNeed Copy data from kernel when needed
          @return void
       ******************************************************************/
      DiagDataDescriptor (diag::basic_commandline* cmd, 
                        const TString& name, bool cmplx, 
                        Int_t length, Int_t tablewidth, Int_t tablecol, 
                        Double_t x0, Double_t dx,
                        bool cpyOnNeed = kTRUE);
   
      /** Destructs a diagnostics data descriptor.
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~DiagDataDescriptor ();
   
      /** Returns true if xy data.
          @memo Is XY.
          @return true if xy
       ******************************************************************/
      virtual bool IsXY() const {
         return fXYData; }
      /** Returns x spacing if xy data.
          @memo x spacing.
          @return x spacing
       ******************************************************************/
      virtual Float_t GetDX() const {
         return fDX; }
      /** Returns a the length of the data array.
          @memo Get N method.
          @return Length
       ******************************************************************/
      virtual Int_t GetN() const {
         if (fNoZero && (fX == 0)) GetData();
         return fN; }
      /** Returns a pointer to the X data array.
          @memo Get X method.
          @return X array
       ******************************************************************/
      virtual Float_t* GetX() const {
         if (fX == 0) GetData();
         return fX; }
      /** Returns a pointer to the Y data array.
          @memo Get Y method.
          @return Y array
       ******************************************************************/
      virtual Float_t* GetY() const {
         if (fY == 0) GetData();
         return fY; }
   
      /** Erases the local data.
          @memo Erase data method.
          @return true if successful
       ******************************************************************/
      virtual bool EraseData();
   
   };

}

namespace diag {



/** This structure describes an auxiliary data object
   
    @memo Object for describing diagnostics data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class AuxDataDesc {
   public:
      /// aux state type
      enum auxstate_type {
      kUnknown = 0,
      kNew,
      kModified,
      kUnchanged,
      kDeleted
      };
   
      /// Default constructor
      AuxDataDesc ();
      // Constructor
      AuxDataDesc (const char* graph, const char* achn, 
                  const char* bchn = 0);
      /// equal
      bool operator== (const AuxDataDesc& aux) const;
      /// smaller
      bool operator< (const AuxDataDesc& aux) const;
   
      /// Graph type
      TString		fGraph;
      /// A channel
      TString		fAChn;
      /// B channel
      TString		fBChn;
      /// Name of data object
      mutable TString	fName;
      /// Index
      mutable Int_t	fIndex;
      /// State
      mutable auxstate_type	fState;
   };

   /// List of auxiliary data object descriptors
   typedef std::set<AuxDataDesc> AuxDataDescList;


/** A routine which adds plot descriptors to a plot set. The routine
    uses the index provided by the diagnostics test kernel to determine
    what to add.
   
    @memo Add plots.
    @param pset Plot set
    @param cmd Command line interpreter
    @param caltable Calibration table
    @param stepnum Measurement step to be added (-1 for all)
    @param suppressStep If true the measurement step index is suppressed
    @param setCurrent If true sets the current measurement result
    @return true if successful
 ************************************************************************/
   bool AddDataFromIndex (PlotSet& pset, basic_commandline& cmd, 
                     calibration::Table* caltable = 0,
                     int stepnum = -1, bool suppressStep = true, 
                     bool setCurrent = true);


/** A routine which adds plot descriptors to a plot set. The routine
    uses the index provided by the diagnostics test kernel to determine
    the entry which has to be added.
   
    @memo Add plots.
    @param pset Plot set
    @param cmd Command line interpreter
    @param entrynum Index entry number
    @param caltable Calibration table
    @param suppressStep If true the measurement step index is suppressed
    @param setCurrent If true sets the current measurement result
    @return true if successful
 ************************************************************************/
   bool AddDataFromIndexEntry (PlotSet& pset, 
                     basic_commandline& cmd, int entrynum,
                     calibration::Table* caltable = 0,
                     bool suppressStep = true,
                     bool setCurrent = true);

/** A routine which adds plot descriptors to a plot set. The routine
    uses the 'get rawdatanames' command to determine the time series 
    data objects to add.
   
    @memo Add plots.
    @param pset Plot set
    @param cmd Command line interpreter
    @param caltable Calibration table
    @return true if successful
 ************************************************************************/
   bool AddRawData (PlotSet& pset, basic_commandline& cmd,
                   calibration::Table* caltable = 0,
                   bool suppessStep = true);

/** A routine which adds plot descriptors to a plot set. The routine
    uses the 'get referencenames' command to determine which reference
    traces to add.
   
    @memo Add plots.
    @param pset Plot set
    @param cmd Command line interpreter
    @param caltable Calibration table
    @param ref Reference table list
    @return true if successful
 ************************************************************************/
   bool AddReferenceTraces (PlotSet& pset, basic_commandline& cmd,
                     calibration::Table* caltable = 0,
                     ligogui::ReferenceTraceList_t* ref = 0,
                     bool suppessStep = true);

/** A routine which adds plot descriptors to a plot set. The routine
    uses the 'get auxdatanames' command to determine which auxiliary
    traces to add.
   
    @memo Add plots.
    @param pset Plot set
    @param cmd Command line interpreter
    @param caltable Calibration table
    @param aux auxiliary data description
    @return true if successful
 ************************************************************************/
   bool AddAuxiliaryTraces (PlotSet& pset, basic_commandline& cmd,
                     calibration::Table* caltable = 0,
                     AuxDataDescList* aux = 0,
                     bool suppessStep = true);


//@}
}

#endif // _GDS_DIAGPLOT_H

