/* -*- mode: c++; c-basic-offset: 3; -*- */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  diagmain						     */
/*                                                                           */
/* Module Description:  main window of diagnostics tests		     */
/*                                                                           */
/*---------------------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <unistd.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <deque>
#include <string>
#include <cstdlib>
#include <strings.h>

#include <TGClient.h>
#include <TApplication.h>
#include <TVirtualX.h>
#include <TGListBox.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGComboBox.h>
#include <TGFileDialog.h>
#include <TSystem.h>
#include <TString.h>
#include <TEnv.h>

#include "gmutex.hh"
#include "PlotSet.hh"
#include "TLGEntry.hh"
#include "TLGColor.hh"
#include "TLGPad.hh"
#include "TLGExport.hh"
#include "TLGMath.hh"
#include "TLGCalDlg.hh"
#include "TLGSave.hh"
#include "TLGOptions.hh"
#include "TLGPrint.hh"
#include "TLGErrorDlg.hh"
#include "BugReportDlg.h"
#include "Xsil.hh"
#include "XsilStd.hh"
#include "Calibrations.hh"
//#include "gdsmain.h" - included from diagnames.h
#include "diagmain.hh"
#include "diagctrl.hh"
#include "cmdapi.hh"
#include "diagnames.h"
//#include "diagplot.hh" - included from diagmain.hh
#include "gdsstringcc.hh"
#include "tconv.h"
#include "gdserr.h" // JCB

namespace diag {
   using namespace std;
   using namespace ligogui;
   using namespace xml;
   using namespace thread;

   class NotificationList : public deque<string> {
   public: 
      thread::mutex	mux;
      NotificationList () {
      }
   };

   static const int my_debug = 0 ;
/* Constants */

   const Long_t kHeartbeatInterval = 100; // 100 ms
   const Long_t kXExitInterval = 120 * 1000; // 2 minutes

   GContext_t DiagMainWindow::fgButtonGC = 0;
   Cursor_t DiagMainWindow::fWaitCursor = (Cursor_t)-1;


   enum EMenuCommandIdentifiers {
   M_FILE_NEW = 100,  
   M_FILE_OPEN = 101,
   M_FILE_SAVE = 102,
   M_FILE_SAVEAS = 103,
   M_FILE_IMPORT = 104,
   M_FILE_EXPORT = 105,
   M_FILE_RFLAG_ALL = 106,
   M_FILE_RFLAG_STD = 107,
   M_FILE_RFLAG_PRM = 108,
   M_FILE_RFLAG_SET = 109,
   M_FILE_RFLAG_CAL = 110,
   M_FILE_SFLAG_ALL = 111,
   M_FILE_SFLAG_STD = 112,
   M_FILE_SFLAG_PRM = 113,
   M_FILE_SFLAG_SET = 114,
   M_FILE_SFLAG_CAL = 115,
   M_FILE_PRINT = 116,
   M_FILE_PRINTSETUP = 117,
   M_FILE_PRINT_GRAPHA = 118,
   M_FILE_PRINT_GRAPHB = 119,
   M_FILE_EXIT = 120,
   
   M_EDIT_START = 200,
   M_EDIT_PAUSE = 201,
   M_EDIT_RESUME = 202,
   M_EDIT_ABORT = 203,
   M_EDIT_READFROMTAPE = 207,
   
   M_MEAS_FFT = 300,
   M_MEAS_SWEPTSINE = 301,
   M_MEAS_SINERESP = 302,
   M_MEAS_TIMERESP = 303,
   M_MEAS_REPEAT = 304,
   M_MEAS_SCAN = 305,
   M_MEAS_OPTIMIZE = 306,
   M_MEAS_MESSAGES = 307,
   
   M_PLOT_RESET = 400,
   M_PLOT_REFERENCE = 401,
   M_PLOT_MATH = 402,
   M_PLOT_CALIBRATION_EDIT = 403,
   M_PLOT_CALIBRATION_READ = 404,
   M_PLOT_CALIBRATION_WRITE = 405,
   
   M_WINDOW_NEW = 500,
   M_WINDOW_ZOOM_OUT = 501,
   M_WINDOW_ZOOM_CUR = 502,
   M_WINDOW_ZOOM_A = 503,
   M_WINDOW_ZOOM_B = 504,
   M_WINDOW_ACTIVE_NEXT = 510,
   M_WINDOW_ACTIVE_A = 511,
   M_WINDOW_ACTIVE_B = 512,
   M_WINDOW_LAYOUT = 513,
   
   // M_UTIL_AWG = 400,
   // M_UTIL_TPMAN = 401,
   // M_UTIL_CMDLINE = 402,
   // M_UTIL_XMLREADER = 403,
   // M_UTIL_MATLAB = 404,
   // M_UTIL_MATHEMATICA = 405,
   
   M_HELP_CONTENTS = 900,
   M_HELP_SEARCH = 901,
   M_HELP_NOTES = 902 ,
   M_HELP_ABOUT = 903,
   M_HELP_BUG = 904
   };


   enum EButtonCommandIdentifiers {
   B_START = 1,
   B_ABORT = 2,
   B_PAUSE = 3,
   B_RESUME = 4
   };



   static const char* const gSaveAsTypes[] = { 
   "LIGO light weight", "*.xml",
   "All files", "*",
   0, 0 };


   static const char* const gOpenTypes[] = { 
   "LIGO light weight", "*.xml",
   "Script file", "*.cmd",
   "All files", "*",
   0, 0 };


   const TString aboutmsg = 
   "LIGO))) Laser Interferometer Gravitational-wave Observatory\n"
   "Global Diagnostics System\n\n"
   "by Daniel Sigg et al., 1998 - 2019, copyright\n"
   "version " CDS_VERSION "\n\n"
   "http://www.ligo.caltech.edu\n"
   "http://git.ligo.org/cds/dtt";


   const char* const cmdnotifymsgs[] = {
   "notification: invalid", 
   "notification: begin of test", 
   "notification: end of test", 
   "notification: begin of measurement", 
   "notification: end of measurement",
   "notification: new test result",
   "notification: new iterator result",
   "notification: data receiving error (measurement skipped)",
   "notification: test failure",
   0
   };

   
   const char *diaggui_changes[] = {
   "Change history for all cds-crtools software can be found at",
   "https://git.ligo.org/cds/dtt/wikis/ChangeHistory",
   "",
   "Version 3.1.0 February 2022",
   "- Use NDS2Client for input from NDS2 servers.",
   "- Checks that nyquist frequency high enough for Sine Response tests.",
   "- Some menu items did nothing.  They were removed.",
   "- Now detects if data will be read from tape drive.  A warning is displayed.",
   "",
   "Version 3.0.0 July 2021",
   "- Swept sine XML save files now save frequencies as double precision values. This is a breaking change with previous versions. These save files will crash older versions of diaggui if they are opened up. Old files can still be opened.",
   "- The option to print all graphs from the command line in diaggui now works as expected.",
   "",
   "Version CDS 0.4.0 March 2021",
   "- Runs using GDS 2.19.4 and ROOT 6.22/06",
   "",
   "Version CDS 0.3.0 September 2020",
   "- New Feature: Specify FFT Tools measurement length in seconds.",
   "- Fixed: XML save files now report correct number of averages for references created from a new measurement.",
   "- Fixed: Swept Sine envelopes can now be specified out of order.",
   "- Fixed: Switching away from User NDS input then back now retains the channel list.",
   "- Fixed: Python3 AWG bindings.",
   "- Project Location changed to git.ligo.org/cds/dtt",
   "- Other minor fixes",
   "",
   "Version CDS 0.2.0 January 2020",
   "",
   "- A discontinuity in the ramp down on swept sine when the excitation is ",
   "aborted has been fixed.  Issue #59",
   "",
   "- A \"--fom\" command line option has been added to diaggui for figures ",
   "of merit displays in the control room.  This option enables both --autostart",
   "and --stallmsg options (see below). Issue #56",
   "",
   "- \"-a\" or \"--autostart\" on the command line for diaggui will, ",
   "when a filename is passed on the commandline, switch to the graph tab,",
   "hide the graph controls and start execution of the test.",
   "",
   "- \"--stallmsg\" on the command line for diaggui will display an ",
   "easily visible message on a graph when the measurement is no longer",
   "running for any reason, including aborts, lost data streams, or simply",
   "that the measurement is finished.  Use this to alert the operators when",
   "measurements that are supposed to run continuously are no longer running.",
   "Issue #57",
   "",
   "- A link from the help menu will take you to a web page where you can",
   "report a bug.  Use this to report bugs in any control room app.  Issue #23",
   "",
   "- diaggui now starts faster. Issue #4",
   "",
   "Version 2.17.14 March 2017",
   "",
   "* Bugzilla 1076 - If NDSSERVER environment variable is not present, the",
   "  default nds1 servers will be searched for at \"nds:8088\" and \"nds1:8088\".",
   "",
   "Version 2.17.9-2 (branch) February 2017",
   "",
   "* Bugzilla 1072 - Add reduced data set channels to those obtained from",
   "  the NDS2 server.",
   "",
   "Version 2.17.9-1 (branch) January 2017",
   "",
   "* Update code for leap second of Descember 31, 2016",
   "",
   "* Bugzilla 1064 - The last character of unit strings for channels was",
   "  overwritten with a 0, leaving the channel units incomplete.",
   "",
   "* Bugzilla 376 - Allow Abort button to stop the collection of data",
   "  while waiting for data to become available.",
   "",
   "Version 2.17.9 (branch) October 2016",
   "",
   "* Bugzilla 1040 - If a reference is deleted using the reference dialog",
   "  box, leaving only one reference, the correct graph type, A, and B",
   "  channel is now displayed.",
   "",
   "Version 2.17.4-1 (branch) September 2016",
   "Version 2.17.10 (trunk) September 2016",
   "",
   "* Bugzilla 812 - Expand number of plot windows which may be saved to",
   "  32 from 9.  Plot pads were saved to .xml file, but the window layout",
   "  was not saved for any window beyond the 9th window.  Hopefully 32",
   "  windows will be adequate for plots",
   "",
   "* Bugzilla 1036 - The number of swept-sine measurement points is now",
   "  left unchanged when the user edits sweep points.",
   "",
   "* Bugzilla 1041 - Plot options can now switch between different layouts",
   "  of the same number of pads and the new layout will work.  This also",
   "  allows reading a horizonal layout of two pads from an xml file, which",
   "  would have been displayed in a vertical layout before fixing this bug.",
   "",
   "Version 2.17.4 (branch) August 2016",
   "Version 2.17.9 (trunk) September 2016",
   "",
   "* Bugzilla 897 - Ramp down swept sine excitation when Abort is clicked.",
   "",
   "* Bugzilla 936 - Ramp up excitation for all analysis. A ramp up time",
   "  can be specified for a test (separate from ramp down) which will be",
   "  applied to an excitation before measurements are started.  For swept",
   "  sine tests, the ramp up will also be applied between frequency changes",
   "  as it's possible to change amplitude at the time a frequency changes.",
   "",
   "* Bugzilla 952 - Remove 1 second limit on ramp when settling time",
   "  calculation is used for swept sine ramp up. For swept-sine tests using",
   "  mHz frequencies, a ramp time could be calculated much longer than one",
   "  second, but the code limited ramp time to one second.  Ramp is now the",
   "  longer of ramp up time or settling time.",
   "",
   "Version 2.17.1.3-2 (branch) July 2016",
   "",
   "* Bugzilla 961 - Allow swept sine analysis when user has specified",
   "  to use fewer number of measurment points than those defined in the",
   "  user edited measurment points.  If the sweep is \"Up\", the first",
   "  n points defined will be used starting with the first point.  If the",
   "  sweep is \"Down\", the last n points will be used in reverse order",
   "  starting with the last point.  The number of points to use will",
   "  automatically be set to the number defined when the user edits the",
   "  points to use and accepts the change.",
   "",
   "Version 2.17.1.3-1 (branch) January 2016",
   "", 
   "* Bugzilla 969 - Resolve manually entered channel names with channels",
   "  that are in the channel list for the source being used.  If any channel",
   "  is not found when the user presses the \"Start\" button, a dialog",
   "  appears telling the user which channels could not be found.  The",
   "  user is given the option of cancelling the measurement or continuing",
   "  without the invalid channels.",
   "",
   "Version 2.17.1.3 (branch) January 2016",
   "", 
   "* Bugzilla 446 - Add ability to specify GPS times for NDS2 channel",
   "  lists.  The user can specify an epoch start and stop time to send",
   "  to the NDS2 server for channel lists",
   "",
   "* Bugzilla 782 - Check for kerberos ticket before using NDS2 interface,",
   "  A warning popup is now presented to warn the user to check for a",
   "  kerberos ticket if authentication can't be made.",
   "",
   "* Bugzilla 925 - Back out short term fix implemented in 2.17.1.2,",
   "  replace with a more comprehensive fix which allows users to specify",
   "  a start and stop time for an NDS2 epoch.  This is translated into",
   "  a command \"set-epoch <start>-<stop>;\" which is sent before a ",
   "  channel list is requested, and causes NDS2 to send back only the ",
   "  valid channels for the time period. This solution reads existing",
   "  xml files.",
   "",
   "* Bugzilla 943 - Correct issue with cached channel lists which prevented",
   "  cached lists from being retrieved and used",
   "",
   "* Bugzilla 947 - Fix error in Channel Selection dialog that is used",
   "  with Lidax input (to read frame files).  The channel selection",
   "  input fields now present a proper hierarhical channel list.",
   "",
   "Version 2.17.1.2 October 2015",
   "",
   "* Bugzilla 925 - Short term fix implemented to address",
   "  access of channels for which the channel name represents",
   "  multiple data rates.  This practice forces a channel name",
   "  to represent multiple types of data, so to differentiate them",
   "  the sample rate is appended to the channel name.  This breaks",
   "  existing .xml files used for NDS2 access as the channel names ",
   "  are effectively all changed.  But with a little effort, the",
   "  correct data can be obtained.",
   "",
   "Version 2.17.1.1 June 2015",
   "",
   "* Merged branch changes to trunk.",
   "",
   "* Bugzilla 883, handle leap second of June 30, 2015",
   "",
   "Version branch/2.16.17.2-1 April 2015",
   "",
   "* Bugzilla 849, Ramp in xml templates not read properly is fixed.",
   "",
   "Version branch/2.16.17.2 April 2015",
   "",
   "* Bugzilla 829, printing plots to a file crashes for some plots.",
   "",
   "* Bugzilla 828, allow awgtpman servers to be found on multiple",
   "  networks with comma-separated list in LIGO_RT_BCAST environment",
   "  variable.",
   "",
   "Version branch/2.16.12.4, January 2015",
   "",
   "* Bugzilla 288, Ramp down excitations for Swept Sine transfer",
   "  functions.  Added widget to Swept Sine GUI to allow user to set",
   "  time of ramp down of excitation when transfer function is aborted",
   "  or test ends.  Similar to feature added in 2.16.12.3 for Fourier",
   "  Tools.",
   "",
   "Version 2.16.12.3, December 2014",
   "",
   "* Bugzilla 761, graph line width control changed to integer values",
   "  greater than 0.",
   "",
   "* Bugzilla 755, file filter for PNG files is incorrect in print",
   "  Save As dialog box.",
   "",
   "* Bugzilla 754, Plot legends have red backgrounds when compiled",
   "  using root-5.34.21.  Backgound of legend boxes now explicitly",
   "  set to white.",
   "",
   "* Bugzilla 288, Ramp down excitations for Fourier transfer functions.",
   "  Added widget to Fourier Tools GUI to allow user to set time of ramp",
   "  down of excitation when transfer function is aborted or test ends.",
   "  Note this does not ramp up excitation, just ramp down at the end.",
   "",
   "Version 2.16.12.2, Sept 2014",
   "* Fix bug that caused long channel names to be ignored in the ",
   "  Calibration dialog box.",
   "",
   "* Bugzilla 688, Add awg.py and awgbase.py to GDS for access to awg",
   "  functions via python.",
   "",
   "Version 2.16.12.1, June 2014",
   "",
   "* Bug fix to cure inability of diaggui to run for extended",
   "  periods of time making continuous measurements (Linux).",
   "  ",
   "* Added preliminary code to allow diag to read 32 bit unsigned",
   "  integers, but it's untested and diag won't do anything useful",
   "  with a 32 bit unsigned int.",
   "",
   "* Changed size of channel selection menus to make fields wider",
   "  to accomodate longer channel names.",
   "  ",
   "Version 2.16.12, March 2014",
   "",
   "* Bugzilla 638, Can't apply filters to some excitations has been",
   "  fixed.",
   "",
   "Version 2.16.9.1, February 2014",
   "",
   "* Bugzilla 254, Improper display of reference traces read from ",
   "  xml file has been fixed.",
   "",
   "Version 2.16.9, January 2014",
   "",
   "* Bugzilla 519, use environment variable LIGO_RT_BCAST to ",
   "  determine the broadcast address to use when discovering awg and",
   "  tp services.",
   "",
   "* Removed 'Kaiser' window option, as no code was ever written to ",
   "  support it.",
   "",
   "* Fixed bugzilla 299, 328, 389, 428, Improper dft results when using",
   "  certain combinations of start, stop, and BW values.",
   "",
   "* Updates to compile with gcc version 4.7.x",
   "",
   "Version 2.16.5, October 2012",
   "",
   "* Updated diag to use larger buffers so more than 128 tp and awg processes",
   "  could be used.  ",
   "",
   "* Added support for generating .png files for plots. (Bugzilla 420)",
   "",
   "* Added the ability to read release notes from the Help menu in diaggui.",
   "",
   "* Expand channel field width in fields and menus that display channel",
   "  names.",
   "",
   "* Add levels of hierarchy in display of channel names in selection menus.",
   "",
   "* Fixed bugzilla 325, segfault using --help option.",
   "",
   "Version 2.16.3, July 2012",
   "",
   "* Modified code for leap second of June 30, 2012.",
   (char *) NULL
   } ;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   string varname (const char* n1, const char* n2 = 0, 
                  int i1A = -1, int i1B = -1, int i2A = -1, int i2B = -1)
   {
      string s (n1);
      char indx[32];
      if (i1A >= 0) {
         sprintf (indx, "[%i]", i1A);
         s += indx;
      }
      if (i1B >= 0) {
         sprintf (indx, "[%i]", i1B);
         s += indx;
      }
      if (n2 != 0) {
         s += string (".") + n2; 
         if (i2A >= 0) {
            sprintf (indx, "[%i]", i2A);
            s += indx;
         }
         if (i2B >= 0) {
            sprintf (indx, "[%i]", i2B);
            s += indx;
         }
      }
      return s;
   }


   string varname (const string& n1, const string& n2, 
                  int i1A = -1, int i1B = -1, int i2A = -1, int i2B = -1)
   {
      return varname (n1.c_str(), n2.c_str(), i1A, i1B, i2A, i2B);
   }


   string testvar (const char* n2, int i2A = -1, int i2B = -1) 
   {
      return varname (stTestParameter, n2, -1, -1, i2A, i2B);
   }


   const Int_t DiagMainWindow::fStoreOptionsMax = 100;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ActionPlots       	                                                //
//                                                                      //
// ActionPlots                                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class ActionPlots : public TLGMultiPad::ActionPlotPads {
   protected:
      /// Reference to main window
      DiagMainWindow* 	fWin;
   
   public:
      /// Default constructor
      explicit ActionPlots (DiagMainWindow* w) : fWin (w) {
      }
      /// apply reset
      virtual bool Reset (const PlotSet& pset, TLGMultiPad& mpad);
      /// export
      virtual bool Export (ExportOption_t* defex,
                        const PlotSet& pset, TLGMultiPad& mpad);
   };

//______________________________________________________________________________
   bool ActionPlots::Reset (const PlotSet& pset, TLGMultiPad& mpad) 
   {
      fWin->ShowDefaultPlot (kTRUE, &mpad); 
      return true; 
   }

//______________________________________________________________________________
   bool ActionPlots::Export (ExportOption_t* defex,
                     const PlotSet& pset, TLGMultiPad& mpad) 
   {
      typedef vector<PlotDescriptor*> PlotDescList;
   
      // make plot decriptors for pad traces
      PlotSet traces;
      // get plot descriptors from embedded pads
      if (fWin->fMainCtrl->GetPad()) {
         fWin->fMainCtrl->GetPad()->Fill (traces, "win0");
      }
      // loop over multi pads
      const PlotSet::winlist* list = fWin->fPlot->GetRegisteredWindows();
      if (!list) {
         return kFALSE;
      }
      Int_t i = 2;
      for (PlotSet::winlist::const_iterator el = list->begin(); 
          el != list->end(); ++el, ++i) {
         TLGPadMain* w = dynamic_cast <TLGPadMain*> (*el);
         if (!w) {
            continue;
         }
         // get plot descriptors from window
         char winid[16];
         sprintf (winid, "win%i", i - 1);
         w->GetPads()->Fill (traces, winid);
      }
      PlotDescList pdlist;
      for (PlotSet::iterator i = traces.begin();
          i != traces.end(); ++i) {
         pdlist.push_back (&*i);
      }
      ((PlotSet&)pset).Merge (traces);
   
      // call export
      bool ret = TLGMultiPad::ActionPlotPads::Export (defex, pset, mpad);
   
      // cleanup plot decriptors for traces
      for (PlotDescList::iterator i = pdlist.begin();
          i != pdlist.end(); ++i) {
         delete *i;
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// DiagMainWindow      	                                                //
//                                                                      //
// DiagMainWindow                                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   DiagMainWindow::DiagMainWindow (const TGWindow *p, basic_commandline* cmdline,
                     Int_t meastype, const char* filename, bool autostart, bool enable_disconnect_message)
   : TGMainFrame (p, 10, 10), fCmdLine (cmdline), 
   fFileSaveFlag (1), fFileRestoreFlag (0), 
   fSettingsSaveFlag (kTRUE), fSettingsRestoreFlag (kTRUE),
   fCalibrationSaveFlag (kTRUE), fCalibrationRestoreFlag (kTRUE), fAutostart(autostart),
   fEnableDisconnectMessage(enable_disconnect_message)
   {
      if (fWaitCursor == (Cursor_t)-1) {
         fWaitCursor = gVirtualX->CreateCursor (kWatch);
      }
   
      // setup test parameters
      fParam = new TestParam_t();
      defaultTestParameters (*fParam, meastype);
   
      // create plot set
      fPlot = new PlotSet ();
      // create printer defaults
      fPrintDef = new TLGPrintParam();
      fPrintDef->fPageOrientation = 1;
      // create import options defaults
      fImportDef = new ImportOption_t;
      SetDefaultImportOptions (*fImportDef);
      // create export options defaults
      fExportDef = new ExportOption_t;
      SetDefaultExportOptions (*fExportDef);
      // create reference traces list
      fRefTraces = new ReferenceTraceList_t;
      SetDefaultReferenceTraces (*fRefTraces);
      // create math function table
      fMathTable = new MathTable_t;
      SetDefaultMathTable (*fMathTable);
      // create calibration table
      fCalTable = new calibration::Table;
      calibration::SetDefaultTable (*fCalTable);
      // initialize reset
      fAction = new ActionPlots (this);
      // initialize notification
      fNotifyMsgs = new NotificationList();
      // create option storage list
      fStoreOptions = new OptionAll_t*[fStoreOptionsMax];
      for (int i = 0; i < fStoreOptionsMax; i++) {
         fStoreOptions[i] = 0;
      }
   
      // Get channel lists
      int len;
      if (!fCmdLine->sendMessage ("get channels", fMeasChannelList, len)) {
         free (fMeasChannelList);
         fMeasChannelList = (char*) malloc (20);
	 if (!fMeasChannelList) /* JCB */
	 {
	    gdsDebug("DiagMainWindow constructor malloc(20) failed.") ;
	 }
         strcpy (fMeasChannelList, "invalid");
      }
      if (!fCmdLine->sendMessage ("awg channels", fExcChannelList, len)) {
         free (fExcChannelList);
         fExcChannelList = (char*) malloc (20);
	 if (!fExcChannelList) /* JCB */
	 {
	    gdsDebug("DiagMainWindow constructor malloc(20) (2) failed.") ;
	 }
         strcpy (fExcChannelList, "invalid");
      }
   
      // Setup graphics context
// JCB      FontStruct_t labelfont;
// JCB A FontStruct_t is defined in ROOT as a pointer to a font structure.
//     it is stored as an unsigned long (JGZ).
      FontStruct_t labelfont = 0 ;
      if (gClient) {
         labelfont = gClient->GetFontByName 
            (gEnv->GetValue
            ("Gui.NormalFont",
            "-adobe-helvetica-bold-r-*-*-14-*-*-*-*-*-iso8859-1"));
         if (fgButtonGC == 0) {
            GCValues_t   gval;
            gval.fMask = kGCForeground | kGCFont;
            gval.fFont = gVirtualX->GetFontHandle (labelfont);
            gval.fForeground = fgBlackPixel;
            //fgButtonGC = gVirtualX->CreateGC (gClient->GetRoot()->GetId(), &gval);
	    TGGC* gc = gClient->GetGC(&gval);
	    fgButtonGC = gc->GetGC();
         }
      }
   
      // Menu
      fMenuBarLayout = 
         new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           0, 0, 1, 1);
      fMenuBarItemLayout = 
         new TGLayoutHints (kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
      fMenuBarHelpLayout = 
         new TGLayoutHints (kLHintsTop | kLHintsRight);
      fMenuBar = new TGMenuBar (this, 1, 1, kHorizontalFrame | kRaisedFrame);
      AddFrame (fMenuBar, fMenuBarLayout);
      // Menu: File
      fMenuFile = new TGPopupMenu (fClient->GetRoot());
      fMenuFile->Associate (this);
      fMenuFile->AddEntry ("&New...", M_FILE_NEW);
      fMenuFile->AddEntry ("&Open...", M_FILE_OPEN);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Save", M_FILE_SAVE);
      fMenuFile->AddEntry ("Save &As...", M_FILE_SAVEAS);
      fMenuFile->AddEntry ("&Import...", M_FILE_IMPORT);
      fMenuFile->AddEntry ("&Export...", M_FILE_EXPORT);
      fMenuFile->AddSeparator();
      fMenuFileFlag[0] = new TGPopupMenu (fClient->GetRoot());
      fMenuFileFlag[0]->AddEntry ("Everything (include raw time series)", 
                           M_FILE_RFLAG_ALL);
      fMenuFileFlag[0]->AddEntry ("Results and Parameters", M_FILE_RFLAG_STD);
      fMenuFileFlag[0]->AddEntry ("Parameters only", M_FILE_RFLAG_PRM);
      fMenuFileFlag[0]->AddSeparator ();
      fMenuFileFlag[0]->AddEntry ("Plot settings", M_FILE_RFLAG_SET);
      fMenuFileFlag[0]->AddSeparator ();
      fMenuFileFlag[0]->AddEntry ("Calibration data", M_FILE_RFLAG_CAL);
      fMenuFile->AddPopup ("Restore Flag", fMenuFileFlag[0]);
      fMenuFileFlag[0]->CheckEntry (M_FILE_RFLAG_ALL);
      fMenuFileFlag[0]->CheckEntry (M_FILE_RFLAG_SET);
      fMenuFileFlag[0]->CheckEntry (M_FILE_RFLAG_CAL);
      fMenuFileFlag[1] = new TGPopupMenu (fClient->GetRoot());
      fMenuFileFlag[1]->AddEntry ("Everything (include raw time series)", 
                           M_FILE_SFLAG_ALL);
      fMenuFileFlag[1]->AddEntry ("Results and Parameters", M_FILE_SFLAG_STD);
      fMenuFileFlag[1]->AddEntry ("Parameters only", M_FILE_SFLAG_PRM);
      fMenuFileFlag[1]->AddSeparator ();
      fMenuFileFlag[1]->AddEntry ("Plot settings", M_FILE_SFLAG_SET);
      fMenuFileFlag[1]->AddSeparator ();
      fMenuFileFlag[1]->AddEntry ("Calibration data", M_FILE_SFLAG_CAL);
      fMenuFile->AddPopup ("Save Flag", fMenuFileFlag[1]);
      fMenuFileFlag[1]->CheckEntry (M_FILE_SFLAG_STD);
      fMenuFileFlag[1]->CheckEntry (M_FILE_SFLAG_SET);
      fMenuFileFlag[1]->CheckEntry (M_FILE_SFLAG_CAL);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Print...", M_FILE_PRINT);
      fMenuFile->AddEntry ("P&rint Setup...", M_FILE_PRINTSETUP);
      fMenuFilePrintGraph = new TGPopupMenu (fClient->GetRoot());
      fMenuFilePrintGraph->AddEntry ("A", M_FILE_PRINT_GRAPHA);
      fMenuFilePrintGraph->AddEntry ("B", M_FILE_PRINT_GRAPHB);
      fMenuFile->AddPopup ("Print &Graph", fMenuFilePrintGraph);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("E&xit", M_FILE_EXIT);
      fMenuBar->AddPopup ("&File", fMenuFile, fMenuBarItemLayout);
      // Menu: Edit
      fMenuEdit = new TGPopupMenu (fClient->GetRoot());
      fMenuEdit->Associate (this);
      fMenuEdit->AddEntry ("&Start", M_EDIT_START);
      fMenuEdit->AddEntry ("&Pause", M_EDIT_PAUSE);
      fMenuEdit->AddEntry ("&Resume", M_EDIT_RESUME);
      fMenuEdit->AddEntry ("&Abort", M_EDIT_ABORT);
      fMenuEdit->AddSeparator();
      fMenuEdit->AddSeparator();
      fMenuEdit->AddEntry ( "Read from &tape", M_EDIT_READFROMTAPE);
      update_read_from_tape();
      fMenuBar->AddPopup ("&Edit", fMenuEdit, fMenuBarItemLayout);
      // Menu: Measurement
      fMenuMeas = new TGPopupMenu (fClient->GetRoot());
      fMenuMeas->Associate (this);
      fMenuMeas->AddEntry ("&Fourier Test", M_MEAS_FFT);
      fMenuMeas->AddEntry ("&Swept Sine", M_MEAS_SWEPTSINE);
      fMenuMeas->AddEntry ("Sine &Response", M_MEAS_SINERESP);
      fMenuMeas->AddEntry ("&Triggered Time Response", M_MEAS_TIMERESP);
      fMenuMeas->AddSeparator();
      fMenuMeas->AddEntry ("Re&peat", M_MEAS_REPEAT);
      fMenuMeas->AddEntry ("S&can", M_MEAS_SCAN);
      fMenuMeas->AddEntry ("&Optimize", M_MEAS_OPTIMIZE);
      fMenuMeas->AddSeparator();
      fMenuMeas->AddEntry ("&Messages...", M_MEAS_MESSAGES);
      fMenuBar->AddPopup ("&Measurement", fMenuMeas, fMenuBarItemLayout);
      // Menu Plot
      fMenuPlot = new TGPopupMenu (fClient->GetRoot());
      fMenuPlot->Associate (this);
      fMenuPlot->AddEntry ("Re&set", M_PLOT_RESET);
      fMenuPlot->AddSeparator();
      fMenuPlot->AddEntry ("Re&ference...", M_PLOT_REFERENCE);
//      fMenuPlot->AddEntry ("&Math...", M_PLOT_MATH);
      fMenuPlot->AddSeparator();
      fMenuPlot->AddEntry ("Calibration &Edit...", M_PLOT_CALIBRATION_EDIT);
      fMenuPlot->AddEntry ("Calibration &Read...", M_PLOT_CALIBRATION_READ);
      fMenuPlot->AddEntry ("Calibration &Write...", M_PLOT_CALIBRATION_WRITE);
      fMenuBar->AddPopup ("&Plot", fMenuPlot, fMenuBarItemLayout);
      // Menu Window
      fMenuWindow = new TGPopupMenu (fClient->GetRoot());
      fMenuWindow->Associate (this);
      fMenuWindow->AddEntry ("&New", M_WINDOW_NEW);
      fMenuWindow->AddSeparator();
      fMenuWindowZoom = new TGPopupMenu (fClient->GetRoot());
      fMenuWindowZoom->AddEntry ("Out", M_WINDOW_ZOOM_OUT);
      fMenuWindowZoom->AddEntry ("Current", M_WINDOW_ZOOM_CUR);
      fMenuWindowZoom->AddEntry ("A", M_WINDOW_ZOOM_A);
      fMenuWindowZoom->AddEntry ("B", M_WINDOW_ZOOM_B);
      fMenuWindow->AddPopup ("&Zoom", fMenuWindowZoom);
      fMenuWindowActive = new TGPopupMenu (fClient->GetRoot());
      fMenuWindowActive->AddEntry ("Next", M_WINDOW_ACTIVE_NEXT);
      fMenuWindowActive->AddEntry ("A", M_WINDOW_ACTIVE_A);
      fMenuWindowActive->AddEntry ("B", M_WINDOW_ACTIVE_B);
      fMenuWindow->AddPopup ("&Active", fMenuWindowActive);
      fMenuWindow->AddSeparator();
      fMenuWindow->AddEntry ("&Layout...", M_WINDOW_LAYOUT);
      fMenuBar->AddPopup ("&Window", fMenuWindow, fMenuBarItemLayout);
      // Menu: Utilities
      // fMenuUtil = new TGPopupMenu (fClient->GetRoot());
      // fMenuUtil->Associate (this);
      // fMenuUtil->AddEntry ("&Arbitrary Waveform Generator", M_UTIL_AWG);
      // fMenuUtil->AddEntry ("&Test Point Manager", M_UTIL_TPMAN);
      // fMenuUtil->AddSeparator();
      // fMenuUtil->AddEntry ("&Command Line", M_UTIL_CMDLINE);
      // fMenuUtil->AddSeparator();
      // fMenuUtil->AddEntry ("&XML Reader", M_UTIL_XMLREADER);
      // fMenuUtil->AddSeparator();
      // fMenuUtil->AddEntry ("Mat&lab", M_UTIL_MATLAB);
      // fMenuUtil->AddEntry ("&Mathematica", M_UTIL_MATHEMATICA);
      // fMenuBar->AddPopup ("&Utilities", fMenuUtil, fMenuBarItemLayout);
      // Menu: Help
      fMenuHelp = new TGPopupMenu(fClient->GetRoot());
      fMenuHelp->Associate (this);
      fMenuHelp->AddEntry("Report a &Bug", M_HELP_BUG);
      fMenuHelp->AddEntry("Release Notes", M_HELP_NOTES) ;

      // Add the standard help menu items to the menu.
      //fMenuHelp->AddEntry("&Contents", M_HELP_CONTENTS);
      //fMenuHelp->AddEntry("&Search...", M_HELP_SEARCH);
      fMenuHelp->AddSeparator();
      fMenuHelp->AddEntry("&About", M_HELP_ABOUT);
      fMenuBar->AddPopup ("&Help", fMenuHelp, fMenuBarHelpLayout);
   
      // Status bar
      fStatus = new TGStatusBar (this, 100, 25);
      Int_t 	parts[4] = {70, 10, 14, 6};
      fStatus->SetParts (parts, 4);
      fStatusBarLayout = 
         new TGLayoutHints (kLHintsExpandX | kLHintsBottom, 0, 0, 0, 0);
      AddFrame (fStatus, fStatusBarLayout);
   
      // Main tab control
      fMainCtrlFrameLayout = 
         new TGLayoutHints (kLHintsExpandX | kLHintsExpandY, 0, 0, 0, 0);
      fMainCtrlLayout =
         new TGLayoutHints (kLHintsExpandX | kLHintsExpandY, 2, 2, 2, 2);
      fMainCtrlFrame = new TGHorizontalFrame (this, 10, 10, kSunkenFrame);
      AddFrame (fMainCtrlFrame, fMainCtrlFrameLayout);
      fMainCtrl = new DiagTabControl (fMainCtrlFrame, this, *fParam,
                           fPlot, fMeasChannelList, fExcChannelList);
      fMainCtrl->Associate (this);
      fMainCtrlFrame->AddFrame (fMainCtrl, fMainCtrlLayout);
      TLGMultiPad* pad = fMainCtrl->GetPad();
      if (pad != 0) {
         pad->SetDefPrintSetup (fPrintDef);
         pad->SetDefImportOpt (fImportDef);
         pad->SetDefExportOpt (fExportDef);
	 // Link DiagMainWindow::fRefTraces to TLGMultiPad::fRefTraces.
         pad->SetReferenceTraces (fRefTraces);
//         pad->SetMathTable (fMathTable);
         pad->SetCalibrationTable (fCalTable);
         pad->SetStoreOptionList (fStoreOptions, fStoreOptionsMax);
         pad->SetActionPlotPads (fAction);
      }
   
      // Buttons
      fButtonFrameLayout = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 0);
      fButtonLayout =
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 50, 50, 10, 6);
      fButtonFrame = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fButtonFrame, fButtonFrameLayout);
      fStart = new TGTextButton (fButtonFrame, "Start", B_START, 
                           fgButtonGC, labelfont);
      fStart->Associate (this);
      fStart->SetToolTipText ("Start a test");
      fButtonFrame->AddFrame (fStart, fButtonLayout);
      fPause = new TGTextButton (fButtonFrame, "Pause", B_PAUSE, 
                           fgButtonGC, labelfont);
      fPause->Associate (this);
      fPause->SetToolTipText ("Pause a test");
      fButtonFrame->AddFrame (fPause, fButtonLayout);
      fResume = new TGTextButton (fButtonFrame, "Resume", B_RESUME, 
                           fgButtonGC, labelfont);
      fResume->Associate (this);
      fResume->SetToolTipText ("Resume a test");
      fButtonFrame->AddFrame (fResume, fButtonLayout);
      fAbort = new TGTextButton (fButtonFrame, "Abort", B_ABORT, 
                           fgButtonGC, labelfont);
      fAbort->Associate (this);
      fAbort->SetToolTipText ("Abort a test");
      fButtonFrame->AddFrame (fAbort, fButtonLayout);
   
      // Intialize
      fState = (DiagState_t) -1;
      SetState (dsInactive);
   
      // show plots if necessary
      // if (UpdatePlot (-1, kFALSE)) {
         // ShowDefaultPlot();
      // }
   
      // Setup window
      fwindow_name = "CDS Diagnostic Test Tools";
      //SetWindowName ("Diagnostics test tools");
      SetWindowName(fwindow_name.c_str());
      MapSubwindows ();
      Resize (1100, 870);
      MapWindow ();
   
      // show plots if necessary
      RestoreCalibrationData();
      Bool_t hasdata = UpdatePlot (-1, kFALSE);
      if (filename && *filename) {
         TransferParameters (kTRUE);
         fMainCtrl->UpdateChannels();
         AddOtherData();
         ShowPlots();
         fPlot->Update();
         if (filename && (strlen (filename) > 0)) {
            fFilename = filename;
            TString n = fwindow_name + " - " + fFilename;
            SetWindowName (n);
            if (hasdata || autostart) fMainCtrl->SetTab (3);
         }
      }
      else {
         TransferParameters (kTRUE, kTRUE);
      }
   
      // initialze heartbeat timer
      fHeartbeat = new TTimer (this, kHeartbeatInterval, kTRUE);
      fHeartbeat->TurnOn();
      // initialze X Windows watchdog
      fXExitTimer = new TTimer (this, kXExitInterval, kTRUE);
      fXExitTimer->TurnOn();

      //start the test if configured to autostart and we have a good filename
      if(autostart && filename && *filename)
      {
          const PlotSet::winlist* list = fPlot->GetRegisteredWindows();
          if (list)
          {
              for (PlotSet::winlist::const_iterator w = list->begin();
                   w != list->end();
                   ++w )
              {
                  const TLGPadMain *ww = dynamic_cast<const TLGPadMain *>(*w);
                  if (!ww)
                  {
                      continue;
                  }
                  // save multipad layout
                  if (my_debug) cerr << "    pad layout = " << ww->GetPads()->GetPadLayout() << endl;
                  Int_t num_pads =  ww->GetPads()->GetPadNumber();
                  for(Int_t padnum = 0; padnum < num_pads; ++padnum)
                  {
                      ww->GetPads()->GetPad(padnum)->HidePanel(kTRUE);
                  }
              }
          }
          Int_t num_pads = fMainCtrl->GetPad()->GetPadNumber();
          for(Int_t padnum = 0; padnum < num_pads; ++padnum)
          {
              fMainCtrl->GetPad()->GetPad(padnum)->HidePanel(kTRUE);
          }
          ProcessButton(B_START, 0);
      }
   }


   DiagMainWindow::~DiagMainWindow()
   {
      // delete main tab
      delete fMainCtrl;
      delete fMainCtrlFrame;
      delete fMainCtrlLayout;
      delete fMainCtrlFrameLayout;
      // delete buttons
      delete fStart;
      delete fAbort;
      delete fPause;
      delete fResume;
      delete fButtonFrame;
      delete fButtonLayout;
      delete fButtonFrameLayout;
      // delete menu
      delete fMenuFileFlag[0];
      delete fMenuFileFlag[1];
      delete fMenuFilePrintGraph;
      delete fMenuFile;
      delete fMenuEdit;
      delete fMenuMeas;
      delete fMenuPlot;
      delete fMenuWindow;
      delete fMenuWindowZoom;
      delete fMenuWindowActive;
      delete fMenuHelp;
      delete fMenuBarLayout;
      delete fMenuBarItemLayout;
      delete fMenuBarHelpLayout;
      delete fMenuBar;
      // delete status bar
      delete fStatus;
      delete fStatusBarLayout;
      // delete parameters
      delete fParam;
      // delete channels
      free (fExcChannelList);
      free (fMeasChannelList);
      // delete notification
      delete fHeartbeat;
      delete fXExitTimer;
      delete fNotifyMsgs;
      fNotifyMsgs = 0;
      // delete plot set
      delete fPlot;
      // delete defaults
      delete fImportDef;
      delete fExportDef;
      delete fRefTraces;
      delete fMathTable;
      delete fCalTable;
      delete fAction;
      // delete options storage list
      for (int i = 0; i < fStoreOptionsMax; i++) {
         delete fStoreOptions[i];
      }
      delete [] fStoreOptions;
   }


   void DiagMainWindow::CloseWindow()
   {
      Int_t	ret = 0;
      new TGMsgBox (gClient->GetRoot(), this, "Exit", 
                   "Do you really want to quit?", kMBIconQuestion, 
                   kMBYes | kMBNo, &ret);
      if (ret == kMBYes) {
         if (IsRunning()) {
            fCmdLine->parse ("abort");
            timespec wait = {0, 500000000};
            nanosleep (&wait, 0);
         }
         fCmdLine->parse ("quit");
         TGMainFrame::CloseWindow();
         gApplication->Terminate(0);
      }
   }

   Bool_t DiagMainWindow::UpdatePlot (Int_t stepnum, Bool_t updatePads)
   {
      bool suppressStep = ((fParam->fIter.fIterType == 0) && 
                          (fParam->fIter.fRepeat <= 1));
      Bool_t ret = AddDataFromIndex (*fPlot, *fCmdLine, 
                           fCalTable, stepnum, suppressStep);
      if (updatePads) {
         fPlot->Update();
      }
      return ret;
   }


   Bool_t DiagMainWindow::GetTimes (unsigned long& start, 
                     unsigned long& duration)
   {
      start = 0;
      duration = 0;
      string s;
      if (!fCmdLine->getVar ("times", s)) {
         return kFALSE;
      }
      sscanf (s.c_str(), "%lu%lu", &start, &duration);
      return kTRUE;
   }


   Bool_t DiagMainWindow::AddOtherData ()
   {
      bool suppressStep = ((fParam->fIter.fIterType == 0) && 
                          (fParam->fIter.fRepeat <= 1));
      AddRawData (*fPlot, *fCmdLine, fCalTable, suppressStep);
      AddReferenceTraces (*fPlot, *fCmdLine, fCalTable, fRefTraces, suppressStep);
      AddAuxiliaryTraces (*fPlot, *fCmdLine, fCalTable, &fAuxList, suppressStep);
      return kTRUE;
   }

   inline bool 
   testPlotOpts(TLGMultiPad* pads, TLGMultiPad* mpads, int optid) {
      OptionAll_t* opt = pads->GetPlotOptions (optid);
      if (!opt) return false;
      if (mpads != 0) {
	 SetDefaultGraphicsOptions (*opt);
	 return true;
      }
      if (opt->fTraces.fGraphType.Length() == 0) return true;
      if (opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser) return true;
      return false;
   }

   void DiagMainWindow::ShowDefaultPlot (Bool_t updatePads, TLGMultiPad* mpads)
   {
      // Determine default graph
      TString graph[2];
      switch (fParam->fMeas.fMeasType) {
         case 0:
            graph[0] = kPTPowerSpectrum;
            graph[1] = kPTCoherence;
            break;
         case 1:
            graph[0] = kPTTransferFunction;
            graph[1] = kPTTransferFunction;
            break;
         case 2:
            graph[0] = kPTTransferCoefficients;
            graph[1] = kPTTransferCoefficients;
            break;
         case 3:
            graph[0] = kPTTimeSeries;
            graph[1] = kPTTimeSeries;
            break;
      }
   
      // Create default set
      const PlotDescriptor* pset[2][8];
      Int_t num[2]; num[0] = 0; num[1] = 0;
      for (PlotSet::iterator iter = fPlot->begin();
          iter != fPlot->end(); ++iter) {
         for (int i = 0; i < 2; i++) {
            if ((num[i] < 8) &&
               (graph[i] == iter->GetGraphType()) &&
               (strchr (iter->GetAChannel(), '[') == 0) &&
               (strchr (iter->GetAChannel(), '(') == 0)) {
               pset[i][num[i]++] = &*iter;
            }
         }
      }
      // Display curves
      TLGMultiPad* pads = mpads ? mpads : fMainCtrl->GetPad();
      if ((fParam->fMeas.fMeasType == 1) ||
         (fParam->fMeas.fMeasType == 2)) {
         if (num[0] > 0) {
	    if (testPlotOpts(pads, mpads, 0)) {
               pads->ShowMultiPlot (pset[0], num[0], graph[0], 0, 0, kFALSE);
            }
	    if (testPlotOpts(pads, mpads, 1)) {
               pads->ShowMultiPlot (pset[0], num[0], graph[0], 1, 1, kFALSE);
            }
         }
         // set manual x range for Bode Plot
         if (fParam->fMeas.fMeasType == 1) {
            for (int i = 0; i < 2; i++) {
               OptionAll_t* opt = pads->GetPlotOptions(i);
               if (opt != 0) {
                  opt->fRange.fRange[0] = kRangeManual;
                  opt->fRange.fRangeFrom[0] = 0.9 * fParam->fMeas.fStart;
                  opt->fRange.fRangeTo[0] = 1.1 * fParam->fMeas.fStop;
               }
            }
         }
      }
      else if (fParam->fMeas.fMeasType == 0) {
         if (num[0] > 0) {
	    if (testPlotOpts(pads, mpads, 0)) {
               pads->ShowMultiPlot (pset[0], num[0], graph[0], 0, 0, kFALSE);
            }
         }
         if (num[1] > 0) {
	    if (testPlotOpts(pads, mpads, 1)) {
               pads->ShowMultiPlot (pset[1], num[1], graph[1], 1, 0, kFALSE);
            }
         }
      }
      else if (fParam->fMeas.fMeasType == 3) {
	 if (testPlotOpts(pads, mpads, 0)) {
            pads->ShowMultiPlot (pset[0], num[0], graph[0], 0, 0, kFALSE);
         }
	 if (testPlotOpts(pads, mpads, 1)) {
            TLGPad* p = pads->GetPad(1);
            if (p != 0) {
               p->Configure (0, 0);
            }
         }
      }
   
      // Update windows if necessary
      if (updatePads) {
         fPlot->Update();
      }
   }


   Int_t DiagMainWindow::GetMeasurementType () const
   {
      // get test type
      string	s;
      fCmdLine->getVar (varname (stTestType), s);
      if (compareTestNames (s, fftname) == 0) {
         return 0;
      }
      else if (compareTestNames (s, sweptsinename) == 0) {
         return 1;
      }
      else if (compareTestNames (s, sineresponsename) == 0) {
         return 2;
      }
      else if (compareTestNames (s, timeseriesname) == 0) {
         return 3;
      }
      else {
         return -1;
      }
   }


   Bool_t DiagMainWindow::SetMeasurementType (Int_t type)
   {
      if ((type < 0) || (type >= 4)) {
         return kFALSE;
      }
      Int_t oldtype = GetMeasurementType();
      if (oldtype == type) {
         return kFALSE;
      }
      else {
         // set test type
         string	s;
         switch (type) {
            case 0:
            default: 
               {
                  s = fftname;
                  break;
               }
            case 1: 
               {
                  s = sweptsinename;
                  break;
               }
            case 2: 
               {
                  s = sineresponsename;
                  break;
               }
            case 3: 
               {
                  s = timeseriesname;
                  break;
               }
         }
         if (fCmdLine->putVar (varname (stTestType), s)) {
            ClearResults (kFALSE, kFALSE);
         }
      }
      return kTRUE;
   }


   Bool_t DiagMainWindow::ClearResults (Bool_t askfirst, Bool_t all)
   {
      if (fPlot->Empty()) {
         return kTRUE;
      }
      if (askfirst) {
         Int_t ret;
         string msg = "This will clear all results\n"
            "from the diagnostics test.\n"
            "Do you want to continue?";
         new TGMsgBox (gClient->GetRoot(), this, "Clear results", 
                      msg.c_str(), kMBIconQuestion, 
                      kMBYes | kMBNo, &ret);
         if (ret != kMBYes) {
            return kFALSE;
         }
      }
      fPlot->Clear (all);
      fPlot->Update();
      fCmdLine->parse ("del results");
      fCmdLine->parse ("del rawdata");
      if (all && fCalTable) {
         fCalTable->Clear();
         fCalTable->ClearChannels();
      }
      if (all && fRefTraces) SetDefaultReferenceTraces (*fRefTraces);
      if (all && fMathTable) SetDefaultMathTable (*fMathTable);
      fAuxList.clear(); 
      return kTRUE;
   }


   Bool_t DiagMainWindow::ReadParam ()
   {
      bool		b;
      string		s;

      if (my_debug) cerr << "DiagMainWindow::ReadParam ()" << endl ;
      // get data input source
      string source = stInputSourceOnline;
      fCmdLine->getVar (varname (stInputSource), source);
      stringcase isource (source.c_str());
      fParam->fData.fNDSName = "";
      fParam->fData.fNDSPort = 0;
      fParam->fData.fNDS2Name = "";
      fParam->fData.fNDS2Port = 0;
      // NDS2
      if (isource.substr (0, lnInputSourceSENDS) == stInputSourceSENDS) {
         fParam->fData.fOnline = true;
         fParam->fData.fUserNDS = DataSourceParam_t::kUserNDS2;
         string s = source;
         s.erase (0, lnInputSourceSENDS);
         while (!s.empty() && isspace (s[0])) s.erase (0, 1);
         string::size_type pos = s.find_first_of (" \t\n\f\r\v:");
         if (pos == string::npos) {
            fParam->fData.fNDS2Name = s.c_str();
         }
         else {
	    string es("epoch_start=") ;
	    string ee("epoch_end=") ;
            fParam->fData.fNDS2Name = s.substr (0, pos).c_str();
            pos = s.find (':');
            if (pos != string::npos) {
               fParam->fData.fNDS2Port = atoi (s.c_str() + pos + 1);
            }
	    // If there's a "?epoch_start=<value>" here, put the value in
	    // the fNDS2Epoch[0].  Otherwise, set it to 0
	    pos = s.find (es);
	    if (pos != string::npos) {
	       fParam->fData.fNDS2Epoch[0] = atoi (s.c_str() + pos + es.length()) ;
	    } else {
	       fParam->fData.fNDS2Epoch[0] = 0 ;
	    }
	    // If there's a "&epoch_end=<value>" here, put the value in
	    // the fNDS2Epoch[1].  Otherwise set it to the current time.
	    pos = s.find (ee) ;
	    if (pos != string::npos) {
	       fParam->fData.fNDS2Epoch[1] = atoi (s.c_str() + pos + ee.length()) ;
	    } else {
	       tainsec_t now = TAInow() ;
	       fParam->fData.fNDS2Epoch[1] = now / _ONESEC ; 
	    }
#if 0
	    pos = s.find (en) ;
	    if (pos != string::npos) {
	       string name ;
	       // The end of the epoch name might be a '&' or npos.
	       size_t en_end = s.find_first_of("&",pos+en.length()) ;
	       if (en_end == string::npos)
		  name = s.substr(pos+en.length()) ;
	       else
		  name = s.substr(pos+en.length(), en_end - (pos+en.length())) ;
	       cerr << "DiagMainWindow::ReadParam - epoch name = \"" << name << "\"" << endl ;
	       fParam->fData.fNDS2EpochName = name ;
	    }
#endif
	    //cerr << "DiagMainWindow::ReadParam() - epoch = " << fParam->fData.fNDS2Epoch[0] << " ," << fParam->fData.fNDS2Epoch[1] << endl ;
         }
      }
      // User NDS
      else if (isource.substr (0, lnInputSourceNDS) == stInputSourceNDS) {
         fParam->fData.fOnline = true;
         fParam->fData.fUserNDS = DataSourceParam_t::kUserNDS1;
         string s = source;
         s.erase (0, lnInputSourceNDS);
         while (!s.empty() && isspace (s[0])) s.erase (0, 1);
         string::size_type pos = s.find_first_of (" \t\n\f\r\v:");
         if (pos == string::npos) {
            fParam->fData.fNDSName = s.c_str();
         }
         else {
            fParam->fData.fNDSName = s.substr (0, pos).c_str();
            pos = s.find (':');
            if (pos != string::npos) {
               fParam->fData.fNDSPort = atoi (s.c_str() + pos + 1);
            }
         }
      }
      // lidax
      else if (isource == stInputSourceLidax) {
         fParam->fData.fOnline = false;
         fParam->fData.fUserNDS =  DataSourceParam_t::kNoNDS;
      }
      // online
      else {
         fParam->fData.fOnline = true;
         fParam->fData.fUserNDS = DataSourceParam_t::kNoNDS;
      }
      b = false;
      fCmdLine->getVar (varname (stDef, stDefReconnect), b);
      fParam->fData.fReconnect = b;
      // get lidax paramters
      dfm::selservers& sel = fParam->fData.fDacc.sel();
      sel.clear();
      for (int i = 0; i < kMaxLidaxServer; i++) {
         string server = "";
         if (!fCmdLine->getVar (varname (stLidax, stLidaxServer, -1, -1, i), 
                              server) || server.empty()) {
            continue;
         }
         // get corresponding UDN
         string udn = "";
         if (!fCmdLine->getVar (varname (stLidax, stLidaxUDN, -1, -1, i), udn)) {
            continue;
         }
         // get channel list
         fantom::channellist clist;
         int gap = 0;
         for (int j = 0; (j < kMaxLidaxChannel) && (gap < 20); ++j) {
            string chn = "";
            if (!fCmdLine->getVar (varname (stLidax, stLidaxChannel, 
                                           -1, -1, i, j), chn) || chn.empty()) {
               ++gap;
               continue;
            }
            gap = 0;
            double rate = 0;
            fCmdLine->getVar(varname(stLidax, stLidaxRate, -1, -1, i, j), 
			     rate);
            if (rate < 0) rate = 0;
            clist.push_back (fantom::channelentry (chn.c_str(), rate));
         }
         // set new server
         fParam->fData.fDacc.addServer (server, udn, clist);
      }
   
      // get test type
      fParam->fMeas.fMeasType = GetMeasurementType();
      if (fParam->fMeas.fMeasType < 0) {
         fParam->fMeas.fMeasType = 0;
      }
      // get test time
      fCmdLine->getTime (varname (stTestTime), fParam->fMeasTime[0], 
                        fParam->fMeasTime[1]);
   
      // get test comment
      fCmdLine->getVar (varname (stTestComment), s);
      fParam->fComment = s.c_str();
   
      // get syncronization time
      fCmdLine->getVar (varname (stSync, stSyncType), fParam->fSync.fType);
      fCmdLine->getTime (varname (stSync, stSyncStart), fParam->fSync.fStart[0],
                        fParam->fSync.fStart[1]);
      fCmdLine->getVar (varname (stSync, stSyncWait), fParam->fSync.fWait);
      fCmdLine->getVar (varname (stSync, stSyncSlowDown), fParam->fSync.fSlowDown);
   
      // get test parameters
      switch (fParam->fMeas.fMeasType) {
         // FFT
         case 0:
            {
               fCmdLine->getVar (testvar (fftStartFrequency), fParam->fMeas.fStart);
               fCmdLine->getVar (testvar (fftStopFrequency), fParam->fMeas.fStop);
               fCmdLine->getVar (testvar (fftBW), fParam->fMeas.fResolutionBW);
               fCmdLine->getVar (testvar (fftWindow), fParam->fMeas.fWindow);
               fCmdLine->getVar (testvar (fftOverlap), fParam->fMeas.fOverlap);
               fCmdLine->getVar (testvar (fftRemoveDC), b);
               fParam->fMeas.fRemoveDC = b;
               fCmdLine->getVar (testvar (fftAChannels), fParam->fMeas.fAChannels);
               fCmdLine->getVar (testvar (fftSettlingTime), fParam->fMeas.fTimeSettling);
               fCmdLine->getVar (testvar (fftRampDown), fParam->fMeas.fRampDownTime) ;
               fCmdLine->getVar (testvar (fftRampUp), fParam->fMeas.fRampUpTime) ;
               fCmdLine->getVar (testvar (fftAverages), fParam->fMeas.fAverages);
               fCmdLine->getVar (testvar (fftBurstNoiseQuietTime_s), fParam->fMeas.fBurstNoiseQuietTime_s);
               fCmdLine->getVar (testvar (fftAverageType), fParam->fMeas.fAverageType);
               break;
            }
         // swept sine
         case 1:
            {
               fCmdLine->getVar (testvar (ssStartFrequency), fParam->fMeas.fStart);
               fCmdLine->getVar (testvar (ssStopFrequency), fParam->fMeas.fStop);
               fCmdLine->getVar (testvar (ssNumberOfPoints), fParam->fMeas.fPoints);
               fCmdLine->getVar (testvar (ssAChannels), fParam->fMeas.fAChannels);
               fCmdLine->getVar (testvar (ssAverages), fParam->fMeas.fAverages);
               fCmdLine->getVar (testvar (ssDoublePrecFreq), fParam->fMeas.fDoublePrecFreq);
               fCmdLine->getVar (testvar (ssMeasurementTime), fParam->fMeas.fTimeMeas[0], 2);
               fCmdLine->getVar (testvar (ssSettlingTime), fParam->fMeas.fTimeSettling);
               fCmdLine->getVar (testvar (ssRampDown), fParam->fMeas.fRampDownTime) ;
	       fCmdLine->getVar (testvar (ssRampUp), fParam->fMeas.fRampUpTime) ;
               fCmdLine->getVar (testvar (ssSweepType), fParam->fMeas.fSweepType);
               float* data = 0;
               int datan;
               if (fCmdLine->getVar (testvar (ssSweepPoints), &data, datan) &&
                  data && (datan > 0)) {
                  float* x = new float [datan];
                  float* y = new float [datan];
                  if (x && y) {
                     if (fParam->fMeas.fSweepType == 2) {
                        for (int i = 0; i < datan; i++) {
                           x[i] = data[i];
                           y[i] = 0;
                        }
                     }
                     else {
                        datan /= 2;
                        for (int i = 0; i < datan; i++) {
                           x[i] = data[2*i];
                           y[i] = data[2*i+1];
                        }
                     }
                     fParam->fMeas.fSweepPoints.reset (
                                          new DataCopy (x, y, datan));
                  }
                  else {
                     fParam->fMeas.fSweepPoints.reset (0);
                  }
                  if (x) delete [] x;
                  if (y) delete [] y;
               }
               if (data) delete [] data;
               fCmdLine->getVar (testvar (ssSweepDirection), fParam->fMeas.fSweepDir);
               fCmdLine->getVar (testvar (ssHarmonicOrder), fParam->fMeas.fHarmonicOrder);
               fCmdLine->getVar (testvar (ssWindow), fParam->fMeas.fWindow);
               fCmdLine->getVar (testvar (ssFFTResult), b);
               fParam->fMeas.fPowerSpec = b;
               break;
            }
         // sine response
         case 2:
            {
               fCmdLine->getVar (testvar (srMeasurementTime), fParam->fMeas.fTimeMeas[0], 2);
               fCmdLine->getVar (testvar (srSettlingTime), fParam->fMeas.fTimeSettling);
               fCmdLine->getVar (testvar (srRampDown), fParam->fMeas.fRampDownTime) ;
	       fCmdLine->getVar (testvar (srRampUp), fParam->fMeas.fRampUpTime) ;
               fCmdLine->getVar (testvar (srAverages), fParam->fMeas.fAverages);
               fCmdLine->getVar (testvar (srAverageType), fParam->fMeas.fAverageType);
               fCmdLine->getVar (testvar (srHarmonicOrder), fParam->fMeas.fHarmonicOrder);
               fCmdLine->getVar (testvar (srWindow), fParam->fMeas.fWindow);
               fCmdLine->getVar (testvar (srFFTResult), b);
               fParam->fMeas.fPowerSpec = b;
               break;
            }
         // triggered time series
         case 3:
            {
               fCmdLine->getVar (testvar (tsMeasurementTime), fParam->fMeas.fTimeMeas[0]);
               fCmdLine->getVar (testvar (tsPreTriggerTime), fParam->fMeas.fTimePreTrig);
               fCmdLine->getVar (testvar (tsSettlingTime), fParam->fMeas.fTimeSettling);
               fCmdLine->getVar (testvar (tsRampDown), fParam->fMeas.fRampDownTime) ;
	       fCmdLine->getVar (testvar (tsRampUp), fParam->fMeas.fRampUpTime) ;
               fCmdLine->getVar (testvar (tsDeadTime), fParam->fMeas.fTimeDead);
               fCmdLine->getVar (testvar (tsBW), fParam->fMeas.fSignalBW);
               fCmdLine->getVar (testvar (tsIncludeStatistics), b);
               fParam->fMeas.fStatistics = b;
               fCmdLine->getVar (testvar (tsFilter), s);
               fParam->fMeas.fFilter = s.c_str();
               fCmdLine->getVar (testvar (tsAverages), fParam->fMeas.fAverages);
               fCmdLine->getVar (testvar (tsBurstNoiseQuietTime_s), fParam->fMeas.fBurstNoiseQuietTime_s);
               fCmdLine->getVar (testvar (tsAverageType), fParam->fMeas.fAverageType);
               break;
            }
      }
      // get measurement channels
      if (my_debug) cerr << "DiagMainWindow::ReadParam() - get measurement channels" << endl ;
      for (int i = 0; i < kMaxMeasChannel; i++) {
         s = "";
         fCmdLine->getVar (testvar (stMeasurementChannel, i), s);
#ifdef TEST_RATE
         fParam->fMeas.fMeasChn[i].first = s.c_str();
	 if (s.length()) {
	    fCmdLine->getVar (testvar(stMeasurementChannelRate, i), fParam->fMeas.fMeasChn[i].second) ; 
	 } else {
	    // Avoid issuing a command if the name is blank.
	    fParam->fMeas.fMeasChn[i].second = 0 ;
	 }
	 if (my_debug) cerr << "fMeas.fMeasChn["<<i<<"].first = " << fParam->fMeas.fMeasChn[i].first << endl ;
	 if (my_debug) cerr << "fMeas.fMeasChn["<<i<<"].second = " << fParam->fMeas.fMeasChn[i].second << endl ;
#else
         fParam->fMeas.fMeasChn[i] = s.c_str();
#endif
         if (fCmdLine->getVar (testvar (stMeasurementChannelActive, i), b)) {
            fParam->fMeas.fMeasActive[i] = b;
         }
         else {
            fParam->fMeas.fMeasActive[i] = !s.empty();
         }
         if ((fParam->fMeas.fMeasActive[i]) || (i < 5)) {
            fParam->fMeas.fMeasChnUsed[i] = kTRUE;
         }
      }
   
      // get excitation
      for (int i = 0; i < kMaxExcChannel; i++) {
         int k = (fParam->fMeas.fMeasType == 1) ? -1 : i;
         // excitation name
         s = "";
         // excitation active
         if (k != -1) {
            fCmdLine->getVar (testvar (fftStimulusChannel, i), s);
            fParam->fExc[i].fName = s.c_str();
            if (fCmdLine->getVar (testvar (stStimulusActive, i), b)) {
               fParam->fExc[i].fActive = b;
            }
            else {
               fParam->fExc[i].fActive = !s.empty();
            }
         }
         else {
            if (i == 0) {
               fCmdLine->getVar (testvar (fftStimulusChannel), s);
               fParam->fExc[i].fName = s.c_str();
               fParam->fExc[i].fActive = kTRUE;
            }
            else {
               fParam->fExc[i].fName = "";
               fParam->fExc[i].fActive = kFALSE;
            }
         }
         if ((fParam->fExc[i].fActive) || (i < 5)) {
            fParam->fExc[i].fUsed = kTRUE;
         }
         // readback
         s = "";
         if ((k != -1) || (i == 0)) {
            fCmdLine->getVar (testvar (stStimulusReadback, k), s);
         }
         if (s.empty()) {
            fParam->fExc[i].fRBType = 0;
            fParam->fExc[i].fRBName = "";
         }
         else if (compareTestNames (s, "!") == 0) {
            fParam->fExc[i].fRBType = 1;
            fParam->fExc[i].fRBName = "";
         }
         else {
            fParam->fExc[i].fRBType = 2;
            fParam->fExc[i].fRBName = s.c_str();
         }
         // amplitude
         fParam->fExc[i].fAmpl = 0;
         if ((k != -1) || (i == 0)) {
            fCmdLine->getVar (testvar (stStimulusAmplitude, k), 
                             fParam->fExc[i].fAmpl);
         }
         // freq, offs, phase
         fParam->fExc[i].fFreq = 0;
         fParam->fExc[i].fOffs = 0;
         fParam->fExc[i].fPhase = 0;
         if (k != -1) {	// not swept sine
            fCmdLine->getVar (testvar (fftStimulusFrequency, i), 
                             fParam->fExc[i].fFreq);
            fCmdLine->getVar (testvar (fftStimulusOffset, i), 
                             fParam->fExc[i].fOffs);
            fCmdLine->getVar (testvar (fftStimulusPhase, i), 
                             fParam->fExc[i].fPhase);
         }
         // waveform, ratio, ranges, filter cmd
         fParam->fExc[i].fWaveform = 0;
         fParam->fExc[i].fRatio = 0.5;
         fParam->fExc[i].fFreqRange = 0;
         fParam->fExc[i].fAmplRange = 0;
         fParam->fExc[i].fFilterCmd = "";
         if ((fParam->fMeas.fMeasType == 0) ||	// FFT & trig. time series
            (fParam->fMeas.fMeasType == 3))  {
            fCmdLine->getVar (testvar (fftStimulusType, i), 
                             fParam->fExc[i].fWaveform);
            fCmdLine->getVar (testvar (fftStimulusRatio, i), 
                             fParam->fExc[i].fRatio);
            fCmdLine->getVar (testvar (fftStimulusFrequencyRange, i), 
                             fParam->fExc[i].fFreqRange);
            fCmdLine->getVar (testvar (fftStimulusAmplitudeRange, i), 
                             fParam->fExc[i].fAmplRange);
            fCmdLine->getVar (testvar (fftStimulusFilterCmd, i), s);
            fParam->fExc[i].fFilterCmd = s.c_str();
         }
         else {
            fParam->fExc[i].fWaveform = 1;
         }
      }
   
      // get iterator type
      fCmdLine->getVar (varname (stTestIterator), s);
      if (compareTestNames (s, repeatIteratorName) == 0) {
         fParam->fIter.fIterType = 0;
      }
      else if (compareTestNames (s, sweepIteratorName) == 0) {
         fParam->fIter.fIterType = 1;
      }
      else if (compareTestNames (s, findIteratorName) == 0) {
         fParam->fIter.fIterType = 2;
      }
      else {
         fParam->fIter.fIterType = 0;
      }
      // get iterator parameters
      fCmdLine->getVar (varname (stSync, stSyncRepeat), fParam->fIter.fRepeat);
   
      if (my_debug) cerr << "DiagMainWindow::ReadParam () - return" << endl ;
      return kTRUE;
   }


   static void putLidaxServer (basic_commandline* fCmdLine, 
                     const dfm::selserverentry& e, int count)
   {
      // server name
      string		s;
      s = e.getName();
      // UDN
      string		u;
      if (e.getUDN().begin() != e.getUDN().end()) {
         u = e.getUDN().begin()->first;
      }
      // Set if not empty
      if (s.empty() || u.empty()) {
         return;
      }
      fCmdLine->putVar (varname (stLidax, stLidaxServer, -1, -1, count), s);
      fCmdLine->putVar (varname (stLidax, stLidaxUDN, -1, -1, count), u);
      // channels
      int cnum = 0;
      for (fantom::const_chniter c = e.channels().begin(); 
          c != e.channels().end(); ++c, ++cnum) {
         s = c->Name();
         fCmdLine->putVar (varname (stLidax, stLidaxChannel, -1, -1, 
                                   count, cnum), s);
         double rate = c->Rate();
         if (rate > 0) {
            fCmdLine->putVar (varname (stLidax, stLidaxRate, -1, -1, 
                                      count, cnum), rate);
         }
      }
   }


   Bool_t DiagMainWindow::WriteParam ()
   {
      bool		b;
      string		s;
   
      // set data input source
      string source = stInputSourceOnline;
      if (!fParam->fData.fOnline) {
         source = stInputSourceLidax;
      }
      else if (fParam->fData.fUserNDS != DataSourceParam_t::kNoNDS) {
	 ostringstream sstr;
	 if (fParam->fData.fUserNDS == DataSourceParam_t::kUserNDS1) {
	    sstr << stInputSourceNDS;
	    sstr << " " << fParam->fData.fNDSName;
	    if (fParam->fData.fNDSPort > 0) sstr << ":" << fParam->fData.fNDSPort;
	    source = sstr.str();
	 } else {
	    sstr << stInputSourceSENDS;
	    sstr << " " << fParam->fData.fNDS2Name;
	    if (fParam->fData.fNDS2Port > 0) sstr << ":" << fParam->fData.fNDS2Port;
	    // Probably want to append the epoch start and stop here.
	    sstr << "?epoch_start=" << fParam->fData.fNDS2Epoch[0] ;
	    sstr << "&epoch_end=" << fParam->fData.fNDS2Epoch[1] ;
	    source = sstr.str();
	 }
      }
      //cerr << "WriteParam:: source = " << source << endl;
      fCmdLine->putVar (varname (stInputSource), source);
      b = fParam->fData.fReconnect;
      fCmdLine->putVar (varname (stDef, stDefReconnect), b);
      // delete data objects first
      fCmdLine->parse (string ("del ") + varname (stLidax));
      // put lidax paramters
      dfm::selservers& sel = fParam->fData.fDacc.sel();
      int count = 0;
      if (sel.isMultiple()) {
         for (dfm::const_selserveriter i = sel.begin(); 
             i != sel.end(); ++i) {
            putLidaxServer (fCmdLine, *i, count++);
         }
      }
      else {
         putLidaxServer (fCmdLine, sel.selectedS(), count++);
      }
      // fCmdLine->getVar ("*", s);
      // cerr << s << endl;
   
   
      // set test type
      SetMeasurementType (fParam->fMeas.fMeasType);
      // set comment
      s = fParam->fComment;
      fCmdLine->putVar (varname (stTestComment), s);
      // set start time
      fCmdLine->putVar (varname (stSync, stSyncType), fParam->fSync.fType);
      fCmdLine->putTime (varname (stSync, stSyncStart), 
                        fParam->fSync.fStart[0], fParam->fSync.fStart[1]);
      fCmdLine->putVar (varname (stSync, stSyncWait), fParam->fSync.fWait);
      fCmdLine->putVar (varname (stSync, stSyncSlowDown), fParam->fSync.fSlowDown);
   
      // set test parameters
      switch (fParam->fMeas.fMeasType) {
         // FFT
         case 0:
         default:
            {
               fCmdLine->putVar (testvar (fftStartFrequency), fParam->fMeas.fStart);
               fCmdLine->putVar (testvar (fftStopFrequency), fParam->fMeas.fStop);
               fCmdLine->putVar (testvar (fftBW), fParam->fMeas.fResolutionBW);
               fCmdLine->putVar (testvar (fftWindow), fParam->fMeas.fWindow);
               b = fParam->fMeas.fRemoveDC;
               fCmdLine->putVar (testvar (fftRemoveDC), b);
               fCmdLine->putVar (testvar (fftOverlap), fParam->fMeas.fOverlap);
               fCmdLine->putVar (testvar (fftAChannels), fParam->fMeas.fAChannels);
               fCmdLine->putVar (testvar (fftSettlingTime), fParam->fMeas.fTimeSettling);
               fCmdLine->putVar (testvar (fftRampDown), fParam->fMeas.fRampDownTime) ;
	           fCmdLine->putVar (testvar (fftRampUp), fParam->fMeas.fRampUpTime) ;
               fCmdLine->putVar (testvar (fftAverages), fParam->fMeas.fAverages);
               fCmdLine->putVar (testvar (fftBurstNoiseQuietTime_s), fParam->fMeas.fBurstNoiseQuietTime_s);
               fCmdLine->putVar (testvar (fftAverageType), fParam->fMeas.fAverageType);
               break;
            }
         // swept sine
         case 1:
            {
               fCmdLine->putVar (testvar (ssStartFrequency), fParam->fMeas.fStart);
               fCmdLine->putVar (testvar (ssStopFrequency), fParam->fMeas.fStop);
               fCmdLine->putVar (testvar (ssNumberOfPoints), fParam->fMeas.fPoints);
               fCmdLine->putVar (testvar (ssAChannels), fParam->fMeas.fAChannels);
               fCmdLine->putVar (testvar (ssAverages), fParam->fMeas.fAverages);
               fCmdLine->putVar (testvar (ssDoublePrecFreq), fParam->fMeas.fDoublePrecFreq);
               fCmdLine->putVar (testvar (ssMeasurementTime), fParam->fMeas.fTimeMeas, 2);
               fCmdLine->putVar (testvar (ssSettlingTime), fParam->fMeas.fTimeSettling);
               fCmdLine->putVar (testvar (ssRampDown), fParam->fMeas.fRampDownTime) ;
               fCmdLine->putVar (testvar (ssRampUp), fParam->fMeas.fRampUpTime) ;
               fCmdLine->putVar (testvar (ssSweepType), fParam->fMeas.fSweepType);
               if (fParam->fMeas.fSweepPoints.get() != 0) {
                  if (fParam->fMeas.fSweepType == 2) {
                     fCmdLine->putVar (testvar (ssSweepPoints), 
                                      fParam->fMeas.fSweepPoints->GetX(), 
                                      fParam->fMeas.fSweepPoints->GetN());
                  }
                  else {
                     int len = fParam->fMeas.fSweepPoints->GetN();
                     float* data = new float [2 * len];
                     if (data && fParam->fMeas.fSweepPoints->GetX() &&
                        fParam->fMeas.fSweepPoints->GetY()) {
                        for (int i = 0; i < len; i++) {
                           data[2*i] = fParam->fMeas.fSweepPoints->GetX()[i];
                           data[2*i+1] = fParam->fMeas.fSweepPoints->GetY()[i];
                        }
                        fCmdLine->putVar (testvar (ssSweepPoints), 
                                         data, 2 * len);
                     }
                     delete [] data;
                  }
               }
               fCmdLine->putVar (testvar (ssSweepDirection), fParam->fMeas.fSweepDir);
               fCmdLine->putVar (testvar (ssHarmonicOrder), fParam->fMeas.fHarmonicOrder);
               fCmdLine->putVar (testvar (ssWindow), fParam->fMeas.fWindow);
               b = fParam->fMeas.fPowerSpec;
               fCmdLine->putVar (testvar (ssFFTResult), b);
               break;
            }
         // sine response
         case 2:
            {
               fCmdLine->putVar (testvar (srMeasurementTime), fParam->fMeas.fTimeMeas, 2);
               fCmdLine->putVar (testvar (srSettlingTime), fParam->fMeas.fTimeSettling);
               fCmdLine->putVar (testvar (srRampDown), fParam->fMeas.fRampDownTime) ;
               fCmdLine->putVar (testvar (srRampUp), fParam->fMeas.fRampUpTime) ;
               fCmdLine->putVar (testvar (srAverages), fParam->fMeas.fAverages);
               fCmdLine->putVar (testvar (srAverageType), fParam->fMeas.fAverageType);
               fCmdLine->putVar (testvar (srHarmonicOrder), fParam->fMeas.fHarmonicOrder);
               fCmdLine->putVar (testvar (srWindow), fParam->fMeas.fWindow);
               b = fParam->fMeas.fPowerSpec;
               fCmdLine->putVar (testvar (srFFTResult), b);
               break;
            }
         // triggered time series
         case 3:
            {
               fCmdLine->putVar (testvar (tsMeasurementTime), fParam->fMeas.fTimeMeas[0]);
               fCmdLine->putVar (testvar (tsPreTriggerTime), fParam->fMeas.fTimePreTrig);
               fCmdLine->putVar (testvar (tsSettlingTime), fParam->fMeas.fTimeSettling);
               fCmdLine->putVar (testvar (tsRampDown), fParam->fMeas.fRampDownTime) ;
               fCmdLine->putVar (testvar (tsRampUp), fParam->fMeas.fRampUpTime) ;
               fCmdLine->putVar (testvar (tsDeadTime), fParam->fMeas.fTimeDead);
               fCmdLine->putVar (testvar (tsBW), fParam->fMeas.fSignalBW);
               b = fParam->fMeas.fStatistics;
               fCmdLine->putVar (testvar (tsIncludeStatistics), b);
               s = fParam->fMeas.fFilter;
               fCmdLine->putVar (testvar (tsFilter), s);
               fCmdLine->putVar (testvar (tsAverages), fParam->fMeas.fAverages);
               fCmdLine->putVar (testvar (tsBurstNoiseQuietTime_s), fParam->fMeas.fBurstNoiseQuietTime_s);
               fCmdLine->putVar (testvar (tsAverageType), fParam->fMeas.fAverageType);
               break;
            }
      }
   
      // set measurement channels
      for (int i = 0; i < kMaxMeasChannel; i++) {
         if (fParam->fMeas.fMeasActive[i]) {
            fParam->fMeas.fMeasChnUsed[i] = kTRUE;
         }
         if (!fParam->fMeas.fMeasChnUsed[i]) {
            continue;
         }
#ifdef TEST_RATE
         s = fParam->fMeas.fMeasChn[i].first ;
	 fCmdLine->putVar (testvar (stMeasurementChannelRate, i), fParam->fMeas.fMeasChn[i].second) ;
#else
         s = fParam->fMeas.fMeasChn[i];
#endif
         fCmdLine->putVar (testvar (stMeasurementChannel, i), s);
	 if (my_debug) cerr << "WriteParam() - writing measurement channel param for " << s << endl ;
#ifdef TEST_RATE
	 if (my_debug) cerr << "  data rate is " << fParam->fMeas.fMeasChn[i].second << endl ;
#endif
         b = fParam->fMeas.fMeasActive[i];
         fCmdLine->putVar (testvar (stMeasurementChannelActive, i), b);
      }
   
      // set excitation
      for (int i = 0; i < kMaxExcChannel; i++) {
         int k =  fParam->fMeas.fMeasType == 1 ? -1 : i;
         if (fParam->fExc[i].fActive) {
            fParam->fExc[i].fUsed = kTRUE;
         }
         if (!fParam->fExc[i].fUsed) {
            continue;
         }
         // excitation name
         s = fParam->fExc[i].fName;
         if ((k != -1) || (i == 0)) {
            fCmdLine->putVar (testvar (fftStimulusChannel, k), s);
         }
         // excitation active
         if (k != -1) {
            b = fParam->fExc[i].fActive;
            fCmdLine->putVar (testvar (stStimulusActive, i), b);
         }
         if ((fParam->fExc[i].fActive) || (i < 5)) {
            fParam->fExc[i].fUsed = kTRUE;
         }
         fprintf(stderr, "frbtype: %d\tfRBName: %s\n", fParam->fExc[i].fRBType, string(fParam->fExc[i].fRBName).c_str());
         // readback
         switch (fParam->fExc[i].fRBType) {
            case 0:
            default:
               {
                  s = "";
                  break;
               }
            case 1:
               {
                  s = "!";
                  break;
               }
            case 2:
               {
                  s = fParam->fExc[i].fRBName;
                  break;
               }
         }

         if ((k != -1) || (i == 0)) {
            fprintf(stderr, "rbname: %s\t\tk: %d\t\ti: %d", s.c_str(), k, i);
            fCmdLine->putVar (testvar (stStimulusReadback, k), s);
         }
         // amplitude
         if ((k != -1) || (i == 0)) {
            fCmdLine->putVar (testvar (stStimulusAmplitude, k),
                             fParam->fExc[i].fAmpl);
         }
         // swept sine done
         if (k == -1) {
            s = "";
            fCmdLine->putVar (testvar (fftStimulusFilterCmd, i), s);
            continue;
         }
         // freq, offs, phase
         fCmdLine->putVar (testvar (fftStimulusFrequency, i),
                          fParam->fExc[i].fFreq);
         fCmdLine->putVar (testvar (fftStimulusOffset, i),
                          fParam->fExc[i].fOffs);
         fCmdLine->putVar (testvar (fftStimulusPhase, i),
                          fParam->fExc[i].fPhase);
         // sine reponse done
         if (fParam->fMeas.fMeasType == 2) {
            s = "";
            fCmdLine->putVar (testvar (fftStimulusFilterCmd, i), s);
            continue;
         }
         // waveform, ratio, ranges
         fCmdLine->putVar (testvar (fftStimulusType, i),
                          fParam->fExc[i].fWaveform);
         fCmdLine->putVar (testvar (fftStimulusRatio, i),
                          fParam->fExc[i].fRatio);
         fCmdLine->putVar (testvar (fftStimulusFrequencyRange, i),
                          fParam->fExc[i].fFreqRange);
         fCmdLine->putVar (testvar (fftStimulusAmplitudeRange, i), 
                          fParam->fExc[i].fAmplRange);
         s = fParam->fExc[i].fFilterCmd;
         fCmdLine->putVar (testvar (fftStimulusFilterCmd, i), s);
      }
   
      // set iterator type
      switch (fParam->fIter.fIterType) {
         case 0:
         default:
            {
               s = repeatIteratorName;
               break;
            }
         case 1:
            {
               s = sweepIteratorName;
               break;
            }
         case 2:
            {
               s = findIteratorName;
               break;
            }
      }
      fCmdLine->putVar (varname (stTestIterator), s);
      // set iterator parameters
      fCmdLine->putVar (varname (stSync, stSyncRepeat), fParam->fIter.fRepeat);
   
      return kTRUE;
   }

   // Check the excitation and measurement channels that are selected, and
   // if any can't be found in the channel list, issue a warning and ask the
   // user if the test should continue.  Return 1 for continue, 0 to abandon 
   // the test.
   //
   // This function should be called after the Start button has been pressed.
   Bool_t DiagMainWindow::CheckChannels()
   {
      Bool_t ret = 1 ; // Assume it works.
      std::vector<std::string>  *bogus ;

      fMainCtrl->ResolveMeasurementChannels() ;
      bogus = fMainCtrl->GetBogusChannels() ;
      if (bogus && !bogus->empty())
      {
	 std::vector<std::string> localBogus = *bogus ;
	 std::vector<std::string>::iterator iter = localBogus.begin() ;
	 // Prepend a message to the list.
	 if (localBogus.size() == 1)
	    localBogus.insert(iter, string("The following channel could not be found:")) ;
	 else
	    localBogus.insert(iter, string("The following channels could not be found:")) ;
	 // Ask the user if they want to continue.
	 new TLGErrorDialog(gClient->GetRoot(), (TGWindow *) this,
			     &localBogus, 
			     string("Cancel"), string("Return to measurement screen"),
			     string("Continue"), string("Run measurement without bad channels"),
			     &ret, TGString("Inaccessible Channels")) ;
      }


      return ret ;
   }


   // With toGUI false, fill in the fParam structure from the GUI, then send
   // the values to the diag kernel with a series of commands.
   // With toGUI True, fill in the fParam structure with values from the diag
   // kernel with a series of commands, then set the GUI with the values from
   // the fParam structure.
   Bool_t DiagMainWindow::TransferParameters (Bool_t toGUI, Bool_t newparam) 
   {
      Bool_t ret = kTRUE;
      if (toGUI) {
         if (!newparam) {
	    // Fill in the fParam structure with the values from the diag kernel.
            ret = ReadParam ();
         }
	 // Transfer the values in the fParam structure to the GUI.
	 // This is DiagTabControl::SetMeasurement()
         fMainCtrl->SetMeasurement (fParam->fMeas.fMeasType, kFALSE, kTRUE);
	 // This is DiagTabControl::SetIterator()
         fMainCtrl->SetIterator (fParam->fIter.fIterType);
	 // This is DiagTabControl::TransferParameters(kTRUE)
         fMainCtrl->TransferParameters (toGUI);
      }
      else {
	 // This is DiagTabControl::TransferParameters(kFALSE)
	 // Transfer GUI values to the fParam structure.
         fMainCtrl->TransferParameters (toGUI);
	 // Send the values in the fParam structure to the diag kernel
         ret = WriteParam ();
      }
      return ret;
   }


   Bool_t DiagMainWindow::TransferPlotSettings (Bool_t toGUI, 
                     Int_t index1, Int_t index2, OptionAll_t* opt) 
   {
      string s;
      bool b;
      bool bvals[kMaxTraces];
      int ivals[kMaxTraces];
      double dvals[kMaxTraces];
      char plotname[128];
   
      if (my_debug) cerr << "DiagMainWindow::TransferPlotSettings(toGUI = " << (toGUI ? "TRUE" : "FALSE") << ", index1 = " << index1 << ", index2 = " << index1 << ",...)" << endl ;
      Bool_t ret = kTRUE;
      sprintf (plotname, "%s[%i][%i]", stPlot, index1, index2);
      if (toGUI) {
         // default & name
         SetDefaultGraphicsOptions (*opt);
         if (!fCmdLine->getVar (varname (plotname, stPlotName), s)) {
            return kFALSE;
         }
         opt->fName = s.c_str();
#ifdef STORE_OPTION_VIS
	 // Visibility of plot options panel
	 if (fCmdLine->getVar (varname (plotname, stPlotOptionVisible), b)) {
	    cerr << "TransferPlotSettings - getVar (stPlotOptionVisible) returned " <<
	       (b ? "true" : "false") << endl ;
	    opt->fOptionVisible = b ;
	 }
	 else
	 {
	    if (my_debug) cerr << "TransferPlotSettings - getVar (stPlotOptionVisible) failed" << endl ;
	 }
#endif
         // traces
         if (fCmdLine->getVar (varname (plotname, stPlotTracesGraphType), s)) {
            opt->fTraces.fGraphType = s.c_str();
         }
         for (int i = 0; i < kMaxTraces; i++) {
            if (fCmdLine->getVar (varname (plotname, stPlotTracesActive, 
                                          -1, -1, i), b)) {
               opt->fTraces.fActive[i] = b;
            }
            if (fCmdLine->getVar (varname (plotname, stPlotTracesAChannel, 
                                          -1, -1, i), s)) {
               opt->fTraces.fAChannel[i] = s.c_str();
            }
            if (fCmdLine->getVar (varname (plotname, stPlotTracesBChannel, 
                                          -1, -1, i), s)) {
               opt->fTraces.fBChannel[i] = s.c_str();
            }
         }
         if (fCmdLine->getVar (varname (plotname, stPlotTracesPlotStyle), 
                              ivals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fPlotStyle[i] = (EPlotStyle) ivals[i];
            }
         }
            // traces: line attributes
         if (fCmdLine->getVar (varname (plotname, stPlotTracesLineAttrColor), 
                              ivals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fLineAttr[i].SetLineColor (
                                    gPlotColorLookup().Add (ivals[i]));
               //opt->fTraces.fLineAttr[i].SetLineColor (ivals[i]);
            }
         }
         if (fCmdLine->getVar (varname (plotname, stPlotTracesLineAttrStyle), 
                              ivals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fLineAttr[i].SetLineStyle (ivals[i]);
            }
         }
         if (fCmdLine->getVar (varname (plotname, stPlotTracesLineAttrWidth), 
                              dvals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fLineAttr[i].SetLineWidth ((Width_t) dvals[i]);
            }
         }
            // traces: marker attributes
         if (fCmdLine->getVar (varname (plotname, stPlotTracesMarkerAttrColor), 
                              ivals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fMarkerAttr[i].SetMarkerColor (
                                    gPlotColorLookup().Add (ivals[i]));
            }
         }
         if (fCmdLine->getVar (varname (plotname, stPlotTracesMarkerAttrStyle), 
                              ivals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fMarkerAttr[i].SetMarkerStyle (ivals[i]);
            }
         }
         if (fCmdLine->getVar (varname (plotname, stPlotTracesMarkerAttrSize), 
                              dvals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fMarkerAttr[i].SetMarkerSize (dvals[i]);
            }
         }
            // traces: bar attributes
         if (fCmdLine->getVar (varname (plotname, stPlotTracesBarAttrColor), 
                              ivals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fBarAttr[i].SetFillColor (
                                    gPlotColorLookup().Add (ivals[i]));
            }
         }
         if (fCmdLine->getVar (varname (plotname, stPlotTracesBarAttrStyle), 
                              ivals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fBarAttr[i].SetFillStyle (ivals[i]);
            }
         }
         if (fCmdLine->getVar (varname (plotname, stPlotTracesBarAttrWidth), 
                              dvals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fTraces.fBarWidth[i] = dvals[i];
            }
         }
         // range
         if (fCmdLine->getVar (varname (plotname, stPlotRangeAxisScale), 
                              ivals[0], 2)) {
            opt->fRange.fAxisScale[0] = (EAxisScale) ivals[0];
            opt->fRange.fAxisScale[1] = (EAxisScale) ivals[1];
         }
         if (fCmdLine->getVar (varname (plotname, stPlotRangeRange), 
                              ivals[0], 2)) {
            opt->fRange.fRange[0] = (ERange) ivals[0];
            opt->fRange.fRange[1] = (ERange) ivals[1];
         }
         fCmdLine->getVar (varname (plotname, stPlotRangeRangeFrom), 
                          opt->fRange.fRangeFrom[0], 2);
         fCmdLine->getVar (varname (plotname, stPlotRangeRangeTo), 
                          opt->fRange.fRangeTo[0], 2);
         fCmdLine->getVar (varname (plotname, stPlotRangeBin), opt->fRange.fBin);
         if (fCmdLine->getVar (varname (plotname, stPlotRangeBinLogSpacing), b)) {
            opt->fRange.fBinLogSpacing = b;
         }
         // units
         if (fCmdLine->getVar (varname (plotname, stPlotUnitsXValues), ivals[0])) {
            opt->fUnits.fXValues = (EXRangeType) ivals[0];
         }
         if (fCmdLine->getVar (varname (plotname, stPlotUnitsYValues), ivals[0])) {
            opt->fUnits.fYValues = (EYRangeType) ivals[0];
         }
         if (fCmdLine->getVar (varname (plotname, stPlotUnitsXUnit), s)) {
            opt->fUnits.fXUnit = s.c_str();
         }
         if (fCmdLine->getVar (varname (plotname, stPlotUnitsYUnit), s)) {
            opt->fUnits.fYUnit = s.c_str();
         }
         fCmdLine->getVar (varname (plotname, stPlotUnitsXMag), 
                          opt->fUnits.fXMag);
         fCmdLine->getVar (varname (plotname, stPlotUnitsYMag), 
                          opt->fUnits.fYMag);
         fCmdLine->getVar (varname (plotname, stPlotUnitsXSlope), 
                          opt->fUnits.fXSlope);
         fCmdLine->getVar (varname (plotname, stPlotUnitsYSlope), 
                          opt->fUnits.fYSlope);
         fCmdLine->getVar (varname (plotname, stPlotUnitsXOffset), 
                          opt->fUnits.fXOffset);
         fCmdLine->getVar (varname (plotname, stPlotUnitsYOffset), 
                          opt->fUnits.fYOffset);
         // cursor
         if (fCmdLine->getVar (varname (plotname, stPlotCursorActive), 
                              bvals[0], 2)) {
            opt->fCursor.fActive[0] = bvals[0];
            opt->fCursor.fActive[1] = bvals[1];
         }
         fCmdLine->getVar (varname (plotname, stPlotCursorTrace), 
                          opt->fCursor.fTrace);
         if (fCmdLine->getVar (varname (plotname, stPlotCursorStyle), ivals[0])) {
            opt->fCursor.fStyle = (ECursorStyle) ivals[0];
         }
         if (fCmdLine->getVar (varname (plotname, stPlotCursorType), ivals[0])) {
            opt->fCursor.fType = (ECursorType) ivals[0];
         }
         fCmdLine->getVar (varname (plotname, stPlotCursorX), 
                          opt->fCursor.fX[0], 2);
         fCmdLine->getVar (varname (plotname, stPlotCursorH), 
                          opt->fCursor.fH[0], 2);
         if (fCmdLine->getVar (varname (plotname, stPlotCursorValid), 
                              bvals[0], kMaxTraces)) {
            for (int i = 0; i < kMaxTraces; i++) {
               opt->fCursor.fValid[i] = bvals[i];
            }
         }
         fCmdLine->getVar (varname (plotname, stPlotCursorN), 
                          opt->fCursor.fN[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorXDiff), 
                          opt->fCursor.fXDiff[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorYDiff), 
                          opt->fCursor.fYDiff[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorMean), 
                          opt->fCursor.fMean[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorRMS), 
                          opt->fCursor.fRMS[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorStdDev), 
                          opt->fCursor.fStdDev[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorSum), 
                          opt->fCursor.fSum[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorSqrSum), 
                          opt->fCursor.fSqrSum[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorArea), 
                          opt->fCursor.fArea[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorRMSArea), 
                          opt->fCursor.fRMSArea[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorPeakX), 
                          opt->fCursor.fPeakX[0], kMaxTraces);
         fCmdLine->getVar (varname (plotname, stPlotCursorPeakY), 
                          opt->fCursor.fPeakY[0], kMaxTraces);
         // config
         if (fCmdLine->getVar (varname (plotname, stPlotConfigAutoConfig), b)) {
            opt->fConfig.fAutoConf = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotConfigRespectUser), b)) {
            opt->fConfig.fRespectUser = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotConfigAutoAxes), b)) {
            opt->fConfig.fAutoAxes = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotConfigAutoBin), b)) {
            opt->fConfig.fAutoBin = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotConfigAutoTimeAdjust), b)) {
            opt->fConfig.fAutoTimeAdjust = b;
         }	
         // style
         if (fCmdLine->getVar (varname (plotname, stPlotStyleTitle), s)) {
            opt->fStyle.fTitle = s.c_str();
         }
         if (fCmdLine->getVar (varname (plotname, stPlotStyleTitleAlign), 
                              ivals[0])) {
            opt->fStyle.fTitleAttr.SetTextAlign (ivals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotStyleTitleAngle), 
                              dvals[0])) {
            opt->fStyle.fTitleAttr.SetTextAngle (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotStyleTitleColor), 
                              ivals[0])) {
            opt->fStyle.fTitleAttr.SetTextColor (
                                 gPlotColorLookup().Add (ivals[0]));
         }
         if (fCmdLine->getVar (varname (plotname, stPlotStyleTitleFont), 
                              ivals[0])) {
            opt->fStyle.fTitleAttr.SetTextFont (ivals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotStyleTitleSize), 
                              dvals[0])) {
            opt->fStyle.fTitleAttr.SetTextSize (dvals[0]);
         }
         fCmdLine->getVar (varname (plotname, stPlotStyleMargin), 
                          opt->fStyle.fMargin[0], 4);
         // X axis
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXTitle), s)) {
            opt->fAxisX.fAxisTitle = s.c_str();
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrAxisColor), 
                              ivals[0])) {
            opt->fAxisX.fAxisAttr.SetAxisColor (
                                 gPlotColorLookup().Add (ivals[0]));
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrLabelColor), 
                              ivals[0])) {
            opt->fAxisX.fAxisAttr.SetLabelColor (
                                 gPlotColorLookup().Add (ivals[0]));
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrLabelFont), 
                              ivals[0])) {
            opt->fAxisX.fAxisAttr.SetLabelFont (ivals[0]);
            opt->fAxisX.fAxisAttr.SetTitleFont (ivals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrLabelOffset), 
                              dvals[0])) {
            opt->fAxisX.fAxisAttr.SetLabelOffset (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrLabelSize), 
                              dvals[0])) {
            opt->fAxisX.fAxisAttr.SetLabelSize (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrNdividions), 
                              ivals[0])) {
            opt->fAxisX.fAxisAttr.SetNdivisions (ivals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrTickLength), 
                              dvals[0])) {
            opt->fAxisX.fAxisAttr.SetTickLength (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrTitleOffset), 
                              dvals[0])) {
            opt->fAxisX.fAxisAttr.SetTitleOffset (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrTitleSize),
                              dvals[0])) {
            opt->fAxisX.fAxisAttr.SetTitleSize (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXAxisAttrTitleColor), 
                              ivals[0])) {
            opt->fAxisX.fAxisAttr.SetTitleColor (
                                 gPlotColorLookup().Add (ivals[0]));
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXGrid), b)) {
            opt->fAxisX.fGrid = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXBothSides), b)) {
            opt->fAxisX.fBothSides = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisXCenterTitle), b)) {
            opt->fAxisX.fCenterTitle = b;
         }
         // Y axis
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYTitle), s)) {
            opt->fAxisY.fAxisTitle = s.c_str();
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrAxisColor), 
                              ivals[0])) {
            opt->fAxisY.fAxisAttr.SetAxisColor (
                                 gPlotColorLookup().Add (ivals[0]));
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrLabelColor), 
                              ivals[0])) {
            opt->fAxisY.fAxisAttr.SetLabelColor (
                                 gPlotColorLookup().Add (ivals[0]));
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrLabelFont), 
                              ivals[0])) {
            opt->fAxisY.fAxisAttr.SetLabelFont (ivals[0]);
            opt->fAxisY.fAxisAttr.SetTitleFont (ivals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrLabelOffset), 
                              dvals[0])) {
            opt->fAxisY.fAxisAttr.SetLabelOffset (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrLabelSize), 
                              dvals[0])) {
            opt->fAxisY.fAxisAttr.SetLabelSize (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrNdividions), 
                              ivals[0])) {
            opt->fAxisY.fAxisAttr.SetNdivisions (ivals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrTickLength), 
                              dvals[0])) {
            opt->fAxisY.fAxisAttr.SetTickLength (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrTitleOffset), 
                              dvals[0])) {
            opt->fAxisY.fAxisAttr.SetTitleOffset (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrTitleSize), 
                              dvals[0])) {
            opt->fAxisY.fAxisAttr.SetTitleSize (dvals[0]);
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYAxisAttrTitleColor), 
                              ivals[0])) {
            opt->fAxisY.fAxisAttr.SetTitleColor (
                                 gPlotColorLookup().Add (ivals[0]));
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYGrid), b)) {
            opt->fAxisY.fGrid = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYBothSides), b)) {
            opt->fAxisY.fBothSides = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotAxisYCenterTitle), b)) {
            opt->fAxisY.fCenterTitle = b;
         }
         // legend
         if (fCmdLine->getVar (varname (plotname, stPlotLegendShow), b)) {
            opt->fLegend.fShow = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotLegendPlacement), 
                              ivals[0])) {
            opt->fLegend.fPlacement = (ELegendPlacement) ivals[0];
         }
         fCmdLine->getVar (varname (plotname, stPlotLegendXAdjust), 
                          opt->fLegend.fXAdjust);
         fCmdLine->getVar (varname (plotname, stPlotLegendYAdjust), 
                          opt->fLegend.fYAdjust);
         if (fCmdLine->getVar (varname (plotname, stPlotLegendSymbolStyle), 
                              ivals[0])) {
            opt->fLegend.fSymbolStyle = (ELegendStyle) ivals[0];
         }
         if (fCmdLine->getVar (varname (plotname, stPlotLegendTextStyle), 
                              ivals[0])) {
            opt->fLegend.fTextStyle = (ELegendText) ivals[0];
         }
         fCmdLine->getVar (varname (plotname, stPlotLegendSize), 
                          opt->fLegend.fSize);
         for (int i = 0; i < kMaxTraces; i++) {
            if (fCmdLine->getVar (varname (plotname, stPlotLegendText, 
                                          -1, -1, i), s)) {
               opt->fLegend.fText[i] = s.c_str();
            }
         }
         // param
         if (fCmdLine->getVar (varname (plotname, stPlotParamShow), b)) {
            opt->fParam.fShow = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotParamT0), b)) {
            opt->fParam.fT0 = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotParamAvg), b)) {
            opt->fParam.fAvg = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotParamSpecial), b)) {
            opt->fParam.fSpecial = b;
         }
         if (fCmdLine->getVar (varname (plotname, stPlotParamTimeFormatUTC), b)) {
            opt->fParam.fTimeFormatUTC = b;
         }
         fCmdLine->getVar (varname (plotname, stPlotParamTextSize), 
                          opt->fParam.fTextSize);
         ret = kTRUE;
      }
      else {
         // default & name
         s = opt->fName;
         if (!fCmdLine->putVar (varname (plotname, stPlotName), s)) {
            return kFALSE;
         }
#ifdef STORE_OPTION_VIS
	 b = opt->fOptionVisible ;
	 fCmdLine->putVar (varname (plotname, stPlotOptionVisible), b) ;
#endif
         // traces
         s = opt->fTraces.fGraphType;
         fCmdLine->putVar (varname (plotname, stPlotTracesGraphType), s);
         for (int i = 0; i < kMaxTraces; i++) {
            fCmdLine->putVar (varname (plotname, stPlotTracesActive, -1, -1, i), 
                             (bool) opt->fTraces.fActive[i]);
            s = opt->fTraces.fAChannel[i];
            fCmdLine->putVar (varname (plotname, stPlotTracesAChannel, 
                                      -1, -1, i), s);
            s = opt->fTraces.fBChannel[i];
            fCmdLine->putVar (varname (plotname, stPlotTracesBChannel, 
                                      -1, -1, i), s);
         }
         for (int i = 0; i < kMaxTraces; i++) {
            ivals[i] = (int) opt->fTraces.fPlotStyle[i];
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesPlotStyle), ivals, 
                          kMaxTraces);
            // traces: line attributes
         for (int i = 0; i < kMaxTraces; i++) {
            ivals[i] = TPlotColorLookup::ColorType 
               (opt->fTraces.fLineAttr[i].GetLineColor()).Compatibility();
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesLineAttrColor), ivals,
                          kMaxTraces);
         for (int i = 0; i < kMaxTraces; i++) {
            ivals[i] = opt->fTraces.fLineAttr[i].GetLineStyle ();
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesLineAttrStyle), ivals,
                          kMaxTraces);
         for (int i = 0; i < kMaxTraces; i++) {
            dvals[i] = opt->fTraces.fLineAttr[i].GetLineWidth ();
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesLineAttrWidth), dvals,
                          kMaxTraces);
            // traces: marker attributes
         for (int i = 0; i < kMaxTraces; i++) {
            ivals[i] = TPlotColorLookup::ColorType
               (opt->fTraces.fMarkerAttr[i].GetMarkerColor()).Compatibility();
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesMarkerAttrColor), ivals,
                          kMaxTraces);
         for (int i = 0; i < kMaxTraces; i++) {
            ivals[i] = opt->fTraces.fMarkerAttr[i].GetMarkerStyle ();
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesMarkerAttrStyle), ivals,
                          kMaxTraces);
         for (int i = 0; i < kMaxTraces; i++) {
            dvals[i] = opt->fTraces.fMarkerAttr[i].GetMarkerSize ();
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesMarkerAttrSize), dvals,
                          kMaxTraces);
            // traces: bar attributes
         for (int i = 0; i < kMaxTraces; i++) {
            ivals[i] = TPlotColorLookup::ColorType
               (opt->fTraces.fBarAttr[i].GetFillColor()).Compatibility();
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesBarAttrColor), ivals,
                          kMaxTraces);
         for (int i = 0; i < kMaxTraces; i++) {
            ivals[i] = opt->fTraces.fBarAttr[i].GetFillStyle ();
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesBarAttrStyle), ivals,
                          kMaxTraces);
         for (int i = 0; i < kMaxTraces; i++) {
            dvals[i] = opt->fTraces.fBarWidth[i];
         }
         fCmdLine->putVar (varname (plotname, stPlotTracesBarAttrWidth), dvals,
                          kMaxTraces);
         // range
         ivals[0] = (int) opt->fRange.fAxisScale[0];
         ivals[1] = (int) opt->fRange.fAxisScale[1];
         fCmdLine->putVar (varname (plotname, stPlotRangeAxisScale), ivals, 2);
         ivals[0] = (int) opt->fRange.fRange[0];
         ivals[1] = (int) opt->fRange.fRange[1];
         fCmdLine->putVar (varname (plotname, stPlotRangeRange), ivals, 2);
         fCmdLine->putVar (varname (plotname, stPlotRangeRangeFrom), 
                          opt->fRange.fRangeFrom, 2);
         fCmdLine->putVar (varname (plotname, stPlotRangeRangeTo), 
                          opt->fRange.fRangeTo, 2);
         fCmdLine->putVar (varname (plotname, stPlotRangeBin), opt->fRange.fBin);
         fCmdLine->putVar (varname (plotname, stPlotRangeBinLogSpacing), 
                          (bool) opt->fRange.fBinLogSpacing);
         // units
         fCmdLine->putVar (varname (plotname, stPlotUnitsXValues), 
                          (int) opt->fUnits.fXValues);
         fCmdLine->putVar (varname (plotname, stPlotUnitsYValues), 
                          (int) opt->fUnits.fYValues);
         s = opt->fUnits.fXUnit;
         fCmdLine->putVar (varname (plotname, stPlotUnitsXUnit), s);
         s = opt->fUnits.fYUnit;
         fCmdLine->putVar (varname (plotname, stPlotUnitsYUnit), s);
         fCmdLine->putVar (varname (plotname, stPlotUnitsXMag), 
                          opt->fUnits.fXMag);
         fCmdLine->putVar (varname (plotname, stPlotUnitsYMag), 
                          opt->fUnits.fYMag);
         fCmdLine->putVar (varname (plotname, stPlotUnitsXSlope), 
                          opt->fUnits.fXSlope);
         fCmdLine->putVar (varname (plotname, stPlotUnitsYSlope), 
                          opt->fUnits.fYSlope);
         fCmdLine->putVar (varname (plotname, stPlotUnitsXOffset), 
                          opt->fUnits.fXOffset);
         fCmdLine->putVar (varname (plotname, stPlotUnitsYOffset), 
                          opt->fUnits.fYOffset);
         // cursor
         bvals[0] = opt->fCursor.fActive[0];
         bvals[1] = opt->fCursor.fActive[1];
         fCmdLine->putVar (varname (plotname, stPlotCursorActive), bvals, 2);
         fCmdLine->putVar (varname (plotname, stPlotCursorTrace), 
                          opt->fCursor.fTrace);
         fCmdLine->putVar (varname (plotname, stPlotCursorStyle),
                          (int) opt->fCursor.fStyle);
         fCmdLine->putVar (varname (plotname, stPlotCursorType), 
                          (int) opt->fCursor.fType);
         fCmdLine->putVar (varname (plotname, stPlotCursorX), 
                          opt->fCursor.fX, 2);
         fCmdLine->putVar (varname (plotname, stPlotCursorH), 
                          opt->fCursor.fH, 2);
         for (int i = 0; i < kMaxTraces; i++) {
            bvals[i] = opt->fCursor.fValid[i];
         }
         fCmdLine->putVar (varname (plotname, stPlotCursorValid), bvals, 
                          kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorN), 
                          opt->fCursor.fN, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorXDiff), 
                          opt->fCursor.fXDiff, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorYDiff), 
                          opt->fCursor.fYDiff, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorMean), 
                          opt->fCursor.fMean, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorRMS), 
                          opt->fCursor.fRMS, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorStdDev), 
                          opt->fCursor.fStdDev, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorSum), 
                          opt->fCursor.fSum, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorSqrSum), 
                          opt->fCursor.fSqrSum, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorArea), 
                          opt->fCursor.fArea, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorRMSArea), 
                          opt->fCursor.fRMSArea, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorPeakX), 
                          opt->fCursor.fPeakX, kMaxTraces);
         fCmdLine->putVar (varname (plotname, stPlotCursorPeakY), 
                          opt->fCursor.fPeakY, kMaxTraces);
         // config
         fCmdLine->putVar (varname (plotname, stPlotConfigAutoConfig), 
                          (bool) opt->fConfig.fAutoConf);
         fCmdLine->putVar (varname (plotname, stPlotConfigRespectUser), 
                          (bool) opt->fConfig.fRespectUser);
         fCmdLine->putVar (varname (plotname, stPlotConfigAutoAxes), 
                          (bool) opt->fConfig.fAutoAxes);
         fCmdLine->putVar (varname (plotname, stPlotConfigAutoBin),
                          (bool) opt->fConfig.fAutoBin);
         fCmdLine->putVar (varname (plotname, stPlotConfigAutoTimeAdjust),
                          (bool) opt->fConfig.fAutoTimeAdjust);
         // style
         s = opt->fStyle.fTitle;
         fCmdLine->putVar (varname (plotname, stPlotStyleTitle), s);
         fCmdLine->putVar (varname (plotname, stPlotStyleTitleAlign), 
                          opt->fStyle.fTitleAttr.GetTextAlign ());
         fCmdLine->putVar (varname (plotname, stPlotStyleTitleAngle), 
                          opt->fStyle.fTitleAttr.GetTextAngle ());
         fCmdLine->putVar (varname (plotname, stPlotStyleTitleColor), 
                          TPlotColorLookup::ColorType
                          (opt->fStyle.fTitleAttr.GetTextColor()).Compatibility());
         fCmdLine->putVar (varname (plotname, stPlotStyleTitleFont), 
                          opt->fStyle.fTitleAttr.GetTextFont ());
         fCmdLine->putVar (varname (plotname, stPlotStyleTitleSize), 
                          opt->fStyle.fTitleAttr.GetTextSize ());
         fCmdLine->putVar (varname (plotname, stPlotStyleMargin), 
                          opt->fStyle.fMargin, 4);
         // X axis
         s = opt->fAxisX.fAxisTitle;
         fCmdLine->putVar (varname (plotname, stPlotAxisXTitle), s);
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrAxisColor), 
                          TPlotColorLookup::ColorType
                          (opt->fAxisX.fAxisAttr.GetAxisColor()).Compatibility());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrLabelColor), 
                          TPlotColorLookup::ColorType
                          (opt->fAxisX.fAxisAttr.GetLabelColor()).Compatibility());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrLabelFont), 
                          opt->fAxisX.fAxisAttr.GetLabelFont ());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrLabelOffset), 
                          opt->fAxisX.fAxisAttr.GetLabelOffset ());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrLabelSize), 
                          opt->fAxisX.fAxisAttr.GetLabelSize ());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrNdividions), 
                          opt->fAxisX.fAxisAttr.GetNdivisions ());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrTickLength), 
                          opt->fAxisX.fAxisAttr.GetTickLength ());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrTitleOffset), 
                          opt->fAxisX.fAxisAttr.GetTitleOffset ());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrTitleSize), 
                          opt->fAxisX.fAxisAttr.GetTitleSize ());
         fCmdLine->putVar (varname (plotname, stPlotAxisXAxisAttrTitleColor), 
                          TPlotColorLookup::ColorType
                          (opt->fAxisX.fAxisAttr.GetTitleColor()).Compatibility());
         fCmdLine->putVar (varname (plotname, stPlotAxisXGrid),
                          (bool) opt->fAxisX.fGrid);
         fCmdLine->putVar (varname (plotname, stPlotAxisXBothSides), 
                          (bool) opt->fAxisX.fBothSides);
         fCmdLine->putVar (varname (plotname, stPlotAxisXCenterTitle), 
                          (bool) opt->fAxisX.fCenterTitle);
         // Y axis
         s = opt->fAxisY.fAxisTitle;
         fCmdLine->putVar (varname (plotname, stPlotAxisYTitle), s);
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrAxisColor), 
                          TPlotColorLookup::ColorType
                          (opt->fAxisY.fAxisAttr.GetAxisColor()).Compatibility());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrLabelColor), 
                          TPlotColorLookup::ColorType
                          (opt->fAxisY.fAxisAttr.GetLabelColor()).Compatibility());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrLabelFont), 
                          opt->fAxisY.fAxisAttr.GetLabelFont ());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrLabelOffset), 
                          opt->fAxisY.fAxisAttr.GetLabelOffset ());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrLabelSize), 
                          opt->fAxisY.fAxisAttr.GetLabelSize ());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrNdividions), 
                          opt->fAxisY.fAxisAttr.GetNdivisions ());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrTickLength), 
                          opt->fAxisY.fAxisAttr.GetTickLength ());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrTitleOffset), 
                          opt->fAxisY.fAxisAttr.GetTitleOffset ());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrTitleSize), 
                          opt->fAxisY.fAxisAttr.GetTitleSize ());
         fCmdLine->putVar (varname (plotname, stPlotAxisYAxisAttrTitleColor), 
                          TPlotColorLookup::ColorType
                          (opt->fAxisY.fAxisAttr.GetTitleColor()).Compatibility());
         fCmdLine->putVar (varname (plotname, stPlotAxisYGrid),
                          (bool) opt->fAxisY.fGrid);
         fCmdLine->putVar (varname (plotname, stPlotAxisYBothSides), 
                          (bool) opt->fAxisY.fBothSides);
         fCmdLine->putVar (varname (plotname, stPlotAxisYCenterTitle), 
                          (bool) opt->fAxisY.fCenterTitle);
         // legend
         fCmdLine->putVar (varname (plotname, stPlotLegendShow), 
                          (bool) opt->fLegend.fShow);
         fCmdLine->putVar (varname (plotname, stPlotLegendPlacement), 
                          (int) opt->fLegend.fPlacement);
         fCmdLine->putVar (varname (plotname, stPlotLegendXAdjust), 
                          opt->fLegend.fXAdjust);
         fCmdLine->putVar (varname (plotname, stPlotLegendYAdjust), 
                          opt->fLegend.fYAdjust);
         fCmdLine->putVar (varname (plotname, stPlotLegendSymbolStyle), 
                          (int) opt->fLegend.fSymbolStyle);
         fCmdLine->putVar (varname (plotname, stPlotLegendTextStyle), 
                          (int) opt->fLegend.fTextStyle);
         fCmdLine->putVar (varname (plotname, stPlotLegendSize), opt->fLegend.fSize);
         for (int i = 0; i < kMaxTraces; i++) {
            s = opt->fLegend.fText[i];
            fCmdLine->putVar (varname (plotname, stPlotLegendText, 
                                      -1, -1, i), s);
         }
         // param
         fCmdLine->putVar (varname (plotname, stPlotParamShow),
                          (bool) opt->fParam.fShow);
         fCmdLine->putVar (varname (plotname, stPlotParamT0), 
                          (bool) opt->fParam.fT0);
         fCmdLine->putVar (varname (plotname, stPlotParamAvg), 
                          (bool) opt->fParam.fAvg);
         fCmdLine->putVar (varname (plotname, stPlotParamSpecial), 
                          (bool) opt->fParam.fSpecial);
         fCmdLine->putVar (varname (plotname, stPlotParamTimeFormatUTC), 
                          (bool) opt->fParam.fTimeFormatUTC);
         fCmdLine->putVar (varname (plotname, stPlotParamTextSize), 
                          opt->fParam.fTextSize);
         ret = kTRUE;
      }
      if (my_debug) cerr << "DiagMainWindow::TransferPlotSettings() returns " << (ret ? "TRUE" : "FALSE") << endl ;
      return ret;
   }


   Bool_t DiagMainWindow::TransferCalibrationSettings (Bool_t toGUI, 
                     Int_t index, calibration::Calibration& cal) 
   {
      string s;
      bool b;
      int i;
      unsigned long sec;
      unsigned long nsec;
      double d;
      char calname[128];
      float* f;
      int n;
   
      sprintf (calname, "%s[%i]", stCal, index);
      if (toGUI) {
         // channel name
         if (!fCmdLine->getVar (varname (calname, stCalChannel), s)) {
            return kFALSE;
         }
         cal.SetChannel (s.c_str());
         // reference
         if (!fCmdLine->getVar (varname (calname, stCalReference), s)) {
            return kFALSE;
         }
         cal.SetRef (s.c_str());
         // unit
         if (!fCmdLine->getVar (varname (calname, stCalUnit), s)) {
            return kFALSE;
         }
         cal.SetUnit (s.c_str());
         // time
         if (fCmdLine->getTime (varname (calname, stCalTime), 
                              sec, nsec)) {
	    Time t(sec);
            cal.SetTime (t);
         }
         // duration
         if (fCmdLine->getVar (varname (calname, stCalDuration), i)) {
            cal.SetDuration (i);
         }
         // conversion
         if (fCmdLine->getVar (varname (calname, stCalConversion), d)) {
            cal.SetConversion (d);
         }
         // offset
         if (fCmdLine->getVar (varname (calname, stCalOffset), d)) {
            cal.SetOffset (d);
         }
         // time delay
         if (fCmdLine->getVar (varname (calname, stCalTimeDelay), d)) {
            cal.SetTimeDelay (d);
         }
         // transfer function
         if (fCmdLine->getVar 
            (varname (calname, stCalTransferFunction), &f, n)) {
            cal.SetTransferFunction (f, n / 3);   
            delete [] f;
         }
         // gain, poles and zeros
         double gain = 1; int pnum = 0; int znum = 0;
         float* poles = 0; float* zeros = 0;
         bool haspz = false;
         if (fCmdLine->getVar (varname (calname, stCalGain), gain)) {
            haspz = true;
         }
         if (fCmdLine->getVar (varname (calname, stCalPoles), 
                              &poles, pnum)) {
            haspz = true;
            pnum /= 2; // complex
         }
         if (fCmdLine->getVar (varname (calname, stCalZeros), 
                              &zeros, znum)) {
            haspz = true;
            znum /= 2;
         }
         if (haspz) {
            float* pzs = new float[4*(pnum > znum ? pnum : znum)];
            if (pzs) {
               for (int i = 0; i < pnum; i++) {
                  pzs[4*i] = poles[2*i];
                  pzs[4*i+1] = poles[2*i+1];
               }
               for (int i = 0; i < znum; i++) {
                  pzs[4*i+2] = zeros[2*i];
                  pzs[4*i+3] = zeros[2*i+1];
               }
               cal.SetPoleZeros (gain, pnum, znum, pzs);
               delete [] pzs;
            }
         }
         if (poles) delete [] poles;
         if (zeros) delete [] zeros;
         // default
         if (fCmdLine->getVar (varname (calname, stCalDefault), b)) {
            cal.SetDefault (b);
         }
         // preferred mag
         if (fCmdLine->getVar (varname (calname, 
                                       stCalPreferredMag), i)) {
            cal.SetPreferredMag (i);
         }
         // preferred derivative
         if (fCmdLine->getVar (varname (calname, 
                                       stCalPreferredD), i)) {
            cal.SetPreferredD (i);
         }
         // comment
         if (fCmdLine->getVar (varname (calname, stCalComment), s)) {
            cal.SetComment (s.c_str());
         }
      }
      else {
         // channel name
         s = cal.GetChannel();
         if (!fCmdLine->putVar (varname (calname, stCalChannel), s)) {
            return kFALSE;
         }
         // reference
         s = cal.GetRef();
         fCmdLine->putVar (varname (calname, stCalReference), s);
         // unit
         s = cal.GetUnit();
         fCmdLine->putVar (varname (calname, stCalUnit), s);
         // time
         if (cal.GetTime() != Time(0)) {
            fCmdLine->putTime (varname (calname, stCalTime), 
                              cal.GetTime().getS(), 0);
         }
         // duration
         if ((int)cal.GetDuration() != 0) {
            fCmdLine->putVar (varname (calname, stCalDuration), 
                             (int)cal.GetDuration());
         }
         // conversion
         if ((cal.GetType() & CALAMPLITUDE) != 0) {
            fCmdLine->putVar (varname (calname, stCalConversion), 
                             cal.GetConversion());
         }
         // offset
         if ((cal.GetType() & CALOFFSET) != 0) {
            fCmdLine->putVar (varname (calname, stCalOffset), 
                             cal.GetOffset());
         }
         // time delay
         if ((cal.GetType() & CALTIMEDELAY) != 0) {
            fCmdLine->putVar (varname (calname, stCalTimeDelay), 
                             cal.GetTimeDelay());
         }
         // transfer function
         if ((cal.GetType() & CALTRANSFERFUNCTION) != 0) {
            const float* trans;
            int len = cal.GetTransferFunction (trans);
            if ((len > 0) && (trans != 0)) {
               fCmdLine->putVar 
                  (varname (calname, stCalTransferFunction), 
                  trans, 3 * len);
            }
         }
         // gain, poles and zeros
         if ((cal.GetType() & CALPOLEZERO) != 0) {
            double gain = 1; int pnum = 0; int znum = 0;
            const float* pzs = 0;
            if (cal.GetPoleZeros (gain, pnum, znum, pzs)) {
               fCmdLine->putVar (varname (calname, stCalGain), gain);
               float* poles = new float [2*pnum];
               if (poles) {
                  for (int i = 0; i < pnum; i++) {
                     poles[2*i] = pzs[4*i];
                     poles[2*i+1] = pzs[4*i+1];
                  }
                  fCmdLine->putVar (varname (calname, stCalPoles), 
                                   poles, 2*pnum);
                  delete [] poles;
               }
               float* zeros = new float [2*znum];
               if (zeros) {
                  for (int i = 0; i < znum; i++) {
                     zeros[2*i] = pzs[4*i+2];
                     zeros[2*i+1] = pzs[4*i+3];
                  }
                  fCmdLine->putVar (varname (calname, stCalZeros), 
                                   zeros, 2*znum);
                  delete [] zeros;
               }
            }
         }
         // default
         if (cal.GetDefault()) {
            b = true;
            fCmdLine->putVar (varname (calname, stCalDefault), b);
         }
         // preferred mag
         if (cal.GetPreferredMag() != 0) {
            fCmdLine->putVar (varname (calname, stCalPreferredMag), 
                             cal.GetPreferredMag());
         }
         // preferred mag
         if (cal.GetPreferredD() != 0) {
            fCmdLine->putVar (varname (calname, stCalPreferredD), 
                             cal.GetPreferredD());
         }
         // comment
         if (cal.GetComment() != NULL) {
            s = cal.GetComment();
            fCmdLine->putVar (varname (calname, stCalComment), s);
         }
      }
      return kTRUE;
   }


   class xsilParamTransferHandler : public xsilHandler {
   protected:
      string 		fVarname;
      xsilStd::DataType fType;
      basic_commandline*	fCmd;
   public:
      xsilParamTransferHandler (basic_commandline* cmd, const string& var, 
                        xsilStd::DataType dtype) 
      : xsilHandler (true), fVarname (var), fType (dtype), fCmd (cmd) {
      }
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const bool& p, int N = 1) {
         fCmd->putVar (varname (fVarname, name), &p, N); 
         return true; }
      /// byte parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const char& p, int N = 1) {
         return false; }
      /// short parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const short& p, int N = 1) {
         return false; }
      /// int parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const int& p, int N = 1) {
         fCmd->putVar (varname (fVarname, name), &p, N);
         return true; }
      /// long parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const long long& p, int N = 1) {
         return false; }
      /// float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const float& p, int N = 1) {
         return false; }
      /// double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const double& p, int N = 1) {
         fCmd->putVar (varname (fVarname, name), &p, N);
         return true; }
      /// complex float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<float>& p, int N = 1) {
         return false; }
      /// complex double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<double>& p, int N = 1) {
         fCmd->putVar (varname (fVarname, name), (double*)&p, 2*N);
         return true; }
      /// string parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::string& p) {
         // ignore channels
         if (strncasecmp (name.c_str(), "Channel", 7) == 0) {
            return true;
         }
         fCmd->putVar (varname (fVarname, name), p);
         return true; }
      /// time callback (must return true if handled)
      virtual bool HandleTime (const std::string& name,
                        const attrlist& attr,
                        unsigned long sec, unsigned long nsec) {
         fCmd->putTime (varname (fVarname, name), sec, nsec);
         return true; }
   };


   class xsilParamTransferHandlerQuery : public xsilHandlerQuery {
   protected:
      string 		fVarname;
      xsilStd::DataType fType;
      basic_commandline*	fCmd;
   public:
      xsilParamTransferHandlerQuery (basic_commandline* cmd,
                        const string& var, xsilStd::DataType dtype) 
      : fVarname (var), fType (dtype), fCmd (cmd) {
      }
      virtual xsilHandler* GetHandler (const attrlist& attr) {
         return new (nothrow) xsilParamTransferHandler (
                              fCmd, fVarname, fType); }
   };


   Bool_t DiagMainWindow::UpdateData ()
   {
      if (my_debug) cerr << "DiagMainWindow::UpdateData ()" << endl ;
      // The fPlot pointer points to a PlotSet class instance that
      // contains an iterator class.  The iterator can be used to access
      // everything that shows up in the "Channels" pulldown menu in the
      // "Traces" tab of the Result tab.

      // make sure all reference traces have been loaded
      int n = 0, r = 0 ; // for debugging
      for (PlotSet::iterator j = fPlot->begin(); j != fPlot->end(); j++) {
	 n++ ; // for debugging
         if (j->IsCalculated() ||  (j->GetData() == 0) || 
            !j->GetData()->IsDirty()) {
            continue;
         }
         // check if reference, all reference objects end with "(REFx)"
         if (strstr (j->GetAChannel(), "(REF") != 0) {
	    r++ ; // for debugging
            // trigger load
            j->GetData()->GetY();
         }
      }
      if (my_debug) cerr << "  There are " << n << " objects in fPlot" << endl ;
      if (my_debug) cerr << "     " << r << " of them are reference traces." << endl ;

      // delete reference traces in storage object next
      fCmdLine->parse ("del references");
   
      // generate list of result indices
      set<int> indexlist;
      basic_commandline::masterindex	mi;
      if (fCmdLine->readMasterIndex (mi)) {
         int entrynum = 0;
         for (basic_commandline::masterindex::iterator iter = mi.begin();
             iter != mi.end(); ++iter, ++entrynum) {
            char buf[256];
            sprintf (buf, "%s.%s[%i]", stIndex, stIndexEntry, entrynum);
            string entry;
            if (!fCmdLine->getVar (string (buf), entry)) {
               continue;
            }
            string::size_type pos = entry.find (string (stResult) + "[");
            while (pos != string::npos) {
               pos += strlen (stResult) + 1;
               int indx = atoi (entry.c_str() + pos);
               if (indx >= 0) indexlist.insert (indx);
               entry.erase (0, pos);
               pos = entry.find (string (stResult) + "[");
            }
         }
	 if (my_debug) cerr << "  The indexlist contains " << entrynum << " items" << endl ;
      }
   
      // update aux list states
      n = 0; // for debugging
      for (AuxDataDescList::iterator i = fAuxList.begin();
          i != fAuxList.end(); ++i) {
	  n++ ; // for debugging
         i->fState = AuxDataDesc::kDeleted;
      }
      if (my_debug) cerr << "  The aux list contains " << n << " items" << endl ;

      for (PlotSet::iterator i = fPlot->begin(); i != fPlot->end(); i++) {
         if (i->IsCalculated() ||  (i->GetData() == 0)) {
            continue;
         }
         AuxDataDescList::iterator pos = 
            fAuxList.find (AuxDataDesc (i->GetGraphType(), i->GetAChannel(), 
                                       i->GetBChannel()));
         if (pos != fAuxList.end()) {
            if (i->IsDirty()) {
               pos->fState = AuxDataDesc::kModified;
            }
            else {
               pos->fState = AuxDataDesc::kUnchanged;
            }
         }
      }
      // delete obsolete aux
      AuxDataDescList newlist;
      for (AuxDataDescList::iterator i = fAuxList.begin();
          i != fAuxList.end(); ++i) {
         if (i->fState == AuxDataDesc::kDeleted) {
            TString cmd = "del " + i->fName;
            fCmdLine->parse ((const char*)cmd);
         }
         else {
            newlist.insert (*i);
            indexlist.insert (i->fIndex);
         }
      }
      fAuxList = newlist;
   
      // go through plot set and update all 'dirty' & not-caluclated 
      // plot descriptors
      if (my_debug) cerr << " Go thru the plot set, update all 'dirty' and not-calculated plot descriptors" << endl ;
      int auxnum = 0;
      for (PlotSet::iterator i = fPlot->begin(); i != fPlot->end(); i++) {
         if (i->IsCalculated() ||  (i->GetData() == 0) || 
            !i->GetData()->IsDirty()) {
	    if (my_debug) cerr << "  " << i->GetAChannel() << " will be skipped." << endl ;
            continue;
         }
         // clear dirty bit for non-persistent objects only
         AuxDataDesc a (i->GetGraphType(), i->GetAChannel(), i->GetBChannel());
         if (!i->IsPersistent()) {
            // this is a new aux record!
            a.fState = AuxDataDesc::kNew;
            set<int>::iterator pos = indexlist.lower_bound (auxnum);
            while ((pos != indexlist.end()) && (auxnum == *pos)) {
               ++pos;
               ++auxnum;
            }
            a.fIndex = auxnum++;
            char buf[256];
            sprintf (buf, "%s[%i]", stResult, a.fIndex);
            a.fName = buf;
            fAuxList.insert (a);
            i->SetDirty (kFALSE);
         }
      
         // name
         string n;
         const char* p;
         if ((p = strstr (i->GetAChannel(), "(REF")) != 0) {
            int refnum = atoi (p + 4);
            char name[256]; 
            sprintf (name, "Reference[%i]", refnum);
            n = name;
	    if (my_debug) cerr << "  " << i->GetAChannel() << " name will be " << n << endl ;
         }
         else if (a.fState != AuxDataDesc::kUnknown) {
            n = a.fName;
            cout << "Update auxiliary data " << n << 
               " (" << a.fGraph << ":" << a.fAChn << "|" << a.fBChn << ")" << endl;
         }
         else {
	    if (my_debug) cerr << "  Reference or aux not found, skip " << i->GetAChannel() << endl ;
            continue;
         }
	 if (my_debug) cerr << " The name will be " << n << endl ;

         // type/subtype
         xsilStd::DataType dtype;
         int subtype;
         if (!xsilStd::GetDataType (i->GetGraphType(), dtype, subtype)) {
            return false;
         }
         // data info
         bool cmplx = false;
         int dim1 = 0;
         int dim2 = 0;
         const float* data = 0;
         bool xydata;
         bool copy;
         if (!xsilStd::GetDataInfo (i->GetData(),
                              dim1, dim2, cmplx, xydata, &data, copy, 
                              false, dtype == xsilStd::kCoefficients)) {
	    if (my_debug) cerr << " No data found for " << i->GetAChannel() << ", skip" << endl ;
            continue;
         }
         if (xydata) {
            subtype = xsilStd::DataSubtypeXY (dtype, subtype);
         }
      
         // write data
         int objtype = (cmplx ? 1 : 2) + 10 * ((int)dtype + 1);
         int stride = cmplx ? (2 * dim2) : dim2;
         int ofs = 0;
         const float* y = data;
         for (int k = 0; k < dim1; k++, y += stride, ofs += dim2) {
	    // n is the name from several lines above....
            if (!fCmdLine->putData (n, y, dim2, objtype, ofs)) {         
               break;
            }
            objtype %= 10;
         }
      
         // write parameters
	 if (my_debug) cerr << "  Write parameters for " << i->GetAChannel() << endl ;
	 // Reminder - i is a PlotSet::iterator, i-> is a PlotDescriptor, i->Param() returns a ParameterDescriptor
	 //  i->Param().GetUser() returns a user string set by SetUser() (See Containers/PlotSet/ParamDesc.hh).

         if (i->Param().GetUser()) {
            stringstream  ss;
            ss << xsilHeader() << endl;
            ss << xsilDataBegin ("Result", 
                                xsilStd::Typename (dtype).c_str()) << endl;

            ss << i->Param().GetUser();


            ss << xsilDataEnd<float> () << endl;
            ss << xsilTrailer() << endl;
            xsilParamTransferHandlerQuery prmtrans (fCmdLine, n, dtype);
            xsilParser parse;
            parse.AddHandler (prmtrans);
            parse.Parse (ss);
            parse.Done();
         }
	 if (my_debug) cerr << "  putvar - " << varname(n, stTimeSeriesSubtype) << ", " << ((int) subtype) << endl ;
         fCmdLine->putVar (varname (n, stTimeSeriesSubtype), (int) subtype);
         fCmdLine->putVar (varname (n, stTimeSeriesN), (int) dim2);
         if (dtype != xsilStd::kTimeSeries) {
            fCmdLine->putVar (varname (n, stSpectrumM), 
                             (int) dim1 - (xydata ? 1 : 0));
         }
         if (!xydata) {
            double x0 = (i->GetData() && (dim2 > 0)) ? i->GetData()->GetX()[0] : 0;
            double dx = i->GetData()->GetDX();
            if (dtype == xsilStd::kTimeSeries) {
               fCmdLine->putVar (varname (n, stTimeSeriesdt), dx);
            }
            else {
               fCmdLine->putVar (varname (n, stSpectrumf0), x0);
               fCmdLine->putVar (varname (n, stSpectrumdf), dx);
            }
         }
         UInt_t sec; UInt_t nsec;
         if (i->Param().GetStartTime (sec, nsec)) {
            fCmdLine->putTime (varname (n, stTimeSeriest0), sec, nsec);
         }
         vector<string> Achn;
         vector<string> Bchn;
         if (TLGXMLSaver::GetChannelList (*fPlot, *i, Achn, Bchn)) {
            if (dtype == xsilStd::kTimeSeries) {
               if (!Achn.empty()) {
                  fCmdLine->putVar (varname (n, stTimeSeriesChannel),
                                   Achn[0]);
               }
            }
            else if ((dtype == xsilStd::kSpectrum) ||
                    (dtype == xsilStd::kTransferFunction)) {
               if (!Achn.empty()) {
                  fCmdLine->putVar (varname (n, stSpectrumChannelA), 
                                   Achn[0]);
               }
               int m = 0;
               for (vector<string>::iterator i = Bchn.begin();
                   i != Bchn.end(); i++, m++) {
                  fCmdLine->putVar (varname (n, stSpectrumChannelB, 
                                            -1 , -1, m), *i);
               }
            }
            else if (dtype == xsilStd::kCoefficients) {
               int m = 0;
               for (vector<string>::iterator i = Achn.begin();
                   i != Achn.end(); i++, m++) {
                  fCmdLine->putVar (varname (n, stCoefficientsChannelA, 
                                            -1 , -1, m), *i);
               }
               m = 0;
               for (vector<string>::iterator i = Bchn.begin();
                   i != Bchn.end(); i++, m++) {
                  fCmdLine->putVar (varname (n, stCoefficientsChannelB, 
                                            -1 , -1, m), *i);
               }
            }
         }
      
         // delete data if necessary and clear dirty
         if (copy) {
            delete [] data;
         }
      }
      if (my_debug) cerr << "DiagMainWindow::UpdateData() return" << endl ;
      return kTRUE;
   }


   Bool_t DiagMainWindow::SaveCalibrationData ()
   {
      // delete old records
      fCmdLine->parse ("del calibration");
   
      if (!fCalibrationSaveFlag) {
         return kTRUE;
      }
      Int_t num = fCalTable->Len();
      if (!fCmdLine->putVar (varname (stDef, stDefCalibrationRecords), num)) {
         return kFALSE;
      }      
      // save calibration data
      for (int i = 0; i < num; i++) {
         TransferCalibrationSettings (kFALSE, i, (*fCalTable)[i]);
      }
      return kTRUE;
   }


   Bool_t DiagMainWindow::RestoreCalibrationData ()
   {
      if (!fCalibrationRestoreFlag) {
         return kTRUE;
      }
   
      // get number of calibration records
      Int_t num = 0;
      if (!fCmdLine->getVar (varname (stDef, stDefCalibrationRecords), num)) {
         return kFALSE;
      }
   
      // restore calibration data
      for (int i = 0; i < num; i++) {
         calibration::Calibration cal;
         if (TransferCalibrationSettings (kTRUE, i, cal)) {
            fCalTable->Add (cal);
            fCalTable->AddChannel (cal.GetChannel());
         }
      }
   
      return kTRUE;
   }


   Bool_t DiagMainWindow::SavePlotSettings ()
   {
      if (my_debug) cerr << "DiagMainWindow::SavePlotSettings()" << endl ;
      Bool_t ret = kTRUE;
   
      // delete old settings
      fCmdLine->parse ("del plotsettings");
   
      // save stored settings (index 0)
      int n = 0;
      if (my_debug) cerr << "  fStoreOptionsMax = " << fStoreOptionsMax << endl ;
      for (int i = 0; i < fStoreOptionsMax; i++) {
         if (fStoreOptions[i] != 0) {
	    // Get plot settings from the GUI.
            if (TransferPlotSettings (kFALSE, 0, n, fStoreOptions[i])) {
               n++;
            }
            else {
               ret = kFALSE;
            }
         }
      }
      // quit if settings flag is not set
      if (!fSettingsSaveFlag) {
         fCmdLine->putVar (varname (stDef, stDefPlotWindows), 0);
         return kTRUE;
      }
   
      // save embedded pads
      if (my_debug) cerr << "  save embedded pads" << endl ;
      if (my_debug) cerr << "    pad layout = " << fMainCtrl->GetPad()->GetPadLayout() << endl ;
      fCmdLine->putVar (varname (stDef, stDefPlotWindowLayout, -1, -1, 1),
                       fMainCtrl->GetPad()->GetPadLayout());
      // save pad settings
      for (Int_t i2 = 0; i2 < fMainCtrl->GetPad()->GetPadNumber(); i2++) {
	 // Get plot settings from the GUI.
         if (!TransferPlotSettings (kFALSE, 1, i2, 
                              fMainCtrl->GetPad()->GetPlotOptions (i2))) {
            ret = kFALSE;
         }
      }
   
      // loop over multi pads
      if (my_debug) cerr << "  save multi pads" << endl ;
      const PlotSet::winlist* list = fPlot->GetRegisteredWindows();
      if (!list) {
         return kFALSE;
      }
      Int_t i1 = 1;
      for (PlotSet::winlist::const_iterator w = list->begin(); 
          w != list->end(); ++w) {
         i1++;
         const TLGPadMain* ww = dynamic_cast<const TLGPadMain*>(*w);
         if (!ww) {
            continue;
         }
         // save multipad layout
	 if (my_debug) cerr << "    pad layout = " <<  ww->GetPads()->GetPadLayout() << endl ;
         fCmdLine->putVar (varname (stDef, stDefPlotWindowLayout, -1, -1, i1),
                          ww->GetPads()->GetPadLayout());
         // save pad settings
         for (Int_t i2 = 0; i2 < ww->GetPads()->GetPadNumber(); i2++) {
	    // Get plot settings from the GUI.
            if (!TransferPlotSettings (kFALSE, i1, i2, 
                                 ww->GetPads()->GetPlotOptions (i2))) {
               ret = kFALSE;
            }
         }
      }
      // set number of windows
      fCmdLine->putVar (varname (stDef, stDefPlotWindows), i1);
      if (my_debug) cerr << "  Number of windows = " << i1 << endl ;
   
      return kTRUE;
   }


   Bool_t DiagMainWindow::ShowPlots ()
   {
      Bool_t ret = kTRUE;
   
      if (my_debug) cerr << "DiagMainWindow::ShowPlots ()" << endl ;
   //    // initialize calibration units for plot descriptors
      // for (PlotSet::iterator i = fPlot->begin(); i != fPlot->end(); i++) {
         // fCalTable->AddUnits (i->Cal());
      // }
   // 
      // initialize stored settings
      if (my_debug) cerr << "  fStoreOptionsMax = " << fStoreOptionsMax << endl ;
      for (int i = 0; i < fStoreOptionsMax; i++) {
         if (fStoreOptions[i] != 0) {
            delete fStoreOptions[i];
            fStoreOptions[i] = 0;
         }
      }   
      // restore stored settings (index 0)
      if (my_debug) cerr << "  Restore stored settings" << endl ;
      int n = 0;
      OptionAll_t opt;
      for (int i = 0; i < fStoreOptionsMax; i++) {
	 // Transfer plot settings to the GUI
         if (TransferPlotSettings (kTRUE, 0, i, &opt)) {
            fStoreOptions[n] = new OptionAll_t;
            if (fStoreOptions[n] != 0) {
               *(fStoreOptions[n]) = opt;
               n++;
            }
         }
      }
      if (my_debug) cerr << "     n = " << n << endl ;
      // get number desired of windows
      Int_t win = 1;
      if (!fSettingsRestoreFlag || 
         !fCmdLine->getVar (varname (stDef, stDefPlotWindows), win) ||
         (win == 0)) {
         ShowDefaultPlot (kFALSE);
         return kTRUE;
      }
      if (my_debug) cerr << "  Number of desired plot windows = " << win << endl ;
      // count active windows 
      const PlotSet::winlist* list = fPlot->GetRegisteredWindows();
      if (!list) {
         return kFALSE;
      }
      // if active windows smaller than desired, create new ones
      if (my_debug) cerr << "  Number of active windows = " << list->size() << endl ;
      for (int i = list->size() + 1; i < win; i++) {
         if (fMainCtrl->GetPad()->NewWindow() == 0) {
            ret = kFALSE;
         }
      }
      // set plot settings in embedded pads
      if (my_debug) cerr << "  Set plot settings in embedded pads" << endl ;
      Int_t layout = 2;
#ifdef STORE_OPTION_VIS
      // Rearranged to read the plot options BEFORE laying out the pads. JCB
      for (Int_t i2 = 0; i2 < fMainCtrl->GetPad()->GetPadNumber(); i2++) {
	 // Transfer plot settings to the GUI
         if (!TransferPlotSettings (kTRUE, 1, i2, 
                              fMainCtrl->GetPad()->GetPlotOptions (i2))) {
            ret = kFALSE;
         }
      }
      fCmdLine->getVar (varname (stDef, stDefPlotWindowLayout, -1, -1, 1),
                       layout);
//      if (fMainCtrl->GetPad()->GetPadLayout() != layout) {
         fMainCtrl->GetPad()->SetPadLayoutAndNumber (layout);
//      }
#else
      fCmdLine->getVar (varname (stDef, stDefPlotWindowLayout, -1, -1, 1), layout);
      if (my_debug) cerr << "    plot layout = " << layout << endl ;
      if (my_debug) cerr << "      fMainCtrl->GetPad()->GetPadLayout() = " << fMainCtrl->GetPad()->GetPadLayout() << endl ;
      if (fMainCtrl->GetPad()->GetPadLayout() != layout) {
	 if (my_debug) cerr << "        --- So change the layout." << endl ;
         fMainCtrl->GetPad()->SetPadLayoutAndNumber (layout);
      }
      if (my_debug) cerr << "      plot layout is now " << fMainCtrl->GetPad()->GetPadLayout() << endl ;
      if (my_debug) cerr << "    fMainCtrl->GetPad()->GetPadNumber() = " << fMainCtrl->GetPad()->GetPadNumber() << endl ;
      for (Int_t i2 = 0; i2 < fMainCtrl->GetPad()->GetPadNumber(); i2++) {
         if (!TransferPlotSettings (kTRUE, 1, i2, 
                              fMainCtrl->GetPad()->GetPlotOptions (i2))) {
            ret = kFALSE;
         }
      }
#endif
   
      // set plot settings in multi pad windows
      if (my_debug) cerr << "  Set plot settings in multi pad windows" << endl ;
      Int_t i1 = 1;
      for (PlotSet::winlist::const_iterator w = list->begin(); 
          w != list->end(); ++w) {
         i1++;
         const TLGPadMain* ww = dynamic_cast<const TLGPadMain*>(*w);
         if (!ww) {
            continue;
         }
         // set multipad layout
         Int_t layout = 2;
         fCmdLine->getVar (varname (stDef, stDefPlotWindowLayout, -1, -1, i1),
                          layout);
	 if (my_debug) cerr << "    layout = " << layout << endl ;
	 if (my_debug) cerr << "       ww->GetPads()->GetPadLayout() = " << ww->GetPads()->GetPadLayout() << endl ;
         if (ww->GetPads()->GetPadLayout() != layout) {
	    if (my_debug) cerr << "        --- So change the layout." << endl ;
            ww->GetPads()->SetPadLayoutAndNumber (layout);
         }
	 if (my_debug) cerr << "    ww->GetPads()->GetPadLayout() = " << ww->GetPads()->GetPadLayout() << endl ;
         // set plot settings
	 if (my_debug) cerr << "    ww->GetPads()->GetPadNumber() = " << ww->GetPads()->GetPadNumber() << endl ;
         for (Int_t i2 = 0; i2 < ww->GetPads()->GetPadNumber(); i2++) {
	    // Transfer plot settings to the GUI
	    if (my_debug) cerr << "    Transfer the plot settings for pad number " << i2 << endl ;
            if (!TransferPlotSettings (kTRUE, i1, i2, ww->GetPads()->GetPlotOptions (i2))) {
               ret = kFALSE;
            }
         }
      }
      if (my_debug) cerr << "DiagMainWindow::ShowPlots () - return " << (ret ? "TRUE" : "FALSE") << endl ;
      return ret;
   }


   void DiagMainWindow::SetState (DiagState_t state)
   {
      bool show_disconnect = false;//change to true if disconnected due to socket close or error.
      if (fState != state) {
         fState = state;
         switch (fState) {
            case dsInactive:
               fStatus->SetText ("", 3);
               fStart->SetState (kButtonUp);
               fAbort->SetState (kButtonDisabled);
               fPause->SetState (kButtonDisabled);
               fResume->SetState (kButtonDisabled);
               fMenuEdit->EnableEntry (M_EDIT_START);
               fMenuEdit->DisableEntry (M_EDIT_ABORT);
               fMenuEdit->DisableEntry (M_EDIT_PAUSE);
               fMenuEdit->DisableEntry (M_EDIT_RESUME);
               show_disconnect = true;
               break;
            case dsRunning:
               fStatus->SetText ("running", 3);
               fStart->SetState (kButtonDisabled);
               fAbort->SetState (kButtonUp);
               fPause->SetState (kButtonUp);
               fResume->SetState (kButtonDisabled);
               fMenuEdit->DisableEntry (M_EDIT_START);
               fMenuEdit->EnableEntry (M_EDIT_ABORT);
               fMenuEdit->EnableEntry (M_EDIT_PAUSE);
               fMenuEdit->DisableEntry (M_EDIT_RESUME);
               break;
            case dsPaused:
               fStatus->SetText ("paused", 3);
               fStart->SetState (kButtonDisabled);
               fAbort->SetState (kButtonUp);
               fPause->SetState (kButtonDisabled);
               fResume->SetState (kButtonUp);
               fMenuEdit->DisableEntry (M_EDIT_START);
               fMenuEdit->EnableEntry (M_EDIT_ABORT);
               fMenuEdit->DisableEntry (M_EDIT_PAUSE);
               fMenuEdit->EnableEntry (M_EDIT_RESUME);
               show_disconnect = true;
               break;
            case dsAborted:
               fStatus->SetText ("stopped", 3);
               fStart->SetState (kButtonUp);
               fAbort->SetState (kButtonDisabled);
               fPause->SetState (kButtonDisabled);
               fResume->SetState (kButtonDisabled);
               fMenuEdit->EnableEntry (M_EDIT_START);
               fMenuEdit->DisableEntry (M_EDIT_ABORT);
               fMenuEdit->DisableEntry (M_EDIT_PAUSE);
               fMenuEdit->DisableEntry (M_EDIT_RESUME);
               show_disconnect = true;
               break;
            case dsFailed:
               fStatus->SetText ("failed", 3);
               fStart->SetState (kButtonUp);
               fAbort->SetState (kButtonDisabled);
               fPause->SetState (kButtonDisabled);
               fResume->SetState (kButtonDisabled);
               fMenuEdit->EnableEntry (M_EDIT_START);
               fMenuEdit->DisableEntry (M_EDIT_ABORT);
               fMenuEdit->DisableEntry (M_EDIT_PAUSE);
               fMenuEdit->DisableEntry (M_EDIT_RESUME);
               show_disconnect = true;
               break;
            case dsDone:
               fStatus->SetText ("done", 3);
               fStart->SetState (kButtonUp);
               fAbort->SetState (kButtonDisabled);
               fPause->SetState (kButtonDisabled);
               fResume->SetState (kButtonDisabled);
               fMenuEdit->EnableEntry (M_EDIT_START);
               fMenuEdit->DisableEntry (M_EDIT_ABORT);
               fMenuEdit->DisableEntry (M_EDIT_PAUSE);
               fMenuEdit->DisableEntry (M_EDIT_RESUME);
               show_disconnect = true;
               break;
         }
      }
      if(fEnableDisconnectMessage)
      {


          int num_pads = fMainCtrl->GetPad()->GetPadNumber();
          for(int i=0; i < num_pads; ++i)
          {
              fMainCtrl->GetPad()->GetPad(i)->ProgramStatus()->Disconnected(show_disconnect);
          }
          fPlot->Update();
      }
   }


   void DiagMainWindow::SetStatusMsg (const char* msg)
   {
      fStatusMsg = msg;
      fStatus->SetText (msg, 0);

      // trap "data on tape" messages and display a help message
      cerr << "status msg = " << msg << endl;
      string msg_str = msg;
      cerr << "status msg = " << msg_str << endl;
      if(msg_str.find("found on tape") != msg_str.npos) {
        new TGMsgBox((gClient->GetRoot()),this,"Data on Tape", "The data was found on a tape drive,\nbut diaggui is not configured to retrieve data from tape.\nTo retrieve data from tape, select 'Edit -> Read from Tape'\nfrom the menu.\n\nWARNING: reading data from tape is can take several minutes.\ndiaggui can not report on progress during this time.");
      }
   }


   void DiagMainWindow::Print (const char* format) const
   {
      TLGPrintParam printparam (format);
      fMainCtrl->GetPad()->PrintPS (printparam);
   }


   void DiagMainWindow::ClearCache ()
   {
      fCmdLine->parse("del cache");
      fParam->fData.fDacc.ClearCache();
   }


   string FileFlag (Int_t id) 
   {
      switch (id) {
         case 0:
            return "-all ";
         case 1:
         default:
            return "-std ";
         case 2:
            return "-prm ";
      }
   }


   Bool_t DiagMainWindow::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        return ProcessButton (parm1, parm2);
                     }
                  case kCM_MENU: 
                     {
                        return ProcessMenu (parm1, parm2);
                     }
               }
               break;
            }
         case kC_STATUS:
            {
               return ProcessStatus (GET_SUBMSG (msg), parm2);
            }
         case kC_NOTIFY:
            {
               return ProcessNotify (parm1, parm2);
            }
      }
      return kTRUE;
   }


   Bool_t DiagMainWindow::ProcessMenu (Long_t parm1, Long_t parm2)
   {
      switch (parm1) {
         case M_FILE_NEW:
            {
               if (IsRunning()) {
                  new TGMsgBox (gClient->GetRoot(), this, "Error", 
                               "Cannot initialize a new test\n"
                               "while another test is running.", 
                               kMBIconStop, kMBOk);
                  break;
               }
               // test if plot results are there
               if (fMainCtrl->HaveResults()) {
                  Int_t ret;
                  TString msg = "This will clear all results.\n"
                     "Do you want to continue?";
                  new TGMsgBox (gClient->GetRoot(), this, "Clear results", 
                               msg, kMBIconQuestion, kMBYes | kMBNo, &ret);
                  if (ret == kMBNo) {
                     break;
                  }
               }
               ClearResults();
               defaultTestParameters (*fParam, fMainCtrl->GetSelectedMeasurement());
               WriteParam ();
               TransferParameters (kTRUE);
               fFilename = "";
               SetWindowName (fwindow_name.c_str());
               for (int i = 0; i < 2; i++) {
                  OptionAll_t* opt = fMainCtrl->GetPad()->GetPlotOptions (i);
                  if (opt != 0) {
                     opt->fTraces.fGraphType = "";
                  }
               }
               SetState (dsInactive);
               break;
            }
         case M_FILE_OPEN:
            {
               if (IsRunning()) {
                  new TGMsgBox (gClient->GetRoot(), this, "Error", 
                               "Cannot open a test file\n"
                               "while a test is running.", 
                               kMBIconStop, kMBOk);
                  break;
               }
               TGFileInfo	info;
               info.fFilename = 0;
               info.fIniDir = 0;
            #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
               info.fFileTypes = const_cast<const char**>(gOpenTypes);
            #else
               info.fFileTypes = const_cast<char**>(gOpenTypes);
            #endif
	    #if 1
	       info.fFileTypeIdx = 4 ; // Point to all files.
	       {
		  new TLGFileDialog(this, &info, kFDOpen) ;
	       }
	       if (info.fFilename)
	    #else
               if (TLGFileDialog (this, info, kFDOpen, 0))
	    #endif
	       {
                  tainsec_t now = TAInow();
                  cout << "open cmd at " << (double) (now%1000000000000LL) / 1E9 << endl;
                  gVirtualX->SetCursor (fId, fWaitCursor);
                  gVirtualX->Update();
                  ClearResults ();
                  //ClearResults (kFALSE, kFALSE);
                  bool exists;
                  string error = "";
                  if (fCmdLine->isXML (string (info.fFilename), &exists)) {
                     fCmdLine->clearEcho();
                     fCmdLine->parse (string ("restore ") + 
                                     FileFlag (fFileRestoreFlag) + 
                                     info.fFilename);
                     if (!fCmdLine->getEcho().empty() &&
                        (fCmdLine->getEcho().back().find ("error") != 
                        string::npos)) {
                        error = fCmdLine->getEcho().back();
                     }
                     else {
                        fFilename = info.fFilename;
                        string n = fwindow_name + " - " + (const char *)fFilename;
                        SetWindowName (n.c_str());
                     }
                  }
                  else if (exists) {
                     fCmdLine->clearEcho();
                     fCmdLine->parse 
                        (string ("read ") + info.fFilename);
                     for (deque<string>::const_iterator iter = 
                         fCmdLine->getEcho().begin(); 
                         iter != fCmdLine->getEcho().end(); iter++) {
                        if ((*iter).find ("error") != string::npos) {
                           if (!error.empty()) error += "\n";
                           error += *iter;
                        }
                     }
                  }
                  else {
                     error = "File not found";
                  }
                  if (!error.empty()) {
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  error.c_str(), kMBIconStop, kMBOk);
                  }
                  RestoreCalibrationData();
                  TransferParameters (kTRUE);
                  fMainCtrl->UpdateChannels();
                  if (UpdatePlot (-1, kFALSE)) {
                     // ShowDefaultPlot (kFALSE);
                  }
                  AddOtherData();
                  ShowPlots();
                  fPlot->Update();
                  gVirtualX->SetCursor (fId, kNone);
                  cout << "open cmd done at " << (double) (TAInow() - now)/ 1E9 << endl;
               }
            #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
               delete [] info.fFilename;
            #endif
               SetState (dsInactive);
               break;
            }
         case M_FILE_SAVE:
            {
	       // If a name hasn't been defined from a previous save or
	       // read operation, ask the user for a filename.
               if (fFilename.Length() == 0) {
                  return ProcessMenu (M_FILE_SAVEAS, 0);
               }
	       // Modern file writes are fast enough that this is probably
	       // not needed.
               gVirtualX->SetCursor (fId, fWaitCursor);
               gVirtualX->Update();
	       // Transfer parameters from the GUI fields to the fParam structure
	       // (which is a struct TestParam_t defined in diagctrl.hh) 
	       // In the TestParam_t are variables to hold all the values that
	       // are seen in the GUI fields for Input, Measurment, and Excitation.
               TransferParameters (kFALSE);
	       // UpdateData 
               UpdateData();
               SavePlotSettings();
               SaveCalibrationData();
               string p = string ("save ") + 
                  FileFlag (fFileSaveFlag) + (const char*)fFilename;
               fCmdLine->clearEcho();
               fCmdLine->parse (p);
               gVirtualX->SetCursor (fId, kNone);
               if (!fCmdLine->getEcho().empty() &&
                  (fCmdLine->getEcho().back().find ("error") != 
                  string::npos)) {
                  string error = fCmdLine->getEcho().back();
                  new TGMsgBox (gClient->GetRoot(), this, "Error", 
                               error.c_str(), kMBIconStop, kMBOk);
               
               }
               break;
            }
         case M_FILE_SAVEAS:
            {
               // file save as dialog
               TGFileInfo	info;
               info.fFilename = 0;
               info.fIniDir = 0;
            #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
               info.fFileTypes = const_cast<const char**>(gSaveAsTypes);
            #else
               info.fFileTypes = const_cast<char**>(gSaveAsTypes);
            #endif
	    #if 1
	       info.fFileTypeIdx = 0 ; // Point to .xml
	       {
		  new TLGFileDialog(this, &info, kFDSave) ;
	       }
	       if (info.fFilename)
	    #else
               if (TLGFileDialog (this, info, kFDSave, ".xml"))
	    #endif
	       {
                  TransferParameters (kFALSE);
                  UpdateData();
                  SavePlotSettings();
                  SaveCalibrationData();
                  string filename (info.fFilename);
                  // now save it
                  gVirtualX->SetCursor (fId, fWaitCursor);
                  gVirtualX->Update();
                  string error = "";
                  fCmdLine->clearEcho();
                  fCmdLine->parse (string ("save ") + 
                                  FileFlag (fFileSaveFlag) + filename);
                  gVirtualX->SetCursor (fId, kNone);
                  // check for errors
                  if (!fCmdLine->getEcho().empty() &&
                     (fCmdLine->getEcho().back().find ("error") != 
                     string::npos)) {
                     error = fCmdLine->getEcho().back();
                  }
                  else {
                     fFilename = filename.c_str();
                      string n = fwindow_name + " - " + (const char *)fFilename;
                      SetWindowName (n.c_str());
                  }
                  if (!error.empty()) {
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  error.c_str(), kMBIconStop, kMBOk);
                  }
               }
            #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
               delete [] info.fFilename;
            #endif
               break;
            }
         case M_FILE_IMPORT:
            {
               fMainCtrl->GetPad()->ImportDlg (fImportDef);
               break;
            }
         case M_FILE_EXPORT:
            {
               fMainCtrl->GetPad()->ExportDlg (fExportDef);
               break;
            }
         case M_FILE_RFLAG_ALL:
            {
               fFileRestoreFlag = 0;
               fMenuFileFlag[0]->CheckEntry (M_FILE_RFLAG_ALL);
               fMenuFileFlag[0]->UnCheckEntry (M_FILE_RFLAG_STD);
               fMenuFileFlag[0]->UnCheckEntry (M_FILE_RFLAG_PRM);
               break;
            }
         case M_FILE_RFLAG_STD:
            {
               fFileRestoreFlag = 1;
               fMenuFileFlag[0]->UnCheckEntry (M_FILE_RFLAG_ALL);
               fMenuFileFlag[0]->CheckEntry (M_FILE_RFLAG_STD);
               fMenuFileFlag[0]->UnCheckEntry (M_FILE_RFLAG_PRM);
               break;
            }
         case M_FILE_RFLAG_PRM:
            {
               fFileRestoreFlag = 2;
               fMenuFileFlag[0]->UnCheckEntry (M_FILE_RFLAG_ALL);
               fMenuFileFlag[0]->UnCheckEntry (M_FILE_RFLAG_STD);
               fMenuFileFlag[0]->CheckEntry (M_FILE_RFLAG_PRM);
               break;
            }
         case M_FILE_RFLAG_SET:
            {
               fSettingsRestoreFlag = !fSettingsRestoreFlag;
               if (fSettingsRestoreFlag) {
                  fMenuFileFlag[0]->CheckEntry (M_FILE_RFLAG_SET);
               }
               else {
                  fMenuFileFlag[0]->UnCheckEntry (M_FILE_RFLAG_SET);
               }
               break;
            }
         case M_FILE_RFLAG_CAL:
            {
               fCalibrationRestoreFlag = !fCalibrationRestoreFlag;
               if (fCalibrationRestoreFlag) {
                  fMenuFileFlag[0]->CheckEntry (M_FILE_RFLAG_CAL);
               }
               else {
                  fMenuFileFlag[0]->UnCheckEntry (M_FILE_RFLAG_CAL);
               }
               break;
            }
         case M_FILE_SFLAG_ALL:
            {
               fFileSaveFlag = 0;
               fMenuFileFlag[1]->CheckEntry (M_FILE_SFLAG_ALL);
               fMenuFileFlag[1]->UnCheckEntry (M_FILE_SFLAG_STD);
               fMenuFileFlag[1]->UnCheckEntry (M_FILE_SFLAG_PRM);
               break;
            }
         case M_FILE_SFLAG_STD:
            {
               fFileSaveFlag = 1;
               fMenuFileFlag[1]->UnCheckEntry (M_FILE_SFLAG_ALL);
               fMenuFileFlag[1]->CheckEntry (M_FILE_SFLAG_STD);
               fMenuFileFlag[1]->UnCheckEntry (M_FILE_SFLAG_PRM);
               break;
            }
         case M_FILE_SFLAG_PRM:
            {
               fFileSaveFlag = 2;
               fMenuFileFlag[1]->UnCheckEntry (M_FILE_SFLAG_ALL);
               fMenuFileFlag[1]->UnCheckEntry (M_FILE_SFLAG_STD);
               fMenuFileFlag[1]->CheckEntry (M_FILE_SFLAG_PRM);
               break;
            }
         case M_FILE_SFLAG_SET:
            {
               fSettingsSaveFlag = !fSettingsSaveFlag;
               if (fSettingsSaveFlag) {
                  fMenuFileFlag[1]->CheckEntry (M_FILE_SFLAG_SET);
               }
               else {
                  fMenuFileFlag[1]->UnCheckEntry (M_FILE_SFLAG_SET);
               }
               break;
            }
         case M_FILE_SFLAG_CAL:
            {
               fCalibrationSaveFlag = !fCalibrationSaveFlag;
               if (fCalibrationSaveFlag) {
                  fMenuFileFlag[1]->CheckEntry (M_FILE_SFLAG_CAL);
               }
               else {
                  fMenuFileFlag[1]->UnCheckEntry (M_FILE_SFLAG_CAL);
               }
               break;
            }
         case M_FILE_PRINT:
            {
               fMainCtrl->GetPad()->PrintPSDlg (fPrintDef);
               break;
            }
         case M_FILE_PRINTSETUP:
            {
               fPrintDef->ShowDialog (fClient->GetRoot(), this,
                                    TLGPrintParam::kPrintSetup);
               break;
            }
         case M_FILE_PRINT_GRAPHA:
         case M_FILE_PRINT_GRAPHB:
            {
               TLGPrintParam pdlg = *fPrintDef;
               pdlg.fPlotSelection = parm1 - M_FILE_PRINT_GRAPHA;
               if (!fMainCtrl->GetPad()->PrintPS (pdlg)) {
                  TString msg = TString ("Unable to complete print job.");
                  new TGMsgBox(fClient->GetRoot(), this, "Error", 
                              msg, kMBIconStop, kMBOk);
               }
               break;
            }
         case M_FILE_EXIT:
            {
               CloseWindow();
               break;
            }
         
         case M_EDIT_START:
            {
               return ProcessButton (B_START, 0);
               break;
            }
         case M_EDIT_PAUSE:
            {
               return ProcessButton (B_PAUSE, 0);
               break;
            }
         case M_EDIT_RESUME:
            {
               return ProcessButton (B_RESUME, 0);
               break;
            }
         case M_EDIT_ABORT:
            {
               return ProcessButton (B_ABORT, 0);
               break;
            }
         case M_EDIT_READFROMTAPE:
            {
               if(!fMenuEdit->IsEntryChecked(M_EDIT_READFROMTAPE))
               {
                 setenv("NDS2_CLIENT_ALLOW_DATA_ON_TAPE", "1", 1);
               }
               else
               {
                 setenv("NDS2_CLIENT_ALLOW_DATA_ON_TAPE", "0", 1);
               }
               update_read_from_tape();
               break;
            }
         case M_MEAS_FFT:
            {
               fMainCtrl->SetMeasurement (0, kTRUE);
               break;
            }
         case M_MEAS_SWEPTSINE:
            {
               fMainCtrl->SetMeasurement (1, kTRUE);
               break;
            }
         case M_MEAS_SINERESP:
            {
               fMainCtrl->SetMeasurement (2, kTRUE);
               break;
            }
         case M_MEAS_TIMERESP:
            {
               fMainCtrl->SetMeasurement (3, kTRUE);
               break;
            }
         case M_MEAS_REPEAT:
            {
               fMainCtrl->SetIterator (0);
               break;
            }
         case M_MEAS_SCAN:
            {
               fMainCtrl->SetIterator (1);
               break;
            }
         case M_MEAS_OPTIMIZE:
            {
               fMainCtrl->SetIterator (2);
               break;
            }
         case M_PLOT_RESET:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->ResetPads();
               }
               break;
            }
         case M_PLOT_REFERENCE:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->ReferenceTracesDlg (fRefTraces);
               }
               break;
            }
         case M_PLOT_MATH:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->MathDlg (fMathTable);
               }
               break;
            }
         case M_PLOT_CALIBRATION_EDIT:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->CalibrationEditDlg (fCalTable);
               }
               break;
            }   
         case M_PLOT_CALIBRATION_READ:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->CalibrationImportDlg (fCalTable);
               }
               break;
            }   
         case M_PLOT_CALIBRATION_WRITE:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->CalibrationExportDlg (fCalTable);
               }
               break;
            }   
         case M_WINDOW_NEW:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->NewWindow();
               }
               break;
            }   
         case M_WINDOW_ZOOM_OUT:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->Zoom (-1);
               }
               break;
            }
         case M_WINDOW_ZOOM_CUR:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->Zoom (pad->GetActivePad());
               }
               break;
            }   
         case M_WINDOW_ZOOM_A:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->Zoom (0);
               }
               break;
            }
         case M_WINDOW_ZOOM_B:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->Zoom (1);
               }
               break;
            }
         case M_WINDOW_ACTIVE_NEXT:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  Int_t id = pad->GetActivePad() + 1;
                  if (id >= pad->GetPadNumber()) id = 0;
                  pad->SetActivePad (id);
               }
               break;
            }
         case M_WINDOW_ACTIVE_A:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->SetActivePad (0);
               }
               break;
            }
         case M_WINDOW_ACTIVE_B:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->SetActivePad (1);
               }
               break;
            }
         case M_WINDOW_LAYOUT:
            {
               TLGMultiPad* pad = fMainCtrl->GetPad();
               if (pad != 0) {
                  pad->OptionDlg ();
               }
               break;
            }
         case M_HELP_ABOUT:
            {
               new TGMsgBox (fClient->GetRoot(), this, "About", 
                            aboutmsg, 0, kMBOk);
               break;
            }
          case M_HELP_BUG:
          {
              ReportBug();
              break;
          }
	 case M_HELP_NOTES:
	    {
	       new TLGErrorDialog(gClient->GetRoot(), (TGWindow *) this,
				 diaggui_changes, "Release Notes") ;
	       break ;
	    }
      }
      return kTRUE;
   }


   Bool_t DiagMainWindow::ProcessButton (Long_t parm1, Long_t parm2) 
   {
      switch (parm1) {
         case B_START:
            {
               if (!IsRunning()) {
                  // remove previous results.
                  ClearResults (kFALSE, kFALSE);
		  // This is DiagTabControl::TransferParameters(kFALSE)
		  // Fill in the fParam structure with values from the GUI
		  // for the Input, Measurement, and Excitation tabs.
		  // Do this first so CheckChannels() will work.
		  fMainCtrl->TransferParameters (kFALSE);
		  // Make a pass thru the measurement and excitation channel
		  // lists to make sure the selected channels exist.  If they
		  // don't, throw up a warning dialog to see if we should
		  // continue.
		  if (CheckChannels() == 1)
		  {
		     TransferParameters (kFALSE);
		     // run
		     fCurMeasurement = 0;
		     fCurAverage = 0;
		     fNewMeasurement = kTRUE;
		     fCmdLine->clearEcho();
		     fCmdLine->parse ("run");
		     // check for errors
		     if (!fCmdLine->getEcho().empty() &&
			(fCmdLine->getEcho().back().find ("error") != 
			string::npos)) {
			string error = fCmdLine->getEcho().back();
			new TGMsgBox (gClient->GetRoot(), this, "Error", 
				     error.c_str(), kMBIconStop, kMBOk);
		     }
		     else {
			SetState (dsRunning);
		     }
		  }
               }
               break;
            }
         case B_PAUSE:
            {
               if (fState == dsRunning) {
                  fCmdLine->clearEcho();
                  fCmdLine->parse ("pause");
                  // check for errors
                  if (!fCmdLine->getEcho().empty() &&
                     (fCmdLine->getEcho().back().find 
                     ("error") != string::npos)) {
                     string error = fCmdLine->getEcho().back();
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  error.c_str(), kMBIconStop, kMBOk);
                     SetState (dsInactive);
                  }
                  else {
                     SetState (dsPaused);
                  }
               }
               break;
            }
         case B_RESUME:
            {
               if (fState == dsPaused) {
                  fCmdLine->clearEcho();
                  fCmdLine->parse ("resume");
                  // check for errors
                  if (!fCmdLine->getEcho().empty() &&
                     (fCmdLine->getEcho().back().find 
                     ("error") != string::npos)) {
                     string error = fCmdLine->getEcho().back();
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  error.c_str(), kMBIconStop, kMBOk);
                     SetState (dsInactive);
                  }
                  else {
                     SetState (dsRunning);
                  }
               }
               break;
            }
         case B_ABORT:
            {
               if (IsRunning()) {
                  fCmdLine->clearEcho();
                  fCmdLine->parse ("abort");
                  // check for errors
                  if (!fCmdLine->getEcho().empty() &&
                     (fCmdLine->getEcho().back().find 
                     ("error") != string::npos)) {
                     string error = fCmdLine->getEcho().back();
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  error.c_str(), kMBIconStop, kMBOk);
                     SetState (dsInactive);
                  }
                  else {
                     SetState (dsAborted);
                  }
               }
               break;
            }
      }
      return kTRUE;
   }


   Bool_t DiagMainWindow::ProcessStatus (Long_t parm1, Long_t parm2) 
   {
      switch (parm1) {
         case kCS_ITERATOR:
            {
               switch (parm2) {
                  case 0:
                     fStatus->SetText ("Repeat", 1);
                     break;
                  case 1:
                     fStatus->SetText ("Scan", 1);
                     break;
                  case 2:
                     fStatus->SetText ("Optimize", 1);
                     break;
               }
               break;
            }
         case kCS_MEASUREMENT:
            {
               switch (parm2) {
                  case 0:
                     fStatus->SetText ("Fourier tools", 2);
                     break;
                  case 1:
                     fStatus->SetText ("Swept sine response", 2);
                     break;
                  case 2:
                     fStatus->SetText ("Sine response", 2);
                     break;
                  case 3:
                     fStatus->SetText ("Triggered time response", 2);
                     break;
               }
               break;
            }
         case kCS_RUN:
            {
               SetState ((DiagState_t) parm2);
               break;
            }
      }
   
      return kTRUE;
   }


   Bool_t DiagMainWindow::HandleTimer (TTimer* timer)
   {
      if (timer == fXExitTimer) {
         if (!gXDisplay) gApplication->Terminate(0);
      }
      else if (timer == fHeartbeat) {
         static int fSkipHeartbeats = 0;
      
         // sometimes skip a heartbeat to avoid overloading the GUI
         if (fSkipHeartbeats) {
            fSkipHeartbeats--;
            timer->Reset();
            return kTRUE;
         }
         // check notification message queue
         while (true) {
            Bool_t newmsg = kFALSE;
            TString msg;
            if (fNotifyMsgs != 0) {
               fNotifyMsgs->mux.lock();
               if (fNotifyMsgs->size() > 0) {
                  msg = fNotifyMsgs->front().c_str();
                  fNotifyMsgs->pop_front();
                  newmsg = kTRUE;
               }
               fNotifyMsgs->mux.unlock();
            }
            if (!newmsg) {
               break;
            }
            fSkipHeartbeats = 3;
         
            // determine message id
            const char* const* nmsg = cmdnotifymsgs;
            int nnum = 0;
            while (nmsg[nnum] != 0) {
               if (strncmp (msg, nmsg[nnum], strlen (nmsg[nnum])) == 0) {
                  break;
               }
               nnum++;
            }
            // ignore unrecognized messages
            if ((nnum <= 0) || (nnum > 8)) {
               continue;
            }
             // ignore stacked up new test results
            if (nnum == 5) {
               fNotifyMsgs->mux.lock();
               if ((fNotifyMsgs->size() > 0) && 
                  (strncmp (fNotifyMsgs->front().c_str(), nmsg[nnum], 
                           strlen (nmsg[nnum])) == 0)) {
                  fNotifyMsgs->mux.unlock();
                  fCurAverage++;
                  continue;
               }
               fNotifyMsgs->mux.unlock();
            }
         
            // display error message on test failure
            if (nnum == 8) {
               TString errmsg = msg;
               string::size_type pos;
               if ((pos = errmsg.First('\n')) != string::npos) {
                  errmsg.Remove (0, pos + 1);
               }
               if ((errmsg.Length() > 0) &&
                  ((pos = errmsg.Last('\n')) == 
                  (string::size_type)(errmsg.Length() - 1))) {
                  errmsg.Remove (pos, 1);
               }
               errmsg.ReplaceAll ("\n", "; ");
               if (errmsg.Contains ("Test aborted", TString::kIgnoreCase)) {
                  nnum = 9;
               }
               SetStatusMsg (errmsg);
            }
            // send notification message and flush event queue
            ProcessNotify (nnum, 0);
         };
      }
   
      // reset timer and return
      timer->Reset();
      return kTRUE;
   }


   Bool_t DiagMainWindow::ProcessNotify (Long_t parm1, Long_t) 
   {
      switch (parm1) {
         // begin of test
         case 1:
            {
               // clear status
               SetStatusMsg ("");
               break;
            }
         // end of test
         case 2:
            {
               // set status
               char buf[512];
               sprintf (buf, "Test done (Measurements %i / Averages %i)", 
                       fCurMeasurement, fCurAverage);
               SetStatusMsg (buf);
               SetState (dsDone);
               break;
            }
         // begin of measurement
         case 3:
            {
               // update measurement time
               if (fCurMeasurement == 0) {
                  fParam->fMeasTime[0] = 0;
                  fParam->fMeasTime[1] = 0;
                  fCmdLine->getTime (varname (stTestTime), fParam->fMeasTime[0],
                                    fParam->fMeasTime[1]);
                  fMainCtrl->SetMeasurementTime();
               }
               // increase measurement number; update status
               fCurMeasurement++;
               fCurAverage = 0;
               fNewMeasurement = kTRUE;
               char buf[512];
               sprintf (buf, "Measurement %i / Average 0", fCurMeasurement);
               SetStatusMsg (buf);
               break;
            }
         // end of measurement
         case 4:
            {
               if (AddRawData (*fPlot, *fCmdLine, fCalTable)) {
                  fPlot->Update();
               }
               break;
            }
         // new test result
         case 5:
            {
               // create plot
               if (fPlot->Empty()) {
                  if (UpdatePlot (-1, kFALSE)) {
                     ShowDefaultPlot();
                  }
                  fNewMeasurement = kFALSE;
               }
               // update plot data
               else {
                  // new iteration step: create new plots if necessary
                  if (fNewMeasurement) {
                     UpdatePlot (fCurMeasurement - 1, kFALSE);
                     fNewMeasurement = kFALSE;
                  }
                  // redraw plots
                  BasicDataDescriptor* pl;
                  for (PlotSet::iterator iter = fPlot->begin();
                      iter != fPlot->end(); ++iter) {
                     pl = (BasicDataDescriptor*) iter->GetData();
                     if (pl != 0) {
                        pl->EraseData();
                        Int_t avg = 0;
                        auto new_avg = fCurAverage + 1;
                        if (iter->Param().GetAverages (avg) && 
                           (avg < new_avg)) {
                           iter->Param().SetAverages (new_avg);
                           if(my_debug) cerr << "Average = " << new_avg << endl;
                           if(iter->Param().GetUser()) {
                               string seek_text = "Name=\"Averages\" Type=\"int\">";
                               string tmpuser_text = iter->Param().GetUser();
                               auto avg_pos = tmpuser_text.find(seek_text);
                               if(avg_pos != string::npos) {
                                   auto end_pos = tmpuser_text.find("<", avg_pos);
                                   if(end_pos != string::npos) {
                                       auto end1 = avg_pos + seek_text.size();
                                       std::stringstream new_user_stream;
                                       new_user_stream << tmpuser_text.substr(0, end1) << new_avg << tmpuser_text.substr(end_pos);
                                       auto new_user = new_user_stream.str();
                                       iter->Param().SetUser(new_user.c_str());
                                   }
                               }
                           }
                           if(my_debug) {
                               const char * xmlcache = iter->Param().GetUser();
                               cerr << "XML cache for param:" << endl << xmlcache << endl;
                           }
                        }
                     }
                  }
                  // Update plot
                  if (fNewMeasurement) {
                     fPlot->Update();
                  }
                  else {
                     fPlot->Update(0);
                  }
               }
               // update status bar
               fCurAverage++;
               char buf[512];
               sprintf (buf, "Measurement %i / Average %i", 
                       fCurMeasurement, fCurAverage);
               SetStatusMsg (buf);
               break;
            }
         // new iterator result
         case 6:
            {
               break;
            }
         // data receiving error
         case 7:
            {
               char buf[512];
               sprintf (buf, "Data receiving error (skipping after "
                       "Measurement %i / Average %i", 
                       fCurMeasurement, fCurAverage);
               SetStatusMsg (buf);
               break;
            }
         // test error 
         case 8:
            {
               if (AddRawData (*fPlot, *fCmdLine, fCalTable)) {
                  fPlot->Update();
               }
               if ((fCurMeasurement > 1) || (fCurAverage > 0)) {
                  char buf[1024];
                  strcpy (buf, fStatusMsg);
                  sprintf (buf + strlen (buf), " (after Measurement %i / Average %i)", 
                          fCurMeasurement, fCurAverage);
                  SetStatusMsg (buf);
               }

//               fPlot->Clear(true);
//               fPlot->Update();

               SetState (dsFailed);
               break;
            }
          // test aborted
         case 9:
            {
               if (AddRawData (*fPlot, *fCmdLine, fCalTable)) {
                  fPlot->Update();
               }
               char buf[1024];
               strcpy (buf, fStatusMsg);
               sprintf (buf + strlen (buf), " (after Measurement %i / Average %i)", 
                       fCurMeasurement, fCurAverage);
               SetStatusMsg (buf);
               SetState (dsAborted);
               break;
            }
      }
      return kTRUE;
   }



   Bool_t DiagMainWindow::Notification (const char* msg)
   {
      if (fNotifyMsgs != 0) {
         fNotifyMsgs->mux.lock();
         fNotifyMsgs->push_back (string (msg));
         fNotifyMsgs->mux.unlock();
      }
      return kTRUE;
   }

   Bool_t DiagMainWindow::ReportBug()
   {
       const char bug_url[]="https://git.ligo.org/cds/dtt/issues/new";
//        //Try to spawn browser
//        bool browser_opened = false;
//
//#if __linux__
//        //if(!fork())
//        //{
//        //    execl("sensible-browser", "http://www.yahoo.com",0);
//        //}
//        string buf = string("sensible-browser") + " " + bug_url;
//        int exit_code = system(buf.c_str());
//        cerr << "system exit code: " << exit_code <<  endl;
//        if(!exit_code)
//        {
//            browser_opened = true;
//        }
//#else //system type.
//        //just fail automatically.
//#endif //system type
//
//        if(!browser_opened)
//        {
//            //don't know how to open in this case
//
//            string buf = string("The program doesn't know how to open a browser on your system. You can manually go to\n\n")
//                         + bug_url + "\n\nto report a bug.";
//            new TGMsgBox (gClient->GetRoot(), this, "Report Bug",
//                          buf.c_str(), 0, kMBOk);
//        }

        new ligogui::BugReportDlg(0, 0, bug_url, "contact+cds-dtt-5116-issue-@support.ligo.org");

        return kTRUE;
   }

   void DiagMainWindow::update_read_from_tape()
   {
     auto allow = getenv("NDS2_CLIENT_ALLOW_DATA_ON_TAPE");
     if(nullptr != allow && strcmp(allow, "0") )
     {
       fMenuEdit->CheckEntry(M_EDIT_READFROMTAPE);
     }
     else {
       fMenuEdit->UnCheckEntry(M_EDIT_READFROMTAPE);
     }
   }

}
