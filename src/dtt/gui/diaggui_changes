Version 2.16.18, December 2014

* Bugzilla 288, Ramp down excitations for Fourier and Swept Sine
  transfer functions.  Added widget to Fourier Tools GUI to allow 
  user to set time of ramp down of excitation when transfer function 
  is aborted or test ends.  Note this does not ramp up excitation, 
  just ramp down at the end.

Version 2.16.17, November 2014

* Bugzilla 761, graph line width control changed to integer values
  greater than 0.
  
* Bugzilla 754, graph legend background is red by default using
  root version 5.34.21, correct by explicitly setting the color to white.

* Bugzilla 755, file filter for PNG files is incorrect in print
  Save As dialog box.

Version 2.16.15, October 2014
* Bugzilla 687, Add copy/paste capability to Calibration dialog box, 
  make fields wider to accomodate longer channel names.

Version 2.16.12.2, Sept 2014
* Fix bug that caused long channel names to be ignored in the 
  Calibration dialog box.

* Bugzilla 688, Add awg.py and awgbase.py to GDS for access to awg
  functions via python.

Version 2.16.12.1, June 2014

* Bug fix to cure inability of diaggui to run for extended
  periods of time making continuous measurements (Linux).
  
* Added preliminary code to allow diag to read 32 bit unsigned
  integers, but it's untested and diag won't do anything useful
  with a 32 bit unsigned int.

* Changed size of channel selection menus to make fields wider
  to accomodate longer channel names.
  
Version 2.16.12, March 2014

* Bugzilla 638, Can't apply filters to some excitations has been
  fixed.

Version 2.16.9.1, February 2014

* Bugzilla 254, Improper display of reference traces read from 
  xml file has been fixed.

Version 2.16.9, January 2014

* Bugzilla 519, use environment variable LIGO_RT_BCAST to 
  determine the broadcast address to use when discovering awg and
  tp services.

* Removed "Kaiser" window option, as no code was ever written to 
  support it.

* Fixed bugzilla 299, 328, 389, 428, Improper dft results when using
  certain combinations of start, stop, and BW values.

* Updates to compile with gcc version 4.7.x

Version 2.16.5, October 2012

* Updated diag to use larger buffers so more than 128 tp and awg processes
  could be used.  

* Added support for generating .png files for plots. (Bugzilla 420)

* Added the ability to read release notes from the Help menu in diaggui.

* Expand channel field width in fields and menus that display channel
  names.

* Add levels of hierarchy in display of channel names in selection menus.

* Fixed bugzilla 325, segfault using --help option.

Version 2.16.3, July 2012

* Modified code for leap second of June 30, 2012.

$Id: diaggui_changes 7276 2015-01-09 21:36:34Z james.batch@LIGO.ORG $
