/* -*- mode: c++; c-basic-offset: 3; -*- */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  diagctrl						     */
/*                                                                           */
/* Module Description:  control tab of diagnostics tests		     */
/*                                                                           */
/*---------------------------------------------------------------------------*/

#include <time.h>
#include <ctype.h>
#include <dlfcn.h>
#include <iostream>
#include <sstream>
#include <string.h>
#include <strings.h>
#include <cstdlib>
#include <TGFrame.h>
#include <TGText.h>
#include <TGMsgBox.h>
#include <TVirtualX.h>
#include "PConfig.h"
#include "TLGEntry.hh"
#include "TLGChannelBox.hh"
#include "TLGTextEditor.hh"
#include "TLGComboEditBox.hh"
#include "TLGPad.hh"
#include "TLGPlot.hh"
#include "TLGExport.hh"
#include "dfmtype.hh"
#include "dfmsends.hh"
#include "TLGDfmSel.hh"
#include "TLGDfmTimes.hh"
#include "tconv.h"
#include "diagctrl.hh"
#include "diagmain.hh"
#include "nds.hh"
#include "math.h"

   using namespace std;


//______________________________________________________________________________
extern "C" {
   typedef void (*func_t) (void);
   typedef bool (*wizfunc_t) (const string& name, string& filter);
}

//______________________________________________________________________________
#ifdef P__WIN32
#define RTLD_LOCAL	0
   const char* const libname = "libfilterwiz.dll";
#else

#ifdef P__DARWIN
   const char* const libname = "libfilterwiz.dylib" ;
#else
   const char* const libname = "libfilterwiz.so";
#endif

#endif
   const char* const fn_wiz  = "wizard___dynamic";

   static const int my_debug = 0 ;

   static bool libloaded = false;        // library loaded
   static void* handle = 0;              // library handle
   static func_t dispatch[10];           // dispatch table
   static const char* const funcname[] = // list of functions
   {fn_wiz, 0};

//______________________________________________________________________________
   static func_t getFunc (int num = 0)
   {
      // dynamically load plot library
      if (!libloaded) {
         // load librray
         handle = ::dlopen (libname, RTLD_NOW | RTLD_LOCAL);
         if (handle == 0) {
            cerr << "Unable to load library " << libname << 
               endl;
            return 0;
         }
         // resolve dispatch table
         for (int i = 0; funcname[i]; ++i) {
            dispatch[i] = (func_t) dlsym (handle, funcname[i]);
            if (dispatch[i] == 0) {
               cerr << "load failed for " << funcname[i] << 
                  endl;
               return 0;
            }
         }
         libloaded = true;
      }
      return dispatch[num];
   }

//______________________________________________________________________________
namespace diag {
   using namespace ligogui;


   // Why is this defined here instead of being pulled from 
   // a system header file???
   const double kPi = 3.1415926535897932384626433832795029;
   const double kDeg = kPi/180.;


   static const char* const gSweepFileTypes[] = { 
   "Text files", "*.txt",
   "All files", "*",
   0, 0 };

   Cursor_t DiagTabControl::fWaitCursor = (Cursor_t)-1;


   inline void Swap (double& x, double& y) {
      double z = x; x = y; y = z; }


   // Set the default values for test parameters
   void defaultTestParameters (TestParam_t& p, Int_t id)
   {
      p.fMeasTime[0] = p.fMeasTime[1] = 0;
      // data source
      p.fData.fReconnect = kFALSE;
   //#ifdef GDS_ONLINE
      p.fData.fOnline = kTRUE;
   //#else
   //   p.fData.fOnline = kFALSE;
   //#endif
      p.fData.fUserNDS = DataSourceParam_t::kNoNDS;
      p.fData.fNDSName = "";
      p.fData.fNDSPort = 31200;
      p.fData.fNDS2Name = "" ;
      p.fData.fNDS2Port = 31200 ;
      p.fData.fNDS2EpochName = "User specified" ;
      p.fData.fNDS2Epoch[0] = 0 ;
      {
	 tainsec_t now = TAInow() ;
	 p.fData.fNDS2Epoch[1] = now / _ONESEC ;
      }
      p.fData.fDacc.support (dfm::st_File); // this will be default
      p.fData.fDacc.support (dfm::dataaccess::kSuppAll);
      // no shared memory or tape support
      p.fData.fDacc.support (dfm::st_SM, false);
      p.fData.fDacc.support (dfm::st_Tape, false);
   
      // measurement
      if ((id < 0) || (id >= 4)) {
         id = 0;
      }
      p.fMeas.fMeasType = id;
      for (int i = 0; i < kMaxMeasChannel; i++) {
         p.fMeas.fMeasActive[i] = kFALSE;
#ifdef TEST_RATE
         p.fMeas.fMeasChn[i].first = "" ;
         p.fMeas.fMeasChn[i].second = 0 ;
#else
         p.fMeas.fMeasChn[i] = "";
#endif
         p.fMeas.fMeasChnUsed[i] = kFALSE;
      }
      p.fMeas.fStart = (id == 1) ? 1 : 0;
      p.fMeas.fStop = 900;
      p.fMeas.fResolutionBW = 1;
      p.fMeas.fSignalBW = 10000;
      p.fMeas.fRampDownTime = 1.0 ;
      p.fMeas.fRampUpTime = 0.0 ;
      p.fMeas.fTimeSettling = (id == 3) ? 0 : 0.1;
      p.fMeas.fWindow = 1;
      p.fMeas.fOverlap = 0.5;
      p.fMeas.fRemoveDC = kTRUE;
      p.fMeas.fAChannels = 0;
      p.fMeas.fAverages = ((id == 1) || (id == 2)) ? 3 : ((id == 3) ? 1 : 10);
      p.fMeas.fBurstNoiseQuietTime_s = 0;
      p.fMeas.fAverageType = 0;
      p.fMeas.fPoints = 61;
      p.fMeas.fDoublePrecFreq = 0;
      p.fMeas.fTimeMeas[0] = (id == 3) ? 1 : 0.1;
      p.fMeas.fTimeMeas[1] = 10;
      p.fMeas.fHarmonicOrder = 1;
      p.fMeas.fPowerSpec = kFALSE;
      p.fMeas.fSweepDir = 1;
      p.fMeas.fSweepType = 1;
      p.fMeas.fSweepPoints.reset (0);
      p.fMeas.fTimePreTrig = 0;
      p.fMeas.fTimeDead = 0;
      p.fMeas.fStatistics = kFALSE;
      p.fMeas.fFilter = "";
   
      // excitation
      for (int i = 0; i < kMaxExcChannel; i++) {
         p.fExc[i].fUsed = kFALSE;
         p.fExc[i].fActive = kFALSE;
         p.fExc[i].fName = "";
         p.fExc[i].fRBType = 0;
         p.fExc[i].fRBName = "";
         p.fExc[i].fWaveform = 0;
         p.fExc[i].fWaveformFile = "";
         p.fExc[i].fFreq = 0;
         p.fExc[i].fAmpl = 0;
         p.fExc[i].fOffs = 0;
         p.fExc[i].fPhase = 0;
         p.fExc[i].fRatio = 0.5;
         p.fExc[i].fFreqRange = 10000;
         p.fExc[i].fAmplRange = 0;
         p.fExc[i].fFilterCmd = "";
      }
   
      // sync
      p.fSync.fType = 0;
      tainsec_t t0 = TAInow();
      p.fSync.fStart[0] = t0 / 1000000000LL;
      p.fSync.fStart[1] = 0;
      p.fSync.fWait = 0;
      p.fSync.fSlowDown = 0;
      // iterator
      p.fIter.fIterType = 0;
      p.fIter.fRepeat = 1;
   }



   class WindowCombobox : public TGComboBox {
   public:
      WindowCombobox (TGWindow* p, Int_t id) : TGComboBox (p, id) {
         Resize (110, 22);
         AddEntry ("Uniform", 0);
         AddEntry ("Hanning", 1);
         AddEntry ("Flat-top", 2);
         AddEntry ("Welch", 3);
         AddEntry ("Bartlett", 4);
         AddEntry ("BMH", 5);
         AddEntry ("Hamming", 6);
	 // Remove Kaiser, it is not supported in SignalProcessing/dttalgo/window.c
         // AddEntry ("Kaiser", 7);
         Select (0);
      }
   };


   class WaveformSelection : public TGComboBox {
   protected:
      Bool_t 	fActive;
   
   public:
      WaveformSelection (TGWindow* p, Int_t id) : TGComboBox (p, id) {
         fActive = kTRUE;
         Resize (150, 22);
         AddEntry ("None", 0);
         AddEntry ("Sine", 1);
         AddEntry ("Square", 2);
         AddEntry ("Ramp", 3);
         AddEntry ("Triangle", 4);
         AddEntry ("Impulse", 5);
         AddEntry ("Offset", 6);
         AddEntry ("Noise (Gauss)", 7);
         AddEntry ("Noise (Uniform)", 8);
         AddEntry ("Arbitrary", 9);
         AddEntry ("Sweep (linear)", 10);
         AddEntry ("Sweep (log)", 11);
         Select (1);
      }
      // Set state
      virtual void SetState (Bool_t active) {
         fActive = active; fComboFrame->EndPopup();
      }
      // Get state
      virtual Bool_t GetState () const {
         return fActive; }
      // Handle button
      virtual Bool_t HandleButton(Event_t *event)
      {
         if (!fActive) {
            return kTRUE; 
         }
         else {
            return TGComboBox::HandleButton (event);
         }
      }
   };



   class  FrameOverlayLayout : public TGLayoutManager {
   protected:
      /// Parent frame
      TGCompositeFrame*	fMain;
      /// List of subframes in parent
      TList*		fList;
   
   public:
      FrameOverlayLayout (TGCompositeFrame* p) :
      TGLayoutManager (), fMain (p), fList (p->GetList()) {
      }
      virtual void Layout();
      virtual TGDimension GetDefaultSize() const;
   };



   class FrameOverlay : public TGCompositeFrame {
   protected: 
      /// Currently selected overlay
      Int_t		fOverlay;
   
   public:
      FrameOverlay (TGWindow* p) 
      : TGCompositeFrame (p, 1, 1), fOverlay (0) {
         SetLayoutManager (new FrameOverlayLayout (this));
      }
      Int_t AddOverlay (TGFrame* f) {
         AddFrame (f, 0); 
         return fList->GetSize() - 1; }
      void SelectOverlay (Int_t ol) {
         fOverlay = ol; Layout(); }
      Int_t GetOverlay () const {
         return fOverlay; }
   };



   void FrameOverlayLayout::Layout()
   {
      UInt_t w = fMain->GetWidth();
      UInt_t h = fMain->GetHeight();
      TGFrameElement* el;
      TIter next (fList);
      Int_t ol = ((FrameOverlay*)fMain)->GetOverlay();
      if (ol >= fList->GetSize()) {
         ol = 0;
      }
      Int_t cur = 0;
      while ((el = (TGFrameElement*) next())) {
         if (cur == ol) {
            el->fFrame->Resize (w, h);
            el->fFrame->RaiseWindow();
         }
         else {
            el->fFrame->Resize (w, h);
            el->fFrame->LowerWindow();
         }
         cur++;
      }
   }


   TGDimension FrameOverlayLayout::GetDefaultSize() const
   {
      TGDimension size (0, 0);
      TGDimension dsize;
      TGFrameElement* el;
      TIter next (fList);
      while ((el = (TGFrameElement*) next())) {
         dsize = el->fFrame->GetDefaultSize();
         if (size.fWidth < dsize.fWidth) size.fWidth = dsize.fWidth;
         if (size.fHeight < dsize.fHeight) size.fHeight = dsize.fHeight;
      }
      size.fWidth += fMain->GetBorderWidth() << 1;
      size.fHeight += fMain->GetBorderWidth() << 1;
      return size;
   }



   DiagTabControl::DiagTabControl (const TGWindow* p, DiagMainWindow* diag,
                     TestParam_t& param, PlotSet* plots,
                     const char* measchns, const char* excchns)
   : TGTab (p, 10, 10), fDiag (diag), fParam (&param), 
     fMeasChannelList (measchns), fMeasChnList (0), fMeasChnLen (0), 
     fExcChannelList (excchns), fExcChnList (0), fExcChnLen (0), 
     fDataChnList (0), fDataChnLen (0), fOnlineChannels (kTRUE), 
     fUserNDSChannels (DataSourceParam_t::kNoNDS), fPlot (plots), 
     fMeasSel (-1), fIterSel (-1), fMeasRangeSel (0), fExcRangeSel (0)
   {
      if (fWaitCursor == (Cursor_t)-1) {
         fWaitCursor = gVirtualX->CreateCursor (kWatch);
      }
      // make online channel list for measurement and excitation.
      TLGChannelCombobox::MakeChannelList (fMeasChannelList, fMeasChnList, 
					   fMeasChnLen, 
					   kChannelTreeSeparateSlow);
      TLGChannelCombobox::MakeChannelList (fExcChannelList, fExcChnList, 
					   fExcChnLen);
      // Input tab
      fInputTab = AddTab ("      Input      ");
      fInputGroupLayout = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 4, 4, 6, 0);
         // Source selection
      fInputF1 = new TGGroupFrame (fInputTab, "Data Source Selection");
      fInputTab->AddFrame (fInputF1, fInputGroupLayout);
         // NDS selection
      fInputF2 = new TGGroupFrame (fInputTab, "NDS Selection");
      fInputTab->AddFrame (fInputF2, fInputGroupLayout);
         // NDS2 selection
      fInputF3 = new TGGroupFrame (fInputTab, "NDS2 Selection") ;
      fInputTab->AddFrame (fInputF3, fInputGroupLayout) ;
         // lidax box
      fSource = new dfm::TLGDfmSelection (fInputTab, fParam->fData.fDacc, 
                           true, "LiDaX Data Source", 
                           kLidaxSource, true, false, true);
      fSource->Associate (this);
      fInputTab->AddFrame (fSource, fInputGroupLayout);
      // Data Source selection
      fInput1Layout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 2);
      fInput1Layout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -6);
      fInput1Layout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fInput1Layout[3] = 
         new TGLayoutHints (kLHintsRight | kLHintsCenterY, 2, 2, 2, 2);
      fDataSelFrame[0] = new TGHorizontalFrame (fInputF1, 10, 10);
      fInputF1->AddFrame (fDataSelFrame[0], fInput1Layout[2]);
      fDataInput[0] = new TGRadioButton (fDataSelFrame[0], 
                           "Online system  ", kDataInput);
      fDataInput[0]->Associate (this);
      fDataInput[0]->SetState (kButtonDown);
      fDataSelFrame[0]->AddFrame (fDataInput[0], fInput1Layout[1]);
      fDataInput[1] = new TGRadioButton (fDataSelFrame[0], 
                           "User NDS      ", kDataInput + 1);
      fDataInput[1]->Associate (this);
      fDataSelFrame[0]->AddFrame (fDataInput[1], fInput1Layout[1]);
      fDataInput[3] = new TGRadioButton (fDataSelFrame[0], 
                           "NDS2          ", kDataInput + 3);
      fDataInput[3]->Associate (this);
      fDataSelFrame[0]->AddFrame (fDataInput[3], fInput1Layout[1]);
      fDataInput[2] = new TGRadioButton (fDataSelFrame[0], 
                           "LiDaX         ", kDataInput + 2);
      fDataInput[2]->Associate (this);
      fDataSelFrame[0]->AddFrame (fDataInput[2], fInput1Layout[1]);
   
      fReconnectInput = new TGCheckButton (fDataSelFrame[0], 
                           "Reconnect", kDataReconnect);
      fReconnectInput->Associate (this);
      fDataSelFrame[0]->AddFrame (fReconnectInput, fInput1Layout[1]);
      fCacheClear = new TGTextButton (fDataSelFrame[0], 
                           "  Clear cache  ", kDataCacheClear);
      fCacheClear->Associate (this);
      fDataSelFrame[0]->AddFrame (fCacheClear, fInput1Layout[3]);
   
      // NDS selection
      fDataSelFrame[1] = new TGHorizontalFrame (fInputF2, 10, 10);
      fInputF2->AddFrame (fDataSelFrame[1], fInput1Layout[2]);
   
      fDataSelLabel[0] = new TGLabel (fDataSelFrame[1], "Server:      ");
      fDataSelFrame[1]->AddFrame (fDataSelLabel[0], fInput1Layout[1]);
      //fNDSName = new TGTextEntry (fDataSelFrame[1], "", kNDSName);
      fNDSName = new TLGComboEditBox (fDataSelFrame[1], kNDSName);
      fNDSName->Resize (327, 24);
      fNDSName->Associate (this);
      fDataSelFrame[1]->AddFrame (fNDSName, fInput1Layout[1]);
      fDataSelLabel[1] = new TGLabel (fDataSelFrame[1], "    Port: ");
      fDataSelFrame[1]->AddFrame (fDataSelLabel[1], fInput1Layout[1]);
      fNDSPort = new TLGNumericControlBox (fDataSelFrame[1], 0, 6, 
                           kNDSPort, kNESInteger, kNEANonNegative);
      fNDSPort->Associate (this);
      fDataSelFrame[1]->AddFrame (fNDSPort, fInput1Layout[1]);

      // NDS2 selection
      // Server selection frame
      fNDS2ServerFrame = new TGHorizontalFrame (fInputF3, 10, 10) ;
      fInputF3->AddFrame (fNDS2ServerFrame, fInput1Layout[2]) ;

      fNDS2Label[0] = new TGLabel (fNDS2ServerFrame, "Server:      ") ;
      fNDS2ServerFrame->AddFrame (fNDS2Label[0], fInput1Layout[1]) ;

      fNDS2Name = new TLGComboEditBox (fNDS2ServerFrame, kNDS2Name) ;
      fNDS2Name->Resize (327, 24) ;
      fNDS2Name->Associate (this) ;
      fNDS2ServerFrame->AddFrame (fNDS2Name, fInput1Layout[1]) ;

      fNDS2Label[1] = new TGLabel (fNDS2ServerFrame, "    Port: ") ;
      fNDS2ServerFrame->AddFrame (fNDS2Label[1], fInput1Layout[1]) ;

      fNDS2Port = new TLGNumericControlBox (fNDS2ServerFrame, 0, 6,
			kNDS2Port, kNESInteger, kNEANonNegative) ;
      fNDS2Port->Associate (this) ;
      fNDS2ServerFrame->AddFrame (fNDS2Port, fInput1Layout[1]) ;

      fNDS2Label[2] = new TGLabel (fNDS2ServerFrame, "    Epoch: ") ;
      fNDS2ServerFrame->AddFrame (fNDS2Label[2], fInput1Layout[1]) ;

      fNDS2EpochName = new TGComboBox (fNDS2ServerFrame, kNDS2EpochName) ;
      fNDS2EpochName->Resize (327, 24) ;
      fNDS2EpochName->Associate (this) ;
      fNDS2ServerFrame->AddFrame (fNDS2EpochName, fInput1Layout[1]) ;
      fNDS2EpochName->AddEntry("User specified", 0) ;
      fNDS2EpochName->Select(0) ;

      // Epoch selection frame - holds two vertical frames.
      fNDS2EpochFrame = new TGHorizontalFrame (fInputF3, 10, 10) ;
      fInputF3->AddFrame (fNDS2EpochFrame, fInput1Layout[2]) ;
      
      ////////////////////////////////////////////////////////////////////
      // Epoch start frame - holds a label and 2 horizontal frames.
      fNDS2EpochStartFrame = new TGGroupFrame (fNDS2EpochFrame, "Epoch Start") ;
      fNDS2EpochFrame->AddFrame (fNDS2EpochStartFrame, fInput1Layout[1]) ;

      // GPS time entry for epoch start
      fNDS2EpochHFrame[0] = new TGHorizontalFrame(fNDS2EpochStartFrame, 10,10) ;
      fNDS2EpochStartFrame->AddFrame (fNDS2EpochHFrame[0], fInput1Layout[1]) ;
      fNDS2Label[3] = new TGLabel (fNDS2EpochHFrame[0], "GPS:        ") ;
      fNDS2EpochHFrame[0]->AddFrame(fNDS2Label[3], fInput1Layout[1]) ;
      fNDS2EpochStartGPS = new TLGNumericControlBox (fNDS2EpochHFrame[0], 0., 12, 
      				kNDS2EpochStartGPS, kNESInteger, kNEANonNegative);
      fNDS2EpochStartGPS->Associate (this) ;
      fNDS2EpochHFrame[0]->AddFrame(fNDS2EpochStartGPS, fInput1Layout[1]) ;
      fNDS2Label[4] = new TGLabel (fNDS2EpochHFrame[0], " sec") ;
      fNDS2EpochHFrame[0]->AddFrame (fNDS2Label[4], fInput1Layout[1]) ;

      // Date/Time radio button and time entry
      fNDS2EpochHFrame[1] = new TGHorizontalFrame (fNDS2EpochStartFrame, 10, 10) ;
      fNDS2EpochStartFrame->AddFrame (fNDS2EpochHFrame[1], fInput1Layout[1]) ;

      fNDS2Label[5] = new TGLabel (fNDS2EpochHFrame[1], "Date/Time: ") ;
      fNDS2EpochHFrame[1]->AddFrame(fNDS2Label[5], fInput1Layout[1]) ;

      fNDS2EpochStartDate = new TLGNumericControlBox(fNDS2EpochHFrame[1], 0., 12,  
				 kNDS2EpochStartDate, kNESDayMYear) ;
      fNDS2EpochStartDate->Associate (this) ;
      fNDS2EpochHFrame[1]->AddFrame(fNDS2EpochStartDate, fInput1Layout[1]) ;

      fNDS2Label[6] = new TGLabel (fNDS2EpochHFrame[1], "dd/mm/yyyy  ") ;
      fNDS2EpochHFrame[1]->AddFrame(fNDS2Label[6], fInput1Layout[1]) ;

      fNDS2EpochStartTime =  new TLGNumericControlBox(fNDS2EpochHFrame[1], 0., 10,
				 kNDS2EpochStartTime, kNESHourMinSec, kNEANonNegative) ;
      fNDS2EpochStartTime->Associate (this) ;
      fNDS2EpochHFrame[1]->AddFrame (fNDS2EpochStartTime, fInput1Layout[1]) ;

      fNDS2Label[7] = new TGLabel (fNDS2EpochHFrame[1], "hh:mm:ss UTC") ;
      fNDS2EpochHFrame[1]->AddFrame(fNDS2Label[7], fInput1Layout[1]) ;


      
      ////////////////////////////////////////////////////////////////////
      // Epoch stop frame - holds a label and 2 horizontal frames.
      fNDS2EpochStopFrame = new TGGroupFrame (fNDS2EpochFrame, "Epoch Stop") ;
      fNDS2EpochFrame->AddFrame (fNDS2EpochStopFrame, fInput1Layout[1]);

      // GPS time entry for epoch stop
      fNDS2EpochHFrame[2] = new TGHorizontalFrame(fNDS2EpochStopFrame, 10,10) ;
      fNDS2EpochStopFrame->AddFrame (fNDS2EpochHFrame[2], fInput1Layout[1]) ;
      fNDS2Label[8] = new TGLabel (fNDS2EpochHFrame[2], "GPS:        ") ;
      fNDS2EpochHFrame[2]->AddFrame(fNDS2Label[8], fInput1Layout[1]) ;
      fNDS2EpochStopGPS = new TLGNumericControlBox (fNDS2EpochHFrame[2], 0., 12,
                                kNDS2EpochStopGPS, kNESInteger, kNEANonNegative);
      fNDS2EpochStopGPS->Associate (this) ;
      fNDS2EpochHFrame[2]->AddFrame(fNDS2EpochStopGPS, fInput1Layout[1]) ;
      fNDS2Label[9] = new TGLabel (fNDS2EpochHFrame[2], " sec") ;
      fNDS2EpochHFrame[2]->AddFrame (fNDS2Label[9], fInput1Layout[1]) ;

      // Date/Time radio button and time entry
      fNDS2EpochHFrame[3] = new TGHorizontalFrame (fNDS2EpochStopFrame, 10, 10) ;
      fNDS2EpochStopFrame->AddFrame (fNDS2EpochHFrame[3], fInput1Layout[1]) ;

      fNDS2Label[10] = new TGLabel (fNDS2EpochHFrame[3], "Date/Time: ") ;
      fNDS2EpochHFrame[3]->AddFrame(fNDS2Label[10], fInput1Layout[1]) ;

      fNDS2EpochStopDate = new TLGNumericControlBox(fNDS2EpochHFrame[3], 0., 12,
                                 kNDS2EpochStopDate, kNESDayMYear) ;
      fNDS2EpochStopDate->Associate (this) ;
      fNDS2EpochHFrame[3]->AddFrame(fNDS2EpochStopDate, fInput1Layout[1]) ;

      fNDS2Label[11] = new TGLabel (fNDS2EpochHFrame[3], "dd/mm/yyyy  ") ;
      fNDS2EpochHFrame[3]->AddFrame(fNDS2Label[11], fInput1Layout[1]) ;

      fNDS2EpochStopTime =  new TLGNumericControlBox(fNDS2EpochHFrame[3], 0., 10, 
                                 kNDS2EpochStopTime, kNESHourMinSec, kNEANonNegative) ;
      fNDS2EpochStopTime->Associate (this) ;
      fNDS2EpochHFrame[3]->AddFrame (fNDS2EpochStopTime, fInput1Layout[1]) ;

      fNDS2Label[12] = new TGLabel (fNDS2EpochHFrame[3], "hh:mm:ss UTC") ;
      fNDS2EpochHFrame[3]->AddFrame(fNDS2Label[12], fInput1Layout[1]) ;

      // Measurement tab
      fMeasTab = AddTab ("Measurement");
      fMeasGroupLayout = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 4, 4, 6, 0);
      fMeasF1 = new TGGroupFrame (fMeasTab, "Measurement");
      fMeasTab->AddFrame (fMeasF1, fMeasGroupLayout);
      fMeasF2 = new TGGroupFrame (fMeasTab, "Measurement Channels");
      fMeasTab->AddFrame (fMeasF2, fMeasGroupLayout);
      fMeasTest = new FrameOverlay (fMeasTab);
      fMeasTab->AddFrame (fMeasTest, fMeasGroupLayout);
      fTimeF = new TGGroupFrame (fMeasTab, "Start Time");
      fMeasTab->AddFrame (fTimeF, fMeasGroupLayout);
      fInfoF = new TGGroupFrame (fMeasTab, "Measurement Information");
      fMeasTab->AddFrame (fInfoF, fMeasGroupLayout);
         // Individual tests
      fMeasFFT = new TGGroupFrame (fMeasTest, "Fourier Tools");
      ((FrameOverlay*)fMeasTest)->AddOverlay (fMeasFFT);
      fMeasSS = new TGGroupFrame (fMeasTest, "Swept Sine Response");
      ((FrameOverlay*)fMeasTest)->AddOverlay (fMeasSS);
      fMeasSR = new TGGroupFrame (fMeasTest, "Sine Response");
      ((FrameOverlay*)fMeasTest)->AddOverlay (fMeasSR);
      fMeasTS = new TGGroupFrame (fMeasTest, "Triggered Time Response");
      ((FrameOverlay*)fMeasTest)->AddOverlay (fMeasTS);
   
      // Measurement selection
      fMeas1Layout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 2);
      fMeas1Layout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -6);
      fMeas1Layout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fMeasSelFrame[0] = new TGHorizontalFrame (fMeasF1, 10, 10);
      fMeasF1->AddFrame (fMeasSelFrame[0], fMeas1Layout[2]);
         // 1st line
      fMeasType[0] = new TGRadioButton (fMeasSelFrame[0], 
                           "Fourier Tools  ", kMeasSel);
      fMeasType[0]->Associate (this);
      fMeasSelFrame[0]->AddFrame (fMeasType[0], fMeas1Layout[1]);
      fMeasType[1] = new TGRadioButton (fMeasSelFrame[0], 
                           "Swept Sine Response  ", kMeasSel + 1);
      fMeasType[1]->Associate (this);
      fMeasSelFrame[0]->AddFrame (fMeasType[1], fMeas1Layout[1]);
      fMeasType[2] = new TGRadioButton (fMeasSelFrame[0], 
			   "Sine Response  ", kMeasSel + 2);
      fMeasType[2]->Associate (this);
      fMeasSelFrame[0]->AddFrame (fMeasType[2], fMeas1Layout[1]);
      fMeasType[3] = new TGRadioButton (fMeasSelFrame[0], 
                           "Triggered Time Response  ", kMeasSel + 3);
      fMeasType[3]->Associate (this);
      fMeasSelFrame[0]->AddFrame (fMeasType[3], fMeas1Layout[1]);
      fMeasType[0]->SetState (kButtonDown);
   
         // Measurement channel selection
      fMeasSelFrame[1] = new TGHorizontalFrame (fMeasF2, 10, 10);
      fMeasF2->AddFrame (fMeasSelFrame[1], fMeas1Layout[0]);

      /* Radio buttons for range of measurement channels. */
      for (int i = 0; i < kMaxMeasChannel / kShowMeasChannel; i++) {
         char buf[256];
         sprintf (buf, "Channels %i to %i  ", i * kShowMeasChannel,
                 (i + 1) * kShowMeasChannel - 1);
         fMeasNum[i] = new TGRadioButton (fMeasSelFrame[1], buf, kMeasNum + i);
         fMeasNum[i]->Associate (this);
         fMeasSelFrame[1]->AddFrame (fMeasNum[i], fMeas1Layout[1]);
      }
      fMeasNum[0]->SetState (kButtonDown);

      // Channel selection boxes
      fMeas2Layout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fMeas2Layout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 5, 0, 0);
      fMeas2Layout[2] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 0, 0, 0);
      fMeas2Layout[3] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 8, 0, 0);
      // For 8 rows of 2 channels.
      Int_t chnh = 8 ; // Number of rows in measurement channel section 
      Int_t chntype = kChannelTreeShowRate | kChannelTreeSeparateSlow;
      if (fMeasChnLen > 1000) chntype |= kChannelTreeLevel3;
      if (fMeasChnLen > 5000) chntype |= kChannelTreeLevel6 ; // JCB - for added hierarchy
      for (Int_t chn = 0; chn < kShowMeasChannel; chn++) {
         if (chn < chnh) {
            fMChnF[chn] = new TGHorizontalFrame (fMeasF2, 10, 10);
            fMeasF2->AddFrame (fMChnF[chn], fMeas2Layout[0]);   
         }
         char chnnum[10];
         if (chn < chnh) {
            sprintf (chnnum, "%3i", chn);
         }
         else {
            sprintf (chnnum, "%4i", chn);
         }

	 /* Channel number to the left of active check box */
         fMChnLabel[chn] = new TGLabel (fMChnF[chn % chnh], chnnum);
         fMChnF[chn % chnh]->AddFrame (fMChnLabel[chn], fMeas2Layout[1]);

	 /* Check box to make channel active. */
         fMChnActive[chn] = new TGCheckButton (fMChnF[chn % chnh], "",
                              kMeasChnActive + chn);
         fMChnActive[chn]->Associate (this);
         fMChnF[chn % chnh]->AddFrame (fMChnActive[chn], fMeas2Layout[2]);

	 /* Channel name combo box */
	 // Use the contructor that takes an array of ligogui::ChannelEntry
	 // but do NOT copy the ChannelEntry array.
         fMChn[chn] = new TLGChannelCombobox(fMChnF[chn % chnh], 
					     kMeasChn + chn, fMeasChnList, 
					     fMeasChnLen, kFALSE, chntype,
					     kTRUE);
#if 1
	 fMChn[chn]->Resize (420, 22) ;
#else
	 /* JCB */
         fMChn[chn]->Resize (300, 22);
	 /* JCB - Was 210 */
#endif
         fMChn[chn]->SetPopupHeight (400);
         fMChn[chn]->Associate (this);
         fMChnF[chn % chnh]->AddFrame (fMChn[chn], fMeas2Layout[3]);
      }
   
      // Fourier tools
      fFFTLayout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fFFTLayout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -6);
      fFFTLayout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      for (int i = 0; i < 3; i++) {
         fFFTFrame[i] = new TGHorizontalFrame (fMeasFFT, 10, 10);
         fMeasFFT->AddFrame (fFFTFrame[i], fFFTLayout[i != 2 ? 0 : 2]);      
      }
         // 1st line
      fFFTLabel[0] = new TGLabel (fFFTFrame[0], "Start:");
      fFFTFrame[0]->AddFrame (fFFTLabel[0], fFFTLayout[1]);
      fFFTStart = new TLGNumericControlBox (fFFTFrame[0], 0, 10, 
                           kFFTStart, kNESReal, kNEANonNegative);
      fFFTStart->Associate (this);
      fFFTFrame[0]->AddFrame (fFFTStart, fFFTLayout[1]);
      fFFTLabel[1] = new TGLabel (fFFTFrame[0], "Hz   Stop:");
      fFFTFrame[0]->AddFrame (fFFTLabel[1], fFFTLayout[1]);
      fFFTStop = new TLGNumericControlBox (fFFTFrame[0], 1000, 10, 
                           kFFTStop, kNESReal, kNEANonNegative);
      fFFTStop->Associate (this);
      fFFTFrame[0]->AddFrame (fFFTStop, fFFTLayout[1]);

      fFFTLabel[2] = new TGLabel (fFFTFrame[0], "Hz   BW:");
      fFFTFrame[0]->AddFrame (fFFTLabel[2], fFFTLayout[1]);
      fFFTBW = new TLGNumericControlBox (fFFTFrame[0], 1, 8, kFFTBW,
                           kNESReal, kNEAPositive);
      fFFTBW->Associate (this);
      fFFTFrame[0]->AddFrame (fFFTBW, fFFTLayout[1]);

      fFFTLabel[13] = new TGLabel (fFFTFrame[0], "Hz  TimeSpan:");
      fFFTFrame[0]->AddFrame (fFFTLabel[13], fFFTLayout[1]);
      fFFTTimeSpan = new TLGNumericControlBox (fFFTFrame[0], 1, 8, kFFTTimeSpan,
                                          kNESReal, kNEAPositive);
      fFFTTimeSpan->Associate (this);
      fFFTFrame[0]->AddFrame (fFFTTimeSpan, fFFTLayout[1]);

      fFFTLabel[3] = new TGLabel (fFFTFrame[0], "s   Settling Time:");
      fFFTFrame[0]->AddFrame (fFFTLabel[3], fFFTLayout[1]);
      fFFTSettling = new TLGNumericControlBox (fFFTFrame[0], 0, 6, 
                           kFFTSettling, kNESRealOne, kNEANonNegative);
      fFFTSettling->Associate (this);
      fFFTFrame[0]->AddFrame (fFFTSettling, fFFTLayout[1]);
      // Add the ramp down widget to the end of the line.
      fFFTLabel[4] = new TGLabel (fFFTFrame[0], "%  Ramp Down:");
      fFFTFrame[0]->AddFrame (fFFTLabel[4], fFFTLayout[1]);
      fFFTRampDown = new TLGNumericControlBox(fFFTFrame[0], 0, 6,
                           kFFTRampDown, kNESRealOne, kNEANonNegative) ;
      fFFTRampDown->Associate(this) ;
      fFFTFrame[0]->AddFrame(fFFTRampDown, fFFTLayout[1]) ;
      // Add the ramp up widget to the end of the line.
      fFFTLabel[11] = new TGLabel(fFFTFrame[0], "Sec  Ramp Up:") ;
      fFFTFrame[0]->AddFrame(fFFTLabel[11], fFFTLayout[1]) ;
      fFFTRampUp = new TLGNumericControlBox(fFFTFrame[0], 0, 6,
			   kFFTRampUp, kNESRealOne, kNEANonNegative) ;
      fFFTRampUp->Associate(this) ;
      fFFTFrame[0]->AddFrame(fFFTRampUp, fFFTLayout[1]) ;
      fFFTLabel[12] = new TGLabel(fFFTFrame[0], "Sec") ;
      fFFTFrame[0]->AddFrame(fFFTLabel[12], fFFTLayout[1]) ;

         // 2nd line
      fFFTLabel[5] = new TGLabel (fFFTFrame[1], "Window:");
      fFFTFrame[1]->AddFrame (fFFTLabel[5], fFFTLayout[1]);
      fFFTWindow = new WindowCombobox (fFFTFrame[1], kFFTWindow);
      fFFTWindow->Associate (this);
      fFFTFrame[1]->AddFrame (fFFTWindow, fFFTLayout[1]);
   
      fFFTLabel[6] = new TGLabel (fFFTFrame[1], "   Overlap:");
      fFFTFrame[1]->AddFrame (fFFTLabel[6], fFFTLayout[1]);
      fFFTOverlap = new TLGNumericControlBox (fFFTFrame[1], 0, 6, 
                           kFFTOverlap, kNESRealOne, kNEAAnyNumber, 
                           kNELLimitMax, 0, 100);
      fFFTOverlap->Associate (this);
      fFFTFrame[1]->AddFrame (fFFTOverlap, fFFTLayout[1]);
      fFFTLabel[10] = new TGLabel (fFFTFrame[1], "%    ");
      fFFTFrame[1]->AddFrame (fFFTLabel[10], fFFTLayout[1]);
      fFFTRemoveDC = new TGCheckButton (fFFTFrame[1], "Remove mean    ", 
                           kFFTRemoveDC);
      fFFTRemoveDC->Associate (this);
      fFFTFrame[1]->AddFrame (fFFTRemoveDC, fFFTLayout[1]);
      fFFTLabel[7] = new TGLabel (fFFTFrame[1], "Number of A channels:");
      fFFTFrame[1]->AddFrame (fFFTLabel[7], fFFTLayout[1]);
      fFFTAChannels = new TLGNumericControlBox (fFFTFrame[1], 0, 6, 
                           kFFTAChannels, kNESInteger, kNEANonNegative);
      fFFTAChannels->Associate (this);
      fFFTFrame[1]->AddFrame (fFFTAChannels, fFFTLayout[1]);
         // 3rd line
      fFFTLabel[8] = new TGLabel (fFFTFrame[2], "Averages:");
      fFFTFrame[2]->AddFrame (fFFTLabel[8], fFFTLayout[1]);
      fFFTAverages = new TLGNumericControlBox (fFFTFrame[2], 0, 6, 
                           kFFTAverages, kNESInteger, kNEAPositive);
      fFFTAverages->Associate (this);
      fFFTFrame[2]->AddFrame (fFFTAverages, fFFTLayout[1]);
      fFFTLabel[9] = new TGLabel (fFFTFrame[2], "   Average Type:");
      fFFTFrame[2]->AddFrame (fFFTLabel[9], fFFTLayout[1]);
      fFFTAverageType[0] = new TGRadioButton (fFFTFrame[2], "Fixed  ", 
                           kFFTAverageType);
      fFFTAverageType[0]->Associate (this);
      fFFTFrame[2]->AddFrame (fFFTAverageType[0], fFFTLayout[1]);
      fFFTAverageType[1] = new TGRadioButton (fFFTFrame[2], "Exponential  ", 
                           kFFTAverageType + 1);
      fFFTAverageType[1]->Associate (this);
      fFFTFrame[2]->AddFrame (fFFTAverageType[1], fFFTLayout[1]);
      fFFTAverageType[2] = new TGRadioButton (fFFTFrame[2], "Accumulative", 
                           kFFTAverageType + 2);
      fFFTAverageType[2]->Associate (this);
      fFFTFrame[2]->AddFrame (fFFTAverageType[2], fFFTLayout[1]);
      TGLabel *lbl = new TGLabel(fFFTFrame[2], "    Burst Noise Quiet Time");
      fFFTFrame[2]->AddFrame(lbl, fFFTLayout[1]);
      fFFTBurstNoiseQuietTime_s = new TLGNumericControlBox(fFFTFrame[2], 0, 6, kFFTBurstNoiseQuietTime,
              kNESRealTwo, kNEANonNegative);
      fFFTBurstNoiseQuietTime_s->Associate(this);
      fFFTFrame[2]->AddFrame(fFFTBurstNoiseQuietTime_s, fFFTLayout[1]);
      lbl = new TGLabel(fFFTFrame[2], "sec");
      fFFTFrame[2]->AddFrame(lbl, fFFTLayout[1]);
   
      // Swept sine
      fSSLayout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fSSLayout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -6);
      fSSLayout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fSSLayout[3] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, -2, 2, 2);
      for (int i = 0; i < 4; i++) {
         const int repos[4] = {0, 3, 2, 1};
         fSSFrame[repos[i]] = new TGHorizontalFrame (fMeasSS, 10, 10);
         fMeasSS->AddFrame (fSSFrame[repos[i]], fSSLayout[i != 3 ? 0 : 2]);      
      }
         // 1st line
      fSSLabel[0] = new TGLabel (fSSFrame[0], "Start:");
      fSSFrame[0]->AddFrame (fSSLabel[0], fSSLayout[1]);
      fSSStart = new TLGNumericControlBox (fSSFrame[0], 0, 10, 
                           kSSStart, kNESReal, kNEANonNegative);
      fSSStart->Associate (this);
      fSSFrame[0]->AddFrame (fSSStart, fSSLayout[1]);
      fSSLabel[1] = new TGLabel (fSSFrame[0], "Hz   Stop:");
      fSSFrame[0]->AddFrame (fSSLabel[1], fSSLayout[1]);
      fSSStop = new TLGNumericControlBox (fSSFrame[0], 1000, 10, 
                           kSSStop, kNESReal, kNEANonNegative);
      fSSStop->Associate (this);
      fSSFrame[0]->AddFrame (fSSStop, fSSLayout[1]);
      fSSLabel[2] = new TGLabel (fSSFrame[0], "Hz   Points:");
      fSSFrame[0]->AddFrame (fSSLabel[2], fSSLayout[1]);
      fSSPoints = new TLGNumericControlBox (fSSFrame[0], 1, 6, kSSPoints,
                           kNESInteger, kNEAPositive);
      fSSPoints->Associate (this);
      fSSFrame[0]->AddFrame (fSSPoints, fSSLayout[1]);
      fSSLabel[3] = new TGLabel (fSSFrame[0], "    Settling Time:");
      fSSFrame[0]->AddFrame (fSSLabel[3], fSSLayout[1]);
      fSSSettling = new TLGNumericControlBox (fSSFrame[0], 0, 6, 
                           kSSSettling, kNESRealOne, kNEANonNegative);
      fSSSettling->Associate (this);
      fSSFrame[0]->AddFrame (fSSSettling, fSSLayout[1]);
      fSSLabel[4] = new TGLabel (fSSFrame[0], "%  Ramp Down:") ;
      fSSFrame[0]->AddFrame (fSSLabel[4], fSSLayout[1]);
      fSSRampDown = new TLGNumericControlBox(fSSFrame[0], 0, 6,
                           kSSRampDown, kNESRealOne, kNEANonNegative) ;
      fSSRampDown->Associate(this) ;
      fSSFrame[0]->AddFrame(fSSRampDown, fSSLayout[1]) ;
      // Add the ramp up widget to the end of the line.
      fSSLabel[14] = new TGLabel(fSSFrame[0], "Sec  Ramp Up:") ;
      fSSFrame[0]->AddFrame(fSSLabel[14], fSSLayout[1]) ;
      fSSRampUp = new TLGNumericControlBox(fSSFrame[0], 0, 6,
			   kSSRampUp, kNESRealOne, kNEANonNegative) ;
      fSSRampUp->Associate(this) ;
      fSSFrame[0]->AddFrame(fSSRampUp, fSSLayout[1]) ;
      fSSLabel[15] = new TGLabel(fSSFrame[0], "Sec") ;
      fSSFrame[0]->AddFrame(fSSLabel[15], fSSLayout[1]) ;
         // 2nd line
      fSSLabel[5] = new TGLabel (fSSFrame[1], "Sweep Direction:");
      fSSFrame[1]->AddFrame (fSSLabel[5], fSSLayout[1]);
      fSSSweepDir[0] = new TGRadioButton (fSSFrame[1], "Up  ", kSSSweepDir);
      fSSSweepDir[0]->Associate (this);
      fSSFrame[1]->AddFrame (fSSSweepDir[0], fSSLayout[1]);
      fSSSweepDir[1] = new TGRadioButton (fSSFrame[1], "Down      Sweep Type:", 
                           kSSSweepDir + 1);
      fSSSweepDir[1]->Associate (this);
      fSSFrame[1]->AddFrame (fSSSweepDir[1], fSSLayout[1]);
      fSSSweepType[0] = new TGRadioButton (fSSFrame[1], "Linear  ", 
                           kSSSweepType);
      fSSSweepType[0]->Associate (this);
      fSSFrame[1]->AddFrame (fSSSweepType[0], fSSLayout[1]);
      fSSSweepType[1] = new TGRadioButton (fSSFrame[1], "Logarithmic  ", 
                           kSSSweepType + 1);
      fSSSweepType[1]->Associate (this);
      fSSFrame[1]->AddFrame (fSSSweepType[1], fSSLayout[1]);
      fSSSweepType[2] = new TGCheckButton (fSSFrame[1], "User ", 
                           kSSSweepType + 2);
      fSSSweepType[2]->Associate (this);
      fSSFrame[1]->AddFrame (fSSSweepType[2], fSSLayout[1]);
      // fSSSweepFile = new TGTextEntry (fSSFrame[1], "",  kSSSweepFile);
      // fSSSweepFile->Associate (this);
      // fSSSweepFile->SetState (kFALSE);
      // fSSSweepFile->SetWidth (280);
      // fSSFrame[1]->AddFrame (fSSSweepFile, fSSLayout[1]);
      fSSLabel[6] = new TGLabel (fSSFrame[1], "   Format: ");
      fSSFrame[1]->AddFrame (fSSLabel[6], fSSLayout[1]);
      // fSSSweepFileFormat = new TGComboBox (fSSFrame[1], kSSSweepFileFormat);
      // fSSSweepFileFormat->Associate (this);
      // fSSSweepFileFormat->Resize (100, 22);
      // fSSSweepFileFormat->AddEntry ("Freq./Ampl.", 0);
      // fSSSweepFileFormat->AddEntry ("Envelope", 1);
      // fSSSweepFileFormat->AddEntry ("Freq. only", 2);
      // fSSSweepFileFormat->Select (0);
      // fSSFrame[1]->AddFrame (fSSSweepFileFormat, fSSLayout[1]);
      // fSSSweepFileRead = new TGTextButton (fSSFrame[1], "   Read...   ", 
                           // kSSSweepFileRead);
      // fSSSweepFileRead->Associate (this);
      // fSSFrame[1]->AddFrame (fSSSweepFileRead, fSSLayout[1]);
      fSSSweepFormat = new TGComboBox (fSSFrame[1], kSSSweepFormat);
      fSSSweepFormat->Associate (this);
      fSSSweepFormat->Resize (100, 22);
      fSSSweepFormat->AddEntry ("Freq./Ampl.", 0);
      fSSSweepFormat->AddEntry ("Envelope", 1);
      fSSSweepFormat->AddEntry ("Freq. only", 2);
      fSSSweepFormat->Select (0);
      fSSFrame[1]->AddFrame (fSSSweepFormat, fSSLayout[1]);
      fSSSweepEdit = new TGTextButton (fSSFrame[1], "   Edit...   ", 
                           kSSSweepEdit);
      fSSSweepEdit->Associate (this);
      fSSFrame[1]->AddFrame (fSSSweepEdit, fSSLayout[1]);
      fSSSweepEditText = 0;
      fSSSweepEditor = 0;
      fSSSweepEditorRet = kFALSE;
      fSSSweepEditorDone = kTRUE;
         // 3rd line
      fSSLabel[7] = new TGLabel (fSSFrame[2], "Window:");
      fSSFrame[2]->AddFrame (fSSLabel[7], fSSLayout[1]);
      fSSWindow = new WindowCombobox (fSSFrame[2], kSSWindow);
      fSSWindow->Associate (this);
      fSSFrame[2]->AddFrame (fSSWindow, fSSLayout[1]);
      fSSLabel[8] = new TGLabel (fSSFrame[2], "   ");
      fSSFrame[2]->AddFrame (fSSLabel[8], fSSLayout[1]);
      fSSPowerSpec = new TGCheckButton (fSSFrame[2], "Power Spectrum",
                           kSSPowerSpec);
      fSSPowerSpec->Associate (this);
      fSSFrame[2]->AddFrame (fSSPowerSpec, fSSLayout[1]);
      fSSLabel[9] = new TGLabel (fSSFrame[2], "    Number of A channels:");
      fSSFrame[2]->AddFrame (fSSLabel[9], fSSLayout[1]);
      fSSAChannels = new TLGNumericControlBox (fSSFrame[2], 0, 6, 
                           kSSAChannels, kNESInteger, kNEANonNegative);
      fSSAChannels->Associate (this);
      fSSFrame[2]->AddFrame (fSSAChannels, fSSLayout[1]);
      TGLabel *double_label = new TGLabel(fSSFrame[2], "   Double prec. Freqs:");
      fSSFrame[2]->AddFrame( double_label, fSSLayout[1]);
      fSSDoublePrecFreq = new TGCheckButton( fSSFrame[2], "", kSSDoublePrecFreqs);
      fSSDoublePrecFreq->Associate(this);
      fSSFrame[2]->AddFrame( fSSDoublePrecFreq, fSSLayout[1]);
         // 4th line
      fSSLabel[10] = new TGLabel (fSSFrame[3], "Measurement Time:");
      fSSFrame[3]->AddFrame (fSSLabel[10], fSSLayout[1]);
      fSSMeasTimeSel[0] = new TGCheckButton (fSSFrame[3], "", 
                           kSSMeasTimeSel);
      fSSMeasTimeSel[0]->Associate (this);
      fSSFrame[3]->AddFrame (fSSMeasTimeSel[0], fSSLayout[3]);
      fSSMeasTime[0] = new TLGNumericControlBox (fSSFrame[3], 0, 6, 
                           kSSMeasTime, kNESReal, kNEANonNegative);
      fSSMeasTime[0]->Associate (this);
      fSSFrame[3]->AddFrame (fSSMeasTime[0], fSSLayout[1]);
      fSSLabel[11] = new TGLabel (fSSFrame[3], "cycles  ");
      fSSFrame[3]->AddFrame (fSSLabel[11], fSSLayout[1]);
      fSSMeasTimeSel[1] = new TGCheckButton (fSSFrame[3], "", 
                           kSSMeasTimeSel + 1);
      fSSMeasTimeSel[1]->Associate (this);
      fSSFrame[3]->AddFrame (fSSMeasTimeSel[1], fSSLayout[3]);
      fSSMeasTime[1] = new TLGNumericControlBox (fSSFrame[3], 0, 6, 
                           kSSMeasTime + 1, kNESReal, kNEANonNegative);
      fSSMeasTime[1]->Associate (this);
      fSSFrame[3]->AddFrame (fSSMeasTime[1], fSSLayout[1]);
      fSSLabel[12] = new TGLabel (fSSFrame[3], "sec    Averages:");
      fSSFrame[3]->AddFrame (fSSLabel[12], fSSLayout[1]);
      fSSAverages = new TLGNumericControlBox (fSSFrame[3], 1, 6, 
                           kSSAverages, kNESInteger, kNEAPositive);
      fSSAverages->Associate (this);
      fSSFrame[3]->AddFrame (fSSAverages, fSSLayout[1]);
      fSSLabel[13] = new TGLabel (fSSFrame[3], "   Harmonic Order:");
      fSSFrame[3]->AddFrame (fSSLabel[13], fSSLayout[1]);
      fSSHarmonicOrder = new TLGNumericControlBox (fSSFrame[3], 1, 6, 
                           kSSHarmonicOrder, kNESInteger, kNEAPositive);
      fSSHarmonicOrder->Associate (this);
      fSSFrame[3]->AddFrame (fSSHarmonicOrder, fSSLayout[1]);
   
      // Sine response
      fSRLayout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fSRLayout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -6);
      fSRLayout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fSRLayout[3] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, -2, 2, 2);
      for (int i = 0; i < 3; i++) {
         fSRFrame[i] = new TGHorizontalFrame (fMeasSR, 10, 10);
         fMeasSR->AddFrame (fSRFrame[i], fSRLayout[i != 3 ? 0 : 2]);      
      }
         // 1st line
      fSRLabel[0] = new TGLabel (fSRFrame[0], "Measurement Time:");
      fSRFrame[0]->AddFrame (fSRLabel[0], fSRLayout[1]);
      fSRMeasTimeSel[0] = new TGCheckButton (fSRFrame[0], "", 
                           kSRMeasTimeSel);
      fSRMeasTimeSel[0]->Associate (this);
      fSRFrame[0]->AddFrame (fSRMeasTimeSel[0], fSRLayout[3]);
      fSRMeasTime[0] = new TLGNumericControlBox (fSRFrame[0], 0, 6, 
                           kSRMeasTime, kNESReal, kNEANonNegative);
      fSRMeasTime[0]->Associate (this);
      fSRFrame[0]->AddFrame (fSRMeasTime[0], fSRLayout[1]);
      fSRLabel[1] = new TGLabel (fSRFrame[0], "cycles  ");
      fSRFrame[0]->AddFrame (fSRLabel[1], fSRLayout[1]);
      fSRMeasTimeSel[1] = new TGCheckButton (fSRFrame[0], "", 
                           kSRMeasTimeSel + 1);
      fSRMeasTimeSel[1]->Associate (this);
      fSRFrame[0]->AddFrame (fSRMeasTimeSel[1], fSRLayout[3]);
      fSRMeasTime[1] = new TLGNumericControlBox (fSRFrame[0], 0, 6, 
                           kSRMeasTime + 1, kNESReal, kNEANonNegative);
      fSRMeasTime[1]->Associate (this);
      fSRFrame[0]->AddFrame (fSRMeasTime[1], fSRLayout[1]);
      fSRLabel[2] = new TGLabel (fSRFrame[0], "sec    Settling Time:");
      fSRFrame[0]->AddFrame (fSRLabel[2], fSRLayout[1]);
      fSRSettling = new TLGNumericControlBox (fSRFrame[0], 0, 6, 
                           kSRSettling, kNESRealOne, kNEANonNegative);
      fSRSettling->Associate (this);
      fSRFrame[0]->AddFrame (fSRSettling, fSRLayout[1]);
      // Add the ramp down widget to the end of the line.
      fSRLabel[3] = new TGLabel (fSRFrame[0], "%  Ramp Down:");
      fSRFrame[0]->AddFrame (fSRLabel[3], fSRLayout[1]);
      fSRRampDown = new TLGNumericControlBox(fSRFrame[0], 0, 6,
			   kSRRampDown, kNESRealOne, kNEANonNegative) ;
      fSRRampDown->Associate(this) ;
      fSRFrame[0]->AddFrame(fSRRampDown, fSRLayout[1]) ;
      // Add the ramp up widget to the end of the line.
      fSRLabel[9] = new TGLabel(fSRFrame[0], "Sec  Ramp Up:") ;
      fSRFrame[0]->AddFrame(fSRLabel[9], fSRLayout[1]) ;
      fSRRampUp = new TLGNumericControlBox(fSRFrame[0], 0, 6,
			   kSRRampUp, kNESRealOne, kNEANonNegative) ;
      fSRRampUp->Associate(this) ;
      fSRFrame[0]->AddFrame(fSRRampUp, fSRLayout[1]) ;
      fSRLabel[10] = new TGLabel(fSRFrame[0], "Sec") ;
      fSRFrame[0]->AddFrame(fSRLabel[10], fSRLayout[1]) ;
         // 2nd line
      fSRLabel[4] = new TGLabel (fSRFrame[1], "Window:");
      fSRFrame[1]->AddFrame (fSRLabel[4], fSRLayout[1]);
      fSRWindow = new WindowCombobox (fSRFrame[1], kSRWindow);
      fSRWindow->Associate (this);
      fSRFrame[1]->AddFrame (fSRWindow, fSRLayout[1]);
      fSRLabel[5] = new TGLabel (fSRFrame[1], "   ");
      fSRFrame[1]->AddFrame (fSRLabel[5], fSRLayout[1]);
      fSRPowerSpec = new TGCheckButton (fSRFrame[1], "Power Spectrum",
                           kSRPowerSpec);
      fSRPowerSpec->Associate (this);
      fSRFrame[1]->AddFrame (fSRPowerSpec, fSRLayout[1]);
      fSRLabel[8] = new TGLabel (fSRFrame[1], "    Harmonic Order:");
      fSRFrame[1]->AddFrame (fSRLabel[8], fSRLayout[1]);
      fSRHarmonicOrder = new TLGNumericControlBox (fSRFrame[1], 1, 6, 
                           kSRHarmonicOrder, kNESInteger, kNEAPositive);
      fSRHarmonicOrder->Associate (this);
      fSRFrame[1]->AddFrame (fSRHarmonicOrder, fSRLayout[1]);
         // 3rd line
      fSRLabel[6] = new TGLabel (fSRFrame[2], "Averages:");
      fSRFrame[2]->AddFrame (fSRLabel[6], fSRLayout[1]);
      fSRAverages = new TLGNumericControlBox (fSRFrame[2], 0, 6, 
                           kSRAverages, kNESInteger, kNEAPositive);
      fSRAverages->Associate (this);
      fSRFrame[2]->AddFrame (fSRAverages, fSRLayout[1]);
      fSRLabel[7] = new TGLabel (fSRFrame[2], "   Average Type:");
      fSRFrame[2]->AddFrame (fSRLabel[7], fSRLayout[1]);
      fSRAverageType[0] = new TGRadioButton (fSRFrame[2], "Fixed  ", 
                           kSRAverageType);
      fSRAverageType[0]->Associate (this);
      fSRFrame[2]->AddFrame (fSRAverageType[0], fSRLayout[1]);
      fSRAverageType[1] = new TGRadioButton (fSRFrame[2], "Exponential  ", 
                           kSRAverageType + 1);
      fSRAverageType[1]->Associate (this);
      fSRFrame[2]->AddFrame (fSRAverageType[1], fSRLayout[1]);
      fSRAverageType[2] = new TGRadioButton (fSRFrame[2], "Accumulative", 
                           kSRAverageType + 2);
      fSRAverageType[2]->Associate (this);
      fSRFrame[2]->AddFrame (fSRAverageType[2], fSRLayout[1]);
   
      // Triggered time response
      fTSLayout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fTSLayout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -6);
      fTSLayout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fTSLayout[3] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, -2, 2, 2);
      fTSLayout[4] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY | 
                           kLHintsExpandX, 2, 2, 2, 2);
      for (int i = 0; i < 4; i++) {
         fTSFrame[i] = new TGHorizontalFrame (fMeasTS, 10, 10);
         fMeasTS->AddFrame (fTSFrame[i], fTSLayout[i != 3 ? 0 : 2]);      
      }
         // 1st line
      fTSLabel[0] = new TGLabel (fTSFrame[0], "Measurement Time:");
      fTSFrame[0]->AddFrame (fTSLabel[0], fTSLayout[1]);
      fTSMeasTime = new TLGNumericControlBox (fTSFrame[0], 0, 6, 
                           kTSMeasTime, kNESReal, kNEANonNegative);
      fTSMeasTime->Associate (this);
      fTSFrame[0]->AddFrame (fTSMeasTime, fTSLayout[1]);
      fTSLabel[1] = new TGLabel (fTSFrame[0], "sec   BW:");
      fTSFrame[0]->AddFrame (fTSLabel[1], fTSLayout[1]);
      fTSBW = new TLGNumericControlBox (fTSFrame[0], 1, 8, kTSBW,
                           kNESReal, kNEAPositive);
      fTSBW->Associate (this);
      fTSFrame[0]->AddFrame (fTSBW, fTSLayout[1]);
      fTSLabel[2] = new TGLabel (fTSFrame[0], "Hz   Settling Time:");
      fTSFrame[0]->AddFrame (fTSLabel[2], fTSLayout[1]);
      fTSSettling = new TLGNumericControlBox (fTSFrame[0], 0, 6, 
                           kTSSettling, kNESRealOne, kNEANonNegative);
      fTSSettling->Associate (this);
      fTSFrame[0]->AddFrame (fTSSettling, fTSLayout[1]);
      // Add the ramp down widget to the end of the line.
      fTSLabel[3] = new TGLabel (fTSFrame[0], "%  Ramp Down:");
      fTSFrame[0]->AddFrame (fTSLabel[3], fTSLayout[1]);
      fTSRampDown = new TLGNumericControlBox(fTSFrame[0], 0, 6,
			   kTSRampDown, kNESRealOne, kNEANonNegative) ;
      fTSRampDown->Associate(this) ;
      fTSFrame[0]->AddFrame(fTSRampDown, fTSLayout[1]) ;
      // Add the ramp up widget to the end of the line.
      fTSLabel[10] = new TGLabel(fTSFrame[0], "Sec  Ramp Up:") ;
      fTSFrame[0]->AddFrame(fTSLabel[10], fTSLayout[1]) ;
      fTSRampUp = new TLGNumericControlBox(fTSFrame[0], 0, 6,
			   kTSRampUp, kNESRealOne, kNEANonNegative) ;
      fTSRampUp->Associate(this) ;
      fTSFrame[0]->AddFrame(fTSRampUp, fTSLayout[1]) ;
      fTSLabel[11] = new TGLabel(fTSFrame[0], "Sec") ;
      fTSFrame[0]->AddFrame(fTSLabel[11], fTSLayout[1]) ;
         // 2nd line
      fTSLabel[4] = new TGLabel (fTSFrame[1], "Pre-trigger Time:");
      fTSFrame[1]->AddFrame (fTSLabel[4], fTSLayout[1]);
      fTSPreTrigTime = new TLGNumericControlBox (fTSFrame[1], 0, 6, 
                           kTSPreTrigTime, kNESReal, kNEAAnyNumber);
      fTSPreTrigTime->Associate (this);
      fTSFrame[1]->AddFrame (fTSPreTrigTime, fTSLayout[1]);
      fTSLabel[5] = new TGLabel (fTSFrame[1], "%   Dead Time:");
      fTSFrame[1]->AddFrame (fTSLabel[5], fTSLayout[1]);
      fTSDeadTime = new TLGNumericControlBox (fTSFrame[1], 1, 8, 
                           kTSDeadTime, kNESReal, kNEAPositive);
      fTSDeadTime->Associate (this);
      fTSFrame[1]->AddFrame (fTSDeadTime, fTSLayout[1]);
      fTSLabel[6] = new TGLabel (fTSFrame[1], "%    ");
      fTSFrame[1]->AddFrame (fTSLabel[6], fTSLayout[1]);
      fTSIncStat = new TGCheckButton (fTSFrame[1], "Include Statistics",
                           kTSIncStat);
      fTSIncStat->Associate (this);
      fTSFrame[1]->AddFrame (fTSIncStat, fTSLayout[1]);
         // 3rd line
      fTSLabel[7] = new TGLabel (fTSFrame[2], "Averages:");
      fTSFrame[2]->AddFrame (fTSLabel[7], fTSLayout[1]);
      fTSAverages = new TLGNumericControlBox (fTSFrame[2], 0, 6, 
                           kTSAverages, kNESInteger, kNEAPositive);
      fTSAverages->Associate (this);
      fTSFrame[2]->AddFrame (fTSAverages, fTSLayout[1]);
      fTSLabel[8] = new TGLabel (fTSFrame[2], "   Average Type:");
      fTSFrame[2]->AddFrame (fTSLabel[8], fTSLayout[1]);
      fTSAverageType[0] = new TGRadioButton (fTSFrame[2], "Fixed  ", 
                           kTSAverageType);
      fTSAverageType[0]->Associate (this);
      fTSFrame[2]->AddFrame (fTSAverageType[0], fTSLayout[1]);
      fTSAverageType[1] = new TGRadioButton (fTSFrame[2], "Exponential  ", 
                           kTSAverageType + 1);
      fTSAverageType[1]->Associate (this);
      fTSFrame[2]->AddFrame (fTSAverageType[1], fTSLayout[1]);
      fTSAverageType[2] = new TGRadioButton (fTSFrame[2], "Accumulative", 
                           kTSAverageType + 2);
      fTSAverageType[2]->Associate (this);
      fTSFrame[2]->AddFrame (fTSAverageType[2], fTSLayout[1]);

       //temporary hide until we can figure the best way to implement for time series.
//       lbl = new TGLabel(fTSFrame[2], "    Burst Noise Quiet Time");
//       fTSFrame[2]->AddFrame(lbl, fTSLayout[1]);
       fTSBurstNoiseQuietTime_s = new TLGNumericControlBox(fTSFrame[2], 0, 6, kTSBurstNoiseQuietTime,
                                                            kNESRealTwo, kNEANonNegative);
//       fTSBurstNoiseQuietTime_s->Associate(this);
//       fTSFrame[2]->AddFrame(fTSBurstNoiseQuietTime_s, fTSLayout[1]);
//       lbl = new TGLabel(fTSFrame[2], "sec");
//       fTSFrame[2]->AddFrame(lbl, fTSLayout[1]);




         // 4th line
      fTSLabel[9] = new TGLabel (fTSFrame[3], "Filter:");
      fTSFrame[3]->AddFrame (fTSLabel[9], fTSLayout[1]);
      fTSFilterSpec = new TLGTextEntry (fTSFrame[3], "", kTSFilterSpec);
      fTSFilterSpec->Associate (this);
      fTSFilterSpec->SetMaxLength (4*1024);
      fTSFrame[3]->AddFrame (fTSFilterSpec, fTSLayout[4]);
      fTSFoton = new TGTextButton (fTSFrame[3], "   Foton...   ", 
                           kTSFoton);
      fTSFoton->Associate (this);
      fTSFrame[3]->AddFrame (fTSFoton, fTSLayout[1]);
   
      // Start time group
      fTimeLayout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fTimeLayout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, -6);
      fTimeLayout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fTimeLayout[3] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, -2, 2, 2);
      fTimeLayout[4] = 
         new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 0);
      fTimeLayout[5] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 30, 0, 0, 0);
      fTimeLayout[6] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 22, 2, 2, 2);
      fTimeLayout[7] = 
         new TGLayoutHints (kLHintsRight | kLHintsCenterY, 2, 2, 2, 2);
      fTimeFrame[7] = new TGHorizontalFrame (fTimeF, 10, 10);
      fTimeF->AddFrame (fTimeFrame[7], fTimeLayout[2]);
      // left panel
      fTimeFrame[5] = new TGVerticalFrame (fTimeFrame[7], 10, 10);
      fTimeFrame[7]->AddFrame (fTimeFrame[5], fTimeLayout[4]);
      // right panel
      fTimeFrame[6] = new TGVerticalFrame (fTimeFrame[7], 10, 10);
      fTimeFrame[7]->AddFrame (fTimeFrame[6], fTimeLayout[5]);
      // lines 1
      fTimeFrame[0] = new TGHorizontalFrame (fTimeFrame[5], 10, 10);
      fTimeFrame[5]->AddFrame (fTimeFrame[0], fTimeLayout[0]);
      fTimeFrame[1] = new TGHorizontalFrame (fTimeFrame[6], 10, 10);
      fTimeFrame[6]->AddFrame (fTimeFrame[1], fTimeLayout[0]);
      // lines 2
      fTimeFrame[3] = new TGHorizontalFrame (fTimeFrame[5], 10, 10);
      fTimeFrame[5]->AddFrame (fTimeFrame[3], fTimeLayout[0]);
      fTimeFrame[2] = new TGHorizontalFrame (fTimeFrame[6], 10, 10);
      fTimeFrame[6]->AddFrame (fTimeFrame[2], fTimeLayout[0]);
      // lines 3
      fTimeFrame[4] = new TGHorizontalFrame (fTimeFrame[5], 10, 10);
      fTimeFrame[5]->AddFrame (fTimeFrame[4], fTimeLayout[0]);
      fTimeFrame[8] = new TGHorizontalFrame (fTimeFrame[6], 10, 10);
      fTimeFrame[6]->AddFrame (fTimeFrame[8], fTimeLayout[0]);
         // 1st line (left panel)
      fTimeSel[0] = new TGRadioButton (fTimeFrame[0], "Now", kTimeStart);
      fTimeSel[0]->Associate (this);
      fTimeFrame[0]->AddFrame (fTimeSel[0], fTimeLayout[1]);
         // 1st line (right panel)
      fTimeSel[1] = new TGRadioButton (fTimeFrame[1], "In the future: ", 
                           kTimeStart + 1);
      fTimeSel[1]->Associate (this);
      fTimeFrame[1]->AddFrame (fTimeSel[1], fTimeLayout[1]);
      fTimeFuture = new TLGNumericControlBox (fTimeFrame[1], 0., 10, kTimeFuture,
                           kNESHourMinSec, kNEANonNegative);
      fTimeFuture->Associate (this);
      fTimeFrame[1]->AddFrame (fTimeFuture, fTimeLayout[1]);
      fTimeLabel[0] = new TGLabel (fTimeFrame[1], "hh:mm:ss");
      fTimeFrame[1]->AddFrame (fTimeLabel[0], fTimeLayout[1]);

         // 2nd line (right panel)
      fTimeSel[2] = new TGRadioButton (fTimeFrame[2], "In the past:   ", 
                           kTimeStart + 2);
      fTimeSel[2]->Associate (this);
      fTimeFrame[2]->AddFrame (fTimeSel[2], fTimeLayout[1]);
      fTimePast = new TLGNumericControlBox (fTimeFrame[2], 0., 10, kTimePast,
                           kNESHourMinSec, kNEANonNegative);
      fTimePast->Associate (this);
      fTimeFrame[2]->AddFrame (fTimePast, fTimeLayout[1]);
      fTimeLabel[1] = new TGLabel (fTimeFrame[2], "hh:mm:ss");
      fTimeFrame[2]->AddFrame (fTimeLabel[1], fTimeLayout[1]);
         // 2nd line (left panel)
      fTimeSel[3] = new TGRadioButton (fTimeFrame[3], "GPS: ", kTimeStart + 3);
      fTimeSel[3]->Associate (this);
      fTimeFrame[3]->AddFrame (fTimeSel[3], fTimeLayout[1]);
      fTimeGPS = new TLGNumericControlBox (fTimeFrame[3], 0., 12, kTimeGPS,
                           kNESInteger, kNEANonNegative);
      fTimeGPS->Associate (this);
      fTimeFrame[3]->AddFrame (fTimeGPS, fTimeLayout[1]);
      fTimeLabel[2] = new TGLabel (fTimeFrame[3], "sec   ");
      fTimeFrame[3]->AddFrame (fTimeLabel[2], fTimeLayout[1]);
      fTimeGPSN = new TLGNumericControlBox (fTimeFrame[3], 0., 12, kTimeGPSN,
                           kNESInteger, kNEANonNegative, kNELLimitMinMax,
                           0, 999999999);
      fTimeGPSN->Associate (this);
      fTimeFrame[3]->AddFrame (fTimeGPSN, fTimeLayout[1]);
      fTimeLabel[3] = new TGLabel (fTimeFrame[3], "nsec");
      fTimeFrame[3]->AddFrame (fTimeLabel[3], fTimeLayout[1]);
         // 3rd line (left panel)
      fTimeSel[4] = new TGRadioButton (fTimeFrame[4], "Date/time: ", 
                           kTimeStart + 4);
      fTimeSel[4]->Associate (this);
      fTimeFrame[4]->AddFrame (fTimeSel[4], fTimeLayout[1]);
      fTimeDate = new TLGNumericControlBox (fTimeFrame[4], 0., 12, kTimeDate,
                           kNESDayMYear);
      fTimeDate->Associate (this);
      fTimeFrame[4]->AddFrame (fTimeDate, fTimeLayout[1]);
      fTimeLabel[4] = new TGLabel (fTimeFrame[4], "dd/mm/yy   ");
      fTimeFrame[4]->AddFrame (fTimeLabel[4], fTimeLayout[1]);
      fTimeTime = new TLGNumericControlBox (fTimeFrame[4], 0., 10, kTimeTime,
                           kNESHourMinSec, kNEANonNegative);
      fTimeTime->Associate (this);
      fTimeFrame[4]->AddFrame (fTimeTime, fTimeLayout[1]);
      fTimeLabel[5] = new TGLabel (fTimeFrame[4], "hh:mm:ss  UTC     ");
      fTimeFrame[4]->AddFrame (fTimeLabel[5], fTimeLayout[1]);
         // 3rd line (right panel)
      fTimeNow = new TGTextButton (fTimeFrame[8], "   Time now   ", 
                           kTimeNow);
      fTimeNow->Associate (this);
      fTimeFrame[8]->AddFrame (fTimeNow, fTimeLayout[1]);
      fTimeLookup = new TGTextButton (fTimeFrame[8], "   Lookup...   ", 
                           kTimeLookup);
      fTimeLookup->Associate (this);
      fTimeFrame[8]->AddFrame (fTimeLookup, fTimeLayout[6]);
   
      fTimeLabel[7] = new TGLabel (fTimeFrame[8], "sec/avrg.");
      fTimeFrame[8]->AddFrame (fTimeLabel[7], fTimeLayout[7]);
      fSlowDown = new TLGNumericControlBox (fTimeFrame[8], 0., 10, kTimeSlowDown,
                           kNESReal, kNEANonNegative);
      fSlowDown->Associate (this);
      fTimeFrame[8]->AddFrame (fSlowDown, fTimeLayout[7]);
      fTimeLabel[6] = new TGLabel (fTimeFrame[8], " Slow down: ");
      fTimeFrame[8]->AddFrame (fTimeLabel[6], fTimeLayout[7]);
   
      // Information group
      fInfoLayout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fInfoLayout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -6);
      fInfoLayout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fInfoLayout[3] = 
         new TGLayoutHints (kLHintsLeft | kLHintsExpandX | kLHintsTop, 
                           2, 2, 2, 2);
      fInfoFrame[0] = new TGHorizontalFrame (fInfoF, 10, 10);
      fInfoF->AddFrame (fInfoFrame[0], fInfoLayout[0]);      
      fInfoFrame[1] = new TGVerticalFrame (fInfoF, 10, 10);
      fInfoF->AddFrame (fInfoFrame[1], fInfoLayout[2]);      
         // 1st line
      fInfoLabel[0] = new TGLabel (fInfoFrame[0], "Measurement Time: ");
      fInfoFrame[0]->AddFrame (fInfoLabel[0], fInfoLayout[1]);
      fMeasTime = new TLGTextEntry (fInfoFrame[0], "", kInfoMeasTime);
      fMeasTime->Associate (this);
      fInfoFrame[0]->AddFrame (fMeasTime, fInfoLayout[1]);
      fMeasTime->SetWidth (300);
      fMeasTime->SetState (kFALSE);
         // 2nd line   
      fInfoLabel[1] = new TGLabel (fInfoFrame[0], 
                           "        Comment / Description: ");
      fInfoFrame[0]->AddFrame (fInfoLabel[1], fInfoLayout[1]);
      fComment = new TLGTextEntry (fInfoFrame[1], "", kInfoComment);
      fInfoFrame[1]->AddFrame (fComment, fInfoLayout[3]);
   
      // Excitation tab
      fExcTab =  AddTab ("   Excitation   ");
      // JCB fExcSelLayout = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 4, 4, 6, 0);
      fExcSelLayout = new TGLayoutHints (kLHintsExpandX | kLHintsTop, 4, 4, 3, 0);

      fExcSelF = new TGGroupFrame (fExcTab, "Channel Selection", 
                           kHorizontalFrame);
      fExcTab->AddFrame (fExcSelF, fExcSelLayout);
      // Channel selection
      fExcNumLayout = 
         new TGLayoutHints (kLHintsLeft | kLHintsTop, 2, 2, 5, -5);
      for (int i = 0; i < kMaxExcChannel / kShowExcChannel; i++) {
         char buf[256];
         sprintf (buf, "Channels %i to %i  ", i * kShowExcChannel,
                 (i + 1) * kShowExcChannel - 1);
         fExcNum[i] = new TGRadioButton (fExcSelF, buf, kExcNum + i);
         fExcNum[i]->Associate (this);
         fExcSelF->AddFrame (fExcNum[i], fExcNumLayout);
      }
      fExcNum[0]->SetState (kButtonDown);
   
      // Excitation channel setup frames
      fExcChnLayout[0] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, 0);
      fExcChnLayout[2] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 4, -6);
      fExcChnLayout[1] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fExcChnLayout[3] = 
         new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, -2, 2, 2);
      fExcChnLayout[4] = 
         new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 2, 2, 2, 2);
      for (int i = 0; i < kShowExcChannel; i++) {
         char buf[256];
         sprintf (buf, "Channel %i", i);
         fExcFName[i] = new TGString (buf);
         fExcF[i] = new TGGroupFrame (fExcTab, fExcFName[i]);
         fExcTab->AddFrame (fExcF[i], fExcSelLayout);

   // Split the first line to allow wider fields for the channel names
   // in the TLGChannelCombobox widgets.  JCB
	 // 1st line
         fExcChnFrame[i][0] = new TGHorizontalFrame (fExcF[i], 10, 10);
         fExcF[i]->AddFrame (fExcChnFrame[i][0], fExcChnLayout[0]);    
         fExcChnActive[i] = new TGCheckButton (fExcChnFrame[i][0], 
                              "Active   Excitation Channel:", kExcChnActive + 1);
         fExcChnActive[i]->Associate (this);
         fExcChnFrame[i][0]->AddFrame (fExcChnActive[i], fExcChnLayout[1]);
         fExcChnName[i] = new TLGChannelCombobox(fExcChnFrame[i][0], 
						 kExcChnName + i, fExcChnList,
						 fExcChnLen, kTRUE, chntype,
						 kTRUE);
         fExcChnName[i]->Associate (this);
         fExcChnName[i]->Resize (450, 22);  // Make the field wide enough for a channel name.
         fExcChnName[i]->SetPopupHeight (400);  // Height of the combo box popup.
         fExcChnFrame[i][0]->AddFrame (fExcChnName[i], fExcChnLayout[1]);

	 // 2nd line
	 fExcChnFrame[i][1] = new TGHorizontalFrame (fExcF[i], 10, 10) ;
	 fExcF[i]->AddFrame (fExcChnFrame[i][1], fExcChnLayout[0]) ;
         fExcChnLabel[i][0] = new TGLabel (fExcChnFrame[i][1], 
                              "Readback Channel:");
         fExcChnFrame[i][1]->AddFrame (fExcChnLabel[i][0], fExcChnLayout[1]);
         fExcChnRBType[i][0] = new TGRadioButton (fExcChnFrame[i][1],
                              "Default  ", kExcChnRBType + i);
         fExcChnRBType[i][0]->Associate (this);
         fExcChnFrame[i][1]->AddFrame (fExcChnRBType[i][0], fExcChnLayout[1]);
         fExcChnRBType[i][1] = new TGRadioButton (fExcChnFrame[i][1],
                              "None  ", kExcChnRBType + kShowExcChannel + i);
         fExcChnRBType[i][1]->Associate (this);
         fExcChnFrame[i][1]->AddFrame (fExcChnRBType[i][1], fExcChnLayout[1]);
         fExcChnRBType[i][2] = new TGRadioButton (fExcChnFrame[i][1],
                              "User:", kExcChnRBType + 2 * kShowExcChannel + i);
         fExcChnRBType[i][2]->Associate (this);
         fExcChnFrame[i][1]->AddFrame (fExcChnRBType[i][2], fExcChnLayout[1]);
         fExcChnRBName[i] = new TLGChannelCombobox (fExcChnFrame[i][1], 
						    kExcChnRBName + i, 
						    fMeasChnList, 
						    fMeasChnLen, kTRUE, 
						    chntype, kTRUE);
         fExcChnRBName[i]->Associate (this);
         fExcChnRBName[i]->Resize (450, 22); // Make the field wide enough for a channel name.
         fExcChnRBName[i]->SetPopupHeight (400); // Height of combo box popup.
         fExcChnFrame[i][1]->AddFrame (fExcChnRBName[i], fExcChnLayout[1]);

         // 3nd line
         fExcChnFrame[i][2] = new TGHorizontalFrame (fExcF[i], 10, 10);
         fExcF[i]->AddFrame (fExcChnFrame[i][2], fExcChnLayout[0]);
         fExcChnLabel[i][1] = new TGLabel (fExcChnFrame[i][2], "Waveform:");
         fExcChnFrame[i][2]->AddFrame (fExcChnLabel[i][1], fExcChnLayout[1]);
         fExcChnWaveform[i] = new WaveformSelection (fExcChnFrame[i][2],
                              kExcChnWaveform + i);
         fExcChnWaveform[i]->Associate (this);
         fExcChnFrame[i][2]->AddFrame (fExcChnWaveform[i], fExcChnLayout[1]);
         fExcChnLabel[i][2] = new TGLabel (fExcChnFrame[i][2], 
                              "   Waveform File:");
         fExcChnFrame[i][2]->AddFrame (fExcChnLabel[i][2], fExcChnLayout[1]);
         fExcChnWaveformFile[i] = new TLGTextEntry (fExcChnFrame[i][2], "",
                              kExcChnWaveformFile + i);
         fExcChnWaveformFile[i]->Resize (300, 22);
         fExcChnWaveformFile[i]->Associate (this);
         fExcChnFrame[i][2]->AddFrame (fExcChnWaveformFile[i], fExcChnLayout[4]);
         fExcChnWaveformFileSel[i] = new TGTextButton (fExcChnFrame[i][2],
                              "  Choose... ", kExcChnWaveformFileSel + i);
         fExcChnWaveformFileSel[i]->Associate (this);
         fExcChnFrame[i][2]->AddFrame (fExcChnWaveformFileSel[i], 
                              fExcChnLayout[1]);

         // 4th line
         fExcChnFrame[i][3] = new TGHorizontalFrame (fExcF[i], 10, 10);
         fExcF[i]->AddFrame (fExcChnFrame[i][3], fExcChnLayout[0]);
         fExcChnLabel[i][3] = new TGLabel (fExcChnFrame[i][3], "Frequency:");
         fExcChnFrame[i][3]->AddFrame (fExcChnLabel[i][3], fExcChnLayout[1]);
         fExcChnFreq[i] = new TLGNumericControlBox (fExcChnFrame[i][3], 0, 6, 
                              kExcChnFreq + i, kNESReal, kNEANonNegative);
         fExcChnFreq[i]->Associate (this);
         fExcChnFrame[i][3]->AddFrame (fExcChnFreq[i], fExcChnLayout[1]);
         fExcChnLabel[i][4] = new TGLabel (fExcChnFrame[i][3], "Hz   Amplitude:");
         fExcChnFrame[i][3]->AddFrame (fExcChnLabel[i][4], fExcChnLayout[1]);
         fExcChnAmpl[i] = new TLGNumericControlBox (fExcChnFrame[i][3], 0, 6, 
                              kExcChnAmpl + i, kNESReal, kNEAAnyNumber);
         fExcChnAmpl[i]->Associate (this);
         fExcChnFrame[i][3]->AddFrame (fExcChnAmpl[i], fExcChnLayout[1]);
         fExcChnLabel[i][5] = new TGLabel (fExcChnFrame[i][3], "   Offset:");
         fExcChnFrame[i][3]->AddFrame (fExcChnLabel[i][5], fExcChnLayout[1]);
         fExcChnOffs[i] = new TLGNumericControlBox (fExcChnFrame[i][3], 0, 6, 
                              kExcChnOffs + i, kNESReal, kNEAAnyNumber);
         fExcChnOffs[i]->Associate (this);
         fExcChnFrame[i][3]->AddFrame (fExcChnOffs[i], fExcChnLayout[1]);
         fExcChnLabel[i][6] = new TGLabel (fExcChnFrame[i][3], "   Phase:");
         fExcChnFrame[i][3]->AddFrame (fExcChnLabel[i][6], fExcChnLayout[1]);
         fExcChnPhase[i] = new TLGNumericControlBox (fExcChnFrame[i][3], 0, 6, 
                              kExcChnPhase + i, kNESReal, kNEAAnyNumber);
         fExcChnPhase[i]->Associate (this);
         fExcChnFrame[i][3]->AddFrame (fExcChnPhase[i], fExcChnLayout[1]);
         fExcChnLabel[i][7] = new TGLabel (fExcChnFrame[i][3], "deg   Ratio:");
         fExcChnFrame[i][3]->AddFrame (fExcChnLabel[i][7], fExcChnLayout[1]);
         fExcChnRatio[i] = new TLGNumericControlBox (fExcChnFrame[i][3], 0, 6, 
                              kExcChnRatio + i, kNESReal, kNEANonNegative,
                              kNELLimitMinMax, 0, 100);
         fExcChnRatio[i]->Associate (this);
         fExcChnFrame[i][3]->AddFrame (fExcChnRatio[i], fExcChnLayout[1]);
         fExcChnLabel[i][8] = new TGLabel (fExcChnFrame[i][3], "%");
         fExcChnFrame[i][3]->AddFrame (fExcChnLabel[i][8], fExcChnLayout[1]);

         // 5th line
         fExcChnFrame[i][4] = new TGHorizontalFrame (fExcF[i], 10, 10);
         fExcF[i]->AddFrame (fExcChnFrame[i][4], fExcChnLayout[2]);
         fExcChnLabel[i][9] = new TGLabel (fExcChnFrame[i][4], 
                              "Freq. Range:");
         fExcChnFrame[i][4]->AddFrame (fExcChnLabel[i][9], fExcChnLayout[1]);
         fExcChnFreqRange[i] = new TLGNumericControlBox (fExcChnFrame[i][4], 0, 
                              6, kExcChnFreqRange + i, kNESReal, kNEANonNegative);
         fExcChnFreqRange[i]->Associate (this);
         fExcChnFrame[i][4]->AddFrame (fExcChnFreqRange[i], fExcChnLayout[1]);
         fExcChnLabel[i][10] = new TGLabel (fExcChnFrame[i][4], 
                              "Hz   Ampl. Range:");
         fExcChnFrame[i][4]->AddFrame (fExcChnLabel[i][10], fExcChnLayout[1]);
         fExcChnAmplRange[i] = new TLGNumericControlBox (fExcChnFrame[i][4], 0, 
                              6, kExcChnAmplRange + i, kNESReal, kNEAAnyNumber);
         fExcChnAmplRange[i]->Associate (this);
         fExcChnFrame[i][4]->AddFrame (fExcChnAmplRange[i], fExcChnLayout[1]);
         fExcChnLabel[i][11] = new TGLabel (fExcChnFrame[i][4], 
                              "   Filter:");
         fExcChnFrame[i][4]->AddFrame (fExcChnLabel[i][11], fExcChnLayout[1]);
         fExcChnFilterCmd[i] = new TLGTextEntry (fExcChnFrame[i][4], "",
                              kExcChnFilterCmd + i);
         fExcChnFilterCmd[i]->Resize (300, 22);
         fExcChnFilterCmd[i]->Associate (this);
         fExcChnFrame[i][4]->AddFrame (fExcChnFilterCmd[i], fExcChnLayout[4]);
         fExcChnFilterWiz[i] = new TGTextButton (fExcChnFrame[i][4],
                              "   Foton...   ", kExcChnFilterWiz + i);
         fExcChnFilterWiz[i]->Associate (this);
         fExcChnFrame[i][4]->AddFrame (fExcChnFilterWiz[i], fExcChnLayout[1]);
      }
   
      // Result tab
      fResTab =  AddTab ("      Result      ");
      fPad = new TLGMultiPad (fResTab, "Plot", *fPlot, 3, 2);
      fPad->GetPad (0)->HidePanel (kFALSE);
      fPad->GetPad (1)->HidePanel (kFALSE);
      fPadLayout = 
         new TGLayoutHints (kLHintsExpandX | kLHintsExpandY, 0, 0, 0, 0);
      fResTab->AddFrame (fPad, fPadLayout);
   
      // Iterator tab
      // fIterTab = AddTab ("     Iterator     ");
      // Synchronizationption tab
      // fSyncTab = AddTab ("Synchronization");
      // Environmental tab
      // fEnvTab =  AddTab (" Environment ");
      // Defaults tab
      // fDefTab =  AddTab ("    Defaults    ");
   
      // Initialze NDS names
      const char* serv = getenv (dfm::kNdsServer);
      if (serv && *serv) {
         char* s = new (nothrow) char [strlen (serv) + 10];
         strcpy (s, serv);
         char* last;
         char* p = strtok_r (s, ",", &last);
         int id = 0;
         while (p) {
            string addr = p;
            while (!addr.empty() && isspace (addr[0])) addr.erase (0, 1);
            while (!addr.empty() && isspace (addr[addr.size()-1])) 
               addr.erase (addr.size()-1, 1);
            p = strtok_r (0, ",", &last);
            // add NDS server
            if (!id && fParam) {
               string::size_type pos = addr.find (':');
               if (pos != string::npos) {
                  fParam->fData.fNDSPort = atoi (addr.c_str() + pos + 1);
               }
               fParam->fData.fNDSName = addr.substr(0, pos).c_str();
            }
            fNDSName->AddEntry (addr.c_str(), id++);
         }
         delete [] s;
      }
      else {
	 // The NDSSERVER environment variable wasn't present, set defaults.
	 fNDSName->AddEntry("nds:8088",  0) ;
	 fNDSName->AddEntry("nds1:8088", 1) ;
	 fNDSPort->SetIntNumber (8088);
	 
	 // Set the default values for fParam.
	 if (fParam) {
	    fParam->fData.fNDSName = "nds" ;
	    fParam->fData.fNDSPort = 8088 ;
	 }
      }

      // Initialize NDS2 names
      const char * serv2 = getenv (dfm::kSendsServer) ;
      if (serv2 && *serv2) {
	 char *s = new (nothrow) char [strlen (serv2) + 10] ;
	 strcpy (s, serv2) ;
         char* last;
         char* p = strtok_r (s, ",", &last);
         int id = 0;
         while (p) {
            string addr = p;
            while (!addr.empty() && isspace (addr[0])) addr.erase (0, 1);
            while (!addr.empty() && isspace (addr[addr.size()-1]))
               addr.erase (addr.size()-1, 1);
            p = strtok_r (0, ",", &last);
            // add NDS server
            if (!id && fParam) {
               string::size_type pos = addr.find (':');
               if (pos != string::npos) {
                  fParam->fData.fNDS2Port = atoi (addr.c_str() + pos + 1);
               }
               fParam->fData.fNDS2Name = addr.substr(0, pos).c_str();
            }
            fNDS2Name->AddEntry (addr.c_str(), id++);
         }
         delete [] s;
      }
      else {
         fNDS2Name->AddEntry("nds.ligo.caltech.edu:31200",    0);
         fNDS2Name->AddEntry("nds.ligo-la.caltech.edu:31200", 1);
         fNDS2Name->AddEntry("nds.ligo-wa.caltech.edu:31200", 2);
         fNDS2Name->AddEntry("aneirin.mit.edu",               5);
         if (fParam) fParam->fData.fNDS2Name = "nds.ligo.caltech.edu";
      }

      // Initialize start time
      tainsec_t now = TAInow();
      fTimeGPS->SetIntNumber (now / _ONESEC);
      fTimeGPSN->SetIntNumber (0);
      // Initialize the epoch fields
      fNDS2EpochStartGPS->SetIntNumber(0) ;
      {
	 // set UTC start time
	 utc_t	utc;
	 TAItoUTC (fNDS2EpochStartGPS->GetIntNumber(), &utc);
	 utc.tm_year += 1900;
	 utc.tm_mon++;
	 fNDS2EpochStartDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
	 fNDS2EpochStartTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
      }
      fNDS2EpochStopGPS->SetIntNumber(now / _ONESEC) ;
      {
	 // set UTC stop time
	 utc_t	utc;
	 TAItoUTC (fNDS2EpochStopGPS->GetIntNumber(), &utc);
	 utc.tm_year += 1900;
	 utc.tm_mon++;
	 fNDS2EpochStopDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
	 fNDS2EpochStopTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
      }

      utc_t utcnow;
      TAIntoUTC (now, &utcnow);
      utcnow.tm_year += 1900;
      fTimeDate->SetDate (utcnow.tm_year, utcnow.tm_mon+1, utcnow.tm_mday);
      fTimeTime->SetTime (utcnow.tm_hour, utcnow.tm_min, utcnow.tm_sec);
      fTimeSel[0]->SetState (kButtonDown);
   
      // Set initial tab
      if (fParam->fData.fOnline) {
         fOnlineChannels = kFALSE;
         SetTab (1);
      }
   }


   DiagTabControl::~DiagTabControl ()
   {
      if (!fSSSweepEditorDone) delete fSSSweepEditor;
      // Delete data source selection
      delete fDataInput[0];
      delete fDataInput[1];
      delete fDataInput[2];
      delete fDataInput[3];
      delete fReconnectInput;
      delete fCacheClear;
      delete fDataSelFrame[0];
      delete fDataSelFrame[1];
      delete fDataSelLabel[0];
      delete fDataSelLabel[1];
      delete fNDSName;
      delete fNDSPort;
      delete fNDS2Name;
      delete fNDS2Port;
      delete fNDS2EpochName;
      delete fNDS2EpochStartGPS;
      delete fNDS2EpochStartDate;
      delete fNDS2EpochStartTime;
      delete fNDS2EpochStopGPS;
      delete fNDS2EpochStopDate;
      delete fNDS2EpochStopTime;
      for (int i = 0; i < 13; i++) {
	 delete fNDS2Label[i] ;
      }
      delete fNDS2EpochStopFrame;
      delete fNDS2EpochStartFrame;
      for (int i = 0; i < 4; i++) {
	 delete fNDS2EpochHFrame[i] ;
      }
      delete fNDS2EpochFrame;
      delete fNDS2ServerFrame;
      delete fInputF3;
      delete fSource;
      delete fInputF1;
      delete fInputF2;
      delete fInputGroupLayout;
      for (int i = 0; i < 4; i++) {
         delete fInput1Layout[i];
      }
   
      // Delete measurement
      for (int i = 0; i < 4; i++ ) {
         delete fMeasType[i];
      }
      for (int i = 0; i < kMaxMeasChannel / kShowMeasChannel; i++) {
         delete fMeasNum[i];
      }
      for (Int_t chn = 0; chn < kShowMeasChannel; chn++) {
         delete fMChnActive[chn];
         delete fMChnLabel[chn];
         delete fMChn[chn];
      }
      /* JCB - Was kShowMeasChannel + 3) / 4); */
      for (Int_t chn = 0; chn < ((kShowMeasChannel + 2) / 3); chn++) {
         delete fMChnF[chn];
      }
         // Delete FFT
      delete fFFTStart;
      delete fFFTStop;
      delete fFFTBW;
      delete fFFTSettling;
      delete fFFTRampDown ;
      delete fFFTRampUp ;
      delete fFFTAverages;
      for (int i = 0; i < 3; i++) {
         delete fFFTAverageType[i];
      }
      delete fFFTWindow;
      delete fFFTRemoveDC;
      delete fFFTOverlap;
      delete fFFTAChannels;
      for (int i = 0; i < 13; i++)
      {
         delete fFFTLabel[i];
      }
      for (int i = 0; i < 3; i++) {
         delete fFFTLayout[i];
      }
      for (int i = 0; i < 3; i++) {
         delete fFFTFrame[i];
      }
         // Delete swept sine
      delete fSSStart;
      delete fSSStop;
      delete fSSPoints;
      delete fSSSettling;
      delete fSSRampDown ;
      delete fSSRampUp ;
      delete fSSSweepDir[0];
      delete fSSSweepDir[1];
      delete fSSSweepType[0];
      delete fSSSweepType[1];
      delete fSSSweepType[2];
      // delete fSSSweepFile;
      // delete fSSSweepFileFormat;
      // delete fSSSweepFileRead;
      delete fSSSweepFormat;
      delete fSSSweepEdit;
      delete fSSSweepEditText;
      delete fSSDoublePrecFreq;
      delete fSSMeasTimeSel[0];
      delete fSSMeasTimeSel[1];
      delete fSSMeasTime[0];
      delete fSSMeasTime[1];
      delete fSSAverages;
      delete fSSHarmonicOrder;
      delete fSSWindow;
      delete fSSAChannels;
      delete fSSPowerSpec;
      for (int i = 0; i < 16; i++)
      {
         delete fSSLabel[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fSSLayout[i];
      }
      for (int i = 0; i < 3; i++) {
         delete fSSFrame[i];
      }
         // Delete sine response
      delete fSRMeasTimeSel[0];
      delete fSRMeasTimeSel[1];
      delete fSRMeasTime[0];
      delete fSRMeasTime[1];
      delete fSRSettling;
      delete fSRRampDown ;
      delete fSRRampDown ;
      delete fSRWindow;
      delete fSRPowerSpec;
      delete fSRAverages;
      delete fSRAverageType[0];
      delete fSRAverageType[1];
      delete fSRAverageType[2];
      delete fSRHarmonicOrder;
      for (int i = 0; i < 11; i++) 
      {
         delete fSRLabel[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fSRLayout[i];
      }
      for (int i = 0; i < 3; i++) {
         delete fSRFrame[i];
      }
         // Delete triggered time response
      delete fTSMeasTime;
      delete fTSBW;
      delete fTSSettling;
      delete fTSRampDown ;
      delete fTSRampUp ;
      delete fTSPreTrigTime;
      delete fTSDeadTime;
      delete fTSIncStat;
      delete fTSFilterSpec;
      delete fTSFoton;
      delete fTSAverages;
      delete fTSAverageType[0];
      delete fTSAverageType[1];
      delete fTSAverageType[2];
      for (int i = 0; i < 12; i++) 
      {
         delete fTSLabel[i];
      }
      for (int i = 0; i < 5; i++) {
         delete fTSLayout[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fTSFrame[i];
      }
         // Delete start time
      delete fTimeNow;
      delete fTimeLookup;
      delete fTimePast;
      delete fTimeFuture;
      delete fTimeGPS;
      delete fTimeGPSN;
      delete fTimeDate;
      delete fTimeTime;
      delete fSlowDown;
      for (int i = 0; i < 5; i++) {
         delete fTimeSel[i];
      }
      for (int i = 0; i < 8; i++) {
         delete fTimeLabel[i];
      }
      for (int i = 0; i < 8; i++) {
         delete fTimeLayout[i];
      }
      for (int i = 0; i < 9; i++) {
         delete fTimeFrame[i];
      }

         // Delete measurement information
      delete fMeasTime;
      delete fComment;
      for (int i = 0; i < 2; i++) {
         delete fInfoLabel[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fInfoLayout[i];
      }
      for (int i = 0; i < 2; i++) {
         delete fInfoFrame[i];
      }
   
         // Delete measurement frames & layouts
      for (int i = 0; i < 3; i++) {
         delete fMeas1Layout[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fMeas2Layout[i];
      }
      delete fMeasSelFrame[0];
      delete fMeasSelFrame[1];
      delete fMeasF1;
      delete fMeasF2;
      delete fMeasFFT;
      delete fMeasSS;
      delete fMeasSR;
      delete fMeasTS;
      delete fTimeF;
      delete fInfoF;
      delete fMeasGroupLayout;
   
      // Delete excitation
      for (int i = 0; i < kMaxExcChannel / kShowExcChannel; i++) {
         delete fExcNum[i] ;
      }
      delete fExcSelF;
      delete fExcSelLayout;
      delete fExcNumLayout;
      for (int i = 0; i < kShowExcChannel; i++) {
         delete fExcChnActive[i];
         delete fExcChnName[i];
         delete fExcChnRBType[i][0];
         delete fExcChnRBType[i][1];
         delete fExcChnRBType[i][2];
         delete fExcChnRBName[i];
         delete fExcChnWaveform[i];
         delete fExcChnWaveformFile[i];
         delete fExcChnWaveformFileSel[i];
         delete fExcChnFreq[i];
         delete fExcChnAmpl[i];
         delete fExcChnOffs[i];
         delete fExcChnPhase[i];
         delete fExcChnRatio[i];
         delete fExcChnFreqRange[i];
         delete fExcChnFilterCmd[i];
         delete fExcChnFilterWiz[i];
         for (int j = 0; j < 12; j++) {
            delete fExcChnLabel[i][j];
         }
	 // JCB changed to 5 because a line was added.
         for (int j = 0; j < 5; j++) {
            delete fExcChnFrame[i][j];
         }
         delete fExcF[i];
      }
      for (int i = 0; i < 5; i++) {
         delete fExcChnLayout[i];
      }
   
      // Delete channel lists
      delete [] fMeasChnList;
      delete [] fExcChnList;
      delete [] fDataChnList;
   
      // Delete result
      delete fPad;
      delete fPadLayout;
   }


   // ReadInfo reads the values of the widgets in the Start Time and 
   // Measurement Information frames of the Measurement tab, transferring
   // the values to the fParam structure.  The time fields in the Start Time 
   // frame are adjusted to be consistent with the selected time.
   void DiagTabControl::ReadInfo ()
   {
      if (my_debug) cerr << "DiagTabControl::ReadInfo()" << endl ;
      // read start time
      fParam->fSync.fType = 0;
      fParam->fSync.fStart[0] = 0;
      fParam->fSync.fStart[1] = 0;
      fParam->fSync.fWait = 0;
      for (int i = 0; i < 5; i++) {
         if (fTimeSel[i]->GetState() == kButtonDown) {
            switch (i) {
               // Time now
               case 0: 
               default:
                  {
                     fParam->fSync.fType = 0;
                     break;
                  }
               // future/past
               case 1: 
               case 2:
                  {
                     fParam->fSync.fType = 1;
                     break;
                  }
               // absolute
               case 3: 
               case 4:
                  {
                     fParam->fSync.fType = 2;
                     break;
                  }
            }
            break;
         }
      }
      if (fTimeSel[3]->GetState() == kButtonDown) {
         // GPS start time
         fParam->fSync.fStart[0] = fTimeGPS->GetIntNumber();
         fParam->fSync.fStart[1] = fTimeGPSN->GetIntNumber();
      	 // set date/time
         utc_t	utc;
         TAItoUTC (fParam->fSync.fStart[0], &utc);
         utc.tm_year += 1900;
         utc.tm_mon++;
         fTimeDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
         fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
      }
      else {
         // UTC start time
         utc_t	utc;
         fTimeDate->GetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
         utc.tm_year -= 1900;
         utc.tm_mon--;
         fTimeTime->GetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
         fParam->fSync.fStart[0] = UTCtoTAI (&utc);
         fParam->fSync.fStart[1] = 0;
         // set GPS seconds
         fTimeGPS->SetIntNumber (fParam->fSync.fStart[0]);
      }
      // Validate time against NDS2 epoch times if nds2 selected.
      if (fDataInput[3]->GetState() == kButtonDown)
      {
	 long tMax = fNDS2EpochStopGPS->GetIntNumber() ;
	 long tMin = fNDS2EpochStartGPS->GetIntNumber() ;
	 long tGPS = fTimeGPS->GetIntNumber() ;
	 if (tGPS < tMin || tGPS > tMax)
	 {
	    ostringstream errmsg ;
	    errmsg << "Measurement start time should\nbe within the bounds of\nthe NDS2 epoch times\n" << tMin << " - " << tMax ;
	    new TGMsgBox (gClient->GetRoot(), fDiag, "Warning",
			   errmsg.str().c_str(), kMBIconExclamation, kMBOk, 0);
	 }
      }
      if (fTimeSel[1]->GetState() == kButtonDown) {
         // future
         fParam->fSync.fWait = fTimeFuture->GetNumber();
      }
      else {
         // past
         fParam->fSync.fWait = -fTimePast->GetNumber();
      }
      // slow down
      fParam->fSync.fSlowDown = fSlowDown->GetNumber();
      // read comment
      fParam->fComment = fComment->GetText ();
   }


   void DiagTabControl::WriteInfo (Bool_t mTimeOnly)
   {
      if (my_debug) cerr << "DiagTabControl::WriteInfo()" << endl ;
      utc_t mtime;
      TAItoUTC (fParam->fMeasTime[0], &mtime);
      char s[120];
      strftime (s, 100, "%d/%m/%Y %H:%M:%S", &mtime);
      Int_t usec = fParam->fMeasTime[1] / 1000;
      if (usec != 0) {
         sprintf (s + strlen (s), ".%06i", usec);
      }
      strcpy (s + strlen (s), "  UTC");
      fMeasTime->SetState (kFALSE);
      fMeasTime->SetText (s);
      if (!mTimeOnly) {
         fComment->SetText (fParam->fComment);
         // sync type
         int sel = 0;
         switch (fParam->fSync.fType) {
            case 1: 
               {
                  sel = (fParam->fSync.fWait > 0) ? 1 : 2;
                  break;
               }
            case 2: 
               {
                  sel = (fParam->fSync.fStart[1] != 0) ? 3 : 4;
                  break;
               }
            case 0:
            default:
               {
                  sel = 0;
                  break;
               }
         }
         // past / future
         if (fParam->fSync.fWait > 0) { 
            fTimeFuture->SetNumber(fParam->fSync.fWait);
            fTimePast->SetNumber (0);
         }
         else {
            fTimeFuture->SetNumber (0);
            fTimePast->SetNumber(-fParam->fSync.fWait);
         }
         // GPS
         fTimeGPS->SetIntNumber (fParam->fSync.fStart[0]);
         fTimeGPSN->SetIntNumber (fParam->fSync.fStart[1]);
         // date/time start time
         utc_t	utc;
         TAItoUTC (fParam->fSync.fStart[0], &utc);
         utc.tm_year += 1900;
         utc.tm_mon++;
         fTimeDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
         fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
         for (int i = 0; i < 5; i++) {
            fTimeSel[i]->SetState (i == sel ? kButtonDown : kButtonUp);
         }
      }
      // slow down
      fSlowDown->SetNumber (fParam->fSync.fSlowDown);
      if (my_debug) cerr << "DiagTabControl::WriteInfo() exit" << endl ;
   }


   // Fill in the fParam structure with values from the Input tab.
   void DiagTabControl::ReadDataSourceParam ()
   {
      if (my_debug) cerr << "DiagTabControl::ReadDataSourceParam ()" << endl ;
      fParam->fData.fOnline = 
         (fDataInput[0]->GetState() == kButtonDown) ||
         (fDataInput[1]->GetState() == kButtonDown) ||
         (fDataInput[3]->GetState() == kButtonDown);
      fParam->fData.fReconnect = 
         (fReconnectInput->GetState() == kButtonDown);
      if (fDataInput[1]->GetState() == kButtonDown) {
	fParam->fData.fUserNDS = DataSourceParam_t::kUserNDS1;
      } else if (fDataInput[3]->GetState() == kButtonDown) {
	fParam->fData.fUserNDS = DataSourceParam_t::kUserNDS2;
      } else {
	fParam->fData.fUserNDS = DataSourceParam_t::kNoNDS;
      }	

      fParam->fData.fNDSName = fNDSName->GetText();
      fParam->fData.fNDSPort = fNDSPort->GetIntNumber();
      fSource->ReadData (true);
      cout << "Data source online/nds-type=" << fParam->fData.fOnline 
	   << "/" << fParam->fData.fUserNDS << " Server " 
	   << fParam->fData.fNDSName << ":" << fParam->fData.fNDSPort
	   << endl;
      // Get the values of the gui fields on the Input tab for NDS2 selection.
      fParam->fData.fNDS2Name = fNDS2Name->GetText() ;
      fParam->fData.fNDS2Port = fNDS2Port->GetIntNumber() ;
      // Causes a core dump...
      //fParam->fData.fNDS2EpochName = fNDS2EpochName->GetTextEntry()->GetDisplayText() ;
      fParam->fData.fNDS2Epoch[0] = fNDS2EpochStartGPS->GetIntNumber() ;
      fParam->fData.fNDS2Epoch[1] = fNDS2EpochStopGPS->GetIntNumber() ;
   }


   void DiagTabControl::WriteDataSourceParam ()
   {
      int j = 0;
      if (my_debug) cerr << "DiagTabControl::WriteDataSourceParam ()" << endl ;
      if (!fParam->fData.fOnline) j = 2;
      else if (fParam->fData.fUserNDS == DataSourceParam_t::kUserNDS1) j = 1;
      else if (fParam->fData.fUserNDS == DataSourceParam_t::kUserNDS2) j = 3;
      for (int i = 0; i < 4; ++i) {
         fDataInput[i]->SetState ((i == j) ?  kButtonDown : kButtonUp);
      }
      fReconnectInput->SetState (fParam->fData.fReconnect ? 
                           kButtonDown : kButtonUp);
      fNDSName->SetText (fParam->fData.fNDSName);
      fNDSPort->SetIntNumber (fParam->fData.fNDSPort);
      fSource->SetSel (fParam->fData.fDacc.sel());
      fSource->Build (-2);
      fNDS2Name->SetText (fParam->fData.fNDS2Name) ;
      fNDS2Port->SetIntNumber (fParam->fData.fNDS2Port) ;
      
      // Need to see if the name is in the menu, if so select it.
      // If not, add it.
      {
	 TGLBEntry *name = fNDS2EpochName->FindEntry(fParam->fData.fNDS2EpochName) ;
	 if (name)
	 {
	    if (my_debug) cerr << "WriteDataSourceParam() - epoch name " << fParam->fData.fNDS2EpochName << " found, id is " << name->EntryId() << endl ;
	    fNDS2EpochName->Select(name->EntryId()) ;
	 }
	 else
	 {
	    Int_t entries = fNDS2EpochName->GetNumberOfEntries() ;
	    if (my_debug) cerr << "WriteDataSourceParam() - epoch name " << fParam->fData.fNDS2EpochName << " not found, there are " << entries << " in the menu" << endl ;
	    fNDS2EpochName->AddEntry(fParam->fData.fNDS2EpochName, entries) ;
	    // Also add it to the vector of epochs.
	    NDS2Epoch_t *tmp = new NDS2Epoch_t ;
	    tmp->fEpochName = fParam->fData.fNDS2EpochName ;
	    tmp->fEpoch[0] = fNDS2EpochStartGPS->GetIntNumber() ;
	    tmp->fEpoch[1] = fNDS2EpochStopGPS->GetIntNumber() ;
	    fEpochList.push_back(tmp) ;

	    // Select the entry.
	    fNDS2EpochName->Select(entries) ;
	 }
      }

      // Fill in the Date/time for the epoch start given the GPS time.
      fNDS2EpochStartGPS->SetIntNumber (fParam->fData.fNDS2Epoch[0]) ;
      {
	 // set UTC start time
	 utc_t	utc;
	 TAItoUTC (fNDS2EpochStartGPS->GetIntNumber(), &utc);
	 utc.tm_year += 1900;
	 utc.tm_mon++;
	 fNDS2EpochStartDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
	 fNDS2EpochStartTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
      }
      fNDS2EpochStopGPS->SetIntNumber (fParam->fData.fNDS2Epoch[1]) ;
      // Fill in the Date/time for the epoch stop given the GPS time.
      {
	 // set UTC stop time
	 utc_t	utc;
	 TAItoUTC (fNDS2EpochStopGPS->GetIntNumber(), &utc);
	 utc.tm_year += 1900;
	 utc.tm_mon++;
	 fNDS2EpochStopDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
	 fNDS2EpochStopTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
      }
   }


   // Fill in the fParam structure with values from the GUI fields for the 
   // measurement parameters for the selected analysis (indicated by the id argument)
   // These values are displayed in the Measurment tab under the Measurement Channels
   // frames and vary according to the selected analysis.
   void DiagTabControl::ReadMeasParam (Int_t id)
   {
      switch (id) {
         /// FFT
         case 0:
            {
               fParam->fMeas.fMeasType = 0;
               fParam->fMeas.fStart = fFFTStart->GetNumber();
               fParam->fMeas.fStop = fFFTStop->GetNumber();
               fParam->fMeas.fResolutionBW = fFFTBW->GetNumber();
               // Convert Settling time from percent to actual value.
               fParam->fMeas.fTimeSettling = fFFTSettling->GetNumber() / 100.;
               fParam->fMeas.fRampDownTime = fFFTRampDown->GetNumber() ;
               fParam->fMeas.fRampUpTime = fFFTRampUp->GetNumber() ;
               fParam->fMeas.fWindow = fFFTWindow->GetSelected();
               fParam->fMeas.fRemoveDC = 
                  (fFFTRemoveDC->GetState() == kButtonDown);
               fParam->fMeas.fOverlap = fFFTOverlap->GetNumber() / 100.;
               fParam->fMeas.fAChannels = fFFTAChannels->GetIntNumber();
               fParam->fMeas.fAverages = fFFTAverages->GetIntNumber();
               fParam->fMeas.fBurstNoiseQuietTime_s = fFFTBurstNoiseQuietTime_s->GetNumber();
               fParam->fMeas.fAverageType = 0;
               for (int i = 0; i < 3; i++) {
                  if (fFFTAverageType[i]->GetState() == kButtonDown) {
                     fParam->fMeas.fAverageType = i;
                  }
               }
               break;
            }
         /// Swept sine
         case 1:
            {
               fParam->fMeas.fMeasType = 1;
               fParam->fMeas.fStart = fSSStart->GetNumber();
               fParam->fMeas.fStop = fSSStop->GetNumber();
               fParam->fMeas.fPoints = fSSPoints->GetIntNumber();
               fParam->fMeas.fTimeSettling = fSSSettling->GetNumber() / 100.;
               fParam->fMeas.fRampDownTime = fSSRampDown->GetNumber() ;
               fParam->fMeas.fRampUpTime = fSSRampUp->GetNumber() ;
               fParam->fMeas.fSweepDir = (fSSSweepDir[0]->GetState() == kButtonDown) ? 0 : 1;
               fParam->fMeas.fSweepType = 0;
               for (int i = 0; i < 2; i++) {
                  if (fSSSweepType[i]->GetState() == kButtonDown) {
                     fParam->fMeas.fSweepType = i;
                  }
               }
            
               if (fSSSweepType[2]->GetState() == kButtonDown) {
                  switch (fSSSweepFormat->GetSelected()) {
                     // frequency / amplitude
                     case 0:
                     default:
                        {
                           fParam->fMeas.fSweepType = 3;
                           break;
                        }
                     // envelope
                     case 1:
                        {
                           fParam->fMeas.fSweepType += 4;
                           break;
                        }
                     // frequency
                     case 2:
                        {
                           fParam->fMeas.fSweepType = 2;
                           break;
                        }
                  }
               }
               fParam->fMeas.fDoublePrecFreq = (fSSDoublePrecFreq->GetState() == kButtonDown);
               for (int i = 0; i < 2; i++) {
                  if (fSSMeasTimeSel[i]->GetState() == kButtonDown) {
                     fParam->fMeas.fTimeMeas[i] = 
                        fSSMeasTime[i]->GetNumber();
                  }
                  else {
                     fParam->fMeas.fTimeMeas[i] = -1;
                  }
               }
               Swap (fParam->fMeas.fTimeMeas[0], fParam->fMeas.fTimeMeas[1]);
               fParam->fMeas.fAverages = fSSAverages->GetIntNumber();
               fParam->fMeas.fHarmonicOrder = fSSHarmonicOrder->GetIntNumber();
               fParam->fMeas.fWindow = fSSWindow->GetSelected();
               fParam->fMeas.fAChannels = fSSAChannels->GetIntNumber();
               fParam->fMeas.fPowerSpec = 
                  (fSSPowerSpec->GetState() == kButtonDown);
               break;
            }
         /// Sine response
         case 2:
            {
               fParam->fMeas.fMeasType = 2;
               for (int i = 0; i < 2; i++) {
                  if (fSRMeasTimeSel[i]->GetState() == kButtonDown) {
                     fParam->fMeas.fTimeMeas[i] = 
                        fSRMeasTime[i]->GetNumber();
                  }
                  else {
                     fParam->fMeas.fTimeMeas[i] = -1;
                  }
               }
               Swap (fParam->fMeas.fTimeMeas[0], fParam->fMeas.fTimeMeas[1]);
               fParam->fMeas.fTimeSettling = fSRSettling->GetNumber() / 100.;
               fParam->fMeas.fRampDownTime = fSRRampDown->GetNumber() ;
               fParam->fMeas.fRampUpTime = fSRRampUp->GetNumber() ;
               fParam->fMeas.fHarmonicOrder = fSRHarmonicOrder->GetIntNumber();
               fParam->fMeas.fWindow = fSRWindow->GetSelected();
               fParam->fMeas.fPowerSpec = (fSRPowerSpec->GetState() == kButtonDown);
               fParam->fMeas.fAverages = fSRAverages->GetIntNumber();
               fParam->fMeas.fAverageType = 0;
               for (int i = 0; i < 3; i++) {
                  if (fSRAverageType[i]->GetState() == kButtonDown) {
                     fParam->fMeas.fAverageType = i;
                  }
               }
               break;
            }
         /// Triggered time response
         case 3:
            {
               fParam->fMeas.fMeasType = 3;
               fParam->fMeas.fTimeMeas[0] = fTSMeasTime->GetNumber();
               fParam->fMeas.fTimeMeas[1] = -1;
               fParam->fMeas.fSignalBW = fTSBW->GetNumber();
               fParam->fMeas.fTimeSettling = fTSSettling->GetNumber() / 100.;
               fParam->fMeas.fRampDownTime = fTSRampDown->GetNumber() ;
               fParam->fMeas.fRampUpTime = fTSRampUp->GetNumber() ;
               fParam->fMeas.fTimePreTrig = fTSPreTrigTime->GetNumber() / 100.;
               fParam->fMeas.fTimeDead = fTSDeadTime->GetNumber() / 100.;
               fParam->fMeas.fStatistics = (fTSIncStat->GetState() == kButtonDown);
               fParam->fMeas.fFilter = fTSFilterSpec->GetText();
               fParam->fMeas.fAverages = fTSAverages->GetIntNumber();
               fParam->fMeas.fBurstNoiseQuietTime_s = fTSBurstNoiseQuietTime_s->GetNumber();
               fParam->fMeas.fAverageType = 0;
               for (int i = 0; i < 3; i++) {
                  if (fTSAverageType[i]->GetState() == kButtonDown) {
                     fParam->fMeas.fAverageType = i;
                  }
               }
               break;
            }
      }
   }


   void DiagTabControl::WriteMeasParam (Int_t id)
   {
      switch (id) {
         /// FFT
         case 0:
            {
               fFFTStart->SetNumber (fParam->fMeas.fStart);
               fFFTStop->SetNumber (fParam->fMeas.fStop);
               fFFTBW->SetNumber (fParam->fMeas.fResolutionBW);
               fFFTSettling->SetNumber (100.*fParam->fMeas.fTimeSettling);
               fFFTRampDown->SetNumber (fParam->fMeas.fRampDownTime) ;
	       fFFTRampUp->SetNumber (fParam->fMeas.fRampUpTime) ;
               fFFTWindow->Select (fParam->fMeas.fWindow);
               fFFTRemoveDC->SetState (fParam->fMeas.fRemoveDC ? 
                                    kButtonDown : kButtonUp);
               fFFTOverlap->SetNumber (100.*fParam->fMeas.fOverlap);
               fFFTAChannels->SetIntNumber (fParam->fMeas.fAChannels);
               fFFTAverages->SetIntNumber (fParam->fMeas.fAverages);
               fFFTBurstNoiseQuietTime_s->SetNumber( fParam->fMeas.fBurstNoiseQuietTime_s);
               for (int i = 0; i < 3; i++) {
                  fFFTAverageType[i]->SetState 
                     ((fParam->fMeas.fAverageType == i) ? 
                     kButtonDown : kButtonUp);
               }
               fFFTAverages->SetState (fParam->fMeas.fAverageType != 2);
               break;
            }
         /// Swept sine
         case 1:
            {
               fSSStart->SetNumber (fParam->fMeas.fStart);
               fSSStop->SetNumber (fParam->fMeas.fStop);
               fSSPoints->SetIntNumber (fParam->fMeas.fPoints);
               fSSSettling->SetNumber (100.*fParam->fMeas.fTimeSettling);
               fSSRampDown->SetNumber (fParam->fMeas.fRampDownTime) ;
               fSSRampUp->SetNumber (fParam->fMeas.fRampUpTime) ;
               if (fParam->fMeas.fSweepDir == 0) {
                  fSSSweepDir[0]->SetState (kButtonDown);
                  fSSSweepDir[1]->SetState (kButtonUp);
               }
               else {
                  fSSSweepDir[1]->SetState (kButtonDown);
                  fSSSweepDir[0]->SetState (kButtonUp);
               }
               Int_t sweeptype = 0;
               switch (fParam->fMeas.fSweepType) {
                  // frequency / amplitude
                  case 3:
                     {
                        sweeptype = 0;
                        fSSSweepType[2]->SetState (kButtonDown);
                        fSSSweepFormat->Select (0);
                        break;
                     }
                  // envelope
                  case 4:
                  case 5:
                     {
                        sweeptype = fParam->fMeas.fSweepType - 4;
                        fSSSweepType[2]->SetState (kButtonDown);
                        fSSSweepFormat->Select (1);
                        break;
                     }
                  // frequency
                  case 2:
                     {
                        sweeptype = 0;
                        fSSSweepType[2]->SetState (kButtonDown);
                        fSSSweepFormat->Select (2);
                        break;
                     }
                  case 0:
                  case 1:
                     {
                        fSSSweepType[2]->SetState (kButtonUp);
                        sweeptype = fParam->fMeas.fSweepType;
                        break;
                     }
                  default:
                     {
                        fSSSweepType[2]->SetState (kButtonUp);
                        sweeptype = 0;
                        break;
                     }
               }
               for (int i = 0; i < 2; i++) {
                  fSSSweepType[i]->SetState 
                     ((sweeptype == i) ? kButtonDown : kButtonUp);
               }
               fSSDoublePrecFreq->SetState( fParam->fMeas.fDoublePrecFreq ? kButtonDown : kButtonUp);
               Swap (fParam->fMeas.fTimeMeas[0], fParam->fMeas.fTimeMeas[1]);
               for (int i = 0; i < 2; i++) {
                  if (fParam->fMeas.fTimeMeas[i] < 0) {
                     fSSMeasTimeSel[i]->SetState (kButtonUp);
                     fSSMeasTime[i]->SetNumber (0);
                  }
                  else {
                     fSSMeasTimeSel[i]->SetState (kButtonDown);
                     fSSMeasTime[i]->SetNumber (fParam->fMeas.fTimeMeas[i]);
                  }
               }
               Swap (fParam->fMeas.fTimeMeas[0], fParam->fMeas.fTimeMeas[1]);
               fSSAverages->SetIntNumber (fParam->fMeas.fAverages);
               fSSHarmonicOrder->SetIntNumber (fParam->fMeas.fHarmonicOrder);
               fSSWindow->Select (fParam->fMeas.fWindow);
               fSSAChannels->SetIntNumber (fParam->fMeas.fAChannels);
               fSSPowerSpec->SetState (fParam->fMeas.fPowerSpec ?
                                    kButtonDown : kButtonUp);
               break;
            }
         /// Sine response
         case 2:
            {
               Swap (fParam->fMeas.fTimeMeas[0], fParam->fMeas.fTimeMeas[1]);
               for (int i = 0; i < 2; i++) {
                  if (fParam->fMeas.fTimeMeas[i] < 0) {
                     fSRMeasTimeSel[i]->SetState (kButtonUp);
                     fSRMeasTime[i]->SetNumber (0);
                  }
                  else {
                     fSRMeasTimeSel[i]->SetState (kButtonDown);
                     fSRMeasTime[i]->SetNumber (fParam->fMeas.fTimeMeas[i]);
                  }
               }
               Swap (fParam->fMeas.fTimeMeas[0], fParam->fMeas.fTimeMeas[1]);
               fSRSettling->SetNumber (100.*fParam->fMeas.fTimeSettling);
               fSRRampDown->SetNumber (fParam->fMeas.fRampDownTime) ;
               fSRRampUp->SetNumber (fParam->fMeas.fRampUpTime) ;
               fSRHarmonicOrder->SetIntNumber (fParam->fMeas.fHarmonicOrder);
               fSRWindow->Select (fParam->fMeas.fWindow);
               fSRPowerSpec->SetState (fParam->fMeas.fPowerSpec ?
                                    kButtonDown : kButtonUp);
               fSRAverages->SetIntNumber (fParam->fMeas.fAverages);
               for (int i = 0; i < 3; i++) {
                  fSRAverageType[i]->SetState 
                     ((fParam->fMeas.fAverageType == i) ? 
                     kButtonDown : kButtonUp);
               }
               fSRAverages->SetState (fParam->fMeas.fAverageType != 2);
               break;
            }
         /// Triggered time response
         case 3:
            {
               fTSMeasTime->SetNumber (fParam->fMeas.fTimeMeas[0]);
               fTSBW->SetNumber (fParam->fMeas.fSignalBW);
               fTSSettling->SetNumber (100.*fParam->fMeas.fTimeSettling);
               fTSRampDown->SetNumber (fParam->fMeas.fRampDownTime) ;
               fTSRampUp->SetNumber (fParam->fMeas.fRampUpTime) ;
               fTSPreTrigTime->SetNumber (100.*fParam->fMeas.fTimePreTrig);
               fTSDeadTime->SetNumber (100.*fParam->fMeas.fTimeDead);
               fTSIncStat->SetState (fParam->fMeas.fStatistics ?
                                    kButtonDown : kButtonUp);
               fTSFilterSpec->SetText (fParam->fMeas.fFilter);
               fTSAverages->SetIntNumber (fParam->fMeas.fAverages);
               fTSBurstNoiseQuietTime_s->SetNumber( fParam->fMeas.fBurstNoiseQuietTime_s);
               for (int i = 0; i < 3; i++) {
                  fTSAverageType[i]->SetState 
                     ((fParam->fMeas.fAverageType == i) ? 
                     kButtonDown : kButtonUp);
               }
               fTSAverages->SetState (fParam->fMeas.fAverageType != 2);
               break;
            }
      }
   }


   // The id is the value of radio button labeled "Channels x to x". Should range
   // from 0 to 5 (as of Nov. 2015).
   // Copy the channel names from the ChannelCombobox widget to fParam->fMeas.
   void DiagTabControl::ReadMeasChannel (Int_t id)
   {
      // kShowMeasChannel is the number of measurment channels shown on the
      // Measurement Channels frame.  kMaxMeasChannel is the total number of
      // measurment channels that can be shown.  It can be adjusted if need be.
      // kMaxMeasChannel/kShowMeasChannel is the number of sets of channels in the
      // Measurement Channels frame.
      if (my_debug) cerr << "DiagTabControl::ReadMeasChannel( id = " << id << " )" << endl ;
      if ((id < 0) || (id >= kMaxMeasChannel / kShowMeasChannel)) {
         return;
      }
      for (int i = 0; i < kShowMeasChannel; i++) {
	 // fMChn[i] is a TLGChannelCombobox pointer.
         const char* chn = fMChn[i]->GetChannel();
	 if (my_debug) cerr << "  channel " << i << " name is " << chn << endl ;

	 long userdata = (long) (fMChn[i]->GetChannelRate()) ;
	 if (my_debug) cerr << "  userdata is " << userdata << endl ;

#ifdef TEST_RATE
         fParam->fMeas.fMeasChn[id * kShowMeasChannel + i].first = (chn != 0) ? chn : "";
	 fParam->fMeas.fMeasChn[id * kShowMeasChannel + i].second = userdata ;
#else
         fParam->fMeas.fMeasChn[id * kShowMeasChannel + i] = (chn != 0) ? chn : "";
#endif
         fParam->fMeas.fMeasActive[id * kShowMeasChannel + i] = fMChnActive[i]->GetState() == kButtonDown;
      }
      if (my_debug) cerr << "DiagTabControl::ReadMeasChannel() - return" << endl ;
   }


   // Transfer channel names from the fParam->fMeas structure to the GUI menu items.  Note
   // that this may occur before the channel list has been read from the NDS or NDS2 server
   // in the case that an xml file is read that contains a different source from the one
   // currently read.  So, we have to take a leap of faith and just set the GUI channel menus
   // to the new values.
   void DiagTabControl::WriteMeasChannel (Int_t id)
   {
      if (my_debug) cerr << "DiagTabControl::WriteMeasChannel( id = " << id << " )" << endl ;
      if ((id < 0) || (id >= kMaxMeasChannel / kShowMeasChannel)) {
         return;
      }
      for (int i = 0; i < kShowMeasChannel; i++) {
#ifdef TEST_RATE
	 string name = fParam->fMeas.fMeasChn[id * kShowMeasChannel + i].first ;
	 int	rate = fParam->fMeas.fMeasChn[id * kShowMeasChannel + i].second ;
	 if (my_debug) cerr << "DiagTabControl::WriteMeasChannel(), i = " << i << ", name = " << name << ", rate = " << rate << endl;
	 // SetChannel() is supposed to return false if the channel isn't in the list.
	 if (my_debug) cerr << "  Add " << name << " (" << rate << ") to fMChn["<<i<<"]" << endl ;
	 fMChn[i]->SetChannel(name.c_str()) ;
	 fMChn[i]->SetChannelRate(rate) ;
	 fMChnActive[i]->SetState(fParam->fMeas.fMeasActive[id * kShowMeasChannel + i] ?  kButtonDown : kButtonUp);
#else
	 string name = fParam->fMeas.fMeasChn[id * kShowMeasChannel + i] ;
	 // SetChannel() is supposed to return false if the channel isn't in the list.
	 fMChn[i]->SetChannel(name.c_str()) ;
	 fMChnActive[i]->SetState(fParam->fMeas.fMeasActive[id * kShowMeasChannel + i] ?  kButtonDown : kButtonUp);
#endif
      }
      if (my_debug) cerr << "DiagTabControl::WriteMeasChannel() - return" << endl ;
   }


   // Fill in the fParam structure with values from the GUI Excitation tab.
   // The id argument indicates the current set of excitation channels selected
   // from the Channel Selection frame radio buttons.
   void DiagTabControl::ReadExcChannel (Int_t id)
   {
      if ((id < 0) || (id >= kMaxExcChannel / kShowExcChannel)) {
         return;
      }
      for (int i = 0; i < kShowExcChannel; i++) {
         Int_t chn = id * kShowExcChannel + i;
         fParam->fExc[chn].fActive = 
            fExcChnActive[i]->GetState() == kButtonDown;
         const char* chnname = fExcChnName[i]->GetChannel();
         fParam->fExc[chn].fName = (chnname != 0) ? chnname : "";
         if (fExcChnRBType[i][1]->GetState() == kButtonDown) {
            fParam->fExc[chn].fRBType = 1;
         }
         else if (fExcChnRBType[i][2]->GetState() == kButtonDown) {
            fParam->fExc[chn].fRBType = 2;
         }
         else {
            fParam->fExc[chn].fRBType = 0;
         }
         fParam->fExc[chn].fRBName = fExcChnRBName[i]->GetChannel();
         fParam->fExc[chn].fWaveform = fExcChnWaveform[i]->GetSelected();
         fParam->fExc[chn].fWaveformFile = fExcChnWaveformFile[i]->GetText();
         fParam->fExc[chn].fFreq = fExcChnFreq[i]->GetNumber();
         fParam->fExc[chn].fAmpl = fExcChnAmpl[i]->GetNumber();
         fParam->fExc[chn].fOffs = fExcChnOffs[i]->GetNumber();
         fParam->fExc[chn].fPhase = fExcChnPhase[i]->GetNumber()*kDeg;
         fParam->fExc[chn].fRatio = fExcChnRatio[i]->GetNumber()/100.;
         fParam->fExc[chn].fFreqRange = fExcChnFreqRange[i]->GetNumber();
         fParam->fExc[chn].fAmplRange = fExcChnAmplRange[i]->GetNumber();
         fParam->fExc[chn].fFilterCmd = fExcChnFilterCmd[i]->GetText();
      }
   }


   void DiagTabControl::WriteExcChannel (Int_t id)
   {
      if ((id < 0) || (id >= kMaxExcChannel / kShowExcChannel)) {
         return;
      }
      for (int i = 0; i < kShowExcChannel; i++) {
         Int_t chn = id * kShowExcChannel + i;
         fExcChnActive[i]->SetState
            (fParam->fExc[chn].fActive ? kButtonDown : kButtonUp);
         fExcChnName[i]->SetChannel (fParam->fExc[chn].fName);
         for (int j = 0; j < 3; j++) {
            fExcChnRBType[i][j]->SetState 
               ((fParam->fExc[chn].fRBType == j) ? kButtonDown : kButtonUp);
         }
         fExcChnRBName[i]->SetChannel (fParam->fExc[chn].fRBName);
         // set disable/enable
         if ((fMeasSel == 1) && (chn > 0)) {
            fExcChnActive[i]->SetState (kButtonDisabled);
            fExcChnName[i]->SetState (kFALSE);
            for (int j = 0; j < 3; j++) {
               fExcChnRBType[i][j]->SetState (kButtonDisabled);
            }
            fExcChnRBName[i]->SetState (kFALSE);
         }
         else {
            fExcChnName[i]->SetState (kTRUE);
            fExcChnRBName[i]->SetState (kTRUE);
         }
         // waveform
         fExcChnWaveform[i]->Select (fParam->fExc[chn].fWaveform);
         fExcChnWaveformFile[i]->SetText (fParam->fExc[chn].fWaveformFile);
         if ((fMeasSel == 1) || (fMeasSel == 2)) {
            ((WaveformSelection*)fExcChnWaveform[i])->SetState (kFALSE);
            fExcChnWaveformFile[i]->SetState (kFALSE);
            fExcChnWaveformFileSel[i]->SetState (kButtonDisabled);
         }
         else {
            ((WaveformSelection*)fExcChnWaveform[i])->SetState (kTRUE);
            fExcChnWaveformFile[i]->SetState (kTRUE);
            fExcChnWaveformFileSel[i]->SetState (kButtonUp);
         }
         // amplitude
         fExcChnAmpl[i]->SetNumber (fParam->fExc[chn].fAmpl);
         if ((fMeasSel == 1) && (chn > 0)) {
            fExcChnAmpl[i]->SetState (kFALSE);
         }
         else {
            fExcChnAmpl[i]->SetState (kTRUE);
         }
         // freq, offs, phase
         fExcChnFreq[i]->SetNumber (fParam->fExc[chn].fFreq);
         fExcChnOffs[i]->SetNumber (fParam->fExc[chn].fOffs);
         fExcChnPhase[i]->SetNumber (fParam->fExc[chn].fPhase/kDeg);
         if (fMeasSel == 1) {
            fExcChnFreq[i]->SetState (kFALSE);
            fExcChnOffs[i]->SetState (kFALSE);
            fExcChnPhase[i]->SetState (kFALSE);
         }
         else {
            fExcChnFreq[i]->SetState (kTRUE);
            fExcChnOffs[i]->SetState (kTRUE);
            fExcChnPhase[i]->SetState (kTRUE);
         }
         fExcChnFreq[i]->SetNumber (fParam->fExc[chn].fFreq);
         // ratio, ranges
         fExcChnRatio[i]->SetNumber (100.*fParam->fExc[chn].fRatio);
         fExcChnFreqRange[i]->SetNumber (fParam->fExc[chn].fFreqRange);
         fExcChnAmplRange[i]->SetNumber (fParam->fExc[chn].fAmplRange);
         fExcChnFilterCmd[i]->SetText (fParam->fExc[chn].fFilterCmd);
         if ((fMeasSel == 1) || (fMeasSel == 2)) {
            fExcChnRatio[i]->SetState (kFALSE);
            fExcChnFreqRange[i]->SetState (kFALSE);
            fExcChnAmplRange[i]->SetState (kFALSE);
            fExcChnFilterCmd[i]->SetState (kFALSE);
            fExcChnFilterWiz[i]->SetState (kButtonDisabled);
         }
         else {
            fExcChnRatio[i]->SetState (kTRUE);
            fExcChnFreqRange[i]->SetState (kTRUE);
            fExcChnAmplRange[i]->SetState (kTRUE);
            fExcChnFilterCmd[i]->SetState (kTRUE);
            fExcChnFilterWiz[i]->SetState (kButtonUp);
         }
      }
   }


   void DiagTabControl::ReadIterParam (Int_t id)
   {
      switch (id) {
         /// Repeat
         case 0:
            {
               fParam->fIter.fIterType = 0;
               break;
            }
         /// Scan
         case 1:
            {
               fParam->fIter.fIterType = 1;
               break;
            }
         /// Optimize
         case 2:
            {
               fParam->fIter.fIterType = 2;
               break;
            }
      }
   }


   void DiagTabControl::WriteIterParam (Int_t id)
   {
   }

   // Give access to the vector of bogus channel names created 
   // by ResolveMeasurmentChannels().
   std::vector<std::string> *DiagTabControl::GetBogusChannels()
   {
      return &fBogusList ;
   }

   // Since the user can enter channels manually instead of selecting them with
   // the nice menu system, go through the measurement channels to try to find
   // all the channels that are selected.  Look for the names in the channel list,
   // and if found, set the channel data rate to the user data (which was filled
   // in when the channel list was read).
   void DiagTabControl::ResolveMeasurementChannels()
   {

      // Clear the bogus channel list.
      fBogusList.clear() ;
      // Read what is on the current screen so it gets included in the fParam->fMeas.
      ReadMeasChannel(fMeasRangeSel) ;
      // The channel list is either fDataChnList for NDS, NDS2, or Lidax, or it's fMeasChnList if online.
      // If it's not online, UpdateChannels() assigns the fDataChnList.
      bool online   = (fDataInput[0]->GetState() == kButtonDown) ;
      bool usernds  = (fDataInput[1]->GetState() == kButtonDown) ;
      bool usernds2 = (fDataInput[3]->GetState() == kButtonDown) ;
      bool lidax    = (fDataInput[2]->GetState() == kButtonDown) ;

      ligogui::ChannelEntry *chnlist = 0 ;
      UInt_t chnlist_len = 0 ;

      if (online) {
	 chnlist = fMeasChnList ;
	 chnlist_len = fMeasChnLen ;
      }
      else if (usernds || usernds2 || lidax) {
	 chnlist = fDataChnList ;
	 chnlist_len = fDataChnLen ;
      }


      if (my_debug) cerr << "DiagTabControl::ResolveMeasurementChannels() - chnlist_len = " << chnlist_len << endl ;
      for (int i = 0; i < kMaxMeasChannel; i++)
      {
	 if (fParam->fMeas.fMeasActive[i])
	 {
	    string name = fParam->fMeas.fMeasChn[i].first ;
	    long rate = (long) fParam->fMeas.fMeasChn[i].second ;
	    if (my_debug) {
	       cerr << "DiagTabControl::ResolveMeasurementChannels() - name = " << name << endl ;
	       cerr << "  channel rate = " << rate << endl ;
	    }
	    if (name.length() != 0 && rate == 0)
	    {
	       unsigned int j ;
	       // Find the channel name in the list.
	       for (j = 0; j < chnlist_len; j++)
	       {
		  if (strcmp(name.c_str(),  chnlist[j].Name()) == 0)
		  {
		     fParam->fMeas.fMeasChn[i].second = chnlist[j].Rate() ;
		     if (my_debug) cerr << "    " << name << " rate changed to " << fParam->fMeas.fMeasChn[i].second << endl ;
		     break ;
		  }
	       }
	       if (j == chnlist_len) {
		  fBogusList.push_back(name) ;
	       }
	    }
	 }
      }
      // Write the potentially modified channels back to the current screen.
      WriteMeasChannel(fMeasRangeSel) ;
   }


   // Go thru the measurement channels, try to select any that are displayed.
   // This might be a bit time consuming, but it's needed for when xml files
   // are read so the proper channels (with the correct data rate) are sent to
   // diag.
   void DiagTabControl::SelectMeasurementChannels()
   {
      for (int i = 0; i < kShowMeasChannel; i++)
      {
	 string name = string(fMChn[i]->GetChannel()) ;
	 long rate = (long) (fMChn[i]->GetChannelRate()) ;
	 if (name.length() != 0)
	 {
	    if (my_debug) cerr << "SelectMeasurementChannels() - select " << name << endl ;
	    if (my_debug) cerr << " Channel rate = " << rate << endl ;
	    bool rc = fMChn[i]->SelectChannel(name.c_str(), (int) rate) ;
	    if (my_debug) cerr << "  fMChn["<<i<<"]->SelectChannel(" << name.c_str() << ", " << rate << ") " << (rc ? "succeeded" : "failed") << endl ;
	 }
      }
   }

   // UpdateChannels gets called from ChangeTab() if any tab is
   // selected other than 0 which is the input tab.  It also gets
   // called from diagmain.cc when a new file is opened, since the
   // source can change.
   void DiagTabControl::UpdateChannels ()
   {
      if (my_debug) cerr << "DiagTabControl::UpdateChannels()" << endl ;

      // get data source
      bool online   = (fDataInput[0]->GetState() == kButtonDown);
      bool usernds  = (fDataInput[1]->GetState() == kButtonDown);
      bool usernds2 = (fDataInput[3]->GetState() == kButtonDown);
      bool userndsx = usernds || usernds2;
      ostringstream ndsaddr ;
      int port ;
      // If we're using NDS2 get the server name and port from the
      // NDS2 selection fields.  Otherwise, get them from the 
      // NDS selection as before.
      if (usernds2) {
	 unsigned long epoch_start = fNDS2EpochStartGPS->GetIntNumber() ;
	 unsigned long epoch_end = fNDS2EpochStopGPS->GetIntNumber() ;
	 ndsaddr << fNDS2Name->GetText() ;
	 port = fNDS2Port->GetIntNumber() ;
	 if (port > 0) {
	    ndsaddr << ":" << port ;
	 }
	 // Add in the epoch times to make this unique for the epoch
	 ndsaddr << "?epoch_start=" << epoch_start ;
	 ndsaddr << "&epoch_end=" << epoch_end ;
      } else {
	 ndsaddr << fNDSName->GetText() ;
	 port = fNDSPort->GetIntNumber() ;
	 if (port > 0) {
	    ndsaddr << ":" << port;
	 }
      }
      dfm::dataservername ndsname (dfm::st_NDS, ndsaddr.str());
      if (my_debug) cerr << " UpdateChannels() - ndsname = " << ndsname.get() << endl ;
      if (my_debug) cerr << " UpdateChannels() - fUserNDSName = " << fUserNDSName << endl ;
      // check if still online
      if (fOnlineChannels && online) {
	 if (my_debug) cerr << " UpdateChannels() - fOnlineChannels && online true, return." << endl ;
         return;
      }

      // check if the same user nds
      if (fUserNDSChannels && userndsx &&
         (ndsname == dfm::dataservername (fUserNDSName))) {
	 if (my_debug) cerr << " UpdateChannels() - same user nds" << endl ;
	 // Go thru the measurement channels, try to select any that are displayed.
	 SelectMeasurementChannels() ;
         return;
      }
      // may take a while
      gVirtualX->SetCursor (fId, fWaitCursor);
      gVirtualX->Update();
      ligogui::ChannelEntry* newchn = 0;
      UInt_t newchn_len = 0;
      // get channel list from NDS or Lidax
      if (!online) {
         // Generate new list
         fantom::channellist clist;
         if (usernds) {
	    cout << " Get nds channel list" << endl;
            dfm::dataaccess::ClearCache();
            dfm::dataserver nds (dfm::st_NDS, ndsname.getAddr());
            string u = string ("nds://") + ndsname.getAddr() + "/frames";
            dfm::UDN udn (u.c_str());
            cout << "NDS size = " << nds.size() << endl << "insert udn\n";
            dfm::UDNInfo* uinfo = nds.insert (udn);
           cout << "NDS size = " << nds.size() << endl;
            if (uinfo) {
	       cout << "Channel list from " << u << endl;
               nds.lookupUDN (udn);
               clist = uinfo->channels();
            }
         }
         else if (usernds2) {
            cout << " Get nds2 channel list" << endl;
	    if (my_debug) cerr << "  create dfm::dataserver object called nds" << endl ;
            dfm::dataserver nds (dfm::st_SENDS, ndsname.getAddr());
            string u = string ("nds2://") + ndsname.getAddr() + "/frames";
	    // Before proceeding, see if the nds server can be contacted.
	    // A failure may be because of an improper name or no kerberos ticket.
	    // We really need to do this here, if you go much further there's no
	    // recovery if there's no ticket and you try again later.
	    {
	       if (my_debug) cerr << "  Check for kerberos ticket" << endl ;

               try {
                 NDS::connection nds2(fNDS2Name->GetText(),
                                      fNDS2Port->GetIntNumber());

                 NDS::epoch epoch;
                 NDS::channel_predicate_object pred;

                 epoch = NDS::epoch(
                     NDS::buffer::gps_second_type(fNDS2EpochStartGPS->GetIntNumber()),
                     NDS::buffer::gps_second_type(fNDS2EpochStopGPS->GetIntNumber()));

                 pred.set(epoch);

                 //get only raw and reduced data set channels
                 pred.set(NDS::channel::CHANNEL_TYPE_RAW);
                 pred.set(NDS::channel::CHANNEL_TYPE_RDS);

                 auto chan_list = nds2.find_channels(pred);

                 // convert from nds2 channel object to fantom channel object
                 // used in dtt
                 clist.clear();
                 for(auto chan: chan_list)
                 {
                   clist.emplace_back(chan.Name().c_str(), chan.SampleRate());
                 }
               }
               catch (std::runtime_error exc)
               {
                  // Create an error popup that warns the user this won't work.
                  ostringstream errmsg ;
                  errmsg << " Cannot connect to " << fNDS2Name->GetText() << ":" << fNDS2Port->GetIntNumber() << "\n" << "Check for Kerberos ticket."
                      << endl << exc.what();
                  new TGMsgBox (gClient->GetRoot(), fDiag, "Error",
                                 errmsg.str().c_str(), kMBIconExclamation, kMBOk, 0);
		  gVirtualX->SetCursor (fId, kNone);
		  return ;
	       }
	       if (my_debug) cerr << "  Found kerberos ticket, continue" << endl ;
	    }
         }
         else {
	    cout << "Get lidax channel list" << endl;
            fSource->GetChannelList (clist);
	    if (my_debug) cerr << "UpdateChannels() - Got lidax channel list." << endl ;
         }
         newchn_len = clist.size();
	 if (my_debug) cerr << "UpdateChannels() - clist.size() = " << newchn_len << endl ;
         if (newchn_len > 0) newchn = new ligogui::ChannelEntry [newchn_len];
         for (UInt_t i = 0; i < newchn_len; ++i) {
            newchn[i].SetName (clist[i].Name());
            newchn[i].SetRate (clist[i].Rate());
	    // Fill in UDN as well?
	    // This brings up the question of what to do with duplicate 
	    // channel name/rate channels that have different UDN's.
	    // This would not come up with NDS or NDS2, but is theoretically 
	    // possible with Lidax (Not sure if that particular feature works...)
	    newchn[i].SetUDN (clist[i].UDN()) ;
         }
	 Int_t sort_type = 0;
	 if (newchn_len > 100) sort_type = kChannelTreeSeparateSlow;
	 if (my_debug) cerr << " UpdateChannels() - Call ChannelTree::SortChannelList()" << endl ;
	 ChannelTree::SortChannelList(newchn, newchn_len, sort_type); 
      }

      //--------------------------------  Compare with old list if both offline
      bool same = false;
      if (!fOnlineChannels && !online) {
         same = (newchn_len == fDataChnLen);
         // check channel names---but only if # below 500!
         // OPTIMIZATION with the hope that sets with #>500 will be identical
         // if the first 500 are the same
	 UInt_t nchan_test = newchn_len;
         if (newchn_len > 500) nchan_test = 500;
	 for (UInt_t i = 0; same && (i < nchan_test); ++i) {
	    //--- Don't bother with case comparisons / flip-flops.
	    same = !strcmp(newchn[i].Name(), fDataChnList[i].Name());
	    same = newchn[i].Rate() == fDataChnList[i].Rate() ;
	 }
      }
      // if different update list
      if (my_debug) cerr << "UpdateChannels() - channel list " << (same ? "different, update list" : "same, skip update!!!") << endl ;
      if (!same) {
         Int_t chntype = kChannelTreeShowRate;
         // restore online list
         if (online) {
            if (fMeasChnLen > 100)  chntype |= kChannelTreeSeparateSlow;
            if (fMeasChnLen > 1000) chntype |= kChannelTreeLevel3;
	    if (fMeasChnLen > 5000) chntype |= kChannelTreeLevel6 ; // JCB - for added hierarchy

            for (Int_t chn = 0; chn < kShowMeasChannel; chn++) {
               fMChn[chn]->SetChannelTreeType (chntype);
               fMChn[chn]->SetChannels(fMeasChnList, fMeasChnLen, kFALSE);
               fMChn[chn]->BuildChannelTree();
            }
         }
         // set list for user NDS or Lidax
         else {
            delete [] fDataChnList;
            fDataChnList = newchn;
            newchn = 0;
            fDataChnLen = newchn_len;
            if (fDataChnLen > 100) chntype |= kChannelTreeSeparateSlow;
            if (fDataChnLen > 1000) chntype |= kChannelTreeLevel3;
	    if (fDataChnLen > 5000) chntype |= kChannelTreeLevel6 ; // JCB - for added hierarchy
            for (Int_t chn = 0; chn < kShowMeasChannel; chn++) {
               fMChn[chn]->SetChannelTreeType (chntype);
               fMChn[chn]->SetChannels (fDataChnList, fDataChnLen, kFALSE);
               fMChn[chn]->BuildChannelTree();
            }
         }
      }
      fOnlineChannels = online;
      fUserNDSChannels = userndsx;
      if (userndsx) {
         fUserNDSName = ndsname;
      }
      
      // Go thru the measurement channels, try to select any that are displayed.
      SelectMeasurementChannels() ;

      // Cleanup
      if (newchn) delete [] newchn;
      gVirtualX->SetCursor (fId, kNone);
      if (my_debug) cerr << "DiagTabControl::UpdateChannels() - return" << endl ;
   }


   void DiagTabControl::TransferParameters (Bool_t toGUI)
   {
      if (my_debug) cerr << "DiagTabControl::TransferParameters(" << (toGUI ? "toGUI true)" : "toGUI false)") << endl ;
      if (toGUI) {
         WriteInfo();
         WriteDataSourceParam();
         WriteMeasParam (fMeasSel);
         // Fill in the measurment channels for the current selection of channels
         // in the Measurement Channels frame on the Measurement tab.
         WriteMeasChannel (fMeasRangeSel);
         WriteExcChannel (fExcRangeSel);
         WriteIterParam (fIterSel);
      }
      else {
         ReadInfo();
         ReadDataSourceParam();
         ReadMeasParam (fMeasSel);
         // Read the measurment channels from the fields on the Measurment Channels frame.
         ReadMeasChannel (fMeasRangeSel);
         ReadExcChannel (fExcRangeSel);
         ReadIterParam (fIterSel);
      }
      if (my_debug) cerr << "DiagTabControl::TransferParameters() exit" << endl ;
   }


   void DiagTabControl::SetMeasurement (Int_t id, Bool_t ask, Bool_t newParam)
   {
      if ((fMeasSel != id) && (id >= 0) && 
         (id < fMeasTest->GetList()->GetSize())) {
         // test if running
         if (fDiag->IsRunning()) {
            new TGMsgBox (gClient->GetRoot(), fDiag, "Error", 
                         "Can not change measurement\n"
                         "while test is in progress.", kMBIconStop, kMBOk);
            for (int i = 0; i < 4; i++) {
               fMeasType[i]->SetState 
                  (i == fMeasSel ? kButtonDown : kButtonUp);
            }
            return;
         }
         // test if plots must be cleared
         if (ask && HaveResults()) {
            Int_t ret;
            TString msg = "Changing the test will clear all results.\n"
               "Do you want to continue?";
            new TGMsgBox (gClient->GetRoot(), fDiag, "Clear results", 
                         msg, kMBIconQuestion, kMBYes | kMBNo, &ret);
            if (ret == kMBYes) {
               fDiag->ClearResults ();
            }
            else {
               for (int i = 0; i < 4; i++) {
                  fMeasType[i]->SetState 
                     (i == fMeasSel ? kButtonDown : kButtonUp);
               }
               return;
            }
         }
         // select new measurement
         fMeasSel = id;
         ((FrameOverlay*)fMeasTest)->SelectOverlay (id);
         WriteMeasParam (fMeasSel);
         for (int i = 0; i < 4; i++) {
            fMeasType[i]->SetState (i == fMeasSel? kButtonDown : kButtonUp);
         }
         if (!newParam) {
            ReadExcChannel (fExcRangeSel);
         }
         WriteExcChannel (fExcRangeSel);
         SendMessage (fMsgWindow, MK_MSG ((EWidgetMessageTypes) kC_STATUS, 
                                       (EWidgetMessageTypes) kCS_MEASUREMENT), 
                     fWidgetId, id);
      }
   }


   void DiagTabControl::GetMeasurement ()
   {
      ReadMeasParam (fMeasSel);
   }


   void DiagTabControl::SetIterator (Int_t id)
   {
      if ((fIterSel != id) && (id >= 0) && (id < 3)) {
         if (fDiag->IsRunning()) {
// Change to get message box centered in frame.  JCB
//            new TGMsgBox (gClient->GetRoot(), this, "Error", 
            new TGMsgBox (gClient->GetRoot(), fDiag, "Error", 
                         "Can not change iterator\n"
                         "while test is in progress.", kMBIconStop, kMBOk);
            // for (int i = 0; i < 3; i++) {
               // fIterType[i]->SetState 
                  // (i == fIterSel ? kButtonDown : kButtonUp);
            // }
            return;
         }
         fIterSel = id;
         // ((FrameOverlay*)fIterSel)->SelectOverlay (id);
         WriteIterParam (fIterSel);
         SendMessage (fMsgWindow, MK_MSG ((EWidgetMessageTypes) kC_STATUS, 
                                       (EWidgetMessageTypes) kCS_ITERATOR), 
                     fWidgetId, id);
      }
   }


   void DiagTabControl::GetIterator ()
   {
      ReadIterParam (fIterSel);
   }


   void DiagTabControl::SetMeasurementChannels (Int_t range)
   {
      if ((fMeasRangeSel != range) && (range >= 0) && 
         (range < kMaxMeasChannel / kShowMeasChannel)) {
	 
         fMeasRangeSel = range;
         Int_t chnh = ((kShowMeasChannel + 1) / 2);
         for (int i = 0; i < kShowMeasChannel; i++) {
            char chnnum[10];
            if (i / chnh < 1) {
               sprintf (chnnum, "%3i", fMeasRangeSel * kShowMeasChannel + i);
            }
            else {
               sprintf (chnnum, "%4i", fMeasRangeSel * kShowMeasChannel + i);
            }
            fMChnLabel[i]->SetText (new TGString (chnnum));
         }
	 // Set the range select radio button states.
         for (int i = 0; i <  kMaxMeasChannel / kShowMeasChannel; i++) {
            fMeasNum[i]->SetState(i == fMeasRangeSel ? kButtonDown : kButtonUp);
         }

         WriteMeasChannel (fMeasRangeSel);
      }
   }


   void DiagTabControl::GetMeasurementChannels ()
   {
      // Copy the channel name and channel rate from the displayed measurement
      // channels to the fParam->fMeas structure.  fMeasRangeSel is the currently
      // dislayed measurment channel page index (starting at 0).
      if (my_debug) cerr << "DiagTabControl::GetMeasurementChannels() fMeasRangeSel = " << fMeasRangeSel << endl ;
      ReadMeasChannel (fMeasRangeSel);
   }


   void DiagTabControl::SetExcitationChannels (Int_t range)
   {
      if ((fExcRangeSel != range) && (range >= 0) && 
         (range <  kMaxExcChannel / kShowExcChannel)) {
         fExcRangeSel = range;
         for (int i = 0; i < kShowExcChannel; i++) {
            char buf[100];
            sprintf (buf, "Channel %i", fExcRangeSel * kShowExcChannel + i);
            *fExcFName[i] = TGString (buf);
            // trick to request redraw of background
            fExcF[i]->Resize (fExcF[i]->GetWidth() - 1, fExcF[i]->GetHeight());
            fExcF[i]->Resize (fExcF[i]->GetWidth() + 1, fExcF[i]->GetHeight());
         }
         for (int i = 0; i <  kMaxExcChannel / kShowExcChannel; i++) {
            fExcNum[i]->SetState (i == fExcRangeSel ? kButtonDown : kButtonUp);
         }
         WriteExcChannel (fExcRangeSel);
      }
   }


   void DiagTabControl::GetExcitationChannels ()
   {
      ReadExcChannel (fExcRangeSel);
   }


#if ROOT_VERSION_CODE < ROOT_VERSION(5,14,0) 
   void DiagTabControl::ChangeTab (Int_t tabIndex)
#else
     void DiagTabControl::ChangeTab (Int_t tabIndex, Bool_t emit)
#endif
   {
      if ((fCurrent == 0) && (tabIndex != 0)) {
         UpdateChannels();
	 // Set the start time if the data source selection is NDS2
	 // and the start time isn't within the bounds of the epoch.
	 {
	    taisec_t epoch_start = fNDS2EpochStartGPS->GetIntNumber() ;
	    taisec_t epoch_end = fNDS2EpochStopGPS->GetIntNumber() ;
	    taisec_t start_time = fTimeGPS->GetIntNumber() ;
	    if (fDataInput[3]->GetState() == kButtonDown) 
	    {
	       // Since this is NDS2 input, select GPS start time rather
	       // than Now, since Now implies current time.  Do this even
	       // if the time now is within the current epoch.
	       Int_t id = 3 ; 
	       for (int i = 0; i < 5; i++) {
		  fTimeSel[i]->SetState (i == id ? kButtonDown : kButtonUp);
	       }
	       // Check to see if the start time is within the epoch bounds.
	       // If it isn't, select a time which is within the bounds.
	       if (start_time < epoch_start || start_time > epoch_end) 
	       {
		  // Add 10 seconds to the start time, some analysis may choose
		  // a start time slightly before the fTimeGPS value.
		  fTimeGPS->SetIntNumber(epoch_start + 10) ;
		  fTimeGPSN->SetIntNumber (0) ;
		  utc_t utc;
		  TAItoUTC (epoch_start, &utc);
		  utc.tm_year += 1900;
		  fTimeDate->SetDate (utc.tm_year, utc.tm_mon+1, utc.tm_mday);
		  fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
	       }
	    }
	 }
      }
#if ROOT_VERSION_CODE < ROOT_VERSION(5,14,0) 
      TGTab::ChangeTab (tabIndex);
#else
      TGTab::ChangeTab (tabIndex, emit);
#endif

   }


#if ROOT_VERSION_CODE < ROOT_VERSION(5,14,0) 
   Bool_t DiagTabControl::SetTab(Int_t tabIndex)
#else
   Bool_t DiagTabControl::SetTab(Int_t tabIndex, Bool_t emit)
#endif
   {
   // ONLY here because ChangeTab is not virtual
   // Brings the composite frame with the index tabIndex to the
   // front and generate the following event if the front tab has
   // changed:
   // kC_COMMAND, kCM_TAB, tab id, 0.
   // Returns kFALSE if tabIndex is a not valid index
   
   // check if tabIndex is a valid index
      if (tabIndex < 0)
         return kFALSE;
   
   // count the tabs
      TIter next(fList);
      Int_t count = 0;
      while (next())
         count++;
   
      count = count / 2 - 1;
      if (tabIndex > count)
         return kFALSE;
   
   // change tab and generate event
#if ROOT_VERSION_CODE < ROOT_VERSION(5,14,0) 
      ChangeTab(tabIndex);
#else
      ChangeTab(tabIndex, emit);
#endif
   
      return kTRUE;
   }


   Bool_t DiagTabControl::HandleButton(Event_t *event)
   {
   // ONLY here because ChangeTab is not virtual
   // Handle button event in the tab widget. Basically we only handle
   // button events in the small tabs.

      if (event->fType == kButtonPress) {
	TGTab* main = (TGTab*)fParent;
	if (main) {
	  TGFrameElement *el;
	  TIter next(main->GetList());
      
	  next();   // skip first container
      
	  Int_t i = 0;
	  Int_t c = main->GetCurrent();
	  while ((el = (TGFrameElement *) next())) {
            if (el->fFrame->GetId() == (Window_t)event->fWindow)
	      c = i;
            next(); i++;
	  }
      
	  // change tab and generate event
	  main->SetTab(c);
	}
      }
      return kTRUE;
   }


   Bool_t DiagTabControl::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                  case kCM_RADIOBUTTON:
                  case kCM_CHECKBUTTON:
                     {
                        return ProcessButton (parm1, parm2);
                     }
                  case kCM_COMBOBOX: 
                     {
                        return ProcessCombobox (parm1, parm2);
                     }
		  default:
		     break ;
               }
               break;
            }
         case kC_TEXTENTRY:
            {
               switch (GET_SUBMSG (msg)) {
                  case kTE_TEXTUPDATED:
                     {
                        return ProcessTextentry (parm1, parm2);
                     }
                  case kTE_TEXTCHANGED:
                     {
                        return ProcessTextchanged (parm1, parm2);
                        break;
                     }
		  default:
		     break ;
               }
               break;
            }
	 default:
	    break ;
      }
      return kTRUE;
   }

   void DiagTabControl::NDS2ConnectionTest()
   {
     // Test for authorization - need a kerberos ticket.
     try {
       NDS::connection nds2(fNDS2Name->GetText(), fNDS2Port->GetIntNumber()) ;
       stringstream epochList ;
       bool first = true;
       auto epochs = nds2.get_epochs();
       for(auto epoch:epochs)
       {
         if(!first)
         {
           epochList << " ";
         }
         first = false;

         // make a string like so:
         // <name>=start-stop
         epochList << epoch.name << " " << epoch.gps_start << "-" << epoch.gps_stop;

       }
       auto epoch_str = epochList.str();
       ParseEpochListString(&epoch_str) ;
     }
     catch(std::runtime_error exc)
     {
       if (my_debug) cerr << " nds2.open(" << fNDS2Name->GetText() << ", " <<  fNDS2Port->GetIntNumber() << ")"<< endl ;
       // Create an error popup that warns the user this won't work.
       ostringstream errmsg ;
       errmsg << " Cannot connect to " << fNDS2Name->GetText() << ":" << fNDS2Port->GetIntNumber() << "\n" << "Check for Kerberos ticket." ;
       new TGMsgBox (gClient->GetRoot(), fDiag, "Error",
                    errmsg.str().c_str(), kMBIconExclamation, kMBOk, 0);
     }
   }

   Bool_t DiagTabControl::ProcessButton (Long_t parm1, Long_t parm2) 
   {
      int excnum = 0;
      if ((parm1 >= kExcChnActive) && 
         (parm1 < kExcChnFilterWiz + kShowExcChannel)) 
      {
         excnum = (parm1 - kExcChnActive) % kShowExcChannel;
         parm1 = parm1 - excnum;
      }
      switch (parm1) {
         // Measurement selection radio buttons
         case kMeasSel:
         case kMeasSel + 1:
         case kMeasSel + 2:
         case kMeasSel + 3:
            {
               Int_t id = parm1 - kMeasSel;
               GetMeasurement ();
               SetMeasurement (id, kTRUE);
               break;
            }
         // Data source selection radio buttons
         case kDataInput:
         case kDataInput + 1:
         case kDataInput + 2:
            {
	       // Set the state of the other buttons.
               Int_t id = parm1 - kDataInput;
               for (int i = 0; i < 4; i++) {
                  fDataInput[i]->SetState (i == id ? kButtonDown : kButtonUp);
               }
               break;
            }
         // Data source selection radio buttons
	 // NDS2 button needs to check for authorization.
         case kDataInput + 3:
	    {
	       // Set the state of the other buttons.
               Int_t id = parm1 - kDataInput;
               for (int i = 0; i < 4; i++) {
                  fDataInput[i]->SetState (i == id ? kButtonDown : kButtonUp);
               }
	       NDS2ConnectionTest();
	    }
	    break ;
         case kDataCacheClear:
            {
               fDiag->ClearCache();
               break;
            }
#if 0
// Move this to a default action since we don't know ahead of time
// how many there are.  JCB
         // Measurement channel number radio buttons
         case kMeasNum:
         case kMeasNum + 1:
         case kMeasNum + 2:
         case kMeasNum + 3:
         case kMeasNum + 4:
#else
	 default:
	    if (parm1 >= kMeasNum && parm1 < kMeasNum + (kMaxMeasChannel / kShowMeasChannel))
#endif
            {
	       // The id is the newly pressed radio button index.
               Int_t id = parm1 - kMeasNum;
	       // GetMeasurementChannels() calls ReadMeasChannel(fMeasRangeSel), where
	       // fMeasRangeSel is the old value.
               GetMeasurementChannels ();
	       // SetMeasurementChannels(id) calls WriteMeadChannel(fMeasRangeSel)
	       // after setting fMeasRangeSel to the new value, id in this case.
               SetMeasurementChannels (id);
            }
	    break;

         // FFT average type radio buttons
         case kFFTAverageType:
         case kFFTAverageType + 1:
         case kFFTAverageType + 2:
            {
               Int_t id = parm1 - kFFTAverageType;
               for (int i = 0; i < 3; i++) {
                  fFFTAverageType[i]->SetState (
                                       i == id ? kButtonDown : kButtonUp);
               }
               fFFTAverages->SetState (id != 2);
               break;
            }
         // Swept sine sweep direction radio buttons
         case kSSSweepDir:
         case kSSSweepDir + 1:
            {
               Int_t id = parm1 - kSSSweepDir;
               for (int i = 0; i < 2; i++) {
                  fSSSweepDir[i]->SetState (
                                       i == id ? kButtonDown : kButtonUp);
               }
               break;
            }
         // Swept sine sweep type radio buttons
         case kSSSweepType:
         case kSSSweepType + 1:
            {
               Int_t id = parm1 - kSSSweepType;
               for (int i = 0; i < 2; i++) {
                  fSSSweepType[i]->SetState (
                                       i == id ? kButtonDown : kButtonUp);
               }
               break;
            }
         // Swept sine sweep point editor
         case kSSSweepEdit:
            {
               if (!fSSSweepEditorDone && fSSSweepEditor) {
                  fSSSweepEditor->RaiseWindow();
                  break;
               }
               // Fill text buffer
               if (fSSSweepEditText) {
                  delete fSSSweepEditText;
               }
               fSSSweepEditText = new TGText;
               fSSSweepEditText->InsLine (0, 
                                    "#Format: frequency (Hz)  amplitude (optional)");
               BasicDataDescriptor* dd = fParam->fMeas.fSweepPoints.get();
               if (dd) {
                  for (int i = 0; i < dd->GetN(); ++i) {
                     char buf[1024];
                     sprintf (buf, "%12g %12g", dd->GetX()[i], dd->GetY()[i]);
                     fSSSweepEditText->InsLine (i + 1, buf);
                  }
               }
               // Call dialog box
               fSSSweepEditorRet = kFALSE;
               fSSSweepEditorDone = kFALSE;
               fSSSweepEditor = new ligogui::TLGTextEditor (gClient->GetRoot(), 
                                    this, "Sweep Point Editor", 66, 44, 
                                    *fSSSweepEditText, fSSSweepEditorRet, kTRUE, 
                                    kSSSweepEditor, &fSSSweepEditorDone);
               fSSSweepEditor->Associate (this);
               break;
            }
         // Sine response average type radio buttons
         case kSRAverageType:
         case kSRAverageType + 1:
         case kSRAverageType + 2:
            {
               Int_t id = parm1 - kSRAverageType;
               for (int i = 0; i < 3; i++) {
                  fSRAverageType[i]->SetState (
                                       i == id ? kButtonDown : kButtonUp);
               }
               fSRAverages->SetState (id != 2);
               break;
            }
         // Triggered time response average type radio buttons
         case kTSAverageType:
         case kTSAverageType + 1:
         case kTSAverageType + 2:
            {
               Int_t id = parm1 - kTSAverageType;
               for (int i = 0; i < 3; i++) {
                  fTSAverageType[i]->SetState (
                                       i == id ? kButtonDown : kButtonUp);
               }
               fTSAverages->SetState (id != 2);
               break;
            }
         // Triggered time response foton button
         case kTSFoton:
            {
               // get function pointer
               wizfunc_t wizf = 0;
               wizf = (wizfunc_t)getFunc();
               if (!wizf) {
                  cerr << "Unable to get foton entry point" << endl;
                  return kFALSE;
               }
               // call function
               string filterspec = fTSFilterSpec->GetText();
               if (!wizf ("DTT", filterspec)) {
                  return kFALSE;
               }
               fTSFilterSpec->SetText (filterspec.c_str());
               break;
            }
         // Start time radio buttons
         case kTimeStart:
         case kTimeStart + 1:
         case kTimeStart + 2:
         case kTimeStart + 3:
         case kTimeStart + 4:
            {
               Int_t id = parm1 - kTimeStart;
               for (int i = 0; i < 5; i++) {
                  fTimeSel[i]->SetState (i == id ? kButtonDown : kButtonUp);
               }
               break;
            }
         // Time now
         case kTimeNow:
            {
               // Initialize start time
               taisec_t now = TAInow() / _ONESEC;
               fTimeGPS->SetIntNumber (now);
               fTimeGPSN->SetIntNumber (0);
               utc_t utc;
               TAItoUTC (now, &utc);
               utc.tm_year += 1900;
               fTimeDate->SetDate (utc.tm_year, utc.tm_mon+1, utc.tm_mday);
               fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
               ProcessButton (kTimeStart + 4, 0);
               break;
            }
         // Time lookup
         case kTimeLookup:
            {
               // get source type and user nds address
               bool online = (fDataInput[0]->GetState() == kButtonDown);
               bool usernds = (fDataInput[1]->GetState() == kButtonDown);
               bool usernds2 = (fDataInput[3]->GetState() == kButtonDown);
               // get a data server
               dfm::dataserver* ds = 0;
               const dfm::UDNList* ulist = 0;
               if (online) {
                  unsigned long start = 0;
                  unsigned long duration = 0;
                  fDiag->GetTimes (start, duration);
                  ds = new dfm::dataserver (dfm::st_File, "online");
                  dfm::UDNInfo info;
                  dfm::UDNInfo::dataseglist dseg;
                  dseg [Time(start, 0)] = Interval (duration, 0);
                  info.setDataSegs (dseg);
                  dfm::UDN u ("online");
                  ds->insert (u, info);
                  dfm::UDNList* ul = new dfm::UDNList;
                  (*ul)[u] = info;
                  ulist = ul;
               }
               else if (usernds) {
		  ostringstream ndsaddr;
		  ndsaddr << fNDSName->GetText();
		  int port = fNDSPort->GetIntNumber();
		  if (port > 0) {
		     ndsaddr << ":" << port;
		  }
		  dfm::dataservername ndsname (dfm::st_NDS, ndsaddr.str());
                  ds = new dfm::dataserver (dfm::st_NDS, ndsname.getAddr());
                  string u = string ("nds://") + ndsname.getAddr() + "/frames";
                  dfm::UDN udn (u.c_str());
                  dfm::UDNInfo* uinfo = ds->insert (udn);
                  if (uinfo) {
                     ds->lookupUDN (udn);
                     dfm::UDNList* ul = new dfm::UDNList;
                     (*ul)[udn] = *uinfo;
                     ulist = ul;
                  }
                  else {
                     delete ds;
                     ds = 0;
                  }
               }
               else if (usernds2) {
		  ostringstream ndsaddr;
		  ndsaddr << fNDS2Name->GetText();
		  int port = fNDS2Port->GetIntNumber();
		  if (port > 0) {
		     ndsaddr << ":" << port;
		  }
		  // Add in the epoch times to make this unique for the epoch
		  unsigned long epoch_start = fNDS2EpochStartGPS->GetIntNumber() ;
		  unsigned long epoch_stop = fNDS2EpochStopGPS->GetIntNumber() ;
		  ostringstream es ;
		  es << "?epoch_start=" << epoch_start ;
		  ostringstream ee ;
		  ee << "&epoch_end=" << epoch_stop ;
		  dfm::dataservername ndsname (dfm::st_SENDS, ndsaddr.str());
                  ds = new dfm::dataserver (dfm::st_SENDS, ndsname.getAddr());
                  string u = string ("nds2://") + ndsname.getAddr() + "/frames" + es.str() + ee.str() ;
                  dfm::UDN udn (u.c_str());
                  dfm::UDNInfo* uinfo = ds->insert (udn);
                  if (uinfo) {
                     ds->lookupUDN (udn);
                     dfm::UDNList* ul = new dfm::UDNList;
                     (*ul)[udn] = *uinfo;
		     // Create a dataseglist for the UDN using the epoch times.
		     dfm::UDNInfo::dataseglist dseg ;
		     dseg [Time(epoch_start, 0)] = Interval (epoch_stop-epoch_start, 0) ;
		     uinfo->setDataSegs(dseg) ;
                     ulist = ul;
                  }
                  else {
                     delete ds;
                     ds = 0;
                  }
               }
               else {
                  const dfm::selserverentry* entry = 
                     fSource->GetSel().selectedEntry();
                  if (entry) {
                     ulist = &entry->getUDN();
                     ds = fParam->fData.fDacc.get (
                                          (const char*)entry->getName());
                  }
               }
               if (!ds || !ulist) {
// Change to get message box centered in frame.  JCB
//                  new TGMsgBox (gClient->GetRoot(), this, "Error", 
                  new TGMsgBox (gClient->GetRoot(), fDiag, "Error", 
                               "Unable to determine time segments",
                               kMBIconStop, kMBOk);
                  break;
               }
               Bool_t ret;
               Time start (fTimeGPS->GetIntNumber(), 
                          fTimeGPSN->GetIntNumber());
               Interval duration (0, 0);
               new dfm::TLGDfmTimeSelDlg (gClient->GetRoot(), GetParent(), 
                                    *ds, *ulist, start, duration, ret);
               if (online || usernds || usernds2) {
                  delete ds;
                  delete ulist;
               }
               if (ret) {
                  fTimeGPS->SetIntNumber (start.getS());
                  fTimeGPSN->SetIntNumber (start.getN());
                  utc_t utc;
                  TAItoUTC (start.getS(), &utc);
                  utc.tm_year += 1900;
                  fTimeDate->SetDate (utc.tm_year, utc.tm_mon+1, utc.tm_mday);
                  fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
                  ProcessButton (kTimeStart + (start.getN() ? 3 : 4), 0);
               }
               break;
            }
         // Excitation channel number radio buttons
         case kExcNum:
         case kExcNum + 1:
         case kExcNum + 2:
         case kExcNum + 3:
         case kExcNum + 4:
            {
               Int_t id = parm1 - kExcNum;
               GetExcitationChannels ();
               SetExcitationChannels (id);
               break;
            }
         // Excitation readback channel type radio buttons
         case kExcChnRBType:
         case kExcChnRBType + kShowExcChannel:
         case kExcChnRBType + 2 * kShowExcChannel:
            {
               Int_t id = parm1 - kExcChnRBType;
               for (int i = 0; i <  3; i++) {
                  fExcChnRBType[excnum][i]->SetState 
                     (i * kShowExcChannel == id ? kButtonDown : kButtonUp);
               }
               break;
            }
         // Excitation waveform select button
         case kExcChnWaveformFileSel:
            {
               TGFileInfo       info;
               info.fFilename = 0;
               info.fIniDir = 0;
            #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
               info.fFileTypes = const_cast<const char**>(gSweepFileTypes);
            #else
               info.fFileTypes = const_cast<char**>(gSweepFileTypes);
            #endif
	    #if 1
	       info.fFileTypeIdx = 2 ; // Point to all files.
	       {
		  new TLGFileDialog((const TGWindow *)this, &info, kFDOpen) ;
	       }
	       if (info.fFilename)
	    #else
               if (TLGFileDialog (this, info, kFDOpen, 0))
	    #endif
	       {
                  fExcChnWaveformFile[excnum]->SetText (info.fFilename);
               }
            #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
               delete [] info.fFilename;
            #endif
               break;
            }
         // Excitation filter foton button
         case kExcChnFilterWiz:
            {
               // get function pointer
               wizfunc_t wizf = 0;
               wizf = (wizfunc_t)getFunc();
               if (!wizf) {
                  cerr << "Unable to get foton entry point" << endl;
                  return kFALSE;
               }
               // call function
               string filterspec = fExcChnFilterCmd[excnum]->GetText();
               if (!wizf ("DTT", filterspec)) {
                  return kFALSE;
               }
               fExcChnFilterCmd[excnum]->SetText (filterspec.c_str());
               break;
            }
      }
      return kTRUE;
   }


   Bool_t DiagTabControl::ProcessCombobox (Long_t parm1, Long_t parm2) 
   {
      switch (parm1) {
         case kNDSName:
            {
               string addr = fNDSName->GetText();
               string::size_type pos = addr.find (':');
               int port = 0;
               if (pos != string::npos) {
                  port = atoi (addr.c_str() + pos + 1);
               }
               fNDSName->SetText (addr.substr(0, pos).c_str());
               fNDSPort->SetIntNumber (port);
               break;
            }
         case kNDS2Name:
            {
               string addr = fNDS2Name->GetText();
               string::size_type pos = addr.find (':');
               int port = 0;
               if (pos != string::npos) {
                  port = atoi (addr.c_str() + pos + 1);
               }
               fNDS2Name->SetText (addr.substr(0, pos).c_str());
               fNDS2Port->SetIntNumber (port);
            }
	    // If the NDS2 server name changes, get the list
	    // of epochs from the server to populate the 
	    // epoch name menu.
	    {
               NDS2ConnectionTest();
	    }
	    break;
	 case kNDS2EpochName:
	    {
	       Int_t	id = fNDS2EpochName->GetSelected() ;
	       if (id > 0)
	       {
		  NDS2Epoch_t *tmp = fEpochList[id-1] ;
		  // Put the time values in the epoch start and stop fields.
		  // set UTC start time
		  utc_t	utc;
		  fNDS2EpochStartGPS->SetIntNumber(tmp->fEpoch[0]) ;
		  TAItoUTC (tmp->fEpoch[0], &utc);
		  utc.tm_year += 1900;
		  utc.tm_mon++;
		  fNDS2EpochStartDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
		  fNDS2EpochStartTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);

		  fNDS2EpochStopGPS->SetIntNumber(tmp->fEpoch[1]) ;
		  TAItoUTC (tmp->fEpoch[1], &utc);
		  utc.tm_year += 1900;
		  utc.tm_mon++;
		  fNDS2EpochStopDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
		  fNDS2EpochStopTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);

		  fNDS2EpochStartGPS->SetState(kFALSE) ;
		  fNDS2EpochStartDate->SetState(kFALSE) ;
		  fNDS2EpochStartTime->SetState(kFALSE) ;
		  fNDS2EpochStopGPS->SetState(kFALSE) ;
		  fNDS2EpochStopDate->SetState(kFALSE) ;
		  fNDS2EpochStopTime->SetState(kFALSE) ;
	       }
	       else 
	       {
		  fNDS2EpochStartGPS->SetState(kTRUE) ;
		  fNDS2EpochStartDate->SetState(kTRUE) ;
		  fNDS2EpochStartTime->SetState(kTRUE) ;
		  fNDS2EpochStopGPS->SetState(kTRUE) ;
		  fNDS2EpochStopDate->SetState(kTRUE) ;
		  fNDS2EpochStopTime->SetState(kTRUE) ;
	       }
	    }
	    break ;
      }
      return kTRUE;
   }

   void DiagTabControl::ParseEpochListString(string *list)
   {
      if (!list->empty())
      {
	 size_t pos_space = 0 ; 
	 size_t p = 0 ;
	 int id = 1 ;

	 // Clear the old entries in the menu, and get rid of epochs
	 // in the vector of epochs.
	 while (!fEpochList.empty())
	 {
	    NDS2Epoch_t *tmp = fEpochList.back() ;

	    delete tmp ;
	    fEpochList.pop_back() ;
	 }
	 fNDS2EpochName->RemoveAll() ;

	 // Add a user-supplied entry at the start of the epoch menu.
	 fNDS2EpochName->AddEntry("User specified", 0) ;
	 fNDS2EpochName->Select(0) ;
	 fNDS2EpochStartGPS->SetState(kTRUE) ;
	 fNDS2EpochStartDate->SetState(kTRUE) ;
	 fNDS2EpochStartTime->SetState(kTRUE) ;
	 fNDS2EpochStopGPS->SetState(kTRUE) ;
	 fNDS2EpochStopDate->SetState(kTRUE) ;
	 fNDS2EpochStopTime->SetState(kTRUE) ;

	 // Add the rest from the string.
	 while (pos_space != string::npos)
	 {
	    string epoch_str ;
	    pos_space = list->find(' ', pos_space) ;
	    // pos_space may point to a space, or it might be string::npos
	    // Either way, use it to create a substring that will contain
	    // a <name>=<start>-<stop> substring.
	    if (pos_space == string::npos)
	       epoch_str = string(*list, p) ;
	    else
	       epoch_str = string(*list, p, pos_space-p) ;  

	    size_t pos_equal, pos_dash ;
	    pos_equal = epoch_str.find('=') ;
	    pos_dash = epoch_str.find('-') ;
	    if (pos_equal != string::npos && pos_dash != string::npos)
	    {
	       // There should be a name, start, and stop.
	       string name(epoch_str, 0, pos_equal) ;
	       string start(epoch_str, pos_equal+1, pos_dash-pos_equal-1) ;
	       string stop(epoch_str, pos_dash+1) ;

	       // Use the values obtained to populate the epoch menu and
	       // vector of epochs.
	       NDS2Epoch_t *tmp = new NDS2Epoch_t ;
	       tmp->fEpochName = TString(name.c_str()) ;
	       tmp->fEpoch[0] = stol(start) ;
	       tmp->fEpoch[1] = stol(stop) ;
	       fEpochList.push_back(tmp) ;
	       // The (id-1) will also be the index into the vector of epochs.
	       fNDS2EpochName->AddEntry(tmp->fEpochName, id++) ;
	    }
	    if (pos_space != string::npos)
	    {
	       pos_space++ ;
	       p = pos_space ;
	    }
	 }
      }
   }

   Bool_t DiagTabControl::ProcessTextentry (Long_t parm1, Long_t parm2) 
   {
      switch (parm1) {
         // Future time has changed
         case kTimeFuture:
            {
               fTimePast->SetNumber (0);
               ProcessButton (kTimeStart + 1, 0);
               break;
            }
         // Past time has changed
         case kTimePast:
            {
               fTimeFuture->SetNumber (0);
               ProcessButton (kTimeStart + 2, 0); 
               break;
            }
         // GPS seconds have changed
         case kTimeGPS:
            {
               // set UTC start time
               utc_t	utc;
               TAItoUTC (fTimeGPS->GetIntNumber(), &utc);
               utc.tm_year += 1900;
               utc.tm_mon++;
               fTimeDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
               fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
               ProcessButton (kTimeStart + 3, 0);
               break;
            }
         // GPS nano-seconds have changed
         case kTimeGPSN:
            {
               ProcessButton (kTimeStart + 3, 0); 
               break;
            }
         // UTC time has changed
         case kTimeDate:
         case kTimeTime:
            {
               // set GPS seconds
               utc_t	utc;
               fTimeDate->GetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
               utc.tm_year -= 1900;
               utc.tm_mon--;
               fTimeTime->GetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
               fTimeGPS->SetIntNumber (UTCtoTAI (&utc));
               ProcessButton (kTimeStart + 4, 0);
               break;
            }
         case kNDS2EpochStartGPS:
            {
               // set UTC start time
               utc_t	utc;
               TAItoUTC (fNDS2EpochStartGPS->GetIntNumber(), &utc);
               utc.tm_year += 1900;
               utc.tm_mon++;
               fNDS2EpochStartDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
               fNDS2EpochStartTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
               break;
            }
         case kNDS2EpochStopGPS:
            {
               // set UTC stop time
               utc_t	utc;
               TAItoUTC (fNDS2EpochStopGPS->GetIntNumber(), &utc);
               utc.tm_year += 1900;
               utc.tm_mon++;
               fNDS2EpochStopDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
               fNDS2EpochStopTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
               break;
            }
         // UTC time has changed
         case kNDS2EpochStartDate:
         case kNDS2EpochStartTime:
            {
               // set GPS seconds
               utc_t	utc;
               fNDS2EpochStartDate->GetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
               utc.tm_year -= 1900;
               utc.tm_mon--;
               fNDS2EpochStartTime->GetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
               fNDS2EpochStartGPS->SetIntNumber (UTCtoTAI (&utc));
               break;
            }
         // UTC time has changed
         case kNDS2EpochStopDate:
         case kNDS2EpochStopTime:
            {
               // set GPS seconds
               utc_t	utc;
               fNDS2EpochStopDate->GetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
               utc.tm_year -= 1900;
               utc.tm_mon--;
               fNDS2EpochStopTime->GetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
               fNDS2EpochStopGPS->SetIntNumber (UTCtoTAI (&utc));
               break;
            }
         // Update swept sine points
         case kSSSweepEditor:
            {
               cerr << "SWEPT SINE SWEEP POINT UPDATE" << endl;
               int sweeptype = fSSSweepFormat->GetSelected();
               // Read values
               int max = fSSSweepEditText->RowCount();
               float* x = new float [max];
               float* y = new float [max];
               int len = 0;
               for (int i = 0; i < max; ++i) {
                  char* p = fSSSweepEditText->GetLine (TGLongPosition (0, i), 
                                       fSSSweepEditText->ColCount());
                  if (p && (strchr (p, '#') == 0)) {
                     x[len] = 0;
                     y[len] = 0;
                     char* pp = p;
                     while (isspace (*pp)) ++pp;
                     bool err = !*pp; // must be non-empty
                     if (sweeptype == 2) {
                        x[len] = strtod (pp, &pp);
                        //sscanf (p, "%g", x+len);
                     }
                     else {
                        x[len] = strtod (pp, &pp);
                        while (isspace (*pp)) ++pp;
                        if (!*pp) err = true; // must be non-empty
                        y[len] = strtod (pp, &pp);
                        //sscanf (p, "%g%g", x+len, y+len);
                     }
                     while (isspace (*pp)) ++pp;
                     if (*pp) err = true; // must be empty
                     if (!err) ++len;
                  }
                  delete [] p;
               }
               BasicDataDescriptor* dd = 0;
               if (len > 0) {
                  dd = new DataCopy (x, y, len);
               }
               fParam->fMeas.fSweepPoints.reset (dd);
               delete [] x;
               delete [] y;
               break;
            }
	 default:
	    break ;
      }
      return kTRUE;
   }




   Bool_t DiagTabControl::ProcessTextchanged (Long_t parm1, Long_t parm2) 
   {
      //catch BW and timespan entries to do an autoconversion from one to the other.
      switch(parm1)
      {
      case kFFTBW:
        UpdateTimeSpanFromBW();
        break;
      case kFFTTimeSpan:
        UpdateBWFromTimeSpan();
        break;
      }
      return kTRUE;
   }


   Bool_t DiagTabControl::UpdateBWFromTimeSpan() {
     double timespan = fFFTTimeSpan->GetNumber();
     if(timespan == last_auto_timespan)
     {
       last_auto_timespan = -1;
       return true;
     }
     if(timespan > 0) {
       double ts_l2 = log2(timespan);
       int pow2 = (int)ts_l2;

       if (ts_l2 > pow2) {
         ++pow2;
       }

       last_auto_bw = pow(2, -pow2);

       fFFTBW->SetNumber(last_auto_bw);
       //bw_timespan_relaxed_update = true;

       printf("Done updating BW\n");
       return true;
     }
     return false;
   }

  Bool_t DiagTabControl::UpdateTimeSpanFromBW() {
    double bw = fFFTBW->GetNumber();
    if(bw == last_auto_bw)
    {
      last_auto_bw = -1;
      return true;
    }
    if(bw > 0)
    {
      double ts_l2 = -log2(bw);

      double ts_l2_whole = (double)(int)(ts_l2);
      double ts_l2_frac = ts_l2 - ts_l2_whole;

      if(ts_l2_frac >= 0.5)
      {
        ts_l2_whole += 1.0;
      }
      else if(ts_l2_frac <= -0.5)
      {
        ts_l2_whole -= 1.0;
      }

      last_auto_timespan = pow(2, ts_l2_whole);

      fFFTTimeSpan->SetNumber(last_auto_timespan);

      return true;
    }
    return false;
  }

}
