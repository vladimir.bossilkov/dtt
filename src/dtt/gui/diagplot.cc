/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  diagplot						     */
/*                                                                           */
/* Module Description:  main window of diagnostics tests		     */
/*                                                                           */
/*---------------------------------------------------------------------------*/

//#include "TLGCore.h"
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <iostream>
#include <TString.h>
#include "diagplot.hh"
#include "diagnames.h"
#include "gdsmain.h"
#include "gdsstring.h"
#include "cmdapi.hh"
#include "PlotSet.hh"
#include "TLGCalDlg.hh"
#include "TLGMath.hh"
#include "Xsil.hh"
#include "XsilStd.hh"
#include "Calibration.hh"

static const int my_debug = 0 ;

namespace ligogui {
   using namespace std;
   using namespace diag;
   using namespace xml;


   DiagDataDescriptor::DiagDataDescriptor (basic_commandline* cmd, 
                     const TString& name, Int_t length, 
                     Int_t offsetX, Int_t offsetY, bool xdouble,
                     bool cmplx, bool cpyOnNeed,
                     bool eliminateZeros)
   : BasicDataDescriptor (cmplx), fCmdLine (cmd), fName (name), 
   fXYData (true), fDX (1.),fOffsetX (offsetX), fOffsetY (offsetY), 
   fLength (length), fTableData (false), fTableWidth (0), 
   fTableCol (0), fNoZero (eliminateZeros), 
   fN (length), fX (0), fY (0), fTable (0), fXDouble(xdouble)
   {
      if (!cpyOnNeed) GetData();
   }


   DiagDataDescriptor::DiagDataDescriptor (basic_commandline* cmd, 
                     const TString& name, Int_t length, 
                     Double_t x0, Double_t dx, 
                     Int_t offsetY, bool cmplx, bool cpyOnNeed,
                     bool eliminateZeros)
   : BasicDataDescriptor (cmplx), fCmdLine (cmd), fName (name), 
   fXYData (false), fDX (dx), fOffsetY (offsetY), fLength (length), 
   fTableData (false), fTableWidth (0), fTableCol (0), 
   fNoZero (eliminateZeros), fN (length), fX (0), fY (0), fTable (0), fXDouble(false)
   {
     printf("DiagDataDescriptor: x0=%0.10le dx=%0.10le\n", x0, dx);
      fX = new Float_t[fLength];
      if (fX != 0) {
         for (int i = 0; i < fLength; i++) {
            fX[i] = x0 + (Double_t) i * dx;
         }
      }
      if (!cpyOnNeed) GetData();
   }


   DiagDataDescriptor::DiagDataDescriptor (basic_commandline* cmd, 
                     const TString& name, bool cmplx, 
                     Int_t length, Int_t tablewidth, Int_t tablecol, 
                     bool cpyOnNeed)
   : BasicDataDescriptor (cmplx), fCmdLine (cmd), fName (name), 
   fXYData (true), fDX (1.), fOffsetY (0), fLength (length), 
   fTableData (true), fTableWidth (tablewidth), fTableCol (tablecol),
   fNoZero (false), fN (length), fX (0), fY (0), fTable (0), fXDouble(false)
   {
      if (!cpyOnNeed) GetData();
   }


   DiagDataDescriptor::DiagDataDescriptor (basic_commandline* cmd, 
                     const TString& name, bool cmplx, 
                     Int_t length, Int_t tablewidth, Int_t tablecol, 
                     Double_t x0, Double_t dx, bool cpyOnNeed)
   : BasicDataDescriptor (cmplx), fCmdLine (cmd), fName (name), 
   fXYData (false), fDX (dx), fOffsetY (0), fLength (length), 
   fTableData (true), fTableWidth (tablewidth), fTableCol (tablecol),
   fNoZero (false), fN (length), fX (0), fY (0), fTable (0), fXDouble(false)
   {
      fX = new Float_t[fLength];
      if (fX != 0) {
         for (int i = 0; i < fLength; i++) {
            fX[i] = x0 + (Double_t) i * dx;
         }
      }
      if (!cpyOnNeed) GetData();
   }


   DiagDataDescriptor::~DiagDataDescriptor () 
   {
      if (fTableData) {
         delete [] fX;
         delete [] fY;
      }
      else if (fXYData) {
         free (fX);
         free (fY);
      }
      else {
         delete [] fX;
         free (fY); 
      }
      free (fTable);
   }


   bool DiagDataDescriptor::EraseData()
   {
      if (fTableData) {
         if (fXYData) {
            delete [] fX; fX = 0;
         }
         delete [] fY; fY = 0;
      }
      else if (fXYData) {
         free (fX); fX = 0;
         free (fY); fY = 0;
      }
      else {
         free (fY); fY = 0;
      }
      free (fTable); fTable = 0;
      return true;
   }


   bool DiagDataDescriptor::GetData() const
   {
      // get table data
      fN = fLength;
      if (fTableData) {
         if (fXYData) {
            delete [] fX; fX = 0;
         }
         delete [] fY; fY = 0;
         free (fTable); fTable = 0;
         // get table
         Int_t width = fXYData ? (fTableWidth + 1) : fTableWidth;
         fCmdLine->getData (string(fName.Data()), fTable, 
                           fLength * width, (IsComplex() ? 1 : 0));
         int d = IsComplex() ? 2 : 1;
         if (fTable != 0) {
            if (fXYData) {
               fX = new (nothrow) float [fLength];
            }
            fY = new (nothrow) float [d * fLength];
         }
         // get X
         if (fXYData && (fX != 0)) {
            for (int j = 0; j < fLength; j++) {
               fX[j] = fTable[d * j * width];
            }
         }
         // get Y
         if (fY != 0) {
            Int_t ofs = fXYData ? 1 : 0;
            for (int j = 0; j < fLength; j++) {
               fY[d * j] = fTable[d * (j * width + fTableCol + ofs)];
               if (IsComplex()) {
                  fY[d * j + 1] = 
                     fTable[d * (j * width + fTableCol + ofs) + 1];
               }
            }
         }
      }
      // get XY data
      else {
         // get X
         printf("%s:%d: fXYData=%d fXDouble=%d\n",
                __FILE__, __LINE__,
                fXYData?1:0,
                fXDouble?1:0);
         if (fXYData) {
            if (fX != 0) free (fX);
            fCmdLine->getData (string(fName.Data()), fX, fLength, 
                              (IsComplex() ? (fXDouble ? 4 : 2) : 0), fOffsetX);
         }
         // get Y
         if (fY != 0) free (fY);
         fCmdLine->getData (string(fName.Data()), fY, fLength, 
                           (IsComplex() ? 1 : 0), fOffsetY);
         // eliminate zeros
         if (fNoZero && fXYData && (fX != 0) && (fY != 0)) {
            Int_t j = 0;
            for (Int_t i = 0; i < fLength; i++) {
               if ((fX[i] == 0) && 
                  ((IsComplex() && (fY[2*i]==0) && (fY[2*i+1]==0)) ||
                  (!IsComplex() && (fY[i]==0)))) {
                  fN--;
               }
               else {
                  if (j < i) {
                     fX[j] = fX[i];
                     if (IsComplex()) {
                        fY[2*j] = fY[2*i];
                        fY[2*j+1] = fY[2*i+1];
                     }
                     else {
                        fY[j] = fY[i];
                     }
                  }
                  j++;
               }
            }
         }
      }
      return (fX != 0) && (fY != 0);
   }


}
namespace diag {
   using namespace ligogui;
   using namespace std;


   AuxDataDesc::AuxDataDesc() 
   : fIndex (-1), fState (kUnknown)
   {
   }

   AuxDataDesc::AuxDataDesc (const char* graph, const char* achn, 
                     const char* bchn)
   : fIndex (-1), fState (kUnknown)
   {
      fGraph = graph ? graph : "";
      fAChn = achn ? achn : "";
      fBChn = bchn ? bchn : "";
   }

   bool AuxDataDesc::operator== (const AuxDataDesc& aux) const
   {
      return (fGraph == aux.fGraph) &&
         (fAChn == aux.fAChn) && (fBChn == aux.fBChn);
   }

   bool AuxDataDesc::operator< (const AuxDataDesc& aux) const
   {
      return (fGraph < aux.fGraph) ||
         ((fGraph == aux.fGraph) && (fAChn < aux.fAChn)) ||
         ((fGraph == aux.fGraph) && (fAChn == aux.fAChn) && (fBChn < aux.fBChn));
   }


   typedef vector<string> channellisttype;
   typedef pair <int, int> indextype;
   class dataentrytype {
   public:
      string 	name;
      int	len;
      int	ofs;
      dataentrytype () : name (""), len (-1), ofs (0) {
      }
   };
   typedef map <indextype, dataentrytype> datalisttype;


   bool AddDataFromIndex (PlotSet& pset, basic_commandline& cmd, 
                     calibration::Table* caltable,
                     int stepnum, bool suppressStep, bool setCurrent)
   {
      basic_commandline::masterindex	mi;
      if (!cmd.readMasterIndex (mi)) {
         return false;
      }
      // get largest step
      int maxstep = 0;
      for (basic_commandline::masterindex::iterator iter = mi.begin();
          iter != mi.end(); iter++) {
         if (iter->getStep() > maxstep) {
            maxstep = iter->getStep();
         }
      }
      int entrynum = 0;
      for (basic_commandline::masterindex::iterator iter = mi.begin();
          iter != mi.end(); iter++, entrynum++) {
         if ((iter->getStep() < 0) ||
            ((stepnum != -1) && (iter->getStep() != stepnum))) {
            continue;
         }
         if (stepnum == -1) {
            setCurrent = (iter->getStep() == maxstep);
         }
         AddDataFromIndexEntry (pset, cmd, entrynum, caltable,
                              suppressStep, setCurrent);
      }
      return true;
   }


   bool AddRMSFromPowerSpectrum (PlotSet& pset,
                     const char* chnA, calibration::Table* caltable = 0,
                     int step = -1, bool setCurrent = false)
   {
      if (chnA == 0) {
         return false;
      }
      if (step >= 0) {
         string a = xsilStd::makeName (chnA, step);
         AddRMSFromPowerSpectrum (pset, a.c_str(), caltable);
      }
      if ((step < 0) || setCurrent) {
         // search for power spectrum
         const PlotDescriptor* pd = pset.Get (kPTPowerSpectrum, chnA);
         if ((pd == 0) || (pd->GetData() == 0)) {
            return false;
         }
         // compute rms
         BasicDataDescriptor* desc = new FreqRMSDataDescriptor (pd->GetData());
         if (desc != 0) {
            string chn = string (chnA) + "(RMS)";
            // bw=1: trick to display both in the same plot w/ same units
            calibration::Descriptor cal (pd->Cal().GetTime(),
                                 caltable ? kPTPowerSpectrum : 0,
                                 chnA, 0, 1.0);
            if (caltable)caltable->AddUnits (cal);
            pset.Add (desc, kPTPowerSpectrum, chn.c_str(), 0, 
                     &(pd->Param()), &cal);
         }
      }
      return true;
   }


   bool AddTransferFunctionFromCross (PlotSet& pset,
                     const char* chnA, const char* chnB, 
                     calibration::Table* caltable = 0,
                     int step = -1, bool setCurrent = false)
   {
      if ((chnA == 0) || (chnB == 0)) {
         return false;
      }
      if (step >= 0) {
         string a = xsilStd::makeName (chnA, step);
         string b = xsilStd::makeName (chnB, step);
         AddTransferFunctionFromCross (pset, a.c_str(), b.c_str(), caltable);
      }
      if ((step < 0) || setCurrent) {
         // search for cross spectrum
         const PlotDescriptor* cross = 
            pset.Get (kPTCrossCorrelation, chnA, chnB);
         if ((cross == 0) || (cross->GetData() == 0)) {
            return false;
         }
         // search for power spectrum
         const PlotDescriptor* psd = pset.Get (kPTPowerSpectrum, chnA);
         if ((psd == 0) || (psd->GetData() == 0)) {
            return false;
         }
         // compute transfer function
         BasicDataDescriptor* desc = 
            new DivSqrDataDescriptor (cross->GetData(), psd->GetData());
         if (desc != 0) {
            calibration::Descriptor cal (cross->Cal().GetTime(),
                                 caltable ? kPTTransferFunction : 0,
                                 chnA, chnB);
            if (caltable) caltable->AddUnits (cal);
            pset.Add (desc, kPTTransferFunction, chnA, chnB, 
                     &(cross->Param()), &cal);
         }
      }
      return true;
   }


   typedef vector<string> paramlist;


   bool filterXML (string& s, const paramlist& plist)
   {
      string t (s);
      s = "";
      while (!t.empty()) {
         string line;
         string::size_type pos;
         if ((pos = t.find ('\n')) == string::npos) {
            line = t;
            t = "";
         }
         else {
            line = t.substr (0, pos);
            t.erase (0, pos + 1);
         }
         if (line.empty()) {
            continue;
         }
         if ((line.find ("Name=\"Channel") == string::npos) &&
            (line.find ("Name=\"M") == string::npos) &&
            (line.find ("Name=\"N") == string::npos)) {
            s += line + "\n";
         }
      }
      for (paramlist::const_iterator i = plist.begin(); i != plist.end(); i++) 
      {
         s += *i + "\n";
      }
      return true;
   }


   bool AddParameter (const string& ictype,
                     const paramlist& plist, basic_commandline& cmd,
                     const string& name, ParameterDescriptor& prmd, 
                     double& x0, double& dx, double& bw, int& subtype)
   {
      if (my_debug)
      {  
	 cerr << "AddParameter() - ictype = " << ictype << ", name = " << name << endl ;
	 cerr << " paramlist = " ;
	 for (paramlist::const_iterator i = plist.begin(); i != plist.end(); i++) 
	    cerr << *i << " " ;
	 cerr << endl ;
      }

      if (compareTestNames (ictype, kPTTimeSeries) == 0) 
      {
	 if (my_debug) cerr << "  Time Series" << endl ;
         // get start time
         x0 = 0;
         dx = 1;
         bw = 0;
         subtype = 0;
         if (!cmd.getVar (name + "." + stResultdt, dx)) {
            return false;
         }
         // get start time
         unsigned long sec = 0;
         unsigned long nsec = 0;
         if (cmd.getTime (name + "." + stTimeSeriest0, sec, nsec)) {
            prmd.SetStartTime (sec, nsec);
         }
         // get averages
         int avg = 1;
         if (cmd.getVar (name + "." + stTimeSeriesAverages, avg)) {
            prmd.SetAverages (avg);
         }
         // check subtype for statistical properties
         cmd.getVar (name + "." + stResultSubtype, subtype);
         // get list of parameters
         string s;
         if ((cmd.getVar (name + ".xml", s)) && !s.empty()) {
            filterXML (s, plist);
            prmd.SetUser (s.c_str());
         }
	 if (my_debug) cerr << "AddParameter() - end, x0 = " << x0 << ", dx = " << dx << ", bw = " << bw << endl ;
         return true;
      }
      else if ((compareTestNames (ictype, kPTFrequencySeries) == 0) ||
              (compareTestNames (ictype, kPTPowerSpectrum) == 0) ||
              (compareTestNames (ictype, kPTCoherence) == 0) ||
              (compareTestNames (ictype, kPTCrossCorrelation) == 0)) 
      {
	 if (my_debug) cerr << "  Frequency series, power spectrum, coherence or cross correlation" << endl ;
         x0 = 0;
         dx = 1;
         bw = 0;
         subtype = 0;
         if (!cmd.getVar (name + "." + stSpectrumf0, x0)) {
            return false;
         }
         // get frequency spacing
         if (!cmd.getVar (name + "." + stSpectrumdf, dx)) {
            return false;
         }
         // get start time
         unsigned long sec = 0;
         unsigned long nsec = 0;
         if (cmd.getTime (name + "." + stSpectrumt0, sec, nsec)) {
            prmd.SetStartTime (sec, nsec);
         }
         // get averages
         int avg = 1;
         if (cmd.getVar (name + "." +stSpectrumAverages, avg)) {
            prmd.SetAverages (avg);
         }
         // get bandwidth
         if (cmd.getVar (name + "." + stSpectrumBW, bw)) {
            char buf[128];
            sprintf (buf, "BW=%g", bw);
            prmd.SetThird (buf);
         }
         // check subtype for statistical properties
         cmd.getVar (name + "." + stResultSubtype, subtype);
         // get list of parameters
         string s;
         if ((cmd.getVar (name + ".xml", s)) && !s.empty()) {
            filterXML (s, plist);
            prmd.SetUser (s.c_str());
         }
	 if (my_debug) cerr << "AddParameter() - end, x0 = " << x0 << ", dx = " << dx << ", bw = " << bw << endl ;
         return true;
      }
      else if ((compareTestNames (ictype, kPTTransferFunction) == 0) ||
              (compareTestNames (ictype, kPTCoherenceFunction) == 0)) 
      {
	 if (my_debug) cerr << "  Transfer function or coherence function" << endl ;
         x0 = 0;
         dx = 1;
         subtype = 0;
   // JCB
	 // Get f0
	 if (!cmd.getVar (name + "." + stSpectrumf0, x0)) {
	    if (my_debug) cerr << "    Getting f0 failed." << endl ;
	    return false ;
	 }
	 // Get frequency spacing
	 if (!cmd.getVar (name + "." + stSpectrumdf, dx)) {
	    if (my_debug) cerr << "    Getting df failed." << endl ;
	    return false ;
	 }
   // JCB -end
         // get start time
         unsigned long sec = 0;
         unsigned long nsec = 0;
         if (cmd.getTime (name + "." + stResultt0, sec, nsec)) {
            prmd.SetStartTime (sec, nsec);
         }
         // get averages
         int avg = 1;
         if (cmd.getVar (name + "." + stResultAverages, avg)) {
            prmd.SetAverages (avg);
         }
         // check subtype for statistical properties
         cmd.getVar (name + "." + stResultSubtype, subtype);
         // get list of parameters
         string s;
         if ((cmd.getVar (name + ".xml", s)) && !s.empty()) {
            filterXML (s, plist);
            prmd.SetUser (s.c_str());
         }      
	 if (my_debug) cerr << "AddParameter() - end, x0 = " << x0 << ", dx = " << dx << endl ;
         return true;
      }
      else if ((compareTestNames (ictype, kPTTransferCoefficients) == 0) ||
              (compareTestNames (ictype, kPTCoherenceCoefficients) == 0) ||
              (compareTestNames (ictype, kPTHarmonicCoefficients) == 0) ||
              (compareTestNames (ictype, kPTIntermodulationCoefficients) == 0)) 
      {
	 if (my_debug) cerr << "  Transfer Coefficients or other coefficients" << endl ;
         x0 = 0;
         dx = 1;
         bw = 0;
         subtype = 0;
         // get start time
         unsigned long sec = 0;
         unsigned long nsec = 0;
         if (cmd.getTime (name + "." + stResultt0, sec, nsec)) {
            prmd.SetStartTime (sec, nsec);
         }
         // get averages
         int avg = 1;
         if (cmd.getVar (name + "." + stResultAverages, avg)) {
            prmd.SetAverages (avg);
         }
         // check subtype for statistical properties
         cmd.getVar (name + "." + stResultSubtype, subtype);
         // get list of parameters
         string s;
         if ((cmd.getVar (name + ".xml", s)) && !s.empty()) {
            filterXML (s, plist);
            prmd.SetUser (s.c_str());
         }
	 if (my_debug) cerr << "AddParameter() - end, x0 = " << x0 << ", dx = " << dx << ", bw = " << bw << endl ;
         return true;
      }
      else {
         return false;
      }
   }


   bool AddFunction (PlotSet& pset, basic_commandline& cmd,
                    const ParameterDescriptor& prmd,
                    calibration::Descriptor& cal, const TString& n,
                    bool xy, bool xdouble, bool cmplx, double x0, double dx,
                    const char* graphtype, const char* chnA, const char* chnB,
                    int len, int ofs = 0, calibration::Table* caltable = 0, 
                    int refnum = -1, int step = -1, bool setCurrent = false) 
   {
      if ((graphtype == 0) || (chnA == 0)) {
         return false;
      }
      bool elZero = false;
      if ((strcasecmp (graphtype, kPTTransferFunction) == 0) ||
         (strcasecmp (graphtype, kPTCoherenceFunction) == 0)) {
         elZero = true;
      }
      bool persistent = false;
      if (strncasecmp (n, stReference, strlen (stReference)) == 0) {
         persistent = true;
      }
      if (step >= 0) {
         // create data descriptor
         BasicDataDescriptor* desc = 0;
         if (xy) {
            desc = new (nothrow) DiagDataDescriptor 
               (&cmd, n, len, (int)0, ofs, xdouble, cmplx, true, elZero);
         }
         else {
            desc = new (nothrow) DiagDataDescriptor 
               (&cmd, n, len, x0, dx, ofs, cmplx, true, elZero);
         }
         if (desc == 0) {
            return false; 
         }
         if (persistent) {
            desc->SetPersistent();
            desc->SetDirty();
         }
         // compose channel name with index
         string a;
         if (strchr (chnA, '(') == 0) {
            a = xsilStd::makeName (chnA, step);
         }
         else {
            string tmp = chnA;
            tmp.erase (tmp.find ('('));
            a = xsilStd::makeName (tmp, step);
            tmp = chnA;
            tmp.erase (0, tmp.find ('('));
            a += tmp;
         }
         string b;
         if (chnB) {
            if (strchr (chnB, '(') == 0) {
               b = xsilStd::makeName (chnB, step);
            }
            else {
               string tmp = chnB;
               tmp.erase (tmp.find ('('));
               b = xsilStd::makeName (tmp, step);
               tmp = chnB;
               tmp.erase (0, tmp.find ('('));
               b += tmp;
            }
         }
         if (refnum >= 0) {
            char buf[64];
            sprintf (buf, "(REF%i)", refnum);
            a += buf;
            b += buf;
         }
         // add plot descriptor to pool
         if (caltable) caltable->AddUnits (cal);
         pset.Add (desc, graphtype, a.c_str(), chnB ? b.c_str() : 0, 
                  &prmd, &cal);
      }
      if ((step < 0) || setCurrent) {
         // create data descriptor
         BasicDataDescriptor* desc = 0;
         if (xy) {
            // cout << "new " << graphtype << " (" << n << ") xy len=" << len << 
               // " ofs=" << ofs << " cmplx?" << (cmplx?"yes":"no") << endl;
            desc = new (nothrow) DiagDataDescriptor 
               (&cmd, n, len, (int)0, ofs, xdouble, cmplx, true, elZero);
         }
         else {
            // cout << "new " << graphtype << " (" << n << ")  y len=" << len << 
               // " ofs=" << ofs << " cmplx?" << (cmplx?"yes":"no") << endl;
            desc = new (nothrow) DiagDataDescriptor 
               (&cmd, n, len, x0, dx, ofs, cmplx, true, elZero);
         }
         if (desc == 0) {
            return false; 
         }
         if (persistent) {
            desc->SetPersistent();
            desc->SetDirty();
         }
         // cout << "Desc N=" << desc->GetN() << " X?" << (desc->GetX()?"yes":"no") <<
            // " Y?" << (desc->GetY()?"yes":"no") << endl;
         // add plot descriptor to pool
         string a = chnA;
         string b = chnB ? chnB : "";
         if (refnum >= 0) {
            char buf[64];
            sprintf (buf, "(REF%i)", refnum);
            a += buf;
            b += buf;
         }
         if (caltable) caltable->AddUnits (cal);
         pset.Add (desc, graphtype, a.c_str(), chnB ? b.c_str() : 0, 
                  &prmd, &cal);
      }
      return true;
   }


   bool AddTable (PlotSet& pset, basic_commandline& cmd,
                 const ParameterDescriptor& prmd,
                 calibration::Descriptor& cal, const TString& n,
                 bool xy, bool cmplx, double x0, double dx, 
                 const char* graphtype, const char* chn, 
                 int col, int M, int N, calibration::Table* caltable = 0, 
                 int refnum = -1, int step = -1, bool setCurrent = false) 
   {
      if ((graphtype == 0) || (chn == 0)) {
         return false;
      }
      bool persistent = false;
      if (strncasecmp (n, stReference, strlen (stReference)) == 0) {
         persistent = true;
      }
      if (step >= 0) {
         // create data descriptor
         BasicDataDescriptor* desc = 0;
         if (xy) {
            desc = new  (nothrow) DiagDataDescriptor 
               (&cmd, n, cmplx, M, N, col);
         }
         else {
            desc = new  (nothrow) DiagDataDescriptor 
               (&cmd, n, cmplx, M, N, col, x0, dx);
         }
         if (desc == 0) {
            return false; 
         }
         if (persistent) {
            desc->SetPersistent();
            desc->SetDirty();
         }
         // compose channel name with index
         string a;
         if (strchr (chn, '(') == 0) {
            a = xsilStd::makeName (chn, step);
         }
         else {
            string tmp = chn;
            tmp.erase (tmp.find ('('));
            a = xsilStd::makeName (tmp, step);
            tmp = chn;
            tmp.erase (0, tmp.find ('('));
            a += tmp;
         }
         if (refnum >= 0) {
            char buf[64];
            sprintf (buf, "(REF%i)", refnum);
            a += buf;
         }
         // add plot descriptors to pool
         if (caltable) caltable->AddUnits (cal);
         pset.Add (desc, graphtype, a.c_str(), 0, &prmd, &cal);
      }
      if ((step < 0) || setCurrent) {
         // create data descriptor
         BasicDataDescriptor* desc = 0;
         if (xy) {
            desc = new  (nothrow) DiagDataDescriptor 
               (&cmd, n, cmplx, M, N, col);
         }
         else {
            desc = new  (nothrow) DiagDataDescriptor 
               (&cmd, n, cmplx, M, N, col, x0, dx);
         }
         if (desc == 0) {
            return false; 
         }
         if (persistent) {
            desc->SetPersistent();
            desc->SetDirty();
         }
         string a = chn;
         if (refnum >= 0) {
            char buf[64];
            sprintf (buf, "(REF%i)", refnum);
            a += buf;
         }
         // add plot descriptors to pool
         if (caltable) caltable->AddUnits (cal);
         pset.Add (desc, graphtype, a.c_str(), 0, &prmd, &cal);
      }
      return true;
   }


   bool AddDataToPool (PlotSet& pset, basic_commandline& cmd, 
                     const char* graphtype, const datalisttype& dlist,
                     const vector<string>& chnA, const vector<string>& chnB,
                     calibration::Table* caltable = 0, int refnum = -1,
                     int step = 0,
                     bool suppressStep = true, bool setCurrent = false) 
   {
      if (my_debug) cerr << "AddDataToPool() - graphtype = " << graphtype << endl ;
      // add time series
      if (strcasecmp (graphtype, kPTTimeSeries) == 0) 
      {
         for (datalisttype::const_iterator iter = dlist.begin(); 
             iter != dlist.end(); iter++) {
            if ((iter->first.first < 0) ||
               (iter->first.first >= (int)chnA.size()) ||
               iter->second.name.empty() ||
               (iter->second.len <= 0)) {
               continue;
            }
            // channels
            const char* a = chnA[iter->first.first].c_str();
            // parameters
            paramlist plist;
            char buf[1024];
            sprintf (buf, "%s%i%s", 
                    "    <Param Name=\"N\" Type=\"int\">", 
                    iter->second.len, "</Param>");
            plist.push_back (string (buf));
            sprintf (buf, "%s%s%s", 
                    "    <Param Name=\"Channel\" Type=\"string\" "
                    "Unit=\"channel\">", a, "</Param>");
            plist.push_back (string (buf));
            ParameterDescriptor prmd;
            double t0 = 0;
            double dt = 0;
            double bw = 0;
            int subtype = 0;
            if (!AddParameter (kPTTimeSeries, plist,
                              cmd, iter->second.name,
                              prmd, t0, dt, bw, subtype)) {
               continue;
            }
            // is complex?
            bool cmplx = (subtype == 1) || (subtype == 5);
            // is XY?
            bool xy = subtype >= 4;
            bool xdouble = false; /// $param x values are stored as double precision
            // set calibration info
            unsigned int sec, nsec;
            prmd.GetStartTime (sec, nsec);
            calibration::Descriptor cal (Time (sec, nsec),
                                 caltable ? kPTTimeSeries : 0, a);
            // add descriptor to plot set
            TString n = iter->second.name.c_str();
            AddFunction (pset, cmd, prmd, cal, n,
                        xy, xdouble, cmplx, t0, dt, kPTTimeSeries, a, 0,
                        iter->second.len, iter->second.ofs, 
                        caltable, refnum, suppressStep ? -1 : step,
                        setCurrent);
            // add statistical values to pool
            int avg;
            if (prmd.GetAverages(avg) && (avg > 1) && (subtype == 3)) {
               // std dev
               AddFunction (pset, cmd, prmd, cal, n,
                           xy, xdouble, cmplx, t0, dt, kPTTimeSeries,
                           (string(a)+"(STDDEV)").c_str(), 0,
                           iter->second.len, 
                           iter->second.ofs + 1 * iter->second.len, 
                           caltable, refnum, suppressStep ? -1 : step,
                           setCurrent);
               // min
               AddFunction (pset, cmd, prmd, cal, n,
                           xy, xdouble, cmplx, t0, dt, kPTTimeSeries,
                           (string(a)+"(MIN)").c_str(), 0,
                           iter->second.len, 
                           iter->second.ofs + 2 * iter->second.len, 
                           caltable, refnum, suppressStep ? -1 : step,
                           setCurrent);
               // max
               AddFunction (pset, cmd, prmd, cal, n,
                           xy, xdouble, cmplx, t0, dt, kPTTimeSeries,
                           (string(a)+"(MAX)").c_str(), 0,
                           iter->second.len, 
                           iter->second.ofs + 3 * iter->second.len, 
                           caltable, refnum, suppressStep ? -1 : step,
                           setCurrent);
               // rms
               AddFunction (pset, cmd, prmd, cal, n,
                           xy, xdouble, cmplx, t0, dt, kPTTimeSeries,
                           (string(a)+"(RMS)").c_str(), 0,
                           iter->second.len, 
                           iter->second.ofs + 4 * iter->second.len, 
                           caltable, refnum, suppressStep ? -1 : step,
                           setCurrent);
            }
         }
      }
      
      // add power spectrum
      // add coherence
      // add cross spectrum
      // add frequency series
      // add transfer function
      // add coherence function
      else if ((strcasecmp (graphtype, kPTPowerSpectrum) == 0) ||
              (strcasecmp (graphtype, kPTCoherence) == 0) ||
              (strcasecmp (graphtype, kPTCrossCorrelation) == 0) ||
              (strcasecmp (graphtype, kPTFrequencySeries) == 0) ||
              (strcasecmp (graphtype, kPTTransferFunction) == 0) ||
              (strcasecmp (graphtype, kPTCoherenceFunction) == 0)) 
      {


         for (datalisttype::const_iterator iter = dlist.begin(); 
             iter != dlist.end(); iter++) {



            if ((iter->first.first < 0) ||
               (iter->first.first >= (int)chnA.size()) ||
               iter->second.name.empty() ||
               (iter->second.len <= 0)) {
               continue;
            }
            // two channel quantity?
            bool twoChn = 
               (strcasecmp (graphtype, kPTCoherence) == 0) ||
               (strcasecmp (graphtype, kPTCrossCorrelation) == 0) ||
               (strcasecmp (graphtype, kPTTransferFunction) == 0) ||
               (strcasecmp (graphtype, kPTCoherenceFunction) == 0);
            if (twoChn && ((iter->first.second < 0) ||
                          (iter->first.second >= (int)chnB.size()))) {
               continue;
            }
            // channels
            const char* a = chnA[iter->first.first].c_str();
            const char* b = twoChn ? chnB[iter->first.second].c_str() : 0;
	    if (my_debug) 
	    {
	       cerr << "  Transfer function, a = " << a ;
	       if (b)
		  cerr << ", b = " << b ;
	       cerr << endl ;
	    }
            // create parameter descriptor
            paramlist plist;
            char buf[1024];
            sprintf (buf, "%s%i%s", 
                    "    <Param Name=\"N\" Type=\"int\">", 
                    iter->second.len, "</Param>");
            plist.push_back (string (buf));
            sprintf (buf, "%s", 
                    "    <Param Name=\"M\" Type=\"int\">1</Param>");
            plist.push_back (string (buf));
            sprintf (buf, "%s%s%s", 
                    "    <Param Name=\"ChannelA\" Type=\"string\" "
                    "Unit=\"channel\">", a, "</Param>");
            plist.push_back (string (buf));
            if (twoChn) {
               sprintf (buf, "%s%s%s", 
                       "    <Param Name=\"ChannelB[0]\" Type=\"string\" "
                       "Unit=\"channel\">", b, "</Param>");
               plist.push_back (string (buf));
            }
            ParameterDescriptor prmd;
            double x0 = 0;
            double dx = 0;
            double bw = 0;
            int subtype = 0;
            if (!AddParameter (graphtype, plist, cmd, iter->second.name,
                              prmd, x0, dx, bw, subtype)) {
               continue;
            }
            // is complex?
            // is XY?
            bool cmplx;
            bool xy;
            bool xdouble = false; /// $param x values are stored as double precision
            if ((strcasecmp (graphtype, kPTPowerSpectrum) == 0) ||
               (strcasecmp (graphtype, kPTCoherence) == 0) ||
               (strcasecmp (graphtype, kPTCrossCorrelation) == 0) ||
               (strcasecmp (graphtype, kPTFrequencySeries) == 0)) {
               cmplx = (subtype == 0) || (subtype == 4) ||
                  (subtype == 2) || (subtype == 6);
               xy = subtype >= 4;
            }
            else {
               cmplx = (subtype == 0) || (subtype == 3) ||
                  (subtype == 1) || (subtype == 4) || (subtype == 6);
               xdouble = (subtype == 6) || (subtype == 7);
               xy = subtype >= 3;


            }
         
            // set calibration info
            unsigned int sec, nsec;
            prmd.GetStartTime (sec, nsec);
            calibration::Descriptor cal (Time (sec, nsec),
                                 caltable ? graphtype : 0, a, b, bw);
            // add descriptor to plot set
            TString n = iter->second.name.c_str();
            AddFunction (pset, cmd, prmd, cal, n, xy, xdouble, cmplx, x0, dx,
                        graphtype, a, b, iter->second.len, iter->second.ofs, 
                        caltable, refnum, suppressStep ? -1 : step, 
                        setCurrent);
         
            // add rms of psd to plot set
            if ((strcasecmp (graphtype, kPTPowerSpectrum) == 0) &&
               (refnum < 0)) {
               AddRMSFromPowerSpectrum (pset, a, caltable, 
                                    suppressStep ? -1 : step, setCurrent);
            }
            // add transfer function from cross
            else if ((strcasecmp (graphtype, kPTCrossCorrelation) == 0) &&
                    (refnum < 0)) {
               AddTransferFunctionFromCross (pset, a, b, caltable, 
                                    suppressStep ? -1 : step, setCurrent);
            }
         }
      }
      
      // add transfer coefficients
      // add coherence coefficients
      // add harmonic coefficients
      // add intermodulation coefficients
      else if ((strcasecmp (graphtype, kPTTransferCoefficients) == 0) ||
              (strcasecmp (graphtype, kPTCoherenceCoefficients) == 0) ||
              (strcasecmp (graphtype, kPTHarmonicCoefficients) == 0) ||
              (strcasecmp (graphtype, kPTIntermodulationCoefficients) == 0)) 
      {
         if (dlist.size() == 1) {
            // setup result records
            int N = chnB.size();
            if (N == 0) N = chnA.size();
            int M = dlist.begin()->second.len / (N > 0 ? N : 1);
            TString n = dlist.begin()->second.name.c_str();
            for (int i = 0; i < N; i++) {
               // channels
               string chnname = (i < (int)chnA.size()) ? chnA[i] : chnB[i];
               paramlist plist;
               char buf[1024];
               sprintf (buf, "%s%i%s", 
                       "    <Param Name=\"M\" Type=\"int\">", M, "</Param>");
               plist.push_back (string (buf));
               sprintf (buf, "%s", 
                       "    <Param Name=\"N\" Type=\"int\">1</Param>");
               plist.push_back (string (buf));
               // for (int j = 0; j < chnA.size(); j++) {
                  // sprintf (buf, "%s%i%s%s%s", 
                          // "    <Param Name=\"ChannelA[", j, 
                          // "]\" Type=\"string\" Unit=\"channel\">", 
                          // chnA[j].c_str(), "</Param>");
                  // plist.push_back (string (buf));
               // }
               sprintf (buf, "%s%s%s", 
                       "    <Param Name=\"ChannelB[0]\" Type=\"string\" "
                       "Unit=\"channel\">", chnname.c_str(), "</Param>");
               plist.push_back (string (buf));
            
               // parameters
               ParameterDescriptor prmd;
               double x0 = 0;
               double dx = 0;
               double bw = 0;
               int subtype = 0;
               if (!AddParameter (graphtype, plist, 
                                 cmd, dlist.begin()->second.name,
                                 prmd, x0, dx, bw, subtype)) {
                  continue;
               }
               bool cmplx = !((subtype == 3) || (subtype == 7));
               bool xy = (subtype >= 4) && (subtype < 8);
               bool xdouble = false;
               // set calibration info
               unsigned int sec, nsec;
               prmd.GetStartTime (sec, nsec);
               calibration::Descriptor cal (Time (sec, nsec),
                                    caltable ? graphtype : 0, 
                                    chnname.c_str(), 0, bw);
               // add it to pool
               if (chnB.size() != 0) {
                  AddTable (pset, cmd, prmd, cal, n, xy, cmplx, x0, dx, 
                           graphtype, chnname.c_str(), i, M, N, caltable, 
                           refnum, suppressStep ? -1 : step, setCurrent);
               }
               else {
                  AddFunction (pset, cmd, prmd, cal, n, xy, xdouble, cmplx, x0, dx,
                              graphtype, chnname.c_str(), 0, M, M, caltable, 
                              refnum, suppressStep ? -1 : step, setCurrent); 
               }
            }
         }
      }
      
      // add transfer matrix
      else if ((strncasecmp (graphtype, kPTTransferCoefficients, 
                            strlen (kPTTransferCoefficients)) == 0) && 
              (strstr (graphtype, "matrix") != 0)) 
      {
         if (dlist.size() == 1) {
            // setup result records
            int N = chnB.size();
            int M = chnA.size();
            if (N == 0) {
               N = M;
               M = 0;
            }
            TString n = dlist.begin()->second.name.c_str();
            for (int i = M; i < N; i++) {
               // channels
               string chnname = (i < (int)chnA.size()) ? chnA[i] : chnB[i];
               paramlist plist;
               char buf[1024];
               sprintf (buf, "%s%i%s", 
                       "    <Param Name=\"M\" Type=\"int\">", M, "</Param>");
               plist.push_back (string (buf));
               sprintf (buf, "%s", 
                       "    <Param Name=\"N\" Type=\"int\">1</Param>");
               plist.push_back (string (buf));
               // for (int j = 0; j < chnA.size(); j++) {
                  // sprintf (buf, "%s%i%s%s%s", 
                          // "    <Param Name=\"ChannelA[", j, 
                          // "]\" Type=\"string\" Unit=\"channel\">", 
                          // chnA[j].c_str(), "</Param>");
                  // plist.push_back (string (buf));
               // }
               sprintf (buf, "%s%s%s", 
                       "    <Param Name=\"ChannelB[0]\" Type=\"string\" "
                       "Unit=\"channel\">", chnname.c_str(), "</Param>");
               plist.push_back (string (buf));
               // parameters
               ParameterDescriptor prmd;
               double x0 = 1.0;
               double dx = 1.0;
               double bw = 0;
               int subtype = 0;
               if (!AddParameter (kPTTransferCoefficients, 
                                 plist, cmd, dlist.begin()->second.name,
                                 prmd, x0, dx, bw, subtype)) {
                  continue;
               }
               bool cmplx = true;
               bool xy = false;
               bool xdouble = false; /// $param x values are stored as double precision
               x0 = 1.0;
               dx = 1.0;
               // set calibration info
               unsigned int sec, nsec;
               prmd.GetStartTime (sec, nsec);
               calibration::Descriptor cal (Time (sec, nsec),
                                    caltable ? graphtype : 0, chnname.c_str(), 0, bw);
               // add it to pool
               if (chnB.size() != 0) {
                  AddTable (pset, cmd, prmd, cal, n, xy, cmplx, x0, dx, 
                           graphtype, chnname.c_str(), i-M, M, N-M, caltable, 
                           refnum, suppressStep ? -1 : step, setCurrent);
               }
               else {
                  int len = dlist.begin()->second.len / (N > 0 ? N : 1);
                  AddFunction (pset, cmd, prmd, cal, n, xy, xdouble, cmplx, x0, dx,
                              graphtype, chnname.c_str(), 0, len, len, caltable, 
                              refnum, suppressStep ? -1 : step, setCurrent); 
               }
            }
         }
      }
      else 
      {
         return false;
      }
      if (my_debug) cerr << "AddDataToPool() end" << endl ;
      return true;
   }


   bool AddDataFromIndexEntry (PlotSet& pset, basic_commandline& cmd, 
                     int entrynum, calibration::Table* caltable,
                     bool suppressStep, bool setCurrent)
   
   {
      if (entrynum <= 0) {
         return false;
      }
      // get entry
      char	buf[256];
      sprintf (buf, "%s.%s[%i]", stIndex, stIndexEntry, entrynum);
      string entry;
      if (!cmd.getVar (string (buf), entry)) {
         return false;
      }
      while (!entry.empty() && (entry[0] == ' ')) {
         entry.erase (0, 1);
      }
   
      istringstream is (entry);
      if (!is) {
         return false;
      }
      // get category
      string line;
      getline (is, line);
      // remove blanks, etc.
      string::size_type pos;
      while ((pos = line.find_first_of (" \t:")) != string::npos) {
         line.erase (pos, 1);
      }
      string cat;
      int step;
      int i1;
      int i2;
      if (!xsilStd::analyzeName (line, cat, step, i2) || (i2 != -1)) {
         return false;
      }
      if (step < 0) {
         step = 0;
      }
   
      // parse index entry
      channellisttype chn;
      channellisttype chnA;
      channellisttype chnB;
      datalisttype dlist;
      string n;
      string val;
      while (true) {
         getline (is, line);
         if (!is)  {
            break;
         }
         if ((pos = line.find ("=")) == string::npos) {
            continue;
         }
         // name and indices
         if (!xsilStd::analyzeName (line.substr (0, pos), n, i1, i2)) {
            continue;
         }
         // get value
         val = line.substr (pos + 1, line.size() - pos - 1);
         while (!val.empty() && (val[0] == ' ')) {
            val.erase (0, 1);
         }
         // remove blanks, etc.
         while ((pos = val.find_first_of (" \t;")) != string::npos) {
            val.erase (pos, 1);
         }
         // add channel
         if (compareTestNames (n, ieChannel) == 0) {
            if (i1 < 0) i1 = 0;
            if ((int)chn.size() < i1 + 1) {
               chn.resize (i1 + 1);
            }
            chn[i1] = val;
         }
         else if (compareTestNames (n, ieChannelA) == 0) {
            if (i1 < 0) i1 = 0;
            if ((int)chnA.size() < i1 + 1) {
               chnA.resize (i1 + 1);
            }
            chnA[i1] = val;
         }
         else if (compareTestNames (n, ieChannelB) == 0) {
            if (i1 < 0) i1 = 0;
            if ((int)chnB.size() < i1 + 1) {
               chnB.resize (i1 + 1);
            }
            chnB[i1] = val;
         }
         // add name
         else if (compareTestNames (n, ieName) == 0) {
            dlist[indextype (i1, i2)].name = val; 
         }
         // add offset
         else if (compareTestNames (n, ieOffset) == 0) {
            dlist[indextype (i1, i2)].ofs = atoi (val.c_str());
         }
         // add length
         else if (compareTestNames (n, ieLength) == 0) {
            dlist[indextype (i1, i2)].len = atoi (val.c_str());
         }
      }
   
      // ignore master index entry
      if (compareTestNames (cat, icMasterIndex) == 0) {
      }
      // add time series
      else if (compareTestNames (cat, icTimeseries) == 0) {
         AddDataToPool (pset, cmd, kPTTimeSeries, dlist, chn, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add power spectrum
      else if (compareTestNames (cat, icPowerspectrum) == 0) {
         AddDataToPool (pset, cmd, kPTPowerSpectrum, dlist, chn, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add coherence
      else if (compareTestNames (cat, icCoherence) == 0) {
         AddDataToPool (pset, cmd, kPTCoherence, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add cross-correlation
      else if (compareTestNames (cat, icCrosscorrelation) == 0) {
         AddDataToPool (pset, cmd, kPTCrossCorrelation, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add cross-correlation
      else if (compareTestNames (cat, icCrosscorrelation) == 0) {
         AddDataToPool (pset, cmd, kPTCrossCorrelation, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add transfer function
      else if (compareTestNames (cat, icTransferFunction) == 0) {
         AddDataToPool (pset, cmd, kPTTransferFunction, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add coherence function
      else if (compareTestNames (cat, icCoherenceFunction) == 0) {
         AddDataToPool (pset, cmd, kPTCoherenceFunction, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add transfer matrix
      else if (compareTestNames (cat, icTransferMatrix) == 0) {      
         AddDataToPool (pset, cmd, (string (kPTTransferCoefficients) +
                                   " (matrix)").c_str(), 
                       dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add transfer coefficients
      else if (compareTestNames (cat, icTransferCoeff) == 0) {
         AddDataToPool (pset, cmd, kPTTransferCoefficients, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add coherence coefficients
      else if (compareTestNames (cat, icCoherenceCoeff) == 0) {
         AddDataToPool (pset, cmd, kPTCoherenceCoefficients, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add harmonic coefficients
      else if (compareTestNames (cat, icHarmonicCoeff) == 0) {
         AddDataToPool (pset, cmd, kPTHarmonicCoefficients, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // add intermodulation coefficients
      else if (compareTestNames (cat, icIntermodulationCoeff) == 0) {
         AddDataToPool (pset, cmd, kPTIntermodulationCoefficients, dlist, chnA, chnB, 
                       caltable, -1, step, suppressStep, setCurrent);
      }
      // else error
      else {
         return false;
      }
   
      return true;
   }


   bool GetDataDescription (const string& n, basic_commandline& cmd,
                     datalisttype& dlist, string& graphtype,
                     vector<string>& chnA, vector<string>& chnB, int& step)
   {
      if (my_debug) cerr << "GetDataDescription (" << n << ", ... ," << graphtype << "...)" << endl ;
      // clear list
      dlist.clear();
      chnA.clear();
      chnB.clear();
   
      // determine type/subtype
      string type;
      int subtype;
      if (!cmd.getVar (n + "." + stObjectType, type)) {
         return false;
      }
      if (!cmd.getVar (n + "." + stResultSubtype, subtype)) {
         return false;
      }
      xsilStd::DataType typei = xsilStd::Typeid (type.c_str());
      if (!xsilStd::GetGraphType (typei, subtype, graphtype)) {
         return false;
      }
      // cout << "TYPE = " << type << " SUBTYPE = " << subtype << 
         // " GRAPHTYPE= " << graphtype << endl;
      // Get data info
      string s;
      if (!cmd.getVar (n + ".xml", s) || s.empty()) {
         return false;
      }
      ostringstream os;
      os << xsilDataBegin (stResult, type.c_str()) << endl;
      os << xsilParameter<int> (stResultSubtype, subtype);
      os << s;
      os << xsilDataEnd<float> () << endl;
      string xml = os.str();
      xsilStd::datainfo info;
      if (!xsilStd::GetDataInfo (xml.c_str(), info)) {
         return false;
      }
      step = info.fMeasurementNumber;
      // fill data & channel lists
      switch (typei) {
      
         // time series
         case xsilStd::kTimeSeries:
            {
	       if (my_debug) cerr << "  time series" << endl ;
               dataentrytype dentry;
               if (info.fAChn.empty() || (info.fN <= 0)) {
                  return false;
               }
               chnA.push_back (info.fAChn[0]);
               dentry.name = n;
               dentry.ofs = (subtype < 4) ? 0 : info.fN; 
               dentry.len = info.fN;
               dlist[indextype (0, 0)] = dentry;
               break;
            }
         
         // spectra
         // transfer functions
         case xsilStd::kSpectrum:
         case xsilStd::kTransferFunction:
            {
	       if (my_debug) cerr << "  spectra or transfer function" << endl ;
               dataentrytype dentry;
               dentry.name = n;
               dentry.len = info.fN;
               if (info.fAChn.empty() || (info.fN <= 0)) {
                  return false;
               }
               chnA.push_back (info.fAChn[0]);
               bool xy = subtype >= (typei == xsilStd::kSpectrum ? 4 : 3);
               // single channel quantity
               if ((typei == xsilStd::kSpectrum) && 
                  ((subtype == 0) || (subtype == 1) || 
                  (subtype == 4) || (subtype == 5))) {
                  dentry.ofs = (xy ? 1 : 0) * info.fN;
                  dlist[indextype (0, 0)] = dentry;
               }
               // two channel quantity
               else {
                  chnB = info.fBChn;
                  for (int i = 0; i < info.fM; i++) {
                     if ((i >= (int)chnB.size()) || chnB[i].empty()) {
                        continue;
                     }
                     dentry.ofs = (i + (xy ? 1 : 0)) * info.fN;
                     dlist[indextype (0, i)] = dentry;
                  }
               }
               break;
            }
         
         // coefficient list
         case xsilStd::kCoefficients:
            {
	       if (my_debug) cerr << "  coefficient list" << endl ;
               dataentrytype dentry;
               chnA = info.fAChn;
               chnB = info.fBChn;
               dentry.name = n;
               dentry.len = info.fN * info.fM;
               dlist[indextype (0, 0)] = dentry;
               break;
            }
         
         // unrecognized type
         default:
            return false;
      }
      if (my_debug) cerr << "GetDataDescription() end" << endl ;
      return true;
   }


   bool AddTraces (PlotSet& pset, basic_commandline& cmd,
                  const char* traces, calibration::Table* caltable = 0,
                  ReferenceTraceList_t* ref = 0, AuxDataDescList* aux = 0,
                  bool suppessStep = true)
   {
      // get list of names
      string s;
      if (!cmd.getVar (traces, s)) {
         return false;
      }
      // cout << "Traces " << traces << " = " << s << endl;
      if (my_debug) cerr << "diagplot::AddTraces() Traces = " << traces << " = " << s << endl ;
   
      while (s.size() > 0) {
         // get next name
         string n;
         string::size_type pos;
         if ((pos = s.find("\n")) != string::npos) {
            n = s.substr (0, pos).c_str();
            s.erase (0, pos + 1);
         }
         else {
            n = s;
            s = "";
         }
      
         // get info about data object
         datalisttype dlist;
         string graphtype;
         vector<string> chnA;
         vector<string> chnB;
         int step;
         if (!GetDataDescription (n, cmd, dlist, graphtype, chnA, chnB, step) ||
            chnA.empty() || dlist.empty()) {
            continue;
         }
         // raw data object use their name as the A channel
         if (strcasecmp (traces, "rawdatanames") == 0) {
	    if (my_debug) cerr << "Raw data object name = " << n << endl ;
            chnA[0] = n;
         }
      
         // reference objects are listed in the ref trace list
         int refnum = -1;
         if (ref && (strcasecmp (traces, "referencenames") == 0)) {
            string r;
            int i2;
            if (xsilStd::analyzeName (n, r, refnum, i2) && 
               (strcasecmp (r.c_str(), "Reference") == 0) && 
               (refnum >= 0) && (refnum < kMaxReferenceTraces) && (i2 == -1)) 
	    {
               ref->fTraces[refnum].fValid = true;
               ref->fTraces[refnum].fModified = kRefTraceModNo;
               ref->fTraces[refnum].fGraph = graphtype.c_str();
               ref->fTraces[refnum].fAChn = chnA[0].c_str();
               ref->fTraces[refnum].fBChn = chnB.empty() ? "" : chnB[0].c_str();
	       if (my_debug)
	       {
		  cerr << "Adding Reference traces: " << chnA[0].c_str() ;
		  if (!chnB.empty())
		     cerr << ", " << chnB[0].c_str() ;
		  cerr << endl ;
	       }
            }
            else {
               continue;
            }
         }
      
         // auxiliary objects are listed in the aux list
         if (aux && (strcasecmp (traces, "auxdatanames") == 0)) {
            AuxDataDesc a;
            a.fGraph = graphtype.c_str();
            a.fAChn = chnA[0].c_str();
            a.fBChn = chnB.empty() ? "" : chnB[0].c_str();
            a.fName = n.c_str();
            a.fIndex = -1;
            string::size_type pos = n.find ('[');
            if (pos != string::npos) {
               a.fIndex = atoi (n.c_str() + pos + 1);
            }
            a.fState = AuxDataDesc::kUnknown;
            aux->insert (a);
         }
      
         // add data object(s) to pool
         // cout << "add data to pool " << n << " >" << graphtype << " " <<
            // (chnA.empty() ? "no A" : chnA[0].c_str()) << "|" << 
            // (chnB.empty() ? "no B" : chnB[0].c_str()) << endl;
         AddDataToPool (pset, cmd, graphtype.c_str(), dlist,
                       chnA, chnB, caltable, refnum, step, suppessStep);
      }
      return true;
   }




   bool AddRawData (PlotSet& pset, basic_commandline& cmd,
                   calibration::Table* caltable, bool suppessStep)
   {
      return AddTraces (pset, cmd, "rawdatanames", caltable, 0, 0,
                       suppessStep);
   }


   bool AddReferenceTraces (PlotSet& pset, basic_commandline& cmd,
                     calibration::Table* caltable, ReferenceTraceList_t* ref,
                     bool suppessStep)
   {
      return AddTraces (pset, cmd, "referencenames", caltable, ref, 0,
                       suppessStep);
   }


   bool AddAuxiliaryTraces (PlotSet& pset, basic_commandline& cmd,
                     calibration::Table* caltable, AuxDataDescList* aux, 
                     bool suppessStep)
   {
      if (aux) aux->clear();
      return AddTraces (pset, cmd, "auxdatanames", caltable, 0, aux,
                       suppessStep);
   }

}


