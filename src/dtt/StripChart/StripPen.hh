//
//    Strip chart pen class
//
#ifndef STRIPPEN_HH
#define STRIPPEN_HH

#include <string>

//--------------------------------------  Root display classes
class TPolyLine;
class TText;

/**  The strip chart pen class.
  *  The pen class maintains the data array for a single pen of a 
  *  strip chart. It saves and performs the normalization of the data
  *  from the user's units to the chart units (defined in the strip 
  *  chart by mxmin, mXmax, mYmin, mYmax). The strip pen class also 
  *  handles the display of the strip data by maintaining the root
  *  display primitives associated with the pen.
  */
class StripPen {
public:
  //------------------------------------  Constructors, destructors
  /**  Construct a pen.
    *  A pen object is constructed with N history points and an optional
    *  title.
    */
  StripPen(int N=0, const char* Title=0);

  /**  Destroy a pen.
    *  Release all data storage.
    */
  ~StripPen();

  //------------------------------------  Accessors
  /** Test logarithmic scale.
    * getLogY() returns true if the pen scasle is logarithmic.
    */
  bool getLogY(void) const {return mLogY;}

  /**  Return pointer to TPolyLine.
    *  getPLine returns a pointer to the TPolyline object used to display 
    *  the pen line.
    */
  TPolyLine* getPLine() const {return mPLine;}

  /**  Get a data point.
    *  getData returns the normalized y coordinate in from position 'inx'
    *  of the history buffer.
    */
  float getData(int inx) const {return mYData[inx];}

  /**  Write the pen title.
    *  The pen title is written to the specified position on fhte current 
    *  canvas.
    */
  void putTitle(float xpos, float ypos, const char* opts);

  /**  Set the bias.
    *  The bias value used in normalizin the pen data is se tto the
    *  specified vaslue.
    */
  void setBias(float bias);

  /**  Set the pen color.
    *  The pen line color is set to the specified index.
    */
  void setColor(int color);

  /**  Set a data value.
    *  A datum is normalized ian store in the specified istory array index.
    */
  void setData(int inx, float y);

  /**  Set the scaleto log/lonear.
    *  The scale mode is set. If 'Log' is true, a logaritmic scale is used, 
    *  otherwise a linear scale is used.
    */
  void setLogY(bool log);

  /**  Set the history length.
    *  The history buffer is reallocated to hold 'N' entries and the 
    *  polyline object is reallocated to hold 2N+1 points. All associated
    *  information is lost, including the line color.
    */
  void setN(int N);

  /**  Set the normaliztion scale factor.
    *  The normalization scale factor is set to the specified value.
    */
  void setScale(float scale);

  /**  Set the pen title.
    *  The pen title is set to the specified string.
    */
  void setTitle(const char* title);

  /**  Clear polyline data.
    *  All polyline (x,y) points in the range start:end are set to (0,0).
    *  If start is omitted, is is assumed to be 0. If end is omitted or
    *  zero it is assumed to be last+1.
    */
  void clear(int start=0, int end=0);

  double transFromDisplay(double x) {return (x - mBias)/mScale;}
  double transToDisplay(double x) {return x*mScale + mBias;}

private:
  /**  Pointer to the root Polyline.
    *  mPLine points to the root polyline used to display the pen.
    */
  TPolyLine* mPLine;

  /**  Pointer to the pen Label.
    *  mText points to a TText object used to display the pen Title.
    */
  TText*  mText;

  /**  Normalization bias.
    *  The normalized Y values plotted are Ynorm[i] = Y[i] * mScale + mBias.
    */
  float   mBias;
  /**  The normalized Y values plotted are Ynorm[i] = Y[i] * mScale + mBias.
    *  @memo Normalization scale
    */
  float   mScale;

  /**  Vertical scale mode.
    *  If mLogY is set the decimal logaritm of the specified Y-value is taken 
    *  before it is normalized.
    */
  bool    mLogY;

  /**  Normalized data vector.
    *  mYData is a circular buffer containing the normalized data history. 
    *  The buffer indices are managed externally.
    */
  float*  mYData;

  /**  Pen Title.
    *  mTitle contains a string which is used to lable the pen polyline.
    */
  std::string  mTitle;
};
#endif
