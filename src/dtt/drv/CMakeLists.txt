SET(DRV_SRC
    cobox.c
    ds340.c
    #epics.c
    #gdsdac.c
    #gdsics115.c
    #gpsclk.c
    #hardware.h
    #ntp.c
    #pci.c
    #rmapi.c
    #timingcard.c
)

add_library(drv_lib OBJECT
    ${DRV_SRC}
)

target_include_directories(drv_lib PRIVATE
    ${DTT_INCLUDES}
)