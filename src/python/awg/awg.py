# Version $Id: awg.py 7147 2014-08-06 17:39:59Z james.batch@LIGO.ORG $
"""
AWG excitation library.

Author: Christopher Wipf, Massachusetts Institute of Technology

Drive excitation channels with the standard waveforms supplied by the
module (e.g. `Sine`, `SweptSine`, `GaussianNoise`, etc.), with a
waveform superposition, or with arbitrary data (`ArbitraryStream`).
Arbitrary data may also be periodically repeated (`ArbitraryLoop`).

Example:
  >>> chan = 'M1:PDE-LSC_DARM_EXC'
  >>> noise = UniformNoise(chan, ampl=20)
  >>> noise.start(ramptime=1)    # returns after setting the excitation
  >>> noise.stop(ramptime=1)     # returns after stopping the excitation
  >>> exc_list = [Sine(chan, freq=200, ampl=50), UniformNoise(chan, ampl=20)]
  >>> noisy_sine = Excitation.compose(exc_list)
  >>> noisy_sine.start()
  >>> noisy_sine.stop()
  >>> from random import uniform                           # this will make a
  >>> random_data = [uniform(0, 20) for n in range(65536)] # frequency comb
  >>> comb = ArbitraryLoop(chan, random_data, rate=65536)  # with 1 Hz spacing
  >>> comb.start()
  >>> comb.stop()

This module depends on the `awgbase` module, which wraps the following
shared libraries: libawg (standard excitation waveforms), libtestpoint
(testpoint handling), and libSIStr (arbitrary loops and streams).  All
these shared libraries must be made available for loading (e.g. by
setting the LD_LIBRARY_PATH environment variable).
  
"""

import sys, numpy, awgbase
from time import sleep
from threading import Thread, Event, Lock


#handle python2 and python3 exceptions with traceback
if sys.version_info[0] == 3:
    def reraise(tp, value, tb=None):
        if value is None:
            value = tp()
        if value.__traceback__ is not tb:
            raise value.with_traceback(tb)
        raise value

else:
    exec("def reraise(tp, value, tb=None):\n    raise tp, value, tb\n")

__docformat__ = 'restructuredtext'
__all__ = ['Excitation', 'Sine', 'Square', 'Ramp', 'Triangle', 'Impulse',
           'Constant', 'GaussianNoise', 'UniformNoise', 'SweptSine',
           'ArbitraryStream', 'ArbitraryLoop', 'awg_cleanup', 'AWGError',
           'AWGStreamError']

class Excitation(object):
    "Base class for AWG excitations."
    _rampmap = {'step':awgbase.AWG_PHASING_STEP,
                'linear':awgbase.AWG_PHASING_LINEAR,
                'quadratic':awgbase.AWG_PHASING_QUADRATIC,
                'log':awgbase.AWG_PHASING_LOG}
    def __init__(self, chan, start=0, duration=-1, restart=-1):
        """
        Initialize an `Excitation`.

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).
        
        """
        self.chan = chan
        self.starttime = start
        self.comp = awgbase.AWG_Component()

        # convert times to integer nsec
        self.comp.start = int(start*10**9)
        if duration < 0:
            self.comp.duration = -1
        else:
            self.comp.duration = int(duration*10**9)
        if restart <= 0:
            self.comp.restart = -1
        else:
            self.comp.restart = int(restart*10**9)

        self.components = [self.comp,]
        self.gain = 1
        self.slot = None
        self.started = False
        self.stopped = False

    def _get_slot(self):
        if awgbase.tpRequestName(self.chan, -1, None, None) < 0:
            raise AWGError("can't get testpoint for " + self.chan)
        self.slot = awgbase.awgSetChannel(self.chan)
        if self.slot < 0:
            awgbase.tpClearName(self.chan)
            raise AWGError("can't set channel for " + self.chan)

    def start(self, ramptime=1, wait=True):
        """
        Starts the excitation.

        :Parameters:Debug
          ramptime : float
            Duration of linear amplitude ramp in sec.
          wait : bool
            If ``True``, sleep until the excitation starts.
        
        """
        if self.started:
            raise AWGError("excitation already started")
        if self.slot is None:
            self._get_slot()
        if ramptime > 0:
            orig_gain = self.gain
            self.set_gain(0)
            # make sure the new gain setting takes effect
            sleep(2*awgbase._EPOCH/float(10**9))
        if self.starttime == 0:
            # an actual start time is nice to have (and setting this avoids
            # some bugs in awg)
            starttime_nsec = awgbase.GPSnow() + 4*awgbase._EPOCH
            for comp in self.components:
                if comp.start == 0:
                    comp.start = int(starttime_nsec)
            self.starttime = starttime_nsec/float(10**9)
        ret = awgbase.awgAddWaveform(self.slot, self.components)
        self.started = True
        if ret != 0:
            raise AWGError("can't add waveform, error %d" % ret)
        if wait:
            sleeptime = self.starttime - awgbase.GPSnow()/float(10**9)
            if sleeptime > 0:
                sleep(sleeptime)
        if ramptime > 0:
            self.set_gain(orig_gain, ramptime=ramptime)
            if wait:
                sleep(ramptime)
        return self

    def stop(self, ramptime=1, wait=True):
        """
        Stops the excitation.

        :Parameters:
          ramptime : float
            Duration of linear amplitude ramp in sec.
          wait : bool
            If ``True``, sleep until the ``ramptime`` is complete, and
            clean up after the excitation as well as stopping it.
        
        """
        if self.stopped or not self.started:
            return
        if ramptime > 0:
            self.set_gain(0, ramptime=ramptime, wait=wait)
        ret = awgbase.awgStopWaveform(self.slot, 0, 0)
        self.stopped = True
        if wait:
            self.clear()
        if ret != 0:
            raise AWGError("problem stopping waveform, error %d" % ret)

    def clear(self):
        awgbase.awgClearWaveforms(self.slot)
        awgbase.awgRemoveChannel(self.slot)
        awgbase.tpClearName(self.chan)

    def set_gain(self, gain, ramptime=0, wait=False):
        """
        Set the gain applied to the excitation.

        :Parameters:
          gain : float
            Overall gain.
          ramptime : float
            Ramping time in sec.

        """
        if self.slot is None:
            self._get_slot()
        ret = awgbase.awgSetGain(self.slot, gain, int(ramptime*10**9))
        if ret < 0:
            raise AWGError("can't set gain, error %d" % ret)
        self.gain = gain
        if wait:
            sleep(ramptime)

    def set_filter(self, coeffs):
        """
        Set the filter applied to the excitation.

        :Parameters:
          coeffs : list
            Contains the second order section coefficents used in the
            filter.  Format: ``coeffs[0]`` is the overall gain, while
            ``coeffs[n:n+4]`` contains the coefficients of the nth
            stage in the order ``(b1, b2, a1, a2)``, where ``b0`` is
            constrained to be ``1``.

        """
        if self.slot is None:
            self._get_slot()
        ret = awgbase.awgSetFilter(self.slot, coeffs)
        if ret < 0:
            raise AWGError("can't set filter, error %d" % ret)
        self.coeffs = coeffs

    def set_phase_in(self, ramp='step', ramptime=0):
        self.comp.ramptype &= 0xFFF0
        self.comp.ramptype |= self._rampmap[ramp]
        self.comp.ramptime = [int(ramptime*10**9), self.comp.ramptime[1]]

    def set_phase_out(self, val=None, amplramp='step', freqramp='step',
                      ramptime=0):
        if val is None:
            par = [0, 0, 0, 0]
        else:
            par = val.comp.par
        self.comp.ramppar = par
        self.comp.ramptype &= 0x0F0F
        self.comp.ramptype |= (self._rampmap[amplramp] << 4)
        self.comp.ramptype |= (self._rampmap[freqramp] << 12)
        self.comp.ramptime = [self.comp.ramptime[0], int(ramptime*10**9)]

    @staticmethod
    def compose(exc_list):
        components = list()
        for exc in exc_list:
            if not isinstance(exc, Excitation):
                raise ValueError
            if exc.chan != exc_list[0].chan:
                raise ValueError
            components.append(exc.comp)
        new_exc = Excitation(exc_list[0].chan)
        new_exc.components = components
        return new_exc

class Sine(Excitation):
    "Sine wave excitation."
    def __init__(self, chan, ampl=0, freq=0, phase=0, offset=0, start=0,
                 duration=-1, restart=-1):
        """
        Initialize a `Sine` excitation.

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq : float
            Frequency in cycles per sec.
          phase : float
            Phase in radians.
          offset : float
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        Excitation.__init__(self, chan, start=start, duration=duration,
                            restart=restart)
        self.comp.wtype = awgbase.awgSine
        self.comp.par = (ampl, freq, phase, offset)

class Square(Excitation):
    "Square wave excitation."
    def __init__(self, chan, ampl=0, freq=0, phase=0, offset=0, start=0,
                 duration=-1, restart=-1):
        """
        Initialize a `Square` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq : float
            Frequency in cycles per sec.
          phase : float
            Phase in radians.
          offset : float
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        Excitation.__init__(self, chan, start=start, duration=duration,
                            restart=restart)
        self.comp.wtype = awgbase.awgSquare
        self.comp.par = (ampl, freq, phase, offset)

class Ramp(Excitation):
    "Ramp excitation."
    def __init__(self, chan, ampl=0, freq=0, phase=0, offset=0, start=0,
                 duration=-1, restart=-1):
        """
        Initialize a `Ramp` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq : float
            Frequency in cycles per sec.
          phase : float
            Phase in radians.
          offset : float
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        Excitation.__init__(self, chan, start=start, duration=duration,
                            restart=restart)
        self.comp.wtype = awgbase.awgRamp
        self.comp.par = (ampl, freq, phase, offset)

class Triangle(Excitation):
    "Triangle wave excitation."
    def __init__(self, chan, ampl=0, freq=0, phase=0, offset=0, start=0,
                 duration=-1, restart=-1):
        """
        Initialize a `Triangle` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq : float
            Frequency in cycles per sec.
          phase : float
            Phase in radians.
          offset : float
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        Excitation.__init__(self, chan, start, duration, restart)
        self.comp.wtype = awgbase.awgTriangle
        self.comp.par = (ampl, freq, phase, offset)

class Impulse(Excitation):
    "Impulse excitation."
    def __init__(self, chan, ampl=0, freq=0, width=0, delay=0, start=0,
                 duration=-1, restart=-1):
        """
        Initialize an `Impulse` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq : float
            Frequency in cycles per sec.
          width : float
            Width in ?.
          delay : float
            Delay in ?.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        Excitation.__init__(self, chan, start=start, duration=duration,
                            restart=restart)
        self.comp.wtype = awgbase.awgImpulse
        self.comp.par = (ampl, freq, width, delay)

class Constant(Excitation):
    "Constant excitation."
    def __init__(self, chan, offset=0, start=0, duration=-1, restart=-1):
        """
        Initialize a `Constant` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          offset : float
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        Excitation.__init__(self, chan, start=start, duration=duration,
                            restart=restart)
        self.comp.wtype = awgbase.awgConst
        self.comp.par = (offset, offset, offset, offset)

class GaussianNoise(Excitation):
    "Gaussian random noise excitation."
    def __init__(self, chan, ampl=0, freq1=0, freq2=0, offset=0, start=0,
                 duration=-1, restart=-1):
        """
        Initialize a `GaussianNoise` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq1 : float
            Bandlimit frequency (not implemented).
          freq2 : float
            Bandlimit frequency (not implemented).
          offset : float
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        Excitation.__init__(self, chan, start=start, duration=duration,
                            restart=restart)
        self.comp.wtype = awgbase.awgNoiseN
        self.comp.par = (ampl, freq1, freq2, offset)

class UniformNoise(Excitation):
    "Uniform random noise excitation."
    def __init__(self, chan, ampl=0, freq1=0, freq2=0, offset=0, start=0,
                 duration=-1, restart=-1):
        """
        Initialize a `UniformNoise` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq1 : float
            Bandlimit frequency (not implemented).
          freq2 : float
            Bandlimit frequency (not implemented).
          offset : float
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        Excitation.__init__(self, chan, start=start, duration=duration,
                            restart=restart)
        self.comp.wtype = awgbase.awgNoiseU
        self.comp.par = (ampl, freq1, freq2, offset)

class SweptSine(Sine):
    "Swept sine excitation."
    def __init__(self, chan, ampl1=0, freq1=0, phase1=0, offset1=0,
                 ampl2=0, freq2=0, phase2=0, offset2=0, amplramp='linear',
                 freqramp='linear', start=0, sweeptime=0):
        """
        Initialize a `SweptSine` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl1 : float
            Amplitude of the start of the sweep in counts.
          freq1 : float
            Frequency of the start of the sweep in cycles per sec.
          phase1 : float
            Phase of the start of the sweep in radians.
          offset1 : float
            Offset of the start of the sweep in counts.
          ampl2 : float
            Amplitude of the end of the sweep in counts.
          freq2 : float
            Frequency of the end of the sweep in cycles per sec.
          phase2 : float
            Phase of the end of the sweep in radians.
          offset2 : float
            Offset of the end of the sweep in counts.
          ramptype : string
            If ``'log'``, perform a logarithmic sweep; otherwise sweep
            linearly.
          start : float
            Start time in GPS sec (immediate if = 0).
          sweeptime : float
            Sweep duration in sec.

        """
        Sine.__init__(self, chan, ampl=ampl1, freq=freq1, phase=phase1,
                      offset=offset1, start=start, duration=sweeptime,
                      restart=sweeptime)
        end_sweep = Sine(chan, ampl=ampl2, freq=freq2, phase=phase2,
                         offset=offset2)
        self.set_phase_out(val=end_sweep, amplramp=amplramp, freqramp=freqramp,
                           ramptime=sweeptime)

    def freeze(self):
        ret = awgbase.awgStopWaveform(self.slot, 1, 0)
        if ret != 0:
            raise AWGError("problem freezing waveform, error %d" % ret)

# this excitation is not currently implemented by the front end...
# we will have to fake it using an ArbitraryStream
## class ArbitraryLoop(Excitation):
##     def __init__(self, chan, data, scale=1, rate=0, freq=0, trig=0, start=0,
##                  duration=-1, restart=-1):
##         Excitation.__init__(self, chan, start, duration, restart)
##         self.comp.wtype = awgapi.awgArb
##         self.comp.par = [scale, rate, freq, trig]
##         self.data = data

##     def start(self):
##         self._get_slot()
##         datalen = len(self.data)
##         fdata = awgapi.floatArray(datalen)
##         for n in xrange(datalen):
##             fdata[n] = self.data[n]
##         ret = awgapi.awgSetWaveform(self.slot, fdata, datalen)
##         if ret != 0:
##             raise AWGError("can't set arbitrary waveform, error %d" % ret)
##         ret = awgapi.awgAddWaveform(self.slot, self.comp, 1)
##         if ret != 0:
##             raise AWGError("can't add waveform, error %d" % ret)

class ArbitraryStream(object):
    "Arbitrary stream excitation."
    def __init__(self, chan, rate=0, start=0,
                 appinfo="cdsutils.awg Python module"):
        """
        Initialize an `ArbitraryStream` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          rate : int
            Sample rate of the excitation channel and user-supplied data.
          start
            GPS second time at which to begin the excitation.
          appinfo : string
            Passed to `SIStrAppInfo()` for logging purposes.

        """
        self.chan = chan
        self.rate = rate
        self.starttime = start
        self.gain = 1
        self.opened = False
        self.sistream = awgbase.SIStream()
        self.sistream.slot = 0
        awgbase.SIStrAppInfo(appinfo)

    def open(self):
        "Open the stream."
        if self.starttime == 0:
            self.starttime = int((awgbase.GPSnow()
                                  + awgbase.SIStr_LEADTIME)/float(10**9))
        ret = awgbase.SIStrOpen(self.sistream, self.chan, self.rate,
                                self.starttime)
        if ret != awgbase.SIStr_OK:
            raise AWGStreamError("can't open stream to " + self.chan \
                                 + ": " + awgbase.SIStrErrorMsg(ret) + " (" + str(ret) + ")")
        self.opened = True

    def append(self, data, scale=1):
        """
        Send excitation data to the front end.

        :Parameters:
          data : list
            To be output onto the excitation channel, at the specified
            sampling rate
          scale
            Multiplicative scaling factor to apply to `data`.
        """
        if not self.opened:
            self.open()
        ret = awgbase.SIStrAppend(self.sistream, data, scale)
        if ret != awgbase.SIStr_OK:
            try:
                self.close()
            except:
                pass
            raise AWGStreamError("couldn't stream data to " + self.chan + \
                                 ": " + awgbase.SIStrErrorMsg(ret) + " (" + str(ret) + ")")

    def close(self):
        "Close the stream."
        if not self.opened:
            return
        ret = awgbase.SIStrClose(self.sistream)
        if ret != awgbase.SIStr_OK:
            raise AWGStreamError("couldn't close stream to " + self.chan \
                                 + ": " + awgbase.SIStrErrorMsg(ret) + " (" + str(ret) + ")")
        self.opened = False

    def send(self, data, scale=1):
        "`open()`, `append()`, and `close()` in one shot."
        self.append(data, scale=scale)
        self.close()

    def abort(self):
        awgbase.SIStrAbort(self.sistream)

    def set_gain(self, gain, ramptime=0, wait=False):
        """
        Set the gain applied to the excitation.  (Not thread safe?)

        :Parameters:
          gain : float
            Overall gain.
          ramptime : float
            Ramping time in sec.

        """
        if self.sistream.slot <= 0:
            raise AWGError("can't set gain, awg slot unavailable")
        ret = awgbase.awgSetGain(self.sistream.slot, gain, int(ramptime*10**9))
        if ret < 0:
            raise AWGError("can't set gain, error %d" % ret)
        self.gain = gain
        if wait:
            sleep(ramptime)

class ArbitraryLoop(ArbitraryStream):
    "Arbitrary loop excitation."
    def __init__(self, chan, data, scale=1, rate=0, start=0,
                 appinfo="cdsutils.awg Python module"):
        """
        Initialize an `ArbitraryLoop` excitation

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          data : list
            To be looped onto the excitation channel, at the specified
            sampling rate.
          scale : float
            Multiplicative scaling factor to apply to `data`.
          rate : int
            Sample rate of the excitation channel and user-supplied data.
          start : int
            Start time in GPS seconds.
          appinfo : string
            Passed to `SIStrAppInfo()` for logging purposes.

        """
        ArbitraryStream.__init__(self, chan=chan, rate=rate, start=start,
                                 appinfo=appinfo)
        self.data = numpy.array(data).astype('f')
        self.chunks = [slice(n, n+self.rate)
                       for n in range(0, len(self.data), self.rate)]
        self.scale = scale
        self.thread = Thread(target=self._loop)
        self.loop_run_event = Event()
        self.open_event = Event()
        self.awg_lock = Lock()
        self.loop_exception = None
        self.keep_running = True

    def start(self, ramptime=1, wait=True):
        """
        Starts the excitation.

        :Parameters:
          ramptime : float
            Duration of linear amplitude ramp in sec.
          wait : bool
            If ``True``, sleep until the excitation starts.
        
        """
        if self.thread.isAlive() or not self.keep_running:
            raise AWGStreamError("couldn't start stream to " + self.chan + \
                                 ": stream already started")
        self.loop_run_event.set()
        self.thread.start()
        self.open_event.wait()
        if self.loop_exception is not None:
            cls, val, tb = self.loop_exception
            reraise(cls, val, tb)
        if ramptime > 0:
            orig_gain = self.gain
            self.set_gain(0)
            delay = max(0, self.starttime - awgbase.GPSnow()/float(10**9))
            sleep(delay)
            self.set_gain(orig_gain, ramptime=ramptime)
        if wait:
            delay = max(0, self.starttime - awgbase.GPSnow()/float(10**9)
                        + ramptime)
            sleep(delay)

    def set_gain(self, gain, ramptime=1, wait=False):
        self.loop_run_event.clear()
        self.awg_lock.acquire()
        ArbitraryStream.set_gain(self, gain, ramptime=ramptime, wait=wait)
        self.awg_lock.release()
        self.loop_run_event.set()

    def _loop(self):
        while self.keep_running:
            try:
                for chunk in self.chunks:
                    self.loop_run_event.wait()
                    self.awg_lock.acquire()
                    self.append(self.data[chunk], scale=self.scale)
                    self.open_event.set()
                    self.awg_lock.release()
                    if not self.keep_running:
                        break
            except:
                self.loop_exception = sys.exc_info()
                self.open_event.set()
                break

    def stop(self, ramptime=1):
        """
        Stops the excitation, returning after it has done so.

        :Parameters:
          ramptime : float
            Duration of linear amplitude ramp in sec.
        
        """
        if ramptime > 0:
            try:
                self.set_gain(0, ramptime=ramptime)
            except:
                pass
            delay = max(0, ramptime - awgbase.SIStr_LEADTIME/float(10**9))
            sleep(delay)
        self.keep_running = False
        self.thread.join()
        if self.loop_exception is not None:
            try:
                self.abort()
                self.close()
            except:
                pass
            cls, val, tb = self.loop_exception
            reraise(cls, val, tb)
        self.abort()
        self.close()

def awg_cleanup():
    awgbase.awg_cleanup()
    awgbase.testpoint_cleanup()

def awg_debug(new_level):
    awgbase.SIStrDebug(new_level)

class AWGError(Exception):
    pass

class AWGStreamError(AWGError):
    pass
