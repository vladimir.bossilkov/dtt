import foton
import ROOT

import unittest
import numpy as np
import io


input_file_name = 'test_filter.txt'
output_file_name = 'test_out.txt'


class TestFotonMethods(unittest.TestCase):
    def setUp(self):
        self.file = foton.FilterFile(input_file_name)
        self.module = self.file['BOUNCE_ALL']
        self.section = self.module[0]
        self.design = foton.FilterDesign(self.section.filt)
        self.filter = foton.Filter(self.design)

    # Module

    def test_module_instance(self):
        self.assertIsInstance(self.module, foton.Module)

    def test_module_fm_instance(self):
        self.assertIsInstance(self.module.fm, ROOT.filterwiz.FilterModule)

    def test_module_name(self):
        self.assertEqual(self.module.name, 'BOUNCE_ALL')

    def test_module_rate(self):
        self.assertEqual(self.module.rate, 16384.0)

    def test_module_length(self):
        self.assertEqual(len(self.module), 10)

    def test_module_list_instance(self):
        for v in self.module:
            self.assertIsInstance(v, foton.Section)

    # Section

    def test_section_instance(self):
        self.assertIsInstance(self.section, foton.Section)

    def test_section_sec_instance(self):
        self.assertIsInstance(self.section.sec, ROOT.filterwiz.FilterSection)

    def test_section_index(self):
        self.assertEqual(self.section.index, 0)

    def test_section_name(self):
        self.assertEqual(self.section.name, 'bp9-11')

    def test_section_design(self):
        self.assertEqual(self.section.design, 'cheby1("BandPass",4,1,9,11)')

    def test_section_filt(self):
        self.assertIsInstance(self.section.filt, foton.FilterDesign)

    def test_section_order(self):
        self.assertEqual(self.section.order, 8)

    def test_section_input_switch(self):
        self.assertEqual(self.section.input_switch, 'ZeroHistory')

    def test_section_output_switch(self):
        self.assertEqual(self.section.output_switch, 'Immediately')

    def test_section_ramp(self):
        self.assertEqual(self.section.ramp, 0.0)

    def test_section_tolerance(self):
        self.assertEqual(self.section.tolerance, 0.0)

    def test_section_timeout(self):
        self.assertEqual(self.section.timeout, 0.0)

    def test_section_header(self):
        lhs = self.section.header
        rhs = '\n# FILTERS FOR ONLINE ## Computer generated file: DO NOT EDIT###################################################################################### BOUNCE_ALL                                                               ######################################################################################                                                                          ###'
        self.assertEqual(lhs, rhs)

    def test_section_empty(self):
        self.assertFalse(self.section.empty())

    def test_section_check(self):
        self.assertTrue(self.section.check())

    def test_section_valid(self):
        self.assertTrue(self.section.valid())

    def test_section_refresh(self):
        self.assertTrue(self.section.refresh())

    # Design

    def test_design_instance(self):
        self.assertIsInstance(self.design, foton.FilterDesign)

    def test_design_string(self):
        self.assertEqual(self.design.string, 'cheby1("BandPass",4,1,9,11)')

    def test_design_design(self):
        self.assertEqual(self.design.design, 'cheby1("BandPass",4,1,9,11)')

    def test_design_rate(self):
        self.assertEqual(self.design.rate, 16384.0)

    def test_design_iir2zpk(self):
        lhs = foton.iir2zpk(self.design)
        rhs = 'zpk([0;0;0;0],[-0.7904940545682951+i*56.6366834383501;-0.7904940545682951-i*56.6366834383501;-2.029992546456222+i*59.97419591565824;-2.029992546456222-i*59.97419591565824;-2.203247319560259+i*65.09284313621457;-2.203247319560259-i*65.09284313621457;-0.9629712418529643+i*68.99419049355016;-0.9629712418529643-i*68.99419049355016],6125.820627807043)'
        self.assertEqual(lhs, rhs)

    def test_design_iir2z(self):
        lhs = foton.iir2z(self.design)
        rhs = [5.3113353394938464e-15, -2.0, 1.0, -1.9998915575865894, 0.999903508991431, -2.0, 1.0,
               -1.9997388162416827, 0.9997522294284502, 2.0, 1.0, -1.9997152854972924, 0.9997310857818233,
               2.0, 1.0, -1.9998647215491248, 0.9998824570556457]
        self.assertEqual(lhs, rhs)

    def test_design_iir2poly(self):
        lhs = foton.iir2poly(self.design)
        rhs = ([1.0, -0.0, 0.0, -0.0, 0.0],
               [1.0, 11.97341032487548, 15863.048550492917, 141862.9758392324,
                93453381.49923556, 554452064.8544362, 242312899647.63275,
                714829857259.9366, 233334932716774.6], 6125.820627807043)
        self.assertEqual(lhs, rhs)

    def test_design_iir2direct(self):
        lhs = foton.iir2direct(self.design)
        rhs = (
            [5.3113353394938464e-15, -2.1245341357975386e-14, 3.186801203696308e-14, -2.1245341357975386e-14,
             5.3113353394938464e-15],
            [7.99921038087469, -27.994531788585004, 55.98377269830372, -69.97324999465526, 55.97354543666938,
             -27.98430449470714, 7.994827231895801, -0.9992694697961847])
        self.assertEqual(lhs, rhs)

    # Filter

    def test_filter_instance(self):
        self.assertIsInstance(self.filter, foton.Filter)

    def test_filter_apply(self):
        lhs = self.filter.apply([1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1])
        rhs = np.array([5.3113353394938464e-15, 5.3109159462973460e-14,
                        2.7083165835495728e-13, 9.5577660914395666e-13,
                        2.6599410852062305e-12, 6.2747841232037422e-12,
                        1.3115907390871857e-11, 2.5007648899668323e-11,
                        4.4367584811872717e-11, 7.4280311726705639e-11,
                        1.1852864488238310e-10, 1.8156101617272869e-10,
                        2.6841636582665616e-10, 3.8463843122555474e-10,
                        5.3619006831396447e-10, 7.2936761061890929e-10,
                        9.7071527088958473e-10])
        self.assertIsNone(np.testing.assert_array_equal(lhs, rhs))

    # File

    def test_file_write(self):
        self.file.filename = output_file_name
        self.file.write()
        with io.open(output_file_name) as tst_f, io.open(input_file_name) as ref_f:
            self.assertListEqual(list(tst_f), list(ref_f))

    def test_zroots_design(self):
        foton.FilterDesign(rate=16384).zroots([0.1], [0.2], 1)

if __name__ == '__main__':
    unittest.main()
